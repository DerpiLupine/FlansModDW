package com.flansmod.client.model.dwvehicles; //Path where the model is located

import com.flansmod.client.model.ModelVehicle;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelK2Timberwolf extends ModelVehicle //Same as Filename
{
	int textureX = 1024;
	int textureY = 1024;

	public ModelK2Timberwolf() //Same as Filename
	{
		bodyModel = new ModelRendererTurbo[130];
		turretModel = new ModelRendererTurbo[114];
		barrelModel = new ModelRendererTurbo[25];
		leftTrackWheelModels = new ModelRendererTurbo[100];
		rightTrackWheelModels = new ModelRendererTurbo[100];

		initbodyModel_1();
		initturretModel_1();
		initbarrelModel_1();
		initleftTrackWheelModels_1();
		initrightTrackWheelModels_1();

		translateAll(0F, 0F, 0F);


		flipAll();
	}

	private void initbodyModel_1()
	{
		bodyModel[0] = new ModelRendererTurbo(this, 112, 292, textureX, textureY); // Box 10
		bodyModel[1] = new ModelRendererTurbo(this, 1, 212, textureX, textureY); // Box 11
		bodyModel[2] = new ModelRendererTurbo(this, 1, 63, textureX, textureY); // Box 12
		bodyModel[3] = new ModelRendererTurbo(this, 1, 44, textureX, textureY); // Box 13
		bodyModel[4] = new ModelRendererTurbo(this, 1, 190, textureX, textureY); // Box 14
		bodyModel[5] = new ModelRendererTurbo(this, 112, 232, textureX, textureY); // Box 15
		bodyModel[6] = new ModelRendererTurbo(this, 112, 61, textureX, textureY); // Box 16
		bodyModel[7] = new ModelRendererTurbo(this, 112, 83, textureX, textureY); // Box 17
		bodyModel[8] = new ModelRendererTurbo(this, 288, 452, textureX, textureY); // Box 18
		bodyModel[9] = new ModelRendererTurbo(this, 205, 452, textureX, textureY); // Box 19
		bodyModel[10] = new ModelRendererTurbo(this, 42, 164, textureX, textureY); // Box 21
		bodyModel[11] = new ModelRendererTurbo(this, 42, 164, textureX, textureY); // Box 24
		bodyModel[12] = new ModelRendererTurbo(this, 112, 446, textureX, textureY); // Box 68
		bodyModel[13] = new ModelRendererTurbo(this, 112, 401, textureX, textureY); // Box 69
		bodyModel[14] = new ModelRendererTurbo(this, 239, 337, textureX, textureY); // Box 70
		bodyModel[15] = new ModelRendererTurbo(this, 357, 253, textureX, textureY); // Box 71
		bodyModel[16] = new ModelRendererTurbo(this, 112, 337, textureX, textureY); // Box 0
		bodyModel[17] = new ModelRendererTurbo(this, 1, 164, textureX, textureY); // Box 1
		bodyModel[18] = new ModelRendererTurbo(this, 406, 331, textureX, textureY); // Box 2
		bodyModel[19] = new ModelRendererTurbo(this, 1, 251, textureX, textureY); // Box 3
		bodyModel[20] = new ModelRendererTurbo(this, 1, 251, textureX, textureY); // Box 4
		bodyModel[21] = new ModelRendererTurbo(this, 1, 251, textureX, textureY); // Box 5
		bodyModel[22] = new ModelRendererTurbo(this, 1, 251, textureX, textureY); // Box 6
		bodyModel[23] = new ModelRendererTurbo(this, 36, 257, textureX, textureY); // Box 7
		bodyModel[24] = new ModelRendererTurbo(this, 1, 114, textureX, textureY); // Box 120
		bodyModel[25] = new ModelRendererTurbo(this, 1, 114, textureX, textureY); // Box 121
		bodyModel[26] = new ModelRendererTurbo(this, 1, 164, textureX, textureY); // Box 381
		bodyModel[27] = new ModelRendererTurbo(this, 1, 164, textureX, textureY); // Box 382
		bodyModel[28] = new ModelRendererTurbo(this, 1, 164, textureX, textureY); // Box 383
		bodyModel[29] = new ModelRendererTurbo(this, 36, 257, textureX, textureY); // Box 326
		bodyModel[30] = new ModelRendererTurbo(this, 1, 279, textureX, textureY); // Box 327
		bodyModel[31] = new ModelRendererTurbo(this, 1, 279, textureX, textureY); // Box 328
		bodyModel[32] = new ModelRendererTurbo(this, 54, 279, textureX, textureY); // Box 329
		bodyModel[33] = new ModelRendererTurbo(this, 1, 279, textureX, textureY); // Box 330
		bodyModel[34] = new ModelRendererTurbo(this, 54, 279, textureX, textureY); // Box 331
		bodyModel[35] = new ModelRendererTurbo(this, 1, 279, textureX, textureY); // Box 332
		bodyModel[36] = new ModelRendererTurbo(this, 1, 279, textureX, textureY); // Box 333
		bodyModel[37] = new ModelRendererTurbo(this, 1, 279, textureX, textureY); // Box 334
		bodyModel[38] = new ModelRendererTurbo(this, 1, 279, textureX, textureY); // Box 335
		bodyModel[39] = new ModelRendererTurbo(this, 1, 279, textureX, textureY); // Box 336
		bodyModel[40] = new ModelRendererTurbo(this, 1, 308, textureX, textureY); // Box 337
		bodyModel[41] = new ModelRendererTurbo(this, 1, 308, textureX, textureY); // Box 338
		bodyModel[42] = new ModelRendererTurbo(this, 26, 327, textureX, textureY); // Box 339
		bodyModel[43] = new ModelRendererTurbo(this, 1, 327, textureX, textureY); // Box 340
		bodyModel[44] = new ModelRendererTurbo(this, 33, 308, textureX, textureY); // Box 341
		bodyModel[45] = new ModelRendererTurbo(this, 33, 308, textureX, textureY); // Box 342
		bodyModel[46] = new ModelRendererTurbo(this, 33, 315, textureX, textureY); // Box 343
		bodyModel[47] = new ModelRendererTurbo(this, 50, 315, textureX, textureY); // Box 344
		bodyModel[48] = new ModelRendererTurbo(this, 1, 300, textureX, textureY); // Box 345
		bodyModel[49] = new ModelRendererTurbo(this, 1, 300, textureX, textureY); // Box 346
		bodyModel[50] = new ModelRendererTurbo(this, 1, 300, textureX, textureY); // Box 347
		bodyModel[51] = new ModelRendererTurbo(this, 1, 300, textureX, textureY); // Box 348
		bodyModel[52] = new ModelRendererTurbo(this, 1, 300, textureX, textureY); // Box 349
		bodyModel[53] = new ModelRendererTurbo(this, 1, 300, textureX, textureY); // Box 350
		bodyModel[54] = new ModelRendererTurbo(this, 1, 356, textureX, textureY); // Box 351
		bodyModel[55] = new ModelRendererTurbo(this, 1, 371, textureX, textureY); // Box 352
		bodyModel[56] = new ModelRendererTurbo(this, 1, 371, textureX, textureY); // Box 353
		bodyModel[57] = new ModelRendererTurbo(this, 1, 356, textureX, textureY); // Box 354
		bodyModel[58] = new ModelRendererTurbo(this, 1, 339, textureX, textureY); // Box 356
		bodyModel[59] = new ModelRendererTurbo(this, 1, 339, textureX, textureY); // Box 357
		bodyModel[60] = new ModelRendererTurbo(this, 1, 339, textureX, textureY); // Box 358
		bodyModel[61] = new ModelRendererTurbo(this, 71, 343, textureX, textureY); // Box 359
		bodyModel[62] = new ModelRendererTurbo(this, 71, 343, textureX, textureY); // Box 360
		bodyModel[63] = new ModelRendererTurbo(this, 56, 339, textureX, textureY); // Box 361
		bodyModel[64] = new ModelRendererTurbo(this, 56, 339, textureX, textureY); // Box 362
		bodyModel[65] = new ModelRendererTurbo(this, 56, 339, textureX, textureY); // Box 363
		bodyModel[66] = new ModelRendererTurbo(this, 56, 339, textureX, textureY); // Box 364
		bodyModel[67] = new ModelRendererTurbo(this, 68, 356, textureX, textureY); // Box 365
		bodyModel[68] = new ModelRendererTurbo(this, 68, 366, textureX, textureY); // Box 366
		bodyModel[69] = new ModelRendererTurbo(this, 68, 380, textureX, textureY); // Box 367
		bodyModel[70] = new ModelRendererTurbo(this, 68, 375, textureX, textureY); // Box 368
		bodyModel[71] = new ModelRendererTurbo(this, 68, 375, textureX, textureY); // Box 369
		bodyModel[72] = new ModelRendererTurbo(this, 68, 356, textureX, textureY); // Box 370
		bodyModel[73] = new ModelRendererTurbo(this, 68, 366, textureX, textureY); // Box 371
		bodyModel[74] = new ModelRendererTurbo(this, 68, 380, textureX, textureY); // Box 372
		bodyModel[75] = new ModelRendererTurbo(this, 1, 382, textureX, textureY); // Box 383
		bodyModel[76] = new ModelRendererTurbo(this, 44, 390, textureX, textureY); // Box 397
		bodyModel[77] = new ModelRendererTurbo(this, 44, 382, textureX, textureY); // Box 398
		bodyModel[78] = new ModelRendererTurbo(this, 44, 398, textureX, textureY); // Box 399
		bodyModel[79] = new ModelRendererTurbo(this, 44, 398, textureX, textureY); // Box 401
		bodyModel[80] = new ModelRendererTurbo(this, 44, 398, textureX, textureY); // Box 403
		bodyModel[81] = new ModelRendererTurbo(this, 44, 382, textureX, textureY); // Box 404
		bodyModel[82] = new ModelRendererTurbo(this, 44, 390, textureX, textureY); // Box 405
		bodyModel[83] = new ModelRendererTurbo(this, 44, 398, textureX, textureY); // Box 406
		bodyModel[84] = new ModelRendererTurbo(this, 384, 108, textureX, textureY); // Box 557
		bodyModel[85] = new ModelRendererTurbo(this, 384, 130, textureX, textureY); // Box 558
		bodyModel[86] = new ModelRendererTurbo(this, 384, 12, textureX, textureY); // Box 35
		bodyModel[87] = new ModelRendererTurbo(this, 419, 12, textureX, textureY); // Box 36
		bodyModel[88] = new ModelRendererTurbo(this, 384, 2, textureX, textureY); // Box 39
		bodyModel[89] = new ModelRendererTurbo(this, 384, 12, textureX, textureY); // Box 44
		bodyModel[90] = new ModelRendererTurbo(this, 419, 12, textureX, textureY); // Box 45
		bodyModel[91] = new ModelRendererTurbo(this, 384, 12, textureX, textureY); // Box 54
		bodyModel[92] = new ModelRendererTurbo(this, 419, 12, textureX, textureY); // Box 55
		bodyModel[93] = new ModelRendererTurbo(this, 419, 12, textureX, textureY); // Box 56
		bodyModel[94] = new ModelRendererTurbo(this, 384, 12, textureX, textureY); // Box 57
		bodyModel[95] = new ModelRendererTurbo(this, 384, 12, textureX, textureY); // Box 58
		bodyModel[96] = new ModelRendererTurbo(this, 419, 12, textureX, textureY); // Box 59
		bodyModel[97] = new ModelRendererTurbo(this, 419, 12, textureX, textureY); // Box 60
		bodyModel[98] = new ModelRendererTurbo(this, 384, 12, textureX, textureY); // Box 61
		bodyModel[99] = new ModelRendererTurbo(this, 384, 12, textureX, textureY); // Box 62
		bodyModel[100] = new ModelRendererTurbo(this, 419, 12, textureX, textureY); // Box 63
		bodyModel[101] = new ModelRendererTurbo(this, 419, 12, textureX, textureY); // Box 64
		bodyModel[102] = new ModelRendererTurbo(this, 384, 12, textureX, textureY); // Box 65
		bodyModel[103] = new ModelRendererTurbo(this, 384, 24, textureX, textureY); // Box 66
		bodyModel[104] = new ModelRendererTurbo(this, 407, 25, textureX, textureY); // Box 67
		bodyModel[105] = new ModelRendererTurbo(this, 1, 114, textureX, textureY); // Box 93
		bodyModel[106] = new ModelRendererTurbo(this, 1, 114, textureX, textureY); // Box 94
		bodyModel[107] = new ModelRendererTurbo(this, 1, 114, textureX, textureY); // Box 120
		bodyModel[108] = new ModelRendererTurbo(this, 1, 114, textureX, textureY); // Box 121
		bodyModel[109] = new ModelRendererTurbo(this, 1, 114, textureX, textureY); // Box 147
		bodyModel[110] = new ModelRendererTurbo(this, 1, 114, textureX, textureY); // Box 148
		bodyModel[111] = new ModelRendererTurbo(this, 384, 2, textureX, textureY); // Box 149
		bodyModel[112] = new ModelRendererTurbo(this, 454, 12, textureX, textureY); // Box 150
		bodyModel[113] = new ModelRendererTurbo(this, 419, 15, textureX, textureY); // Box 151
		bodyModel[114] = new ModelRendererTurbo(this, 454, 12, textureX, textureY); // Box 152
		bodyModel[115] = new ModelRendererTurbo(this, 419, 15, textureX, textureY); // Box 153
		bodyModel[116] = new ModelRendererTurbo(this, 419, 15, textureX, textureY); // Box 154
		bodyModel[117] = new ModelRendererTurbo(this, 454, 12, textureX, textureY); // Box 155
		bodyModel[118] = new ModelRendererTurbo(this, 454, 12, textureX, textureY); // Box 156
		bodyModel[119] = new ModelRendererTurbo(this, 419, 15, textureX, textureY); // Box 157
		bodyModel[120] = new ModelRendererTurbo(this, 419, 15, textureX, textureY); // Box 158
		bodyModel[121] = new ModelRendererTurbo(this, 454, 12, textureX, textureY); // Box 159
		bodyModel[122] = new ModelRendererTurbo(this, 454, 12, textureX, textureY); // Box 160
		bodyModel[123] = new ModelRendererTurbo(this, 419, 15, textureX, textureY); // Box 161
		bodyModel[124] = new ModelRendererTurbo(this, 419, 15, textureX, textureY); // Box 162
		bodyModel[125] = new ModelRendererTurbo(this, 454, 12, textureX, textureY); // Box 163
		bodyModel[126] = new ModelRendererTurbo(this, 454, 12, textureX, textureY); // Box 164
		bodyModel[127] = new ModelRendererTurbo(this, 419, 15, textureX, textureY); // Box 165
		bodyModel[128] = new ModelRendererTurbo(this, 443, 25, textureX, textureY); // Box 166
		bodyModel[129] = new ModelRendererTurbo(this, 420, 24, textureX, textureY); // Box 167

		bodyModel[0].addShapeBox(0F, 0F, 0F, 64, 6, 38, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 15F, 0F, 0F, 15F, 0F, 0F, 15F, 0F, 0F, 15F); // Box 10
		bodyModel[0].setRotationPoint(-27F, -26F, -19F);

		bodyModel[1].addShapeBox(0F, 0F, 0F, 23, 6, 15, 0F,0F, -5.9F, 0F, 0F, -5.9F, 0F, -6F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 11
		bodyModel[1].setRotationPoint(37F, -26F, -34F);

		bodyModel[2].addShapeBox(0F, 0F, 0F, 23, 6, 12, 0F,0F, 0F, 0F, -6F, -3F, 0F, -6F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 12
		bodyModel[2].setRotationPoint(37F, -26F, -19F);

		bodyModel[3].addShapeBox(0F, 0F, 0F, 23, 6, 12, 0F,0F, 0F, 0F, -6F, -3F, 0F, -6F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 13
		bodyModel[3].setRotationPoint(37F, -26F, 7F);

		bodyModel[4].addShapeBox(0F, 0F, 0F, 23, 6, 15, 0F,0F, 0F, 0F, -6F, -3F, 0F, 0F, -5.9F, 0F, 0F, -5.9F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 14
		bodyModel[4].setRotationPoint(37F, -26F, 19F);

		bodyModel[5].addBox(0F, 0F, 0F, 114, 17, 42, 0F); // Box 15
		bodyModel[5].setRotationPoint(-59F, -20F, -21F);

		bodyModel[6].addBox(0F, 0F, 0F, 94, 5, 16, 0F); // Box 16
		bodyModel[6].setRotationPoint(-34F, -20F, -34F);

		bodyModel[7].addBox(0F, 0F, 0F, 94, 5, 16, 0F); // Box 17
		bodyModel[7].setRotationPoint(-34F, -20F, 18F);

		bodyModel[8].addShapeBox(0F, 0F, 0F, 8, 17, 36, 0F,0F, 0F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -10F, 0F, 0F, -10F, 0F, 0F, 0F, 0F); // Box 18
		bodyModel[8].setRotationPoint(60F, -20F, -18F);

		bodyModel[9].addShapeBox(0F, 0F, 0F, 5, 17, 36, 0F,0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F); // Box 19
		bodyModel[9].setRotationPoint(55F, -20F, -18F);

		bodyModel[10].addShapeBox(0F, 0F, 0F, 2, 9, 16, 0F,0F, 0F, 0F, 0F, -2F, -2F, 0F, -2F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F); // Box 21
		bodyModel[10].setRotationPoint(68F, -18F, 18F);

		bodyModel[11].addShapeBox(0F, 0F, 0F, 2, 9, 16, 0F,0F, 0F, 0F, 0F, -2F, -2F, 0F, -2F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F); // Box 24
		bodyModel[11].setRotationPoint(68F, -18F, -34F);

		bodyModel[12].addShapeBox(0F, 0F, 0F, 4, 17, 42, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F); // Box 68
		bodyModel[12].setRotationPoint(-63F, -20F, -21F);

		bodyModel[13].addShapeBox(0F, 0F, 0F, 7, 6, 38, 0F,0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 0F, 15F, 0F, 0F, 15F, 0F, 0F, 15F, 0F, 0F, 15F); // Box 69
		bodyModel[13].setRotationPoint(-34F, -26F, -19F);

		bodyModel[14].addShapeBox(0F, 0F, 0F, 27, 6, 56, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 6F, 0F, 0F, 6F, 0F, 0F, 6F, 0F, 0F, 6F); // Box 70
		bodyModel[14].setRotationPoint(-68F, -29F, -28F);

		bodyModel[15].addBox(0F, 0F, 0F, 34, 8, 68, 0F); // Box 71
		bodyModel[15].setRotationPoint(-68F, -23F, -34F);

		bodyModel[16].addShapeBox(0F, 0F, 0F, 7, 6, 56, 0F,0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 6F, 0F, 0F, 6F, 0F, 0F, 6F, 0F, 0F, 6F); // Box 0
		bodyModel[16].setRotationPoint(-41F, -29F, -28F);

		bodyModel[17].addShapeBox(0F, 0F, 0F, 4, 9, 16, 0F,0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -4F, 0F); // Box 1
		bodyModel[17].setRotationPoint(60F, -20F, -34F);

		bodyModel[18].addShapeBox(0F, 0F, 0F, 3, 12, 56, 0F,0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F); // Box 2
		bodyModel[18].setRotationPoint(-71F, -29F, -28F);

		bodyModel[19].addBox(0F, 0F, 0F, 2, 12, 15, 0F); // Box 3
		bodyModel[19].setRotationPoint(-67.5F, -15F, -33F);

		bodyModel[20].addShapeBox(0F, 0F, 0F, 2, 12, 15, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F); // Box 4
		bodyModel[20].setRotationPoint(-65.5F, -15F, -33F);

		bodyModel[21].addBox(0F, 0F, 0F, 2, 12, 15, 0F); // Box 5
		bodyModel[21].setRotationPoint(-67.5F, -15F, 18F);

		bodyModel[22].addShapeBox(0F, 0F, 0F, 2, 12, 15, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F); // Box 6
		bodyModel[22].setRotationPoint(-65.5F, -15F, 18F);

		bodyModel[23].addShapeBox(0F, 0F, 0F, 6, 6, 15, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -5F, 0F, 0F, -5F, 0F, 0F, 0F, 0F); // Box 7
		bodyModel[23].setRotationPoint(-63.5F, -16F, 18F);

		bodyModel[24].addShapeBox(0F, 0F, 0F, 5, 3, 46, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, 4F, -1.5F, -0.5F, 0F, -1.5F, -0.5F, 0F, -1.5F, -0.5F, 4F, -1.5F, -0.5F, 4F); // Box 120
		bodyModel[24].setRotationPoint(46F, -2F, -25F);

		bodyModel[25].addShapeBox(0F, 0F, 0F, 5, 3, 46, 0F,-1.5F, -0.5F, 0F, -1.5F, -0.5F, 0F, -1.5F, -0.5F, 4F, -1.5F, -0.5F, 4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, 4F); // Box 121
		bodyModel[25].setRotationPoint(46F, -5F, -25F);

		bodyModel[26].addShapeBox(0F, 0F, 0F, 4, 9, 16, 0F,0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 381
		bodyModel[26].setRotationPoint(64F, -18F, -34F);

		bodyModel[27].addShapeBox(0F, 0F, 0F, 4, 9, 16, 0F,0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -4F, 0F); // Box 382
		bodyModel[27].setRotationPoint(60F, -20F, 18F);

		bodyModel[28].addShapeBox(0F, 0F, 0F, 4, 9, 16, 0F,0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 383
		bodyModel[28].setRotationPoint(64F, -18F, 18F);

		bodyModel[29].addShapeBox(0F, 0F, 0F, 6, 6, 15, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -5F, 0F, 0F, -5F, 0F, 0F, 0F, 0F); // Box 326
		bodyModel[29].setRotationPoint(-63.5F, -16F, -33F);

		bodyModel[30].addShapeBox(0F, 0F, 0F, 9, 3, 17, 0F,-3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 327
		bodyModel[30].setRotationPoint(-80F, -29F, -24F);

		bodyModel[31].addShapeBox(0F, 0F, 0F, 9, 3, 17, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F); // Box 328
		bodyModel[31].setRotationPoint(-80F, -23F, -24F);

		bodyModel[32].addBox(0F, 0F, 0F, 9, 3, 17, 0F); // Box 329
		bodyModel[32].setRotationPoint(-80F, -26F, -24F);

		bodyModel[33].addShapeBox(0F, 0F, 0F, 9, 3, 17, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F); // Box 330
		bodyModel[33].setRotationPoint(-80F, -23F, 7F);

		bodyModel[34].addBox(0F, 0F, 0F, 9, 3, 17, 0F); // Box 331
		bodyModel[34].setRotationPoint(-80F, -26F, 7F);

		bodyModel[35].addShapeBox(0F, 0F, 0F, 9, 3, 17, 0F,-3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 332
		bodyModel[35].setRotationPoint(-80F, -29F, 7F);

		bodyModel[36].addShapeBox(0F, 0F, 0F, 4, 4, 4, 0F,0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 333
		bodyModel[36].setRotationPoint(-73F, -30F, 8F);

		bodyModel[37].addShapeBox(0F, 0F, 0F, 4, 4, 4, 0F,0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 334
		bodyModel[37].setRotationPoint(-73F, -30F, 19F);

		bodyModel[38].addShapeBox(0F, 0F, 0F, 4, 4, 4, 0F,0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 335
		bodyModel[38].setRotationPoint(-73F, -30F, -23F);

		bodyModel[39].addShapeBox(0F, 0F, 0F, 4, 4, 4, 0F,0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 336
		bodyModel[39].setRotationPoint(-73F, -30F, -12F);

		bodyModel[40].addBox(0F, 0F, 0F, 7, 1, 17, 0F); // Box 337
		bodyModel[40].setRotationPoint(-69F, -30F, 8F);

		bodyModel[41].addBox(0F, 0F, 0F, 7, 1, 17, 0F); // Box 338
		bodyModel[41].setRotationPoint(-69F, -30F, -25F);

		bodyModel[42].addShapeBox(0F, 0F, 0F, 2, 1, 10, 0F,-0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 339
		bodyModel[42].setRotationPoint(-62F, -31F, -5F);

		bodyModel[43].addBox(0F, 0F, 0F, 2, 1, 10, 0F); // Box 340
		bodyModel[43].setRotationPoint(-62F, -30F, -5F);

		bodyModel[44].addShapeBox(0F, 0F, 0F, 2, 2, 4, 0F,0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 341
		bodyModel[44].setRotationPoint(-66F, -30.5F, -2F);

		bodyModel[45].addShapeBox(0F, 0F, 0F, 2, 2, 4, 0F,0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 342
		bodyModel[45].setRotationPoint(-64F, -30.5F, -2F);

		bodyModel[46].addBox(0F, 0F, 0F, 2, 1, 5, 0F); // Box 343
		bodyModel[46].setRotationPoint(-64F, -30F, -2.5F);

		bodyModel[47].addBox(0F, 0F, 0F, 7, 1, 10, 0F); // Box 344
		bodyModel[47].setRotationPoint(-69F, -29.5F, -5F);

		bodyModel[48].addBox(0F, 0F, 0F, 19, 1, 6, 0F); // Box 345
		bodyModel[48].setRotationPoint(-24F, -22.5F, -28.5F);
		bodyModel[48].rotateAngleX = 0.38397244F;

		bodyModel[49].addBox(0F, 0F, 0F, 19, 1, 6, 0F); // Box 346
		bodyModel[49].setRotationPoint(16F, -22.5F, -28.5F);
		bodyModel[49].rotateAngleX = 0.38397244F;

		bodyModel[50].addBox(0F, 0F, 0F, 19, 1, 6, 0F); // Box 347
		bodyModel[50].setRotationPoint(-4F, -22.5F, -28.5F);
		bodyModel[50].rotateAngleX = 0.38397244F;

		bodyModel[51].addBox(0F, 0F, 0F, 19, 1, 6, 0F); // Box 348
		bodyModel[51].setRotationPoint(-4F, -25F, 22.5F);
		bodyModel[51].rotateAngleX = -0.38397244F;

		bodyModel[52].addBox(0F, 0F, 0F, 19, 1, 6, 0F); // Box 349
		bodyModel[52].setRotationPoint(-24F, -25F, 22.5F);
		bodyModel[52].rotateAngleX = -0.38397244F;

		bodyModel[53].addBox(0F, 0F, 0F, 19, 1, 6, 0F); // Box 350
		bodyModel[53].setRotationPoint(16F, -25F, 22.5F);
		bodyModel[53].rotateAngleX = -0.38397244F;

		bodyModel[54].addBox(0F, 0F, 0F, 26, 7, 7, 0F); // Box 351
		bodyModel[54].setRotationPoint(-67F, -28F, 28F);

		bodyModel[55].addShapeBox(0F, 0F, 0F, 26, 7, 3, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, -5F, 0F, 0F, -5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 352
		bodyModel[55].setRotationPoint(-67F, -28F, 35F);

		bodyModel[56].addShapeBox(0F, 0F, 0F, 26, 7, 3, 0F,0F, -5F, 0F, 0F, -5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 353
		bodyModel[56].setRotationPoint(-67F, -28F, -38F);

		bodyModel[57].addBox(0F, 0F, 0F, 26, 7, 7, 0F); // Box 354
		bodyModel[57].setRotationPoint(-67F, -28F, -35F);

		bodyModel[58].addBox(0F, 0F, 0F, 12, 1, 15, 0F); // Box 356
		bodyModel[58].setRotationPoint(-59F, -30F, -25F);

		bodyModel[59].addBox(0F, 0F, 0F, 12, 1, 15, 0F); // Box 357
		bodyModel[59].setRotationPoint(-59F, -30F, -7.5F);

		bodyModel[60].addBox(0F, 0F, 0F, 12, 1, 15, 0F); // Box 358
		bodyModel[60].setRotationPoint(-59F, -30F, 10F);

		bodyModel[61].addBox(0F, 0F, 0F, 1, 4, 8, 0F); // Box 359
		bodyModel[61].setRotationPoint(-64F, -15F, -17F);

		bodyModel[62].addBox(0F, 0F, 0F, 1, 4, 8, 0F); // Box 360
		bodyModel[62].setRotationPoint(-64F, -15F, 9F);

		bodyModel[63].addBox(0F, 0F, 0F, 1, 10, 6, 0F); // Box 361
		bodyModel[63].setRotationPoint(67F, -12F, -16F);
		bodyModel[63].rotateAngleZ = -0.68067841F;

		bodyModel[64].addBox(0F, 0F, 0F, 1, 10, 6, 0F); // Box 362
		bodyModel[64].setRotationPoint(67F, -12F, -7F);
		bodyModel[64].rotateAngleZ = -0.68067841F;

		bodyModel[65].addBox(0F, 0F, 0F, 1, 10, 6, 0F); // Box 363
		bodyModel[65].setRotationPoint(67F, -12F, 1F);
		bodyModel[65].rotateAngleZ = -0.68067841F;

		bodyModel[66].addBox(0F, 0F, 0F, 1, 10, 6, 0F); // Box 364
		bodyModel[66].setRotationPoint(67F, -12F, 10F);
		bodyModel[66].rotateAngleZ = -0.68067841F;

		bodyModel[67].addBox(0F, 0F, 0F, 3, 2, 7, 0F); // Box 365
		bodyModel[67].setRotationPoint(59F, -21F, 19F);

		bodyModel[68].addShapeBox(0F, 0F, 0F, 3, 1, 7, 0F,-1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 366
		bodyModel[68].setRotationPoint(59F, -22F, 19F);

		bodyModel[69].addShapeBox(0F, 0F, 0F, 4, 1, 1, 0F,-1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 367
		bodyModel[69].setRotationPoint(58.5F, -22.5F, 24F);

		bodyModel[70].addBox(0F, 0F, 0F, 4, 3, 1, 0F); // Box 368
		bodyModel[70].setRotationPoint(58.5F, -21.5F, 24F);

		bodyModel[71].addBox(0F, 0F, 0F, 4, 3, 1, 0F); // Box 369
		bodyModel[71].setRotationPoint(58.5F, -21.5F, -25F);

		bodyModel[72].addBox(0F, 0F, 0F, 3, 2, 7, 0F); // Box 370
		bodyModel[72].setRotationPoint(59F, -21F, -26F);

		bodyModel[73].addShapeBox(0F, 0F, 0F, 3, 1, 7, 0F,-1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 371
		bodyModel[73].setRotationPoint(59F, -22F, -26F);

		bodyModel[74].addShapeBox(0F, 0F, 0F, 4, 1, 1, 0F,-1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 372
		bodyModel[74].setRotationPoint(58.5F, -22.5F, -25F);

		bodyModel[75].addBox(0F, 0F, 0F, 3, 2, 36, 0F); // Box 383
		bodyModel[75].setRotationPoint(63F, -19F, -18F);

		bodyModel[76].addShapeBox(0F, 0F, 0F, 1, 2, 5, 0F,0F, -1F, -1.5F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, -1F, -1.5F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 397
		bodyModel[76].setRotationPoint(63F, -21F, 7F);

		bodyModel[77].addShapeBox(0F, 0F, 0F, 2, 2, 5, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 398
		bodyModel[77].setRotationPoint(64F, -21F, 7F);

		bodyModel[78].addShapeBox(0F, 0F, 0F, 1, 2, 4, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 399
		bodyModel[78].setRotationPoint(65.5F, -20.5F, 7.5F);

		bodyModel[79].addShapeBox(0F, 0F, 0F, 1, 2, 4, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 401
		bodyModel[79].setRotationPoint(65.5F, -18.5F, 7.5F);

		bodyModel[80].addShapeBox(0F, 0F, 0F, 1, 2, 4, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 403
		bodyModel[80].setRotationPoint(65.5F, -20.5F, -11.5F);

		bodyModel[81].addShapeBox(0F, 0F, 0F, 2, 2, 5, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 404
		bodyModel[81].setRotationPoint(64F, -21F, -12F);

		bodyModel[82].addShapeBox(0F, 0F, 0F, 1, 2, 5, 0F,0F, -1F, -1.5F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, -1F, -1.5F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 405
		bodyModel[82].setRotationPoint(63F, -21F, -12F);

		bodyModel[83].addShapeBox(0F, 0F, 0F, 1, 2, 4, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 406
		bodyModel[83].setRotationPoint(65.5F, -18.5F, -11.5F);

		bodyModel[84].addShapeBox(0F, 0F, 0F, 100, 20, 1, 0F,0F, 0F, 0F, -50F, 0F, 0F, -50F, 0F, 0F, 0F, 0F, 0F, 0F, -10F, 0F, -50F, -10F, 0F, -50F, -10F, 0F, 0F, -10F, 0F); // Box 557
		bodyModel[84].setRotationPoint(-29F, -20.5F, -38.3F);

		bodyModel[85].addShapeBox(0F, 0F, 0F, 100, 20, 1, 0F,0F, 0F, 0F, -50F, 0F, 0F, -50F, 0F, 0F, 0F, 0F, 0F, 0F, -10F, 0F, -50F, -10F, 0F, -50F, -10F, 0F, 0F, -10F, 0F); // Box 558
		bodyModel[85].setRotationPoint(-29F, -20.5F, 37.3F);

		bodyModel[86].addBox(0F, 0F, 0F, 16, 10, 1, 0F); // Box 35
		bodyModel[86].setRotationPoint(-68F, -21F, 37F);

		bodyModel[87].addShapeBox(0F, 0F, 0F, 16, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, 0F, 0F); // Box 36
		bodyModel[87].setRotationPoint(-68F, -11F, 37F);

		bodyModel[88].addBox(0F, 0F, 0F, 128, 6, 3, 0F); // Box 39
		bodyModel[88].setRotationPoint(-68F, -21F, 34F);

		bodyModel[89].addBox(0F, 0F, 0F, 16, 10, 1, 0F); // Box 44
		bodyModel[89].setRotationPoint(-52F, -21F, 37F);

		bodyModel[90].addShapeBox(0F, 0F, 0F, 16, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F); // Box 45
		bodyModel[90].setRotationPoint(-52F, -11F, 37F);

		bodyModel[91].addBox(0F, 0F, 0F, 16, 10, 1, 0F); // Box 54
		bodyModel[91].setRotationPoint(-20F, -21F, 37F);

		bodyModel[92].addShapeBox(0F, 0F, 0F, 16, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F); // Box 55
		bodyModel[92].setRotationPoint(-20F, -11F, 37F);

		bodyModel[93].addShapeBox(0F, 0F, 0F, 16, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, 0F, 0F); // Box 56
		bodyModel[93].setRotationPoint(-36F, -11F, 37F);

		bodyModel[94].addBox(0F, 0F, 0F, 16, 10, 1, 0F); // Box 57
		bodyModel[94].setRotationPoint(-36F, -21F, 37F);

		bodyModel[95].addBox(0F, 0F, 0F, 16, 10, 1, 0F); // Box 58
		bodyModel[95].setRotationPoint(12F, -21F, 37F);

		bodyModel[96].addShapeBox(0F, 0F, 0F, 16, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F); // Box 59
		bodyModel[96].setRotationPoint(12F, -11F, 37F);

		bodyModel[97].addShapeBox(0F, 0F, 0F, 16, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, 0F, 0F); // Box 60
		bodyModel[97].setRotationPoint(-4F, -11F, 37F);

		bodyModel[98].addBox(0F, 0F, 0F, 16, 10, 1, 0F); // Box 61
		bodyModel[98].setRotationPoint(-4F, -21F, 37F);

		bodyModel[99].addBox(0F, 0F, 0F, 16, 10, 1, 0F); // Box 62
		bodyModel[99].setRotationPoint(44F, -21F, 37F);

		bodyModel[100].addShapeBox(0F, 0F, 0F, 16, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F); // Box 63
		bodyModel[100].setRotationPoint(44F, -11F, 37F);

		bodyModel[101].addShapeBox(0F, 0F, 0F, 16, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, 0F, 0F); // Box 64
		bodyModel[101].setRotationPoint(28F, -11F, 37F);

		bodyModel[102].addBox(0F, 0F, 0F, 16, 10, 1, 0F); // Box 65
		bodyModel[102].setRotationPoint(28F, -21F, 37F);

		bodyModel[103].addShapeBox(0F, 0F, 0F, 8, 11, 3, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, -2F, 0F, 0F, 0F); // Box 66
		bodyModel[103].setRotationPoint(60F, -21F, 34F);

		bodyModel[104].addShapeBox(0F, 0F, 0F, 4, 11, 2, 0F,0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -2F, 0F, 0F, -2F, -1F, 0F, 0F, 0F); // Box 67
		bodyModel[104].setRotationPoint(60F, -21F, 36F);

		bodyModel[105].addShapeBox(0F, 0F, 0F, 5, 3, 46, 0F,-1.5F, -0.5F, 0F, -1.5F, -0.5F, 0F, -1.5F, -0.5F, 4F, -1.5F, -0.5F, 4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, 4F); // Box 93
		bodyModel[105].setRotationPoint(14F, -5F, -25F);

		bodyModel[106].addShapeBox(0F, 0F, 0F, 5, 3, 46, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, 4F, -1.5F, -0.5F, 0F, -1.5F, -0.5F, 0F, -1.5F, -0.5F, 4F, -1.5F, -0.5F, 4F); // Box 94
		bodyModel[106].setRotationPoint(14F, -2F, -25F);

		bodyModel[107].addShapeBox(0F, 0F, 0F, 5, 3, 46, 0F,-1.5F, -0.5F, 0F, -1.5F, -0.5F, 0F, -1.5F, -0.5F, 4F, -1.5F, -0.5F, 4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, 4F); // Box 120
		bodyModel[107].setRotationPoint(-19F, -5F, -25F);

		bodyModel[108].addShapeBox(0F, 0F, 0F, 5, 3, 46, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, 4F, -1.5F, -0.5F, 0F, -1.5F, -0.5F, 0F, -1.5F, -0.5F, 4F, -1.5F, -0.5F, 4F); // Box 121
		bodyModel[108].setRotationPoint(-19F, -2F, -25F);

		bodyModel[109].addShapeBox(0F, 0F, 0F, 5, 3, 46, 0F,-1.5F, -0.5F, 0F, -1.5F, -0.5F, 0F, -1.5F, -0.5F, 4F, -1.5F, -0.5F, 4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, 4F); // Box 147
		bodyModel[109].setRotationPoint(-51F, -5F, -25F);

		bodyModel[110].addShapeBox(0F, 0F, 0F, 5, 3, 46, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, 4F, -1.5F, -0.5F, 0F, -1.5F, -0.5F, 0F, -1.5F, -0.5F, 4F, -1.5F, -0.5F, 4F); // Box 148
		bodyModel[110].setRotationPoint(-51F, -2F, -25F);

		bodyModel[111].addBox(0F, 0F, 0F, 128, 6, 3, 0F); // Box 149
		bodyModel[111].setRotationPoint(-68F, -21F, -37F);

		bodyModel[112].addBox(0F, 0F, 0F, 16, 10, 1, 0F); // Box 150
		bodyModel[112].setRotationPoint(-68F, -21F, -38F);

		bodyModel[113].addShapeBox(0F, 0F, 0F, 16, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, 0F, 0F); // Box 151
		bodyModel[113].setRotationPoint(-68F, -11F, -38F);

		bodyModel[114].addBox(0F, 0F, 0F, 16, 10, 1, 0F); // Box 152
		bodyModel[114].setRotationPoint(-52F, -21F, -38F);

		bodyModel[115].addShapeBox(0F, 0F, 0F, 16, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F); // Box 153
		bodyModel[115].setRotationPoint(-52F, -11F, -38F);

		bodyModel[116].addShapeBox(0F, 0F, 0F, 16, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F); // Box 154
		bodyModel[116].setRotationPoint(-20F, -11F, -38F);

		bodyModel[117].addBox(0F, 0F, 0F, 16, 10, 1, 0F); // Box 155
		bodyModel[117].setRotationPoint(-36F, -21F, -38F);

		bodyModel[118].addBox(0F, 0F, 0F, 16, 10, 1, 0F); // Box 156
		bodyModel[118].setRotationPoint(-20F, -21F, -38F);

		bodyModel[119].addShapeBox(0F, 0F, 0F, 16, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, 0F, 0F); // Box 157
		bodyModel[119].setRotationPoint(-36F, -11F, -38F);

		bodyModel[120].addShapeBox(0F, 0F, 0F, 16, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F); // Box 158
		bodyModel[120].setRotationPoint(12F, -11F, -38F);

		bodyModel[121].addBox(0F, 0F, 0F, 16, 10, 1, 0F); // Box 159
		bodyModel[121].setRotationPoint(-4F, -21F, -38F);

		bodyModel[122].addBox(0F, 0F, 0F, 16, 10, 1, 0F); // Box 160
		bodyModel[122].setRotationPoint(12F, -21F, -38F);

		bodyModel[123].addShapeBox(0F, 0F, 0F, 16, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, 0F, 0F); // Box 161
		bodyModel[123].setRotationPoint(-4F, -11F, -38F);

		bodyModel[124].addShapeBox(0F, 0F, 0F, 16, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F); // Box 162
		bodyModel[124].setRotationPoint(44F, -11F, -38F);

		bodyModel[125].addBox(0F, 0F, 0F, 16, 10, 1, 0F); // Box 163
		bodyModel[125].setRotationPoint(28F, -21F, -38F);

		bodyModel[126].addBox(0F, 0F, 0F, 16, 10, 1, 0F); // Box 164
		bodyModel[126].setRotationPoint(44F, -21F, -38F);

		bodyModel[127].addShapeBox(0F, 0F, 0F, 16, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, 0F, 0F); // Box 165
		bodyModel[127].setRotationPoint(28F, -11F, -38F);

		bodyModel[128].addShapeBox(0F, 0F, 0F, 4, 11, 2, 0F,0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, -2F, -1F, 0F, -2F, 0F, 0F, 0F, -1F); // Box 166
		bodyModel[128].setRotationPoint(60F, -21F, -38F);

		bodyModel[129].addShapeBox(0F, 0F, 0F, 8, 11, 3, 0F,0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, -2F, 0F, -2F, 0F, 0F, 0F, 0F); // Box 167
		bodyModel[129].setRotationPoint(60F, -21F, -37F);
	}

	private void initturretModel_1()
	{
		turretModel[0] = new ModelRendererTurbo(this, 112, 1, textureX, textureY); // Box 1
		turretModel[1] = new ModelRendererTurbo(this, 112, 31, textureX, textureY); // Box 2
		turretModel[2] = new ModelRendererTurbo(this, 112, 170, textureX, textureY); // Box 0
		turretModel[3] = new ModelRendererTurbo(this, 112, 105, textureX, textureY); // Box 1
		turretModel[4] = new ModelRendererTurbo(this, 283, 108, textureX, textureY); // Box 2
		turretModel[5] = new ModelRendererTurbo(this, 239, 170, textureX, textureY); // Box 4
		turretModel[6] = new ModelRendererTurbo(this, 330, 400, textureX, textureY); // Box 5
		turretModel[7] = new ModelRendererTurbo(this, 1, 234, textureX, textureY); // Box 6
		turretModel[8] = new ModelRendererTurbo(this, 203, 400, textureX, textureY); // Box 7
		turretModel[9] = new ModelRendererTurbo(this, 60, 114, textureX, textureY); // Box 8
		turretModel[10] = new ModelRendererTurbo(this, 60, 135, textureX, textureY); // Box 9
		turretModel[11] = new ModelRendererTurbo(this, 60, 123, textureX, textureY); // Box 10
		turretModel[12] = new ModelRendererTurbo(this, 60, 129, textureX, textureY); // Box 11
		turretModel[13] = new ModelRendererTurbo(this, 60, 123, textureX, textureY); // Box 12
		turretModel[14] = new ModelRendererTurbo(this, 60, 129, textureX, textureY); // Box 13
		turretModel[15] = new ModelRendererTurbo(this, 60, 148, textureX, textureY); // Box 14
		turretModel[16] = new ModelRendererTurbo(this, 60, 143, textureX, textureY); // Box 15
		turretModel[17] = new ModelRendererTurbo(this, 60, 143, textureX, textureY); // Box 16
		turretModel[18] = new ModelRendererTurbo(this, 60, 148, textureX, textureY); // Box 17
		turretModel[19] = new ModelRendererTurbo(this, 79, 202, textureX, textureY); // Box 18
		turretModel[20] = new ModelRendererTurbo(this, 79, 202, textureX, textureY); // Box 19
		turretModel[21] = new ModelRendererTurbo(this, 79, 202, textureX, textureY); // Box 20
		turretModel[22] = new ModelRendererTurbo(this, 79, 202, textureX, textureY); // Box 21
		turretModel[23] = new ModelRendererTurbo(this, 79, 190, textureX, textureY); // Box 22
		turretModel[24] = new ModelRendererTurbo(this, 92, 201, textureX, textureY); // Box 23
		turretModel[25] = new ModelRendererTurbo(this, 1, 123, textureX, textureY); // Box 25
		turretModel[26] = new ModelRendererTurbo(this, 24, 123, textureX, textureY); // Box 26
		turretModel[27] = new ModelRendererTurbo(this, 79, 173, textureX, textureY); // Box 27
		turretModel[28] = new ModelRendererTurbo(this, 1, 114, textureX, textureY); // Box 28
		turretModel[29] = new ModelRendererTurbo(this, 102, 190, textureX, textureY); // Box 29
		turretModel[30] = new ModelRendererTurbo(this, 1, 144, textureX, textureY); // Box 30
		turretModel[31] = new ModelRendererTurbo(this, 1, 128, textureX, textureY); // Box 31
		turretModel[32] = new ModelRendererTurbo(this, 1, 82, textureX, textureY); // Box 0
		turretModel[33] = new ModelRendererTurbo(this, 1, 96, textureX, textureY); // Box 5
		turretModel[34] = new ModelRendererTurbo(this, 1, 104, textureX, textureY); // Box 7
		turretModel[35] = new ModelRendererTurbo(this, 1, 96, textureX, textureY); // Box 8
		turretModel[36] = new ModelRendererTurbo(this, 1, 104, textureX, textureY); // Box 9
		turretModel[37] = new ModelRendererTurbo(this, 79, 212, textureX, textureY); // Box 373
		turretModel[38] = new ModelRendererTurbo(this, 79, 212, textureX, textureY); // Box 374
		turretModel[39] = new ModelRendererTurbo(this, 79, 212, textureX, textureY); // Box 375
		turretModel[40] = new ModelRendererTurbo(this, 79, 212, textureX, textureY); // Box 376
		turretModel[41] = new ModelRendererTurbo(this, 79, 212, textureX, textureY); // Box 377
		turretModel[42] = new ModelRendererTurbo(this, 79, 212, textureX, textureY); // Box 378
		turretModel[43] = new ModelRendererTurbo(this, 79, 212, textureX, textureY); // Box 379
		turretModel[44] = new ModelRendererTurbo(this, 79, 212, textureX, textureY); // Box 380
		turretModel[45] = new ModelRendererTurbo(this, 79, 212, textureX, textureY); // Box 381
		turretModel[46] = new ModelRendererTurbo(this, 79, 212, textureX, textureY); // Box 382
		turretModel[47] = new ModelRendererTurbo(this, 1, 389, textureX, textureY); // Box 384
		turretModel[48] = new ModelRendererTurbo(this, 1, 382, textureX, textureY); // Box 385
		turretModel[49] = new ModelRendererTurbo(this, 427, 86, textureX, textureY); // decal
		turretModel[50] = new ModelRendererTurbo(this, 384, 86, textureX, textureY); // Box 408
		turretModel[51] = new ModelRendererTurbo(this, 60, 129, textureX, textureY); // Box 409
		turretModel[52] = new ModelRendererTurbo(this, 60, 123, textureX, textureY); // Box 410
		turretModel[53] = new ModelRendererTurbo(this, 60, 114, textureX, textureY); // Box 411
		turretModel[54] = new ModelRendererTurbo(this, 60, 123, textureX, textureY); // Box 412
		turretModel[55] = new ModelRendererTurbo(this, 60, 129, textureX, textureY); // Box 413
		turretModel[56] = new ModelRendererTurbo(this, 60, 135, textureX, textureY); // Box 30
		turretModel[57] = new ModelRendererTurbo(this, 60, 135, textureX, textureY); // Box 31
		turretModel[58] = new ModelRendererTurbo(this, 60, 135, textureX, textureY); // Box 32
		turretModel[59] = new ModelRendererTurbo(this, 60, 135, textureX, textureY); // Box 33
		turretModel[60] = new ModelRendererTurbo(this, 60, 135, textureX, textureY); // Box 34
		turretModel[61] = new ModelRendererTurbo(this, 60, 135, textureX, textureY); // Box 35
		turretModel[62] = new ModelRendererTurbo(this, 117, 116, textureX, textureY); // Box 36
		turretModel[63] = new ModelRendererTurbo(this, 117, 116, textureX, textureY); // Box 37
		turretModel[64] = new ModelRendererTurbo(this, 117, 105, textureX, textureY); // Box 38
		turretModel[65] = new ModelRendererTurbo(this, 112, 105, textureX, textureY); // Box 39
		turretModel[66] = new ModelRendererTurbo(this, 124, 116, textureX, textureY); // Box 40
		turretModel[67] = new ModelRendererTurbo(this, 94, 212, textureX, textureY); // Box 41
		turretModel[68] = new ModelRendererTurbo(this, 94, 212, textureX, textureY); // Box 42
		turretModel[69] = new ModelRendererTurbo(this, 94, 212, textureX, textureY); // Box 43
		turretModel[70] = new ModelRendererTurbo(this, 94, 212, textureX, textureY); // Box 44
		turretModel[71] = new ModelRendererTurbo(this, 94, 212, textureX, textureY); // Box 45
		turretModel[72] = new ModelRendererTurbo(this, 285, 32, textureX, textureY); // Box 559
		turretModel[73] = new ModelRendererTurbo(this, 285, 32, textureX, textureY); // Box 560
		turretModel[74] = new ModelRendererTurbo(this, 348, 45, textureX, textureY); // Box 561
		turretModel[75] = new ModelRendererTurbo(this, 346, 64, textureX, textureY); // Box 563
		turretModel[76] = new ModelRendererTurbo(this, 346, 64, textureX, textureY); // Box 564
		turretModel[77] = new ModelRendererTurbo(this, 317, 64, textureX, textureY); // Box 565
		turretModel[78] = new ModelRendererTurbo(this, 346, 87, textureX, textureY); // Box 566
		turretModel[79] = new ModelRendererTurbo(this, 317, 87, textureX, textureY); // Box 568
		turretModel[80] = new ModelRendererTurbo(this, 346, 87, textureX, textureY); // Box 569
		turretModel[81] = new ModelRendererTurbo(this, 312, 120, textureX, textureY); // Box 570
		turretModel[82] = new ModelRendererTurbo(this, 308, 105, textureX, textureY); // Box 571
		turretModel[83] = new ModelRendererTurbo(this, 312, 120, textureX, textureY); // Box 572
		turretModel[84] = new ModelRendererTurbo(this, 80, 32, textureX, textureY); // Box 0
		turretModel[85] = new ModelRendererTurbo(this, 80, 27, textureX, textureY); // Box 1
		turretModel[86] = new ModelRendererTurbo(this, 80, 22, textureX, textureY); // Box 2
		turretModel[87] = new ModelRendererTurbo(this, 80, 13, textureX, textureY); // Box 3
		turretModel[88] = new ModelRendererTurbo(this, 80, 13, textureX, textureY); // Box 4
		turretModel[89] = new ModelRendererTurbo(this, 80, 1, textureX, textureY); // Box 5
		turretModel[90] = new ModelRendererTurbo(this, 105, 1, textureX, textureY); // Box 6
		turretModel[91] = new ModelRendererTurbo(this, 80, 17, textureX, textureY); // Box 7
		turretModel[92] = new ModelRendererTurbo(this, 97, 17, textureX, textureY); // Box 8
		turretModel[93] = new ModelRendererTurbo(this, 80, 17, textureX, textureY); // Box 9
		turretModel[94] = new ModelRendererTurbo(this, 98, 5, textureX, textureY); // Box 10
		turretModel[95] = new ModelRendererTurbo(this, 105, 5, textureX, textureY); // Box 11
		turretModel[96] = new ModelRendererTurbo(this, 98, 1, textureX, textureY); // Box 12
		turretModel[97] = new ModelRendererTurbo(this, 80, 32, textureX, textureY); // Box 13
		turretModel[98] = new ModelRendererTurbo(this, 80, 1, textureX, textureY); // Box 14
		turretModel[99] = new ModelRendererTurbo(this, 80, 27, textureX, textureY); // Box 15
		turretModel[100] = new ModelRendererTurbo(this, 98, 5, textureX, textureY); // Box 16
		turretModel[101] = new ModelRendererTurbo(this, 105, 5, textureX, textureY); // Box 17
		turretModel[102] = new ModelRendererTurbo(this, 105, 1, textureX, textureY); // Box 18
		turretModel[103] = new ModelRendererTurbo(this, 80, 13, textureX, textureY); // Box 19
		turretModel[104] = new ModelRendererTurbo(this, 80, 13, textureX, textureY); // Box 20
		turretModel[105] = new ModelRendererTurbo(this, 80, 22, textureX, textureY); // Box 21
		turretModel[106] = new ModelRendererTurbo(this, 98, 1, textureX, textureY); // Box 22
		turretModel[107] = new ModelRendererTurbo(this, 80, 17, textureX, textureY); // Box 23
		turretModel[108] = new ModelRendererTurbo(this, 97, 17, textureX, textureY); // Box 24
		turretModel[109] = new ModelRendererTurbo(this, 80, 17, textureX, textureY); // Box 25
		turretModel[110] = new ModelRendererTurbo(this, 101, 42, textureX, textureY); // Box 26
		turretModel[111] = new ModelRendererTurbo(this, 109, 29, textureX, textureY); // Box 27
		turretModel[112] = new ModelRendererTurbo(this, 109, 29, textureX, textureY); // Box 28
		turretModel[113] = new ModelRendererTurbo(this, 80, 40, textureX, textureY); // Box 29

		turretModel[0].addShapeBox(28F, -12F, 7F, 12, 6, 18, 0F,0F, 0F, 0F, 0F, 0F, 0F, -11F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 10F, 0F, 0F, 1F, 0F, -1F, 0F, 0F, 1F); // Box 1
		turretModel[0].setRotationPoint(-6F, -26F, 0F);

		turretModel[1].addShapeBox(28F, -12F, -25F, 12, 6, 18, 0F,0F, 0F, 0F, -11F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 1F, 0F, -1F, 10F, 0F, 0F, 0F, 0F, 0F); // Box 2
		turretModel[1].setRotationPoint(-6F, -26F, 0F);

		turretModel[2].addShapeBox(15F, -12F, -25F, 13, 11, 50, 0F,0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 2F); // Box 0
		turretModel[2].setRotationPoint(-6F, -26F, 0F);

		turretModel[3].addShapeBox(-20F, -15F, -25F, 35, 14, 50, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 2F); // Box 1
		turretModel[3].setRotationPoint(-6F, -26F, 0F);

		turretModel[4].addShapeBox(-28F, -15F, -25F, 8, 11, 50, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 3F, 2F, 0F, 3F, 2F, 0F, 0F, 2F); // Box 2
		turretModel[4].setRotationPoint(-6F, -26F, 0F);

		turretModel[5].addShapeBox(-41F, -15F, -25F, 13, 11, 50, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 2F, -2F, 0F, 2F); // Box 4
		turretModel[5].setRotationPoint(-6F, -26F, 0F);

		turretModel[6].addShapeBox(-20F, -18F, -21F, 35, 3, 42, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, 4F); // Box 5
		turretModel[6].setRotationPoint(-6F, -26F, 0F);

		turretModel[7].addShapeBox(15F, -18F, 8F, 5, 3, 13, 0F,0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1.2F, 0F, 0F, 1.2F, 4F, 0F, 0F, 4F); // Box 6
		turretModel[7].setRotationPoint(-6F, -26F, 0F);

		turretModel[8].addShapeBox(-41F, -18F, -21F, 21, 3, 42, 0F,0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, 4F); // Box 7
		turretModel[8].setRotationPoint(-6F, -26F, 0F);

		turretModel[9].addBox(-7F, -20F, -12F, 18, 2, 6, 0F); // Box 8
		turretModel[9].setRotationPoint(-6F, -26F, 0F);

		turretModel[10].addBox(-7F, -22F, -12F, 18, 1, 6, 0F); // Box 9
		turretModel[10].setRotationPoint(-6F, -26F, 0F);

		turretModel[11].addShapeBox(-7F, -20F, -6F, 18, 2, 3, 0F,0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F); // Box 10
		turretModel[11].setRotationPoint(-6F, -26F, 0F);

		turretModel[12].addShapeBox(-5F, -20F, -3F, 14, 2, 3, 0F,0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F); // Box 11
		turretModel[12].setRotationPoint(-6F, -26F, 0F);

		turretModel[13].addShapeBox(-7F, -20F, -15F, 18, 2, 3, 0F,-2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 12
		turretModel[13].setRotationPoint(-6F, -26F, 0F);

		turretModel[14].addShapeBox(-5F, -20F, -18F, 14, 2, 3, 0F,-4F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 13
		turretModel[14].setRotationPoint(-6F, -26F, 0F);

		turretModel[15].addShapeBox(-5F, -22F, -18F, 14, 1, 3, 0F,-4F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 14
		turretModel[15].setRotationPoint(-6F, -26F, 0F);

		turretModel[16].addShapeBox(-7F, -22F, -15F, 18, 1, 3, 0F,-2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 15
		turretModel[16].setRotationPoint(-6F, -26F, 0F);

		turretModel[17].addShapeBox(-7F, -22F, -6F, 18, 1, 3, 0F,0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F); // Box 16
		turretModel[17].setRotationPoint(-6F, -26F, 0F);

		turretModel[18].addShapeBox(-5F, -22F, -3F, 14, 1, 3, 0F,0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F); // Box 17
		turretModel[18].setRotationPoint(-6F, -26F, 0F);

		turretModel[19].addShapeBox(-10F, -23F, -14F, 2, 5, 4, 0F,0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 18
		turretModel[19].setRotationPoint(-6F, -26F, 0F);

		turretModel[20].addShapeBox(-8F, -23F, -14F, 2, 5, 4, 0F,0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 19
		turretModel[20].setRotationPoint(-6F, -26F, 0F);

		turretModel[21].addShapeBox(-10F, -23F, -8F, 2, 5, 4, 0F,0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 20
		turretModel[21].setRotationPoint(-6F, -26F, 0F);

		turretModel[22].addShapeBox(-8F, -23F, -8F, 2, 5, 4, 0F,0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 21
		turretModel[22].setRotationPoint(-6F, -26F, 0F);

		turretModel[23].addBox(6F, -24F, -17F, 6, 5, 5, 0F); // Box 22
		turretModel[23].setRotationPoint(-6F, -26F, 0F);

		turretModel[24].addShapeBox(5F, -24F, -17F, 1, 5, 5, 0F,0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 23
		turretModel[24].setRotationPoint(-6F, -26F, 0F);

		turretModel[25].addBox(5F, -19F, -18F, 8, 1, 3, 0F); // Box 25
		turretModel[25].setRotationPoint(-6F, -26F, 0F);

		turretModel[26].addShapeBox(13F, -19F, -18F, 2, 1, 3, 0F,0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 26
		turretModel[26].setRotationPoint(-6F, -26F, 0F);

		turretModel[27].addBox(15F, -19F, -17F, 9, 7, 9, 0F); // Box 27
		turretModel[27].setRotationPoint(-6F, -26F, 0F);

		turretModel[28].addShapeBox(9F, -19F, -15F, 6, 1, 7, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 5F); // Box 28
		turretModel[28].setRotationPoint(-6F, -26F, 0F);

		turretModel[29].addBox(24F, -19F, -16F, 1, 7, 5, 0F); // Box 29
		turretModel[29].setRotationPoint(-6F, -26F, 0F);

		turretModel[30].addShapeBox(15F, -18F, -3F, 5, 4, 11, 0F,0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 30
		turretModel[30].setRotationPoint(-6F, -26F, 0F);

		turretModel[31].addShapeBox(20F, -16F, -3F, 10, 4, 11, 0F,0F, 0F, 0F, 0F, -3F, -8F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -8F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 31
		turretModel[31].setRotationPoint(-6F, -26F, 0F);

		turretModel[32].addBox(-16F, -1F, -6F, 32, 1, 12, 0F); // Box 0
		turretModel[32].setRotationPoint(-6F, -26F, 0F);

		turretModel[33].addShapeBox(-16F, -1F, 6F, 32, 1, 6, 0F,0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F); // Box 5
		turretModel[33].setRotationPoint(-6F, -26F, 0F);

		turretModel[34].addShapeBox(-13F, -1F, 12F, 26, 1, 6, 0F,0F, 0F, 0F, 0F, 0F, 0F, -7F, 0F, 0F, -7F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -7F, 0F, 0F, -7F, 0F, 0F); // Box 7
		turretModel[34].setRotationPoint(-6F, -26F, 0F);

		turretModel[35].addShapeBox(-16F, -1F, -12F, 32, 1, 6, 0F,-3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 8
		turretModel[35].setRotationPoint(-6F, -26F, 0F);

		turretModel[36].addShapeBox(-13F, -1F, -18F, 26, 1, 6, 0F,-7F, 0F, 0F, -7F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -7F, 0F, 0F, -7F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 9
		turretModel[36].setRotationPoint(-6F, -26F, 0F);

		turretModel[37].addShapeBox(-19.5F, -13F, -26F, 6, 10, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1.5F, 0F, 0F, 1.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 373
		turretModel[37].setRotationPoint(-6F, -26F, 0F);

		turretModel[38].addShapeBox(-12.5F, -13F, -26F, 6, 10, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1.5F, 0F, 0F, 1.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 374
		turretModel[38].setRotationPoint(-6F, -26F, 0F);

		turretModel[39].addShapeBox(1.5F, -13F, -26F, 6, 10, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1.5F, 0F, 0F, 1.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 375
		turretModel[39].setRotationPoint(-6F, -26F, 0F);

		turretModel[40].addShapeBox(-5.5F, -13F, -26F, 6, 10, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1.5F, 0F, 0F, 1.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 376
		turretModel[40].setRotationPoint(-6F, -26F, 0F);

		turretModel[41].addShapeBox(8.5F, -13F, -26F, 6, 10, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1.5F, 0F, 0F, 1.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 377
		turretModel[41].setRotationPoint(-6F, -26F, 0F);

		turretModel[42].addShapeBox(8.5F, -13F, 25F, 6, 10, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1.5F, 0F, 0F, 1.5F); // Box 378
		turretModel[42].setRotationPoint(-6F, -26F, 0F);

		turretModel[43].addShapeBox(1.5F, -13F, 25F, 6, 10, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1.5F, 0F, 0F, 1.5F); // Box 379
		turretModel[43].setRotationPoint(-6F, -26F, 0F);

		turretModel[44].addShapeBox(-5.5F, -13F, 25F, 6, 10, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1.5F, 0F, 0F, 1.5F); // Box 380
		turretModel[44].setRotationPoint(-6F, -26F, 0F);

		turretModel[45].addShapeBox(-12.5F, -13F, 25F, 6, 10, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1.5F, 0F, 0F, 1.5F); // Box 381
		turretModel[45].setRotationPoint(-6F, -26F, 0F);

		turretModel[46].addShapeBox(-19.5F, -13F, 25F, 6, 10, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1.5F, 0F, 0F, 1.5F); // Box 382
		turretModel[46].setRotationPoint(-6F, -26F, 0F);

		turretModel[47].addBox(21F, -14F, 9F, 3, 2, 3, 0F); // Box 384
		turretModel[47].setRotationPoint(-6F, -26F, 0F);

		turretModel[48].addBox(15.5F, -15F, -7.5F, 3, 2, 4, 0F); // Box 385
		turretModel[48].setRotationPoint(-6F, -26F, 0F);

		turretModel[49].addShapeBox(-38.5F, -14.5F, -25.3F, 20, 20, 1, 0F,0F, 0F, 0F, -10F, 0F, 0F, -10F, 0F, 0F, 0F, 0F, 0F, 0F, -10F, 2F, -10F, -10F, 2F, -10F, -10F, 0F, 0F, -10F, 0F); // decal
		turretModel[49].setRotationPoint(-6F, -26F, 0F);

		turretModel[50].addShapeBox(-38.5F, -14.5F, 24.3F, 20, 20, 1, 0F,0F, 0F, 0F, -10F, 0F, 0F, -10F, 0F, 0F, 0F, 0F, 0F, 0F, -10F, 0F, -10F, -10F, 0F, -10F, -10F, 2F, 0F, -10F, 2F); // Box 408
		turretModel[50].setRotationPoint(-6F, -26F, 0F);

		turretModel[51].addShapeBox(-16F, -20F, 15F, 14, 2, 3, 0F,0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F); // Box 409
		turretModel[51].setRotationPoint(-6F, -26F, 0F);

		turretModel[52].addShapeBox(-18F, -20F, 12F, 18, 2, 3, 0F,0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F); // Box 410
		turretModel[52].setRotationPoint(-6F, -26F, 0F);

		turretModel[53].addBox(-18F, -20F, 6F, 18, 2, 6, 0F); // Box 411
		turretModel[53].setRotationPoint(-6F, -26F, 0F);

		turretModel[54].addShapeBox(-18F, -20F, 3F, 18, 2, 3, 0F,-2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 412
		turretModel[54].setRotationPoint(-6F, -26F, 0F);

		turretModel[55].addShapeBox(-16F, -20F, 0F, 14, 2, 3, 0F,-4F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 413
		turretModel[55].setRotationPoint(-6F, -26F, 0F);

		turretModel[56].addBox(10F, -21F, -7F, 1, 1, 1, 0F); // Box 30
		turretModel[56].setRotationPoint(-6F, -26F, 0F);

		turretModel[57].addBox(10F, -21F, -12F, 1, 1, 1, 0F); // Box 31
		turretModel[57].setRotationPoint(-6F, -26F, 0F);

		turretModel[58].addBox(4F, -21F, -1F, 1, 1, 1, 0F); // Box 32
		turretModel[58].setRotationPoint(-6F, -26F, 0F);

		turretModel[59].addBox(-1F, -21F, -1F, 1, 1, 1, 0F); // Box 33
		turretModel[59].setRotationPoint(-6F, -26F, 0F);

		turretModel[60].addBox(4F, -21F, -18F, 1, 1, 1, 0F); // Box 34
		turretModel[60].setRotationPoint(-6F, -26F, 0F);

		turretModel[61].addBox(-1F, -21F, -18F, 1, 1, 1, 0F); // Box 35
		turretModel[61].setRotationPoint(-6F, -26F, 0F);

		turretModel[62].addShapeBox(-42F, -19F, 15F, 1, 7, 2, 0F,0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F); // Box 36
		turretModel[62].setRotationPoint(-6F, -26F, 0F);

		turretModel[63].addShapeBox(-41F, -19F, 15F, 1, 7, 2, 0F,0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F); // Box 37
		turretModel[63].setRotationPoint(-6F, -26F, 0F);

		turretModel[64].addBox(-42F, -12F, 14F, 3, 4, 6, 0F); // Box 38
		turretModel[64].setRotationPoint(-6F, -26F, 0F);

		turretModel[65].addBox(-41.5F, -39F, 15.5F, 1, 20, 1, 0F); // Box 39
		turretModel[65].setRotationPoint(-6F, -26F, 0F);

		turretModel[66].addBox(-42F, -41F, 15F, 2, 2, 2, 0F); // Box 40
		turretModel[66].setRotationPoint(-6F, -26F, 0F);

		turretModel[67].addShapeBox(-40F, -14F, 7F, 1, 9, 6, 0F,1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 41
		turretModel[67].setRotationPoint(-6F, -26F, 0F);

		turretModel[68].addShapeBox(-40F, -14F, 0F, 1, 9, 6, 0F,1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 42
		turretModel[68].setRotationPoint(-6F, -26F, 0F);

		turretModel[69].addShapeBox(-40F, -14F, -14F, 1, 9, 6, 0F,1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 43
		turretModel[69].setRotationPoint(-6F, -26F, 0F);

		turretModel[70].addShapeBox(-40F, -14F, -7F, 1, 9, 6, 0F,1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 44
		turretModel[70].setRotationPoint(-6F, -26F, 0F);

		turretModel[71].addShapeBox(-40F, -14F, -21F, 1, 9, 6, 0F,1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 45
		turretModel[71].setRotationPoint(-6F, -26F, 0F);

		turretModel[72].addShapeBox(28F, -6F, -25F, 13, 5, 18, 0F,0F, 0F, 1F, 0F, 0F, -1F, 9F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 0F, 9F, 0F, 0F, 0F, 0F, 0F); // Box 559
		turretModel[72].setRotationPoint(-6F, -26F, 0F);

		turretModel[73].addShapeBox(28F, -6F, 7F, 13, 5, 18, 0F,0F, 0F, 0F, 9F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F, 0F, 9F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F); // Box 560
		turretModel[73].setRotationPoint(-6F, -26F, 0F);

		turretModel[74].addBox(30F, -13F, 10F, 6, 4, 6, 0F); // Box 561
		turretModel[74].setRotationPoint(-6F, -26F, 0F);

		turretModel[75].addShapeBox(28F, -14.5F, 8F, 3, 2, 10, 0F,0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F); // Box 563
		turretModel[75].setRotationPoint(-6F, -26F, 0F);

		turretModel[76].addShapeBox(35F, -14.5F, 8F, 3, 2, 10, 0F,0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F); // Box 564
		turretModel[76].setRotationPoint(-6F, -26F, 0F);

		turretModel[77].addBox(31F, -14.5F, 8F, 4, 2, 10, 0F); // Box 565
		turretModel[77].setRotationPoint(-6F, -26F, 0F);

		turretModel[78].addShapeBox(35F, -15.5F, 8F, 3, 1, 10, 0F,0F, 0F, -1F, -1F, 0F, -3F, -1F, 0F, -3F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F); // Box 566
		turretModel[78].setRotationPoint(-6F, -26F, 0F);

		turretModel[79].addShapeBox(31F, -15.5F, 8F, 4, 1, 10, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 568
		turretModel[79].setRotationPoint(-6F, -26F, 0F);

		turretModel[80].addShapeBox(28F, -15.5F, 8F, 3, 1, 10, 0F,-1F, 0F, -3F, 0F, 0F, -1F, 0F, 0F, -1F, -1F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F); // Box 569
		turretModel[80].setRotationPoint(-6F, -26F, 0F);

		turretModel[81].addShapeBox(29F, -21.5F, 9F, 2, 6, 8, 0F,-1F, 0F, -3F, 1F, 0F, -1F, 1F, 0F, -1F, -1F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F); // Box 570
		turretModel[81].setRotationPoint(-6F, -26F, 0F);

		turretModel[82].addShapeBox(31F, -21.5F, 9F, 4, 6, 8, 0F,-1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 571
		turretModel[82].setRotationPoint(-6F, -26F, 0F);

		turretModel[83].addShapeBox(35F, -21.5F, 9F, 2, 6, 8, 0F,1F, 0F, -1F, -1F, 0F, -3F, -1F, 0F, -3F, 1F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F); // Box 572
		turretModel[83].setRotationPoint(-6F, -26F, 0F);

		turretModel[84].addBox(-7F, -29F, 10F, 11, 4, 3, 0F); // Box 0
		turretModel[84].setRotationPoint(-6F, -26F, 0F);

		turretModel[85].addShapeBox(-7F, -30F, 10F, 11, 1, 3, 0F,-1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 1
		turretModel[85].setRotationPoint(-6F, -26F, 0F);

		turretModel[86].addShapeBox(4F, -27.5F, 10.5F, 10, 2, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F); // Box 2
		turretModel[86].setRotationPoint(-6F, -26F, 0F);

		turretModel[87].addShapeBox(4F, -29.5F, 10.5F, 14, 1, 2, 0F,0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 3
		turretModel[87].setRotationPoint(-6F, -26F, 0F);

		turretModel[88].addShapeBox(4F, -27.5F, 10.5F, 14, 1, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 4
		turretModel[88].setRotationPoint(-6F, -27F, 0F);

		turretModel[89].addBox(-3F, -26F, 9.5F, 5, 4, 7, 0F); // Box 5
		turretModel[89].setRotationPoint(-6F, -26F, 0F);

		turretModel[90].addBox(0F, -30.5F, 10.5F, 6, 1, 2, 0F); // Box 6
		turretModel[90].setRotationPoint(-6F, -26F, 0F);

		turretModel[91].addShapeBox(18F, -30F, 10F, 4, 1, 3, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 7
		turretModel[91].setRotationPoint(-6F, -26F, 0F);

		turretModel[92].addBox(18F, -29F, 10F, 4, 1, 3, 0F); // Box 8
		turretModel[92].setRotationPoint(-6F, -26F, 0F);

		turretModel[93].addShapeBox(18F, -28F, 10F, 4, 1, 3, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 9
		turretModel[93].setRotationPoint(-6F, -26F, 0F);

		turretModel[94].addBox(0F, -30.5F, 9.5F, 1, 1, 1, 0F); // Box 10
		turretModel[94].setRotationPoint(-6F, -26F, 0F);

		turretModel[95].addBox(-3F, -31.5F, 9.5F, 4, 1, 1, 0F); // Box 11
		turretModel[95].setRotationPoint(-6F, -26F, 0F);

		turretModel[96].addBox(5F, -32.5F, 11F, 1, 2, 1, 0F); // Box 12
		turretModel[96].setRotationPoint(-6F, -26F, 0F);

		turretModel[97].addBox(-7F, -29F, 4F, 11, 4, 3, 0F); // Box 13
		turretModel[97].setRotationPoint(-6F, -26F, 0F);

		turretModel[98].addBox(-3F, -26F, 0.5F, 5, 4, 7, 0F); // Box 14
		turretModel[98].setRotationPoint(-6F, -26F, 0F);

		turretModel[99].addShapeBox(-7F, -30F, 4F, 11, 1, 3, 0F,-1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 15
		turretModel[99].setRotationPoint(-6F, -26F, 0F);

		turretModel[100].addBox(0F, -30.5F, 6.5F, 1, 1, 1, 0F); // Box 16
		turretModel[100].setRotationPoint(-6F, -26F, 0F);

		turretModel[101].addBox(-3F, -31.5F, 6.5F, 4, 1, 1, 0F); // Box 17
		turretModel[101].setRotationPoint(-6F, -26F, 0F);

		turretModel[102].addBox(0F, -30.5F, 4.5F, 6, 1, 2, 0F); // Box 18
		turretModel[102].setRotationPoint(-6F, -26F, 0F);

		turretModel[103].addShapeBox(4F, -29.5F, 4.5F, 14, 1, 2, 0F,0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 19
		turretModel[103].setRotationPoint(-6F, -26F, 0F);

		turretModel[104].addShapeBox(4F, -28.5F, 4.5F, 14, 1, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 20
		turretModel[104].setRotationPoint(-6F, -26F, 0F);

		turretModel[105].addShapeBox(4F, -27.5F, 4.5F, 10, 2, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F); // Box 21
		turretModel[105].setRotationPoint(-6F, -26F, 0F);

		turretModel[106].addBox(5F, -32.5F, 5F, 1, 2, 1, 0F); // Box 22
		turretModel[106].setRotationPoint(-6F, -26F, 0F);

		turretModel[107].addShapeBox(18F, -30F, 4F, 4, 1, 3, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 23
		turretModel[107].setRotationPoint(-6F, -26F, 0F);

		turretModel[108].addBox(18F, -29F, 4F, 4, 1, 3, 0F); // Box 24
		turretModel[108].setRotationPoint(-6F, -26F, 0F);

		turretModel[109].addShapeBox(18F, -28F, 4F, 4, 1, 3, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 25
		turretModel[109].setRotationPoint(-6F, -26F, 0F);

		turretModel[110].addShapeBox(4F, -27.5F, 6.5F, 5, 2, 4, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F); // Box 26
		turretModel[110].setRotationPoint(-6F, -26F, 0F);

		turretModel[111].addShapeBox(4.5F, -25.5F, 6F, 3, 8, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 8F, 0F, 0F, -8F, 0F, 0F, -8F, 0F, 0F, 8F, 0F, 0F); // Box 27
		turretModel[111].setRotationPoint(-6F, -26F, 0F);

		turretModel[112].addShapeBox(4.5F, -25.5F, 9F, 3, 8, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 8F, 0F, 0F, -8F, 0F, 0F, -8F, 0F, 0F, 8F, 0F, 0F); // Box 28
		turretModel[112].setRotationPoint(-6F, -26F, 0F);

		turretModel[113].addBox(-3.5F, -20.5F, 5.5F, 4, 4, 6, 0F); // Box 29
		turretModel[113].setRotationPoint(-6F, -26F, 0F);
	}

	private void initbarrelModel_1()
	{
		barrelModel[0] = new ModelRendererTurbo(this, 209, 14, textureX, textureY); // Box 0
		barrelModel[1] = new ModelRendererTurbo(this, 209, 21, textureX, textureY); // Box 1
		barrelModel[2] = new ModelRendererTurbo(this, 209, 14, textureX, textureY); // Box 3
		barrelModel[3] = new ModelRendererTurbo(this, 176, 1, textureX, textureY); // Box 4
		barrelModel[4] = new ModelRendererTurbo(this, 176, 7, textureX, textureY); // Box 5
		barrelModel[5] = new ModelRendererTurbo(this, 176, 1, textureX, textureY); // Box 6
		barrelModel[6] = new ModelRendererTurbo(this, 176, 14, textureX, textureY); // Box 7
		barrelModel[7] = new ModelRendererTurbo(this, 176, 21, textureX, textureY); // Box 8
		barrelModel[8] = new ModelRendererTurbo(this, 176, 14, textureX, textureY); // Box 9
		barrelModel[9] = new ModelRendererTurbo(this, 242, 14, textureX, textureY); // Box 10
		barrelModel[10] = new ModelRendererTurbo(this, 176, 30, textureX, textureY); // Box 11
		barrelModel[11] = new ModelRendererTurbo(this, 176, 49, textureX, textureY); // Box 12
		barrelModel[12] = new ModelRendererTurbo(this, 256, 33, textureX, textureY); // Box 13
		barrelModel[13] = new ModelRendererTurbo(this, 256, 14, textureX, textureY); // Box 14
		barrelModel[14] = new ModelRendererTurbo(this, 217, 30, textureX, textureY); // Box 15
		barrelModel[15] = new ModelRendererTurbo(this, 270, 43, textureX, textureY); // Box 3
		barrelModel[16] = new ModelRendererTurbo(this, 305, 7, textureX, textureY); // Box 386
		barrelModel[17] = new ModelRendererTurbo(this, 305, 1, textureX, textureY); // Box 387
		barrelModel[18] = new ModelRendererTurbo(this, 305, 1, textureX, textureY); // Box 388
		barrelModel[19] = new ModelRendererTurbo(this, 310, 18, textureX, textureY); // Box 389
		barrelModel[20] = new ModelRendererTurbo(this, 310, 18, textureX, textureY); // Box 390
		barrelModel[21] = new ModelRendererTurbo(this, 305, 18, textureX, textureY); // Box 391
		barrelModel[22] = new ModelRendererTurbo(this, 310, 14, textureX, textureY); // Box 392
		barrelModel[23] = new ModelRendererTurbo(this, 310, 14, textureX, textureY); // Box 393
		barrelModel[24] = new ModelRendererTurbo(this, 305, 14, textureX, textureY); // Box 394

		barrelModel[0].addShapeBox(18F, -5F, -2.5F, 11, 1, 5, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 0
		barrelModel[0].setRotationPoint(29F, -28F, 0F);

		barrelModel[1].addBox(18F, -4F, -2.5F, 11, 3, 5, 0F); // Box 1
		barrelModel[1].setRotationPoint(29F, -28F, 0F);

		barrelModel[2].addShapeBox(18F, -1F, -2.5F, 11, 1, 5, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 3
		barrelModel[2].setRotationPoint(29F, -28F, 0F);

		barrelModel[3].addShapeBox(29F, -1.5F, -2F, 60, 1, 4, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 4
		barrelModel[3].setRotationPoint(29F, -28F, 0F);

		barrelModel[4].addBox(29F, -3.5F, -2F, 60, 2, 4, 0F); // Box 5
		barrelModel[4].setRotationPoint(29F, -28F, 0F);

		barrelModel[5].addShapeBox(29F, -4.5F, -2F, 60, 1, 4, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 6
		barrelModel[5].setRotationPoint(29F, -28F, 0F);

		barrelModel[6].addShapeBox(61F, -5F, -2.5F, 11, 1, 5, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 7
		barrelModel[6].setRotationPoint(29F, -28F, 0F);

		barrelModel[7].addBox(61F, -4F, -2.5F, 11, 3, 5, 0F); // Box 8
		barrelModel[7].setRotationPoint(29F, -28F, 0F);

		barrelModel[8].addShapeBox(61F, -1F, -2.5F, 11, 1, 5, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 9
		barrelModel[8].setRotationPoint(29F, -28F, 0F);

		barrelModel[9].addShapeBox(93F, -6.5F, -1F, 5, 2, 2, 0F,0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 10
		barrelModel[9].setRotationPoint(29F, -28F, 0F);

		barrelModel[10].addBox(6F, -9F, -3F, 12, 10, 8, 0F); // Box 11
		barrelModel[10].setRotationPoint(29F, -28F, -1F);

		barrelModel[11].addShapeBox(5F, -12F, -3F, 13, 3, 8, 0F,-3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 12
		barrelModel[11].setRotationPoint(29F, -28F, -1F);

		barrelModel[12].addShapeBox(8F, -13F, -3F, 10, 1, 8, 0F,-1F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -1F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 13
		barrelModel[12].setRotationPoint(29F, -28F, -1F);

		barrelModel[13].addBox(5F, -10F, -7F, 3, 4, 14, 0F); // Box 14
		barrelModel[13].setRotationPoint(29F, -28F, 0F);

		barrelModel[14].addShapeBox(-7F, -10F, -7F, 12, 11, 14, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 10F, 0F, 0F, 10F, 0F, 0F, 0F, 0F, 0F); // Box 15
		barrelModel[14].setRotationPoint(29F, -28F, 0F);

		barrelModel[15].addBox(18F, -12F, -2F, 1, 6, 6, 0F); // Box 3
		barrelModel[15].setRotationPoint(29F, -28F, -1F);

		barrelModel[16].addBox(89F, -3.5F, -2F, 4, 2, 4, 0F); // Box 386
		barrelModel[16].setRotationPoint(29F, -28F, 0F);

		barrelModel[17].addShapeBox(89F, -1.5F, -2F, 10, 1, 4, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 387
		barrelModel[17].setRotationPoint(29F, -28F, 0F);

		barrelModel[18].addShapeBox(89F, -4.5F, -2F, 10, 1, 4, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 388
		barrelModel[18].setRotationPoint(29F, -28F, 0F);

		barrelModel[19].addBox(94F, -3.5F, -2F, 1, 2, 1, 0F); // Box 389
		barrelModel[19].setRotationPoint(29F, -28F, 0F);

		barrelModel[20].addBox(96F, -3.5F, -2F, 1, 2, 1, 0F); // Box 390
		barrelModel[20].setRotationPoint(29F, -28F, 0F);

		barrelModel[21].addBox(98F, -3.5F, -2F, 1, 2, 1, 0F); // Box 391
		barrelModel[21].setRotationPoint(29F, -28F, 0F);

		barrelModel[22].addBox(96F, -3.5F, 1F, 1, 2, 1, 0F); // Box 392
		barrelModel[22].setRotationPoint(29F, -28F, 0F);

		barrelModel[23].addBox(94F, -3.5F, 1F, 1, 2, 1, 0F); // Box 393
		barrelModel[23].setRotationPoint(29F, -28F, 0F);

		barrelModel[24].addBox(98F, -3.5F, 1F, 1, 2, 1, 0F); // Box 394
		barrelModel[24].setRotationPoint(29F, -28F, 0F);
	}

	private void initleftTrackWheelModels_1()
	{
		leftTrackWheelModels[0] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 12
		leftTrackWheelModels[1] = new ModelRendererTurbo(this, 59, 8, textureX, textureY); // Box 12
		leftTrackWheelModels[2] = new ModelRendererTurbo(this, 59, 1, textureX, textureY); // Box 12
		leftTrackWheelModels[3] = new ModelRendererTurbo(this, 46, 11, textureX, textureY); // Box 12
		leftTrackWheelModels[4] = new ModelRendererTurbo(this, 46, 1, textureX, textureY); // Box 12
		leftTrackWheelModels[5] = new ModelRendererTurbo(this, 1, 32, textureX, textureY); // Box 12
		leftTrackWheelModels[6] = new ModelRendererTurbo(this, 35, 32, textureX, textureY); // Box 12
		leftTrackWheelModels[7] = new ModelRendererTurbo(this, 18, 32, textureX, textureY); // Box 12
		leftTrackWheelModels[8] = new ModelRendererTurbo(this, 52, 32, textureX, textureY); // Box 12
		leftTrackWheelModels[9] = new ModelRendererTurbo(this, 59, 8, textureX, textureY); // Box 12
		leftTrackWheelModels[10] = new ModelRendererTurbo(this, 1, 32, textureX, textureY); // Box 12
		leftTrackWheelModels[11] = new ModelRendererTurbo(this, 46, 11, textureX, textureY); // Box 12
		leftTrackWheelModels[12] = new ModelRendererTurbo(this, 35, 32, textureX, textureY); // Box 12
		leftTrackWheelModels[13] = new ModelRendererTurbo(this, 59, 1, textureX, textureY); // Box 12
		leftTrackWheelModels[14] = new ModelRendererTurbo(this, 18, 32, textureX, textureY); // Box 12
		leftTrackWheelModels[15] = new ModelRendererTurbo(this, 46, 1, textureX, textureY); // Box 12
		leftTrackWheelModels[16] = new ModelRendererTurbo(this, 52, 32, textureX, textureY); // Box 12
		leftTrackWheelModels[17] = new ModelRendererTurbo(this, 80, 61, textureX, textureY); // Box 12
		leftTrackWheelModels[18] = new ModelRendererTurbo(this, 80, 51, textureX, textureY); // Box 12
		leftTrackWheelModels[19] = new ModelRendererTurbo(this, 95, 51, textureX, textureY); // Box 12
		leftTrackWheelModels[20] = new ModelRendererTurbo(this, 104, 60, textureX, textureY); // Box 12
		leftTrackWheelModels[21] = new ModelRendererTurbo(this, 104, 60, textureX, textureY); // Box 12
		leftTrackWheelModels[22] = new ModelRendererTurbo(this, 95, 68, textureX, textureY); // Box 12
		leftTrackWheelModels[23] = new ModelRendererTurbo(this, 95, 68, textureX, textureY); // Box 12
		leftTrackWheelModels[24] = new ModelRendererTurbo(this, 104, 51, textureX, textureY); // Box 12
		leftTrackWheelModels[25] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 12
		leftTrackWheelModels[26] = new ModelRendererTurbo(this, 59, 8, textureX, textureY); // Box 12
		leftTrackWheelModels[27] = new ModelRendererTurbo(this, 59, 1, textureX, textureY); // Box 12
		leftTrackWheelModels[28] = new ModelRendererTurbo(this, 46, 11, textureX, textureY); // Box 12
		leftTrackWheelModels[29] = new ModelRendererTurbo(this, 46, 1, textureX, textureY); // Box 12
		leftTrackWheelModels[30] = new ModelRendererTurbo(this, 1, 32, textureX, textureY); // Box 12
		leftTrackWheelModels[31] = new ModelRendererTurbo(this, 35, 32, textureX, textureY); // Box 12
		leftTrackWheelModels[32] = new ModelRendererTurbo(this, 18, 32, textureX, textureY); // Box 12
		leftTrackWheelModels[33] = new ModelRendererTurbo(this, 52, 32, textureX, textureY); // Box 12
		leftTrackWheelModels[34] = new ModelRendererTurbo(this, 59, 8, textureX, textureY); // Box 12
		leftTrackWheelModels[35] = new ModelRendererTurbo(this, 1, 32, textureX, textureY); // Box 12
		leftTrackWheelModels[36] = new ModelRendererTurbo(this, 46, 11, textureX, textureY); // Box 12
		leftTrackWheelModels[37] = new ModelRendererTurbo(this, 35, 32, textureX, textureY); // Box 12
		leftTrackWheelModels[38] = new ModelRendererTurbo(this, 59, 1, textureX, textureY); // Box 12
		leftTrackWheelModels[39] = new ModelRendererTurbo(this, 18, 32, textureX, textureY); // Box 12
		leftTrackWheelModels[40] = new ModelRendererTurbo(this, 46, 1, textureX, textureY); // Box 12
		leftTrackWheelModels[41] = new ModelRendererTurbo(this, 52, 32, textureX, textureY); // Box 12
		leftTrackWheelModels[42] = new ModelRendererTurbo(this, 80, 61, textureX, textureY); // Box 12
		leftTrackWheelModels[43] = new ModelRendererTurbo(this, 80, 51, textureX, textureY); // Box 12
		leftTrackWheelModels[44] = new ModelRendererTurbo(this, 95, 51, textureX, textureY); // Box 12
		leftTrackWheelModels[45] = new ModelRendererTurbo(this, 104, 60, textureX, textureY); // Box 12
		leftTrackWheelModels[46] = new ModelRendererTurbo(this, 104, 60, textureX, textureY); // Box 12
		leftTrackWheelModels[47] = new ModelRendererTurbo(this, 95, 68, textureX, textureY); // Box 12
		leftTrackWheelModels[48] = new ModelRendererTurbo(this, 95, 68, textureX, textureY); // Box 12
		leftTrackWheelModels[49] = new ModelRendererTurbo(this, 104, 51, textureX, textureY); // Box 12
		leftTrackWheelModels[50] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 12
		leftTrackWheelModels[51] = new ModelRendererTurbo(this, 59, 8, textureX, textureY); // Box 12
		leftTrackWheelModels[52] = new ModelRendererTurbo(this, 59, 1, textureX, textureY); // Box 12
		leftTrackWheelModels[53] = new ModelRendererTurbo(this, 46, 11, textureX, textureY); // Box 12
		leftTrackWheelModels[54] = new ModelRendererTurbo(this, 46, 1, textureX, textureY); // Box 12
		leftTrackWheelModels[55] = new ModelRendererTurbo(this, 1, 32, textureX, textureY); // Box 12
		leftTrackWheelModels[56] = new ModelRendererTurbo(this, 35, 32, textureX, textureY); // Box 12
		leftTrackWheelModels[57] = new ModelRendererTurbo(this, 18, 32, textureX, textureY); // Box 12
		leftTrackWheelModels[58] = new ModelRendererTurbo(this, 52, 32, textureX, textureY); // Box 12
		leftTrackWheelModels[59] = new ModelRendererTurbo(this, 59, 8, textureX, textureY); // Box 12
		leftTrackWheelModels[60] = new ModelRendererTurbo(this, 1, 32, textureX, textureY); // Box 12
		leftTrackWheelModels[61] = new ModelRendererTurbo(this, 46, 11, textureX, textureY); // Box 12
		leftTrackWheelModels[62] = new ModelRendererTurbo(this, 35, 32, textureX, textureY); // Box 12
		leftTrackWheelModels[63] = new ModelRendererTurbo(this, 59, 1, textureX, textureY); // Box 12
		leftTrackWheelModels[64] = new ModelRendererTurbo(this, 18, 32, textureX, textureY); // Box 12
		leftTrackWheelModels[65] = new ModelRendererTurbo(this, 46, 1, textureX, textureY); // Box 12
		leftTrackWheelModels[66] = new ModelRendererTurbo(this, 52, 32, textureX, textureY); // Box 12
		leftTrackWheelModels[67] = new ModelRendererTurbo(this, 80, 61, textureX, textureY); // Box 12
		leftTrackWheelModels[68] = new ModelRendererTurbo(this, 80, 51, textureX, textureY); // Box 12
		leftTrackWheelModels[69] = new ModelRendererTurbo(this, 95, 51, textureX, textureY); // Box 12
		leftTrackWheelModels[70] = new ModelRendererTurbo(this, 104, 60, textureX, textureY); // Box 12
		leftTrackWheelModels[71] = new ModelRendererTurbo(this, 104, 60, textureX, textureY); // Box 12
		leftTrackWheelModels[72] = new ModelRendererTurbo(this, 95, 68, textureX, textureY); // Box 12
		leftTrackWheelModels[73] = new ModelRendererTurbo(this, 95, 68, textureX, textureY); // Box 12
		leftTrackWheelModels[74] = new ModelRendererTurbo(this, 104, 51, textureX, textureY); // Box 12
		leftTrackWheelModels[75] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 12
		leftTrackWheelModels[76] = new ModelRendererTurbo(this, 59, 8, textureX, textureY); // Box 12
		leftTrackWheelModels[77] = new ModelRendererTurbo(this, 59, 1, textureX, textureY); // Box 12
		leftTrackWheelModels[78] = new ModelRendererTurbo(this, 46, 11, textureX, textureY); // Box 12
		leftTrackWheelModels[79] = new ModelRendererTurbo(this, 46, 1, textureX, textureY); // Box 12
		leftTrackWheelModels[80] = new ModelRendererTurbo(this, 1, 32, textureX, textureY); // Box 12
		leftTrackWheelModels[81] = new ModelRendererTurbo(this, 35, 32, textureX, textureY); // Box 12
		leftTrackWheelModels[82] = new ModelRendererTurbo(this, 18, 32, textureX, textureY); // Box 12
		leftTrackWheelModels[83] = new ModelRendererTurbo(this, 52, 32, textureX, textureY); // Box 12
		leftTrackWheelModels[84] = new ModelRendererTurbo(this, 59, 8, textureX, textureY); // Box 12
		leftTrackWheelModels[85] = new ModelRendererTurbo(this, 1, 32, textureX, textureY); // Box 12
		leftTrackWheelModels[86] = new ModelRendererTurbo(this, 46, 11, textureX, textureY); // Box 12
		leftTrackWheelModels[87] = new ModelRendererTurbo(this, 35, 32, textureX, textureY); // Box 12
		leftTrackWheelModels[88] = new ModelRendererTurbo(this, 59, 1, textureX, textureY); // Box 12
		leftTrackWheelModels[89] = new ModelRendererTurbo(this, 18, 32, textureX, textureY); // Box 12
		leftTrackWheelModels[90] = new ModelRendererTurbo(this, 46, 1, textureX, textureY); // Box 12
		leftTrackWheelModels[91] = new ModelRendererTurbo(this, 52, 32, textureX, textureY); // Box 12
		leftTrackWheelModels[92] = new ModelRendererTurbo(this, 80, 61, textureX, textureY); // Box 12
		leftTrackWheelModels[93] = new ModelRendererTurbo(this, 80, 51, textureX, textureY); // Box 12
		leftTrackWheelModels[94] = new ModelRendererTurbo(this, 95, 51, textureX, textureY); // Box 12
		leftTrackWheelModels[95] = new ModelRendererTurbo(this, 104, 60, textureX, textureY); // Box 12
		leftTrackWheelModels[96] = new ModelRendererTurbo(this, 104, 60, textureX, textureY); // Box 12
		leftTrackWheelModels[97] = new ModelRendererTurbo(this, 95, 68, textureX, textureY); // Box 12
		leftTrackWheelModels[98] = new ModelRendererTurbo(this, 95, 68, textureX, textureY); // Box 12
		leftTrackWheelModels[99] = new ModelRendererTurbo(this, 104, 51, textureX, textureY); // Box 12

		leftTrackWheelModels[0].addShape3D(11F, -11F, -4F, new Shape2D(new Coord2D[] { new Coord2D(7, 0, 7, 0), new Coord2D(15, 0, 15, 0), new Coord2D(22, 7, 22, 7), new Coord2D(22, 15, 22, 15), new Coord2D(15, 22, 15, 22), new Coord2D(7, 22, 7, 22), new Coord2D(0, 15, 0, 15), new Coord2D(0, 7, 0, 7) }), 8, 22, 22, 72, 8, ModelRendererTurbo.MR_FRONT, new float[] {10 ,8 ,10 ,8 ,10 ,8 ,10 ,8}); // Box 12
		leftTrackWheelModels[0].setRotationPoint(48.5F, -2F, 29F);

		leftTrackWheelModels[1].addShapeBox(-4F, -11F, 4F, 8, 5, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F); // Box 12
		leftTrackWheelModels[1].setRotationPoint(48.5F, -2F, 29F);

		leftTrackWheelModels[2].addShapeBox(-4F, 6F, 4F, 8, 5, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F); // Box 12
		leftTrackWheelModels[2].setRotationPoint(48.5F, -2F, 29F);

		leftTrackWheelModels[3].addShapeBox(-11F, -4F, 4F, 5, 8, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 12
		leftTrackWheelModels[3].setRotationPoint(48.5F, -2F, 29F);

		leftTrackWheelModels[4].addShapeBox(6F, -4F, 4F, 5, 8, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 12
		leftTrackWheelModels[4].setRotationPoint(48.5F, -2F, 29F);

		leftTrackWheelModels[5].addShapeBox(-11F, -11F, 4F, 7, 7, 1, 0F,-7F, 0F, 0F, 0F, -5F, 0F, 0F, -4F, 0F, -7F, -1F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -3F, 0F, 0F, -1F, 0F, 0F); // Box 12
		leftTrackWheelModels[5].setRotationPoint(48.5F, -2F, 29F);

		leftTrackWheelModels[6].addShapeBox(-11F, 4F, 4F, 7, 7, 1, 0F,0F, 0F, 0F, -2F, 0F, 0F, -3F, 0F, 0F, -1F, 0F, 0F, -7F, 0F, 0F, 0F, -5F, 0F, 0F, -4F, 0F, -7F, -1F, 0F); // Box 12
		leftTrackWheelModels[6].setRotationPoint(48.5F, -2F, 29F);

		leftTrackWheelModels[7].addShapeBox(4F, 4F, 4F, 7, 7, 1, 0F,-2F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -3F, 0F, 0F, 0F, -5F, 0F, -7F, 0F, 0F, -7F, -1F, 0F, 0F, -4F, 0F); // Box 12
		leftTrackWheelModels[7].setRotationPoint(48.5F, -2F, 29F);

		leftTrackWheelModels[8].addShapeBox(4F, -11F, 4F, 7, 7, 1, 0F,0F, -5F, 0F, -7F, 0F, 0F, -7F, -1F, 0F, 0F, -4F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -3F, 0F, 0F); // Box 12
		leftTrackWheelModels[8].setRotationPoint(48.5F, -2F, 29F);

		leftTrackWheelModels[9].addShapeBox(-4F, -11F, -5F, 8, 5, 1, 0F,0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 12
		leftTrackWheelModels[9].setRotationPoint(48.5F, -2F, 29F);

		leftTrackWheelModels[10].addShapeBox(-11F, -11F, -5F, 7, 7, 1, 0F,-7F, -1F, 0F, 0F, -4F, 0F, 0F, -5F, 0F, -7F, 0F, 0F, -1F, 0F, 0F, -3F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F); // Box 12
		leftTrackWheelModels[10].setRotationPoint(48.5F, -2F, 29F);

		leftTrackWheelModels[11].addShapeBox(-11F, -4F, -5F, 5, 8, 1, 0F,-1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 12
		leftTrackWheelModels[11].setRotationPoint(48.5F, -2F, 29F);

		leftTrackWheelModels[12].addShapeBox(-11F, 4F, -5F, 7, 7, 1, 0F,-1F, 0F, 0F, -3F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, -7F, -1F, 0F, 0F, -4F, 0F, 0F, -5F, 0F, -7F, 0F, 0F); // Box 12
		leftTrackWheelModels[12].setRotationPoint(48.5F, -2F, 29F);

		leftTrackWheelModels[13].addShapeBox(-4F, 6F, -5F, 8, 5, 1, 0F,0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 12
		leftTrackWheelModels[13].setRotationPoint(48.5F, -2F, 29F);

		leftTrackWheelModels[14].addShapeBox(4F, 4F, -5F, 7, 7, 1, 0F,-3F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, -4F, 0F, -7F, -1F, 0F, -7F, 0F, 0F, 0F, -5F, 0F); // Box 12
		leftTrackWheelModels[14].setRotationPoint(48.5F, -2F, 29F);

		leftTrackWheelModels[15].addShapeBox(6F, -4F, -5F, 5, 8, 1, 0F,-1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 12
		leftTrackWheelModels[15].setRotationPoint(48.5F, -2F, 29F);

		leftTrackWheelModels[16].addShapeBox(4F, -11F, -5F, 7, 7, 1, 0F,0F, -4F, 0F, -7F, -1F, 0F, -7F, 0F, 0F, 0F, -5F, 0F, -3F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F); // Box 12
		leftTrackWheelModels[16].setRotationPoint(48.5F, -2F, 29F);

		leftTrackWheelModels[17].addShapeBox(1F, -4F, 5F, 6, 8, 1, 0F,0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F); // Box 12
		leftTrackWheelModels[17].setRotationPoint(48.5F, -2F, 29F);

		leftTrackWheelModels[18].addShapeBox(-7F, -4F, 4F, 6, 8, 1, 0F,0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F); // Box 12
		leftTrackWheelModels[18].setRotationPoint(48.5F, -2F, 29F);

		leftTrackWheelModels[19].addBox(-1F, -7F, 4F, 2, 14, 2, 0F); // Box 12
		leftTrackWheelModels[19].setRotationPoint(48.5F, -2F, 29F);

		leftTrackWheelModels[20].addBox(-2F, -3.5F, 4.2F, 1, 1, 1, 0F); // Box 12
		leftTrackWheelModels[20].setRotationPoint(48.5F, -2F, 29F);

		leftTrackWheelModels[21].addBox(-2F, 2.5F, 4.2F, 1, 1, 1, 0F); // Box 12
		leftTrackWheelModels[21].setRotationPoint(48.5F, -2F, 29F);

		leftTrackWheelModels[22].addBox(5F, 2.5F, 5.2F, 1, 1, 1, 0F); // Box 12
		leftTrackWheelModels[22].setRotationPoint(48.5F, -2F, 29F);

		leftTrackWheelModels[23].addBox(5F, -3.5F, 5.2F, 1, 1, 1, 0F); // Box 12
		leftTrackWheelModels[23].setRotationPoint(48.5F, -2F, 29F);

		leftTrackWheelModels[24].addBox(-3F, -3.5F, 4.2F, 1, 7, 1, 0F); // Box 12
		leftTrackWheelModels[24].setRotationPoint(48.5F, -2F, 29F);

		leftTrackWheelModels[25].addShape3D(11F, -11F, -4F, new Shape2D(new Coord2D[] { new Coord2D(7, 0, 7, 0), new Coord2D(15, 0, 15, 0), new Coord2D(22, 7, 22, 7), new Coord2D(22, 15, 22, 15), new Coord2D(15, 22, 15, 22), new Coord2D(7, 22, 7, 22), new Coord2D(0, 15, 0, 15), new Coord2D(0, 7, 0, 7) }), 8, 22, 22, 72, 8, ModelRendererTurbo.MR_FRONT, new float[] {10 ,8 ,10 ,8 ,10 ,8 ,10 ,8}); // Box 12
		leftTrackWheelModels[25].setRotationPoint(16.5F, -2F, 29F);

		leftTrackWheelModels[26].addShapeBox(-4F, -11F, 4F, 8, 5, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F); // Box 12
		leftTrackWheelModels[26].setRotationPoint(16.5F, -2F, 29F);

		leftTrackWheelModels[27].addShapeBox(-4F, 6F, 4F, 8, 5, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F); // Box 12
		leftTrackWheelModels[27].setRotationPoint(16.5F, -2F, 29F);

		leftTrackWheelModels[28].addShapeBox(-11F, -4F, 4F, 5, 8, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 12
		leftTrackWheelModels[28].setRotationPoint(16.5F, -2F, 29F);

		leftTrackWheelModels[29].addShapeBox(6F, -4F, 4F, 5, 8, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 12
		leftTrackWheelModels[29].setRotationPoint(16.5F, -2F, 29F);

		leftTrackWheelModels[30].addShapeBox(-11F, -11F, 4F, 7, 7, 1, 0F,-7F, 0F, 0F, 0F, -5F, 0F, 0F, -4F, 0F, -7F, -1F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -3F, 0F, 0F, -1F, 0F, 0F); // Box 12
		leftTrackWheelModels[30].setRotationPoint(16.5F, -2F, 29F);

		leftTrackWheelModels[31].addShapeBox(-11F, 4F, 4F, 7, 7, 1, 0F,0F, 0F, 0F, -2F, 0F, 0F, -3F, 0F, 0F, -1F, 0F, 0F, -7F, 0F, 0F, 0F, -5F, 0F, 0F, -4F, 0F, -7F, -1F, 0F); // Box 12
		leftTrackWheelModels[31].setRotationPoint(16.5F, -2F, 29F);

		leftTrackWheelModels[32].addShapeBox(4F, 4F, 4F, 7, 7, 1, 0F,-2F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -3F, 0F, 0F, 0F, -5F, 0F, -7F, 0F, 0F, -7F, -1F, 0F, 0F, -4F, 0F); // Box 12
		leftTrackWheelModels[32].setRotationPoint(16.5F, -2F, 29F);

		leftTrackWheelModels[33].addShapeBox(4F, -11F, 4F, 7, 7, 1, 0F,0F, -5F, 0F, -7F, 0F, 0F, -7F, -1F, 0F, 0F, -4F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -3F, 0F, 0F); // Box 12
		leftTrackWheelModels[33].setRotationPoint(16.5F, -2F, 29F);

		leftTrackWheelModels[34].addShapeBox(-4F, -11F, -5F, 8, 5, 1, 0F,0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 12
		leftTrackWheelModels[34].setRotationPoint(16.5F, -2F, 29F);

		leftTrackWheelModels[35].addShapeBox(-11F, -11F, -5F, 7, 7, 1, 0F,-7F, -1F, 0F, 0F, -4F, 0F, 0F, -5F, 0F, -7F, 0F, 0F, -1F, 0F, 0F, -3F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F); // Box 12
		leftTrackWheelModels[35].setRotationPoint(16.5F, -2F, 29F);

		leftTrackWheelModels[36].addShapeBox(-11F, -4F, -5F, 5, 8, 1, 0F,-1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 12
		leftTrackWheelModels[36].setRotationPoint(16.5F, -2F, 29F);

		leftTrackWheelModels[37].addShapeBox(-11F, 4F, -5F, 7, 7, 1, 0F,-1F, 0F, 0F, -3F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, -7F, -1F, 0F, 0F, -4F, 0F, 0F, -5F, 0F, -7F, 0F, 0F); // Box 12
		leftTrackWheelModels[37].setRotationPoint(16.5F, -2F, 29F);

		leftTrackWheelModels[38].addShapeBox(-4F, 6F, -5F, 8, 5, 1, 0F,0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 12
		leftTrackWheelModels[38].setRotationPoint(16.5F, -2F, 29F);

		leftTrackWheelModels[39].addShapeBox(4F, 4F, -5F, 7, 7, 1, 0F,-3F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, -4F, 0F, -7F, -1F, 0F, -7F, 0F, 0F, 0F, -5F, 0F); // Box 12
		leftTrackWheelModels[39].setRotationPoint(16.5F, -2F, 29F);

		leftTrackWheelModels[40].addShapeBox(6F, -4F, -5F, 5, 8, 1, 0F,-1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 12
		leftTrackWheelModels[40].setRotationPoint(16.5F, -2F, 29F);

		leftTrackWheelModels[41].addShapeBox(4F, -11F, -5F, 7, 7, 1, 0F,0F, -4F, 0F, -7F, -1F, 0F, -7F, 0F, 0F, 0F, -5F, 0F, -3F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F); // Box 12
		leftTrackWheelModels[41].setRotationPoint(16.5F, -2F, 29F);

		leftTrackWheelModels[42].addShapeBox(1F, -4F, 5F, 6, 8, 1, 0F,0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F); // Box 12
		leftTrackWheelModels[42].setRotationPoint(16.5F, -2F, 29F);

		leftTrackWheelModels[43].addShapeBox(-7F, -4F, 4F, 6, 8, 1, 0F,0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F); // Box 12
		leftTrackWheelModels[43].setRotationPoint(16.5F, -2F, 29F);

		leftTrackWheelModels[44].addBox(-1F, -7F, 4F, 2, 14, 2, 0F); // Box 12
		leftTrackWheelModels[44].setRotationPoint(16.5F, -2F, 29F);

		leftTrackWheelModels[45].addBox(-2F, -3.5F, 4.2F, 1, 1, 1, 0F); // Box 12
		leftTrackWheelModels[45].setRotationPoint(16.5F, -2F, 29F);

		leftTrackWheelModels[46].addBox(-2F, 2.5F, 4.2F, 1, 1, 1, 0F); // Box 12
		leftTrackWheelModels[46].setRotationPoint(16.5F, -2F, 29F);

		leftTrackWheelModels[47].addBox(5F, 2.5F, 5.2F, 1, 1, 1, 0F); // Box 12
		leftTrackWheelModels[47].setRotationPoint(16.5F, -2F, 29F);

		leftTrackWheelModels[48].addBox(5F, -3.5F, 5.2F, 1, 1, 1, 0F); // Box 12
		leftTrackWheelModels[48].setRotationPoint(16.5F, -2F, 29F);

		leftTrackWheelModels[49].addBox(-3F, -3.5F, 4.2F, 1, 7, 1, 0F); // Box 12
		leftTrackWheelModels[49].setRotationPoint(16.5F, -2F, 29F);

		leftTrackWheelModels[50].addShape3D(11F, -11F, -4F, new Shape2D(new Coord2D[] { new Coord2D(7, 0, 7, 0), new Coord2D(15, 0, 15, 0), new Coord2D(22, 7, 22, 7), new Coord2D(22, 15, 22, 15), new Coord2D(15, 22, 15, 22), new Coord2D(7, 22, 7, 22), new Coord2D(0, 15, 0, 15), new Coord2D(0, 7, 0, 7) }), 8, 22, 22, 72, 8, ModelRendererTurbo.MR_FRONT, new float[] {10 ,8 ,10 ,8 ,10 ,8 ,10 ,8}); // Box 12
		leftTrackWheelModels[50].setRotationPoint(-16.5F, -2F, 29F);

		leftTrackWheelModels[51].addShapeBox(-4F, -11F, 4F, 8, 5, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F); // Box 12
		leftTrackWheelModels[51].setRotationPoint(-16.5F, -2F, 29F);

		leftTrackWheelModels[52].addShapeBox(-4F, 6F, 4F, 8, 5, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F); // Box 12
		leftTrackWheelModels[52].setRotationPoint(-16.5F, -2F, 29F);

		leftTrackWheelModels[53].addShapeBox(-11F, -4F, 4F, 5, 8, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 12
		leftTrackWheelModels[53].setRotationPoint(-16.5F, -2F, 29F);

		leftTrackWheelModels[54].addShapeBox(6F, -4F, 4F, 5, 8, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 12
		leftTrackWheelModels[54].setRotationPoint(-16.5F, -2F, 29F);

		leftTrackWheelModels[55].addShapeBox(-11F, -11F, 4F, 7, 7, 1, 0F,-7F, 0F, 0F, 0F, -5F, 0F, 0F, -4F, 0F, -7F, -1F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -3F, 0F, 0F, -1F, 0F, 0F); // Box 12
		leftTrackWheelModels[55].setRotationPoint(-16.5F, -2F, 29F);

		leftTrackWheelModels[56].addShapeBox(-11F, 4F, 4F, 7, 7, 1, 0F,0F, 0F, 0F, -2F, 0F, 0F, -3F, 0F, 0F, -1F, 0F, 0F, -7F, 0F, 0F, 0F, -5F, 0F, 0F, -4F, 0F, -7F, -1F, 0F); // Box 12
		leftTrackWheelModels[56].setRotationPoint(-16.5F, -2F, 29F);

		leftTrackWheelModels[57].addShapeBox(4F, 4F, 4F, 7, 7, 1, 0F,-2F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -3F, 0F, 0F, 0F, -5F, 0F, -7F, 0F, 0F, -7F, -1F, 0F, 0F, -4F, 0F); // Box 12
		leftTrackWheelModels[57].setRotationPoint(-16.5F, -2F, 29F);

		leftTrackWheelModels[58].addShapeBox(4F, -11F, 4F, 7, 7, 1, 0F,0F, -5F, 0F, -7F, 0F, 0F, -7F, -1F, 0F, 0F, -4F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -3F, 0F, 0F); // Box 12
		leftTrackWheelModels[58].setRotationPoint(-16.5F, -2F, 29F);

		leftTrackWheelModels[59].addShapeBox(-4F, -11F, -5F, 8, 5, 1, 0F,0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 12
		leftTrackWheelModels[59].setRotationPoint(-16.5F, -2F, 29F);

		leftTrackWheelModels[60].addShapeBox(-11F, -11F, -5F, 7, 7, 1, 0F,-7F, -1F, 0F, 0F, -4F, 0F, 0F, -5F, 0F, -7F, 0F, 0F, -1F, 0F, 0F, -3F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F); // Box 12
		leftTrackWheelModels[60].setRotationPoint(-16.5F, -2F, 29F);

		leftTrackWheelModels[61].addShapeBox(-11F, -4F, -5F, 5, 8, 1, 0F,-1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 12
		leftTrackWheelModels[61].setRotationPoint(-16.5F, -2F, 29F);

		leftTrackWheelModels[62].addShapeBox(-11F, 4F, -5F, 7, 7, 1, 0F,-1F, 0F, 0F, -3F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, -7F, -1F, 0F, 0F, -4F, 0F, 0F, -5F, 0F, -7F, 0F, 0F); // Box 12
		leftTrackWheelModels[62].setRotationPoint(-16.5F, -2F, 29F);

		leftTrackWheelModels[63].addShapeBox(-4F, 6F, -5F, 8, 5, 1, 0F,0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 12
		leftTrackWheelModels[63].setRotationPoint(-16.5F, -2F, 29F);

		leftTrackWheelModels[64].addShapeBox(4F, 4F, -5F, 7, 7, 1, 0F,-3F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, -4F, 0F, -7F, -1F, 0F, -7F, 0F, 0F, 0F, -5F, 0F); // Box 12
		leftTrackWheelModels[64].setRotationPoint(-16.5F, -2F, 29F);

		leftTrackWheelModels[65].addShapeBox(6F, -4F, -5F, 5, 8, 1, 0F,-1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 12
		leftTrackWheelModels[65].setRotationPoint(-16.5F, -2F, 29F);

		leftTrackWheelModels[66].addShapeBox(4F, -11F, -5F, 7, 7, 1, 0F,0F, -4F, 0F, -7F, -1F, 0F, -7F, 0F, 0F, 0F, -5F, 0F, -3F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F); // Box 12
		leftTrackWheelModels[66].setRotationPoint(-16.5F, -2F, 29F);

		leftTrackWheelModels[67].addShapeBox(1F, -4F, 5F, 6, 8, 1, 0F,0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F); // Box 12
		leftTrackWheelModels[67].setRotationPoint(-16.5F, -2F, 29F);

		leftTrackWheelModels[68].addShapeBox(-7F, -4F, 4F, 6, 8, 1, 0F,0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F); // Box 12
		leftTrackWheelModels[68].setRotationPoint(-16.5F, -2F, 29F);

		leftTrackWheelModels[69].addBox(-1F, -7F, 4F, 2, 14, 2, 0F); // Box 12
		leftTrackWheelModels[69].setRotationPoint(-16.5F, -2F, 29F);

		leftTrackWheelModels[70].addBox(-2F, -3.5F, 4.2F, 1, 1, 1, 0F); // Box 12
		leftTrackWheelModels[70].setRotationPoint(-16.5F, -2F, 29F);

		leftTrackWheelModels[71].addBox(-2F, 2.5F, 4.2F, 1, 1, 1, 0F); // Box 12
		leftTrackWheelModels[71].setRotationPoint(-16.5F, -2F, 29F);

		leftTrackWheelModels[72].addBox(5F, 2.5F, 5.2F, 1, 1, 1, 0F); // Box 12
		leftTrackWheelModels[72].setRotationPoint(-16.5F, -2F, 29F);

		leftTrackWheelModels[73].addBox(5F, -3.5F, 5.2F, 1, 1, 1, 0F); // Box 12
		leftTrackWheelModels[73].setRotationPoint(-16.5F, -2F, 29F);

		leftTrackWheelModels[74].addBox(-3F, -3.5F, 4.2F, 1, 7, 1, 0F); // Box 12
		leftTrackWheelModels[74].setRotationPoint(-16.5F, -2F, 29F);

		leftTrackWheelModels[75].addShape3D(11F, -11F, -4F, new Shape2D(new Coord2D[] { new Coord2D(7, 0, 7, 0), new Coord2D(15, 0, 15, 0), new Coord2D(22, 7, 22, 7), new Coord2D(22, 15, 22, 15), new Coord2D(15, 22, 15, 22), new Coord2D(7, 22, 7, 22), new Coord2D(0, 15, 0, 15), new Coord2D(0, 7, 0, 7) }), 8, 22, 22, 72, 8, ModelRendererTurbo.MR_FRONT, new float[] {10 ,8 ,10 ,8 ,10 ,8 ,10 ,8}); // Box 12
		leftTrackWheelModels[75].setRotationPoint(-48.5F, -2F, 29F);

		leftTrackWheelModels[76].addShapeBox(-4F, -11F, 4F, 8, 5, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F); // Box 12
		leftTrackWheelModels[76].setRotationPoint(-48.5F, -2F, 29F);

		leftTrackWheelModels[77].addShapeBox(-4F, 6F, 4F, 8, 5, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F); // Box 12
		leftTrackWheelModels[77].setRotationPoint(-48.5F, -2F, 29F);

		leftTrackWheelModels[78].addShapeBox(-11F, -4F, 4F, 5, 8, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 12
		leftTrackWheelModels[78].setRotationPoint(-48.5F, -2F, 29F);

		leftTrackWheelModels[79].addShapeBox(6F, -4F, 4F, 5, 8, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 12
		leftTrackWheelModels[79].setRotationPoint(-48.5F, -2F, 29F);

		leftTrackWheelModels[80].addShapeBox(-11F, -11F, 4F, 7, 7, 1, 0F,-7F, 0F, 0F, 0F, -5F, 0F, 0F, -4F, 0F, -7F, -1F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -3F, 0F, 0F, -1F, 0F, 0F); // Box 12
		leftTrackWheelModels[80].setRotationPoint(-48.5F, -2F, 29F);

		leftTrackWheelModels[81].addShapeBox(-11F, 4F, 4F, 7, 7, 1, 0F,0F, 0F, 0F, -2F, 0F, 0F, -3F, 0F, 0F, -1F, 0F, 0F, -7F, 0F, 0F, 0F, -5F, 0F, 0F, -4F, 0F, -7F, -1F, 0F); // Box 12
		leftTrackWheelModels[81].setRotationPoint(-48.5F, -2F, 29F);

		leftTrackWheelModels[82].addShapeBox(4F, 4F, 4F, 7, 7, 1, 0F,-2F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -3F, 0F, 0F, 0F, -5F, 0F, -7F, 0F, 0F, -7F, -1F, 0F, 0F, -4F, 0F); // Box 12
		leftTrackWheelModels[82].setRotationPoint(-48.5F, -2F, 29F);

		leftTrackWheelModels[83].addShapeBox(4F, -11F, 4F, 7, 7, 1, 0F,0F, -5F, 0F, -7F, 0F, 0F, -7F, -1F, 0F, 0F, -4F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -3F, 0F, 0F); // Box 12
		leftTrackWheelModels[83].setRotationPoint(-48.5F, -2F, 29F);

		leftTrackWheelModels[84].addShapeBox(-4F, -11F, -5F, 8, 5, 1, 0F,0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 12
		leftTrackWheelModels[84].setRotationPoint(-48.5F, -2F, 29F);

		leftTrackWheelModels[85].addShapeBox(-11F, -11F, -5F, 7, 7, 1, 0F,-7F, -1F, 0F, 0F, -4F, 0F, 0F, -5F, 0F, -7F, 0F, 0F, -1F, 0F, 0F, -3F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F); // Box 12
		leftTrackWheelModels[85].setRotationPoint(-48.5F, -2F, 29F);

		leftTrackWheelModels[86].addShapeBox(-11F, -4F, -5F, 5, 8, 1, 0F,-1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 12
		leftTrackWheelModels[86].setRotationPoint(-48.5F, -2F, 29F);

		leftTrackWheelModels[87].addShapeBox(-11F, 4F, -5F, 7, 7, 1, 0F,-1F, 0F, 0F, -3F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, -7F, -1F, 0F, 0F, -4F, 0F, 0F, -5F, 0F, -7F, 0F, 0F); // Box 12
		leftTrackWheelModels[87].setRotationPoint(-48.5F, -2F, 29F);

		leftTrackWheelModels[88].addShapeBox(-4F, 6F, -5F, 8, 5, 1, 0F,0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 12
		leftTrackWheelModels[88].setRotationPoint(-48.5F, -2F, 29F);

		leftTrackWheelModels[89].addShapeBox(4F, 4F, -5F, 7, 7, 1, 0F,-3F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, -4F, 0F, -7F, -1F, 0F, -7F, 0F, 0F, 0F, -5F, 0F); // Box 12
		leftTrackWheelModels[89].setRotationPoint(-48.5F, -2F, 29F);

		leftTrackWheelModels[90].addShapeBox(6F, -4F, -5F, 5, 8, 1, 0F,-1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 12
		leftTrackWheelModels[90].setRotationPoint(-48.5F, -2F, 29F);

		leftTrackWheelModels[91].addShapeBox(4F, -11F, -5F, 7, 7, 1, 0F,0F, -4F, 0F, -7F, -1F, 0F, -7F, 0F, 0F, 0F, -5F, 0F, -3F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F); // Box 12
		leftTrackWheelModels[91].setRotationPoint(-48.5F, -2F, 29F);

		leftTrackWheelModels[92].addShapeBox(1F, -4F, 5F, 6, 8, 1, 0F,0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F); // Box 12
		leftTrackWheelModels[92].setRotationPoint(-48.5F, -2F, 29F);

		leftTrackWheelModels[93].addShapeBox(-7F, -4F, 4F, 6, 8, 1, 0F,0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F); // Box 12
		leftTrackWheelModels[93].setRotationPoint(-48.5F, -2F, 29F);

		leftTrackWheelModels[94].addBox(-1F, -7F, 4F, 2, 14, 2, 0F); // Box 12
		leftTrackWheelModels[94].setRotationPoint(-48.5F, -2F, 29F);

		leftTrackWheelModels[95].addBox(-2F, -3.5F, 4.2F, 1, 1, 1, 0F); // Box 12
		leftTrackWheelModels[95].setRotationPoint(-48.5F, -2F, 29F);

		leftTrackWheelModels[96].addBox(-2F, 2.5F, 4.2F, 1, 1, 1, 0F); // Box 12
		leftTrackWheelModels[96].setRotationPoint(-48.5F, -2F, 29F);

		leftTrackWheelModels[97].addBox(5F, 2.5F, 5.2F, 1, 1, 1, 0F); // Box 12
		leftTrackWheelModels[97].setRotationPoint(-48.5F, -2F, 29F);

		leftTrackWheelModels[98].addBox(5F, -3.5F, 5.2F, 1, 1, 1, 0F); // Box 12
		leftTrackWheelModels[98].setRotationPoint(-48.5F, -2F, 29F);

		leftTrackWheelModels[99].addBox(-3F, -3.5F, 4.2F, 1, 7, 1, 0F); // Box 12
		leftTrackWheelModels[99].setRotationPoint(-48.5F, -2F, 29F);
	}

	private void initrightTrackWheelModels_1()
	{
		rightTrackWheelModels[0] = new ModelRendererTurbo(this, 80, 61, textureX, textureY); // Shape 168
		rightTrackWheelModels[1] = new ModelRendererTurbo(this, 80, 51, textureX, textureY); // Shape 168
		rightTrackWheelModels[2] = new ModelRendererTurbo(this, 95, 51, textureX, textureY); // Shape 168
		rightTrackWheelModels[3] = new ModelRendererTurbo(this, 104, 60, textureX, textureY); // Shape 168
		rightTrackWheelModels[4] = new ModelRendererTurbo(this, 104, 60, textureX, textureY); // Shape 168
		rightTrackWheelModels[5] = new ModelRendererTurbo(this, 95, 68, textureX, textureY); // Shape 168
		rightTrackWheelModels[6] = new ModelRendererTurbo(this, 95, 68, textureX, textureY); // Shape 168
		rightTrackWheelModels[7] = new ModelRendererTurbo(this, 104, 51, textureX, textureY); // Shape 168
		rightTrackWheelModels[8] = new ModelRendererTurbo(this, 80, 61, textureX, textureY); // Shape 168
		rightTrackWheelModels[9] = new ModelRendererTurbo(this, 80, 51, textureX, textureY); // Shape 168
		rightTrackWheelModels[10] = new ModelRendererTurbo(this, 95, 51, textureX, textureY); // Shape 168
		rightTrackWheelModels[11] = new ModelRendererTurbo(this, 104, 60, textureX, textureY); // Shape 168
		rightTrackWheelModels[12] = new ModelRendererTurbo(this, 104, 60, textureX, textureY); // Shape 168
		rightTrackWheelModels[13] = new ModelRendererTurbo(this, 95, 68, textureX, textureY); // Shape 168
		rightTrackWheelModels[14] = new ModelRendererTurbo(this, 95, 68, textureX, textureY); // Shape 168
		rightTrackWheelModels[15] = new ModelRendererTurbo(this, 104, 51, textureX, textureY); // Shape 168
		rightTrackWheelModels[16] = new ModelRendererTurbo(this, 80, 61, textureX, textureY); // Shape 168
		rightTrackWheelModels[17] = new ModelRendererTurbo(this, 80, 51, textureX, textureY); // Shape 168
		rightTrackWheelModels[18] = new ModelRendererTurbo(this, 95, 51, textureX, textureY); // Shape 168
		rightTrackWheelModels[19] = new ModelRendererTurbo(this, 104, 60, textureX, textureY); // Shape 168
		rightTrackWheelModels[20] = new ModelRendererTurbo(this, 104, 60, textureX, textureY); // Shape 168
		rightTrackWheelModels[21] = new ModelRendererTurbo(this, 95, 68, textureX, textureY); // Shape 168
		rightTrackWheelModels[22] = new ModelRendererTurbo(this, 95, 68, textureX, textureY); // Shape 168
		rightTrackWheelModels[23] = new ModelRendererTurbo(this, 104, 51, textureX, textureY); // Shape 168
		rightTrackWheelModels[24] = new ModelRendererTurbo(this, 80, 61, textureX, textureY); // Shape 168
		rightTrackWheelModels[25] = new ModelRendererTurbo(this, 80, 51, textureX, textureY); // Shape 168
		rightTrackWheelModels[26] = new ModelRendererTurbo(this, 95, 51, textureX, textureY); // Shape 168
		rightTrackWheelModels[27] = new ModelRendererTurbo(this, 104, 60, textureX, textureY); // Shape 168
		rightTrackWheelModels[28] = new ModelRendererTurbo(this, 104, 60, textureX, textureY); // Shape 168
		rightTrackWheelModels[29] = new ModelRendererTurbo(this, 95, 68, textureX, textureY); // Shape 168
		rightTrackWheelModels[30] = new ModelRendererTurbo(this, 95, 68, textureX, textureY); // Shape 168
		rightTrackWheelModels[31] = new ModelRendererTurbo(this, 104, 51, textureX, textureY); // Shape 168
		rightTrackWheelModels[32] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Shape 268
		rightTrackWheelModels[33] = new ModelRendererTurbo(this, 59, 8, textureX, textureY); // Box 269
		rightTrackWheelModels[34] = new ModelRendererTurbo(this, 59, 1, textureX, textureY); // Box 270
		rightTrackWheelModels[35] = new ModelRendererTurbo(this, 46, 11, textureX, textureY); // Box 271
		rightTrackWheelModels[36] = new ModelRendererTurbo(this, 46, 1, textureX, textureY); // Box 272
		rightTrackWheelModels[37] = new ModelRendererTurbo(this, 1, 32, textureX, textureY); // Box 273
		rightTrackWheelModels[38] = new ModelRendererTurbo(this, 35, 32, textureX, textureY); // Box 274
		rightTrackWheelModels[39] = new ModelRendererTurbo(this, 18, 32, textureX, textureY); // Box 275
		rightTrackWheelModels[40] = new ModelRendererTurbo(this, 52, 32, textureX, textureY); // Box 276
		rightTrackWheelModels[41] = new ModelRendererTurbo(this, 59, 8, textureX, textureY); // Box 277
		rightTrackWheelModels[42] = new ModelRendererTurbo(this, 1, 32, textureX, textureY); // Box 278
		rightTrackWheelModels[43] = new ModelRendererTurbo(this, 46, 11, textureX, textureY); // Box 279
		rightTrackWheelModels[44] = new ModelRendererTurbo(this, 35, 32, textureX, textureY); // Box 280
		rightTrackWheelModels[45] = new ModelRendererTurbo(this, 59, 1, textureX, textureY); // Box 281
		rightTrackWheelModels[46] = new ModelRendererTurbo(this, 18, 32, textureX, textureY); // Box 282
		rightTrackWheelModels[47] = new ModelRendererTurbo(this, 46, 1, textureX, textureY); // Box 283
		rightTrackWheelModels[48] = new ModelRendererTurbo(this, 52, 32, textureX, textureY); // Box 284
		rightTrackWheelModels[49] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Shape 285
		rightTrackWheelModels[50] = new ModelRendererTurbo(this, 59, 8, textureX, textureY); // Box 286
		rightTrackWheelModels[51] = new ModelRendererTurbo(this, 59, 1, textureX, textureY); // Box 287
		rightTrackWheelModels[52] = new ModelRendererTurbo(this, 46, 11, textureX, textureY); // Box 288
		rightTrackWheelModels[53] = new ModelRendererTurbo(this, 46, 1, textureX, textureY); // Box 289
		rightTrackWheelModels[54] = new ModelRendererTurbo(this, 1, 32, textureX, textureY); // Box 290
		rightTrackWheelModels[55] = new ModelRendererTurbo(this, 35, 32, textureX, textureY); // Box 291
		rightTrackWheelModels[56] = new ModelRendererTurbo(this, 18, 32, textureX, textureY); // Box 292
		rightTrackWheelModels[57] = new ModelRendererTurbo(this, 52, 32, textureX, textureY); // Box 293
		rightTrackWheelModels[58] = new ModelRendererTurbo(this, 59, 8, textureX, textureY); // Box 294
		rightTrackWheelModels[59] = new ModelRendererTurbo(this, 1, 32, textureX, textureY); // Box 295
		rightTrackWheelModels[60] = new ModelRendererTurbo(this, 46, 11, textureX, textureY); // Box 296
		rightTrackWheelModels[61] = new ModelRendererTurbo(this, 35, 32, textureX, textureY); // Box 297
		rightTrackWheelModels[62] = new ModelRendererTurbo(this, 59, 1, textureX, textureY); // Box 298
		rightTrackWheelModels[63] = new ModelRendererTurbo(this, 18, 32, textureX, textureY); // Box 299
		rightTrackWheelModels[64] = new ModelRendererTurbo(this, 46, 1, textureX, textureY); // Box 300
		rightTrackWheelModels[65] = new ModelRendererTurbo(this, 52, 32, textureX, textureY); // Box 301
		rightTrackWheelModels[66] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Shape 302
		rightTrackWheelModels[67] = new ModelRendererTurbo(this, 59, 8, textureX, textureY); // Box 303
		rightTrackWheelModels[68] = new ModelRendererTurbo(this, 59, 1, textureX, textureY); // Box 304
		rightTrackWheelModels[69] = new ModelRendererTurbo(this, 46, 11, textureX, textureY); // Box 305
		rightTrackWheelModels[70] = new ModelRendererTurbo(this, 46, 1, textureX, textureY); // Box 306
		rightTrackWheelModels[71] = new ModelRendererTurbo(this, 1, 32, textureX, textureY); // Box 307
		rightTrackWheelModels[72] = new ModelRendererTurbo(this, 35, 32, textureX, textureY); // Box 308
		rightTrackWheelModels[73] = new ModelRendererTurbo(this, 18, 32, textureX, textureY); // Box 309
		rightTrackWheelModels[74] = new ModelRendererTurbo(this, 52, 32, textureX, textureY); // Box 310
		rightTrackWheelModels[75] = new ModelRendererTurbo(this, 59, 8, textureX, textureY); // Box 311
		rightTrackWheelModels[76] = new ModelRendererTurbo(this, 1, 32, textureX, textureY); // Box 312
		rightTrackWheelModels[77] = new ModelRendererTurbo(this, 46, 11, textureX, textureY); // Box 313
		rightTrackWheelModels[78] = new ModelRendererTurbo(this, 35, 32, textureX, textureY); // Box 314
		rightTrackWheelModels[79] = new ModelRendererTurbo(this, 59, 1, textureX, textureY); // Box 315
		rightTrackWheelModels[80] = new ModelRendererTurbo(this, 18, 32, textureX, textureY); // Box 316
		rightTrackWheelModels[81] = new ModelRendererTurbo(this, 46, 1, textureX, textureY); // Box 317
		rightTrackWheelModels[82] = new ModelRendererTurbo(this, 52, 32, textureX, textureY); // Box 318
		rightTrackWheelModels[83] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Shape 319
		rightTrackWheelModels[84] = new ModelRendererTurbo(this, 59, 8, textureX, textureY); // Box 320
		rightTrackWheelModels[85] = new ModelRendererTurbo(this, 59, 1, textureX, textureY); // Box 321
		rightTrackWheelModels[86] = new ModelRendererTurbo(this, 46, 11, textureX, textureY); // Box 322
		rightTrackWheelModels[87] = new ModelRendererTurbo(this, 46, 1, textureX, textureY); // Box 323
		rightTrackWheelModels[88] = new ModelRendererTurbo(this, 1, 32, textureX, textureY); // Box 324
		rightTrackWheelModels[89] = new ModelRendererTurbo(this, 35, 32, textureX, textureY); // Box 325
		rightTrackWheelModels[90] = new ModelRendererTurbo(this, 18, 32, textureX, textureY); // Box 326
		rightTrackWheelModels[91] = new ModelRendererTurbo(this, 52, 32, textureX, textureY); // Box 327
		rightTrackWheelModels[92] = new ModelRendererTurbo(this, 59, 8, textureX, textureY); // Box 328
		rightTrackWheelModels[93] = new ModelRendererTurbo(this, 1, 32, textureX, textureY); // Box 329
		rightTrackWheelModels[94] = new ModelRendererTurbo(this, 46, 11, textureX, textureY); // Box 330
		rightTrackWheelModels[95] = new ModelRendererTurbo(this, 35, 32, textureX, textureY); // Box 331
		rightTrackWheelModels[96] = new ModelRendererTurbo(this, 59, 1, textureX, textureY); // Box 332
		rightTrackWheelModels[97] = new ModelRendererTurbo(this, 18, 32, textureX, textureY); // Box 333
		rightTrackWheelModels[98] = new ModelRendererTurbo(this, 46, 1, textureX, textureY); // Box 334
		rightTrackWheelModels[99] = new ModelRendererTurbo(this, 52, 32, textureX, textureY); // Box 335

		rightTrackWheelModels[0].addShapeBox(-7F, -4F, -6F, 6, 8, 1, 0F,0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F); // Shape 168
		rightTrackWheelModels[0].setRotationPoint(-48.5F, -2F, -29F);

		rightTrackWheelModels[1].addShapeBox(1F, -4F, -5F, 6, 8, 1, 0F,0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F); // Shape 168
		rightTrackWheelModels[1].setRotationPoint(-48.5F, -2F, -29F);

		rightTrackWheelModels[2].addBox(-1F, -7F, -6F, 2, 14, 2, 0F); // Shape 168
		rightTrackWheelModels[2].setRotationPoint(-48.5F, -2F, -29F);

		rightTrackWheelModels[3].addBox(1F, -3.5F, -5.2F, 1, 1, 1, 0F); // Shape 168
		rightTrackWheelModels[3].setRotationPoint(-48.5F, -2F, -29F);

		rightTrackWheelModels[4].addBox(1F, 2.5F, -5.2F, 1, 1, 1, 0F); // Shape 168
		rightTrackWheelModels[4].setRotationPoint(-48.5F, -2F, -29F);

		rightTrackWheelModels[5].addBox(-5F, 2.5F, -5.2F, 1, 1, 1, 0F); // Shape 168
		rightTrackWheelModels[5].setRotationPoint(-49.5F, -2F, -30F);

		rightTrackWheelModels[6].addBox(-5F, -3.5F, -5.2F, 1, 1, 1, 0F); // Shape 168
		rightTrackWheelModels[6].setRotationPoint(-49.5F, -2F, -30F);

		rightTrackWheelModels[7].addBox(2F, -3.5F, -5.2F, 1, 7, 1, 0F); // Shape 168
		rightTrackWheelModels[7].setRotationPoint(-48.5F, -2F, -29F);

		rightTrackWheelModels[8].addShapeBox(-7F, -4F, -4F, 6, 8, 1, 0F,0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F); // Shape 168
		rightTrackWheelModels[8].setRotationPoint(-16.5F, -2F, -31F);

		rightTrackWheelModels[9].addShapeBox(1F, -4F, -5F, 6, 8, 1, 0F,0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F); // Shape 168
		rightTrackWheelModels[9].setRotationPoint(-16.5F, -2F, -29F);

		rightTrackWheelModels[10].addBox(-1F, -7F, -6F, 2, 14, 2, 0F); // Shape 168
		rightTrackWheelModels[10].setRotationPoint(-16.5F, -2F, -29F);

		rightTrackWheelModels[11].addBox(1F, -3.5F, -5.2F, 1, 1, 1, 0F); // Shape 168
		rightTrackWheelModels[11].setRotationPoint(-16.5F, -2F, -29F);

		rightTrackWheelModels[12].addBox(1F, 2.5F, -5.2F, 1, 1, 1, 0F); // Shape 168
		rightTrackWheelModels[12].setRotationPoint(-16.5F, -2F, -29F);

		rightTrackWheelModels[13].addBox(-5F, 2.5F, -5.2F, 1, 1, 1, 0F); // Shape 168
		rightTrackWheelModels[13].setRotationPoint(-17.5F, -2F, -30F);

		rightTrackWheelModels[14].addBox(-5F, -3.5F, -5.2F, 1, 1, 1, 0F); // Shape 168
		rightTrackWheelModels[14].setRotationPoint(-17.5F, -2F, -30F);

		rightTrackWheelModels[15].addBox(2F, -3.5F, -5.2F, 1, 7, 1, 0F); // Shape 168
		rightTrackWheelModels[15].setRotationPoint(-16.5F, -2F, -29F);

		rightTrackWheelModels[16].addShapeBox(-7F, -4F, -6F, 6, 8, 1, 0F,0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F); // Shape 168
		rightTrackWheelModels[16].setRotationPoint(16.5F, -2F, -29F);

		rightTrackWheelModels[17].addShapeBox(1F, -4F, -5F, 6, 8, 1, 0F,0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F); // Shape 168
		rightTrackWheelModels[17].setRotationPoint(16.5F, -2F, -29F);

		rightTrackWheelModels[18].addBox(-1F, -7F, -6F, 2, 14, 2, 0F); // Shape 168
		rightTrackWheelModels[18].setRotationPoint(16.5F, -2F, -29F);

		rightTrackWheelModels[19].addBox(1F, -3.5F, -5.2F, 1, 1, 1, 0F); // Shape 168
		rightTrackWheelModels[19].setRotationPoint(16.5F, -2F, -29F);

		rightTrackWheelModels[20].addBox(1F, 2.5F, -5.2F, 1, 1, 1, 0F); // Shape 168
		rightTrackWheelModels[20].setRotationPoint(16.5F, -2F, -29F);

		rightTrackWheelModels[21].addBox(-5F, 2.5F, -5.2F, 1, 1, 1, 0F); // Shape 168
		rightTrackWheelModels[21].setRotationPoint(15.5F, -2F, -30F);

		rightTrackWheelModels[22].addBox(-5F, -3.5F, -5.2F, 1, 1, 1, 0F); // Shape 168
		rightTrackWheelModels[22].setRotationPoint(15.5F, -2F, -30F);

		rightTrackWheelModels[23].addBox(2F, -3.5F, -5.2F, 1, 7, 1, 0F); // Shape 168
		rightTrackWheelModels[23].setRotationPoint(16.5F, -2F, -29F);

		rightTrackWheelModels[24].addShapeBox(-7F, -4F, -6F, 6, 8, 1, 0F,0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F); // Shape 168
		rightTrackWheelModels[24].setRotationPoint(48.5F, -2F, -29F);

		rightTrackWheelModels[25].addShapeBox(1F, -4F, -5F, 6, 8, 1, 0F,0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F); // Shape 168
		rightTrackWheelModels[25].setRotationPoint(48.5F, -2F, -29F);

		rightTrackWheelModels[26].addBox(-1F, -7F, -6F, 2, 14, 2, 0F); // Shape 168
		rightTrackWheelModels[26].setRotationPoint(48.5F, -2F, -29F);

		rightTrackWheelModels[27].addBox(1F, -3.5F, -5.2F, 1, 1, 1, 0F); // Shape 168
		rightTrackWheelModels[27].setRotationPoint(48.5F, -2F, -29F);

		rightTrackWheelModels[28].addBox(1F, 2.5F, -5.2F, 1, 1, 1, 0F); // Shape 168
		rightTrackWheelModels[28].setRotationPoint(48.5F, -2F, -29F);

		rightTrackWheelModels[29].addBox(-5F, 2.5F, -5.2F, 1, 1, 1, 0F); // Shape 168
		rightTrackWheelModels[29].setRotationPoint(47.5F, -2F, -30F);

		rightTrackWheelModels[30].addBox(-5F, -3.5F, -5.2F, 1, 1, 1, 0F); // Shape 168
		rightTrackWheelModels[30].setRotationPoint(47.5F, -2F, -30F);

		rightTrackWheelModels[31].addBox(2F, -3.5F, -5.2F, 1, 7, 1, 0F); // Shape 168
		rightTrackWheelModels[31].setRotationPoint(48.5F, -2F, -29F);

		rightTrackWheelModels[32].addShape3D(11F, -11F, -4F, new Shape2D(new Coord2D[] { new Coord2D(7, 0, 7, 0), new Coord2D(15, 0, 15, 0), new Coord2D(22, 7, 22, 7), new Coord2D(22, 15, 22, 15), new Coord2D(15, 22, 15, 22), new Coord2D(7, 22, 7, 22), new Coord2D(0, 15, 0, 15), new Coord2D(0, 7, 0, 7) }), 8, 22, 22, 72, 8, ModelRendererTurbo.MR_FRONT, new float[] {10 ,8 ,10 ,8 ,10 ,8 ,10 ,8}); // Shape 268
		rightTrackWheelModels[32].setRotationPoint(48.5F, -2F, -29F);

		rightTrackWheelModels[33].addShapeBox(-4F, -11F, 4F, 8, 5, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F); // Box 269
		rightTrackWheelModels[33].setRotationPoint(48.5F, -2F, -29F);

		rightTrackWheelModels[34].addShapeBox(-4F, 6F, 4F, 8, 5, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F); // Box 270
		rightTrackWheelModels[34].setRotationPoint(48.5F, -2F, -29F);

		rightTrackWheelModels[35].addShapeBox(-11F, -4F, 4F, 5, 8, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 271
		rightTrackWheelModels[35].setRotationPoint(48.5F, -2F, -29F);

		rightTrackWheelModels[36].addShapeBox(6F, -4F, 4F, 5, 8, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 272
		rightTrackWheelModels[36].setRotationPoint(48.5F, -2F, -29F);

		rightTrackWheelModels[37].addShapeBox(-11F, -11F, 4F, 7, 7, 1, 0F,-7F, 0F, 0F, 0F, -5F, 0F, 0F, -4F, 0F, -7F, -1F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -3F, 0F, 0F, -1F, 0F, 0F); // Box 273
		rightTrackWheelModels[37].setRotationPoint(48.5F, -2F, -29F);

		rightTrackWheelModels[38].addShapeBox(-11F, 4F, 4F, 7, 7, 1, 0F,0F, 0F, 0F, -2F, 0F, 0F, -3F, 0F, 0F, -1F, 0F, 0F, -7F, 0F, 0F, 0F, -5F, 0F, 0F, -4F, 0F, -7F, -1F, 0F); // Box 274
		rightTrackWheelModels[38].setRotationPoint(48.5F, -2F, -29F);

		rightTrackWheelModels[39].addShapeBox(4F, 4F, 4F, 7, 7, 1, 0F,-2F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -3F, 0F, 0F, 0F, -5F, 0F, -7F, 0F, 0F, -7F, -1F, 0F, 0F, -4F, 0F); // Box 275
		rightTrackWheelModels[39].setRotationPoint(48.5F, -2F, -29F);

		rightTrackWheelModels[40].addShapeBox(4F, -11F, 4F, 7, 7, 1, 0F,0F, -5F, 0F, -7F, 0F, 0F, -7F, -1F, 0F, 0F, -4F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -3F, 0F, 0F); // Box 276
		rightTrackWheelModels[40].setRotationPoint(48.5F, -2F, -29F);

		rightTrackWheelModels[41].addShapeBox(-4F, -11F, -5F, 8, 5, 1, 0F,0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 277
		rightTrackWheelModels[41].setRotationPoint(48.5F, -2F, -29F);

		rightTrackWheelModels[42].addShapeBox(-11F, -11F, -5F, 7, 7, 1, 0F,-7F, -1F, 0F, 0F, -4F, 0F, 0F, -5F, 0F, -7F, 0F, 0F, -1F, 0F, 0F, -3F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F); // Box 278
		rightTrackWheelModels[42].setRotationPoint(48.5F, -2F, -29F);

		rightTrackWheelModels[43].addShapeBox(-11F, -4F, -5F, 5, 8, 1, 0F,-1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 279
		rightTrackWheelModels[43].setRotationPoint(48.5F, -2F, -29F);

		rightTrackWheelModels[44].addShapeBox(-11F, 4F, -5F, 7, 7, 1, 0F,-1F, 0F, 0F, -3F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, -7F, -1F, 0F, 0F, -4F, 0F, 0F, -5F, 0F, -7F, 0F, 0F); // Box 280
		rightTrackWheelModels[44].setRotationPoint(48.5F, -2F, -29F);

		rightTrackWheelModels[45].addShapeBox(-4F, 6F, -5F, 8, 5, 1, 0F,0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 281
		rightTrackWheelModels[45].setRotationPoint(48.5F, -2F, -29F);

		rightTrackWheelModels[46].addShapeBox(4F, 4F, -5F, 7, 7, 1, 0F,-3F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, -4F, 0F, -7F, -1F, 0F, -7F, 0F, 0F, 0F, -5F, 0F); // Box 282
		rightTrackWheelModels[46].setRotationPoint(48.5F, -2F, -29F);

		rightTrackWheelModels[47].addShapeBox(6F, -4F, -5F, 5, 8, 1, 0F,-1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 283
		rightTrackWheelModels[47].setRotationPoint(48.5F, -2F, -29F);

		rightTrackWheelModels[48].addShapeBox(4F, -11F, -5F, 7, 7, 1, 0F,0F, -4F, 0F, -7F, -1F, 0F, -7F, 0F, 0F, 0F, -5F, 0F, -3F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F); // Box 284
		rightTrackWheelModels[48].setRotationPoint(48.5F, -2F, -29F);

		rightTrackWheelModels[49].addShape3D(11F, -11F, -4F, new Shape2D(new Coord2D[] { new Coord2D(7, 0, 7, 0), new Coord2D(15, 0, 15, 0), new Coord2D(22, 7, 22, 7), new Coord2D(22, 15, 22, 15), new Coord2D(15, 22, 15, 22), new Coord2D(7, 22, 7, 22), new Coord2D(0, 15, 0, 15), new Coord2D(0, 7, 0, 7) }), 8, 22, 22, 72, 8, ModelRendererTurbo.MR_FRONT, new float[] {10 ,8 ,10 ,8 ,10 ,8 ,10 ,8}); // Shape 285
		rightTrackWheelModels[49].setRotationPoint(16.5F, -2F, -29F);

		rightTrackWheelModels[50].addShapeBox(-4F, -11F, 4F, 8, 5, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F); // Box 286
		rightTrackWheelModels[50].setRotationPoint(16.5F, -2F, -29F);

		rightTrackWheelModels[51].addShapeBox(-4F, 6F, 4F, 8, 5, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F); // Box 287
		rightTrackWheelModels[51].setRotationPoint(16.5F, -2F, -29F);

		rightTrackWheelModels[52].addShapeBox(-11F, -4F, 4F, 5, 8, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 288
		rightTrackWheelModels[52].setRotationPoint(16.5F, -2F, -29F);

		rightTrackWheelModels[53].addShapeBox(6F, -4F, 4F, 5, 8, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 289
		rightTrackWheelModels[53].setRotationPoint(16.5F, -2F, -29F);

		rightTrackWheelModels[54].addShapeBox(-11F, -11F, 4F, 7, 7, 1, 0F,-7F, 0F, 0F, 0F, -5F, 0F, 0F, -4F, 0F, -7F, -1F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -3F, 0F, 0F, -1F, 0F, 0F); // Box 290
		rightTrackWheelModels[54].setRotationPoint(16.5F, -2F, -29F);

		rightTrackWheelModels[55].addShapeBox(-11F, 4F, 4F, 7, 7, 1, 0F,0F, 0F, 0F, -2F, 0F, 0F, -3F, 0F, 0F, -1F, 0F, 0F, -7F, 0F, 0F, 0F, -5F, 0F, 0F, -4F, 0F, -7F, -1F, 0F); // Box 291
		rightTrackWheelModels[55].setRotationPoint(16.5F, -2F, -29F);

		rightTrackWheelModels[56].addShapeBox(4F, 4F, 4F, 7, 7, 1, 0F,-2F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -3F, 0F, 0F, 0F, -5F, 0F, -7F, 0F, 0F, -7F, -1F, 0F, 0F, -4F, 0F); // Box 292
		rightTrackWheelModels[56].setRotationPoint(16.5F, -2F, -29F);

		rightTrackWheelModels[57].addShapeBox(4F, -11F, 4F, 7, 7, 1, 0F,0F, -5F, 0F, -7F, 0F, 0F, -7F, -1F, 0F, 0F, -4F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -3F, 0F, 0F); // Box 293
		rightTrackWheelModels[57].setRotationPoint(16.5F, -2F, -29F);

		rightTrackWheelModels[58].addShapeBox(-4F, -11F, -5F, 8, 5, 1, 0F,0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 294
		rightTrackWheelModels[58].setRotationPoint(16.5F, -2F, -29F);

		rightTrackWheelModels[59].addShapeBox(-11F, -11F, -5F, 7, 7, 1, 0F,-7F, -1F, 0F, 0F, -4F, 0F, 0F, -5F, 0F, -7F, 0F, 0F, -1F, 0F, 0F, -3F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F); // Box 295
		rightTrackWheelModels[59].setRotationPoint(16.5F, -2F, -29F);

		rightTrackWheelModels[60].addShapeBox(-11F, -4F, -5F, 5, 8, 1, 0F,-1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 296
		rightTrackWheelModels[60].setRotationPoint(16.5F, -2F, -29F);

		rightTrackWheelModels[61].addShapeBox(-11F, 4F, -5F, 7, 7, 1, 0F,-1F, 0F, 0F, -3F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, -7F, -1F, 0F, 0F, -4F, 0F, 0F, -5F, 0F, -7F, 0F, 0F); // Box 297
		rightTrackWheelModels[61].setRotationPoint(16.5F, -2F, -29F);

		rightTrackWheelModels[62].addShapeBox(-4F, 6F, -5F, 8, 5, 1, 0F,0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 298
		rightTrackWheelModels[62].setRotationPoint(16.5F, -2F, -29F);

		rightTrackWheelModels[63].addShapeBox(4F, 4F, -5F, 7, 7, 1, 0F,-3F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, -4F, 0F, -7F, -1F, 0F, -7F, 0F, 0F, 0F, -5F, 0F); // Box 299
		rightTrackWheelModels[63].setRotationPoint(16.5F, -2F, -29F);

		rightTrackWheelModels[64].addShapeBox(6F, -4F, -5F, 5, 8, 1, 0F,-1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 300
		rightTrackWheelModels[64].setRotationPoint(16.5F, -2F, -29F);

		rightTrackWheelModels[65].addShapeBox(4F, -11F, -5F, 7, 7, 1, 0F,0F, -4F, 0F, -7F, -1F, 0F, -7F, 0F, 0F, 0F, -5F, 0F, -3F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F); // Box 301
		rightTrackWheelModels[65].setRotationPoint(16.5F, -2F, -29F);

		rightTrackWheelModels[66].addShape3D(11F, -11F, -4F, new Shape2D(new Coord2D[] { new Coord2D(7, 0, 7, 0), new Coord2D(15, 0, 15, 0), new Coord2D(22, 7, 22, 7), new Coord2D(22, 15, 22, 15), new Coord2D(15, 22, 15, 22), new Coord2D(7, 22, 7, 22), new Coord2D(0, 15, 0, 15), new Coord2D(0, 7, 0, 7) }), 8, 22, 22, 72, 8, ModelRendererTurbo.MR_FRONT, new float[] {10 ,8 ,10 ,8 ,10 ,8 ,10 ,8}); // Shape 302
		rightTrackWheelModels[66].setRotationPoint(-16.5F, -2F, -29F);

		rightTrackWheelModels[67].addShapeBox(-4F, -11F, 4F, 8, 5, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F); // Box 303
		rightTrackWheelModels[67].setRotationPoint(-16.5F, -2F, -29F);

		rightTrackWheelModels[68].addShapeBox(-4F, 6F, 4F, 8, 5, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F); // Box 304
		rightTrackWheelModels[68].setRotationPoint(-16.5F, -2F, -29F);

		rightTrackWheelModels[69].addShapeBox(-11F, -4F, 4F, 5, 8, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 305
		rightTrackWheelModels[69].setRotationPoint(-16.5F, -2F, -29F);

		rightTrackWheelModels[70].addShapeBox(6F, -4F, 4F, 5, 8, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 306
		rightTrackWheelModels[70].setRotationPoint(-16.5F, -2F, -29F);

		rightTrackWheelModels[71].addShapeBox(-11F, -11F, 4F, 7, 7, 1, 0F,-7F, 0F, 0F, 0F, -5F, 0F, 0F, -4F, 0F, -7F, -1F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -3F, 0F, 0F, -1F, 0F, 0F); // Box 307
		rightTrackWheelModels[71].setRotationPoint(-16.5F, -2F, -29F);

		rightTrackWheelModels[72].addShapeBox(-11F, 4F, 4F, 7, 7, 1, 0F,0F, 0F, 0F, -2F, 0F, 0F, -3F, 0F, 0F, -1F, 0F, 0F, -7F, 0F, 0F, 0F, -5F, 0F, 0F, -4F, 0F, -7F, -1F, 0F); // Box 308
		rightTrackWheelModels[72].setRotationPoint(-16.5F, -2F, -29F);

		rightTrackWheelModels[73].addShapeBox(4F, 4F, 4F, 7, 7, 1, 0F,-2F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -3F, 0F, 0F, 0F, -5F, 0F, -7F, 0F, 0F, -7F, -1F, 0F, 0F, -4F, 0F); // Box 309
		rightTrackWheelModels[73].setRotationPoint(-16.5F, -2F, -29F);

		rightTrackWheelModels[74].addShapeBox(4F, -11F, 4F, 7, 7, 1, 0F,0F, -5F, 0F, -7F, 0F, 0F, -7F, -1F, 0F, 0F, -4F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -3F, 0F, 0F); // Box 310
		rightTrackWheelModels[74].setRotationPoint(-16.5F, -2F, -29F);

		rightTrackWheelModels[75].addShapeBox(-4F, -11F, -5F, 8, 5, 1, 0F,0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 311
		rightTrackWheelModels[75].setRotationPoint(-16.5F, -2F, -29F);

		rightTrackWheelModels[76].addShapeBox(-11F, -11F, -5F, 7, 7, 1, 0F,-7F, -1F, 0F, 0F, -4F, 0F, 0F, -5F, 0F, -7F, 0F, 0F, -1F, 0F, 0F, -3F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F); // Box 312
		rightTrackWheelModels[76].setRotationPoint(-16.5F, -2F, -29F);

		rightTrackWheelModels[77].addShapeBox(-11F, -4F, -5F, 5, 8, 1, 0F,-1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 313
		rightTrackWheelModels[77].setRotationPoint(-16.5F, -2F, -29F);

		rightTrackWheelModels[78].addShapeBox(-11F, 4F, -5F, 7, 7, 1, 0F,-1F, 0F, 0F, -3F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, -7F, -1F, 0F, 0F, -4F, 0F, 0F, -5F, 0F, -7F, 0F, 0F); // Box 314
		rightTrackWheelModels[78].setRotationPoint(-16.5F, -2F, -29F);

		rightTrackWheelModels[79].addShapeBox(-4F, 6F, -5F, 8, 5, 1, 0F,0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 315
		rightTrackWheelModels[79].setRotationPoint(-16.5F, -2F, -29F);

		rightTrackWheelModels[80].addShapeBox(4F, 4F, -5F, 7, 7, 1, 0F,-3F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, -4F, 0F, -7F, -1F, 0F, -7F, 0F, 0F, 0F, -5F, 0F); // Box 316
		rightTrackWheelModels[80].setRotationPoint(-16.5F, -2F, -29F);

		rightTrackWheelModels[81].addShapeBox(6F, -4F, -5F, 5, 8, 1, 0F,-1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 317
		rightTrackWheelModels[81].setRotationPoint(-16.5F, -2F, -29F);

		rightTrackWheelModels[82].addShapeBox(4F, -11F, -5F, 7, 7, 1, 0F,0F, -4F, 0F, -7F, -1F, 0F, -7F, 0F, 0F, 0F, -5F, 0F, -3F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F); // Box 318
		rightTrackWheelModels[82].setRotationPoint(-16.5F, -2F, -29F);

		rightTrackWheelModels[83].addShape3D(11F, -11F, -4F, new Shape2D(new Coord2D[] { new Coord2D(7, 0, 7, 0), new Coord2D(15, 0, 15, 0), new Coord2D(22, 7, 22, 7), new Coord2D(22, 15, 22, 15), new Coord2D(15, 22, 15, 22), new Coord2D(7, 22, 7, 22), new Coord2D(0, 15, 0, 15), new Coord2D(0, 7, 0, 7) }), 8, 22, 22, 72, 8, ModelRendererTurbo.MR_FRONT, new float[] {10 ,8 ,10 ,8 ,10 ,8 ,10 ,8}); // Shape 319
		rightTrackWheelModels[83].setRotationPoint(-48.5F, -2F, -29F);

		rightTrackWheelModels[84].addShapeBox(-4F, -11F, 4F, 8, 5, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F); // Box 320
		rightTrackWheelModels[84].setRotationPoint(-48.5F, -2F, -29F);

		rightTrackWheelModels[85].addShapeBox(-4F, 6F, 4F, 8, 5, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F); // Box 321
		rightTrackWheelModels[85].setRotationPoint(-48.5F, -2F, -29F);

		rightTrackWheelModels[86].addShapeBox(-11F, -4F, 4F, 5, 8, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 322
		rightTrackWheelModels[86].setRotationPoint(-48.5F, -2F, -29F);

		rightTrackWheelModels[87].addShapeBox(6F, -4F, 4F, 5, 8, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 323
		rightTrackWheelModels[87].setRotationPoint(-48.5F, -2F, -29F);

		rightTrackWheelModels[88].addShapeBox(-11F, -11F, 4F, 7, 7, 1, 0F,-7F, 0F, 0F, 0F, -5F, 0F, 0F, -4F, 0F, -7F, -1F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -3F, 0F, 0F, -1F, 0F, 0F); // Box 324
		rightTrackWheelModels[88].setRotationPoint(-48.5F, -2F, -29F);

		rightTrackWheelModels[89].addShapeBox(-11F, 4F, 4F, 7, 7, 1, 0F,0F, 0F, 0F, -2F, 0F, 0F, -3F, 0F, 0F, -1F, 0F, 0F, -7F, 0F, 0F, 0F, -5F, 0F, 0F, -4F, 0F, -7F, -1F, 0F); // Box 325
		rightTrackWheelModels[89].setRotationPoint(-48.5F, -2F, -29F);

		rightTrackWheelModels[90].addShapeBox(4F, 4F, 4F, 7, 7, 1, 0F,-2F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -3F, 0F, 0F, 0F, -5F, 0F, -7F, 0F, 0F, -7F, -1F, 0F, 0F, -4F, 0F); // Box 326
		rightTrackWheelModels[90].setRotationPoint(-48.5F, -2F, -29F);

		rightTrackWheelModels[91].addShapeBox(4F, -11F, 4F, 7, 7, 1, 0F,0F, -5F, 0F, -7F, 0F, 0F, -7F, -1F, 0F, 0F, -4F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -3F, 0F, 0F); // Box 327
		rightTrackWheelModels[91].setRotationPoint(-48.5F, -2F, -29F);

		rightTrackWheelModels[92].addShapeBox(-4F, -11F, -5F, 8, 5, 1, 0F,0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 328
		rightTrackWheelModels[92].setRotationPoint(-48.5F, -2F, -29F);

		rightTrackWheelModels[93].addShapeBox(-11F, -11F, -5F, 7, 7, 1, 0F,-7F, -1F, 0F, 0F, -4F, 0F, 0F, -5F, 0F, -7F, 0F, 0F, -1F, 0F, 0F, -3F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F); // Box 329
		rightTrackWheelModels[93].setRotationPoint(-48.5F, -2F, -29F);

		rightTrackWheelModels[94].addShapeBox(-11F, -4F, -5F, 5, 8, 1, 0F,-1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 330
		rightTrackWheelModels[94].setRotationPoint(-48.5F, -2F, -29F);

		rightTrackWheelModels[95].addShapeBox(-11F, 4F, -5F, 7, 7, 1, 0F,-1F, 0F, 0F, -3F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, -7F, -1F, 0F, 0F, -4F, 0F, 0F, -5F, 0F, -7F, 0F, 0F); // Box 331
		rightTrackWheelModels[95].setRotationPoint(-48.5F, -2F, -29F);

		rightTrackWheelModels[96].addShapeBox(-4F, 6F, -5F, 8, 5, 1, 0F,0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 332
		rightTrackWheelModels[96].setRotationPoint(-48.5F, -2F, -29F);

		rightTrackWheelModels[97].addShapeBox(4F, 4F, -5F, 7, 7, 1, 0F,-3F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, -4F, 0F, -7F, -1F, 0F, -7F, 0F, 0F, 0F, -5F, 0F); // Box 333
		rightTrackWheelModels[97].setRotationPoint(-48.5F, -2F, -29F);

		rightTrackWheelModels[98].addShapeBox(6F, -4F, -5F, 5, 8, 1, 0F,-1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 334
		rightTrackWheelModels[98].setRotationPoint(-48.5F, -2F, -29F);

		rightTrackWheelModels[99].addShapeBox(4F, -11F, -5F, 7, 7, 1, 0F,0F, -4F, 0F, -7F, -1F, 0F, -7F, 0F, 0F, 0F, -5F, 0F, -3F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F); // Box 335
		rightTrackWheelModels[99].setRotationPoint(-48.5F, -2F, -29F);
	}
}