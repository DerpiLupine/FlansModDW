package com.flansmod.client.model.dwvehicles; //Path where the model is located

import com.flansmod.client.model.ModelVehicle;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelKattushaMAWMissile extends ModelVehicle //Same as Filename
{
	int textureX = 1024;
	int textureY = 1024;

	public ModelKattushaMAWMissile() //Same as Filename
	{
		bodyModel = new ModelRendererTurbo[236];

		initbodyModel_1();

		initGuns();

		translateAll(0F, 0F, 0F);


		flipAll();
	}

	private void initbodyModel_1()
	{
		bodyModel[0] = new ModelRendererTurbo(this, 136, 66, textureX, textureY); // antenna1
		bodyModel[1] = new ModelRendererTurbo(this, 131, 45, textureX, textureY); // antenna2
		bodyModel[2] = new ModelRendererTurbo(this, 145, 67, textureX, textureY); // antenna3
		bodyModel[3] = new ModelRendererTurbo(this, 44, 33, textureX, textureY); // axleLeftBack1
		bodyModel[4] = new ModelRendererTurbo(this, 44, 33, textureX, textureY); // axleLeftBack2
		bodyModel[5] = new ModelRendererTurbo(this, 44, 33, textureX, textureY); // axleLeftBack3
		bodyModel[6] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // axleLeftFront1
		bodyModel[7] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // axleLeftFront2
		bodyModel[8] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // axleLeftFront3
		bodyModel[9] = new ModelRendererTurbo(this, 44, 17, textureX, textureY); // axleRightBack1
		bodyModel[10] = new ModelRendererTurbo(this, 44, 17, textureX, textureY); // axleRightBack2
		bodyModel[11] = new ModelRendererTurbo(this, 44, 17, textureX, textureY); // axleRightBack3
		bodyModel[12] = new ModelRendererTurbo(this, 44, 1, textureX, textureY); // axleRightFront1
		bodyModel[13] = new ModelRendererTurbo(this, 44, 1, textureX, textureY); // axleRightFront2
		bodyModel[14] = new ModelRendererTurbo(this, 44, 1, textureX, textureY); // axleRightFront3
		bodyModel[15] = new ModelRendererTurbo(this, 1, 202, textureX, textureY); // backBody1
		bodyModel[16] = new ModelRendererTurbo(this, 1, 202, textureX, textureY); // backBody2
		bodyModel[17] = new ModelRendererTurbo(this, 312, 252, textureX, textureY); // backBody3
		bodyModel[18] = new ModelRendererTurbo(this, 57, 209, textureX, textureY); // backBody4
		bodyModel[19] = new ModelRendererTurbo(this, 173, 209, textureX, textureY); // backBody5
		bodyModel[20] = new ModelRendererTurbo(this, 125, 224, textureX, textureY); // backDoor1
		bodyModel[21] = new ModelRendererTurbo(this, 20, 205, textureX, textureY); // backDoor2
		bodyModel[22] = new ModelRendererTurbo(this, 136, 220, textureX, textureY); // backDoor3
		bodyModel[23] = new ModelRendererTurbo(this, 57, 217, textureX, textureY); // backDoor4
		bodyModel[24] = new ModelRendererTurbo(this, 87, 2, textureX, textureY); // backFlap1
		bodyModel[25] = new ModelRendererTurbo(this, 87, 2, textureX, textureY); // backFlap2
		bodyModel[26] = new ModelRendererTurbo(this, 50, 266, textureX, textureY); // backWindow3
		bodyModel[27] = new ModelRendererTurbo(this, 50, 266, textureX, textureY); // backWindow4
		bodyModel[28] = new ModelRendererTurbo(this, 73, 265, textureX, textureY); // backWindow7
		bodyModel[29] = new ModelRendererTurbo(this, 73, 265, textureX, textureY); // backWindow8
		bodyModel[30] = new ModelRendererTurbo(this, 584, 164, textureX, textureY); // body1
		bodyModel[31] = new ModelRendererTurbo(this, 312, 214, textureX, textureY); // body10
		bodyModel[32] = new ModelRendererTurbo(this, 113, 121, textureX, textureY); // body11
		bodyModel[33] = new ModelRendererTurbo(this, 1, 88, textureX, textureY); // body12
		bodyModel[34] = new ModelRendererTurbo(this, 117, 112, textureX, textureY); // body13
		bodyModel[35] = new ModelRendererTurbo(this, 113, 121, textureX, textureY); // body15
		bodyModel[36] = new ModelRendererTurbo(this, 77, 115, textureX, textureY); // body16
		bodyModel[37] = new ModelRendererTurbo(this, 77, 115, textureX, textureY); // body17
		bodyModel[38] = new ModelRendererTurbo(this, 113, 115, textureX, textureY); // body18
		bodyModel[39] = new ModelRendererTurbo(this, 16, 88, textureX, textureY); // body19
		bodyModel[40] = new ModelRendererTurbo(this, 35, 118, textureX, textureY); // body2
		bodyModel[41] = new ModelRendererTurbo(this, 205, 152, textureX, textureY); // body21
		bodyModel[42] = new ModelRendererTurbo(this, 62, 167, textureX, textureY); // body22
		bodyModel[43] = new ModelRendererTurbo(this, 45, 110, textureX, textureY); // body24
		bodyModel[44] = new ModelRendererTurbo(this, 313, 349, textureX, textureY); // body25
		bodyModel[45] = new ModelRendererTurbo(this, 184, 169, textureX, textureY); // body27
		bodyModel[46] = new ModelRendererTurbo(this, 62, 156, textureX, textureY); // body29
		bodyModel[47] = new ModelRendererTurbo(this, 685, 214, textureX, textureY); // body3
		bodyModel[48] = new ModelRendererTurbo(this, 162, 184, textureX, textureY); // body30
		bodyModel[49] = new ModelRendererTurbo(this, 62, 200, textureX, textureY); // body31
		bodyModel[50] = new ModelRendererTurbo(this, 141, 184, textureX, textureY); // body32
		bodyModel[51] = new ModelRendererTurbo(this, 184, 156, textureX, textureY); // body33
		bodyModel[52] = new ModelRendererTurbo(this, 313, 360, textureX, textureY); // body35
		bodyModel[53] = new ModelRendererTurbo(this, 584, 316, textureX, textureY); // body4
		bodyModel[54] = new ModelRendererTurbo(this, 584, 1, textureX, textureY); // body5
		bodyModel[55] = new ModelRendererTurbo(this, 1, 122, textureX, textureY); // body6
		bodyModel[56] = new ModelRendererTurbo(this, 705, 115, textureX, textureY); // body7
		bodyModel[57] = new ModelRendererTurbo(this, 35, 118, textureX, textureY); // body8
		bodyModel[58] = new ModelRendererTurbo(this, 312, 58, textureX, textureY); // body9
		bodyModel[59] = new ModelRendererTurbo(this, 104, 81, textureX, textureY); // bodyFrontVent1
		bodyModel[60] = new ModelRendererTurbo(this, 31, 81, textureX, textureY); // bodyFrontVent2
		bodyModel[61] = new ModelRendererTurbo(this, 670, 18, textureX, textureY); // bodyTop11
		bodyModel[62] = new ModelRendererTurbo(this, 109, 12, textureX, textureY); // bodyTop11
		bodyModel[63] = new ModelRendererTurbo(this, 312, 158, textureX, textureY); // bodyTop3
		bodyModel[64] = new ModelRendererTurbo(this, 312, 110, textureX, textureY); // bodyTop4
		bodyModel[65] = new ModelRendererTurbo(this, 584, 272, textureX, textureY); // bodyTop9
		bodyModel[66] = new ModelRendererTurbo(this, 421, 187, textureX, textureY); // BOX_placeholder
		bodyModel[67] = new ModelRendererTurbo(this, 78, 86, textureX, textureY); // bumper1
		bodyModel[68] = new ModelRendererTurbo(this, 78, 86, textureX, textureY); // bumper2
		bodyModel[69] = new ModelRendererTurbo(this, 179, 23, textureX, textureY); // doorFrontLeft1
		bodyModel[70] = new ModelRendererTurbo(this, 109, 45, textureX, textureY); // doorFrontLeft2
		bodyModel[71] = new ModelRendererTurbo(this, 242, 42, textureX, textureY); // doorFrontLeft3
		bodyModel[72] = new ModelRendererTurbo(this, 166, 42, textureX, textureY); // doorFrontLeft5
		bodyModel[73] = new ModelRendererTurbo(this, 232, 23, textureX, textureY); // doorFrontRight1
		bodyModel[74] = new ModelRendererTurbo(this, 242, 57, textureX, textureY); // doorFrontRight2
		bodyModel[75] = new ModelRendererTurbo(this, 242, 47, textureX, textureY); // doorFrontRight3
		bodyModel[76] = new ModelRendererTurbo(this, 120, 45, textureX, textureY); // doorFrontRight4
		bodyModel[77] = new ModelRendererTurbo(this, 154, 45, textureX, textureY); // doorHinge1
		bodyModel[78] = new ModelRendererTurbo(this, 154, 45, textureX, textureY); // doorHinge3
		bodyModel[79] = new ModelRendererTurbo(this, 78, 267, textureX, textureY); // doorHinge5
		bodyModel[80] = new ModelRendererTurbo(this, 78, 267, textureX, textureY); // doorHinge6
		bodyModel[81] = new ModelRendererTurbo(this, 1, 325, textureX, textureY); // interior1
		bodyModel[82] = new ModelRendererTurbo(this, 1, 253, textureX, textureY); // interior4
		bodyModel[83] = new ModelRendererTurbo(this, 1, 438, textureX, textureY); // interior6
		bodyModel[84] = new ModelRendererTurbo(this, 45, 240, textureX, textureY); // interiorComms1
		bodyModel[85] = new ModelRendererTurbo(this, 36, 417, textureX, textureY); // interiorComms2
		bodyModel[86] = new ModelRendererTurbo(this, 36, 427, textureX, textureY); // interiorComms3
		bodyModel[87] = new ModelRendererTurbo(this, 1, 278, textureX, textureY); // interiorDashboard
		bodyModel[88] = new ModelRendererTurbo(this, 82, 438, textureX, textureY); // interiorFrontRightSeat1
		bodyModel[89] = new ModelRendererTurbo(this, 82, 454, textureX, textureY); // interiorFrontRightSeat2
		bodyModel[90] = new ModelRendererTurbo(this, 1, 410, textureX, textureY); // interiorFrontRightSeat3
		bodyModel[91] = new ModelRendererTurbo(this, 1, 395, textureX, textureY); // interiorFrontRightSeat4
		bodyModel[92] = new ModelRendererTurbo(this, 20, 399, textureX, textureY); // interiorFrontRightSeat5
		bodyModel[93] = new ModelRendererTurbo(this, 1, 382, textureX, textureY); // interiorFrontRightSeat6
		bodyModel[94] = new ModelRendererTurbo(this, 73, 419, textureX, textureY); // interiorGearBox
		bodyModel[95] = new ModelRendererTurbo(this, 178, 69, textureX, textureY); // leftHeadlight1
		bodyModel[96] = new ModelRendererTurbo(this, 178, 69, textureX, textureY); // leftHeadlight2
		bodyModel[97] = new ModelRendererTurbo(this, 178, 69, textureX, textureY); // leftHeadlight3
		bodyModel[98] = new ModelRendererTurbo(this, 179, 42, textureX, textureY); // plateBack
		bodyModel[99] = new ModelRendererTurbo(this, 212, 45, textureX, textureY); // plateFront
		bodyModel[100] = new ModelRendererTurbo(this, 136, 45, textureX, textureY); // radioPoint2
		bodyModel[101] = new ModelRendererTurbo(this, 136, 59, textureX, textureY); // radioPoint3
		bodyModel[102] = new ModelRendererTurbo(this, 203, 69, textureX, textureY); // rightHeadlight1
		bodyModel[103] = new ModelRendererTurbo(this, 203, 69, textureX, textureY); // rightHeadlight2
		bodyModel[104] = new ModelRendererTurbo(this, 203, 69, textureX, textureY); // rightHeadlight3
		bodyModel[105] = new ModelRendererTurbo(this, 584, 107, textureX, textureY); // rimPart1
		bodyModel[106] = new ModelRendererTurbo(this, 1, 75, textureX, textureY); // rimPart10
		bodyModel[107] = new ModelRendererTurbo(this, 411, 249, textureX, textureY); // rimPart2
		bodyModel[108] = new ModelRendererTurbo(this, 584, 50, textureX, textureY); // rimPart3
		bodyModel[109] = new ModelRendererTurbo(this, 312, 1, textureX, textureY); // rimPart4_Main
		bodyModel[110] = new ModelRendererTurbo(this, 1, 62, textureX, textureY); // rimPart5
		bodyModel[111] = new ModelRendererTurbo(this, 1, 49, textureX, textureY); // rimPart6
		bodyModel[112] = new ModelRendererTurbo(this, 1, 75, textureX, textureY); // rimPart7
		bodyModel[113] = new ModelRendererTurbo(this, 1, 62, textureX, textureY); // rimPart8
		bodyModel[114] = new ModelRendererTurbo(this, 1, 49, textureX, textureY); // rimPart9
		bodyModel[115] = new ModelRendererTurbo(this, 163, 82, textureX, textureY); // shockFront1
		bodyModel[116] = new ModelRendererTurbo(this, 163, 82, textureX, textureY); // shockFront2
		bodyModel[117] = new ModelRendererTurbo(this, 163, 82, textureX, textureY); // shockFront3
		bodyModel[118] = new ModelRendererTurbo(this, 90, 369, textureX, textureY); // steeringWheel1
		bodyModel[119] = new ModelRendererTurbo(this, 90, 369, textureX, textureY); // steeringWheel2
		bodyModel[120] = new ModelRendererTurbo(this, 90, 369, textureX, textureY); // steeringWheel3
		bodyModel[121] = new ModelRendererTurbo(this, 671, 285, textureX, textureY); // underBody1
		bodyModel[122] = new ModelRendererTurbo(this, 313, 306, textureX, textureY); // underBody2
		bodyModel[123] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelRightFront0
		bodyModel[124] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelRightFront1
		bodyModel[125] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelRightFront2
		bodyModel[126] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelRightFront3
		bodyModel[127] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelRightFront4
		bodyModel[128] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelRightFront5
		bodyModel[129] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelRightFront6
		bodyModel[130] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelRightFront9
		bodyModel[131] = new ModelRendererTurbo(this, 109, 23, textureX, textureY); // wheelRightFrontCap
		bodyModel[132] = new ModelRendererTurbo(this, 152, 38, textureX, textureY); // wingLeftMirror1
		bodyModel[133] = new ModelRendererTurbo(this, 152, 23, textureX, textureY); // wingLeftMirror2
		bodyModel[134] = new ModelRendererTurbo(this, 159, 38, textureX, textureY); // wingRightMirror1
		bodyModel[135] = new ModelRendererTurbo(this, 165, 23, textureX, textureY); // wingRightMirror2
		bodyModel[136] = new ModelRendererTurbo(this, 713, 69, textureX, textureY); // wireFrame1
		bodyModel[137] = new ModelRendererTurbo(this, 713, 69, textureX, textureY); // wireFrame2
		bodyModel[138] = new ModelRendererTurbo(this, 109, 1, textureX, textureY); // wireFrame3
		bodyModel[139] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelRightFront7
		bodyModel[140] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelRightFront8
		bodyModel[141] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelRightFront10
		bodyModel[142] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelRightFront11
		bodyModel[143] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelLeftFront0
		bodyModel[144] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelLeftFront11
		bodyModel[145] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelLeftFront2
		bodyModel[146] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelLeftFront3
		bodyModel[147] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelLeftFront2
		bodyModel[148] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelLeftFront5
		bodyModel[149] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelLeftFront6
		bodyModel[150] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelLeftFront7
		bodyModel[151] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelLeftFront8
		bodyModel[152] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelLeftFront9
		bodyModel[153] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelLeftFront10
		bodyModel[154] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelLeftFront4
		bodyModel[155] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelRightBack0
		bodyModel[156] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelRightBack1
		bodyModel[157] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelRightBack2
		bodyModel[158] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelRightBack3
		bodyModel[159] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelRightBack4
		bodyModel[160] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelRightBack5
		bodyModel[161] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelRightBack6
		bodyModel[162] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelRightBack7
		bodyModel[163] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelRightBack8
		bodyModel[164] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelRightBack9
		bodyModel[165] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelRightBack10
		bodyModel[166] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelRightBack11
		bodyModel[167] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelLeftBack0
		bodyModel[168] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelLeftBack1
		bodyModel[169] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelLeftBack2
		bodyModel[170] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelLeftBack3
		bodyModel[171] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelLeftBack4
		bodyModel[172] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelLeftBack5
		bodyModel[173] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelLeftBack6
		bodyModel[174] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelLeftBack7
		bodyModel[175] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelLeftBack8
		bodyModel[176] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelLeftBack9
		bodyModel[177] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelLeftBack10
		bodyModel[178] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelLeftBack11
		bodyModel[179] = new ModelRendererTurbo(this, 109, 23, textureX, textureY); // wheelLeftFrontCap
		bodyModel[180] = new ModelRendererTurbo(this, 109, 23, textureX, textureY); // wheelLeftBackCap
		bodyModel[181] = new ModelRendererTurbo(this, 109, 23, textureX, textureY); // wheelRightBackCap
		bodyModel[182] = new ModelRendererTurbo(this, 1, 179, textureX, textureY); // Box 0
		bodyModel[183] = new ModelRendererTurbo(this, 1, 156, textureX, textureY); // Box 2
		bodyModel[184] = new ModelRendererTurbo(this, 128, 189, textureX, textureY); // Box 3
		bodyModel[185] = new ModelRendererTurbo(this, 128, 178, textureX, textureY); // Box 4
		bodyModel[186] = new ModelRendererTurbo(this, 63, 271, textureX, textureY); // Box 5
		bodyModel[187] = new ModelRendererTurbo(this, 1, 239, textureX, textureY); // Box 6
		bodyModel[188] = new ModelRendererTurbo(this, 63, 271, textureX, textureY); // Box 7
		bodyModel[189] = new ModelRendererTurbo(this, 1, 239, textureX, textureY); // Box 8
		bodyModel[190] = new ModelRendererTurbo(this, 1, 410, textureX, textureY); // Box 0
		bodyModel[191] = new ModelRendererTurbo(this, 1, 395, textureX, textureY); // Box 1
		bodyModel[192] = new ModelRendererTurbo(this, 20, 399, textureX, textureY); // Box 2
		bodyModel[193] = new ModelRendererTurbo(this, 1, 382, textureX, textureY); // Box 3
		bodyModel[194] = new ModelRendererTurbo(this, 82, 454, textureX, textureY); // Box 4
		bodyModel[195] = new ModelRendererTurbo(this, 82, 438, textureX, textureY); // Box 5
		bodyModel[196] = new ModelRendererTurbo(this, 242, 52, textureX, textureY); // Box 6
		bodyModel[197] = new ModelRendererTurbo(this, 584, 217, textureX, textureY); // Box 7
		bodyModel[198] = new ModelRendererTurbo(this, 51, 283, textureX, textureY); // Box 9
		bodyModel[199] = new ModelRendererTurbo(this, 125, 224, textureX, textureY); // Box 10
		bodyModel[200] = new ModelRendererTurbo(this, 136, 220, textureX, textureY); // Box 11
		bodyModel[201] = new ModelRendererTurbo(this, 57, 217, textureX, textureY); // Box 12
		bodyModel[202] = new ModelRendererTurbo(this, 50, 266, textureX, textureY); // Box 13
		bodyModel[203] = new ModelRendererTurbo(this, 73, 265, textureX, textureY); // Box 14
		bodyModel[204] = new ModelRendererTurbo(this, 50, 266, textureX, textureY); // Box 15
		bodyModel[205] = new ModelRendererTurbo(this, 73, 265, textureX, textureY); // Box 16
		bodyModel[206] = new ModelRendererTurbo(this, 141, 156, textureX, textureY); // Box 17
		bodyModel[207] = new ModelRendererTurbo(this, 141, 170, textureX, textureY); // Box 18
		bodyModel[208] = new ModelRendererTurbo(this, 1, 363, textureX, textureY); // Box 19
		bodyModel[209] = new ModelRendererTurbo(this, 116, 207, textureX, textureY); // Box 20
		bodyModel[210] = new ModelRendererTurbo(this, 166, 39, textureX, textureY); // Box 21
		bodyModel[211] = new ModelRendererTurbo(this, 1, 455, textureX, textureY); // Box 22
		bodyModel[212] = new ModelRendererTurbo(this, 79, 240, textureX, textureY); // Box 0
		bodyModel[213] = new ModelRendererTurbo(this, 312, 166, textureX, textureY); // Box 1
		bodyModel[214] = new ModelRendererTurbo(this, 312, 341, textureX, textureY); // Box 2
		bodyModel[215] = new ModelRendererTurbo(this, 687, 170, textureX, textureY); // Box 3
		bodyModel[216] = new ModelRendererTurbo(this, 313, 371, textureX, textureY); // Box 0
		bodyModel[217] = new ModelRendererTurbo(this, 313, 392, textureX, textureY); // Box 1
		bodyModel[218] = new ModelRendererTurbo(this, 313, 421, textureX, textureY); // Box 2
		bodyModel[219] = new ModelRendererTurbo(this, 313, 405, textureX, textureY); // Box 3
		bodyModel[220] = new ModelRendererTurbo(this, 353, 407, textureX, textureY); // Box 4
		bodyModel[221] = new ModelRendererTurbo(this, 344, 406, textureX, textureY); // Box 5
		bodyModel[222] = new ModelRendererTurbo(this, 342, 422, textureX, textureY); // Box 6
		bodyModel[223] = new ModelRendererTurbo(this, 342, 422, textureX, textureY); // Box 7
		bodyModel[224] = new ModelRendererTurbo(this, 313, 428, textureX, textureY); // Box 0
		bodyModel[225] = new ModelRendererTurbo(this, 346, 456, textureX, textureY); // Box 1
		bodyModel[226] = new ModelRendererTurbo(this, 313, 456, textureX, textureY); // Box 2
		bodyModel[227] = new ModelRendererTurbo(this, 313, 456, textureX, textureY); // Box 3
		bodyModel[228] = new ModelRendererTurbo(this, 313, 456, textureX, textureY); // Box 4
		bodyModel[229] = new ModelRendererTurbo(this, 379, 389, textureX, textureY); // Box 15
		bodyModel[230] = new ModelRendererTurbo(this, 377, 371, textureX, textureY); // Box 16
		bodyModel[231] = new ModelRendererTurbo(this, 379, 389, textureX, textureY); // Box 17
		bodyModel[232] = new ModelRendererTurbo(this, 313, 456, textureX, textureY); // Box 0
		bodyModel[233] = new ModelRendererTurbo(this, 313, 456, textureX, textureY); // Box 1
		bodyModel[234] = new ModelRendererTurbo(this, 313, 456, textureX, textureY); // Box 3
		bodyModel[235] = new ModelRendererTurbo(this, 136, 45, textureX, textureY); // Box 20

		bodyModel[0].addBox(0F, 0F, 0F, 2, 3, 2, 0F); // antenna1
		bodyModel[0].setRotationPoint(25F, -29F, -20F);

		bodyModel[1].addBox(0F, 0F, 0F, 1, 24, 1, 0F); // antenna2
		bodyModel[1].setRotationPoint(25.5F, -53F, -19.5F);

		bodyModel[2].addBox(0F, 0F, 0F, 2, 2, 2, 0F); // antenna3
		bodyModel[2].setRotationPoint(25F, -55F, -20F);

		bodyModel[3].addShapeBox(0F, 0F, 0F, 9, 3, 12, 0F,-3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // axleLeftBack1
		bodyModel[3].setRotationPoint(-60.5F, -7.5F, 16F);

		bodyModel[4].addBox(0F, 0F, 0F, 9, 3, 12, 0F); // axleLeftBack2
		bodyModel[4].setRotationPoint(-60.5F, -4.5F, 16F);

		bodyModel[5].addShapeBox(0F, 0F, 0F, 9, 3, 12, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F); // axleLeftBack3
		bodyModel[5].setRotationPoint(-60.5F, -1.5F, 16F);

		bodyModel[6].addShapeBox(0F, 0F, 0F, 9, 3, 12, 0F,-3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // axleLeftFront1
		bodyModel[6].setRotationPoint(39.5F, -7.5F, 16F);

		bodyModel[7].addBox(0F, 0F, 0F, 9, 3, 12, 0F); // axleLeftFront2
		bodyModel[7].setRotationPoint(39.5F, -4.5F, 16F);

		bodyModel[8].addShapeBox(0F, 0F, 0F, 9, 3, 12, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F); // axleLeftFront3
		bodyModel[8].setRotationPoint(39.5F, -1.5F, 16F);

		bodyModel[9].addShapeBox(0F, 0F, 0F, 9, 3, 12, 0F,-3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // axleRightBack1
		bodyModel[9].setRotationPoint(-60.5F, -7.5F, -27F);

		bodyModel[10].addBox(0F, 0F, 0F, 9, 3, 12, 0F); // axleRightBack2
		bodyModel[10].setRotationPoint(-60.5F, -4.5F, -27F);

		bodyModel[11].addShapeBox(0F, 0F, 0F, 9, 3, 12, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F); // axleRightBack3
		bodyModel[11].setRotationPoint(-60.5F, -1.5F, -27F);

		bodyModel[12].addShapeBox(0F, 0F, 0F, 9, 3, 12, 0F,-3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // axleRightFront1
		bodyModel[12].setRotationPoint(39.5F, -7.5F, -27F);

		bodyModel[13].addBox(0F, 0F, 0F, 9, 3, 12, 0F); // axleRightFront2
		bodyModel[13].setRotationPoint(39.5F, -4.5F, -27F);

		bodyModel[14].addShapeBox(0F, 0F, 0F, 9, 3, 12, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F); // axleRightFront3
		bodyModel[14].setRotationPoint(39.5F, -1.5F, -27F);

		bodyModel[15].addBox(0F, 0F, 0F, 3, 30, 6, 0F); // backBody1
		bodyModel[15].setRotationPoint(-77F, -41F, 17F);

		bodyModel[16].addBox(0F, 0F, 0F, 3, 30, 6, 0F); // backBody2
		bodyModel[16].setRotationPoint(-77F, -41F, -22F);

		bodyModel[17].addBox(0F, 0F, 0F, 4, 8, 45, 0F); // backBody3
		bodyModel[17].setRotationPoint(-78F, -11F, -22F);

		bodyModel[18].addBox(0F, 0F, 0F, 4, 4, 25, 0F); // backBody4
		bodyModel[18].setRotationPoint(-77F, -4F, -12F);

		bodyModel[19].addBox(0F, 0F, 0F, 8, 2, 27, 0F); // backBody5
		bodyModel[19].setRotationPoint(-82F, -1F, -13F);

		bodyModel[20].addShapeBox(0F, 0F, 0F, 2, 11, 3, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // backDoor1
		bodyModel[20].setRotationPoint(-76.5F, -39F, -3.5F);

		bodyModel[21].addBox(0F, 0F, 0F, 2, 17, 16, 0F); // backDoor2
		bodyModel[21].setRotationPoint(-76.5F, -28F, -16F);

		bodyModel[22].addShapeBox(0F, 0F, 0F, 2, 2, 16, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // backDoor3
		bodyModel[22].setRotationPoint(-76.5F, -41F, -16.5F);

		bodyModel[23].addShapeBox(0F, 0F, 0F, 2, 11, 5, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // backDoor4
		bodyModel[23].setRotationPoint(-76.5F, -39F, -16.5F);

		bodyModel[24].addBox(0F, 0F, 0F, 1, 16, 9, 0F); // backFlap1
		bodyModel[24].setRotationPoint(-73F, -13F, -25.9F);

		bodyModel[25].addBox(0F, 0F, 0F, 1, 16, 9, 0F); // backFlap2
		bodyModel[25].setRotationPoint(-73F, -13F, 17.9F);

		bodyModel[26].addShapeBox(0F, 0F, 0F, 1, 1, 10, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // backWindow3
		bodyModel[26].setRotationPoint(-77.5F, -40F, -12.5F);

		bodyModel[27].addShapeBox(0F, 0F, 0F, 1, 1, 10, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // backWindow4
		bodyModel[27].setRotationPoint(-77.5F, -28F, -12.5F);

		bodyModel[28].addShapeBox(0F, 0F, 0F, 1, 11, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // backWindow7
		bodyModel[28].setRotationPoint(-77.5F, -39F, -12.5F);

		bodyModel[29].addShapeBox(0F, 0F, 0F, 1, 11, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // backWindow8
		bodyModel[29].setRotationPoint(-77.5F, -39F, -3.5F);

		bodyModel[30].addBox(0F, 0F, 0F, 6, 7, 45, 0F); // body1
		bodyModel[30].setRotationPoint(60F, -7F, -22F);

		bodyModel[31].addBox(0F, 0F, 0F, 36, 1, 36, 0F); // body10
		bodyModel[31].setRotationPoint(-74F, -11F, -17.5F);

		bodyModel[32].addShapeBox(0F, 0F, 0F, 12, 4, 3, 0F,0F, 0F, 0F, 2F, -18F, 0F, 2F, -18F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -6F, 14F, 0F, -6F, 14F, 0F, 0F, 0F, 0F); // body11
		bodyModel[32].setRotationPoint(11F, -45F, 20F);

		bodyModel[33].addBox(0F, 0F, 0F, 4, 30, 3, 0F); // body12
		bodyModel[33].setRotationPoint(-11F, -41F, 20F);

		bodyModel[34].addShapeBox(0F, 0F, 0F, 3, 4, 39, 0F,0F, 0F, 0F, 0F, -3.5F, 0F, 0F, -3.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body13
		bodyModel[34].setRotationPoint(11F, -45F, -19F);

		bodyModel[35].addShapeBox(0F, 0F, 0F, 12, 4, 3, 0F,0F, 0F, 0F, 2F, -18F, 0F, 2F, -18F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -6F, 14F, 0F, -6F, 14F, 0F, 0F, 0F, 0F); // body15
		bodyModel[35].setRotationPoint(11F, -45F, -22F);

		bodyModel[36].addShapeBox(0F, 0F, 0F, 8, 2, 19, 0F,0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body16
		bodyModel[36].setRotationPoint(17F, -29F, 1F);

		bodyModel[37].addShapeBox(0F, 0F, 0F, 8, 2, 19, 0F,0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body17
		bodyModel[37].setRotationPoint(17F, -29F, -19F);

		bodyModel[38].addShapeBox(0F, 0F, 0F, 12, 4, 1, 0F,0F, 0F, 0F, 2F, -18F, 0F, 2F, -18F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -6F, 14F, 0F, -6F, 14F, 0F, 0F, 0F, 0F); // body18
		bodyModel[38].setRotationPoint(11F, -45F, 0F);

		bodyModel[39].addBox(0F, 0F, 0F, 4, 30, 3, 0F); // body19
		bodyModel[39].setRotationPoint(-11F, -41F, -22F);

		bodyModel[40].addShapeBox(0F, 0F, 0F, 8, 12, 12, 0F,0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, -5F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, -5F, 0F, 0F); // body2
		bodyModel[40].setRotationPoint(55F, -19F, -22F);

		bodyModel[41].addBox(0F, 0F, 0F, 1, 10, 19, 0F); // body21
		bodyModel[41].setRotationPoint(66F, -18F, -9F);

		bodyModel[42].addBox(0F, 0F, 0F, 35, 8, 2, 0F); // body22
		bodyModel[42].setRotationPoint(-74F, -31F, 20.5F);

		bodyModel[43].addBox(0F, 0F, 0F, 63, 2, 2, 0F); // body24
		bodyModel[43].setRotationPoint(-74F, -41F, 20.5F);

		bodyModel[44].addBox(0F, 0F, 0F, 44, 8, 2, 0F); // body25
		bodyModel[44].setRotationPoint(-74F, -39F, 20.5F);

		bodyModel[45].addShapeBox(0F, 0F, 0F, 8, 10, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -8F, 0F, 0F, -8F, 0F, 0F, 0F, 0F, 0F); // body27
		bodyModel[45].setRotationPoint(-74F, -23F, 20.5F);

		bodyModel[46].addBox(0F, 0F, 0F, 35, 8, 2, 0F); // body29
		bodyModel[46].setRotationPoint(-74F, -31F, -21.5F);

		bodyModel[47].addShapeBox(0F, 0F, 0F, 7, 12, 45, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -6F, 0F, 0F, -6F, 0F, 0F, 0F, 0F, 0F); // body3
		bodyModel[47].setRotationPoint(25F, -19F, -22F);

		bodyModel[48].addShapeBox(0F, 0F, 0F, 8, 10, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -8F, 0F, 0F, -8F, 0F, 0F, 0F, 0F, 0F); // body30
		bodyModel[48].setRotationPoint(-74F, -23F, -21.5F);

		bodyModel[49].addBox(0F, 0F, 0F, 63, 2, 2, 0F); // body31
		bodyModel[49].setRotationPoint(-74F, -41F, -21.5F);

		bodyModel[50].addShapeBox(0F, 0F, 0F, 8, 10, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -8F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -8F, 0F, 0F); // body32
		bodyModel[50].setRotationPoint(-47F, -23F, 20.5F);

		bodyModel[51].addShapeBox(0F, 0F, 0F, 8, 10, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -8F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -8F, 0F, 0F); // body33
		bodyModel[51].setRotationPoint(-47F, -23F, -21.5F);

		bodyModel[52].addBox(0F, 0F, 0F, 44, 8, 2, 0F); // body35
		bodyModel[52].setRotationPoint(-74F, -39F, -21.5F);

		bodyModel[53].addBox(0F, 0F, 0F, 20, 8, 45, 0F); // body4
		bodyModel[53].setRotationPoint(25F, -27F, -22F);

		bodyModel[54].addShapeBox(0F, 0F, 0F, 20, 3, 45, 0F,0F, 5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body5
		bodyModel[54].setRotationPoint(45F, -22F, -22F);

		bodyModel[55].addBox(0F, 0F, 0F, 6, 12, 21, 0F); // body6
		bodyModel[55].setRotationPoint(60F, -19F, -10F);

		bodyModel[56].addShapeBox(0F, 0F, 0F, 3, 2, 45, 0F,0F, 1F, 0F, 0F, 0F, -12F, 0F, 0F, -12F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, -12F, 0F, 0F, -12F, 0F, 0F, 0F); // body7
		bodyModel[56].setRotationPoint(65F, -21F, -22F);

		bodyModel[57].addShapeBox(0F, 0F, 0F, 8, 12, 12, 0F,0F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -5F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, -5F, 0F, 0F); // body8
		bodyModel[57].setRotationPoint(55F, -19F, 11F);

		bodyModel[58].addBox(0F, 0F, 0F, 58, 6, 45, 0F); // body9
		bodyModel[58].setRotationPoint(-33F, -11F, -22F);

		bodyModel[59].addShapeBox(0F, 0F, 0F, 14, 2, 26, 0F,0F, 0F, -2F, 0F, -2F, -2F, 0F, -2F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, 0F, 0F); // bodyFrontVent1
		bodyModel[59].setRotationPoint(45F, -28F, -12F);

		bodyModel[60].addShapeBox(0F, 0F, 0F, 10, 2, 26, 0F,0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // bodyFrontVent2
		bodyModel[60].setRotationPoint(35F, -28F, -12F);

		bodyModel[61].addShapeBox(0F, 0F, 0F, 25, 2, 25, 0F,-2F, 0F, -2F, -2F, 0F, -2F, -2F, 0F, -2F, -2F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // bodyTop11
		bodyModel[61].setRotationPoint(-44F, -49F, -12F);

		bodyModel[62].addBox(0F, 0F, 0F, 95, 5, 5, 0F); // bodyTop11
		bodyModel[62].setRotationPoint(-53.5F, -5.5F, 11F);

		bodyModel[63].addBox(0F, 0F, 0F, 88, 4, 3, 0F); // bodyTop3
		bodyModel[63].setRotationPoint(-77F, -45F, -22F);

		bodyModel[64].addShapeBox(0F, 0F, 0F, 89, 2, 45, 0F,-2F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, -2F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // bodyTop4
		bodyModel[64].setRotationPoint(-77F, -47F, -22F);

		bodyModel[65].addShapeBox(0F, 0F, 0F, 5, 5, 38, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // bodyTop9
		bodyModel[65].setRotationPoint(-78.5F, -48F, -18.5F);

		bodyModel[66].addBox(0F, 0F, 0F, 8, 16, 45, 0F); // BOX_placeholder
		bodyModel[66].setRotationPoint(17F, -27F, -22F);

		bodyModel[67].addShapeBox(0F, 0F, 0F, 2, 5, 15, 0F,0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F); // bumper1
		bodyModel[67].setRotationPoint(66F, -6F, -22F);

		bodyModel[68].addShapeBox(0F, 0F, 0F, 2, 5, 15, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, 0F); // bumper2
		bodyModel[68].setRotationPoint(66F, -6F, 8F);

		bodyModel[69].addBox(0F, 0F, 0F, 24, 16, 2, 0F); // doorFrontLeft1
		bodyModel[69].setRotationPoint(-7F, -27F, 20.5F);

		bodyModel[70].addBox(0F, 0F, 0F, 3, 12, 2, 0F); // doorFrontLeft2
		bodyModel[70].setRotationPoint(-7F, -39F, 20.5F);

		bodyModel[71].addBox(0F, 0F, 0F, 18, 2, 2, 0F); // doorFrontLeft3
		bodyModel[71].setRotationPoint(-7F, -41F, 20.5F);

		bodyModel[72].addBox(0F, 0F, 0F, 5, 1, 1, 0F); // doorFrontLeft5
		bodyModel[72].setRotationPoint(-5F, -24F, 22.5F);

		bodyModel[73].addBox(0F, 0F, 0F, 24, 16, 2, 0F); // doorFrontRight1
		bodyModel[73].setRotationPoint(-7F, -27F, -21.5F);

		bodyModel[74].addShapeBox(0F, 0F, 0F, 4, 2, 2, 0F,0F, 0F, 0F, 2F, -14F, 0F, 2F, -14F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 12F, 0F, -2F, 12F, 0F, 0F, 0F, 0F); // doorFrontRight2
		bodyModel[74].setRotationPoint(11F, -41F, -21.5F);

		bodyModel[75].addBox(0F, 0F, 0F, 18, 2, 2, 0F); // doorFrontRight3
		bodyModel[75].setRotationPoint(-7F, -41F, -21.5F);

		bodyModel[76].addBox(0F, 0F, 0F, 3, 12, 2, 0F); // doorFrontRight4
		bodyModel[76].setRotationPoint(-7F, -39F, -21.5F);

		bodyModel[77].addShapeBox(0F, 0F, 0F, 3, 7, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // doorHinge1
		bodyModel[77].setRotationPoint(15.5F, -23F, 22.5F);

		bodyModel[78].addShapeBox(0F, 0F, 0F, 3, 7, 1, 0F,-1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // doorHinge3
		bodyModel[78].setRotationPoint(15.5F, -23F, -22.5F);

		bodyModel[79].addShapeBox(0F, 0F, 0F, 1, 7, 3, 0F,0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // doorHinge5
		bodyModel[79].setRotationPoint(-77.5F, -25F, -17.5F);

		bodyModel[80].addShapeBox(0F, 0F, 0F, 1, 7, 3, 0F,0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // doorHinge6
		bodyModel[80].setRotationPoint(-77.5F, -25F, 15.5F);

		bodyModel[81].addShapeBox(0F, 0F, 0F, 5, 8, 29, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F); // interior1
		bodyModel[81].setRotationPoint(12F, -22F, -19F);

		bodyModel[82].addBox(0F, 0F, 0F, 5, 5, 19, 0F); // interior4
		bodyModel[82].setRotationPoint(-28.5F, -5.5F, -8F);

		bodyModel[83].addBox(0F, 0F, 0F, 12, 3, 13, 0F); // interior6
		bodyModel[83].setRotationPoint(-54F, -14F, -7F);

		bodyModel[84].addBox(0F, 0F, 0F, 12, 9, 4, 0F); // interiorComms1
		bodyModel[84].setRotationPoint(-72F, -32F, -19.5F);

		bodyModel[85].addShapeBox(0F, 0F, 0F, 12, 6, 3, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F); // interiorComms2
		bodyModel[85].setRotationPoint(-72F, -39F, -19.5F);

		bodyModel[86].addBox(0F, 0F, 0F, 13, 5, 5, 0F); // interiorComms3
		bodyModel[86].setRotationPoint(-72.5F, -31F, -20F);

		bodyModel[87].addShapeBox(0F, 0F, 0F, 5, 7, 39, 0F,-1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // interiorDashboard
		bodyModel[87].setRotationPoint(12F, -29F, -19F);

		bodyModel[88].addBox(0F, 0F, 0F, 13, 3, 12, 0F); // interiorFrontRightSeat1
		bodyModel[88].setRotationPoint(-4F, -14F, -17F);

		bodyModel[89].addBox(0F, 0F, 0F, 15, 3, 14, 0F); // interiorFrontRightSeat2
		bodyModel[89].setRotationPoint(-4F, -17F, -18F);

		bodyModel[90].addShapeBox(0F, 0F, 0F, 3, 13, 14, 0F,1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F, 1F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // interiorFrontRightSeat3
		bodyModel[90].setRotationPoint(-4F, -30F, -18F);

		bodyModel[91].addShapeBox(0F, 0F, 0F, 3, 2, 12, 0F,0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // interiorFrontRightSeat4
		bodyModel[91].setRotationPoint(-5F, -32F, -17F);

		bodyModel[92].addBox(0F, 0F, 0F, 1, 1, 6, 0F); // interiorFrontRightSeat5
		bodyModel[92].setRotationPoint(-4F, -33F, -14F);

		bodyModel[93].addBox(0F, 0F, 0F, 3, 4, 8, 0F); // interiorFrontRightSeat6
		bodyModel[93].setRotationPoint(-5F, -37F, -15F);

		bodyModel[94].addBox(0F, 0F, 0F, 18, 10, 8, 0F); // interiorGearBox
		bodyModel[94].setRotationPoint(-6F, -21F, -3.5F);

		bodyModel[95].addShapeBox(0F, 0F, 0F, 3, 3, 9, 0F,0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // leftHeadlight1
		bodyModel[95].setRotationPoint(62.5F, -18F, 13.5F);

		bodyModel[96].addBox(0F, 0F, 0F, 3, 3, 9, 0F); // leftHeadlight2
		bodyModel[96].setRotationPoint(62.5F, -15F, 13.5F);

		bodyModel[97].addShapeBox(0F, 0F, 0F, 3, 3, 9, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // leftHeadlight3
		bodyModel[97].setRotationPoint(62.5F, -12F, 13.5F);

		bodyModel[98].addBox(0F, 0F, 0F, 1, 6, 15, 0F); // plateBack
		bodyModel[98].setRotationPoint(-79F, -10F, -7F);

		bodyModel[99].addBox(0F, 0F, 0F, 1, 6, 13, 0F); // plateFront
		bodyModel[99].setRotationPoint(66F, -4F, -6F);

		bodyModel[100].addShapeBox(0F, 0F, 0F, 1, 10, 3, 0F,0F, 0F, 0F, 0.5F, 0F, -1F, 0.5F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, -1F, 0.5F, 0F, -1F, 0F, 0F, 0F); // radioPoint2
		bodyModel[100].setRotationPoint(-69.5F, -55F, 21F);

		bodyModel[101].addBox(0F, 0F, 0F, 4, 4, 2, 0F); // radioPoint3
		bodyModel[101].setRotationPoint(-71.5F, -45F, 22.5F);

		bodyModel[102].addShapeBox(0F, 0F, 0F, 3, 3, 9, 0F,0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // rightHeadlight1
		bodyModel[102].setRotationPoint(62.5F, -18F, -20.5F);

		bodyModel[103].addBox(0F, 0F, 0F, 3, 3, 9, 0F); // rightHeadlight2
		bodyModel[103].setRotationPoint(62.5F, -15F, -20.5F);

		bodyModel[104].addShapeBox(0F, 0F, 0F, 3, 3, 9, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // rightHeadlight3
		bodyModel[104].setRotationPoint(62.5F, -12F, -20.5F);

		bodyModel[105].addShapeBox(0F, 0F, 0F, 7, 3, 53, 0F,0F, 0F, 0F, 0F, -10F, 0F, 0F, -10F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 10F, 0F, 0F, 10F, 0F, 0F, 0F, 0F); // rimPart1
		bodyModel[105].setRotationPoint(52F, -23F, -26F);

		bodyModel[106].addShapeBox(0F, 0F, 0F, 8, 3, 9, 0F,0F, -10F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -10F, 0F, 0F, 10F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 10F, 0F); // rimPart10
		bodyModel[106].setRotationPoint(-74F, -23F, 18F);

		bodyModel[107].addBox(0F, 0F, 0F, 18, 3, 53, 0F); // rimPart2
		bodyModel[107].setRotationPoint(34F, -23F, -26F);

		bodyModel[108].addShapeBox(0F, 0F, 0F, 10, 3, 53, 0F,0F, -18F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -18F, 0F, 0F, 18F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 18F, 0F); // rimPart3
		bodyModel[108].setRotationPoint(24F, -23F, -26F);

		bodyModel[109].addBox(0F, 0F, 0F, 57, 3, 53, 0F); // rimPart4_Main
		bodyModel[109].setRotationPoint(-33F, -5F, -26F);

		bodyModel[110].addShapeBox(0F, 0F, 0F, 13, 3, 9, 0F,0F, 0F, 0F, 0F, -18F, 0F, 0F, -18F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 18F, 0F, 0F, 18F, 0F, 0F, 0F, 0F); // rimPart5
		bodyModel[110].setRotationPoint(-46F, -23F, -26F);

		bodyModel[111].addBox(0F, 0F, 0F, 20, 3, 9, 0F); // rimPart6
		bodyModel[111].setRotationPoint(-66F, -23F, -26F);

		bodyModel[112].addShapeBox(0F, 0F, 0F, 8, 3, 9, 0F,0F, -10F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -10F, 0F, 0F, 10F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 10F, 0F); // rimPart7
		bodyModel[112].setRotationPoint(-74F, -23F, -26F);

		bodyModel[113].addShapeBox(0F, 0F, 0F, 13, 3, 9, 0F,0F, 0F, 0F, 0F, -18F, 0F, 0F, -18F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 18F, 0F, 0F, 18F, 0F, 0F, 0F, 0F); // rimPart8
		bodyModel[113].setRotationPoint(-46F, -23F, 18F);

		bodyModel[114].addBox(0F, 0F, 0F, 20, 3, 9, 0F); // rimPart9
		bodyModel[114].setRotationPoint(-66F, -23F, 18F);

		bodyModel[115].addShapeBox(0F, 0F, 0F, 9, 3, 16, 0F,-3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // shockFront1
		bodyModel[115].setRotationPoint(39.5F, -7.5F, -7F);

		bodyModel[116].addBox(0F, 0F, 0F, 9, 3, 16, 0F); // shockFront2
		bodyModel[116].setRotationPoint(39.5F, -4.5F, -7F);

		bodyModel[117].addShapeBox(0F, 0F, 0F, 9, 3, 16, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F); // shockFront3
		bodyModel[117].setRotationPoint(39.5F, -1.5F, -7F);

		bodyModel[118].addShapeBox(0F, -4.5F, -4.5F, 1, 3, 9, 0F,0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // steeringWheel1
		bodyModel[118].setRotationPoint(11.5F, -26.5F, 12F);
		bodyModel[118].rotateAngleZ = -0.17453293F;

		bodyModel[119].addBox(0F, -1.5F, -4.5F, 1, 3, 9, 0F); // steeringWheel2
		bodyModel[119].setRotationPoint(11.5F, -26.5F, 12F);
		bodyModel[119].rotateAngleZ = -0.17453293F;

		bodyModel[120].addShapeBox(0F, 1.5F, -4.5F, 1, 3, 9, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // steeringWheel3
		bodyModel[120].setRotationPoint(11.5F, -26.5F, 12F);
		bodyModel[120].rotateAngleZ = -0.17453293F;

		bodyModel[121].addBox(0F, 0F, 0F, 35, 11, 19, 0F); // underBody1
		bodyModel[121].setRotationPoint(-23.5F, -7.5F, -8F);

		bodyModel[122].addBox(0F, 0F, 0F, 25, 14, 19, 0F); // underBody2
		bodyModel[122].setRotationPoint(11.5F, -7.5F, -8F);

		bodyModel[123].addBox(-4F, -15F, -5F, 8, 8, 10, 0F); // wheelRightFront0
		bodyModel[123].setRotationPoint(44F, -3F, -23F);

		bodyModel[124].addShapeBox(-12F, -15F, -5F, 8, 8, 10, 0F,-1F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, -4F, 0F, -5F, 3F, 0F, 4F, -1F, 0F, 4F, -1F, 0F, -5F, 3F, 0F); // wheelRightFront1
		bodyModel[124].setRotationPoint(44F, -3F, -23F);

		bodyModel[125].addShapeBox(-12F, -6.9F, -5F, 8, 8, 10, 0F,3F, -3F, 0F, -7F, 4F, 0F, -7F, 4F, 0F, 3F, -3F, 0F, -4F, -1F, 0F, 0F, -8F, 0F, 0F, -8F, 0F, -4F, -1F, 0F); // wheelRightFront2
		bodyModel[125].setRotationPoint(44F, -3F, -23F);

		bodyModel[126].addBox(-15F, -4F, -5F, 8, 8, 10, 0F); // wheelRightFront3
		bodyModel[126].setRotationPoint(44F, -3F, -23F);

		bodyModel[127].addShapeBox(-15F, 4.1F, -5F, 8, 8, 10, 0F,-7F, 4F, 0F, 3F, -3F, 0F, 3F, -3F, 0F, -7F, 4F, 0F, 0F, -8F, 0F, -4F, -1F, 0F, -4F, -1F, 0F, 0F, -8F, 0F); // wheelRightFront4
		bodyModel[127].setRotationPoint(44F, -3F, -23F);

		bodyModel[128].addShapeBox(-7F, 4F, -5F, 8, 8, 10, 0F,0F, 0F, 0F, -1F, -4F, 0F, -1F, -4F, 0F, 0F, 0F, 0F, 4F, -1F, 0F, -5F, 3F, 0F, -5F, 3F, 0F, 4F, -1F, 0F); // wheelRightFront5
		bodyModel[128].setRotationPoint(44F, -3F, -23F);

		bodyModel[129].addBox(-4F, 7F, -5F, 8, 8, 10, 0F); // wheelRightFront6
		bodyModel[129].setRotationPoint(44F, -3F, -23F);

		bodyModel[130].addBox(7F, -4F, -5F, 8, 8, 10, 0F); // wheelRightFront9
		bodyModel[130].setRotationPoint(44F, -3F, -23F);

		bodyModel[131].addBox(-6F, -7F, -3F, 14, 14, 7, 0F); // wheelRightFrontCap
		bodyModel[131].setRotationPoint(43F, -3F, -23F);

		bodyModel[132].addBox(0F, 0F, 0F, 1, 4, 2, 0F); // wingLeftMirror1
		bodyModel[132].setRotationPoint(13F, -34F, 22.5F);

		bodyModel[133].addBox(0F, 0F, 0F, 1, 9, 5, 0F); // wingLeftMirror2
		bodyModel[133].setRotationPoint(13F, -38F, 24.3F);
		bodyModel[133].rotateAngleY = 0.17453293F;

		bodyModel[134].addBox(0F, 0F, 0F, 1, 4, 2, 0F); // wingRightMirror1
		bodyModel[134].setRotationPoint(12.7F, -34F, -23.5F);

		bodyModel[135].addBox(0F, 0F, 0F, 1, 9, 5, 0F); // wingRightMirror2
		bodyModel[135].setRotationPoint(12F, -38F, -28.3F);
		bodyModel[135].rotateAngleY = -0.17453293F;

		bodyModel[136].addBox(0F, 0F, 0F, 5, 5, 31, 0F); // wireFrame1
		bodyModel[136].setRotationPoint(41.5F, -5.5F, -15F);

		bodyModel[137].addBox(0F, 0F, 0F, 5, 5, 31, 0F); // wireFrame2
		bodyModel[137].setRotationPoint(-58.5F, -5.5F, -15F);

		bodyModel[138].addBox(0F, 0F, 0F, 95, 5, 5, 0F); // wireFrame3
		bodyModel[138].setRotationPoint(-53.5F, -5.5F, -13F);

		bodyModel[139].addShapeBox(-1F, 4F, -5F, 8, 8, 10, 0F,-1F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, -4F, 0F, -5F, 3F, 0F, 4F, -1F, 0F, 4F, -1F, 0F, -5F, 3F, 0F); // wheelRightFront7
		bodyModel[139].setRotationPoint(44F, -3F, -23F);

		bodyModel[140].addShapeBox(7F, 4.1F, -5F, 8, 8, 10, 0F,3F, -3F, 0F, -7F, 4F, 0F, -7F, 4F, 0F, 3F, -3F, 0F, -4F, -1F, 0F, 0F, -8F, 0F, 0F, -8F, 0F, -4F, -1F, 0F); // wheelRightFront8
		bodyModel[140].setRotationPoint(44F, -3F, -23F);

		bodyModel[141].addShapeBox(4F, -7.1F, -5F, 8, 8, 10, 0F,-7F, 4F, 0F, 3F, -3F, 0F, 3F, -3F, 0F, -7F, 4F, 0F, 0F, -8F, 0F, -4F, -1F, 0F, -4F, -1F, 0F, 0F, -8F, 0F); // wheelRightFront10
		bodyModel[141].setRotationPoint(44F, -3F, -23F);

		bodyModel[142].addShapeBox(4F, -15F, -5F, 8, 8, 10, 0F,0F, 0F, 0F, -1F, -4F, 0F, -1F, -4F, 0F, 0F, 0F, 0F, 4F, -1F, 0F, -5F, 3F, 0F, -5F, 3F, 0F, 4F, -1F, 0F); // wheelRightFront11
		bodyModel[142].setRotationPoint(44F, -3F, -23F);

		bodyModel[143].addBox(-4F, -15F, -5F, 8, 8, 10, 0F); // wheelLeftFront0
		bodyModel[143].setRotationPoint(44F, -3F, 24F);

		bodyModel[144].addShapeBox(4F, -15F, -5F, 8, 8, 10, 0F,0F, 0F, 0F, -1F, -4F, 0F, -1F, -4F, 0F, 0F, 0F, 0F, 4F, -1F, 0F, -5F, 3F, 0F, -5F, 3F, 0F, 4F, -1F, 0F); // wheelLeftFront11
		bodyModel[144].setRotationPoint(44F, -3F, 24F);

		bodyModel[145].addShapeBox(-12F, -15F, -5F, 8, 8, 10, 0F,-1F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, -4F, 0F, -5F, 3F, 0F, 4F, -1F, 0F, 4F, -1F, 0F, -5F, 3F, 0F); // wheelLeftFront2
		bodyModel[145].setRotationPoint(44F, -3F, 24F);

		bodyModel[146].addBox(-15F, -4F, -5F, 8, 8, 10, 0F); // wheelLeftFront3
		bodyModel[146].setRotationPoint(44F, -3F, 24F);

		bodyModel[147].addShapeBox(-12F, -6.9F, -5F, 8, 8, 10, 0F,3F, -3F, 0F, -7F, 4F, 0F, -7F, 4F, 0F, 3F, -3F, 0F, -4F, -1F, 0F, 0F, -8F, 0F, 0F, -8F, 0F, -4F, -1F, 0F); // wheelLeftFront2
		bodyModel[147].setRotationPoint(44F, -3F, 24F);

		bodyModel[148].addShapeBox(-7F, 4F, -5F, 8, 8, 10, 0F,0F, 0F, 0F, -1F, -4F, 0F, -1F, -4F, 0F, 0F, 0F, 0F, 4F, -1F, 0F, -5F, 3F, 0F, -5F, 3F, 0F, 4F, -1F, 0F); // wheelLeftFront5
		bodyModel[148].setRotationPoint(44F, -3F, 24F);

		bodyModel[149].addBox(-4F, 7F, -5F, 8, 8, 10, 0F); // wheelLeftFront6
		bodyModel[149].setRotationPoint(44F, -3F, 24F);

		bodyModel[150].addShapeBox(-1F, 4F, -5F, 8, 8, 10, 0F,-1F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, -4F, 0F, -5F, 3F, 0F, 4F, -1F, 0F, 4F, -1F, 0F, -5F, 3F, 0F); // wheelLeftFront7
		bodyModel[150].setRotationPoint(44F, -3F, 24F);

		bodyModel[151].addShapeBox(7F, 4.1F, -5F, 8, 8, 10, 0F,3F, -3F, 0F, -7F, 4F, 0F, -7F, 4F, 0F, 3F, -3F, 0F, -4F, -1F, 0F, 0F, -8F, 0F, 0F, -8F, 0F, -4F, -1F, 0F); // wheelLeftFront8
		bodyModel[151].setRotationPoint(44F, -3F, 24F);

		bodyModel[152].addBox(7F, -4F, -5F, 8, 8, 10, 0F); // wheelLeftFront9
		bodyModel[152].setRotationPoint(44F, -3F, 24F);

		bodyModel[153].addShapeBox(4F, -7.1F, -5F, 8, 8, 10, 0F,-7F, 4F, 0F, 3F, -3F, 0F, 3F, -3F, 0F, -7F, 4F, 0F, 0F, -8F, 0F, -4F, -1F, 0F, -4F, -1F, 0F, 0F, -8F, 0F); // wheelLeftFront10
		bodyModel[153].setRotationPoint(44F, -3F, 24F);

		bodyModel[154].addShapeBox(-15F, 4.1F, -5F, 8, 8, 10, 0F,-7F, 4F, 0F, 3F, -3F, 0F, 3F, -3F, 0F, -7F, 4F, 0F, 0F, -8F, 0F, -4F, -1F, 0F, -4F, -1F, 0F, 0F, -8F, 0F); // wheelLeftFront4
		bodyModel[154].setRotationPoint(44F, -3F, 24F);

		bodyModel[155].addBox(-4F, -15F, -5F, 8, 8, 10, 0F); // wheelRightBack0
		bodyModel[155].setRotationPoint(-56F, -3F, -23F);

		bodyModel[156].addShapeBox(-12F, -15F, -5F, 8, 8, 10, 0F,-1F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, -4F, 0F, -5F, 3F, 0F, 4F, -1F, 0F, 4F, -1F, 0F, -5F, 3F, 0F); // wheelRightBack1
		bodyModel[156].setRotationPoint(-56F, -3F, -23F);

		bodyModel[157].addShapeBox(-12F, -6.9F, -5F, 8, 8, 10, 0F,3F, -3F, 0F, -7F, 4F, 0F, -7F, 4F, 0F, 3F, -3F, 0F, -4F, -1F, 0F, 0F, -8F, 0F, 0F, -8F, 0F, -4F, -1F, 0F); // wheelRightBack2
		bodyModel[157].setRotationPoint(-56F, -3F, -23F);

		bodyModel[158].addBox(-15F, -4F, -5F, 8, 8, 10, 0F); // wheelRightBack3
		bodyModel[158].setRotationPoint(-56F, -3F, -23F);

		bodyModel[159].addShapeBox(-15F, 4.1F, -5F, 8, 8, 10, 0F,-7F, 4F, 0F, 3F, -3F, 0F, 3F, -3F, 0F, -7F, 4F, 0F, 0F, -8F, 0F, -4F, -1F, 0F, -4F, -1F, 0F, 0F, -8F, 0F); // wheelRightBack4
		bodyModel[159].setRotationPoint(-56F, -3F, -23F);

		bodyModel[160].addShapeBox(-7F, 4F, -5F, 8, 8, 10, 0F,0F, 0F, 0F, -1F, -4F, 0F, -1F, -4F, 0F, 0F, 0F, 0F, 4F, -1F, 0F, -5F, 3F, 0F, -5F, 3F, 0F, 4F, -1F, 0F); // wheelRightBack5
		bodyModel[160].setRotationPoint(-56F, -3F, -23F);

		bodyModel[161].addBox(-4F, 7F, -5F, 8, 8, 10, 0F); // wheelRightBack6
		bodyModel[161].setRotationPoint(-56F, -3F, -23F);

		bodyModel[162].addShapeBox(-1F, 4F, -5F, 8, 8, 10, 0F,-1F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, -4F, 0F, -5F, 3F, 0F, 4F, -1F, 0F, 4F, -1F, 0F, -5F, 3F, 0F); // wheelRightBack7
		bodyModel[162].setRotationPoint(-56F, -3F, -23F);

		bodyModel[163].addShapeBox(7F, 4.1F, -5F, 8, 8, 10, 0F,3F, -3F, 0F, -7F, 4F, 0F, -7F, 4F, 0F, 3F, -3F, 0F, -4F, -1F, 0F, 0F, -8F, 0F, 0F, -8F, 0F, -4F, -1F, 0F); // wheelRightBack8
		bodyModel[163].setRotationPoint(-56F, -3F, -23F);

		bodyModel[164].addBox(7F, -4F, -5F, 8, 8, 10, 0F); // wheelRightBack9
		bodyModel[164].setRotationPoint(-56F, -3F, -23F);

		bodyModel[165].addShapeBox(4F, -7.1F, -5F, 8, 8, 10, 0F,-7F, 4F, 0F, 3F, -3F, 0F, 3F, -3F, 0F, -7F, 4F, 0F, 0F, -8F, 0F, -4F, -1F, 0F, -4F, -1F, 0F, 0F, -8F, 0F); // wheelRightBack10
		bodyModel[165].setRotationPoint(-56F, -3F, -23F);

		bodyModel[166].addShapeBox(4F, -15F, -5F, 8, 8, 10, 0F,0F, 0F, 0F, -1F, -4F, 0F, -1F, -4F, 0F, 0F, 0F, 0F, 4F, -1F, 0F, -5F, 3F, 0F, -5F, 3F, 0F, 4F, -1F, 0F); // wheelRightBack11
		bodyModel[166].setRotationPoint(-56F, -3F, -23F);

		bodyModel[167].addBox(-4F, -15F, -5F, 8, 8, 10, 0F); // wheelLeftBack0
		bodyModel[167].setRotationPoint(-56F, -3F, 24F);

		bodyModel[168].addShapeBox(-12F, -15F, -5F, 8, 8, 10, 0F,-1F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, -4F, 0F, -5F, 3F, 0F, 4F, -1F, 0F, 4F, -1F, 0F, -5F, 3F, 0F); // wheelLeftBack1
		bodyModel[168].setRotationPoint(-56F, -3F, 24F);

		bodyModel[169].addShapeBox(-12F, -6.9F, -5F, 8, 8, 10, 0F,3F, -3F, 0F, -7F, 4F, 0F, -7F, 4F, 0F, 3F, -3F, 0F, -4F, -1F, 0F, 0F, -8F, 0F, 0F, -8F, 0F, -4F, -1F, 0F); // wheelLeftBack2
		bodyModel[169].setRotationPoint(-56F, -3F, 24F);

		bodyModel[170].addBox(-15F, -4F, -5F, 8, 8, 10, 0F); // wheelLeftBack3
		bodyModel[170].setRotationPoint(-56F, -3F, 24F);

		bodyModel[171].addShapeBox(-15F, 4.1F, -5F, 8, 8, 10, 0F,-7F, 4F, 0F, 3F, -3F, 0F, 3F, -3F, 0F, -7F, 4F, 0F, 0F, -8F, 0F, -4F, -1F, 0F, -4F, -1F, 0F, 0F, -8F, 0F); // wheelLeftBack4
		bodyModel[171].setRotationPoint(-56F, -3F, 24F);

		bodyModel[172].addShapeBox(-7F, 4F, -5F, 8, 8, 10, 0F,0F, 0F, 0F, -1F, -4F, 0F, -1F, -4F, 0F, 0F, 0F, 0F, 4F, -1F, 0F, -5F, 3F, 0F, -5F, 3F, 0F, 4F, -1F, 0F); // wheelLeftBack5
		bodyModel[172].setRotationPoint(-56F, -3F, 24F);

		bodyModel[173].addBox(-4F, 7F, -5F, 8, 8, 10, 0F); // wheelLeftBack6
		bodyModel[173].setRotationPoint(-56F, -3F, 24F);

		bodyModel[174].addShapeBox(-1F, 4F, -5F, 8, 8, 10, 0F,-1F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, -4F, 0F, -5F, 3F, 0F, 4F, -1F, 0F, 4F, -1F, 0F, -5F, 3F, 0F); // wheelLeftBack7
		bodyModel[174].setRotationPoint(-56F, -3F, 24F);

		bodyModel[175].addShapeBox(7F, 4.1F, -5F, 8, 8, 10, 0F,3F, -3F, 0F, -7F, 4F, 0F, -7F, 4F, 0F, 3F, -3F, 0F, -4F, -1F, 0F, 0F, -8F, 0F, 0F, -8F, 0F, -4F, -1F, 0F); // wheelLeftBack8
		bodyModel[175].setRotationPoint(-56F, -3F, 24F);

		bodyModel[176].addBox(7F, -4F, -5F, 8, 8, 10, 0F); // wheelLeftBack9
		bodyModel[176].setRotationPoint(-56F, -3F, 24F);

		bodyModel[177].addShapeBox(4F, -7.1F, -5F, 8, 8, 10, 0F,-7F, 4F, 0F, 3F, -3F, 0F, 3F, -3F, 0F, -7F, 4F, 0F, 0F, -8F, 0F, -4F, -1F, 0F, -4F, -1F, 0F, 0F, -8F, 0F); // wheelLeftBack10
		bodyModel[177].setRotationPoint(-56F, -3F, 24F);

		bodyModel[178].addShapeBox(4F, -15F, -5F, 8, 8, 10, 0F,0F, 0F, 0F, -1F, -4F, 0F, -1F, -4F, 0F, 0F, 0F, 0F, 4F, -1F, 0F, -5F, 3F, 0F, -5F, 3F, 0F, 4F, -1F, 0F); // wheelLeftBack11
		bodyModel[178].setRotationPoint(-56F, -3F, 24F);

		bodyModel[179].addBox(-6F, -7F, -3F, 14, 14, 7, 0F); // wheelLeftFrontCap
		bodyModel[179].setRotationPoint(43F, -3F, 23F);

		bodyModel[180].addBox(-6F, -7F, -3F, 14, 14, 7, 0F); // wheelLeftBackCap
		bodyModel[180].setRotationPoint(-57F, -3F, 23F);

		bodyModel[181].addBox(-6F, -7F, -3F, 14, 14, 7, 0F); // wheelRightBackCap
		bodyModel[181].setRotationPoint(-57F, -3F, -23F);

		bodyModel[182].addBox(0F, 0F, 0F, 28, 20, 2, 0F); // Box 0
		bodyModel[182].setRotationPoint(-39F, -31F, -21.5F);

		bodyModel[183].addBox(0F, 0F, 0F, 28, 20, 2, 0F); // Box 2
		bodyModel[183].setRotationPoint(-39F, -31F, 20.5F);

		bodyModel[184].addBox(0F, 0F, 0F, 4, 8, 2, 0F); // Box 3
		bodyModel[184].setRotationPoint(-15F, -39F, 20.5F);

		bodyModel[185].addBox(0F, 0F, 0F, 4, 8, 2, 0F); // Box 4
		bodyModel[185].setRotationPoint(-15F, -39F, -21.5F);

		bodyModel[186].addBox(0F, 0F, 0F, 2, 3, 1, 0F); // Box 5
		bodyModel[186].setRotationPoint(-66F, -29F, 27.5F);

		bodyModel[187].addBox(0F, 0F, 0F, 16, 8, 5, 0F); // Box 6
		bodyModel[187].setRotationPoint(-73F, -30F, 22.5F);

		bodyModel[188].addBox(0F, 0F, 0F, 2, 3, 1, 0F); // Box 7
		bodyModel[188].setRotationPoint(-49F, -29F, 27.5F);

		bodyModel[189].addBox(0F, 0F, 0F, 16, 8, 5, 0F); // Box 8
		bodyModel[189].setRotationPoint(-56F, -30F, 22.5F);

		bodyModel[190].addShapeBox(0F, 0F, 0F, 3, 13, 14, 0F,1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F, 1F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 0
		bodyModel[190].setRotationPoint(-4F, -30F, 5F);

		bodyModel[191].addShapeBox(0F, 0F, 0F, 3, 2, 12, 0F,0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 1
		bodyModel[191].setRotationPoint(-5F, -32F, 6F);

		bodyModel[192].addBox(0F, 0F, 0F, 1, 1, 6, 0F); // Box 2
		bodyModel[192].setRotationPoint(-4F, -33F, 9F);

		bodyModel[193].addBox(0F, 0F, 0F, 3, 4, 8, 0F); // Box 3
		bodyModel[193].setRotationPoint(-5F, -37F, 8F);

		bodyModel[194].addBox(0F, 0F, 0F, 15, 3, 14, 0F); // Box 4
		bodyModel[194].setRotationPoint(-4F, -17F, 5F);

		bodyModel[195].addBox(0F, 0F, 0F, 13, 3, 12, 0F); // Box 5
		bodyModel[195].setRotationPoint(-4F, -14F, 6F);

		bodyModel[196].addShapeBox(0F, 0F, 0F, 4, 2, 2, 0F,0F, 0F, 0F, 2F, -14F, 0F, 2F, -14F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 12F, 0F, -2F, 12F, 0F, 0F, 0F, 0F); // Box 6
		bodyModel[196].setRotationPoint(11F, -41F, 20.5F);

		bodyModel[197].addShapeBox(0F, 0F, 0F, 5, 9, 45, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -7F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -7F, 0F); // Box 7
		bodyModel[197].setRotationPoint(-38F, -11F, -22F);

		bodyModel[198].addBox(0F, 0F, 0F, 2, 17, 16, 0F); // Box 9
		bodyModel[198].setRotationPoint(-76.5F, -28F, 1F);

		bodyModel[199].addShapeBox(0F, 0F, 0F, 2, 11, 3, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // Box 10
		bodyModel[199].setRotationPoint(-76.5F, -39F, 1F);

		bodyModel[200].addShapeBox(0F, 0F, 0F, 2, 2, 16, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // Box 11
		bodyModel[200].setRotationPoint(-76.5F, -41F, 1F);

		bodyModel[201].addShapeBox(0F, 0F, 0F, 2, 11, 5, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // Box 12
		bodyModel[201].setRotationPoint(-76.5F, -39F, 12.5F);

		bodyModel[202].addShapeBox(0F, 0F, 0F, 1, 1, 10, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // Box 13
		bodyModel[202].setRotationPoint(-77.5F, -40F, 3.5F);

		bodyModel[203].addShapeBox(0F, 0F, 0F, 1, 11, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // Box 14
		bodyModel[203].setRotationPoint(-77.5F, -39F, 3.5F);

		bodyModel[204].addShapeBox(0F, 0F, 0F, 1, 1, 10, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // Box 15
		bodyModel[204].setRotationPoint(-77.5F, -28F, 3.5F);

		bodyModel[205].addShapeBox(0F, 0F, 0F, 1, 11, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // Box 16
		bodyModel[205].setRotationPoint(-77.5F, -39F, 12.5F);

		bodyModel[206].addShapeBox(0F, 0F, 0F, 20, 12, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 10F, 0F, 0F, 9F, 0F, 0F, 9F, 0F, 0F, 10F, 0F, 0F); // Box 17
		bodyModel[206].setRotationPoint(-66F, -23F, -17.5F);

		bodyModel[207].addShapeBox(0F, 0F, 0F, 20, 12, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 10F, 0F, 0F, 9F, 0F, 0F, 9F, 0F, 0F, 10F, 0F, 0F); // Box 18
		bodyModel[207].setRotationPoint(-66F, -23F, 17.5F);

		bodyModel[208].addBox(0F, 0F, 0F, 14, 3, 15, 0F); // Box 19
		bodyModel[208].setRotationPoint(-55F, -17F, -8F);

		bodyModel[209].addShapeBox(0F, 0F, 0F, 3, 30, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // Box 20
		bodyModel[209].setRotationPoint(-77F, -41F, -0.25F);

		bodyModel[210].addBox(0F, 0F, 0F, 5, 1, 1, 0F); // Box 21
		bodyModel[210].setRotationPoint(-5F, -24F, -22.5F);

		bodyModel[211].addShapeBox(0F, 0F, 0F, 14, 15, 3, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 22
		bodyModel[211].setRotationPoint(-55F, -32F, 4F);

		bodyModel[212].addBox(0F, 0F, 0F, 12, 12, 1, 0F); // Box 0
		bodyModel[212].setRotationPoint(-72F, -23F, -16.5F);

		bodyModel[213].addShapeBox(0F, 0F, 0F, 27, 12, 35, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 7F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 7F, 0F, 0F); // Box 1
		bodyModel[213].setRotationPoint(33F, -19F, -17F);

		bodyModel[214].addBox(0F, 0F, 0F, 88, 4, 3, 0F); // Box 2
		bodyModel[214].setRotationPoint(-77F, -45F, 20F);

		bodyModel[215].addBox(0F, 0F, 0F, 4, 4, 39, 0F); // Box 3
		bodyModel[215].setRotationPoint(-77F, -45F, -19F);

		bodyModel[216].addBox(0F, 0F, 0F, 21, 12, 8, 0F); // Box 0
		bodyModel[216].setRotationPoint(-58F, -23F, -16.5F);

		bodyModel[217].addBox(0F, 0F, 0F, 21, 1, 11, 0F); // Box 1
		bodyModel[217].setRotationPoint(-58F, -24F, -19.5F);

		bodyModel[218].addShapeBox(0F, 0F, 0F, 9, 1, 5, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 2
		bodyModel[218].setRotationPoint(-52F, -25F, -14.5F);

		bodyModel[219].addBox(0F, 0F, 0F, 11, 11, 4, 0F); // Box 3
		bodyModel[219].setRotationPoint(-53F, -35F, -19.5F);

		bodyModel[220].addBox(0F, 0F, 0F, 5, 9, 4, 0F); // Box 4
		bodyModel[220].setRotationPoint(-58F, -33F, -19.5F);

		bodyModel[221].addBox(0F, 0F, 0F, 2, 12, 2, 0F); // Box 5
		bodyModel[221].setRotationPoint(-56.5F, -45F, -19F);

		bodyModel[222].addBox(0F, 0F, 0F, 3, 2, 3, 0F); // Box 6
		bodyModel[222].setRotationPoint(-57F, -43F, -19.5F);

		bodyModel[223].addBox(0F, 0F, 0F, 3, 2, 3, 0F); // Box 7
		bodyModel[223].setRotationPoint(-57F, -37F, -19.5F);

		bodyModel[224].addBox(0F, 0F, 0F, 25, 2, 25, 0F); // Box 0
		bodyModel[224].setRotationPoint(-44F, -45F, -12F);

		bodyModel[225].addBox(0F, 0F, 0F, 15, 4, 15, 0F); // Box 1
		bodyModel[225].setRotationPoint(-39F, -53F, -7F);

		bodyModel[226].addShapeBox(0F, 0F, 0F, 4, 12, 12, 0F,0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F); // Box 2
		bodyModel[226].setRotationPoint(-37.5F, -72F, -5.5F);

		bodyModel[227].addShapeBox(0F, 0F, 0F, 4, 12, 12, 0F,0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 0F); // Box 3
		bodyModel[227].setRotationPoint(-29.5F, -72F, -5.5F);

		bodyModel[228].addBox(0F, 0F, 0F, 4, 12, 12, 0F); // Box 4
		bodyModel[228].setRotationPoint(-33.5F, -72F, -5.5F);

		bodyModel[229].addShapeBox(0F, 0F, 0F, 3, 7, 10, 0F,0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F); // Box 15
		bodyModel[229].setRotationPoint(-36.5F, -60F, -4.5F);

		bodyModel[230].addBox(0F, 0F, 0F, 4, 7, 10, 0F); // Box 16
		bodyModel[230].setRotationPoint(-33.5F, -60F, -4.5F);

		bodyModel[231].addShapeBox(0F, 0F, 0F, 3, 7, 10, 0F,0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F); // Box 17
		bodyModel[231].setRotationPoint(-29.5F, -60F, -4.5F);

		bodyModel[232].addShapeBox(0F, 0F, 0F, 4, 3, 12, 0F,-3F, 0F, -4F, 0F, 0F, -3F, 0F, 0F, -3F, -3F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F); // Box 0
		bodyModel[232].setRotationPoint(-37.5F, -75F, -5.5F);

		bodyModel[233].addShapeBox(0F, 0F, 0F, 4, 3, 12, 0F,0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 1
		bodyModel[233].setRotationPoint(-33.5F, -75F, -5.5F);

		bodyModel[234].addShapeBox(0F, 0F, 0F, 4, 3, 12, 0F,0F, 0F, -3F, -3F, 0F, -4F, -3F, 0F, -4F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 0F); // Box 3
		bodyModel[234].setRotationPoint(-29.5F, -75F, -5.5F);

		bodyModel[235].addShapeBox(0F, 0F, 0F, 1, 10, 3, 0F,0.5F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, -1F, 0.5F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, -1F); // Box 20
		bodyModel[235].setRotationPoint(-70.5F, -55F, 21F);
	}

	private void initGuns()
	{

		// Passenger 3
		ModelRendererTurbo[][] gun_2_Model = new ModelRendererTurbo[3][];

		gun_2_Model[0] = new ModelRendererTurbo[0];

		gun_2_Model[1] = new ModelRendererTurbo[0];

		gun_2_Model[2] = new ModelRendererTurbo[65];
		gun_2_Model[2][0] = new ModelRendererTurbo(this, 313, 484, textureX, textureY); // Box 10
		gun_2_Model[2][1] = new ModelRendererTurbo(this, 368, 485, textureX, textureY); // Box 11
		gun_2_Model[2][2] = new ModelRendererTurbo(this, 368, 485, textureX, textureY); // Box 12
		gun_2_Model[2][3] = new ModelRendererTurbo(this, 443, 376, textureX, textureY); // Box 42
		gun_2_Model[2][4] = new ModelRendererTurbo(this, 406, 375, textureX, textureY); // Box 43
		gun_2_Model[2][5] = new ModelRendererTurbo(this, 443, 376, textureX, textureY); // Box 44
		gun_2_Model[2][6] = new ModelRendererTurbo(this, 443, 376, textureX, textureY); // Box 45
		gun_2_Model[2][7] = new ModelRendererTurbo(this, 406, 375, textureX, textureY); // Box 46
		gun_2_Model[2][8] = new ModelRendererTurbo(this, 443, 376, textureX, textureY); // Box 47
		gun_2_Model[2][9] = new ModelRendererTurbo(this, 463, 364, textureX, textureY); // Box 48
		gun_2_Model[2][10] = new ModelRendererTurbo(this, 406, 362, textureX, textureY); // Box 49
		gun_2_Model[2][11] = new ModelRendererTurbo(this, 463, 364, textureX, textureY); // Box 50
		gun_2_Model[2][12] = new ModelRendererTurbo(this, 463, 364, textureX, textureY); // Box 51
		gun_2_Model[2][13] = new ModelRendererTurbo(this, 406, 362, textureX, textureY); // Box 52
		gun_2_Model[2][14] = new ModelRendererTurbo(this, 463, 364, textureX, textureY); // Box 53
		gun_2_Model[2][15] = new ModelRendererTurbo(this, 463, 351, textureX, textureY); // Box 54
		gun_2_Model[2][16] = new ModelRendererTurbo(this, 406, 349, textureX, textureY); // Box 55
		gun_2_Model[2][17] = new ModelRendererTurbo(this, 463, 351, textureX, textureY); // Box 56
		gun_2_Model[2][18] = new ModelRendererTurbo(this, 463, 351, textureX, textureY); // Box 57
		gun_2_Model[2][19] = new ModelRendererTurbo(this, 406, 349, textureX, textureY); // Box 58
		gun_2_Model[2][20] = new ModelRendererTurbo(this, 463, 351, textureX, textureY); // Box 59
		gun_2_Model[2][21] = new ModelRendererTurbo(this, 437, 391, textureX, textureY); // Box 60
		gun_2_Model[2][22] = new ModelRendererTurbo(this, 406, 390, textureX, textureY); // Box 61
		gun_2_Model[2][23] = new ModelRendererTurbo(this, 437, 391, textureX, textureY); // Box 62
		gun_2_Model[2][24] = new ModelRendererTurbo(this, 437, 391, textureX, textureY); // Box 63
		gun_2_Model[2][25] = new ModelRendererTurbo(this, 406, 390, textureX, textureY); // Box 64
		gun_2_Model[2][26] = new ModelRendererTurbo(this, 437, 391, textureX, textureY); // Box 65
		gun_2_Model[2][27] = new ModelRendererTurbo(this, 443, 376, textureX, textureY); // Box 66
		gun_2_Model[2][28] = new ModelRendererTurbo(this, 406, 375, textureX, textureY); // Box 67
		gun_2_Model[2][29] = new ModelRendererTurbo(this, 443, 376, textureX, textureY); // Box 68
		gun_2_Model[2][30] = new ModelRendererTurbo(this, 443, 376, textureX, textureY); // Box 69
		gun_2_Model[2][31] = new ModelRendererTurbo(this, 406, 375, textureX, textureY); // Box 70
		gun_2_Model[2][32] = new ModelRendererTurbo(this, 443, 376, textureX, textureY); // Box 71
		gun_2_Model[2][33] = new ModelRendererTurbo(this, 463, 364, textureX, textureY); // Box 72
		gun_2_Model[2][34] = new ModelRendererTurbo(this, 406, 362, textureX, textureY); // Box 73
		gun_2_Model[2][35] = new ModelRendererTurbo(this, 463, 364, textureX, textureY); // Box 74
		gun_2_Model[2][36] = new ModelRendererTurbo(this, 463, 364, textureX, textureY); // Box 75
		gun_2_Model[2][37] = new ModelRendererTurbo(this, 406, 362, textureX, textureY); // Box 76
		gun_2_Model[2][38] = new ModelRendererTurbo(this, 463, 364, textureX, textureY); // Box 77
		gun_2_Model[2][39] = new ModelRendererTurbo(this, 463, 351, textureX, textureY); // Box 78
		gun_2_Model[2][40] = new ModelRendererTurbo(this, 406, 349, textureX, textureY); // Box 79
		gun_2_Model[2][41] = new ModelRendererTurbo(this, 463, 351, textureX, textureY); // Box 80
		gun_2_Model[2][42] = new ModelRendererTurbo(this, 463, 351, textureX, textureY); // Box 81
		gun_2_Model[2][43] = new ModelRendererTurbo(this, 406, 349, textureX, textureY); // Box 82
		gun_2_Model[2][44] = new ModelRendererTurbo(this, 463, 351, textureX, textureY); // Box 83
		gun_2_Model[2][45] = new ModelRendererTurbo(this, 437, 391, textureX, textureY); // Box 84
		gun_2_Model[2][46] = new ModelRendererTurbo(this, 406, 390, textureX, textureY); // Box 85
		gun_2_Model[2][47] = new ModelRendererTurbo(this, 437, 391, textureX, textureY); // Box 86
		gun_2_Model[2][48] = new ModelRendererTurbo(this, 437, 391, textureX, textureY); // Box 87
		gun_2_Model[2][49] = new ModelRendererTurbo(this, 406, 390, textureX, textureY); // Box 88
		gun_2_Model[2][50] = new ModelRendererTurbo(this, 437, 391, textureX, textureY); // Box 89
		gun_2_Model[2][51] = new ModelRendererTurbo(this, 414, 456, textureX, textureY); // Box 4
		gun_2_Model[2][52] = new ModelRendererTurbo(this, 414, 436, textureX, textureY); // Box 5
		gun_2_Model[2][53] = new ModelRendererTurbo(this, 414, 421, textureX, textureY); // Box 6
		gun_2_Model[2][54] = new ModelRendererTurbo(this, 414, 421, textureX, textureY); // Box 7
		gun_2_Model[2][55] = new ModelRendererTurbo(this, 423, 456, textureX, textureY); // Box 8
		gun_2_Model[2][56] = new ModelRendererTurbo(this, 441, 435, textureX, textureY); // Box 9
		gun_2_Model[2][57] = new ModelRendererTurbo(this, 441, 443, textureX, textureY); // Box 10
		gun_2_Model[2][58] = new ModelRendererTurbo(this, 441, 435, textureX, textureY); // Box 11
		gun_2_Model[2][59] = new ModelRendererTurbo(this, 441, 435, textureX, textureY); // Box 12
		gun_2_Model[2][60] = new ModelRendererTurbo(this, 441, 443, textureX, textureY); // Box 13
		gun_2_Model[2][61] = new ModelRendererTurbo(this, 441, 435, textureX, textureY); // Box 14
		gun_2_Model[2][62] = new ModelRendererTurbo(this, 434, 456, textureX, textureY); // Box 15
		gun_2_Model[2][63] = new ModelRendererTurbo(this, 414, 469, textureX, textureY); // Box 17
		gun_2_Model[2][64] = new ModelRendererTurbo(this, 423, 465, textureX, textureY); // Box 18

		gun_2_Model[2][0].addBox(-3.5F, -1.5F, -9.5F, 7, 3, 20, 0F); // Box 10

		gun_2_Model[2][1].addShapeBox(-3.5F, -3.5F, -9.5F, 7, 2, 20, 0F,-2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 11

		gun_2_Model[2][2].addShapeBox(-3.5F, 1.5F, -9.5F, 7, 2, 20, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F); // Box 12

		gun_2_Model[2][3].addShapeBox(-4F, -5F, 18.5F, 8, 3, 10, 0F,0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 42

		gun_2_Model[2][4].addBox(-4F, -2F, 18.5F, 8, 4, 10, 0F); // Box 43

		gun_2_Model[2][5].addShapeBox(-4F, 2F, 18.5F, 8, 3, 10, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 44

		gun_2_Model[2][6].addShapeBox(-4F, -5F, 8.5F, 8, 3, 10, 0F,0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 45

		gun_2_Model[2][7].addBox(-4F, -2F, 8.5F, 8, 4, 10, 0F); // Box 46

		gun_2_Model[2][8].addShapeBox(-4F, 2F, 8.5F, 8, 3, 10, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 47

		gun_2_Model[2][9].addShapeBox(4F, 2F, 9.5F, 20, 2, 8, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F); // Box 48

		gun_2_Model[2][10].addBox(4F, -2F, 9.5F, 20, 4, 8, 0F); // Box 49

		gun_2_Model[2][11].addShapeBox(4F, -4F, 9.5F, 20, 2, 8, 0F,0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 50

		gun_2_Model[2][12].addShapeBox(4F, 2F, 19.5F, 20, 2, 8, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F); // Box 51

		gun_2_Model[2][13].addBox(4F, -2F, 19.5F, 20, 4, 8, 0F); // Box 52

		gun_2_Model[2][14].addShapeBox(4F, -4F, 19.5F, 20, 2, 8, 0F,0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 53

		gun_2_Model[2][15].addShapeBox(-24F, -4F, 19.5F, 20, 2, 8, 0F,0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 54

		gun_2_Model[2][16].addBox(-24F, -2F, 19.5F, 20, 4, 8, 0F); // Box 55

		gun_2_Model[2][17].addShapeBox(-24F, 2F, 19.5F, 20, 2, 8, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F); // Box 56

		gun_2_Model[2][18].addShapeBox(-24F, -4F, 9.5F, 20, 2, 8, 0F,0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 57

		gun_2_Model[2][19].addBox(-24F, -2F, 9.5F, 20, 4, 8, 0F); // Box 58

		gun_2_Model[2][20].addShapeBox(-24F, 2F, 9.5F, 20, 2, 8, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F); // Box 59

		gun_2_Model[2][21].addShapeBox(-29F, 2F, 8.5F, 5, 3, 10, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 60

		gun_2_Model[2][22].addBox(-29F, -2F, 8.5F, 5, 4, 10, 0F); // Box 61

		gun_2_Model[2][23].addShapeBox(-29F, -5F, 8.5F, 5, 3, 10, 0F,0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 62

		gun_2_Model[2][24].addShapeBox(-29F, 2F, 18.5F, 5, 3, 10, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 63

		gun_2_Model[2][25].addBox(-29F, -2F, 18.5F, 5, 4, 10, 0F); // Box 64

		gun_2_Model[2][26].addShapeBox(-29F, -5F, 18.5F, 5, 3, 10, 0F,0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 65

		gun_2_Model[2][27].addShapeBox(-4F, -5F, -17.5F, 8, 3, 10, 0F,0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 66

		gun_2_Model[2][28].addBox(-4F, -2F, -17.5F, 8, 4, 10, 0F); // Box 67

		gun_2_Model[2][29].addShapeBox(-4F, 2F, -17.5F, 8, 3, 10, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 68

		gun_2_Model[2][30].addShapeBox(-4F, -5F, -27.5F, 8, 3, 10, 0F,0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 69

		gun_2_Model[2][31].addBox(-4F, -2F, -27.5F, 8, 4, 10, 0F); // Box 70

		gun_2_Model[2][32].addShapeBox(-4F, 2F, -27.5F, 8, 3, 10, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 71

		gun_2_Model[2][33].addShapeBox(4F, 2F, -26.5F, 20, 2, 8, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F); // Box 72

		gun_2_Model[2][34].addBox(4F, -2F, -26.5F, 20, 4, 8, 0F); // Box 73

		gun_2_Model[2][35].addShapeBox(4F, -4F, -26.5F, 20, 2, 8, 0F,0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 74

		gun_2_Model[2][36].addShapeBox(4F, 2F, -16.5F, 20, 2, 8, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F); // Box 75

		gun_2_Model[2][37].addBox(4F, -2F, -16.5F, 20, 4, 8, 0F); // Box 76

		gun_2_Model[2][38].addShapeBox(4F, -4F, -16.5F, 20, 2, 8, 0F,0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 77

		gun_2_Model[2][39].addShapeBox(-24F, -4F, -16.5F, 20, 2, 8, 0F,0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 78

		gun_2_Model[2][40].addBox(-24F, -2F, -16.5F, 20, 4, 8, 0F); // Box 79

		gun_2_Model[2][41].addShapeBox(-24F, 2F, -16.5F, 20, 2, 8, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F); // Box 80

		gun_2_Model[2][42].addShapeBox(-24F, -4F, -26.5F, 20, 2, 8, 0F,0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 81

		gun_2_Model[2][43].addBox(-24F, -2F, -26.5F, 20, 4, 8, 0F); // Box 82

		gun_2_Model[2][44].addShapeBox(-24F, 2F, -26.5F, 20, 2, 8, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F); // Box 83

		gun_2_Model[2][45].addShapeBox(-29F, 2F, -27.5F, 5, 3, 10, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 84

		gun_2_Model[2][46].addBox(-29F, -2F, -27.5F, 5, 4, 10, 0F); // Box 85

		gun_2_Model[2][47].addShapeBox(-29F, -5F, -27.5F, 5, 3, 10, 0F,0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 86

		gun_2_Model[2][48].addShapeBox(-29F, 2F, -17.5F, 5, 3, 10, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 87

		gun_2_Model[2][49].addBox(-29F, -2F, -17.5F, 5, 4, 10, 0F); // Box 88

		gun_2_Model[2][50].addShapeBox(-29F, -5F, -17.5F, 5, 3, 10, 0F,0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 89

		gun_2_Model[2][51].addBox(-1F, -17.5F, -0.5F, 2, 10, 2, 0F); // Box 4

		gun_2_Model[2][52].addShapeBox(0F, -20.5F, -5.5F, 1, 7, 12, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F); // Box 5

		gun_2_Model[2][53].addShapeBox(2F, -13.5F, -5.5F, 1, 2, 12, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F); // Box 6

		gun_2_Model[2][54].addShapeBox(0F, -22.5F, -5.5F, 1, 2, 12, 0F,-1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 7

		gun_2_Model[2][55].addShapeBox(0F, -18.5F, -1.5F, 1, 4, 4, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F); // Box 8

		gun_2_Model[2][56].addShapeBox(2F, -13.5F, -10.5F, 1, 2, 5, 0F,-2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, 4F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F); // Box 9

		gun_2_Model[2][57].addShapeBox(0F, -20.5F, -10.5F, 1, 7, 5, 0F,-2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, 4F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F); // Box 10

		gun_2_Model[2][58].addShapeBox(0F, -22.5F, -10.5F, 1, 2, 5, 0F,-3F, 0F, 0F, 3F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 11

		gun_2_Model[2][59].addShapeBox(0F, -22.5F, 6.5F, 1, 2, 5, 0F,-1F, 0F, 0F, 1F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F); // Box 12

		gun_2_Model[2][60].addShapeBox(0F, -20.5F, 6.5F, 1, 7, 5, 0F,0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F, 4F, 0F, 0F, -4F, 0F, 0F); // Box 13

		gun_2_Model[2][61].addShapeBox(2F, -13.5F, 6.5F, 1, 2, 5, 0F,0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F, 4F, 0F, 0F, -4F, 0F, 0F); // Box 14

		gun_2_Model[2][62].addShapeBox(5F, -11.5F, -0.5F, 6, 2, 2, 0F,0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F); // Box 15

		gun_2_Model[2][63].addBox(9F, -14.5F, -0.5F, 2, 3, 2, 0F); // Box 17

		gun_2_Model[2][64].addBox(9.5F, -16.5F, -1F, 1, 2, 3, 0F); // Box 18

		//Gun Origin
		for (ModelRendererTurbo gunPart : gun_2_Model[2])
		{
			gunPart.setRotationPoint(-32F, -64F, 0F);
		}


		registerGunModel("Katyusha_Missiles", gun_2_Model);
	}
}