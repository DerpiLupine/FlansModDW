package com.flansmod.client.model.dwvehicles; //Path where the model is located

import com.flansmod.client.model.ModelVehicle;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelKattushaMAW extends ModelVehicle //Same as Filename
{
	int textureX = 1024;
	int textureY = 1024;

	public ModelKattushaMAW() //Same as Filename
	{
		bodyModel = new ModelRendererTurbo[167];
		leftFrontWheelModel = new ModelRendererTurbo[13];
		rightFrontWheelModel = new ModelRendererTurbo[13];
		leftBackWheelModel = new ModelRendererTurbo[13];
		rightBackWheelModel = new ModelRendererTurbo[13];

		initbodyModel_1();
		initleftFrontWheelModel_1();
		initrightFrontWheelModel_1();
		initleftBackWheelModel_1();
		initrightBackWheelModel_1();

		translateAll(0F, 0F, 0F);


		flipAll();
	}

	private void initbodyModel_1()
	{
		bodyModel[0] = new ModelRendererTurbo(this, 136, 66, textureX, textureY); // antenna1
		bodyModel[1] = new ModelRendererTurbo(this, 131, 45, textureX, textureY); // antenna2
		bodyModel[2] = new ModelRendererTurbo(this, 145, 67, textureX, textureY); // antenna3
		bodyModel[3] = new ModelRendererTurbo(this, 44, 33, textureX, textureY); // axleLeftBack1
		bodyModel[4] = new ModelRendererTurbo(this, 44, 33, textureX, textureY); // axleLeftBack2
		bodyModel[5] = new ModelRendererTurbo(this, 44, 33, textureX, textureY); // axleLeftBack3
		bodyModel[6] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // axleLeftFront1
		bodyModel[7] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // axleLeftFront2
		bodyModel[8] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // axleLeftFront3
		bodyModel[9] = new ModelRendererTurbo(this, 44, 17, textureX, textureY); // axleRightBack1
		bodyModel[10] = new ModelRendererTurbo(this, 44, 17, textureX, textureY); // axleRightBack2
		bodyModel[11] = new ModelRendererTurbo(this, 44, 17, textureX, textureY); // axleRightBack3
		bodyModel[12] = new ModelRendererTurbo(this, 44, 1, textureX, textureY); // axleRightFront1
		bodyModel[13] = new ModelRendererTurbo(this, 44, 1, textureX, textureY); // axleRightFront2
		bodyModel[14] = new ModelRendererTurbo(this, 44, 1, textureX, textureY); // axleRightFront3
		bodyModel[15] = new ModelRendererTurbo(this, 1, 202, textureX, textureY); // backBody1
		bodyModel[16] = new ModelRendererTurbo(this, 1, 202, textureX, textureY); // backBody2
		bodyModel[17] = new ModelRendererTurbo(this, 312, 252, textureX, textureY); // backBody3
		bodyModel[18] = new ModelRendererTurbo(this, 57, 209, textureX, textureY); // backBody4
		bodyModel[19] = new ModelRendererTurbo(this, 173, 209, textureX, textureY); // backBody5
		bodyModel[20] = new ModelRendererTurbo(this, 125, 224, textureX, textureY); // backDoor1
		bodyModel[21] = new ModelRendererTurbo(this, 20, 205, textureX, textureY); // backDoor2
		bodyModel[22] = new ModelRendererTurbo(this, 136, 220, textureX, textureY); // backDoor3
		bodyModel[23] = new ModelRendererTurbo(this, 57, 217, textureX, textureY); // backDoor4
		bodyModel[24] = new ModelRendererTurbo(this, 87, 2, textureX, textureY); // backFlap1
		bodyModel[25] = new ModelRendererTurbo(this, 87, 2, textureX, textureY); // backFlap2
		bodyModel[26] = new ModelRendererTurbo(this, 50, 266, textureX, textureY); // backWindow3
		bodyModel[27] = new ModelRendererTurbo(this, 50, 266, textureX, textureY); // backWindow4
		bodyModel[28] = new ModelRendererTurbo(this, 73, 265, textureX, textureY); // backWindow7
		bodyModel[29] = new ModelRendererTurbo(this, 73, 265, textureX, textureY); // backWindow8
		bodyModel[30] = new ModelRendererTurbo(this, 584, 164, textureX, textureY); // body1
		bodyModel[31] = new ModelRendererTurbo(this, 312, 214, textureX, textureY); // body10
		bodyModel[32] = new ModelRendererTurbo(this, 113, 121, textureX, textureY); // body11
		bodyModel[33] = new ModelRendererTurbo(this, 1, 88, textureX, textureY); // body12
		bodyModel[34] = new ModelRendererTurbo(this, 117, 112, textureX, textureY); // body13
		bodyModel[35] = new ModelRendererTurbo(this, 113, 121, textureX, textureY); // body15
		bodyModel[36] = new ModelRendererTurbo(this, 77, 115, textureX, textureY); // body16
		bodyModel[37] = new ModelRendererTurbo(this, 77, 115, textureX, textureY); // body17
		bodyModel[38] = new ModelRendererTurbo(this, 113, 115, textureX, textureY); // body18
		bodyModel[39] = new ModelRendererTurbo(this, 16, 88, textureX, textureY); // body19
		bodyModel[40] = new ModelRendererTurbo(this, 35, 118, textureX, textureY); // body2
		bodyModel[41] = new ModelRendererTurbo(this, 205, 152, textureX, textureY); // body21
		bodyModel[42] = new ModelRendererTurbo(this, 62, 167, textureX, textureY); // body22
		bodyModel[43] = new ModelRendererTurbo(this, 45, 110, textureX, textureY); // body24
		bodyModel[44] = new ModelRendererTurbo(this, 62, 189, textureX, textureY); // body25
		bodyModel[45] = new ModelRendererTurbo(this, 111, 189, textureX, textureY); // body26
		bodyModel[46] = new ModelRendererTurbo(this, 184, 169, textureX, textureY); // body27
		bodyModel[47] = new ModelRendererTurbo(this, 62, 156, textureX, textureY); // body29
		bodyModel[48] = new ModelRendererTurbo(this, 685, 214, textureX, textureY); // body3
		bodyModel[49] = new ModelRendererTurbo(this, 162, 184, textureX, textureY); // body30
		bodyModel[50] = new ModelRendererTurbo(this, 62, 200, textureX, textureY); // body31
		bodyModel[51] = new ModelRendererTurbo(this, 141, 184, textureX, textureY); // body32
		bodyModel[52] = new ModelRendererTurbo(this, 184, 156, textureX, textureY); // body33
		bodyModel[53] = new ModelRendererTurbo(this, 111, 178, textureX, textureY); // body34
		bodyModel[54] = new ModelRendererTurbo(this, 62, 178, textureX, textureY); // body35
		bodyModel[55] = new ModelRendererTurbo(this, 584, 316, textureX, textureY); // body4
		bodyModel[56] = new ModelRendererTurbo(this, 584, 1, textureX, textureY); // body5
		bodyModel[57] = new ModelRendererTurbo(this, 1, 122, textureX, textureY); // body6
		bodyModel[58] = new ModelRendererTurbo(this, 705, 115, textureX, textureY); // body7
		bodyModel[59] = new ModelRendererTurbo(this, 35, 118, textureX, textureY); // body8
		bodyModel[60] = new ModelRendererTurbo(this, 312, 58, textureX, textureY); // body9
		bodyModel[61] = new ModelRendererTurbo(this, 104, 81, textureX, textureY); // bodyFrontVent1
		bodyModel[62] = new ModelRendererTurbo(this, 31, 81, textureX, textureY); // bodyFrontVent2
		bodyModel[63] = new ModelRendererTurbo(this, 670, 18, textureX, textureY); // bodyTop11
		bodyModel[64] = new ModelRendererTurbo(this, 109, 12, textureX, textureY); // bodyTop11
		bodyModel[65] = new ModelRendererTurbo(this, 312, 158, textureX, textureY); // bodyTop3
		bodyModel[66] = new ModelRendererTurbo(this, 312, 110, textureX, textureY); // bodyTop4
		bodyModel[67] = new ModelRendererTurbo(this, 584, 272, textureX, textureY); // bodyTop9
		bodyModel[68] = new ModelRendererTurbo(this, 421, 187, textureX, textureY); // BOX_placeholder
		bodyModel[69] = new ModelRendererTurbo(this, 78, 86, textureX, textureY); // bumper1
		bodyModel[70] = new ModelRendererTurbo(this, 78, 86, textureX, textureY); // bumper2
		bodyModel[71] = new ModelRendererTurbo(this, 179, 23, textureX, textureY); // doorFrontLeft1
		bodyModel[72] = new ModelRendererTurbo(this, 109, 45, textureX, textureY); // doorFrontLeft2
		bodyModel[73] = new ModelRendererTurbo(this, 242, 42, textureX, textureY); // doorFrontLeft3
		bodyModel[74] = new ModelRendererTurbo(this, 166, 42, textureX, textureY); // doorFrontLeft5
		bodyModel[75] = new ModelRendererTurbo(this, 232, 23, textureX, textureY); // doorFrontRight1
		bodyModel[76] = new ModelRendererTurbo(this, 242, 57, textureX, textureY); // doorFrontRight2
		bodyModel[77] = new ModelRendererTurbo(this, 242, 47, textureX, textureY); // doorFrontRight3
		bodyModel[78] = new ModelRendererTurbo(this, 120, 45, textureX, textureY); // doorFrontRight4
		bodyModel[79] = new ModelRendererTurbo(this, 154, 45, textureX, textureY); // doorHinge1
		bodyModel[80] = new ModelRendererTurbo(this, 154, 45, textureX, textureY); // doorHinge3
		bodyModel[81] = new ModelRendererTurbo(this, 78, 267, textureX, textureY); // doorHinge5
		bodyModel[82] = new ModelRendererTurbo(this, 78, 267, textureX, textureY); // doorHinge6
		bodyModel[83] = new ModelRendererTurbo(this, 1, 325, textureX, textureY); // interior1
		bodyModel[84] = new ModelRendererTurbo(this, 1, 253, textureX, textureY); // interior4
		bodyModel[85] = new ModelRendererTurbo(this, 1, 438, textureX, textureY); // interior6
		bodyModel[86] = new ModelRendererTurbo(this, 45, 240, textureX, textureY); // interiorComms1
		bodyModel[87] = new ModelRendererTurbo(this, 36, 417, textureX, textureY); // interiorComms2
		bodyModel[88] = new ModelRendererTurbo(this, 36, 427, textureX, textureY); // interiorComms3
		bodyModel[89] = new ModelRendererTurbo(this, 1, 278, textureX, textureY); // interiorDashboard
		bodyModel[90] = new ModelRendererTurbo(this, 82, 438, textureX, textureY); // interiorFrontRightSeat1
		bodyModel[91] = new ModelRendererTurbo(this, 82, 454, textureX, textureY); // interiorFrontRightSeat2
		bodyModel[92] = new ModelRendererTurbo(this, 1, 410, textureX, textureY); // interiorFrontRightSeat3
		bodyModel[93] = new ModelRendererTurbo(this, 1, 395, textureX, textureY); // interiorFrontRightSeat4
		bodyModel[94] = new ModelRendererTurbo(this, 20, 399, textureX, textureY); // interiorFrontRightSeat5
		bodyModel[95] = new ModelRendererTurbo(this, 1, 382, textureX, textureY); // interiorFrontRightSeat6
		bodyModel[96] = new ModelRendererTurbo(this, 73, 419, textureX, textureY); // interiorGearBox
		bodyModel[97] = new ModelRendererTurbo(this, 178, 69, textureX, textureY); // leftHeadlight1
		bodyModel[98] = new ModelRendererTurbo(this, 178, 69, textureX, textureY); // leftHeadlight2
		bodyModel[99] = new ModelRendererTurbo(this, 178, 69, textureX, textureY); // leftHeadlight3
		bodyModel[100] = new ModelRendererTurbo(this, 179, 42, textureX, textureY); // plateBack
		bodyModel[101] = new ModelRendererTurbo(this, 212, 45, textureX, textureY); // plateFront
		bodyModel[102] = new ModelRendererTurbo(this, 145, 45, textureX, textureY); // radioPoint1
		bodyModel[103] = new ModelRendererTurbo(this, 136, 45, textureX, textureY); // radioPoint2
		bodyModel[104] = new ModelRendererTurbo(this, 136, 59, textureX, textureY); // radioPoint3
		bodyModel[105] = new ModelRendererTurbo(this, 203, 69, textureX, textureY); // rightHeadlight1
		bodyModel[106] = new ModelRendererTurbo(this, 203, 69, textureX, textureY); // rightHeadlight2
		bodyModel[107] = new ModelRendererTurbo(this, 203, 69, textureX, textureY); // rightHeadlight3
		bodyModel[108] = new ModelRendererTurbo(this, 584, 107, textureX, textureY); // rimPart1
		bodyModel[109] = new ModelRendererTurbo(this, 1, 75, textureX, textureY); // rimPart10
		bodyModel[110] = new ModelRendererTurbo(this, 411, 249, textureX, textureY); // rimPart2
		bodyModel[111] = new ModelRendererTurbo(this, 584, 50, textureX, textureY); // rimPart3
		bodyModel[112] = new ModelRendererTurbo(this, 312, 1, textureX, textureY); // rimPart4_Main
		bodyModel[113] = new ModelRendererTurbo(this, 1, 62, textureX, textureY); // rimPart5
		bodyModel[114] = new ModelRendererTurbo(this, 1, 49, textureX, textureY); // rimPart6
		bodyModel[115] = new ModelRendererTurbo(this, 1, 75, textureX, textureY); // rimPart7
		bodyModel[116] = new ModelRendererTurbo(this, 1, 62, textureX, textureY); // rimPart8
		bodyModel[117] = new ModelRendererTurbo(this, 1, 49, textureX, textureY); // rimPart9
		bodyModel[118] = new ModelRendererTurbo(this, 163, 82, textureX, textureY); // shockFront1
		bodyModel[119] = new ModelRendererTurbo(this, 163, 82, textureX, textureY); // shockFront2
		bodyModel[120] = new ModelRendererTurbo(this, 163, 82, textureX, textureY); // shockFront3
		bodyModel[121] = new ModelRendererTurbo(this, 90, 369, textureX, textureY); // steeringWheel1
		bodyModel[122] = new ModelRendererTurbo(this, 90, 369, textureX, textureY); // steeringWheel2
		bodyModel[123] = new ModelRendererTurbo(this, 90, 369, textureX, textureY); // steeringWheel3
		bodyModel[124] = new ModelRendererTurbo(this, 671, 285, textureX, textureY); // underBody1
		bodyModel[125] = new ModelRendererTurbo(this, 313, 306, textureX, textureY); // underBody2
		bodyModel[126] = new ModelRendererTurbo(this, 152, 38, textureX, textureY); // wingLeftMirror1
		bodyModel[127] = new ModelRendererTurbo(this, 152, 23, textureX, textureY); // wingLeftMirror2
		bodyModel[128] = new ModelRendererTurbo(this, 159, 38, textureX, textureY); // wingRightMirror1
		bodyModel[129] = new ModelRendererTurbo(this, 165, 23, textureX, textureY); // wingRightMirror2
		bodyModel[130] = new ModelRendererTurbo(this, 713, 69, textureX, textureY); // wireFrame1
		bodyModel[131] = new ModelRendererTurbo(this, 713, 69, textureX, textureY); // wireFrame2
		bodyModel[132] = new ModelRendererTurbo(this, 109, 1, textureX, textureY); // wireFrame3
		bodyModel[133] = new ModelRendererTurbo(this, 1, 179, textureX, textureY); // Box 0
		bodyModel[134] = new ModelRendererTurbo(this, 1, 156, textureX, textureY); // Box 2
		bodyModel[135] = new ModelRendererTurbo(this, 128, 189, textureX, textureY); // Box 3
		bodyModel[136] = new ModelRendererTurbo(this, 128, 178, textureX, textureY); // Box 4
		bodyModel[137] = new ModelRendererTurbo(this, 63, 271, textureX, textureY); // Box 5
		bodyModel[138] = new ModelRendererTurbo(this, 1, 239, textureX, textureY); // Box 6
		bodyModel[139] = new ModelRendererTurbo(this, 63, 271, textureX, textureY); // Box 7
		bodyModel[140] = new ModelRendererTurbo(this, 1, 239, textureX, textureY); // Box 8
		bodyModel[141] = new ModelRendererTurbo(this, 1, 410, textureX, textureY); // Box 0
		bodyModel[142] = new ModelRendererTurbo(this, 1, 395, textureX, textureY); // Box 1
		bodyModel[143] = new ModelRendererTurbo(this, 20, 399, textureX, textureY); // Box 2
		bodyModel[144] = new ModelRendererTurbo(this, 1, 382, textureX, textureY); // Box 3
		bodyModel[145] = new ModelRendererTurbo(this, 82, 454, textureX, textureY); // Box 4
		bodyModel[146] = new ModelRendererTurbo(this, 82, 438, textureX, textureY); // Box 5
		bodyModel[147] = new ModelRendererTurbo(this, 242, 52, textureX, textureY); // Box 6
		bodyModel[148] = new ModelRendererTurbo(this, 584, 217, textureX, textureY); // Box 7
		bodyModel[149] = new ModelRendererTurbo(this, 51, 283, textureX, textureY); // Box 9
		bodyModel[150] = new ModelRendererTurbo(this, 125, 224, textureX, textureY); // Box 10
		bodyModel[151] = new ModelRendererTurbo(this, 136, 220, textureX, textureY); // Box 11
		bodyModel[152] = new ModelRendererTurbo(this, 57, 217, textureX, textureY); // Box 12
		bodyModel[153] = new ModelRendererTurbo(this, 50, 266, textureX, textureY); // Box 13
		bodyModel[154] = new ModelRendererTurbo(this, 73, 265, textureX, textureY); // Box 14
		bodyModel[155] = new ModelRendererTurbo(this, 50, 266, textureX, textureY); // Box 15
		bodyModel[156] = new ModelRendererTurbo(this, 73, 265, textureX, textureY); // Box 16
		bodyModel[157] = new ModelRendererTurbo(this, 141, 170, textureX, textureY); // Box 17
		bodyModel[158] = new ModelRendererTurbo(this, 141, 156, textureX, textureY); // Box 18
		bodyModel[159] = new ModelRendererTurbo(this, 1, 364, textureX, textureY); // Box 19
		bodyModel[160] = new ModelRendererTurbo(this, 116, 207, textureX, textureY); // Box 20
		bodyModel[161] = new ModelRendererTurbo(this, 166, 39, textureX, textureY); // Box 21
		bodyModel[162] = new ModelRendererTurbo(this, 1, 454, textureX, textureY); // Box 22
		bodyModel[163] = new ModelRendererTurbo(this, 79, 240, textureX, textureY); // Box 0
		bodyModel[164] = new ModelRendererTurbo(this, 312, 166, textureX, textureY); // Box 1
		bodyModel[165] = new ModelRendererTurbo(this, 312, 341, textureX, textureY); // Box 2
		bodyModel[166] = new ModelRendererTurbo(this, 687, 170, textureX, textureY); // Box 3

		bodyModel[0].addBox(0F, 0F, 0F, 2, 3, 2, 0F); // antenna1
		bodyModel[0].setRotationPoint(25F, -29F, -20F);

		bodyModel[1].addBox(0F, 0F, 0F, 1, 24, 1, 0F); // antenna2
		bodyModel[1].setRotationPoint(25.5F, -53F, -19.5F);

		bodyModel[2].addBox(0F, 0F, 0F, 2, 2, 2, 0F); // antenna3
		bodyModel[2].setRotationPoint(25F, -55F, -20F);

		bodyModel[3].addShapeBox(0F, 0F, 0F, 9, 3, 12, 0F,-3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // axleLeftBack1
		bodyModel[3].setRotationPoint(-60.5F, -7.5F, 16F);

		bodyModel[4].addBox(0F, 0F, 0F, 9, 3, 12, 0F); // axleLeftBack2
		bodyModel[4].setRotationPoint(-60.5F, -4.5F, 16F);

		bodyModel[5].addShapeBox(0F, 0F, 0F, 9, 3, 12, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F); // axleLeftBack3
		bodyModel[5].setRotationPoint(-60.5F, -1.5F, 16F);

		bodyModel[6].addShapeBox(0F, 0F, 0F, 9, 3, 12, 0F,-3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // axleLeftFront1
		bodyModel[6].setRotationPoint(39.5F, -7.5F, 16F);

		bodyModel[7].addBox(0F, 0F, 0F, 9, 3, 12, 0F); // axleLeftFront2
		bodyModel[7].setRotationPoint(39.5F, -4.5F, 16F);

		bodyModel[8].addShapeBox(0F, 0F, 0F, 9, 3, 12, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F); // axleLeftFront3
		bodyModel[8].setRotationPoint(39.5F, -1.5F, 16F);

		bodyModel[9].addShapeBox(0F, 0F, 0F, 9, 3, 12, 0F,-3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // axleRightBack1
		bodyModel[9].setRotationPoint(-60.5F, -7.5F, -27F);

		bodyModel[10].addBox(0F, 0F, 0F, 9, 3, 12, 0F); // axleRightBack2
		bodyModel[10].setRotationPoint(-60.5F, -4.5F, -27F);

		bodyModel[11].addShapeBox(0F, 0F, 0F, 9, 3, 12, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F); // axleRightBack3
		bodyModel[11].setRotationPoint(-60.5F, -1.5F, -27F);

		bodyModel[12].addShapeBox(0F, 0F, 0F, 9, 3, 12, 0F,-3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // axleRightFront1
		bodyModel[12].setRotationPoint(39.5F, -7.5F, -27F);

		bodyModel[13].addBox(0F, 0F, 0F, 9, 3, 12, 0F); // axleRightFront2
		bodyModel[13].setRotationPoint(39.5F, -4.5F, -27F);

		bodyModel[14].addShapeBox(0F, 0F, 0F, 9, 3, 12, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F); // axleRightFront3
		bodyModel[14].setRotationPoint(39.5F, -1.5F, -27F);

		bodyModel[15].addBox(0F, 0F, 0F, 3, 30, 6, 0F); // backBody1
		bodyModel[15].setRotationPoint(-77F, -41F, 17F);

		bodyModel[16].addBox(0F, 0F, 0F, 3, 30, 6, 0F); // backBody2
		bodyModel[16].setRotationPoint(-77F, -41F, -22F);

		bodyModel[17].addBox(0F, 0F, 0F, 4, 8, 45, 0F); // backBody3
		bodyModel[17].setRotationPoint(-78F, -11F, -22F);

		bodyModel[18].addBox(0F, 0F, 0F, 4, 4, 25, 0F); // backBody4
		bodyModel[18].setRotationPoint(-77F, -4F, -12F);

		bodyModel[19].addBox(0F, 0F, 0F, 8, 2, 27, 0F); // backBody5
		bodyModel[19].setRotationPoint(-82F, -1F, -13F);

		bodyModel[20].addShapeBox(0F, 0F, 0F, 2, 11, 3, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // backDoor1
		bodyModel[20].setRotationPoint(-76.5F, -39F, -3.5F);

		bodyModel[21].addBox(0F, 0F, 0F, 2, 17, 16, 0F); // backDoor2
		bodyModel[21].setRotationPoint(-76.5F, -28F, -16F);

		bodyModel[22].addShapeBox(0F, 0F, 0F, 2, 2, 16, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // backDoor3
		bodyModel[22].setRotationPoint(-76.5F, -41F, -16.5F);

		bodyModel[23].addShapeBox(0F, 0F, 0F, 2, 11, 5, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // backDoor4
		bodyModel[23].setRotationPoint(-76.5F, -39F, -16.5F);

		bodyModel[24].addBox(0F, 0F, 0F, 1, 16, 9, 0F); // backFlap1
		bodyModel[24].setRotationPoint(-73F, -13F, -25.9F);

		bodyModel[25].addBox(0F, 0F, 0F, 1, 16, 9, 0F); // backFlap2
		bodyModel[25].setRotationPoint(-73F, -13F, 17.9F);

		bodyModel[26].addShapeBox(0F, 0F, 0F, 1, 1, 10, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // backWindow3
		bodyModel[26].setRotationPoint(-77.5F, -40F, -12.5F);

		bodyModel[27].addShapeBox(0F, 0F, 0F, 1, 1, 10, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // backWindow4
		bodyModel[27].setRotationPoint(-77.5F, -28F, -12.5F);

		bodyModel[28].addShapeBox(0F, 0F, 0F, 1, 11, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // backWindow7
		bodyModel[28].setRotationPoint(-77.5F, -39F, -12.5F);

		bodyModel[29].addShapeBox(0F, 0F, 0F, 1, 11, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // backWindow8
		bodyModel[29].setRotationPoint(-77.5F, -39F, -3.5F);

		bodyModel[30].addBox(0F, 0F, 0F, 6, 7, 45, 0F); // body1
		bodyModel[30].setRotationPoint(60F, -7F, -22F);

		bodyModel[31].addBox(0F, 0F, 0F, 36, 1, 36, 0F); // body10
		bodyModel[31].setRotationPoint(-74F, -11F, -17.5F);

		bodyModel[32].addShapeBox(0F, 0F, 0F, 12, 4, 3, 0F,0F, 0F, 0F, 2F, -18F, 0F, 2F, -18F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -6F, 14F, 0F, -6F, 14F, 0F, 0F, 0F, 0F); // body11
		bodyModel[32].setRotationPoint(11F, -45F, 20F);

		bodyModel[33].addBox(0F, 0F, 0F, 4, 30, 3, 0F); // body12
		bodyModel[33].setRotationPoint(-11F, -41F, 20F);

		bodyModel[34].addShapeBox(0F, 0F, 0F, 3, 4, 39, 0F,0F, 0F, 0F, 0F, -3.5F, 0F, 0F, -3.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body13
		bodyModel[34].setRotationPoint(11F, -45F, -19F);

		bodyModel[35].addShapeBox(0F, 0F, 0F, 12, 4, 3, 0F,0F, 0F, 0F, 2F, -18F, 0F, 2F, -18F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -6F, 14F, 0F, -6F, 14F, 0F, 0F, 0F, 0F); // body15
		bodyModel[35].setRotationPoint(11F, -45F, -22F);

		bodyModel[36].addShapeBox(0F, 0F, 0F, 8, 2, 19, 0F,0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body16
		bodyModel[36].setRotationPoint(17F, -29F, 1F);

		bodyModel[37].addShapeBox(0F, 0F, 0F, 8, 2, 19, 0F,0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body17
		bodyModel[37].setRotationPoint(17F, -29F, -19F);

		bodyModel[38].addShapeBox(0F, 0F, 0F, 12, 4, 1, 0F,0F, 0F, 0F, 2F, -18F, 0F, 2F, -18F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -6F, 14F, 0F, -6F, 14F, 0F, 0F, 0F, 0F); // body18
		bodyModel[38].setRotationPoint(11F, -45F, 0F);

		bodyModel[39].addBox(0F, 0F, 0F, 4, 30, 3, 0F); // body19
		bodyModel[39].setRotationPoint(-11F, -41F, -22F);

		bodyModel[40].addShapeBox(0F, 0F, 0F, 8, 12, 12, 0F,0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, -5F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, -5F, 0F, 0F); // body2
		bodyModel[40].setRotationPoint(55F, -19F, -22F);

		bodyModel[41].addBox(0F, 0F, 0F, 1, 10, 19, 0F); // body21
		bodyModel[41].setRotationPoint(66F, -18F, -9F);

		bodyModel[42].addBox(0F, 0F, 0F, 35, 8, 2, 0F); // body22
		bodyModel[42].setRotationPoint(-74F, -31F, 20.5F);

		bodyModel[43].addBox(0F, 0F, 0F, 63, 2, 2, 0F); // body24
		bodyModel[43].setRotationPoint(-74F, -41F, 20.5F);

		bodyModel[44].addBox(0F, 0F, 0F, 22, 8, 2, 0F); // body25
		bodyModel[44].setRotationPoint(-74F, -39F, 20.5F);

		bodyModel[45].addBox(0F, 0F, 0F, 6, 8, 2, 0F); // body26
		bodyModel[45].setRotationPoint(-36F, -39F, 20.5F);

		bodyModel[46].addShapeBox(0F, 0F, 0F, 8, 10, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -8F, 0F, 0F, -8F, 0F, 0F, 0F, 0F, 0F); // body27
		bodyModel[46].setRotationPoint(-74F, -23F, 20.5F);

		bodyModel[47].addBox(0F, 0F, 0F, 35, 8, 2, 0F); // body29
		bodyModel[47].setRotationPoint(-74F, -31F, -21.5F);

		bodyModel[48].addShapeBox(0F, 0F, 0F, 7, 12, 45, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -6F, 0F, 0F, -6F, 0F, 0F, 0F, 0F, 0F); // body3
		bodyModel[48].setRotationPoint(25F, -19F, -22F);

		bodyModel[49].addShapeBox(0F, 0F, 0F, 8, 10, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -8F, 0F, 0F, -8F, 0F, 0F, 0F, 0F, 0F); // body30
		bodyModel[49].setRotationPoint(-74F, -23F, -21.5F);

		bodyModel[50].addBox(0F, 0F, 0F, 63, 2, 2, 0F); // body31
		bodyModel[50].setRotationPoint(-74F, -41F, -21.5F);

		bodyModel[51].addShapeBox(0F, 0F, 0F, 8, 10, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -8F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -8F, 0F, 0F); // body32
		bodyModel[51].setRotationPoint(-47F, -23F, 20.5F);

		bodyModel[52].addShapeBox(0F, 0F, 0F, 8, 10, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -8F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -8F, 0F, 0F); // body33
		bodyModel[52].setRotationPoint(-47F, -23F, -21.5F);

		bodyModel[53].addBox(0F, 0F, 0F, 6, 8, 2, 0F); // body34
		bodyModel[53].setRotationPoint(-36F, -39F, -21.5F);

		bodyModel[54].addBox(0F, 0F, 0F, 22, 8, 2, 0F); // body35
		bodyModel[54].setRotationPoint(-74F, -39F, -21.5F);

		bodyModel[55].addBox(0F, 0F, 0F, 20, 8, 45, 0F); // body4
		bodyModel[55].setRotationPoint(25F, -27F, -22F);

		bodyModel[56].addShapeBox(0F, 0F, 0F, 20, 3, 45, 0F,0F, 5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body5
		bodyModel[56].setRotationPoint(45F, -22F, -22F);

		bodyModel[57].addBox(0F, 0F, 0F, 6, 12, 21, 0F); // body6
		bodyModel[57].setRotationPoint(60F, -19F, -10F);

		bodyModel[58].addShapeBox(0F, 0F, 0F, 3, 2, 45, 0F,0F, 1F, 0F, 0F, 0F, -12F, 0F, 0F, -12F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, -12F, 0F, 0F, -12F, 0F, 0F, 0F); // body7
		bodyModel[58].setRotationPoint(65F, -21F, -22F);

		bodyModel[59].addShapeBox(0F, 0F, 0F, 8, 12, 12, 0F,0F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -5F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, -5F, 0F, 0F); // body8
		bodyModel[59].setRotationPoint(55F, -19F, 11F);

		bodyModel[60].addBox(0F, 0F, 0F, 58, 6, 45, 0F); // body9
		bodyModel[60].setRotationPoint(-33F, -11F, -22F);

		bodyModel[61].addShapeBox(0F, 0F, 0F, 14, 2, 26, 0F,0F, 0F, -2F, 0F, -2F, -2F, 0F, -2F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, 0F, 0F); // bodyFrontVent1
		bodyModel[61].setRotationPoint(45F, -28F, -12F);

		bodyModel[62].addShapeBox(0F, 0F, 0F, 10, 2, 26, 0F,0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // bodyFrontVent2
		bodyModel[62].setRotationPoint(35F, -28F, -12F);

		bodyModel[63].addShapeBox(0F, 0F, 0F, 25, 2, 25, 0F,-2F, 0F, -2F, -2F, 0F, -2F, -2F, 0F, -2F, -2F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // bodyTop11
		bodyModel[63].setRotationPoint(-21F, -49F, -12F);

		bodyModel[64].addBox(0F, 0F, 0F, 95, 5, 5, 0F); // bodyTop11
		bodyModel[64].setRotationPoint(-53.5F, -5.5F, 11F);

		bodyModel[65].addBox(0F, 0F, 0F, 88, 4, 3, 0F); // bodyTop3
		bodyModel[65].setRotationPoint(-77F, -45F, -22F);

		bodyModel[66].addShapeBox(0F, 0F, 0F, 89, 2, 45, 0F,-2F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, -2F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // bodyTop4
		bodyModel[66].setRotationPoint(-77F, -47F, -22F);

		bodyModel[67].addShapeBox(0F, 0F, 0F, 5, 5, 38, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // bodyTop9
		bodyModel[67].setRotationPoint(-78.5F, -48F, -18.5F);

		bodyModel[68].addBox(0F, 0F, 0F, 8, 16, 45, 0F); // BOX_placeholder
		bodyModel[68].setRotationPoint(17F, -27F, -22F);

		bodyModel[69].addShapeBox(0F, 0F, 0F, 2, 5, 15, 0F,0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F); // bumper1
		bodyModel[69].setRotationPoint(66F, -6F, -22F);

		bodyModel[70].addShapeBox(0F, 0F, 0F, 2, 5, 15, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, 0F); // bumper2
		bodyModel[70].setRotationPoint(66F, -6F, 8F);

		bodyModel[71].addBox(0F, 0F, 0F, 24, 16, 2, 0F); // doorFrontLeft1
		bodyModel[71].setRotationPoint(-7F, -27F, 20.5F);

		bodyModel[72].addBox(0F, 0F, 0F, 3, 12, 2, 0F); // doorFrontLeft2
		bodyModel[72].setRotationPoint(-7F, -39F, 20.5F);

		bodyModel[73].addBox(0F, 0F, 0F, 18, 2, 2, 0F); // doorFrontLeft3
		bodyModel[73].setRotationPoint(-7F, -41F, 20.5F);

		bodyModel[74].addBox(0F, 0F, 0F, 5, 1, 1, 0F); // doorFrontLeft5
		bodyModel[74].setRotationPoint(-5F, -24F, 22.5F);

		bodyModel[75].addBox(0F, 0F, 0F, 24, 16, 2, 0F); // doorFrontRight1
		bodyModel[75].setRotationPoint(-7F, -27F, -21.5F);

		bodyModel[76].addShapeBox(0F, 0F, 0F, 4, 2, 2, 0F,0F, 0F, 0F, 2F, -14F, 0F, 2F, -14F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 12F, 0F, -2F, 12F, 0F, 0F, 0F, 0F); // doorFrontRight2
		bodyModel[76].setRotationPoint(11F, -41F, -21.5F);

		bodyModel[77].addBox(0F, 0F, 0F, 18, 2, 2, 0F); // doorFrontRight3
		bodyModel[77].setRotationPoint(-7F, -41F, -21.5F);

		bodyModel[78].addBox(0F, 0F, 0F, 3, 12, 2, 0F); // doorFrontRight4
		bodyModel[78].setRotationPoint(-7F, -39F, -21.5F);

		bodyModel[79].addShapeBox(0F, 0F, 0F, 3, 7, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // doorHinge1
		bodyModel[79].setRotationPoint(15.5F, -23F, 22.5F);

		bodyModel[80].addShapeBox(0F, 0F, 0F, 3, 7, 1, 0F,-1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // doorHinge3
		bodyModel[80].setRotationPoint(15.5F, -23F, -22.5F);

		bodyModel[81].addShapeBox(0F, 0F, 0F, 1, 7, 3, 0F,0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // doorHinge5
		bodyModel[81].setRotationPoint(-77.5F, -25F, -17.5F);

		bodyModel[82].addShapeBox(0F, 0F, 0F, 1, 7, 3, 0F,0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // doorHinge6
		bodyModel[82].setRotationPoint(-77.5F, -25F, 15.5F);

		bodyModel[83].addShapeBox(0F, 0F, 0F, 5, 8, 29, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F); // interior1
		bodyModel[83].setRotationPoint(12F, -22F, -19F);

		bodyModel[84].addBox(0F, 0F, 0F, 5, 5, 19, 0F); // interior4
		bodyModel[84].setRotationPoint(-28.5F, -5.5F, -8F);

		bodyModel[85].addBox(0F, 0F, 0F, 28, 3, 12, 0F); // interior6
		bodyModel[85].setRotationPoint(-70F, -14F, 6F);

		bodyModel[86].addBox(0F, 0F, 0F, 12, 9, 4, 0F); // interiorComms1
		bodyModel[86].setRotationPoint(-72F, -32F, -19.5F);

		bodyModel[87].addShapeBox(0F, 0F, 0F, 12, 6, 3, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F); // interiorComms2
		bodyModel[87].setRotationPoint(-72F, -39F, -19.5F);

		bodyModel[88].addBox(0F, 0F, 0F, 13, 5, 5, 0F); // interiorComms3
		bodyModel[88].setRotationPoint(-72.5F, -31F, -20F);

		bodyModel[89].addShapeBox(0F, 0F, 0F, 5, 7, 39, 0F,-1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // interiorDashboard
		bodyModel[89].setRotationPoint(12F, -29F, -19F);

		bodyModel[90].addBox(0F, 0F, 0F, 13, 3, 12, 0F); // interiorFrontRightSeat1
		bodyModel[90].setRotationPoint(-4F, -14F, -17F);

		bodyModel[91].addBox(0F, 0F, 0F, 15, 3, 14, 0F); // interiorFrontRightSeat2
		bodyModel[91].setRotationPoint(-4F, -17F, -18F);

		bodyModel[92].addShapeBox(0F, 0F, 0F, 3, 13, 14, 0F,1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F, 1F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // interiorFrontRightSeat3
		bodyModel[92].setRotationPoint(-4F, -30F, -18F);

		bodyModel[93].addShapeBox(0F, 0F, 0F, 3, 2, 12, 0F,0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // interiorFrontRightSeat4
		bodyModel[93].setRotationPoint(-5F, -32F, -17F);

		bodyModel[94].addBox(0F, 0F, 0F, 1, 1, 6, 0F); // interiorFrontRightSeat5
		bodyModel[94].setRotationPoint(-4F, -33F, -14F);

		bodyModel[95].addBox(0F, 0F, 0F, 3, 4, 8, 0F); // interiorFrontRightSeat6
		bodyModel[95].setRotationPoint(-5F, -37F, -15F);

		bodyModel[96].addBox(0F, 0F, 0F, 18, 10, 8, 0F); // interiorGearBox
		bodyModel[96].setRotationPoint(-6F, -21F, -3.5F);

		bodyModel[97].addShapeBox(0F, 0F, 0F, 3, 3, 9, 0F,0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // leftHeadlight1
		bodyModel[97].setRotationPoint(62.5F, -18F, 13.5F);

		bodyModel[98].addBox(0F, 0F, 0F, 3, 3, 9, 0F); // leftHeadlight2
		bodyModel[98].setRotationPoint(62.5F, -15F, 13.5F);

		bodyModel[99].addShapeBox(0F, 0F, 0F, 3, 3, 9, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // leftHeadlight3
		bodyModel[99].setRotationPoint(62.5F, -12F, 13.5F);

		bodyModel[100].addBox(0F, 0F, 0F, 1, 6, 15, 0F); // plateBack
		bodyModel[100].setRotationPoint(-79F, -10F, -7F);

		bodyModel[101].addBox(0F, 0F, 0F, 1, 6, 13, 0F); // plateFront
		bodyModel[101].setRotationPoint(66F, -4F, -6F);

		bodyModel[102].addShapeBox(0F, 0F, 0F, 1, 10, 3, 0F,0.5F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, -1F, 0.5F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, -1F); // radioPoint1
		bodyModel[102].setRotationPoint(-70.5F, -55F, 21F);

		bodyModel[103].addShapeBox(0F, 0F, 0F, 1, 10, 3, 0F,0F, 0F, 0F, 0.5F, 0F, -1F, 0.5F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, -1F, 0.5F, 0F, -1F, 0F, 0F, 0F); // radioPoint2
		bodyModel[103].setRotationPoint(-69.5F, -55F, 21F);

		bodyModel[104].addBox(0F, 0F, 0F, 4, 4, 2, 0F); // radioPoint3
		bodyModel[104].setRotationPoint(-71.5F, -45F, 22.5F);

		bodyModel[105].addShapeBox(0F, 0F, 0F, 3, 3, 9, 0F,0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // rightHeadlight1
		bodyModel[105].setRotationPoint(62.5F, -18F, -20.5F);

		bodyModel[106].addBox(0F, 0F, 0F, 3, 3, 9, 0F); // rightHeadlight2
		bodyModel[106].setRotationPoint(62.5F, -15F, -20.5F);

		bodyModel[107].addShapeBox(0F, 0F, 0F, 3, 3, 9, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // rightHeadlight3
		bodyModel[107].setRotationPoint(62.5F, -12F, -20.5F);

		bodyModel[108].addShapeBox(0F, 0F, 0F, 7, 3, 53, 0F,0F, 0F, 0F, 0F, -10F, 0F, 0F, -10F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 10F, 0F, 0F, 10F, 0F, 0F, 0F, 0F); // rimPart1
		bodyModel[108].setRotationPoint(52F, -23F, -26F);

		bodyModel[109].addShapeBox(0F, 0F, 0F, 8, 3, 9, 0F,0F, -10F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -10F, 0F, 0F, 10F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 10F, 0F); // rimPart10
		bodyModel[109].setRotationPoint(-74F, -23F, 18F);

		bodyModel[110].addBox(0F, 0F, 0F, 18, 3, 53, 0F); // rimPart2
		bodyModel[110].setRotationPoint(34F, -23F, -26F);

		bodyModel[111].addShapeBox(0F, 0F, 0F, 10, 3, 53, 0F,0F, -18F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -18F, 0F, 0F, 18F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 18F, 0F); // rimPart3
		bodyModel[111].setRotationPoint(24F, -23F, -26F);

		bodyModel[112].addBox(0F, 0F, 0F, 57, 3, 53, 0F); // rimPart4_Main
		bodyModel[112].setRotationPoint(-33F, -5F, -26F);

		bodyModel[113].addShapeBox(0F, 0F, 0F, 13, 3, 9, 0F,0F, 0F, 0F, 0F, -18F, 0F, 0F, -18F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 18F, 0F, 0F, 18F, 0F, 0F, 0F, 0F); // rimPart5
		bodyModel[113].setRotationPoint(-46F, -23F, -26F);

		bodyModel[114].addBox(0F, 0F, 0F, 20, 3, 9, 0F); // rimPart6
		bodyModel[114].setRotationPoint(-66F, -23F, -26F);

		bodyModel[115].addShapeBox(0F, 0F, 0F, 8, 3, 9, 0F,0F, -10F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -10F, 0F, 0F, 10F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 10F, 0F); // rimPart7
		bodyModel[115].setRotationPoint(-74F, -23F, -26F);

		bodyModel[116].addShapeBox(0F, 0F, 0F, 13, 3, 9, 0F,0F, 0F, 0F, 0F, -18F, 0F, 0F, -18F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 18F, 0F, 0F, 18F, 0F, 0F, 0F, 0F); // rimPart8
		bodyModel[116].setRotationPoint(-46F, -23F, 18F);

		bodyModel[117].addBox(0F, 0F, 0F, 20, 3, 9, 0F); // rimPart9
		bodyModel[117].setRotationPoint(-66F, -23F, 18F);

		bodyModel[118].addShapeBox(0F, 0F, 0F, 9, 3, 16, 0F,-3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // shockFront1
		bodyModel[118].setRotationPoint(39.5F, -7.5F, -7F);

		bodyModel[119].addBox(0F, 0F, 0F, 9, 3, 16, 0F); // shockFront2
		bodyModel[119].setRotationPoint(39.5F, -4.5F, -7F);

		bodyModel[120].addShapeBox(0F, 0F, 0F, 9, 3, 16, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F); // shockFront3
		bodyModel[120].setRotationPoint(39.5F, -1.5F, -7F);

		bodyModel[121].addShapeBox(0F, -4.5F, -4.5F, 1, 3, 9, 0F,0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // steeringWheel1
		bodyModel[121].setRotationPoint(11.5F, -26.5F, 12F);
		bodyModel[121].rotateAngleZ = -0.17453293F;

		bodyModel[122].addBox(0F, -1.5F, -4.5F, 1, 3, 9, 0F); // steeringWheel2
		bodyModel[122].setRotationPoint(11.5F, -26.5F, 12F);
		bodyModel[122].rotateAngleZ = -0.17453293F;

		bodyModel[123].addShapeBox(0F, 1.5F, -4.5F, 1, 3, 9, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // steeringWheel3
		bodyModel[123].setRotationPoint(11.5F, -26.5F, 12F);
		bodyModel[123].rotateAngleZ = -0.17453293F;

		bodyModel[124].addBox(0F, 0F, 0F, 35, 11, 19, 0F); // underBody1
		bodyModel[124].setRotationPoint(-23.5F, -7.5F, -8F);

		bodyModel[125].addBox(0F, 0F, 0F, 25, 14, 19, 0F); // underBody2
		bodyModel[125].setRotationPoint(11.5F, -7.5F, -8F);

		bodyModel[126].addBox(0F, 0F, 0F, 1, 4, 2, 0F); // wingLeftMirror1
		bodyModel[126].setRotationPoint(13F, -34F, 22.5F);

		bodyModel[127].addBox(0F, 0F, 0F, 1, 9, 5, 0F); // wingLeftMirror2
		bodyModel[127].setRotationPoint(13F, -38F, 24.3F);
		bodyModel[127].rotateAngleY = 0.17453293F;

		bodyModel[128].addBox(0F, 0F, 0F, 1, 4, 2, 0F); // wingRightMirror1
		bodyModel[128].setRotationPoint(12.7F, -34F, -23.5F);

		bodyModel[129].addBox(0F, 0F, 0F, 1, 9, 5, 0F); // wingRightMirror2
		bodyModel[129].setRotationPoint(12F, -38F, -28.3F);
		bodyModel[129].rotateAngleY = -0.17453293F;

		bodyModel[130].addBox(0F, 0F, 0F, 5, 5, 31, 0F); // wireFrame1
		bodyModel[130].setRotationPoint(41.5F, -5.5F, -15F);

		bodyModel[131].addBox(0F, 0F, 0F, 5, 5, 31, 0F); // wireFrame2
		bodyModel[131].setRotationPoint(-58.5F, -5.5F, -15F);

		bodyModel[132].addBox(0F, 0F, 0F, 95, 5, 5, 0F); // wireFrame3
		bodyModel[132].setRotationPoint(-53.5F, -5.5F, -13F);

		bodyModel[133].addBox(0F, 0F, 0F, 28, 20, 2, 0F); // Box 0
		bodyModel[133].setRotationPoint(-39F, -31F, -21.5F);

		bodyModel[134].addBox(0F, 0F, 0F, 28, 20, 2, 0F); // Box 2
		bodyModel[134].setRotationPoint(-39F, -31F, 20.5F);

		bodyModel[135].addBox(0F, 0F, 0F, 4, 8, 2, 0F); // Box 3
		bodyModel[135].setRotationPoint(-15F, -39F, 20.5F);

		bodyModel[136].addBox(0F, 0F, 0F, 4, 8, 2, 0F); // Box 4
		bodyModel[136].setRotationPoint(-15F, -39F, -21.5F);

		bodyModel[137].addBox(0F, 0F, 0F, 2, 3, 1, 0F); // Box 5
		bodyModel[137].setRotationPoint(-66F, -29F, 27.5F);

		bodyModel[138].addBox(0F, 0F, 0F, 16, 8, 5, 0F); // Box 6
		bodyModel[138].setRotationPoint(-73F, -30F, 22.5F);

		bodyModel[139].addBox(0F, 0F, 0F, 2, 3, 1, 0F); // Box 7
		bodyModel[139].setRotationPoint(-49F, -29F, 27.5F);

		bodyModel[140].addBox(0F, 0F, 0F, 16, 8, 5, 0F); // Box 8
		bodyModel[140].setRotationPoint(-56F, -30F, 22.5F);

		bodyModel[141].addShapeBox(0F, 0F, 0F, 3, 13, 14, 0F,1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F, 1F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 0
		bodyModel[141].setRotationPoint(-4F, -30F, 5F);

		bodyModel[142].addShapeBox(0F, 0F, 0F, 3, 2, 12, 0F,0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 1
		bodyModel[142].setRotationPoint(-5F, -32F, 6F);

		bodyModel[143].addBox(0F, 0F, 0F, 1, 1, 6, 0F); // Box 2
		bodyModel[143].setRotationPoint(-4F, -33F, 9F);

		bodyModel[144].addBox(0F, 0F, 0F, 3, 4, 8, 0F); // Box 3
		bodyModel[144].setRotationPoint(-5F, -37F, 8F);

		bodyModel[145].addBox(0F, 0F, 0F, 15, 3, 14, 0F); // Box 4
		bodyModel[145].setRotationPoint(-4F, -17F, 5F);

		bodyModel[146].addBox(0F, 0F, 0F, 13, 3, 12, 0F); // Box 5
		bodyModel[146].setRotationPoint(-4F, -14F, 6F);

		bodyModel[147].addShapeBox(0F, 0F, 0F, 4, 2, 2, 0F,0F, 0F, 0F, 2F, -14F, 0F, 2F, -14F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 12F, 0F, -2F, 12F, 0F, 0F, 0F, 0F); // Box 6
		bodyModel[147].setRotationPoint(11F, -41F, 20.5F);

		bodyModel[148].addShapeBox(0F, 0F, 0F, 5, 9, 45, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -7F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -7F, 0F); // Box 7
		bodyModel[148].setRotationPoint(-38F, -11F, -22F);

		bodyModel[149].addBox(0F, 0F, 0F, 2, 17, 16, 0F); // Box 9
		bodyModel[149].setRotationPoint(-76.5F, -28F, 1F);

		bodyModel[150].addShapeBox(0F, 0F, 0F, 2, 11, 3, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // Box 10
		bodyModel[150].setRotationPoint(-76.5F, -39F, 1F);

		bodyModel[151].addShapeBox(0F, 0F, 0F, 2, 2, 16, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // Box 11
		bodyModel[151].setRotationPoint(-76.5F, -41F, 1F);

		bodyModel[152].addShapeBox(0F, 0F, 0F, 2, 11, 5, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // Box 12
		bodyModel[152].setRotationPoint(-76.5F, -39F, 12.5F);

		bodyModel[153].addShapeBox(0F, 0F, 0F, 1, 1, 10, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // Box 13
		bodyModel[153].setRotationPoint(-77.5F, -40F, 3.5F);

		bodyModel[154].addShapeBox(0F, 0F, 0F, 1, 11, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // Box 14
		bodyModel[154].setRotationPoint(-77.5F, -39F, 3.5F);

		bodyModel[155].addShapeBox(0F, 0F, 0F, 1, 1, 10, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // Box 15
		bodyModel[155].setRotationPoint(-77.5F, -28F, 3.5F);

		bodyModel[156].addShapeBox(0F, 0F, 0F, 1, 11, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // Box 16
		bodyModel[156].setRotationPoint(-77.5F, -39F, 12.5F);

		bodyModel[157].addShapeBox(0F, 0F, 0F, 20, 12, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 10F, 0F, 0F, 9F, 0F, 0F, 9F, 0F, 0F, 10F, 0F, 0F); // Box 17
		bodyModel[157].setRotationPoint(-66F, -23F, -17.5F);

		bodyModel[158].addShapeBox(0F, 0F, 0F, 20, 12, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 10F, 0F, 0F, 9F, 0F, 0F, 9F, 0F, 0F, 10F, 0F, 0F); // Box 18
		bodyModel[158].setRotationPoint(-66F, -23F, 17.5F);

		bodyModel[159].addBox(0F, 0F, 0F, 30, 3, 14, 0F); // Box 19
		bodyModel[159].setRotationPoint(-71F, -17F, 4F);

		bodyModel[160].addShapeBox(0F, 0F, 0F, 3, 30, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // Box 20
		bodyModel[160].setRotationPoint(-77F, -41F, -0.25F);

		bodyModel[161].addBox(0F, 0F, 0F, 5, 1, 1, 0F); // Box 21
		bodyModel[161].setRotationPoint(-5F, -24F, -22.5F);

		bodyModel[162].addShapeBox(0F, 0F, 0F, 30, 13, 2, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 22
		bodyModel[162].setRotationPoint(-71F, -30F, 16F);

		bodyModel[163].addBox(0F, 0F, 0F, 12, 12, 1, 0F); // Box 0
		bodyModel[163].setRotationPoint(-72F, -23F, -16.5F);

		bodyModel[164].addShapeBox(0F, 0F, 0F, 27, 12, 35, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 7F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 7F, 0F, 0F); // Box 1
		bodyModel[164].setRotationPoint(33F, -19F, -17F);

		bodyModel[165].addBox(0F, 0F, 0F, 88, 4, 3, 0F); // Box 2
		bodyModel[165].setRotationPoint(-77F, -45F, 20F);

		bodyModel[166].addBox(0F, 0F, 0F, 4, 4, 39, 0F); // Box 3
		bodyModel[166].setRotationPoint(-77F, -45F, -19F);
	}

	private void initleftFrontWheelModel_1()
	{
		leftFrontWheelModel[0] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelLeftFront0
		leftFrontWheelModel[1] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelLeftFront11
		leftFrontWheelModel[2] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelLeftFront2
		leftFrontWheelModel[3] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelLeftFront3
		leftFrontWheelModel[4] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelLeftFront2
		leftFrontWheelModel[5] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelLeftFront5
		leftFrontWheelModel[6] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelLeftFront6
		leftFrontWheelModel[7] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelLeftFront7
		leftFrontWheelModel[8] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelLeftFront8
		leftFrontWheelModel[9] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelLeftFront9
		leftFrontWheelModel[10] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelLeftFront10
		leftFrontWheelModel[11] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelLeftFront4
		leftFrontWheelModel[12] = new ModelRendererTurbo(this, 109, 23, textureX, textureY); // wheelLeftFrontCap

		leftFrontWheelModel[0].addBox(-4F, -15F, -5F, 8, 8, 10, 0F); // wheelLeftFront0
		leftFrontWheelModel[0].setRotationPoint(44F, -3F, 24F);

		leftFrontWheelModel[1].addShapeBox(4F, -15F, -5F, 8, 8, 10, 0F,0F, 0F, 0F, -1F, -4F, 0F, -1F, -4F, 0F, 0F, 0F, 0F, 4F, -1F, 0F, -5F, 3F, 0F, -5F, 3F, 0F, 4F, -1F, 0F); // wheelLeftFront11
		leftFrontWheelModel[1].setRotationPoint(44F, -3F, 24F);

		leftFrontWheelModel[2].addShapeBox(-12F, -15F, -5F, 8, 8, 10, 0F,-1F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, -4F, 0F, -5F, 3F, 0F, 4F, -1F, 0F, 4F, -1F, 0F, -5F, 3F, 0F); // wheelLeftFront2
		leftFrontWheelModel[2].setRotationPoint(44F, -3F, 24F);

		leftFrontWheelModel[3].addBox(-15F, -4F, -5F, 8, 8, 10, 0F); // wheelLeftFront3
		leftFrontWheelModel[3].setRotationPoint(44F, -3F, 24F);

		leftFrontWheelModel[4].addShapeBox(-12F, -6.9F, -5F, 8, 8, 10, 0F,3F, -3F, 0F, -7F, 4F, 0F, -7F, 4F, 0F, 3F, -3F, 0F, -4F, -1F, 0F, 0F, -8F, 0F, 0F, -8F, 0F, -4F, -1F, 0F); // wheelLeftFront2
		leftFrontWheelModel[4].setRotationPoint(44F, -3F, 24F);

		leftFrontWheelModel[5].addShapeBox(-7F, 4F, -5F, 8, 8, 10, 0F,0F, 0F, 0F, -1F, -4F, 0F, -1F, -4F, 0F, 0F, 0F, 0F, 4F, -1F, 0F, -5F, 3F, 0F, -5F, 3F, 0F, 4F, -1F, 0F); // wheelLeftFront5
		leftFrontWheelModel[5].setRotationPoint(44F, -3F, 24F);

		leftFrontWheelModel[6].addBox(-4F, 7F, -5F, 8, 8, 10, 0F); // wheelLeftFront6
		leftFrontWheelModel[6].setRotationPoint(44F, -3F, 24F);

		leftFrontWheelModel[7].addShapeBox(-1F, 4F, -5F, 8, 8, 10, 0F,-1F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, -4F, 0F, -5F, 3F, 0F, 4F, -1F, 0F, 4F, -1F, 0F, -5F, 3F, 0F); // wheelLeftFront7
		leftFrontWheelModel[7].setRotationPoint(44F, -3F, 24F);

		leftFrontWheelModel[8].addShapeBox(7F, 4.1F, -5F, 8, 8, 10, 0F,3F, -3F, 0F, -7F, 4F, 0F, -7F, 4F, 0F, 3F, -3F, 0F, -4F, -1F, 0F, 0F, -8F, 0F, 0F, -8F, 0F, -4F, -1F, 0F); // wheelLeftFront8
		leftFrontWheelModel[8].setRotationPoint(44F, -3F, 24F);

		leftFrontWheelModel[9].addBox(7F, -4F, -5F, 8, 8, 10, 0F); // wheelLeftFront9
		leftFrontWheelModel[9].setRotationPoint(44F, -3F, 24F);

		leftFrontWheelModel[10].addShapeBox(4F, -7.1F, -5F, 8, 8, 10, 0F,-7F, 4F, 0F, 3F, -3F, 0F, 3F, -3F, 0F, -7F, 4F, 0F, 0F, -8F, 0F, -4F, -1F, 0F, -4F, -1F, 0F, 0F, -8F, 0F); // wheelLeftFront10
		leftFrontWheelModel[10].setRotationPoint(44F, -3F, 24F);

		leftFrontWheelModel[11].addShapeBox(-15F, 4.1F, -5F, 8, 8, 10, 0F,-7F, 4F, 0F, 3F, -3F, 0F, 3F, -3F, 0F, -7F, 4F, 0F, 0F, -8F, 0F, -4F, -1F, 0F, -4F, -1F, 0F, 0F, -8F, 0F); // wheelLeftFront4
		leftFrontWheelModel[11].setRotationPoint(44F, -3F, 24F);

		leftFrontWheelModel[12].addBox(-6F, -7F, -3F, 14, 14, 7, 0F); // wheelLeftFrontCap
		leftFrontWheelModel[12].setRotationPoint(43F, -3F, 23F);
	}

	private void initrightFrontWheelModel_1()
	{
		rightFrontWheelModel[0] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelRightFront0
		rightFrontWheelModel[1] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelRightFront1
		rightFrontWheelModel[2] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelRightFront2
		rightFrontWheelModel[3] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelRightFront3
		rightFrontWheelModel[4] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelRightFront4
		rightFrontWheelModel[5] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelRightFront5
		rightFrontWheelModel[6] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelRightFront6
		rightFrontWheelModel[7] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelRightFront9
		rightFrontWheelModel[8] = new ModelRendererTurbo(this, 109, 23, textureX, textureY); // wheelRightFrontCap
		rightFrontWheelModel[9] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelRightFront7
		rightFrontWheelModel[10] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelRightFront8
		rightFrontWheelModel[11] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelRightFront10
		rightFrontWheelModel[12] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelRightFront11

		rightFrontWheelModel[0].addBox(-4F, -15F, -5F, 8, 8, 10, 0F); // wheelRightFront0
		rightFrontWheelModel[0].setRotationPoint(44F, -3F, -23F);

		rightFrontWheelModel[1].addShapeBox(-12F, -15F, -5F, 8, 8, 10, 0F,-1F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, -4F, 0F, -5F, 3F, 0F, 4F, -1F, 0F, 4F, -1F, 0F, -5F, 3F, 0F); // wheelRightFront1
		rightFrontWheelModel[1].setRotationPoint(44F, -3F, -23F);

		rightFrontWheelModel[2].addShapeBox(-12F, -6.9F, -5F, 8, 8, 10, 0F,3F, -3F, 0F, -7F, 4F, 0F, -7F, 4F, 0F, 3F, -3F, 0F, -4F, -1F, 0F, 0F, -8F, 0F, 0F, -8F, 0F, -4F, -1F, 0F); // wheelRightFront2
		rightFrontWheelModel[2].setRotationPoint(44F, -3F, -23F);

		rightFrontWheelModel[3].addBox(-15F, -4F, -5F, 8, 8, 10, 0F); // wheelRightFront3
		rightFrontWheelModel[3].setRotationPoint(44F, -3F, -23F);

		rightFrontWheelModel[4].addShapeBox(-15F, 4.1F, -5F, 8, 8, 10, 0F,-7F, 4F, 0F, 3F, -3F, 0F, 3F, -3F, 0F, -7F, 4F, 0F, 0F, -8F, 0F, -4F, -1F, 0F, -4F, -1F, 0F, 0F, -8F, 0F); // wheelRightFront4
		rightFrontWheelModel[4].setRotationPoint(44F, -3F, -23F);

		rightFrontWheelModel[5].addShapeBox(-7F, 4F, -5F, 8, 8, 10, 0F,0F, 0F, 0F, -1F, -4F, 0F, -1F, -4F, 0F, 0F, 0F, 0F, 4F, -1F, 0F, -5F, 3F, 0F, -5F, 3F, 0F, 4F, -1F, 0F); // wheelRightFront5
		rightFrontWheelModel[5].setRotationPoint(44F, -3F, -23F);

		rightFrontWheelModel[6].addBox(-4F, 7F, -5F, 8, 8, 10, 0F); // wheelRightFront6
		rightFrontWheelModel[6].setRotationPoint(44F, -3F, -23F);

		rightFrontWheelModel[7].addBox(7F, -4F, -5F, 8, 8, 10, 0F); // wheelRightFront9
		rightFrontWheelModel[7].setRotationPoint(44F, -3F, -23F);

		rightFrontWheelModel[8].addBox(-6F, -7F, -3F, 14, 14, 7, 0F); // wheelRightFrontCap
		rightFrontWheelModel[8].setRotationPoint(43F, -3F, -23F);

		rightFrontWheelModel[9].addShapeBox(-1F, 4F, -5F, 8, 8, 10, 0F,-1F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, -4F, 0F, -5F, 3F, 0F, 4F, -1F, 0F, 4F, -1F, 0F, -5F, 3F, 0F); // wheelRightFront7
		rightFrontWheelModel[9].setRotationPoint(44F, -3F, -23F);

		rightFrontWheelModel[10].addShapeBox(7F, 4.1F, -5F, 8, 8, 10, 0F,3F, -3F, 0F, -7F, 4F, 0F, -7F, 4F, 0F, 3F, -3F, 0F, -4F, -1F, 0F, 0F, -8F, 0F, 0F, -8F, 0F, -4F, -1F, 0F); // wheelRightFront8
		rightFrontWheelModel[10].setRotationPoint(44F, -3F, -23F);

		rightFrontWheelModel[11].addShapeBox(4F, -7.1F, -5F, 8, 8, 10, 0F,-7F, 4F, 0F, 3F, -3F, 0F, 3F, -3F, 0F, -7F, 4F, 0F, 0F, -8F, 0F, -4F, -1F, 0F, -4F, -1F, 0F, 0F, -8F, 0F); // wheelRightFront10
		rightFrontWheelModel[11].setRotationPoint(44F, -3F, -23F);

		rightFrontWheelModel[12].addShapeBox(4F, -15F, -5F, 8, 8, 10, 0F,0F, 0F, 0F, -1F, -4F, 0F, -1F, -4F, 0F, 0F, 0F, 0F, 4F, -1F, 0F, -5F, 3F, 0F, -5F, 3F, 0F, 4F, -1F, 0F); // wheelRightFront11
		rightFrontWheelModel[12].setRotationPoint(44F, -3F, -23F);
	}

	private void initleftBackWheelModel_1()
	{
		leftBackWheelModel[0] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelLeftBack0
		leftBackWheelModel[1] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelLeftBack1
		leftBackWheelModel[2] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelLeftBack2
		leftBackWheelModel[3] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelLeftBack3
		leftBackWheelModel[4] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelLeftBack4
		leftBackWheelModel[5] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelLeftBack5
		leftBackWheelModel[6] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelLeftBack6
		leftBackWheelModel[7] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelLeftBack7
		leftBackWheelModel[8] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelLeftBack8
		leftBackWheelModel[9] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelLeftBack9
		leftBackWheelModel[10] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelLeftBack10
		leftBackWheelModel[11] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelLeftBack11
		leftBackWheelModel[12] = new ModelRendererTurbo(this, 109, 23, textureX, textureY); // wheelLeftBackCap

		leftBackWheelModel[0].addBox(-4F, -15F, -5F, 8, 8, 10, 0F); // wheelLeftBack0
		leftBackWheelModel[0].setRotationPoint(-56F, -3F, 24F);

		leftBackWheelModel[1].addShapeBox(-12F, -15F, -5F, 8, 8, 10, 0F,-1F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, -4F, 0F, -5F, 3F, 0F, 4F, -1F, 0F, 4F, -1F, 0F, -5F, 3F, 0F); // wheelLeftBack1
		leftBackWheelModel[1].setRotationPoint(-56F, -3F, 24F);

		leftBackWheelModel[2].addShapeBox(-12F, -6.9F, -5F, 8, 8, 10, 0F,3F, -3F, 0F, -7F, 4F, 0F, -7F, 4F, 0F, 3F, -3F, 0F, -4F, -1F, 0F, 0F, -8F, 0F, 0F, -8F, 0F, -4F, -1F, 0F); // wheelLeftBack2
		leftBackWheelModel[2].setRotationPoint(-56F, -3F, 24F);

		leftBackWheelModel[3].addBox(-15F, -4F, -5F, 8, 8, 10, 0F); // wheelLeftBack3
		leftBackWheelModel[3].setRotationPoint(-56F, -3F, 24F);

		leftBackWheelModel[4].addShapeBox(-15F, 4.1F, -5F, 8, 8, 10, 0F,-7F, 4F, 0F, 3F, -3F, 0F, 3F, -3F, 0F, -7F, 4F, 0F, 0F, -8F, 0F, -4F, -1F, 0F, -4F, -1F, 0F, 0F, -8F, 0F); // wheelLeftBack4
		leftBackWheelModel[4].setRotationPoint(-56F, -3F, 24F);

		leftBackWheelModel[5].addShapeBox(-7F, 4F, -5F, 8, 8, 10, 0F,0F, 0F, 0F, -1F, -4F, 0F, -1F, -4F, 0F, 0F, 0F, 0F, 4F, -1F, 0F, -5F, 3F, 0F, -5F, 3F, 0F, 4F, -1F, 0F); // wheelLeftBack5
		leftBackWheelModel[5].setRotationPoint(-56F, -3F, 24F);

		leftBackWheelModel[6].addBox(-4F, 7F, -5F, 8, 8, 10, 0F); // wheelLeftBack6
		leftBackWheelModel[6].setRotationPoint(-56F, -3F, 24F);

		leftBackWheelModel[7].addShapeBox(-1F, 4F, -5F, 8, 8, 10, 0F,-1F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, -4F, 0F, -5F, 3F, 0F, 4F, -1F, 0F, 4F, -1F, 0F, -5F, 3F, 0F); // wheelLeftBack7
		leftBackWheelModel[7].setRotationPoint(-56F, -3F, 24F);

		leftBackWheelModel[8].addShapeBox(7F, 4.1F, -5F, 8, 8, 10, 0F,3F, -3F, 0F, -7F, 4F, 0F, -7F, 4F, 0F, 3F, -3F, 0F, -4F, -1F, 0F, 0F, -8F, 0F, 0F, -8F, 0F, -4F, -1F, 0F); // wheelLeftBack8
		leftBackWheelModel[8].setRotationPoint(-56F, -3F, 24F);

		leftBackWheelModel[9].addBox(7F, -4F, -5F, 8, 8, 10, 0F); // wheelLeftBack9
		leftBackWheelModel[9].setRotationPoint(-56F, -3F, 24F);

		leftBackWheelModel[10].addShapeBox(4F, -7.1F, -5F, 8, 8, 10, 0F,-7F, 4F, 0F, 3F, -3F, 0F, 3F, -3F, 0F, -7F, 4F, 0F, 0F, -8F, 0F, -4F, -1F, 0F, -4F, -1F, 0F, 0F, -8F, 0F); // wheelLeftBack10
		leftBackWheelModel[10].setRotationPoint(-56F, -3F, 24F);

		leftBackWheelModel[11].addShapeBox(4F, -15F, -5F, 8, 8, 10, 0F,0F, 0F, 0F, -1F, -4F, 0F, -1F, -4F, 0F, 0F, 0F, 0F, 4F, -1F, 0F, -5F, 3F, 0F, -5F, 3F, 0F, 4F, -1F, 0F); // wheelLeftBack11
		leftBackWheelModel[11].setRotationPoint(-56F, -3F, 24F);

		leftBackWheelModel[12].addBox(-6F, -7F, -3F, 14, 14, 7, 0F); // wheelLeftBackCap
		leftBackWheelModel[12].setRotationPoint(-57F, -3F, 23F);
	}

	private void initrightBackWheelModel_1()
	{
		rightBackWheelModel[0] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelRightBack0
		rightBackWheelModel[1] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelRightBack1
		rightBackWheelModel[2] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelRightBack2
		rightBackWheelModel[3] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelRightBack3
		rightBackWheelModel[4] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelRightBack4
		rightBackWheelModel[5] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelRightBack5
		rightBackWheelModel[6] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelRightBack6
		rightBackWheelModel[7] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelRightBack7
		rightBackWheelModel[8] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelRightBack8
		rightBackWheelModel[9] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelRightBack9
		rightBackWheelModel[10] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelRightBack10
		rightBackWheelModel[11] = new ModelRendererTurbo(this, 2, 22, textureX, textureY); // wheelRightBack11
		rightBackWheelModel[12] = new ModelRendererTurbo(this, 109, 23, textureX, textureY); // wheelRightBackCap

		rightBackWheelModel[0].addBox(-4F, -15F, -5F, 8, 8, 10, 0F); // wheelRightBack0
		rightBackWheelModel[0].setRotationPoint(-56F, -3F, -23F);

		rightBackWheelModel[1].addShapeBox(-12F, -15F, -5F, 8, 8, 10, 0F,-1F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, -4F, 0F, -5F, 3F, 0F, 4F, -1F, 0F, 4F, -1F, 0F, -5F, 3F, 0F); // wheelRightBack1
		rightBackWheelModel[1].setRotationPoint(-56F, -3F, -23F);

		rightBackWheelModel[2].addShapeBox(-12F, -6.9F, -5F, 8, 8, 10, 0F,3F, -3F, 0F, -7F, 4F, 0F, -7F, 4F, 0F, 3F, -3F, 0F, -4F, -1F, 0F, 0F, -8F, 0F, 0F, -8F, 0F, -4F, -1F, 0F); // wheelRightBack2
		rightBackWheelModel[2].setRotationPoint(-56F, -3F, -23F);

		rightBackWheelModel[3].addBox(-15F, -4F, -5F, 8, 8, 10, 0F); // wheelRightBack3
		rightBackWheelModel[3].setRotationPoint(-56F, -3F, -23F);

		rightBackWheelModel[4].addShapeBox(-15F, 4.1F, -5F, 8, 8, 10, 0F,-7F, 4F, 0F, 3F, -3F, 0F, 3F, -3F, 0F, -7F, 4F, 0F, 0F, -8F, 0F, -4F, -1F, 0F, -4F, -1F, 0F, 0F, -8F, 0F); // wheelRightBack4
		rightBackWheelModel[4].setRotationPoint(-56F, -3F, -23F);

		rightBackWheelModel[5].addShapeBox(-7F, 4F, -5F, 8, 8, 10, 0F,0F, 0F, 0F, -1F, -4F, 0F, -1F, -4F, 0F, 0F, 0F, 0F, 4F, -1F, 0F, -5F, 3F, 0F, -5F, 3F, 0F, 4F, -1F, 0F); // wheelRightBack5
		rightBackWheelModel[5].setRotationPoint(-56F, -3F, -23F);

		rightBackWheelModel[6].addBox(-4F, 7F, -5F, 8, 8, 10, 0F); // wheelRightBack6
		rightBackWheelModel[6].setRotationPoint(-56F, -3F, -23F);

		rightBackWheelModel[7].addShapeBox(-1F, 4F, -5F, 8, 8, 10, 0F,-1F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, -4F, 0F, -5F, 3F, 0F, 4F, -1F, 0F, 4F, -1F, 0F, -5F, 3F, 0F); // wheelRightBack7
		rightBackWheelModel[7].setRotationPoint(-56F, -3F, -23F);

		rightBackWheelModel[8].addShapeBox(7F, 4.1F, -5F, 8, 8, 10, 0F,3F, -3F, 0F, -7F, 4F, 0F, -7F, 4F, 0F, 3F, -3F, 0F, -4F, -1F, 0F, 0F, -8F, 0F, 0F, -8F, 0F, -4F, -1F, 0F); // wheelRightBack8
		rightBackWheelModel[8].setRotationPoint(-56F, -3F, -23F);

		rightBackWheelModel[9].addBox(7F, -4F, -5F, 8, 8, 10, 0F); // wheelRightBack9
		rightBackWheelModel[9].setRotationPoint(-56F, -3F, -23F);

		rightBackWheelModel[10].addShapeBox(4F, -7.1F, -5F, 8, 8, 10, 0F,-7F, 4F, 0F, 3F, -3F, 0F, 3F, -3F, 0F, -7F, 4F, 0F, 0F, -8F, 0F, -4F, -1F, 0F, -4F, -1F, 0F, 0F, -8F, 0F); // wheelRightBack10
		rightBackWheelModel[10].setRotationPoint(-56F, -3F, -23F);

		rightBackWheelModel[11].addShapeBox(4F, -15F, -5F, 8, 8, 10, 0F,0F, 0F, 0F, -1F, -4F, 0F, -1F, -4F, 0F, 0F, 0F, 0F, 4F, -1F, 0F, -5F, 3F, 0F, -5F, 3F, 0F, 4F, -1F, 0F); // wheelRightBack11
		rightBackWheelModel[11].setRotationPoint(-56F, -3F, -23F);

		rightBackWheelModel[12].addBox(-6F, -7F, -3F, 14, 14, 7, 0F); // wheelRightBackCap
		rightBackWheelModel[12].setRotationPoint(-57F, -3F, -23F);
	}
}