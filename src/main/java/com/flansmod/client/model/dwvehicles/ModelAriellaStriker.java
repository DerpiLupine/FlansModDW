package com.flansmod.client.model.dwvehicles; //Path where the model is located

import com.flansmod.client.model.ModelVehicle;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelAriellaStriker extends ModelVehicle //Same as Filename
{
	int textureX = 1024;
	int textureY = 1024;

	public ModelAriellaStriker() //Same as Filename
	{
		bodyModel = new ModelRendererTurbo[127];
		bodyModel[0] = new ModelRendererTurbo(this, 1, 92, textureX, textureY); // axleLeftFront1
		bodyModel[1] = new ModelRendererTurbo(this, 1, 103, textureX, textureY); // axleLeftFront2
		bodyModel[2] = new ModelRendererTurbo(this, 1, 92, textureX, textureY); // Box 12
		bodyModel[3] = new ModelRendererTurbo(this, 28, 80, textureX, textureY); // Box 13
		bodyModel[4] = new ModelRendererTurbo(this, 28, 64, textureX, textureY); // Box 25
		bodyModel[5] = new ModelRendererTurbo(this, 28, 64, textureX, textureY); // Box 26
		bodyModel[6] = new ModelRendererTurbo(this, 106, 95, textureX, textureY); // Box 27
		bodyModel[7] = new ModelRendererTurbo(this, 128, 2, textureX, textureY); // Box 43
		bodyModel[8] = new ModelRendererTurbo(this, 128, 2, textureX, textureY); // Box 44
		bodyModel[9] = new ModelRendererTurbo(this, 128, 2, textureX, textureY); // Box 45
		bodyModel[10] = new ModelRendererTurbo(this, 128, 82, textureX, textureY); // Box 46
		bodyModel[11] = new ModelRendererTurbo(this, 149, 84, textureX, textureY); // Box 47
		bodyModel[12] = new ModelRendererTurbo(this, 149, 84, textureX, textureY); // Box 48
		bodyModel[13] = new ModelRendererTurbo(this, 115, 95, textureX, textureY); // Box 49
		bodyModel[14] = new ModelRendererTurbo(this, 106, 95, textureX, textureY); // Box 50
		bodyModel[15] = new ModelRendererTurbo(this, 106, 95, textureX, textureY); // Box 51
		bodyModel[16] = new ModelRendererTurbo(this, 115, 95, textureX, textureY); // Box 52
		bodyModel[17] = new ModelRendererTurbo(this, 115, 95, textureX, textureY); // Box 53
		bodyModel[18] = new ModelRendererTurbo(this, 115, 95, textureX, textureY); // Box 54
		bodyModel[19] = new ModelRendererTurbo(this, 106, 95, textureX, textureY); // Box 55
		bodyModel[20] = new ModelRendererTurbo(this, 106, 95, textureX, textureY); // Box 56
		bodyModel[21] = new ModelRendererTurbo(this, 106, 95, textureX, textureY); // Box 57
		bodyModel[22] = new ModelRendererTurbo(this, 1, 30, textureX, textureY); // Box 58
		bodyModel[23] = new ModelRendererTurbo(this, 1, 30, textureX, textureY); // Box 59
		bodyModel[24] = new ModelRendererTurbo(this, 1, 30, textureX, textureY); // Box 60
		bodyModel[25] = new ModelRendererTurbo(this, 128, 19, textureX, textureY); // Box 64
		bodyModel[26] = new ModelRendererTurbo(this, 128, 34, textureX, textureY); // Box 65
		bodyModel[27] = new ModelRendererTurbo(this, 86, 100, textureX, textureY); // Box 66
		bodyModel[28] = new ModelRendererTurbo(this, 72, 1, textureX, textureY); // Box 80
		bodyModel[29] = new ModelRendererTurbo(this, 72, 92, textureX, textureY); // Box 82
		bodyModel[30] = new ModelRendererTurbo(this, 72, 92, textureX, textureY); // Box 84
		bodyModel[31] = new ModelRendererTurbo(this, 1, 21, textureX, textureY); // Box 86
		bodyModel[32] = new ModelRendererTurbo(this, 28, 21, textureX, textureY); // Box 87
		bodyModel[33] = new ModelRendererTurbo(this, 1, 21, textureX, textureY); // Box 88
		bodyModel[34] = new ModelRendererTurbo(this, 42, 42, textureX, textureY); // Box 89
		bodyModel[35] = new ModelRendererTurbo(this, 32, 96, textureX, textureY); // Box 90
		bodyModel[36] = new ModelRendererTurbo(this, 42, 51, textureX, textureY); // Box 91
		bodyModel[37] = new ModelRendererTurbo(this, 72, 92, textureX, textureY); // Box 92
		bodyModel[38] = new ModelRendererTurbo(this, 72, 92, textureX, textureY); // Box 93
		bodyModel[39] = new ModelRendererTurbo(this, 97, 76, textureX, textureY); // Box 94
		bodyModel[40] = new ModelRendererTurbo(this, 151, 7, textureX, textureY); // Box 98
		bodyModel[41] = new ModelRendererTurbo(this, 162, 7, textureX, textureY); // Box 99
		bodyModel[42] = new ModelRendererTurbo(this, 151, 1, textureX, textureY); // Box 100
		bodyModel[43] = new ModelRendererTurbo(this, 106, 39, textureX, textureY); // Box 102
		bodyModel[44] = new ModelRendererTurbo(this, 106, 45, textureX, textureY); // Box 103
		bodyModel[45] = new ModelRendererTurbo(this, 106, 39, textureX, textureY); // Box 104
		bodyModel[46] = new ModelRendererTurbo(this, 157, 61, textureX, textureY); // Box 113
		bodyModel[47] = new ModelRendererTurbo(this, 157, 61, textureX, textureY); // Box 114
		bodyModel[48] = new ModelRendererTurbo(this, 72, 42, textureX, textureY); // Box 115
		bodyModel[49] = new ModelRendererTurbo(this, 103, 55, textureX, textureY); // Box 117
		bodyModel[50] = new ModelRendererTurbo(this, 72, 31, textureX, textureY); // Box 118
		bodyModel[51] = new ModelRendererTurbo(this, 72, 81, textureX, textureY); // Box 119
		bodyModel[52] = new ModelRendererTurbo(this, 72, 55, textureX, textureY); // Box 121
		bodyModel[53] = new ModelRendererTurbo(this, 72, 70, textureX, textureY); // Box 122
		bodyModel[54] = new ModelRendererTurbo(this, 72, 17, textureX, textureY); // Box 123
		bodyModel[55] = new ModelRendererTurbo(this, 93, 43, textureX, textureY); // Box 124
		bodyModel[56] = new ModelRendererTurbo(this, 100, 31, textureX, textureY); // Box 125
		bodyModel[57] = new ModelRendererTurbo(this, 128, 75, textureX, textureY); // Box 126
		bodyModel[58] = new ModelRendererTurbo(this, 128, 75, textureX, textureY); // Box 127
		bodyModel[59] = new ModelRendererTurbo(this, 128, 75, textureX, textureY); // Box 128
		bodyModel[60] = new ModelRendererTurbo(this, 128, 75, textureX, textureY); // Box 129
		bodyModel[61] = new ModelRendererTurbo(this, 134, 95, textureX, textureY); // Box 130
		bodyModel[62] = new ModelRendererTurbo(this, 134, 95, textureX, textureY); // Box 131
		bodyModel[63] = new ModelRendererTurbo(this, 134, 95, textureX, textureY); // Box 132
		bodyModel[64] = new ModelRendererTurbo(this, 134, 95, textureX, textureY); // Box 133
		bodyModel[65] = new ModelRendererTurbo(this, 1, 68, textureX, textureY); // Box 134
		bodyModel[66] = new ModelRendererTurbo(this, 1, 80, textureX, textureY); // Box 135
		bodyModel[67] = new ModelRendererTurbo(this, 1, 68, textureX, textureY); // Box 136
		bodyModel[68] = new ModelRendererTurbo(this, 85, 96, textureX, textureY); // Box 139
		bodyModel[69] = new ModelRendererTurbo(this, 85, 96, textureX, textureY); // Box 144
		bodyModel[70] = new ModelRendererTurbo(this, 85, 96, textureX, textureY); // Box 145
		bodyModel[71] = new ModelRendererTurbo(this, 85, 96, textureX, textureY); // Box 146
		bodyModel[72] = new ModelRendererTurbo(this, 122, 95, textureX, textureY); // Box 147
		bodyModel[73] = new ModelRendererTurbo(this, 122, 95, textureX, textureY); // Box 0
		bodyModel[74] = new ModelRendererTurbo(this, 129, 95, textureX, textureY); // Box 1
		bodyModel[75] = new ModelRendererTurbo(this, 122, 95, textureX, textureY); // Box 2
		bodyModel[76] = new ModelRendererTurbo(this, 72, 100, textureX, textureY); // Box 3
		bodyModel[77] = new ModelRendererTurbo(this, 86, 100, textureX, textureY); // Box 5
		bodyModel[78] = new ModelRendererTurbo(this, 72, 96, textureX, textureY); // Box 6
		bodyModel[79] = new ModelRendererTurbo(this, 72, 96, textureX, textureY); // Box 7
		bodyModel[80] = new ModelRendererTurbo(this, 129, 95, textureX, textureY); // Box 8
		bodyModel[81] = new ModelRendererTurbo(this, 106, 92, textureX, textureY); // Box 9
		bodyModel[82] = new ModelRendererTurbo(this, 106, 92, textureX, textureY); // Box 11
		bodyModel[83] = new ModelRendererTurbo(this, 97, 100, textureX, textureY); // Box 12
		bodyModel[84] = new ModelRendererTurbo(this, 79, 100, textureX, textureY); // Box 13
		bodyModel[85] = new ModelRendererTurbo(this, 79, 100, textureX, textureY); // Box 14
		bodyModel[86] = new ModelRendererTurbo(this, 128, 34, textureX, textureY); // Box 15
		bodyModel[87] = new ModelRendererTurbo(this, 128, 19, textureX, textureY); // Box 16
		bodyModel[88] = new ModelRendererTurbo(this, 128, 60, textureX, textureY); // Box 17
		bodyModel[89] = new ModelRendererTurbo(this, 128, 48, textureX, textureY); // Box 18
		bodyModel[90] = new ModelRendererTurbo(this, 72, 92, textureX, textureY); // Box 19
		bodyModel[91] = new ModelRendererTurbo(this, 72, 92, textureX, textureY); // Box 20
		bodyModel[92] = new ModelRendererTurbo(this, 79, 100, textureX, textureY); // Box 21
		bodyModel[93] = new ModelRendererTurbo(this, 72, 92, textureX, textureY); // Box 22
		bodyModel[94] = new ModelRendererTurbo(this, 72, 92, textureX, textureY); // Box 23
		bodyModel[95] = new ModelRendererTurbo(this, 97, 100, textureX, textureY); // Box 24
		bodyModel[96] = new ModelRendererTurbo(this, 79, 100, textureX, textureY); // Box 25
		bodyModel[97] = new ModelRendererTurbo(this, 177, 14, textureX, textureY); // Box 26
		bodyModel[98] = new ModelRendererTurbo(this, 177, 14, textureX, textureY); // Box 29
		bodyModel[99] = new ModelRendererTurbo(this, 192, 14, textureX, textureY); // Box 30
		bodyModel[100] = new ModelRendererTurbo(this, 177, 26, textureX, textureY); // Box 31
		bodyModel[101] = new ModelRendererTurbo(this, 177, 26, textureX, textureY); // Box 33
		bodyModel[102] = new ModelRendererTurbo(this, 177, 33, textureX, textureY); // Box 34
		bodyModel[103] = new ModelRendererTurbo(this, 177, 33, textureX, textureY); // Box 35
		bodyModel[104] = new ModelRendererTurbo(this, 32, 1, textureX, textureY); // Box 37
		bodyModel[105] = new ModelRendererTurbo(this, 32, 1, textureX, textureY); // Box 38
		bodyModel[106] = new ModelRendererTurbo(this, 32, 1, textureX, textureY); // Box 39
		bodyModel[107] = new ModelRendererTurbo(this, 198, 1, textureX, textureY); // Box 40
		bodyModel[108] = new ModelRendererTurbo(this, 219, 1, textureX, textureY); // Box 0
		bodyModel[109] = new ModelRendererTurbo(this, 177, 1, textureX, textureY); // Box 1
		bodyModel[110] = new ModelRendererTurbo(this, 192, 27, textureX, textureY); // Box 2
		bodyModel[111] = new ModelRendererTurbo(this, 201, 29, textureX, textureY); // Box 3
		bodyModel[112] = new ModelRendererTurbo(this, 32, 1, textureX, textureY); // Box 5
		bodyModel[113] = new ModelRendererTurbo(this, 1, 115, textureX, textureY); // Box 6
		bodyModel[114] = new ModelRendererTurbo(this, 6, 120, textureX, textureY); // Box 7
		bodyModel[115] = new ModelRendererTurbo(this, 1, 120, textureX, textureY); // Box 8
		bodyModel[116] = new ModelRendererTurbo(this, 11, 120, textureX, textureY); // Box 9
		bodyModel[117] = new ModelRendererTurbo(this, 11, 126, textureX, textureY); // Box 10
		bodyModel[118] = new ModelRendererTurbo(this, 11, 120, textureX, textureY); // Box 11
		bodyModel[119] = new ModelRendererTurbo(this, 6, 131, textureX, textureY); // Box 12
		bodyModel[120] = new ModelRendererTurbo(this, 1, 131, textureX, textureY); // Box 13
		bodyModel[121] = new ModelRendererTurbo(this, 101, 107, textureX, textureY); // Box 15
		bodyModel[122] = new ModelRendererTurbo(this, 72, 107, textureX, textureY); // Box 16
		bodyModel[123] = new ModelRendererTurbo(this, 72, 119, textureX, textureY); // Box 17
		bodyModel[124] = new ModelRendererTurbo(this, 1, 60, textureX, textureY); // Box 4
		bodyModel[125] = new ModelRendererTurbo(this, 1, 39, textureX, textureY); // Box 5
		bodyModel[126] = new ModelRendererTurbo(this, 1, 51, textureX, textureY); // Box 7

		bodyModel[0].addShapeBox(0F, 0F, 0F, 7, 2, 8, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // axleLeftFront1
		bodyModel[0].setRotationPoint(10.5F, -0.5F, -4F);

		bodyModel[1].addBox(0F, 0F, 0F, 7, 3, 8, 0F); // axleLeftFront2
		bodyModel[1].setRotationPoint(10.5F, 1.5F, -4F);

		bodyModel[2].addShapeBox(0F, 0F, 0F, 7, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F); // Box 12
		bodyModel[2].setRotationPoint(10.5F, 4.5F, -4F);

		bodyModel[3].addBox(0F, 0F, 0F, 3, 1, 12, 0F); // Box 13
		bodyModel[3].setRotationPoint(12.5F, 2.5F, -6F);

		bodyModel[4].addShapeBox(0F, 0F, 0F, 3, 1, 12, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 25
		bodyModel[4].setRotationPoint(12.5F, 1.5F, -6F);

		bodyModel[5].addShapeBox(0F, 0F, 0F, 3, 1, 12, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 26
		bodyModel[5].setRotationPoint(12.5F, 3.5F, -6F);

		bodyModel[6].addShapeBox(0F, 0F, 0F, 3, 6, 1, 0F, 3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 27
		bodyModel[6].setRotationPoint(9.5F, -9.5F, -5.5F);

		bodyModel[7].addShapeBox(0F, 0F, 0F, 3, 1, 14, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 43
		bodyModel[7].setRotationPoint(6.5F, -11.5F, -7F);

		bodyModel[8].addBox(0F, 0F, 0F, 3, 1, 14, 0F); // Box 44
		bodyModel[8].setRotationPoint(6.5F, -10.5F, -7F);

		bodyModel[9].addShapeBox(0F, 0F, 0F, 3, 1, 14, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 45
		bodyModel[9].setRotationPoint(6.5F, -9.5F, -7F);

		bodyModel[10].addBox(0F, 0F, 0F, 4, 3, 6, 0F); // Box 46
		bodyModel[10].setRotationPoint(6F, -12F, -3F);

		bodyModel[11].addShapeBox(0F, 0F, 0F, 4, 1, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 47
		bodyModel[11].setRotationPoint(6F, -9F, -3F);

		bodyModel[12].addShapeBox(0F, 0F, 0F, 4, 1, 6, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 48
		bodyModel[12].setRotationPoint(6F, -13F, -3F);

		bodyModel[13].addShapeBox(0F, 0F, 0F, 2, 6, 1, 0F, 2.5F, 0F, 0F, -3.5F, 0F, 0F, -3F, 0F, 0F, 3F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 49
		bodyModel[13].setRotationPoint(13F, -3.5F, -6F);

		bodyModel[14].addShapeBox(0F, 0F, 0F, 3, 6, 1, 0F, 2F, 0F, 0F, -4F, 0F, 0F, -3F, 0F, 0F, 3F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 50
		bodyModel[14].setRotationPoint(9.5F, -9.5F, -6.5F);

		bodyModel[15].addShapeBox(0F, 0F, 0F, 3, 6, 1, 0F, 3F, 0F, 0F, -3F, 0F, 0F, -4F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 51
		bodyModel[15].setRotationPoint(9.5F, -9.5F, -4.5F);

		bodyModel[16].addShapeBox(0F, 0F, 0F, 2, 6, 1, 0F, 3F, 0F, 0F, -3F, 0F, 0F, -3.5F, 0F, 0F, 2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 52
		bodyModel[16].setRotationPoint(13F, -3.5F, -5F);

		bodyModel[17].addShapeBox(0F, 0F, 0F, 2, 6, 1, 0F, 3F, 0F, 0F, -3F, 0F, 0F, -3.5F, 0F, 0F, 2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 53
		bodyModel[17].setRotationPoint(13F, -3.5F, 5F);

		bodyModel[18].addShapeBox(0F, 0F, 0F, 2, 6, 1, 0F, 2.5F, 0F, 0F, -3.5F, 0F, 0F, -3F, 0F, 0F, 3F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 54
		bodyModel[18].setRotationPoint(13F, -3.5F, 4F);

		bodyModel[19].addShapeBox(0F, 0F, 0F, 3, 6, 1, 0F, 3F, 0F, 0F, -3F, 0F, 0F, -4F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 55
		bodyModel[19].setRotationPoint(9.5F, -9.5F, 5.5F);

		bodyModel[20].addShapeBox(0F, 0F, 0F, 3, 6, 1, 0F, 3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 56
		bodyModel[20].setRotationPoint(9.5F, -9.5F, 4.5F);

		bodyModel[21].addShapeBox(0F, 0F, 0F, 3, 6, 1, 0F, 2F, 0F, 0F, -4F, 0F, 0F, -3F, 0F, 0F, 3F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 57
		bodyModel[21].setRotationPoint(9.5F, -9.5F, 3.5F);

		bodyModel[22].addShapeBox(0F, 0F, 0F, 5, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 58
		bodyModel[22].setRotationPoint(7.5F, -10F, -3F);

		bodyModel[23].addBox(0F, 0F, 0F, 5, 2, 6, 0F); // Box 59
		bodyModel[23].setRotationPoint(7.5F, -8F, -3F);

		bodyModel[24].addShapeBox(0F, 0F, 0F, 5, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 60
		bodyModel[24].setRotationPoint(7.5F, -6F, -3F);

		bodyModel[25].addBox(0F, 0F, 0F, 9, 2, 12, 0F); // Box 64
		bodyModel[25].setRotationPoint(-10.5F, -4.5F, -6F);

		bodyModel[26].addShapeBox(0F, 0F, 0F, 9, 1, 12, 0F, -1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 65
		bodyModel[26].setRotationPoint(-10.5F, -5.5F, -6F);

		bodyModel[27].addShapeBox(0F, 0F, 0F, 4, 2, 1, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -3.5F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, 2.5F, 0F); // Box 66
		bodyModel[27].setRotationPoint(0.5F, 2.5F, 6F);

		bodyModel[28].addBox(0F, 0F, 0F, 12, 5, 10, 0F); // Box 80
		bodyModel[28].setRotationPoint(-7.5F, 1.5F, -5F);

		bodyModel[29].addShapeBox(0F, 0F, 0F, 15, 2, 1, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 82
		bodyModel[29].setRotationPoint(-14.5F, 5.5F, -7F);

		bodyModel[30].addShapeBox(0F, 0F, 0F, 15, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 84
		bodyModel[30].setRotationPoint(-14.5F, 5.5F, -6F);

		bodyModel[31].addShapeBox(0F, 0F, 0F, 7, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F); // Box 86
		bodyModel[31].setRotationPoint(-23.5F, 4.5F, -3F);

		bodyModel[32].addBox(0F, 0F, 0F, 7, 3, 6, 0F); // Box 87
		bodyModel[32].setRotationPoint(-23.5F, 1.5F, -3F);

		bodyModel[33].addShapeBox(0F, 0F, 0F, 7, 2, 6, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 88
		bodyModel[33].setRotationPoint(-23.5F, -0.5F, -3F);

		bodyModel[34].addShapeBox(0F, 0F, 0F, 3, 3, 5, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F); // Box 89
		bodyModel[34].setRotationPoint(-15.5F, -2.5F, -2.5F);

		bodyModel[35].addShapeBox(0F, 0F, 0F, 8, 2, 5, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F); // Box 90
		bodyModel[35].setRotationPoint(-22.5F, -4.5F, -2.5F);

		bodyModel[36].addBox(0F, 0F, 0F, 1, 6, 5, 0F); // Box 91
		bodyModel[36].setRotationPoint(-13.5F, 0.5F, -2.5F);

		bodyModel[37].addShapeBox(0F, 0F, 0F, 15, 2, 1, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 92
		bodyModel[37].setRotationPoint(-14.5F, 5.5F, 5F);

		bodyModel[38].addShapeBox(0F, 0F, 0F, 15, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 93
		bodyModel[38].setRotationPoint(-14.5F, 5.5F, 6F);

		bodyModel[39].addShapeBox(0F, 0F, 0F, 3, 5, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F); // Box 94
		bodyModel[39].setRotationPoint(-10.5F, 1.5F, -5F);

		bodyModel[40].addBox(0F, 0F, 0F, 2, 5, 3, 0F); // Box 98
		bodyModel[40].setRotationPoint(-12.5F, 0.5F, -1.5F);

		bodyModel[41].addBox(0F, 0F, 0F, 4, 3, 3, 0F); // Box 99
		bodyModel[41].setRotationPoint(-14.5F, -2.5F, -1.5F);

		bodyModel[42].addBox(0F, 0F, 0F, 7, 2, 3, 0F); // Box 100
		bodyModel[42].setRotationPoint(-17.5F, -4.5F, -1.5F);

		bodyModel[43].addShapeBox(0F, 0F, 0F, 1, 1, 4, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 102
		bodyModel[43].setRotationPoint(-5.5F, -2.5F, 0.5F);

		bodyModel[44].addBox(0F, 0F, 0F, 2, 1, 4, 0F); // Box 103
		bodyModel[44].setRotationPoint(-4.5F, -2.5F, 0.5F);

		bodyModel[45].addShapeBox(0F, 0F, 0F, 1, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 104
		bodyModel[45].setRotationPoint(-2.5F, -2.5F, 0.5F);

		bodyModel[46].addBox(0F, 0F, 0F, 1, 1, 12, 0F); // Box 113
		bodyModel[46].setRotationPoint(-7.5F, 6.5F, -6F);

		bodyModel[47].addBox(0F, 0F, 0F, 1, 1, 12, 0F); // Box 114
		bodyModel[47].setRotationPoint(-0.5F, 6.5F, -6F);

		bodyModel[48].addBox(0F, 0F, 0F, 2, 4, 8, 0F); // Box 115
		bodyModel[48].setRotationPoint(4.5F, 2.5F, -4F);

		bodyModel[49].addShapeBox(0F, 0F, 0F, 3, 5, 9, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 117
		bodyModel[49].setRotationPoint(-1.5F, -7.5F, -4.5F);

		bodyModel[50].addShapeBox(0F, 0F, 0F, 9, 1, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 118
		bodyModel[50].setRotationPoint(-1.5F, -2.5F, -4.5F);

		bodyModel[51].addShapeBox(0F, 0F, 0F, 3, 1, 9, 0F, 0F, -3F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, -3F, -1F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F); // Box 119
		bodyModel[51].setRotationPoint(-1.5F, -8.5F, -4.5F);

		bodyModel[52].addBox(0F, 0F, 0F, 6, 5, 9, 0F); // Box 121
		bodyModel[52].setRotationPoint(1.5F, -7.5F, -4.5F);

		bodyModel[53].addShapeBox(0F, 0F, 0F, 6, 1, 9, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 122
		bodyModel[53].setRotationPoint(1.5F, -8.5F, -4.5F);

		bodyModel[54].addShapeBox(0F, 0F, 0F, 12, 3, 10, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 123
		bodyModel[54].setRotationPoint(-7.5F, -1.5F, -5F);

		bodyModel[55].addShapeBox(0F, 0F, 0F, 2, 3, 8, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 124
		bodyModel[55].setRotationPoint(4.5F, -0.5F, -4F);

		bodyModel[56].addShapeBox(0F, 0F, 0F, 4, 1, 4, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 125
		bodyModel[56].setRotationPoint(-5.5F, -2.5F, -4F);

		bodyModel[57].addShapeBox(0F, 0F, 0F, 8, 4, 2, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 126
		bodyModel[57].setRotationPoint(-22.5F, 4.5F, 4F);

		bodyModel[58].addShapeBox(0F, 0F, 0F, 8, 4, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F); // Box 127
		bodyModel[58].setRotationPoint(-22.5F, 4.5F, 6F);

		bodyModel[59].addShapeBox(0F, 0F, 0F, 8, 4, 2, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 128
		bodyModel[59].setRotationPoint(-22.5F, 4.5F, -8F);

		bodyModel[60].addShapeBox(0F, 0F, 0F, 8, 4, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F); // Box 129
		bodyModel[60].setRotationPoint(-22.5F, 4.5F, -6F);

		bodyModel[61].addShapeBox(0F, 0F, 0F, 2, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 130
		bodyModel[61].setRotationPoint(-24.5F, 5.5F, -6F);

		bodyModel[62].addShapeBox(0F, 0F, 0F, 2, 2, 1, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 131
		bodyModel[62].setRotationPoint(-24.5F, 5.5F, -7F);

		bodyModel[63].addShapeBox(0F, 0F, 0F, 2, 2, 1, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 132
		bodyModel[63].setRotationPoint(-24.5F, 5.5F, 5F);

		bodyModel[64].addShapeBox(0F, 0F, 0F, 2, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 133
		bodyModel[64].setRotationPoint(-24.5F, 5.5F, 6F);

		bodyModel[65].addShapeBox(0F, 0F, 0F, 3, 1, 10, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 134
		bodyModel[65].setRotationPoint(-21.5F, 1.5F, -5F);

		bodyModel[66].addBox(0F, 0F, 0F, 3, 1, 10, 0F); // Box 135
		bodyModel[66].setRotationPoint(-21.5F, 2.5F, -5F);

		bodyModel[67].addShapeBox(0F, 0F, 0F, 3, 1, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 136
		bodyModel[67].setRotationPoint(-21.5F, 3.5F, -5F);

		bodyModel[68].addShapeBox(0F, 0F, 0F, 9, 2, 1, 0F, 0F, -0.5F, 0F, 0F, -2F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, 1F, 0F, 0F, 1.5F, 0F, 0F, 0F, 0F); // Box 139
		bodyModel[68].setRotationPoint(-19.5F, 2F, -5F);

		bodyModel[69].addShapeBox(0F, 0F, 0F, 9, 2, 1, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -2F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 1.5F, 0F, 0F, 1F, 0F, 0F, -0.5F, 0F); // Box 144
		bodyModel[69].setRotationPoint(-19.5F, 2F, -4F);

		bodyModel[70].addShapeBox(0F, 0F, 0F, 9, 2, 1, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -2F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 1.5F, 0F, 0F, 1F, 0F, 0F, -0.5F, 0F); // Box 145
		bodyModel[70].setRotationPoint(-19.5F, 2F, 4F);

		bodyModel[71].addShapeBox(0F, 0F, 0F, 9, 2, 1, 0F, 0F, -0.5F, 0F, 0F, -2F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, 1F, 0F, 0F, 1.5F, 0F, 0F, 0F, 0F); // Box 146
		bodyModel[71].setRotationPoint(-19.5F, 2F, 3F);

		bodyModel[72].addShapeBox(0F, 0F, 0F, 2, 5, 1, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 147
		bodyModel[72].setRotationPoint(-18F, 0.5F, 5F);

		bodyModel[73].addShapeBox(0F, 0F, 0F, 2, 5, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 0
		bodyModel[73].setRotationPoint(-18F, 0.5F, 6F);

		bodyModel[74].addShapeBox(0F, 0F, 0F, 1, 5, 1, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 1
		bodyModel[74].setRotationPoint(-17.5F, -4.5F, 5.5F);

		bodyModel[75].addShapeBox(0F, 0F, 0F, 2, 5, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 2
		bodyModel[75].setRotationPoint(-18F, 0.5F, -6F);

		bodyModel[76].addShapeBox(0F, 0F, 0F, 2, 5, 1, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 3
		bodyModel[76].setRotationPoint(-18F, 0.5F, -7F);

		bodyModel[77].addShapeBox(0F, 0F, 0F, 4, 2, 1, 0F, 0F, -3.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 2.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 3F, 0F); // Box 5
		bodyModel[77].setRotationPoint(0.5F, 2.5F, 5F);

		bodyModel[78].addShapeBox(0F, 0F, 0F, 4, 2, 1, 0F, 0F, -3.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 2.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 3F, 0F); // Box 6
		bodyModel[78].setRotationPoint(0.5F, 2.5F, -7F);

		bodyModel[79].addShapeBox(0F, 0F, 0F, 4, 2, 1, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -3.5F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, 2.5F, 0F); // Box 7
		bodyModel[79].setRotationPoint(0.5F, 2.5F, -6F);

		bodyModel[80].addShapeBox(0F, 0F, 0F, 1, 5, 1, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 8
		bodyModel[80].setRotationPoint(-17.5F, -4.5F, -6.5F);

		bodyModel[81].addShapeBox(0F, 0F, 0F, 15, 1, 1, 0F, 0F, 0F, 0F, 0F, 5F, -2F, 0F, 5F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -5F, -2F, 0F, -5F, 2F, 0F, 0F, 0F); // Box 9
		bodyModel[81].setRotationPoint(-16.5F, 1.5F, -6.5F);

		bodyModel[82].addShapeBox(0F, 0F, 0F, 15, 1, 1, 0F, 0F, 0F, 0F, 0F, 5F, 2F, 0F, 5F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -5F, 2F, 0F, -5F, -2F, 0F, 0F, 0F); // Box 11
		bodyModel[82].setRotationPoint(-16.5F, 1.5F, 5.5F);

		bodyModel[83].addShapeBox(0F, 0F, 0F, 1, 2, 1, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, 0F); // Box 12
		bodyModel[83].setRotationPoint(5.5F, 2.5F, 6F);

		bodyModel[84].addShapeBox(0F, 0F, 0F, 1, 2, 2, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F); // Box 13
		bodyModel[84].setRotationPoint(5.5F, 2.5F, 4F);

		bodyModel[85].addShapeBox(0F, 0F, 0F, 1, 2, 2, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F); // Box 14
		bodyModel[85].setRotationPoint(4.5F, 2.5F, 4F);

		bodyModel[86].addShapeBox(0F, 0F, 0F, 9, 1, 12, 0F, -1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 15
		bodyModel[86].setRotationPoint(-23.5F, -7.5F, -6F);

		bodyModel[87].addBox(0F, 0F, 0F, 9, 2, 12, 0F); // Box 16
		bodyModel[87].setRotationPoint(-23.5F, -6.5F, -6F);

		bodyModel[88].addBox(0F, 0F, 0F, 4, 4, 10, 0F); // Box 17
		bodyModel[88].setRotationPoint(-14.5F, -8.5F, -5F);

		bodyModel[89].addShapeBox(0F, 0F, 0F, 4, 1, 10, 0F, -1F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 18
		bodyModel[89].setRotationPoint(-14.5F, -9.5F, -5F);

		bodyModel[90].addShapeBox(0F, 0F, 0F, 1, 2, 1, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 19
		bodyModel[90].setRotationPoint(4.5F, 2.5F, 5F);

		bodyModel[91].addShapeBox(0F, 0F, 0F, 1, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 20
		bodyModel[91].setRotationPoint(4.5F, 2.5F, 6F);

		bodyModel[92].addShapeBox(0F, 0F, 0F, 1, 2, 2, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F); // Box 21
		bodyModel[92].setRotationPoint(4.5F, 2.5F, -6F);

		bodyModel[93].addShapeBox(0F, 0F, 0F, 1, 2, 1, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 22
		bodyModel[93].setRotationPoint(4.5F, 2.5F, -7F);

		bodyModel[94].addShapeBox(0F, 0F, 0F, 1, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 23
		bodyModel[94].setRotationPoint(4.5F, 2.5F, -6F);

		bodyModel[95].addShapeBox(0F, 0F, 0F, 1, 2, 1, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, 0F, 0F, 0F, 0F); // Box 24
		bodyModel[95].setRotationPoint(5.5F, 2.5F, -7F);

		bodyModel[96].addShapeBox(0F, 0F, 0F, 1, 2, 2, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F); // Box 25
		bodyModel[96].setRotationPoint(5.5F, 2.5F, -6F);

		bodyModel[97].addShapeBox(0F, 0F, 0F, 2, 1, 10, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 26
		bodyModel[97].setRotationPoint(3.5F, -12.5F, -5F);

		bodyModel[98].addShapeBox(0F, 0F, 0F, 2, 1, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 29
		bodyModel[98].setRotationPoint(3.5F, -11.5F, -5F);

		bodyModel[99].addBox(0F, 0F, 0F, 2, 3, 4, 0F); // Box 30
		bodyModel[99].setRotationPoint(4.5F, -11.5F, -2F);

		bodyModel[100].addShapeBox(0F, 0F, 0F, 2, 1, 5, 0F, 2.5F, 0F, 0F, -3.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 31
		bodyModel[100].setRotationPoint(3.5F, -12.5F, -10F);

		bodyModel[101].addShapeBox(0F, 0F, 0F, 2, 1, 5, 0F, 3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2.5F, 0F, 0F, -3.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 33
		bodyModel[101].setRotationPoint(3.5F, -11.5F, -10F);

		bodyModel[102].addShapeBox(0F, 0F, 0F, 2, 1, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 3F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -3.5F, 0F, 0F, 2.5F, 0F, 0F); // Box 34
		bodyModel[102].setRotationPoint(3.5F, -11.5F, 5F);

		bodyModel[103].addShapeBox(0F, 0F, 0F, 2, 1, 5, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -3.5F, 0F, 0F, 2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 3F, 0F, 0F); // Box 35
		bodyModel[103].setRotationPoint(3.5F, -12.5F, 5F);

		bodyModel[104].addShapeBox(0F, 0F, 0F, 1, 6, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, -3F, 0F, -3F, -3F); // Box 37
		bodyModel[104].setRotationPoint(7.5F, -15.5F, 6F);

		bodyModel[105].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 38
		bodyModel[105].setRotationPoint(7.5F, -12.5F, 6F);

		bodyModel[106].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F); // Box 39
		bodyModel[106].setRotationPoint(7.5F, -12.5F, -7F);

		bodyModel[107].addShapeBox(0F, -4F, -2F, 1, 3, 9, 0F, 0F, -2F, -1F, 0F, -2F, -1F, 0F, -2F, -6F, 0F, -2F, -6F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -5F, 0F, 0F, -5F); // Box 40
		bodyModel[107].setRotationPoint(6.5F, -14.5F, -3F);
		bodyModel[107].rotateAngleZ = -0.43633231F;

		bodyModel[108].addShapeBox(0F, -2F, -2F, 1, 3, 9, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, -5F, 0F, -1F, -5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -5F, 0F, 0F, -5F); // Box 0
		bodyModel[108].setRotationPoint(6.5F, -14.5F, -3F);
		bodyModel[108].rotateAngleZ = -0.43633231F;

		bodyModel[109].addShapeBox(0F, 1F, -2F, 1, 3, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -5F, 0F, 0F, -5F, 0F, -2F, -1F, 0F, -2F, -1F, 0F, -2F, -6F, 0F, -2F, -6F); // Box 1
		bodyModel[109].setRotationPoint(6.5F, -14.5F, -3F);
		bodyModel[109].rotateAngleZ = -0.43633231F;

		bodyModel[110].addBox(0F, 0F, 0F, 1, 2, 3, 0F); // Box 2
		bodyModel[110].setRotationPoint(7.5F, -15F, -4.5F);

		bodyModel[111].addShapeBox(0F, 0F, 0F, 1, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 3
		bodyModel[111].setRotationPoint(7.5F, -13F, -4.5F);

		bodyModel[112].addShapeBox(0F, 0F, 0F, 1, 6, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, -3F, 0F, -3F, -3F); // Box 5
		bodyModel[112].setRotationPoint(7.5F, -15.5F, -9F);

		bodyModel[113].addBox(0F, 0F, 0F, 11, 1, 3, 0F); // Box 6
		bodyModel[113].setRotationPoint(-24.5F, 3.5F, 7F);

		bodyModel[114].addShapeBox(0F, 0F, 0F, 1, 9, 1, 0F, 0F, 0F, 1.5F, 0F, 0F, 1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 7
		bodyModel[114].setRotationPoint(-23F, -4.5F, 6.5F);

		bodyModel[115].addShapeBox(0F, 0F, 0F, 1, 9, 1, 0F, 0F, 0F, 1.5F, 0F, 0F, 1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 8
		bodyModel[115].setRotationPoint(-16F, -4.5F, 6.5F);

		bodyModel[116].addShapeBox(0F, 0F, 1F, 9, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0.5F, 0F, -1F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // Box 9
		bodyModel[116].setRotationPoint(-23.5F, -5F, 6F);
		bodyModel[116].rotateAngleX = 0.17453293F;

		bodyModel[117].addBox(0F, 4F, 0F, 8, 5, 2, 0F); // Box 10
		bodyModel[117].setRotationPoint(-23F, -5F, 6F);
		bodyModel[117].rotateAngleX = 0.17453293F;

		bodyModel[118].addBox(0F, 0F, 0F, 9, 4, 1, 0F); // Box 11
		bodyModel[118].setRotationPoint(-23.5F, -5F, 6F);
		bodyModel[118].rotateAngleX = 0.17453293F;

		bodyModel[119].addBox(1.5F, 3F, 2F, 1, 3, 1, 0F); // Box 12
		bodyModel[119].setRotationPoint(-23F, -5F, 6F);
		bodyModel[119].rotateAngleX = 0.17453293F;

		bodyModel[120].addBox(5.5F, 3F, 2F, 1, 3, 1, 0F); // Box 13
		bodyModel[120].setRotationPoint(-23F, -5F, 6F);
		bodyModel[120].rotateAngleX = 0.17453293F;

		bodyModel[121].addBox(0F, 0F, 0F, 2, 2, 4, 0F); // Box 15
		bodyModel[121].setRotationPoint(-20.5F, -4.5F, -6.5F);

		bodyModel[122].addBox(0F, 0F, 0F, 11, 8, 3, 0F); // Box 16
		bodyModel[122].setRotationPoint(-25.5F, -5F, -9.5F);
		bodyModel[122].rotateAngleZ = 0.34906585F;

		bodyModel[123].addShapeBox(0.5F, 0.5F, -0.2F, 30, 21, 1, 0F, 0F, 0F, 0F, -20F, 0F, 0F, -20F, 0F, 0F, 0F, 0F, 0F, 0F, -14F, 0F, -20F, -14F, 0F, -20F, -14F, 0F, 0F, -14F, 0F); // Box 17
		bodyModel[123].setRotationPoint(-25.5F, -5F, -9.5F);
		bodyModel[123].rotateAngleZ = 0.34906585F;

		bodyModel[124].addShapeBox(-5.5F, -7.5F, -2.5F, 11, 2, 5, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 4
		bodyModel[124].setRotationPoint(14F, 3F, 0F);

		bodyModel[125].addShapeBox(-7.5F, -2.5F, -2.5F, 15, 6, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -5F, 0F, 0F, -5F, 0F, 0F, 0F, 0F); // Box 5
		bodyModel[125].setRotationPoint(14F, 3F, 0F);

		bodyModel[126].addShapeBox(-7.5F, -5.5F, -2.5F, 15, 3, 5, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 7
		bodyModel[126].setRotationPoint(14F, 3F, 0F);


		frontWheelModel = new ModelRendererTurbo[1];
		frontWheelModel[0] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Shape 81

		frontWheelModel[0].addShape3D(8F, -8F, -2F, new Shape2D(new Coord2D[] { new Coord2D(6, 1, 6, 1), new Coord2D(10, 1, 10, 1), new Coord2D(13, 3, 13, 3), new Coord2D(15, 6, 15, 6), new Coord2D(15, 10, 15, 10), new Coord2D(13, 13, 13, 13), new Coord2D(10, 15, 10, 15), new Coord2D(6, 15, 6, 15), new Coord2D(3, 13, 3, 13), new Coord2D(1, 10, 1, 10), new Coord2D(1, 6, 1, 6), new Coord2D(3, 3, 3, 3) }), 4, 15, 15, 48, 4, ModelRendererTurbo.MR_FRONT, new float[] {4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4}); // Shape 81
		frontWheelModel[0].setRotationPoint(14F, 3F, 0F);


		backWheelModel = new ModelRendererTurbo[1];
		backWheelModel[0] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Shape 14

		backWheelModel[0].addShape3D(8F, -8F, -2F, new Shape2D(new Coord2D[] { new Coord2D(6, 1, 6, 1), new Coord2D(10, 1, 10, 1), new Coord2D(13, 3, 13, 3), new Coord2D(15, 6, 15, 6), new Coord2D(15, 10, 15, 10), new Coord2D(13, 13, 13, 13), new Coord2D(10, 15, 10, 15), new Coord2D(6, 15, 6, 15), new Coord2D(3, 13, 3, 13), new Coord2D(1, 10, 1, 10), new Coord2D(1, 6, 1, 6), new Coord2D(3, 3, 3, 3) }), 4, 15, 15, 48, 4, ModelRendererTurbo.MR_FRONT, new float[] {4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,4}); // Shape 14
		backWheelModel[0].setRotationPoint(-20F, 3F, 0F);



		translateAll(0F, 0F, 0F);


		flipAll();
	}
}