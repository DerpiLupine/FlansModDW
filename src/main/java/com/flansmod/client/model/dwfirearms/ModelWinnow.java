package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelWinnow extends ModelGun
{
	int textureX = 512;
	int textureY = 256;

	public ModelWinnow()
	{
		gunModel = new ModelRendererTurbo[43];
		gunModel[0] = new ModelRendererTurbo(this, 0, 44, textureX, textureY); // Import barrelLeft
		gunModel[1] = new ModelRendererTurbo(this, 0, 52, textureX, textureY); // Import barrelMainLeft
		gunModel[2] = new ModelRendererTurbo(this, 0, 60, textureX, textureY); // Import barrelMainMiddle
		gunModel[3] = new ModelRendererTurbo(this, 0, 52, textureX, textureY); // Import barrelMainRight
		gunModel[4] = new ModelRendererTurbo(this, 20, 44, textureX, textureY); // Import barrelMiddle
		gunModel[5] = new ModelRendererTurbo(this, 0, 44, textureX, textureY); // Import barrelRight
		gunModel[6] = new ModelRendererTurbo(this, 0, 0, textureX, textureY); // Import body1
		gunModel[7] = new ModelRendererTurbo(this, 0, 14, textureX, textureY); // Import body2
		gunModel[8] = new ModelRendererTurbo(this, 116, 22, textureX, textureY); // Import body3
		gunModel[9] = new ModelRendererTurbo(this, 116, 22, textureX, textureY); // Import body4
		gunModel[10] = new ModelRendererTurbo(this, 116, 22, textureX, textureY); // Import body5
		gunModel[11] = new ModelRendererTurbo(this, 116, 14, textureX, textureY); // Import body6
		gunModel[12] = new ModelRendererTurbo(this, 74, 12, textureX, textureY); // Import bodyMain
		gunModel[13] = new ModelRendererTurbo(this, 74, 0, textureX, textureY); // Import bodyTop1
		gunModel[14] = new ModelRendererTurbo(this, 74, 24, textureX, textureY); // Import bodyTop2
		gunModel[15] = new ModelRendererTurbo(this, 74, 36, textureX, textureY); // Import bodyTop3
		gunModel[16] = new ModelRendererTurbo(this, 74, 46, textureX, textureY); // Import bodyTop4
		gunModel[17] = new ModelRendererTurbo(this, 74, 46, textureX, textureY); // Import bodyTop5
		gunModel[18] = new ModelRendererTurbo(this, 74, 24, textureX, textureY); // Import bodyTop6
		gunModel[19] = new ModelRendererTurbo(this, 74, 89, textureX, textureY); // Import bodyTopBolt1
		gunModel[20] = new ModelRendererTurbo(this, 74, 89, textureX, textureY); // Import bodyTopBolt2
		gunModel[21] = new ModelRendererTurbo(this, 76, 91, textureX, textureY); // Import boltPart1
		gunModel[22] = new ModelRendererTurbo(this, 76, 91, textureX, textureY); // Import boltPart2
		gunModel[23] = new ModelRendererTurbo(this, 32, 36, textureX, textureY); // Import canisterFarEnd
		gunModel[24] = new ModelRendererTurbo(this, 32, 36, textureX, textureY); // Import canisterFrontEnd
		gunModel[25] = new ModelRendererTurbo(this, 24, 29, textureX, textureY); // Import canisterMiddle
		gunModel[26] = new ModelRendererTurbo(this, 0, 36, textureX, textureY); // Import gasBlockLeft
		gunModel[27] = new ModelRendererTurbo(this, 10, 36, textureX, textureY); // Import gasBlockLeft2
		gunModel[28] = new ModelRendererTurbo(this, 18, 36, textureX, textureY); // Import gasBlockLeft3
		gunModel[29] = new ModelRendererTurbo(this, 0, 36, textureX, textureY); // Import gasBlockMiddle
		gunModel[30] = new ModelRendererTurbo(this, 10, 36, textureX, textureY); // Import gasBlockMiddle2
		gunModel[31] = new ModelRendererTurbo(this, 18, 36, textureX, textureY); // Import gasBlockMiddle3
		gunModel[32] = new ModelRendererTurbo(this, 0, 36, textureX, textureY); // Import gasBlockRight
		gunModel[33] = new ModelRendererTurbo(this, 10, 36, textureX, textureY); // Import gasBlockRight2
		gunModel[34] = new ModelRendererTurbo(this, 18, 36, textureX, textureY); // Import gasBlockRight3
		gunModel[35] = new ModelRendererTurbo(this, 0, 84, textureX, textureY); // Import gripPart1
		gunModel[36] = new ModelRendererTurbo(this, 0, 94, textureX, textureY); // Import gripPart2
		gunModel[37] = new ModelRendererTurbo(this, 28, 68, textureX, textureY); // Import gripPart3
		gunModel[38] = new ModelRendererTurbo(this, 0, 68, textureX, textureY); // Import gripPart4
		gunModel[39] = new ModelRendererTurbo(this, 0, 103, textureX, textureY); // Import gripPart5
		gunModel[40] = new ModelRendererTurbo(this, 86, 50, textureX, textureY); // Import ironSightFixed
		gunModel[41] = new ModelRendererTurbo(this, 74, 50, textureX, textureY); // Import ironSightPart1
		gunModel[42] = new ModelRendererTurbo(this, 74, 50, textureX, textureY); // Import ironSightPart2

		gunModel[0].addShapeBox(0F, 0F, 0F, 8, 6, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.8F, 0F, 0F, -1.8F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.8F, 0F, 0F, -1.8F, 0F); // Import barrelLeft
		gunModel[0].setRotationPoint(36F, -25F, 1F);

		gunModel[1].addShapeBox(0F, 0F, 0F, 16, 6, 2, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1.9F, -0.5F, 0F, -1.9F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1.9F, -0.5F, 0F, -1.9F, -0.5F); // Import barrelMainLeft
		gunModel[1].setRotationPoint(20F, -25F, 1F);

		gunModel[2].addShapeBox(0F, 0F, 0F, 16, 6, 2, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F); // Import barrelMainMiddle
		gunModel[2].setRotationPoint(20F, -25F, -1F);

		gunModel[3].addShapeBox(0F, 0F, 0F, 16, 6, 2, 0F, 0F, -1.9F, -0.5F, 0F, -1.9F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1.9F, -0.5F, 0F, -1.9F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F); // Import barrelMainRight
		gunModel[3].setRotationPoint(20F, -25F, -3F);

		gunModel[4].addBox(0F, 0F, 0F, 8, 6, 2, 0F); // Import barrelMiddle
		gunModel[4].setRotationPoint(36F, -25F, -1F);

		gunModel[5].addShapeBox(0F, 0F, 0F, 8, 6, 2, 0F, 0F, -1.8F, 0F, 0F, -1.8F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.8F, 0F, 0F, -1.8F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import barrelRight
		gunModel[5].setRotationPoint(36F, -25F, -3F);

		gunModel[6].addBox(0F, 0F, 0F, 26, 4, 10, 0F); // Import body1
		gunModel[6].setRotationPoint(-6F, -23F, -5F);

		gunModel[7].addBox(0F, 0F, 0F, 26, 7, 8, 0F); // Import body2
		gunModel[7].setRotationPoint(-6F, -19F, -4F);

		gunModel[8].addShapeBox(0F, 0F, 0F, 16, 2, 2, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import body3
		gunModel[8].setRotationPoint(3F, -16.5F, -4.8F);

		gunModel[9].addShapeBox(0F, 0F, 0F, 16, 2, 2, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -2F, -1F, 0F, -2F, 0F, 0F, 0F, 0F, 0F); // Import body4
		gunModel[9].setRotationPoint(3F, -14.5F, -4.8F);

		gunModel[10].addShapeBox(0F, 0F, 0F, 16, 2, 2, 0F, 0F, -1F, 0F, -2F, -1F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import body5
		gunModel[10].setRotationPoint(3F, -18.5F, -4.8F);

		gunModel[11].addShapeBox(0F, 0F, 0F, 6, 6, 2, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import body6
		gunModel[11].setRotationPoint(-2F, -18.5F, -5F);

		gunModel[12].addBox(0F, 0F, 0F, 12, 4, 8, 0F); // Import bodyMain
		gunModel[12].setRotationPoint(8F, -12F, -4F);

		gunModel[13].addShapeBox(0F, 0F, 0F, 21, 2, 10, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import bodyTop1
		gunModel[13].setRotationPoint(-1F, -25F, -5F);

		gunModel[14].addBox(0F, 0F, 0F, 6, 2, 10, 0F); // Import bodyTop2
		gunModel[14].setRotationPoint(-7F, -25F, -5F);

		gunModel[15].addBox(0F, 0F, 0F, 6, 1, 9, 0F); // Import bodyTop3
		gunModel[15].setRotationPoint(-7F, -26F, -4.5F);

		gunModel[16].addBox(0F, 0F, 0F, 18, 1, 1, 0F); // Import bodyTop4
		gunModel[16].setRotationPoint(0F, -26F, -2F);

		gunModel[17].addBox(0F, 0F, 0F, 18, 1, 1, 0F); // Import bodyTop5
		gunModel[17].setRotationPoint(0F, -26F, 1F);

		gunModel[18].addBox(0F, 0F, 0F, 1, 1, 4, 0F); // Import bodyTop6
		gunModel[18].setRotationPoint(18F, -26F, -2F);

		gunModel[19].addShapeBox(0F, 0F, 0F, 1, 3, 11, 0F, 0F, -0.5F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -0.5F, 0F); // Import bodyTopBolt1
		gunModel[19].setRotationPoint(-3.5F, -24.5F, -5.5F);

		gunModel[20].addShapeBox(0F, 0F, 0F, 1, 3, 11, 0F, 0F, -1F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1F, 0F); // Import bodyTopBolt2
		gunModel[20].setRotationPoint(-4.5F, -24.5F, -5.5F);

		gunModel[21].addShapeBox(0F, 0F, 0F, 1, 3, 9, 0F, 0F, -1F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1F, 0F); // Import boltPart1
		gunModel[21].setRotationPoint(-5F, -13.5F, -4.5F);

		gunModel[22].addShapeBox(0F, 0F, 0F, 1, 3, 9, 0F, 0F, -0.5F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -0.5F, 0F); // Import boltPart2
		gunModel[22].setRotationPoint(-4F, -13.5F, -4.5F);

		gunModel[23].addShapeBox(0F, 0F, 0F, 2, 6, 2, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1.9F, -0.5F, 0F, -1.9F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1.9F, -0.5F, 0F, -1.9F, -0.5F); // Import canisterFarEnd
		gunModel[23].setRotationPoint(-2F, -19.5F, 4F);

		gunModel[24].addShapeBox(0F, 0F, 0F, 2, 6, 2, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1.9F, -0.5F, 0F, -1.9F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1.9F, -0.5F, 0F, -1.9F, -0.5F); // Import canisterFrontEnd
		gunModel[24].setRotationPoint(8F, -19.5F, 4F);

		gunModel[25].addShapeBox(0F, 0F, 0F, 8, 5, 2, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1.5F, -1F, 0F, -1.5F, -1F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1.5F, -1F, 0F, -1.5F, -1F); // Import canisterMiddle
		gunModel[25].setRotationPoint(0F, -19F, 4F);

		gunModel[26].addShapeBox(0F, 0F, 0F, 3, 6, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F); // Import gasBlockLeft
		gunModel[26].setRotationPoint(20F, -19F, 1F);

		gunModel[27].addShapeBox(0F, 0F, 0F, 2, 6, 2, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1.9F, -0.5F, 0F, -1.9F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1.9F, -0.5F, 0F, -1.9F, -0.5F); // Import gasBlockLeft2
		gunModel[27].setRotationPoint(23F, -19F, 1F);

		gunModel[28].addShapeBox(0F, 0F, 0F, 1, 6, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F); // Import gasBlockLeft3
		gunModel[28].setRotationPoint(25F, -19F, 1F);

		gunModel[29].addBox(0F, 0F, 0F, 3, 6, 2, 0F); // Import gasBlockMiddle
		gunModel[29].setRotationPoint(20F, -19F, -1F);

		gunModel[30].addShapeBox(0F, 0F, 0F, 2, 6, 2, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F); // Import gasBlockMiddle2
		gunModel[30].setRotationPoint(23F, -19F, -1F);

		gunModel[31].addBox(0F, 0F, 0F, 1, 6, 2, 0F); // Import gasBlockMiddle3
		gunModel[31].setRotationPoint(25F, -19F, -1F);

		gunModel[32].addShapeBox(0F, 0F, 0F, 3, 6, 2, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import gasBlockRight
		gunModel[32].setRotationPoint(20F, -19F, -3F);

		gunModel[33].addShapeBox(0F, 0F, 0F, 2, 6, 2, 0F, 0F, -1.9F, -0.5F, 0F, -1.9F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1.9F, -0.5F, 0F, -1.9F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F); // Import gasBlockRight2
		gunModel[33].setRotationPoint(23F, -19F, -3F);

		gunModel[34].addShapeBox(0F, 0F, 0F, 1, 6, 2, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import gasBlockRight3
		gunModel[34].setRotationPoint(25F, -19F, -3F);

		gunModel[35].addShapeBox(0F, 0F, 0F, 12, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import gripPart1
		gunModel[35].setRotationPoint(-6F, -12F, -4F);

		gunModel[36].addShapeBox(0F, 0F, 0F, 9, 3, 6, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import gripPart2
		gunModel[36].setRotationPoint(-8F, -10F, -3F);

		gunModel[37].addShapeBox(0F, 0F, 0F, 2, 3, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F); // Import gripPart3
		gunModel[37].setRotationPoint(1F, -10F, -3F);

		gunModel[38].addShapeBox(0F, 0F, 0F, 8, 10, 6, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, -1F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, -1F, 0F); // Import gripPart4
		gunModel[38].setRotationPoint(-9F, -7F, -3F);

		gunModel[39].addBox(0F, 0F, 0F, 6, 1, 5, 0F); // Import gripPart5
		gunModel[39].setRotationPoint(-8.5F, 1.8F, -2.5F);
		gunModel[39].rotateAngleZ = -0.13962634F;

		gunModel[40].addShapeBox(0F, 0F, 0F, 4, 2, 1, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import ironSightFixed
		gunModel[40].setRotationPoint(40F, -27F, -0.5F);

		gunModel[41].addShapeBox(0F, 0F, 0F, 4, 2, 2, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import ironSightPart1
		gunModel[41].setRotationPoint(-6F, -27.5F, -3F);

		gunModel[42].addShapeBox(0F, 0F, 0F, 4, 2, 2, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import ironSightPart2
		gunModel[42].setRotationPoint(-6F, -27.5F, 1F);


		ammoModel = new ModelRendererTurbo[2];
		ammoModel[0] = new ModelRendererTurbo(this, 74, 66, textureX, textureY); // Import ammoPart1
		ammoModel[1] = new ModelRendererTurbo(this, 74, 80, textureX, textureY); // Import ammoPart2

		ammoModel[0].addBox(0F, 0F, 0F, 11, 8, 6, 0F); // Import ammoPart1
		ammoModel[0].setRotationPoint(8.5F, -12F, -3F);

		ammoModel[1].addBox(0F, 0F, 0F, 12, 1, 7, 0F); // Import ammoPart2
		ammoModel[1].setRotationPoint(8F, -4F, -3.5F);


		slideModel = new ModelRendererTurbo[7];
		slideModel[0] = new ModelRendererTurbo(this, 0, 29, textureX, textureY); // Import sliderLeft
		slideModel[1] = new ModelRendererTurbo(this, 0, 29, textureX, textureY); // Import sliderMiddle
		slideModel[2] = new ModelRendererTurbo(this, 74, 54, textureX, textureY); // Import sliderPart1
		slideModel[3] = new ModelRendererTurbo(this, 24, 36, textureX, textureY); // Import sliderPinLeft
		slideModel[4] = new ModelRendererTurbo(this, 24, 36, textureX, textureY); // Import sliderPinMiddle
		slideModel[5] = new ModelRendererTurbo(this, 24, 36, textureX, textureY); // Import sliderPinRight
		slideModel[6] = new ModelRendererTurbo(this, 0, 29, textureX, textureY); // Import sliderRight

		slideModel[0].addShapeBox(0F, 0F, 0F, 10, 5, 2, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1.5F, -1F, 0F, -1.5F, -1F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1.5F, -1F, 0F, -1.5F, -1F); // Import sliderLeft
		slideModel[0].setRotationPoint(-9.5F, -24.5F, 1F);

		slideModel[1].addShapeBox(0F, 0F, 0F, 10, 5, 2, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F); // Import sliderMiddle
		slideModel[1].setRotationPoint(-9.5F, -24.5F, -1F);

		slideModel[2].addShapeBox(0F, 0F, 0F, 5, 2, 2, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import sliderPart1
		slideModel[2].setRotationPoint(12F, -26.5F, -1F);

		slideModel[3].addShapeBox(0F, 0F, 0F, 2, 6, 2, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1.9F, -0.5F, 0F, -1.9F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1.9F, -0.5F, 0F, -1.9F, -0.5F); // Import sliderPinLeft
		slideModel[3].setRotationPoint(-9F, -25F, 1F);

		slideModel[4].addShapeBox(0F, 0F, 0F, 2, 6, 2, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F); // Import sliderPinMiddle
		slideModel[4].setRotationPoint(-9F, -25F, -1F);

		slideModel[5].addShapeBox(0F, 0F, 0F, 2, 6, 2, 0F, 0F, -1.9F, -0.5F, 0F, -1.9F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1.9F, -0.5F, 0F, -1.9F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F); // Import sliderPinRight
		slideModel[5].setRotationPoint(-9F, -25F, -3F);

		slideModel[6].addShapeBox(0F, 0F, 0F, 10, 5, 2, 0F, 0F, -1.5F, -1F, 0F, -1.5F, -1F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1.5F, -1F, 0F, -1.5F, -1F, 0F, -0.5F, 0F, 0F, -0.5F, 0F); // Import sliderRight
		slideModel[6].setRotationPoint(-9.5F, -24.5F, -3F);


		pumpModel = new ModelRendererTurbo[3];
		pumpModel[0] = new ModelRendererTurbo(this, 74, 58, textureX, textureY); // Import pumpBolt1
		pumpModel[1] = new ModelRendererTurbo(this, 74, 58, textureX, textureY); // Import pumpBolt2
		pumpModel[2] = new ModelRendererTurbo(this, 82, 58, textureX, textureY); // Import pumpBolt3

		pumpModel[0].addShapeBox(0F, 0F, 0F, 1, 3, 3, 0F, 0F, -0.5F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -0.5F, 0F); // Import pumpBolt1
		pumpModel[0].setRotationPoint(15.5F, -28.5F, -5.5F);
		pumpModel[0].rotateAngleX = -0.78539816F;

		pumpModel[1].addShapeBox(0F, 0F, 0F, 1, 3, 3, 0F, 0F, -1F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1F, 0F); // Import pumpBolt2
		pumpModel[1].setRotationPoint(14.5F, -28.5F, -5.5F);
		pumpModel[1].rotateAngleX = -0.78539816F;

		pumpModel[2].addBox(0F, 0F, 0F, 1, 1, 2, 0F); // Import pumpBolt3
		pumpModel[2].setRotationPoint(15F, -26F, -4.5F);
		pumpModel[2].rotateAngleX = -0.78539816F;

		barrelAttachPoint = new Vector3f(36 /16F, 22F /16F, 0F /16F);

		gunSlideDistance = 0.5F;
		animationType = EnumAnimationType.CUSTOM;

		/* ----Start of Reload Block---- */
		/* Reload Name: DropMag Pistol Clip */
		rotateGunHorizontal = 60F;
		translateGun = new Vector3f(0.15F, 0.25F, 0F);

		translateClip = new Vector3f(0F, -4F, 0F);
		/* ----End of Reload Block---- */


		translateAll(0F, 0F, 0F);


		flipAll();
	}
}