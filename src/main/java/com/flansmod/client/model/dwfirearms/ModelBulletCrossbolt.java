package com.flansmod.client.model.dwfirearms;

import net.minecraft.entity.Entity;

import com.flansmod.client.model.ModelBullet;
import com.flansmod.client.tmt.ModelRendererTurbo;

public class ModelBulletCrossbolt extends ModelBullet 
{
	public ModelRendererTurbo[] bulletModel;
	
	public ModelBulletCrossbolt()
	{
		int textureX = 64;
		int textureY = 32;
	
		bulletModel = new ModelRendererTurbo[2];
		bulletModel[0] = new ModelRendererTurbo(this, 5, 0, textureX, textureY); // Box 0
		bulletModel[1] = new ModelRendererTurbo(this, 0, 10, textureX, textureY); // Box 0

		bulletModel[0].addShapeBox(0F, 0F, 0F, 1, 7, 1, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -1.5F, -0.5F, 0F, -1.5F, -0.5F, 0F, -1.5F, 0F, -0.5F, -1.5F, 0F); // Box 0
		bulletModel[0].setRotationPoint(-0.5F, -1.5F, -0.5F);

		bulletModel[1].addShapeBox(0F, 0F, 0F, 1, 3, 1, 0F, 0F, -0.2F, 0F, 0F, -0.2F, 0F, 0F, -0.2F, 0F, 0F, -0.2F, 0F, -0.4F, -1.5F, -0.4F, -0.4F, -1.5F, -0.4F, -0.4F, -1.5F, -0.4F, -0.4F, -1.5F, -0.4F); // Box 0
		bulletModel[1].setRotationPoint(-0.25F, 3.75F, -0.25F);

	}
	
	@Override
	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
	{
		//GL11.glScalef(0.5F, 0.5F, 0.5F);
		for(ModelRendererTurbo mrt : bulletModel)
			mrt.render(f5);
	}

	//uses the bullet map texture.
}