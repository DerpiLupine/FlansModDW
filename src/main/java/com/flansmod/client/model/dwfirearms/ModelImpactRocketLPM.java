package com.flansmod.client.model.dwfirearms;

import net.minecraft.client.model.ModelBase;
import net.minecraft.entity.Entity;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelImpactRocketLPM extends ModelBase
{
	public ModelRendererTurbo rocketModel[];

	int textureX = 64;
	int textureY = 32;

	public ModelImpactRocketLPM()
	{
		rocketModel = new ModelRendererTurbo[10];
		rocketModel[0] = new ModelRendererTurbo(this, 2, 2, textureX, textureY); // Box 1
		rocketModel[1] = new ModelRendererTurbo(this, 20, 2, textureX, textureY); // Box 2
		rocketModel[2] = new ModelRendererTurbo(this, 11, 2, textureX, textureY); // Box 3
		rocketModel[3] = new ModelRendererTurbo(this, 2, 9, textureX, textureY); // Box 8
		rocketModel[4] = new ModelRendererTurbo(this, 2, 9, textureX, textureY); // Box 9
		rocketModel[5] = new ModelRendererTurbo(this, 20, 9, textureX, textureY); // Box 3
		rocketModel[6] = new ModelRendererTurbo(this, 2, 9, textureX, textureY); // Box 0
		rocketModel[7] = new ModelRendererTurbo(this, 11, 9, textureX, textureY); // Box 1
		rocketModel[8] = new ModelRendererTurbo(this, 11, 9, textureX, textureY); // Box 2
		rocketModel[9] = new ModelRendererTurbo(this, 11, 9, textureX, textureY); // Box 3

		rocketModel[0].addShapeBox(-1.5F, -1F, -1.5F, 3, 4, 1, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 1
		rocketModel[0].setRotationPoint(0F, 0F, 0F);

		rocketModel[1].addShapeBox(-1.5F, -1F, 0.5F, 3, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 2
		rocketModel[1].setRotationPoint(0F, 0F, 0F);

		rocketModel[2].addBox(-1.5F, -1F, -0.5F, 3, 4, 1, 0F); // Box 3
		rocketModel[2].setRotationPoint(0F, 0F, 0F);

		rocketModel[3].addShapeBox(-1.5F, -4F, -0.5F, 3, 3, 1, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 8
		rocketModel[3].setRotationPoint(0F, 0F, 0F);

		rocketModel[4].addShapeBox(-1.5F, -4F, -1.5F, 3, 3, 1, 0F, -1.2F, 0F, -0.75F, -1.2F, 0F, -0.75F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 9
		rocketModel[4].setRotationPoint(0F, 0F, 0F);

		rocketModel[5].addBox(-0.5F, 3.5F, -0.5F, 1, 4, 1, 0F); // Box 3
		rocketModel[5].setRotationPoint(0F, 0F, 0F);

		rocketModel[6].addShapeBox(-1.5F, -4F, 0.5F, 3, 3, 1, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, -1.2F, 0F, -0.75F, -1.2F, 0F, -0.75F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 0
		rocketModel[6].setRotationPoint(0F, 0F, 0F);

		rocketModel[7].addShapeBox(-1.5F, 3F, -1.5F, 3, 1, 1, 0F, -1.2F, 0F, -0.75F, -1.2F, 0F, -0.75F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 1
		rocketModel[7].setRotationPoint(0F, 0F, 0F);

		rocketModel[8].addShapeBox(-1.5F, 3F, 0.5F, 3, 1, 1, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, -1.2F, 0F, -0.75F, -1.2F, 0F, -0.75F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 2
		rocketModel[8].setRotationPoint(0F, 0F, 0F);

		rocketModel[9].addShapeBox(-1.5F, 3F, -0.5F, 3, 1, 1, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 3
		rocketModel[9].setRotationPoint(0F, 0F, 0F);

		for(int i = 0; i < 10; i++)
			rocketModel[i].rotateAngleZ = 3.14159265F;
	}

	@Override
	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
	{
		for(int i = 0; i < 10; i++)
		{
			rocketModel[i].render(f5);
		}
	}

	public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5)
	{
	}
	
}