package com.flansmod.client.model.dwfirearms; //Path where the model is located

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelThunderStriker extends ModelGun //Same as Filename
{
	int textureX = 512;
	int textureY = 256;

	public ModelThunderStriker() //Same as Filename
	{
		gunModel = new ModelRendererTurbo[96];
		gunModel[0] = new ModelRendererTurbo(this, 80, 40, textureX, textureY); // gun
		gunModel[1] = new ModelRendererTurbo(this, 81, 15, textureX, textureY); // gun
		gunModel[2] = new ModelRendererTurbo(this, 147, 198, textureX, textureY); // gun
		gunModel[3] = new ModelRendererTurbo(this, 213, 43, textureX, textureY); // gun
		gunModel[4] = new ModelRendererTurbo(this, 375, 12, textureX, textureY); // gun
		gunModel[5] = new ModelRendererTurbo(this, 375, 1, textureX, textureY); // gun
		gunModel[6] = new ModelRendererTurbo(this, 375, 12, textureX, textureY); // gun
		gunModel[7] = new ModelRendererTurbo(this, 317, 24, textureX, textureY); // gun
		gunModel[8] = new ModelRendererTurbo(this, 317, 34, textureX, textureY); // gun
		gunModel[9] = new ModelRendererTurbo(this, 317, 24, textureX, textureY); // gun
		gunModel[10] = new ModelRendererTurbo(this, 55, 6, textureX, textureY); // gun
		gunModel[11] = new ModelRendererTurbo(this, 240, 2, textureX, textureY); // gun
		gunModel[12] = new ModelRendererTurbo(this, 240, 11, textureX, textureY); // gun
		gunModel[13] = new ModelRendererTurbo(this, 240, 2, textureX, textureY); // gun
		gunModel[14] = new ModelRendererTurbo(this, 358, 12, textureX, textureY); // gun
		gunModel[15] = new ModelRendererTurbo(this, 358, 1, textureX, textureY); // gun
		gunModel[16] = new ModelRendererTurbo(this, 240, 33, textureX, textureY); // gun
		gunModel[17] = new ModelRendererTurbo(this, 240, 33, textureX, textureY); // gun
		gunModel[18] = new ModelRendererTurbo(this, 240, 24, textureX, textureY); // gun
		gunModel[19] = new ModelRendererTurbo(this, 134, 76, textureX, textureY); // gun
		gunModel[20] = new ModelRendererTurbo(this, 80, 1, textureX, textureY); // gun
		gunModel[21] = new ModelRendererTurbo(this, 80, 84, textureX, textureY); // gun
		gunModel[22] = new ModelRendererTurbo(this, 80, 99, textureX, textureY); // gun
		gunModel[23] = new ModelRendererTurbo(this, 80, 111, textureX, textureY); // gun
		gunModel[24] = new ModelRendererTurbo(this, 147, 184, textureX, textureY); // gun
		gunModel[25] = new ModelRendererTurbo(this, 172, 215, textureX, textureY); // gun
		gunModel[26] = new ModelRendererTurbo(this, 147, 212, textureX, textureY); // gun
		gunModel[27] = new ModelRendererTurbo(this, 197, 212, textureX, textureY); // gun
		gunModel[28] = new ModelRendererTurbo(this, 216, 225, textureX, textureY); // gun
		gunModel[29] = new ModelRendererTurbo(this, 197, 219, textureX, textureY); // gun
		gunModel[30] = new ModelRendererTurbo(this, 197, 231, textureX, textureY); // gun
		gunModel[31] = new ModelRendererTurbo(this, 204, 231, textureX, textureY); // gun
		gunModel[32] = new ModelRendererTurbo(this, 204, 231, textureX, textureY); // gun
		gunModel[33] = new ModelRendererTurbo(this, 180, 101, textureX, textureY); // gun
		gunModel[34] = new ModelRendererTurbo(this, 80, 57, textureX, textureY); // gun
		gunModel[35] = new ModelRendererTurbo(this, 80, 74, textureX, textureY); // gun
		gunModel[36] = new ModelRendererTurbo(this, 180, 101, textureX, textureY); // gun
		gunModel[37] = new ModelRendererTurbo(this, 180, 101, textureX, textureY); // gun
		gunModel[38] = new ModelRendererTurbo(this, 180, 57, textureX, textureY); // gun
		gunModel[39] = new ModelRendererTurbo(this, 180, 43, textureX, textureY); // gun
		gunModel[40] = new ModelRendererTurbo(this, 180, 69, textureX, textureY); // gun
		gunModel[41] = new ModelRendererTurbo(this, 206, 88, textureX, textureY); // gun
		gunModel[42] = new ModelRendererTurbo(this, 211, 104, textureX, textureY); // gun
		gunModel[43] = new ModelRendererTurbo(this, 209, 70, textureX, textureY); // gun
		gunModel[44] = new ModelRendererTurbo(this, 180, 88, textureX, textureY); // gun
		gunModel[45] = new ModelRendererTurbo(this, 197, 231, textureX, textureY); // gun
		gunModel[46] = new ModelRendererTurbo(this, 197, 225, textureX, textureY); // gun
		gunModel[47] = new ModelRendererTurbo(this, 58, 28, textureX, textureY); // gun
		gunModel[48] = new ModelRendererTurbo(this, 1, 16, textureX, textureY); // gun
		gunModel[49] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // gun
		gunModel[50] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // gun
		gunModel[51] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // gun
		gunModel[52] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // gun
		gunModel[53] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // gun
		gunModel[54] = new ModelRendererTurbo(this, 58, 28, textureX, textureY); // gun
		gunModel[55] = new ModelRendererTurbo(this, 58, 28, textureX, textureY); // gun
		gunModel[56] = new ModelRendererTurbo(this, 58, 28, textureX, textureY); // gun
		gunModel[57] = new ModelRendererTurbo(this, 1, 28, textureX, textureY); // gun
		gunModel[58] = new ModelRendererTurbo(this, 58, 28, textureX, textureY); // gun
		gunModel[59] = new ModelRendererTurbo(this, 58, 28, textureX, textureY); // gun
		gunModel[60] = new ModelRendererTurbo(this, 58, 28, textureX, textureY); // gun
		gunModel[61] = new ModelRendererTurbo(this, 58, 28, textureX, textureY); // gun
		gunModel[62] = new ModelRendererTurbo(this, 22, 1, textureX, textureY); // gun
		gunModel[63] = new ModelRendererTurbo(this, 22, 1, textureX, textureY); // gun
		gunModel[64] = new ModelRendererTurbo(this, 22, 1, textureX, textureY); // gun
		gunModel[65] = new ModelRendererTurbo(this, 22, 1, textureX, textureY); // gun
		gunModel[66] = new ModelRendererTurbo(this, 358, 12, textureX, textureY); // gun
		gunModel[67] = new ModelRendererTurbo(this, 338, 24, textureX, textureY); // gun
		gunModel[68] = new ModelRendererTurbo(this, 338, 34, textureX, textureY); // gun
		gunModel[69] = new ModelRendererTurbo(this, 338, 24, textureX, textureY); // gun
		gunModel[70] = new ModelRendererTurbo(this, 355, 25, textureX, textureY); // gun
		gunModel[71] = new ModelRendererTurbo(this, 236, 225, textureX, textureY); // gun
		gunModel[72] = new ModelRendererTurbo(this, 236, 212, textureX, textureY); // gun
		gunModel[73] = new ModelRendererTurbo(this, 243, 230, textureX, textureY); // gun
		gunModel[74] = new ModelRendererTurbo(this, 255, 230, textureX, textureY); // gun
		gunModel[75] = new ModelRendererTurbo(this, 250, 230, textureX, textureY); // gun
		gunModel[76] = new ModelRendererTurbo(this, 236, 230, textureX, textureY); // gun
		gunModel[77] = new ModelRendererTurbo(this, 236, 219, textureX, textureY); // gun
		gunModel[78] = new ModelRendererTurbo(this, 136, 51, textureX, textureY); // gun
		gunModel[79] = new ModelRendererTurbo(this, 1, 215, textureX, textureY); // gun
		gunModel[80] = new ModelRendererTurbo(this, 168, 25, textureX, textureY); // gun
		gunModel[81] = new ModelRendererTurbo(this, 194, 19, textureX, textureY); // gun
		gunModel[82] = new ModelRendererTurbo(this, 54, 32, textureX, textureY); // gun
		gunModel[83] = new ModelRendererTurbo(this, 54, 29, textureX, textureY); // gun
		gunModel[84] = new ModelRendererTurbo(this, 54, 29, textureX, textureY); // gun
		gunModel[85] = new ModelRendererTurbo(this, 54, 29, textureX, textureY); // gun
		gunModel[86] = new ModelRendererTurbo(this, 1, 44, textureX, textureY); // gun
		gunModel[87] = new ModelRendererTurbo(this, 1, 66, textureX, textureY); // gun
		gunModel[88] = new ModelRendererTurbo(this, 2, 77, textureX, textureY); // gun
		gunModel[89] = new ModelRendererTurbo(this, 1, 92, textureX, textureY); // gun
		gunModel[90] = new ModelRendererTurbo(this, 1, 109, textureX, textureY); // gun
		gunModel[91] = new ModelRendererTurbo(this, 38, 66, textureX, textureY); // gun
		gunModel[92] = new ModelRendererTurbo(this, 39, 77, textureX, textureY); // gun
		gunModel[93] = new ModelRendererTurbo(this, 38, 92, textureX, textureY); // gun
		gunModel[94] = new ModelRendererTurbo(this, 36, 109, textureX, textureY); // gun
		gunModel[95] = new ModelRendererTurbo(this, 1, 56, textureX, textureY); // gun

		gunModel[0].addBox(0F, 0F, 0F, 24, 8, 9, 0F); // gun
		gunModel[0].setRotationPoint(-11F, -12F, -4.5F);

		gunModel[1].addShapeBox(0F, 0F, 0F, 48, 1, 8, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // gun
		gunModel[1].setRotationPoint(-11F, -20F, -4F);

		gunModel[2].addBox(0F, 0F, 0F, 20, 4, 9, 0F); // gun
		gunModel[2].setRotationPoint(13F, -12F, -4.5F);

		gunModel[3].addBox(0F, 0F, 0F, 4, 6, 9, 0F); // gun
		gunModel[3].setRotationPoint(33F, -12F, -4.5F);

		gunModel[4].addShapeBox(0F, 0F, 0F, 4, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // gun
		gunModel[4].setRotationPoint(59F, -19F, -3.5F);

		gunModel[5].addBox(0F, 0F, 0F, 4, 3, 7, 0F); // gun
		gunModel[5].setRotationPoint(59F, -17F, -3.5F);

		gunModel[6].addShapeBox(0F, 0F, 0F, 4, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // gun
		gunModel[6].setRotationPoint(59F, -14F, -3.5F);

		gunModel[7].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // gun
		gunModel[7].setRotationPoint(61F, -11.5F, -3.5F);

		gunModel[8].addBox(0F, 0F, 0F, 3, 3, 7, 0F); // gun
		gunModel[8].setRotationPoint(61F, -9.5F, -3.5F);

		gunModel[9].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // gun
		gunModel[9].setRotationPoint(61F, -6.5F, -3.5F);

		gunModel[10].addBox(0F, 0F, 0F, 6, 1, 5, 0F); // gun
		gunModel[10].setRotationPoint(34F, -21F, -2.5F);

		gunModel[11].addShapeBox(0F, 0F, 0F, 30, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // gun
		gunModel[11].setRotationPoint(63F, -14.5F, -3F);

		gunModel[12].addBox(0F, 0F, 0F, 30, 2, 6, 0F); // gun
		gunModel[12].setRotationPoint(63F, -16.5F, -3F);

		gunModel[13].addShapeBox(0F, 0F, 0F, 30, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // gun
		gunModel[13].setRotationPoint(63F, -18.5F, -3F);

		gunModel[14].addShapeBox(0F, 0F, 0F, 1, 2, 7, 0F, 0F, -0.5F, -2.5F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, -0.5F, -2.5F, 0F, 0.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, -0.5F); // gun
		gunModel[14].setRotationPoint(91F, -19F, -3.5F);

		gunModel[15].addShapeBox(0F, 0F, 0F, 1, 3, 7, 0F, 0F, -0.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -0.5F); // gun
		gunModel[15].setRotationPoint(91F, -17F, -3.5F);

		gunModel[16].addShapeBox(0F, 0F, 0F, 2, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // gun
		gunModel[16].setRotationPoint(59F, -7F, -3F);

		gunModel[17].addBox(0F, 0F, 0F, 2, 2, 6, 0F); // gun
		gunModel[17].setRotationPoint(59F, -9F, -3F);

		gunModel[18].addShapeBox(0F, 0F, 0F, 2, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // gun
		gunModel[18].setRotationPoint(59F, -11F, -3F);

		gunModel[19].addShapeBox(0F, 0F, 0F, 11, 5, 11, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // gun
		gunModel[19].setRotationPoint(-11F, -19F, -5.5F);

		gunModel[20].addShapeBox(0F, 0F, 0F, 48, 2, 11, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // gun
		gunModel[20].setRotationPoint(-11F, -14F, -5.5F);

		gunModel[21].addBox(0F, 0F, 0F, 22, 5, 9, 0F); // gun
		gunModel[21].setRotationPoint(37F, -11F, -4.5F);

		gunModel[22].addShapeBox(0F, 0F, 0F, 26, 2, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // gun
		gunModel[22].setRotationPoint(33F, -6F, -4.5F);

		gunModel[23].addShapeBox(0F, 0F, 0F, 22, 2, 9, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // gun
		gunModel[23].setRotationPoint(37F, -13F, -4.5F);

		gunModel[24].addShapeBox(0F, 0F, 0F, 20, 2, 10, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // gun
		gunModel[24].setRotationPoint(13F, -8F, -5F);

		gunModel[25].addShapeBox(0F, 0F, 0F, 1, 13, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F); // gun
		gunModel[25].setRotationPoint(13F, -6F, -5F);

		gunModel[26].addBox(0F, 0F, 0F, 1, 16, 10, 0F); // gun
		gunModel[26].setRotationPoint(32F, -6F, -5F);

		gunModel[27].addBox(0F, 0F, 0F, 18, 5, 1, 0F); // gun
		gunModel[27].setRotationPoint(14F, 2F, -5F);

		gunModel[28].addShapeBox(0F, 0F, 0F, 4, 3, 1, 0F, 5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // gun
		gunModel[28].setRotationPoint(28F, 7F, -5F);

		gunModel[29].addBox(0F, 0F, 0F, 18, 4, 1, 0F); // gun
		gunModel[29].setRotationPoint(14F, -6F, -5F);

		gunModel[30].addShapeBox(0F, 0F, 0F, 2, 4, 1, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // gun
		gunModel[30].setRotationPoint(14F, -2F, -5F);

		gunModel[31].addShapeBox(0F, 0F, 0F, 1, 4, 1, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F); // gun
		gunModel[31].setRotationPoint(18F, -2F, -5F);

		gunModel[32].addShapeBox(0F, 0F, 0F, 1, 4, 1, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F); // gun
		gunModel[32].setRotationPoint(21F, -2F, -5F);

		gunModel[33].addShapeBox(0F, 0F, 0F, 4, 2, 11, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // gun
		gunModel[33].setRotationPoint(37.2F, -14F, -5.5F);

		gunModel[34].addShapeBox(0F, 0F, 0F, 22, 5, 11, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // gun
		gunModel[34].setRotationPoint(37.2F, -19F, -5.5F);

		gunModel[35].addShapeBox(0F, 0F, 0F, 22, 1, 8, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // gun
		gunModel[35].setRotationPoint(37.2F, -20F, -4F);

		gunModel[36].addShapeBox(0F, 0F, 0F, 4, 2, 11, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // gun
		gunModel[36].setRotationPoint(55.2F, -14F, -5.5F);

		gunModel[37].addShapeBox(0F, 0F, 0F, 4, 2, 11, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // gun
		gunModel[37].setRotationPoint(46.2F, -14F, -5.5F);

		gunModel[38].addShapeBox(0F, 0F, 0F, 8, 2, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F); // gun
		gunModel[38].setRotationPoint(-11F, -4F, -4.5F);

		gunModel[39].addBox(0F, 0F, 0F, 7, 4, 9, 0F); // gun
		gunModel[39].setRotationPoint(-3F, -4F, -4.5F);

		gunModel[40].addShapeBox(0F, 0F, 0F, 2, 6, 12, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // gun
		gunModel[40].setRotationPoint(-13F, -19.5F, -6F);

		gunModel[41].addShapeBox(0F, 0F, 0F, 2, 2, 12, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // gun
		gunModel[41].setRotationPoint(-13F, -13.5F, -6F);

		gunModel[42].addShapeBox(0F, 0F, 0F, 2, 1, 9, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // gun
		gunModel[42].setRotationPoint(-13F, -20.5F, -4.5F);

		gunModel[43].addBox(0F, 0F, 0F, 2, 7, 10, 0F); // gun
		gunModel[43].setRotationPoint(-13F, -11.5F, -5F);

		gunModel[44].addShapeBox(0F, 0F, 0F, 2, 2, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -1F, 0F, -0.5F, -1F, 0F, -0.5F, -1F, 0F, -0.5F, -1F); // gun
		gunModel[44].setRotationPoint(-13F, -4.5F, -5F);

		gunModel[45].addShapeBox(0F, 0F, 0F, 2, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F); // gun
		gunModel[45].setRotationPoint(24F, -2F, -5F);

		gunModel[46].addBox(0F, 0F, 0F, 6, 4, 1, 0F); // gun
		gunModel[46].setRotationPoint(26F, -2F, -5F);

		gunModel[47].addShapeBox(0F, 0F, 0F, 2, 1, 8, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // gun
		gunModel[47].setRotationPoint(30F, -20.5F, -4F);

		gunModel[48].addBox(0F, 0F, 0F, 27, 3, 7, 0F); // gun
		gunModel[48].setRotationPoint(7F, -21.5F, -3.5F);

		gunModel[49].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // gun
		gunModel[49].setRotationPoint(7F, -23.5F, -3.5F);

		gunModel[50].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // gun
		gunModel[50].setRotationPoint(13F, -23.5F, -3.5F);

		gunModel[51].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // gun
		gunModel[51].setRotationPoint(19F, -23.5F, -3.5F);

		gunModel[52].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // gun
		gunModel[52].setRotationPoint(25F, -23.5F, -3.5F);

		gunModel[53].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // gun
		gunModel[53].setRotationPoint(31F, -23.5F, -3.5F);

		gunModel[54].addShapeBox(0F, 0F, 0F, 2, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // gun
		gunModel[54].setRotationPoint(30F, -19.5F, -4F);

		gunModel[55].addShapeBox(0F, 0F, 0F, 2, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // gun
		gunModel[55].setRotationPoint(9F, -19.5F, -4F);

		gunModel[56].addShapeBox(0F, 0F, 0F, 2, 1, 8, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // gun
		gunModel[56].setRotationPoint(9F, -20.5F, -4F);

		gunModel[57].addBox(0F, 0F, 0F, 21, 2, 7, 0F); // gun
		gunModel[57].setRotationPoint(37F, -5F, -3.5F);

		gunModel[58].addShapeBox(0F, 0F, 0F, 2, 1, 8, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // gun
		gunModel[58].setRotationPoint(39F, -6F, -4F);

		gunModel[59].addShapeBox(0F, 0F, 0F, 2, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // gun
		gunModel[59].setRotationPoint(39F, -5F, -4F);

		gunModel[60].addShapeBox(0F, 0F, 0F, 2, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // gun
		gunModel[60].setRotationPoint(54F, -5F, -4F);

		gunModel[61].addShapeBox(0F, 0F, 0F, 2, 1, 8, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // gun
		gunModel[61].setRotationPoint(54F, -6F, -4F);

		gunModel[62].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // gun
		gunModel[62].setRotationPoint(55F, -3F, -3.5F);

		gunModel[63].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // gun
		gunModel[63].setRotationPoint(49F, -3F, -3.5F);

		gunModel[64].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // gun
		gunModel[64].setRotationPoint(43F, -3F, -3.5F);

		gunModel[65].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // gun
		gunModel[65].setRotationPoint(37F, -3F, -3.5F);

		gunModel[66].addShapeBox(0F, 0F, 0F, 1, 2, 7, 0F, 0F, 0.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, -0.5F, 0F, -0.5F, -2.5F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, -0.5F, -2.5F); // gun
		gunModel[66].setRotationPoint(91F, -14F, -3.5F);

		gunModel[67].addShapeBox(0F, 0F, 0F, 1, 2, 7, 0F, 0F, 0F, -2F, 0F, -0.5F, -2.5F, 0F, -0.5F, -2.5F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0.5F, -0.5F, 0F, 0.5F, -0.5F, 0F, 0F, 0F); // gun
		gunModel[67].setRotationPoint(64F, -11.5F, -3.5F);

		gunModel[68].addShapeBox(0F, 0F, 0F, 1, 3, 7, 0F, 0F, 0F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, 0F); // gun
		gunModel[68].setRotationPoint(64F, -9.5F, -3.5F);

		gunModel[69].addShapeBox(0F, 0F, 0F, 1, 2, 7, 0F, 0F, 0F, 0F, 0F, 0.5F, -0.5F, 0F, 0.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, -0.5F, -2.5F, 0F, -0.5F, -2.5F, 0F, 0F, -2F); // gun
		gunModel[69].setRotationPoint(64F, -6.5F, -3.5F);

		gunModel[70].addShapeBox(0F, 0F, 0F, 4, 5, 3, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // gun
		gunModel[70].setRotationPoint(59F, -24F, -1.5F);

		gunModel[71].addShapeBox(0F, 0F, 0F, 4, 3, 1, 0F, 5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // gun
		gunModel[71].setRotationPoint(28F, 7F, 4F);

		gunModel[72].addBox(0F, 0F, 0F, 18, 5, 1, 0F); // gun
		gunModel[72].setRotationPoint(14F, 2F, 4F);

		gunModel[73].addShapeBox(0F, 0F, 0F, 2, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F); // gun
		gunModel[73].setRotationPoint(24F, -2F, 4F);

		gunModel[74].addShapeBox(0F, 0F, 0F, 1, 4, 1, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F); // gun
		gunModel[74].setRotationPoint(21F, -2F, 4F);

		gunModel[75].addShapeBox(0F, 0F, 0F, 1, 4, 1, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F); // gun
		gunModel[75].setRotationPoint(18F, -2F, 4F);

		gunModel[76].addShapeBox(0F, 0F, 0F, 2, 4, 1, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // gun
		gunModel[76].setRotationPoint(14F, -2F, 4F);

		gunModel[77].addBox(0F, 0F, 0F, 18, 4, 1, 0F); // gun
		gunModel[77].setRotationPoint(14F, -6F, 4F);

		gunModel[78].addShapeBox(0F, 0F, 0F, 8, 5, 11, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // gun
		gunModel[78].setRotationPoint(29F, -19F, -5.5F);

		gunModel[79].addShapeBox(0F, 0F, 0F, 58, 10, 9, 0F, 0F, 0F, 0F, -29F, 0F, 0F, -29F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, -5F, 0F, -29F, -5F, 0F, -29F, -5F, 0F, 0F, -5F, 0F); // gun
		gunModel[79].setRotationPoint(0F, -19F, -3.5F);

		gunModel[80].addShapeBox(0F, 0F, 0F, 29, 2, 1, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // gun
		gunModel[80].setRotationPoint(0F, -19F, -4.5F);

		gunModel[81].addShapeBox(0F, 0F, 0F, 15, 3, 2, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // gun
		gunModel[81].setRotationPoint(0F, -17F, -5.25F);

		gunModel[82].addBox(0F, 0F, 0F, 1, 1, 1, 0F); // gun
		gunModel[82].setRotationPoint(1F, -9F, 4F);

		gunModel[83].addBox(0F, 0F, 0F, 3, 1, 1, 0F); // gun
		gunModel[83].setRotationPoint(0F, -7F, 4F);

		gunModel[84].addShapeBox(0F, 0F, 0F, 3, 1, 1, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // gun
		gunModel[84].setRotationPoint(0F, -8F, 4F);

		gunModel[85].addShapeBox(0F, 0F, 0F, 3, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // gun
		gunModel[85].setRotationPoint(0F, -6F, 4F);

		gunModel[86].addShapeBox(0F, 0F, 0F, 12, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F); // gun
		gunModel[86].setRotationPoint(-11F, -3F, -4F);

		gunModel[87].addShapeBox(0F, 0F, 0F, 10, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // gun
		gunModel[87].setRotationPoint(-8F, 0F, -4F);

		gunModel[88].addShapeBox(0F, 0F, 0F, 10, 6, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 3F, 0F, 0F); // gun
		gunModel[88].setRotationPoint(-8F, 2F, -4F);

		gunModel[89].addShapeBox(0F, 0F, 0F, 10, 8, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, -3F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 3F, -3F, 0F); // gun
		gunModel[89].setRotationPoint(-11F, 8F, -4F);

		gunModel[90].addShapeBox(0F, 0F, 0F, 9, 2, 8, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F); // gun
		gunModel[90].setRotationPoint(-14F, 16F, -4F);

		gunModel[91].addShapeBox(0F, 0F, 0F, 2, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F); // gun
		gunModel[91].setRotationPoint(2F, 0F, -4F);

		gunModel[92].addShapeBox(0F, 0F, 0F, 2, 6, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, -1.5F, -3F, 0F, -1.5F, 3F, 0F, 0F); // gun
		gunModel[92].setRotationPoint(2F, 2F, -4F);

		gunModel[93].addShapeBox(0F, 0F, 0F, 2, 8, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 4F, 0F, 0F, -4F, 0F, -1.5F, -4F, 0F, -1.5F, 4F, 0F, 0F); // gun
		gunModel[93].setRotationPoint(-1F, 8F, -4F);

		gunModel[94].addShapeBox(0F, 0F, 0F, 2, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F); // gun
		gunModel[94].setRotationPoint(-5F, 16F, -4F);

		gunModel[95].addBox(0F, 0F, 0F, 12, 1, 8, 0F); // gun
		gunModel[95].setRotationPoint(-11F, -4F, -4F);


		defaultBarrelModel = new ModelRendererTurbo[3];
		defaultBarrelModel[0] = new ModelRendererTurbo(this, 323, 12, textureX, textureY); // gun
		defaultBarrelModel[1] = new ModelRendererTurbo(this, 323, 12, textureX, textureY); // gun
		defaultBarrelModel[2] = new ModelRendererTurbo(this, 323, 1, textureX, textureY); // gun

		defaultBarrelModel[0].addShapeBox(0F, 0F, 0F, 10, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // gun
		defaultBarrelModel[0].setRotationPoint(92F, -14F, -3.5F);

		defaultBarrelModel[1].addShapeBox(0F, 0F, 0F, 10, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // gun
		defaultBarrelModel[1].setRotationPoint(92F, -19F, -3.5F);

		defaultBarrelModel[2].addBox(0F, 0F, 0F, 10, 3, 7, 0F); // gun
		defaultBarrelModel[2].setRotationPoint(92F, -17F, -3.5F);


		defaultScopeModel = new ModelRendererTurbo[47];
		defaultScopeModel[0] = new ModelRendererTurbo(this, 326, 105, textureX, textureY); // scope
		defaultScopeModel[1] = new ModelRendererTurbo(this, 283, 103, textureX, textureY); // scope
		defaultScopeModel[2] = new ModelRendererTurbo(this, 240, 101, textureX, textureY); // scope
		defaultScopeModel[3] = new ModelRendererTurbo(this, 283, 103, textureX, textureY); // scope
		defaultScopeModel[4] = new ModelRendererTurbo(this, 326, 105, textureX, textureY); // scope
		defaultScopeModel[5] = new ModelRendererTurbo(this, 283, 120, textureX, textureY); // scope
		defaultScopeModel[6] = new ModelRendererTurbo(this, 326, 122, textureX, textureY); // scope
		defaultScopeModel[7] = new ModelRendererTurbo(this, 283, 120, textureX, textureY); // scope
		defaultScopeModel[8] = new ModelRendererTurbo(this, 240, 118, textureX, textureY); // scope
		defaultScopeModel[9] = new ModelRendererTurbo(this, 326, 122, textureX, textureY); // scope
		defaultScopeModel[10] = new ModelRendererTurbo(this, 293, 72, textureX, textureY); // scope
		defaultScopeModel[11] = new ModelRendererTurbo(this, 240, 71, textureX, textureY); // scope
		defaultScopeModel[12] = new ModelRendererTurbo(this, 293, 72, textureX, textureY); // scope
		defaultScopeModel[13] = new ModelRendererTurbo(this, 240, 87, textureX, textureY); // scope
		defaultScopeModel[14] = new ModelRendererTurbo(this, 240, 87, textureX, textureY); // scope
		defaultScopeModel[15] = new ModelRendererTurbo(this, 240, 87, textureX, textureY); // scope
		defaultScopeModel[16] = new ModelRendererTurbo(this, 297, 91, textureX, textureY); // scope
		defaultScopeModel[17] = new ModelRendererTurbo(this, 297, 91, textureX, textureY); // scope
		defaultScopeModel[18] = new ModelRendererTurbo(this, 297, 91, textureX, textureY); // scope
		defaultScopeModel[19] = new ModelRendererTurbo(this, 286, 91, textureX, textureY); // scope
		defaultScopeModel[20] = new ModelRendererTurbo(this, 286, 91, textureX, textureY); // scope
		defaultScopeModel[21] = new ModelRendererTurbo(this, 286, 91, textureX, textureY); // scope
		defaultScopeModel[22] = new ModelRendererTurbo(this, 263, 87, textureX, textureY); // scope
		defaultScopeModel[23] = new ModelRendererTurbo(this, 263, 87, textureX, textureY); // scope
		defaultScopeModel[24] = new ModelRendererTurbo(this, 263, 87, textureX, textureY); // scope
		defaultScopeModel[25] = new ModelRendererTurbo(this, 240, 87, textureX, textureY); // scope
		defaultScopeModel[26] = new ModelRendererTurbo(this, 240, 87, textureX, textureY); // scope
		defaultScopeModel[27] = new ModelRendererTurbo(this, 240, 87, textureX, textureY); // scope
		defaultScopeModel[28] = new ModelRendererTurbo(this, 240, 48, textureX, textureY); // scope
		defaultScopeModel[29] = new ModelRendererTurbo(this, 301, 57, textureX, textureY); // scope
		defaultScopeModel[30] = new ModelRendererTurbo(this, 301, 57, textureX, textureY); // scope
		defaultScopeModel[31] = new ModelRendererTurbo(this, 362, 62, textureX, textureY); // scope
		defaultScopeModel[32] = new ModelRendererTurbo(this, 362, 77, textureX, textureY); // scope
		defaultScopeModel[33] = new ModelRendererTurbo(this, 362, 82, textureX, textureY); // scope
		defaultScopeModel[34] = new ModelRendererTurbo(this, 362, 72, textureX, textureY); // scope
		defaultScopeModel[35] = new ModelRendererTurbo(this, 297, 145, textureX, textureY); // scope
		defaultScopeModel[36] = new ModelRendererTurbo(this, 297, 135, textureX, textureY); // scope
		defaultScopeModel[37] = new ModelRendererTurbo(this, 385, 73, textureX, textureY); // scope
		defaultScopeModel[38] = new ModelRendererTurbo(this, 385, 73, textureX, textureY); // scope
		defaultScopeModel[39] = new ModelRendererTurbo(this, 240, 151, textureX, textureY); // scope
		defaultScopeModel[40] = new ModelRendererTurbo(this, 240, 165, textureX, textureY); // scope
		defaultScopeModel[41] = new ModelRendererTurbo(this, 362, 55, textureX, textureY); // scope
		defaultScopeModel[42] = new ModelRendererTurbo(this, 362, 48, textureX, textureY); // scope
		defaultScopeModel[43] = new ModelRendererTurbo(this, 240, 135, textureX, textureY); // scope
		defaultScopeModel[44] = new ModelRendererTurbo(this, 340, 145, textureX, textureY); // scope
		defaultScopeModel[45] = new ModelRendererTurbo(this, 340, 135, textureX, textureY); // scope
		defaultScopeModel[46] = new ModelRendererTurbo(this, 314, 87, textureX, textureY); // Box 199

		defaultScopeModel[0].addShapeBox(0F, 0F, 0F, 8, 2, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // scope
		defaultScopeModel[0].setRotationPoint(33F, -27.5F, -5F);

		defaultScopeModel[1].addShapeBox(0F, 0F, 0F, 8, 2, 12, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // scope
		defaultScopeModel[1].setRotationPoint(33F, -29.5F, -6F);

		defaultScopeModel[2].addBox(0F, 0F, 0F, 8, 4, 12, 0F); // scope
		defaultScopeModel[2].setRotationPoint(33F, -33.5F, -6F);

		defaultScopeModel[3].addShapeBox(0F, 0F, 0F, 8, 2, 12, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope
		defaultScopeModel[3].setRotationPoint(33F, -35.5F, -6F);

		defaultScopeModel[4].addShapeBox(0F, 0F, 0F, 8, 2, 10, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope
		defaultScopeModel[4].setRotationPoint(33F, -37.5F, -5F);

		defaultScopeModel[5].addShapeBox(0F, 0F, 0F, 9, 2, 12, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope
		defaultScopeModel[5].setRotationPoint(0F, -35.5F, -6F);

		defaultScopeModel[6].addShapeBox(0F, 0F, 0F, 9, 2, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // scope
		defaultScopeModel[6].setRotationPoint(0F, -27.5F, -5F);

		defaultScopeModel[7].addShapeBox(0F, 0F, 0F, 9, 2, 12, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // scope
		defaultScopeModel[7].setRotationPoint(0F, -29.5F, -6F);

		defaultScopeModel[8].addBox(0F, 0F, 0F, 9, 4, 12, 0F); // scope
		defaultScopeModel[8].setRotationPoint(0F, -33.5F, -6F);

		defaultScopeModel[9].addShapeBox(0F, 0F, 0F, 9, 2, 10, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope
		defaultScopeModel[9].setRotationPoint(0F, -37.5F, -5F);

		defaultScopeModel[10].addShapeBox(0F, 0F, 0F, 15, 3, 11, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope
		defaultScopeModel[10].setRotationPoint(28F, -36.5F, -5.5F);

		defaultScopeModel[11].addBox(0F, 0F, 0F, 15, 4, 11, 0F); // scope
		defaultScopeModel[11].setRotationPoint(28F, -33.5F, -5.5F);

		defaultScopeModel[12].addShapeBox(0F, 0F, 0F, 15, 3, 11, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // scope
		defaultScopeModel[12].setRotationPoint(28F, -29.5F, -5.5F);

		defaultScopeModel[13].addShapeBox(0F, 0F, 0F, 1, 3, 10, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope
		defaultScopeModel[13].setRotationPoint(-0.2F, -36F, -5F);

		defaultScopeModel[14].addBox(0F, 0F, 0F, 1, 3, 10, 0F); // scope
		defaultScopeModel[14].setRotationPoint(-0.2F, -33F, -5F);

		defaultScopeModel[15].addShapeBox(0F, 0F, 0F, 1, 3, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // scope
		defaultScopeModel[15].setRotationPoint(-0.2F, -30F, -5F);

		defaultScopeModel[16].addShapeBox(0F, 0F, 0F, 2, 3, 6, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F); // scope
		defaultScopeModel[16].setRotationPoint(16F, -39.5F, -3F);

		defaultScopeModel[17].addShapeBox(0F, 0F, 0F, 2, 3, 6, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F); // scope
		defaultScopeModel[17].setRotationPoint(20F, -39.5F, -3F);

		defaultScopeModel[18].addBox(0F, 0F, 0F, 2, 3, 6, 0F); // scope
		defaultScopeModel[18].setRotationPoint(18F, -39.5F, -3F);

		defaultScopeModel[19].addShapeBox(0F, 0F, 0F, 2, 6, 3, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F); // scope
		defaultScopeModel[19].setRotationPoint(34F, -34.5F, -8F);

		defaultScopeModel[20].addBox(0F, 0F, 0F, 2, 6, 3, 0F); // scope
		defaultScopeModel[20].setRotationPoint(36F, -34.5F, -8F);

		defaultScopeModel[21].addShapeBox(0F, 0F, 0F, 2, 6, 3, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F); // scope
		defaultScopeModel[21].setRotationPoint(38F, -34.5F, -8F);

		defaultScopeModel[22].addShapeBox(0F, 0F, 0F, 1, 3, 10, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope
		defaultScopeModel[22].setRotationPoint(8.9F, -36F, -5F);

		defaultScopeModel[23].addBox(0F, 0F, 0F, 1, 3, 10, 0F); // scope
		defaultScopeModel[23].setRotationPoint(8.9F, -33F, -5F);

		defaultScopeModel[24].addShapeBox(0F, 0F, 0F, 1, 3, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // scope
		defaultScopeModel[24].setRotationPoint(8.9F, -30F, -5F);

		defaultScopeModel[25].addShapeBox(0F, 0F, 0F, 1, 3, 10, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope
		defaultScopeModel[25].setRotationPoint(42.2F, -36F, -5F);

		defaultScopeModel[26].addBox(0F, 0F, 0F, 1, 3, 10, 0F); // scope
		defaultScopeModel[26].setRotationPoint(42.2F, -33F, -5F);

		defaultScopeModel[27].addShapeBox(0F, 0F, 0F, 1, 3, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // scope
		defaultScopeModel[27].setRotationPoint(42.2F, -30F, -5F);

		defaultScopeModel[28].addBox(0F, 0F, 0F, 18, 10, 12, 0F); // scope
		defaultScopeModel[28].setRotationPoint(10F, -36.5F, -6F);

		defaultScopeModel[29].addShapeBox(0F, 0F, 0F, 18, 1, 12, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope
		defaultScopeModel[29].setRotationPoint(10F, -37.5F, -6F);

		defaultScopeModel[30].addShapeBox(0F, 0F, 0F, 18, 1, 12, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // scope
		defaultScopeModel[30].setRotationPoint(10F, -26.5F, -6F);

		defaultScopeModel[31].addShapeBox(0F, 0F, 0F, 8, 4, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F); // scope
		defaultScopeModel[31].setRotationPoint(11F, -36.5F, 6F);

		defaultScopeModel[32].addShapeBox(0F, 0F, 0F, 8, 2, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F); // scope
		defaultScopeModel[32].setRotationPoint(11F, -32.5F, 6F);

		defaultScopeModel[33].addBox(0F, 0F, 0F, 8, 4, 1, 0F); // scope
		defaultScopeModel[33].setRotationPoint(11F, -30.5F, 5.5F);

		defaultScopeModel[34].addShapeBox(0F, 0F, 0F, 8, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope
		defaultScopeModel[34].setRotationPoint(11F, -37.5F, 5F);

		defaultScopeModel[35].addShapeBox(0F, 0F, 0F, 14, 1, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope
		defaultScopeModel[35].setRotationPoint(0.5F, -40.5F, -3.5F);

		defaultScopeModel[36].addBox(0F, 0F, 0F, 14, 2, 7, 0F); // scope
		defaultScopeModel[36].setRotationPoint(0.5F, -39.5F, -3.5F);

		defaultScopeModel[37].addShapeBox(0F, 0F, 0F, 1, 1, 2, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope
		defaultScopeModel[37].setRotationPoint(14.5F, -38.5F, 1F);

		defaultScopeModel[38].addShapeBox(0F, 0F, 0F, 1, 1, 2, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope
		defaultScopeModel[38].setRotationPoint(14.5F, -38.5F, -3F);

		defaultScopeModel[39].addShapeBox(0F, 0F, 0F, 16, 1, 8, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope
		defaultScopeModel[39].setRotationPoint(11F, -24.5F, -4F);

		defaultScopeModel[40].addBox(0F, 0F, 0F, 12, 1, 6, 0F); // scope
		defaultScopeModel[40].setRotationPoint(13F, -25.5F, -3F);

		defaultScopeModel[41].addShapeBox(0F, 0F, 0F, 12, 5, 1, 0F, -1F, 0F, 0F, 2F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, -1F, -1F, 0F, -0.5F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope
		defaultScopeModel[41].setRotationPoint(10F, -31.5F, -6.5F);

		defaultScopeModel[42].addShapeBox(0F, 0F, 0F, 15, 5, 1, 0F, -1F, -1F, 0F, -1F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope
		defaultScopeModel[42].setRotationPoint(10F, -36.5F, -6.5F);

		defaultScopeModel[43].addBox(0F, 0F, 0F, 16, 3, 8, 0F); // scope
		defaultScopeModel[43].setRotationPoint(11F, -23.5F, -4F);

		defaultScopeModel[44].addShapeBox(0F, 0F, 0F, 3, 1, 8, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope
		defaultScopeModel[44].setRotationPoint(1.5F, -37.5F, -4F);

		defaultScopeModel[45].addBox(0F, 0F, 0F, 3, 1, 8, 0F); // scope
		defaultScopeModel[45].setRotationPoint(1.5F, -36.5F, -4F);

		defaultScopeModel[46].addShapeBox(0F, 0F, 0F, 1, 6, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -5F, 0F, 0F, -5F, 0F, -5F, 0F, 0F, -5F, 0F, 0F, -5F, -5F, 0F, -5F, -5F); // Box 199
		defaultScopeModel[46].setRotationPoint(0.3F, -39.5F, -2.5F);


		defaultStockModel = new ModelRendererTurbo[28];
		defaultStockModel[0] = new ModelRendererTurbo(this, 1, 141, textureX, textureY); // stock
		defaultStockModel[1] = new ModelRendererTurbo(this, 1, 124, textureX, textureY); // stock
		defaultStockModel[2] = new ModelRendererTurbo(this, 1, 155, textureX, textureY); // stock
		defaultStockModel[3] = new ModelRendererTurbo(this, 54, 122, textureX, textureY); // stock
		defaultStockModel[4] = new ModelRendererTurbo(this, 54, 141, textureX, textureY); // stock
		defaultStockModel[5] = new ModelRendererTurbo(this, 54, 156, textureX, textureY); // stock
		defaultStockModel[6] = new ModelRendererTurbo(this, 91, 141, textureX, textureY); // stock
		defaultStockModel[7] = new ModelRendererTurbo(this, 69, 171, textureX, textureY); // stock
		defaultStockModel[8] = new ModelRendererTurbo(this, 48, 170, textureX, textureY); // stock
		defaultStockModel[9] = new ModelRendererTurbo(this, 48, 205, textureX, textureY); // stock
		defaultStockModel[10] = new ModelRendererTurbo(this, 1, 176, textureX, textureY); // stock
		defaultStockModel[11] = new ModelRendererTurbo(this, 1, 165, textureX, textureY); // stock
		defaultStockModel[12] = new ModelRendererTurbo(this, 91, 128, textureX, textureY); // stock
		defaultStockModel[13] = new ModelRendererTurbo(this, 1, 165, textureX, textureY); // stock
		defaultStockModel[14] = new ModelRendererTurbo(this, 80, 193, textureX, textureY); // stock
		defaultStockModel[15] = new ModelRendererTurbo(this, 48, 198, textureX, textureY); // stock
		defaultStockModel[16] = new ModelRendererTurbo(this, 48, 187, textureX, textureY); // stock
		defaultStockModel[17] = new ModelRendererTurbo(this, 48, 205, textureX, textureY); // stock
		defaultStockModel[18] = new ModelRendererTurbo(this, 63, 205, textureX, textureY); // stock
		defaultStockModel[19] = new ModelRendererTurbo(this, 63, 205, textureX, textureY); // stock
		defaultStockModel[20] = new ModelRendererTurbo(this, 76, 206, textureX, textureY); // stock
		defaultStockModel[21] = new ModelRendererTurbo(this, 1, 189, textureX, textureY); // stock
		defaultStockModel[22] = new ModelRendererTurbo(this, 1, 189, textureX, textureY); // stock
		defaultStockModel[23] = new ModelRendererTurbo(this, 1, 189, textureX, textureY); // stock
		defaultStockModel[24] = new ModelRendererTurbo(this, 1, 205, textureX, textureY); // stock
		defaultStockModel[25] = new ModelRendererTurbo(this, 1, 198, textureX, textureY); // stock
		defaultStockModel[26] = new ModelRendererTurbo(this, 1, 205, textureX, textureY); // stock
		defaultStockModel[27] = new ModelRendererTurbo(this, 95, 189, textureX, textureY); // stock

		defaultStockModel[0].addShapeBox(0F, 0F, 0F, 15, 2, 11, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // stock
		defaultStockModel[0].setRotationPoint(-45F, -14F, -5.5F);

		defaultStockModel[1].addShapeBox(0F, 0F, 0F, 15, 5, 11, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // stock
		defaultStockModel[1].setRotationPoint(-45F, -19F, -5.5F);

		defaultStockModel[2].addShapeBox(0F, 0F, 0F, 15, 1, 8, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // stock
		defaultStockModel[2].setRotationPoint(-45F, -20F, -4F);

		defaultStockModel[3].addShapeBox(0F, 0F, 0F, 6, 6, 12, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // stock
		defaultStockModel[3].setRotationPoint(-51F, -19.5F, -6F);

		defaultStockModel[4].addShapeBox(0F, 0F, 0F, 6, 2, 12, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // stock
		defaultStockModel[4].setRotationPoint(-51F, -13.5F, -6F);

		defaultStockModel[5].addShapeBox(0F, 0F, 0F, 6, 1, 9, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // stock
		defaultStockModel[5].setRotationPoint(-51F, -20.5F, -4.5F);

		defaultStockModel[6].addBox(0F, 0F, 0F, 3, 16, 10, 0F); // stock
		defaultStockModel[6].setRotationPoint(-51F, -11.5F, -5F);

		defaultStockModel[7].addShapeBox(0F, 0F, 0F, 1, 6, 9, 0F, 0F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // stock
		defaultStockModel[7].setRotationPoint(-45F, -12F, -4.5F);

		defaultStockModel[8].addBox(0F, 0F, 0F, 1, 7, 9, 0F); // stock
		defaultStockModel[8].setRotationPoint(-45F, -6F, -4.5F);

		defaultStockModel[9].addShapeBox(0F, 0F, 0F, 2, 4, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F); // stock
		defaultStockModel[9].setRotationPoint(-36F, -7F, -2.5F);

		defaultStockModel[10].addShapeBox(0F, 0F, 0F, 1, 3, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F); // stock
		defaultStockModel[10].setRotationPoint(-45F, 1F, -4.5F);

		defaultStockModel[11].addBox(0F, 0F, 0F, 6, 1, 9, 0F); // stock
		defaultStockModel[11].setRotationPoint(-48F, 4F, -4.5F);

		defaultStockModel[12].addShapeBox(0F, 0F, 0F, 3, 2, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // stock
		defaultStockModel[12].setRotationPoint(-51F, 4.5F, -5F);

		defaultStockModel[13].addShapeBox(0F, 0F, 0F, 6, 1, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // stock
		defaultStockModel[13].setRotationPoint(-48F, 5F, -4.5F);

		defaultStockModel[14].addBox(0F, 0F, 0F, 2, 6, 5, 0F); // stock
		defaultStockModel[14].setRotationPoint(-44F, -2F, -2.5F);

		defaultStockModel[15].addShapeBox(0F, 0F, 0F, 10, 1, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F); // stock
		defaultStockModel[15].setRotationPoint(-44F, -3F, -2.5F);

		defaultStockModel[16].addBox(0F, 0F, 0F, 13, 5, 5, 0F); // stock
		defaultStockModel[16].setRotationPoint(-44F, -12F, -2.5F);

		defaultStockModel[17].addShapeBox(0F, 0F, 0F, 2, 4, 5, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // stock
		defaultStockModel[17].setRotationPoint(-44F, -7F, -2.5F);

		defaultStockModel[18].addShapeBox(0F, 0F, 0F, 1, 4, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F); // stock
		defaultStockModel[18].setRotationPoint(-40F, -7F, -2.5F);

		defaultStockModel[19].addShapeBox(0F, 0F, 0F, 1, 4, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F); // stock
		defaultStockModel[19].setRotationPoint(-38F, -7F, -2.5F);

		defaultStockModel[20].addShapeBox(0F, 0F, 0F, 5, 4, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F); // stock
		defaultStockModel[20].setRotationPoint(-41F, -7F, -2F);

		defaultStockModel[21].addShapeBox(0F, 0F, 0F, 17, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // stock
		defaultStockModel[21].setRotationPoint(-30F, -18.5F, -3F);

		defaultStockModel[22].addBox(0F, 0F, 0F, 17, 2, 6, 0F); // stock
		defaultStockModel[22].setRotationPoint(-30F, -16.5F, -3F);

		defaultStockModel[23].addShapeBox(0F, 0F, 0F, 17, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // stock
		defaultStockModel[23].setRotationPoint(-30F, -14.5F, -3F);

		defaultStockModel[24].addShapeBox(0F, 0F, 0F, 18, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // stock
		defaultStockModel[24].setRotationPoint(-31F, -8.5F, -2F);

		defaultStockModel[25].addBox(0F, 0F, 0F, 18, 2, 4, 0F); // stock
		defaultStockModel[25].setRotationPoint(-31F, -10.5F, -2F);

		defaultStockModel[26].addShapeBox(0F, 0F, 0F, 18, 1, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // stock
		defaultStockModel[26].setRotationPoint(-31F, -11.5F, -2F);

		defaultStockModel[27].addBox(0F, 0F, 0F, 3, 16, 9, 0F); // stock
		defaultStockModel[27].setRotationPoint(-48F, -12F, -4.5F);


		ammoModel = new ModelRendererTurbo[16];
		ammoModel[0] = new ModelRendererTurbo(this, 147, 117, textureX, textureY); // ammo
		ammoModel[1] = new ModelRendererTurbo(this, 147, 169, textureX, textureY); // ammo
		ammoModel[2] = new ModelRendererTurbo(this, 213, 151, textureX, textureY); // ammo
		ammoModel[3] = new ModelRendererTurbo(this, 147, 169, textureX, textureY); // ammo
		ammoModel[4] = new ModelRendererTurbo(this, 147, 169, textureX, textureY); // ammo
		ammoModel[5] = new ModelRendererTurbo(this, 186, 151, textureX, textureY); // ammo
		ammoModel[6] = new ModelRendererTurbo(this, 147, 141, textureX, textureY); // ammo
		ammoModel[7] = new ModelRendererTurbo(this, 147, 151, textureX, textureY); // ammo
		ammoModel[8] = new ModelRendererTurbo(this, 204, 120, textureX, textureY); // ammo
		ammoModel[9] = new ModelRendererTurbo(this, 204, 133, textureX, textureY); // ammo
		ammoModel[10] = new ModelRendererTurbo(this, 204, 127, textureX, textureY); // ammo
		ammoModel[11] = new ModelRendererTurbo(this, 221, 127, textureX, textureY); // ammo
		ammoModel[12] = new ModelRendererTurbo(this, 204, 143, textureX, textureY); // ammo
		ammoModel[13] = new ModelRendererTurbo(this, 204, 148, textureX, textureY); // ammo
		ammoModel[14] = new ModelRendererTurbo(this, 204, 143, textureX, textureY); // ammo
		ammoModel[15] = new ModelRendererTurbo(this, 204, 148, textureX, textureY); // ammo

		ammoModel[0].addBox(0F, 0F, 0F, 18, 4, 8, 0F); // ammo
		ammoModel[0].setRotationPoint(14F, -6F, -4F);

		ammoModel[1].addBox(0F, 0F, 0F, 12, 2, 8, 0F); // ammo
		ammoModel[1].setRotationPoint(19F, 0F, -4F);

		ammoModel[2].addBox(0F, 0F, 0F, 1, 12, 8, 0F); // ammo
		ammoModel[2].setRotationPoint(31F, -2F, -4F);

		ammoModel[3].addBox(0F, 0F, 0F, 12, 2, 8, 0F); // ammo
		ammoModel[3].setRotationPoint(19F, 4F, -4F);

		ammoModel[4].addBox(0F, 0F, 0F, 12, 2, 8, 0F); // ammo
		ammoModel[4].setRotationPoint(19F, 8F, -4F);

		ammoModel[5].addBox(0F, 0F, 0F, 5, 12, 8, 0F); // ammo
		ammoModel[5].setRotationPoint(14F, -2F, -4F);

		ammoModel[6].addShapeBox(0F, 0F, 0F, 18, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F); // ammo
		ammoModel[6].setRotationPoint(14F, 10F, -4F);

		ammoModel[7].addBox(0F, 0F, 0F, 12, 10, 7, 0F); // ammo
		ammoModel[7].setRotationPoint(19F, -2F, -3.5F);

		ammoModel[8].addShapeBox(0F, 0F, 0F, 8, 1, 5, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // ammo
		ammoModel[8].setRotationPoint(15F, -7F, -2.5F);

		ammoModel[9].addShapeBox(0F, 0F, 0F, 2, 1, 5, 0F, 0F, 0F, -1F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F); // ammo
		ammoModel[9].setRotationPoint(23F, -7F, -2.5F);

		ammoModel[10].addShapeBox(0F, 0F, 0F, 4, 1, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // ammo
		ammoModel[10].setRotationPoint(25F, -6.8F, -2F);

		ammoModel[11].addShapeBox(0F, 0F, 0F, 2, 1, 4, 0F, 0F, 0F, -1F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F); // ammo
		ammoModel[11].setRotationPoint(29F, -6.8F, -2F);

		ammoModel[12].addBox(0F, 0F, 0F, 7, 3, 1, 0F); // ammo
		ammoModel[12].setRotationPoint(20F, -6.5F, -4.5F);

		ammoModel[13].addBox(0F, 0F, 0F, 3, 1, 1, 0F); // ammo
		ammoModel[13].setRotationPoint(22F, -3.5F, -4.5F);

		ammoModel[14].addBox(0F, 0F, 0F, 7, 3, 1, 0F); // ammo
		ammoModel[14].setRotationPoint(20F, -6.5F, 3.5F);

		ammoModel[15].addBox(0F, 0F, 0F, 3, 1, 1, 0F); // ammo
		ammoModel[15].setRotationPoint(22F, -3.5F, 3.5F);


		pumpModel = new ModelRendererTurbo[10];
		pumpModel[0] = new ModelRendererTurbo(this, 44, 1, textureX, textureY); // slide
		pumpModel[1] = new ModelRendererTurbo(this, 42, 48, textureX, textureY); // slide
		pumpModel[2] = new ModelRendererTurbo(this, 42, 48, textureX, textureY); // slide
		pumpModel[3] = new ModelRendererTurbo(this, 42, 48, textureX, textureY); // slide
		pumpModel[4] = new ModelRendererTurbo(this, 61, 50, textureX, textureY); // slide
		pumpModel[5] = new ModelRendererTurbo(this, 61, 43, textureX, textureY); // slide
		pumpModel[6] = new ModelRendererTurbo(this, 61, 50, textureX, textureY); // slide
		pumpModel[7] = new ModelRendererTurbo(this, 53, 44, textureX, textureY); // slide
		pumpModel[8] = new ModelRendererTurbo(this, 42, 44, textureX, textureY); // slide
		pumpModel[9] = new ModelRendererTurbo(this, 53, 44, textureX, textureY); // slide

		pumpModel[0].addShapeBox(0F, 0F, 0F, 14, 3, 1, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // slide
		pumpModel[0].setRotationPoint(15F, -17F, -4.75F);

		pumpModel[1].addShapeBox(0.5F, 0.5F, 4F, 3, 1, 6, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // slide
		pumpModel[1].setRotationPoint(11F, -20F, -13.5F);
		pumpModel[1].rotateAngleX = -0.26179939F;

		pumpModel[2].addBox(0.5F, 1.5F, 4F, 3, 1, 6, 0F); // slide
		pumpModel[2].setRotationPoint(11F, -20F, -13.5F);
		pumpModel[2].rotateAngleX = -0.26179939F;

		pumpModel[3].addShapeBox(0.5F, 2.5F, 4F, 3, 1, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // slide
		pumpModel[3].setRotationPoint(11F, -20F, -13.5F);
		pumpModel[3].rotateAngleX = -0.26179939F;

		pumpModel[4].addShapeBox(0F, 3F, 0F, 4, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // slide
		pumpModel[4].setRotationPoint(11F, -20F, -13.5F);
		pumpModel[4].rotateAngleX = -0.26179939F;

		pumpModel[5].addBox(0F, 1F, 0F, 4, 2, 4, 0F); // slide
		pumpModel[5].setRotationPoint(11F, -20F, -13.5F);
		pumpModel[5].rotateAngleX = -0.26179939F;

		pumpModel[6].addShapeBox(0F, 0F, 0F, 4, 1, 4, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // slide
		pumpModel[6].setRotationPoint(11F, -20F, -13.5F);
		pumpModel[6].rotateAngleX = -0.26179939F;

		pumpModel[7].addShapeBox(0F, 0F, -1F, 4, 1, 1, 0F, -1F, -0.5F, -0.5F, -1F, -0.5F, -0.5F, -1F, 0F, 0F, -1F, 0F, 0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // slide
		pumpModel[7].setRotationPoint(11F, -20F, -13.5F);
		pumpModel[7].rotateAngleX = -0.26179939F;

		pumpModel[8].addShapeBox(0F, 1F, -1F, 4, 2, 1, 0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // slide
		pumpModel[8].setRotationPoint(11F, -20F, -13.5F);
		pumpModel[8].rotateAngleX = -0.26179939F;

		pumpModel[9].addShapeBox(0F, 3F, -1F, 4, 1, 1, 0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, -0.5F, -0.5F, -1F, -0.5F, -0.5F, -1F, 0F, 0F, -1F, 0F, 0F); // slide
		pumpModel[9].setRotationPoint(11F, -20F, -13.5F);
		pumpModel[9].rotateAngleX = -0.26179939F;

		barrelAttachPoint = new Vector3f(91F /16F, 8.5F /16F, 0F /16F);
		stockAttachPoint = new Vector3f(-13F /16F, 12F /16F, 0F /16F);
		scopeAttachPoint = new Vector3f(19F /16F, 20F /16F, 0F /16F);
		gripAttachPoint = new Vector3f(48 /16F, 5F /16F, 0F /16F);

		animationType = EnumAnimationType.BOTTOM_CLIP;

		//Pump parameters
		pumpDelayAfterReload = 85;
		pumpDelay = 10;
		pumpTime = 8;
		gripIsOnPump = false;
		pumpHandleDistance = 0.8F;

		translateAll(0F, 0F, 0F);

		flipAll();
	}
}