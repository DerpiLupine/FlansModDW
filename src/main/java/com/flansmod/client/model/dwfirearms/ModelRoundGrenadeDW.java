package com.flansmod.client.model.dwfirearms;

import net.minecraft.client.model.ModelBase;
import net.minecraft.entity.Entity;

import com.flansmod.client.tmt.ModelRendererTurbo;

public class ModelRoundGrenadeDW extends ModelBase 
{
	public ModelRendererTurbo[] grenadeModel;

	int textureX = 64;
	int textureY = 32;

	public ModelRoundGrenadeDW()
	{
		grenadeModel = new ModelRendererTurbo[6];
		grenadeModel[0] = new ModelRendererTurbo(this, 45, 2, textureX, textureY); // Import 
		grenadeModel[1] = new ModelRendererTurbo(this, 32, 1, textureX, textureY); // Box 0
		grenadeModel[2] = new ModelRendererTurbo(this, 45, 2, textureX, textureY); // Box 2
		grenadeModel[3] = new ModelRendererTurbo(this, 45, 11, textureX, textureY); // Box 3
		grenadeModel[4] = new ModelRendererTurbo(this, 32, 10, textureX, textureY); // Box 4
		grenadeModel[5] = new ModelRendererTurbo(this, 45, 11, textureX, textureY); // Box 6

		grenadeModel[0].addShapeBox(-2F, 0F, -2F, 4, 6, 1, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import 
		grenadeModel[1].addBox(-2F, 0F, -1F, 4, 6, 2, 0F); // Box 0
		grenadeModel[2].addShapeBox(-2F, 0F, 1F, 4, 6, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 2
		grenadeModel[3].addShapeBox(-2F, 6F, -2F, 4, 2, 1, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, -1F, -1.5F, 0F, -1F, -1F, 0F, 0.5F, -1F, 0F, 0.5F); // Box 3
		grenadeModel[4].addShapeBox(-2F, 6F, -1F, 4, 2, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -0.5F, -1F, 0F, -0.5F, -1F, 0F, -0.5F, -1F, 0F, -0.5F); // Box 4
		grenadeModel[5].addShapeBox(-2F, 6F, 1F, 4, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0.5F, -1F, 0F, 0.5F, -1.5F, 0F, -1F, -1.5F, 0F, -1F); // Box 6

		for(int i = 0; i < 6; i++)
			grenadeModel[i].rotateAngleZ = 1.57079633F;
	}

	@Override
	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
	{
		for(ModelRendererTurbo mineModelBit : grenadeModel)
			mineModelBit.render(f5);
	}
}
