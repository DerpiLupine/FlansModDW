package com.flansmod.client.model.dwfirearms;

import net.minecraft.entity.Entity;

import com.flansmod.client.model.ModelBullet;
import com.flansmod.client.tmt.ModelRendererTurbo;

public class ModelBulletRailwaySpike extends ModelBullet 
{
	public ModelRendererTurbo[] bulletModel;
	
	public ModelBulletRailwaySpike()
	{
		int textureX = 64;
		int textureY = 32;
	
		bulletModel = new ModelRendererTurbo[2];
		bulletModel[0] = new ModelRendererTurbo(this, 0, 15, textureX, textureY); // Box 0
		bulletModel[1] = new ModelRendererTurbo(this, 5, 15, textureX, textureY); // Box 1

		bulletModel[0].addShapeBox(0F, 0F, 0F, 1, 3, 1, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -1.5F, -0.5F, 0F, -1.5F, -0.5F, 0F, -1.5F, 0F, -0.5F, -1.5F, 0F); // Box 0
		bulletModel[0].setRotationPoint(-0.5F, -1.5F, -0.5F);

		bulletModel[1].addShapeBox(0F, 0F, 0F, 1, 2, 1, 0F, -0.5F, -0.2F, 0F, 0F, -0.2F, 0F, 0F, -0.2F, 0F, -0.5F, -0.2F, 0F, -0.5F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, -0.5F, -1.5F, 0F); // Box 1
		bulletModel[1].setRotationPoint(-0.5F, -2F, -0.4F);

	}
	
	@Override
	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
	{
		//GL11.glScalef(0.5F, 0.5F, 0.5F);
		for(ModelRendererTurbo mrt : bulletModel)
			mrt.render(f5);
	}

	//uses the bullet map texture.
}