package com.flansmod.client.model.dwfirearms;

import net.minecraft.client.model.ModelBase;
import net.minecraft.entity.Entity;

import com.flansmod.client.tmt.ModelRendererTurbo;

public class ModelFragGrenadeLPM extends ModelBase 
{
	public ModelRendererTurbo[] grenadeModel;

	int textureX = 32;
	int textureY = 16;

	public ModelFragGrenadeLPM()
	{
		grenadeModel = new ModelRendererTurbo[13];
		grenadeModel[0] = new ModelRendererTurbo(this, 10, 5, textureX, textureY); // lever2
		grenadeModel[1] = new ModelRendererTurbo(this, 10, 1, textureX, textureY); // lever1
		grenadeModel[2] = new ModelRendererTurbo(this, 17, 1, textureX, textureY); // buttonTop1
		grenadeModel[3] = new ModelRendererTurbo(this, 17, 1, textureX, textureY); // buttonTop2
		grenadeModel[4] = new ModelRendererTurbo(this, 1, 4, textureX, textureY); // Box 1
		grenadeModel[5] = new ModelRendererTurbo(this, 1, 4, textureX, textureY); // Box 3
		grenadeModel[6] = new ModelRendererTurbo(this, 1, 4, textureX, textureY); // Box 4
		grenadeModel[7] = new ModelRendererTurbo(this, 1, 7, textureX, textureY); // Box 6
		grenadeModel[8] = new ModelRendererTurbo(this, 1, 7, textureX, textureY); // Box 7
		grenadeModel[9] = new ModelRendererTurbo(this, 1, 7, textureX, textureY); // Box 8
		grenadeModel[10] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 9
		grenadeModel[11] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 10
		grenadeModel[12] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 11

		grenadeModel[0].addShapeBox(-0.5F, -2.6F, 1.3F, 1, 2, 1, 0F,0.1F, -0.2F, 0.2F,0.1F, -0.2F, 0.2F,0.1F, -0.2F, -0.5F,0.1F, -0.2F, -0.5F,0.1F, 0F, -0.3F,0.1F, 0F, -0.3F,0.1F, 0F, 0F,0.1F, 0F, 0F); // lever2
		grenadeModel[1].addShapeBox(-0.5F, -2.7F, -0.7F, 1, 1, 2, 0F,0.1F, -0.3F, 0F,0.1F, -0.3F, 0F,0.1F, -0.3F, 0F,0.1F, -0.3F, 0F,0.1F, 0F, 0F,0.1F, 0F, 0F,0.1F, 0F, 0F,0.1F, 0F, 0F); // lever1
		grenadeModel[2].addShapeBox(-0.5F, -2F, -0.5F, 1, 1, 1, 0F,0F, 0F, -0.25F,-0.5F, 0F, 0F,-0.5F, 0F, 0F,0F, 0F, -0.25F,0F, 0F, -0.25F,-0.5F, 0F, 0F,-0.5F, 0F, 0F,0F, 0F, -0.25F); // buttonTop1
		grenadeModel[3].addShapeBox(-0.5F, -2F, -0.5F, 1, 1, 1, 0F,-0.5F, 0F, 0F,0F, 0F, -0.25F,0F, 0F, -0.25F,-0.5F, 0F, 0F,-0.5F, 0F, 0F,0F, 0F, -0.25F,0F, 0F, -0.25F,-0.5F, 0F, 0F); // buttonTop2
		grenadeModel[4].addBox(-1.5F, -0.5F, -0.5F, 3, 1, 1, 0F); // Box 1
		grenadeModel[5].addShapeBox(-1.5F, -0.5F, -1.5F, 3, 1, 1, 0F,-1F, 0F, 0F,-1F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,-1F, 0F, 0F,-1F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F); // Box 3
		grenadeModel[6].addShapeBox(-1.5F, -0.5F, 0.5F, 3, 1, 1, 0F,0F, 0F, 0F,0F, 0F, 0F,-1F, 0F, 0F,-1F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,-1F, 0F, 0F,-1F, 0F, 0F); // Box 4
		grenadeModel[7].addShapeBox(-1.5F, 0.5F, 0.5F, 3, 1, 1, 0F,0F, 0F, 0F,0F, 0F, 0F,-1F, 0F, 0F,-1F, 0F, 0F,-0.9F, 0F, 0F,-0.9F, 0F, 0F,-1.3F, 0F, -0.7F,-1.3F, 0F, -0.7F); // Box 6
		grenadeModel[8].addShapeBox(-1.5F, 0.5F, -0.5F, 3, 1, 1, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,-0.9F, 0F, 0F,-0.9F, 0F, 0F,-0.9F, 0F, 0F,-0.9F, 0F, 0F); // Box 7
		grenadeModel[9].addShapeBox(-1.5F, 0.5F, -1.5F, 3, 1, 1, 0F,-1F, 0F, 0F,-1F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,-1.3F, 0F, -0.7F,-1.3F, 0F, -0.7F,-0.9F, 0F, 0F,-0.9F, 0F, 0F); // Box 8
		grenadeModel[10].addShapeBox(-1.5F, -1.5F, 0.5F, 3, 1, 1, 0F,-0.9F, 0F, 0F,-0.9F, 0F, 0F,-1.3F, 0F, -0.7F,-1.3F, 0F, -0.7F,0F, 0F, 0F,0F, 0F, 0F,-1F, 0F, 0F,-1F, 0F, 0F); // Box 9
		grenadeModel[11].addShapeBox(-1.5F, -1.5F, -1.5F, 3, 1, 1, 0F,-1.3F, 0F, -0.7F,-1.3F, 0F, -0.7F,-0.9F, 0F, 0F,-0.9F, 0F, 0F,-1F, 0F, 0F,-1F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F); // Box 10
		grenadeModel[12].addShapeBox(-1.5F, -1.5F, -0.5F, 3, 1, 1, 0F,-0.9F, 0F, 0F,-0.9F, 0F, 0F,-0.9F, 0F, 0F,-0.9F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F); // Box 11
	
		for(int i = 0; i < 13; i++)
			grenadeModel[i].rotateAngleZ = 3.14159265F;
	}

	@Override
	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
	{
		for(ModelRendererTurbo mineModelBit : grenadeModel)
			mineModelBit.render(f5);
	}
}