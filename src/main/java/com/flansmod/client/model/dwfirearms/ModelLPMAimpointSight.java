package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.ModelAttachment;
import com.flansmod.client.tmt.ModelRendererTurbo;

import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelLPMAimpointSight extends ModelAttachment
{
	int textureX = 512;
	int textureY = 256;

	public ModelLPMAimpointSight()
	{
		attachmentModel = new ModelRendererTurbo[33];
		attachmentModel[0] = new ModelRendererTurbo(this, 1, 46, textureX, textureY); // mounter
		attachmentModel[1] = new ModelRendererTurbo(this, 1, 68, textureX, textureY); // mounterBase3
		attachmentModel[2] = new ModelRendererTurbo(this, 1, 59, textureX, textureY); // mounterBase2
		attachmentModel[3] = new ModelRendererTurbo(this, 1, 76, textureX, textureY); // scope4
		attachmentModel[4] = new ModelRendererTurbo(this, 1, 85, textureX, textureY); // scope3
		attachmentModel[5] = new ModelRendererTurbo(this, 1, 85, textureX, textureY); // scope3
		attachmentModel[6] = new ModelRendererTurbo(this, 1, 85, textureX, textureY); // scope3
		attachmentModel[7] = new ModelRendererTurbo(this, 1, 85, textureX, textureY); // scope3
		attachmentModel[8] = new ModelRendererTurbo(this, 37, 63, textureX, textureY); // sight
		attachmentModel[9] = new ModelRendererTurbo(this, 22, 91, textureX, textureY); // scope1
		attachmentModel[10] = new ModelRendererTurbo(this, 22, 91, textureX, textureY); // scope1
		attachmentModel[11] = new ModelRendererTurbo(this, 1, 91, textureX, textureY); // scope2
		attachmentModel[12] = new ModelRendererTurbo(this, 1, 76, textureX, textureY); // Box 13
		attachmentModel[13] = new ModelRendererTurbo(this, 37, 63, textureX, textureY); // Box 14
		attachmentModel[14] = new ModelRendererTurbo(this, 37, 63, textureX, textureY); // Box 15
		attachmentModel[15] = new ModelRendererTurbo(this, 44, 61, textureX, textureY); // Box 16
		attachmentModel[16] = new ModelRendererTurbo(this, 44, 61, textureX, textureY); // Box 17
		attachmentModel[17] = new ModelRendererTurbo(this, 44, 68, textureX, textureY); // Box 18
		attachmentModel[18] = new ModelRendererTurbo(this, 44, 68, textureX, textureY); // Box 19
		attachmentModel[19] = new ModelRendererTurbo(this, 61, 69, textureX, textureY); // Box 19
		attachmentModel[20] = new ModelRendererTurbo(this, 61, 69, textureX, textureY); // Box 20
		attachmentModel[21] = new ModelRendererTurbo(this, 51, 46, textureX, textureY); // Box 21
		attachmentModel[22] = new ModelRendererTurbo(this, 49, 76, textureX, textureY); // Box 22
		attachmentModel[23] = new ModelRendererTurbo(this, 49, 76, textureX, textureY); // Box 23
		attachmentModel[24] = new ModelRendererTurbo(this, 57, 82, textureX, textureY); // Box 24
		attachmentModel[25] = new ModelRendererTurbo(this, 62, 76, textureX, textureY); // Box 25
		attachmentModel[26] = new ModelRendererTurbo(this, 41, 92, textureX, textureY); // Box 26
		attachmentModel[27] = new ModelRendererTurbo(this, 44, 85, textureX, textureY); // Box 27
		attachmentModel[28] = new ModelRendererTurbo(this, 44, 85, textureX, textureY); // Box 28
		attachmentModel[29] = new ModelRendererTurbo(this, 51, 37, textureX, textureY); // Box 29
		attachmentModel[30] = new ModelRendererTurbo(this, 62, 76, textureX, textureY); // Box 30
		attachmentModel[31] = new ModelRendererTurbo(this, 60, 55, textureX, textureY); // Box 31
		attachmentModel[32] = new ModelRendererTurbo(this, 60, 55, textureX, textureY); // Box 32

		attachmentModel[0].addBox(-10F, -3F, -4.5F, 20, 3, 9, 0F); // mounter
		attachmentModel[0].setRotationPoint(0F, 0F, 0F);

		attachmentModel[1].addBox(-7F, -5F, -3F, 14, 1, 6, 0F); // mounterBase3
		attachmentModel[1].setRotationPoint(0F, 0F, 0F);

		attachmentModel[2].addBox(-7F, -4F, -3.5F, 14, 1, 7, 0F); // mounterBase2
		attachmentModel[2].setRotationPoint(0F, 0F, 0F);

		attachmentModel[3].addShapeBox(-10F, -15F, -3.5F, 20, 1, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope4
		attachmentModel[3].setRotationPoint(0F, 0F, 0F);

		attachmentModel[4].addShapeBox(-10F, -14F, -3.5F, 20, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F); // scope3
		attachmentModel[4].setRotationPoint(0F, 0F, 0F);

		attachmentModel[5].addShapeBox(-10F, -10F, 4.5F, 20, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F); // scope3
		attachmentModel[5].setRotationPoint(0F, 0F, 0F);

		attachmentModel[6].addShapeBox(-10F, -14F, 4.5F, 20, 4, 1, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope3
		attachmentModel[6].setRotationPoint(0F, 0F, 0F);

		attachmentModel[7].addShapeBox(-10F, -10F, -3.5F, 20, 4, 1, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope3
		attachmentModel[7].setRotationPoint(0F, 0F, 0F);

		attachmentModel[8].addBox(0F, -12F, -0.5F, 1, 1, 1, 0F); // sight
		attachmentModel[8].setRotationPoint(0F, 0F, 0F);

		attachmentModel[9].addShapeBox(-3F, -16.5F, -3.5F, 2, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F); // scope1
		attachmentModel[9].setRotationPoint(0F, 0F, 0F);

		attachmentModel[10].addShapeBox(2F, -16.5F, -3.5F, 2, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F); // scope1
		attachmentModel[10].setRotationPoint(0F, 0F, 0F);

		attachmentModel[11].addBox(-1F, -16.5F, -3.5F, 3, 2, 7, 0F); // scope2
		attachmentModel[11].setRotationPoint(0F, 0F, 0F);

		attachmentModel[12].addShapeBox(-10F, -6F, -3.5F, 20, 1, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 13
		attachmentModel[12].setRotationPoint(0F, 0F, 0F);

		attachmentModel[13].addBox(0F, -9.5F, -2F, 1, 1, 1, 0F); // Box 14
		attachmentModel[13].setRotationPoint(0F, 0F, 0F);

		attachmentModel[14].addBox(0F, -9.5F, 1F, 1, 1, 1, 0F); // Box 15
		attachmentModel[14].setRotationPoint(0F, 0F, 0F);

		attachmentModel[15].addShapeBox(-8F, -15.5F, 3.5F, 16, 2, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 16
		attachmentModel[15].setRotationPoint(0F, 0F, 0F);

		attachmentModel[16].addShapeBox(-8F, -13.5F, 3.5F, 16, 2, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 17
		attachmentModel[16].setRotationPoint(0F, 0F, 0F);

		attachmentModel[17].addShapeBox(-6.5F, -12F, -8.5F, 4, 2, 4, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 18
		attachmentModel[17].setRotationPoint(0F, 0F, 0F);

		attachmentModel[18].addShapeBox(-6.5F, -10F, -8.5F, 4, 2, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 19
		attachmentModel[18].setRotationPoint(0F, 0F, 0F);

		attachmentModel[19].addShapeBox(-15F, -14F, -3.5F, 5, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 19
		attachmentModel[19].setRotationPoint(0F, 0F, 0F);

		attachmentModel[20].addShapeBox(-15F, -10F, -3.5F, 5, 4, 1, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 20
		attachmentModel[20].setRotationPoint(0F, 0F, 0F);

		attachmentModel[21].addShapeBox(-15F, -6F, -3.5F, 5, 1, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 21
		attachmentModel[21].setRotationPoint(0F, 0F, 0F);

		attachmentModel[22].addShapeBox(-15F, -10F, 4.5F, 5, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 22
		attachmentModel[22].setRotationPoint(0F, 0F, 0F);

		attachmentModel[23].addShapeBox(-15F, -14F, 4.5F, 5, 4, 1, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 23
		attachmentModel[23].setRotationPoint(0F, 0F, 0F);

		attachmentModel[24].addShapeBox(-15F, -15F, -3.5F, 5, 1, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 24
		attachmentModel[24].setRotationPoint(0F, 0F, 0F);

		attachmentModel[25].addShapeBox(10F, -14F, -3.5F, 5, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 25
		attachmentModel[25].setRotationPoint(0F, 0F, 0F);

		attachmentModel[26].addShapeBox(10F, -15F, -3.5F, 5, 1, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 26
		attachmentModel[26].setRotationPoint(0F, 0F, 0F);

		attachmentModel[27].addShapeBox(10F, -14F, 4.5F, 5, 4, 1, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 27
		attachmentModel[27].setRotationPoint(0F, 0F, 0F);

		attachmentModel[28].addShapeBox(10F, -10F, 4.5F, 5, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 28
		attachmentModel[28].setRotationPoint(0F, 0F, 0F);

		attachmentModel[29].addShapeBox(10F, -6F, -3.5F, 5, 1, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 29
		attachmentModel[29].setRotationPoint(0F, 0F, 0F);

		attachmentModel[30].addShapeBox(10F, -10F, -3.5F, 5, 4, 1, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 30
		attachmentModel[30].setRotationPoint(0F, 0F, 0F);

		attachmentModel[31].addShapeBox(-2F, -14F, -6.5F, 5, 4, 1, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 31
		attachmentModel[31].setRotationPoint(0F, 0F, 0F);

		attachmentModel[32].addShapeBox(-2F, -10F, -6.5F, 5, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 2F, 0F, 0F, 2F); // Box 32
		attachmentModel[32].setRotationPoint(0F, 0F, 0F);

		renderOffset = 0F;

		flipAll();
	}
}