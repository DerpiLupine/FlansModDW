package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelSteamClockedSpinnaker extends ModelGun
{
	int textureX = 512;
	int textureY = 256;

	public ModelSteamClockedSpinnaker()
	{
		gunModel = new ModelRendererTurbo[77];
		gunModel[0] = new ModelRendererTurbo(this, 42, 101, textureX, textureY); // Import ammoEntrance
		gunModel[1] = new ModelRendererTurbo(this, 69, 1, textureX, textureY); // Import barrelBase1
		gunModel[2] = new ModelRendererTurbo(this, 69, 1, textureX, textureY); // Import barrelBase1-10
		gunModel[3] = new ModelRendererTurbo(this, 69, 1, textureX, textureY); // Import barrelBase1-11
		gunModel[4] = new ModelRendererTurbo(this, 69, 1, textureX, textureY); // Import barrelBase1-12
		gunModel[5] = new ModelRendererTurbo(this, 69, 1, textureX, textureY); // Import barrelBase1-2
		gunModel[6] = new ModelRendererTurbo(this, 69, 1, textureX, textureY); // Import barrelBase1-3
		gunModel[7] = new ModelRendererTurbo(this, 69, 1, textureX, textureY); // Import barrelBase1-4
		gunModel[8] = new ModelRendererTurbo(this, 69, 1, textureX, textureY); // Import barrelBase1-5
		gunModel[9] = new ModelRendererTurbo(this, 69, 1, textureX, textureY); // Import barrelBase1-6
		gunModel[10] = new ModelRendererTurbo(this, 69, 1, textureX, textureY); // Import barrelBase1-7
		gunModel[11] = new ModelRendererTurbo(this, 69, 1, textureX, textureY); // Import barrelBase1-8
		gunModel[12] = new ModelRendererTurbo(this, 69, 1, textureX, textureY); // Import barrelBase1-9
		gunModel[13] = new ModelRendererTurbo(this, 42, 1, textureX, textureY); // Import barrelBase2
		gunModel[14] = new ModelRendererTurbo(this, 162, 88, textureX, textureY); // Import barrelBolt1
		gunModel[15] = new ModelRendererTurbo(this, 88, 6, textureX, textureY); // Import barrelBolt2
		gunModel[16] = new ModelRendererTurbo(this, 88, 1, textureX, textureY); // Import barrelBolt2-2
		gunModel[17] = new ModelRendererTurbo(this, 142, 22, textureX, textureY); // Import body1
		gunModel[18] = new ModelRendererTurbo(this, 142, 1, textureX, textureY); // Import body2
		gunModel[19] = new ModelRendererTurbo(this, 142, 46, textureX, textureY); // Import body3
		gunModel[20] = new ModelRendererTurbo(this, 142, 46, textureX, textureY); // Import body3-2
		gunModel[21] = new ModelRendererTurbo(this, 195, 22, textureX, textureY); // Import body4
		gunModel[22] = new ModelRendererTurbo(this, 195, 46, textureX, textureY); // Import body5
		gunModel[23] = new ModelRendererTurbo(this, 1, 46, textureX, textureY); // Import boiler1
		gunModel[24] = new ModelRendererTurbo(this, 1, 33, textureX, textureY); // Import boiler2
		gunModel[25] = new ModelRendererTurbo(this, 1, 46, textureX, textureY); // Import boiler3
		gunModel[26] = new ModelRendererTurbo(this, 44, 48, textureX, textureY); // Import boiler4
		gunModel[27] = new ModelRendererTurbo(this, 44, 48, textureX, textureY); // Import boiler4-2
		gunModel[28] = new ModelRendererTurbo(this, 44, 48, textureX, textureY); // Import boiler4-3
		gunModel[29] = new ModelRendererTurbo(this, 44, 48, textureX, textureY); // Import boiler4-4
		gunModel[30] = new ModelRendererTurbo(this, 44, 33, textureX, textureY); // Import boiler5
		gunModel[31] = new ModelRendererTurbo(this, 44, 33, textureX, textureY); // Import boiler5-2
		gunModel[32] = new ModelRendererTurbo(this, 1, 58, textureX, textureY); // Import boiler6
		gunModel[33] = new ModelRendererTurbo(this, 44, 61, textureX, textureY); // Import boiler7
		gunModel[34] = new ModelRendererTurbo(this, 44, 61, textureX, textureY); // Import boiler8
		gunModel[35] = new ModelRendererTurbo(this, 144, 88, textureX, textureY); // Import caseHinge
		gunModel[36] = new ModelRendererTurbo(this, 144, 85, textureX, textureY); // Import caseRail1
		gunModel[37] = new ModelRendererTurbo(this, 144, 85, textureX, textureY); // Import caseRail1-2
		gunModel[38] = new ModelRendererTurbo(this, 1, 8, textureX, textureY); // Import gearPart1
		gunModel[39] = new ModelRendererTurbo(this, 1, 12, textureX, textureY); // Import gearPart2
		gunModel[40] = new ModelRendererTurbo(this, 1, 8, textureX, textureY); // Import gearPart3
		gunModel[41] = new ModelRendererTurbo(this, 1, 24, textureX, textureY); // Import gearPart4
		gunModel[42] = new ModelRendererTurbo(this, 1, 16, textureX, textureY); // Import gearPart5
		gunModel[43] = new ModelRendererTurbo(this, 1, 20, textureX, textureY); // Import gearPart6
		gunModel[44] = new ModelRendererTurbo(this, 1, 16, textureX, textureY); // Import gearPart7
		gunModel[45] = new ModelRendererTurbo(this, 1, 24, textureX, textureY); // Import gearPart8
		gunModel[46] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Import gearStub1
		gunModel[47] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Import gearStub2
		gunModel[48] = new ModelRendererTurbo(this, 1, 4, textureX, textureY); // Import gearStub3
		gunModel[49] = new ModelRendererTurbo(this, 1, 4, textureX, textureY); // Import gearStub4
		gunModel[50] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Import gearStub5
		gunModel[51] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Import gearStub6
		gunModel[52] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Import gearStub7
		gunModel[53] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Import gearStub8
		gunModel[54] = new ModelRendererTurbo(this, 42, 111, textureX, textureY); // Import grip1
		gunModel[55] = new ModelRendererTurbo(this, 1, 101, textureX, textureY); // Import grip2
		gunModel[56] = new ModelRendererTurbo(this, 16, 20, textureX, textureY); // Import ironSightBase
		gunModel[57] = new ModelRendererTurbo(this, 23, 1, textureX, textureY); // Import meter1
		gunModel[58] = new ModelRendererTurbo(this, 23, 1, textureX, textureY); // Import meter1-2
		gunModel[59] = new ModelRendererTurbo(this, 23, 5, textureX, textureY); // Import meter2
		gunModel[60] = new ModelRendererTurbo(this, 23, 5, textureX, textureY); // Import meter2-2
		gunModel[61] = new ModelRendererTurbo(this, 23, 9, textureX, textureY); // Import meter3
		gunModel[62] = new ModelRendererTurbo(this, 23, 9, textureX, textureY); // Import meter3-2
		gunModel[63] = new ModelRendererTurbo(this, 1, 73, textureX, textureY); // Import rail1
		gunModel[64] = new ModelRendererTurbo(this, 1, 65, textureX, textureY); // Import rail2
		gunModel[65] = new ModelRendererTurbo(this, 1, 79, textureX, textureY); // Import rail3
		gunModel[66] = new ModelRendererTurbo(this, 1, 79, textureX, textureY); // Import rail3-2
		gunModel[67] = new ModelRendererTurbo(this, 1, 82, textureX, textureY); // Import steamPipe1
		gunModel[68] = new ModelRendererTurbo(this, 1, 82, textureX, textureY); // Import steamPipe1-2
		gunModel[69] = new ModelRendererTurbo(this, 1, 82, textureX, textureY); // Import steamPipe1-3
		gunModel[70] = new ModelRendererTurbo(this, 10, 89, textureX, textureY); // Import steamPipe2
		gunModel[71] = new ModelRendererTurbo(this, 10, 89, textureX, textureY); // Import steamPipe2-2
		gunModel[72] = new ModelRendererTurbo(this, 10, 82, textureX, textureY); // Import steamPipe3
		gunModel[73] = new ModelRendererTurbo(this, 1, 28, textureX, textureY); // Import steamPipe4
		gunModel[74] = new ModelRendererTurbo(this, 1, 28, textureX, textureY); // Import steamPipe4-2
		gunModel[75] = new ModelRendererTurbo(this, 1, 28, textureX, textureY); // Import steamPipe4-3
		gunModel[76] = new ModelRendererTurbo(this, 23, 82, textureX, textureY); // Import steamPipe5

		gunModel[0].addShapeBox(0F, 0F, 0F, 9, 3, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F); // Import ammoEntrance
		gunModel[0].setRotationPoint(12F, -23F, 0F);

		gunModel[1].addBox(0F, 0F, 0F, 4, 5, 5, 0F); // Import barrelBase1
		gunModel[1].setRotationPoint(40F, -24.3F, -2.5F);

		gunModel[2].addBox(0F, 0F, 0F, 4, 5, 5, 0F); // Import barrelBase1-10
		gunModel[2].setRotationPoint(40F, -17.5F, -9.3F);

		gunModel[3].addBox(0F, 0F, 0F, 4, 5, 5, 0F); // Import barrelBase1-11
		gunModel[3].setRotationPoint(40F, -21.8F, -6.8F);
		gunModel[3].rotateAngleX = -0.52359878F;

		gunModel[4].addBox(0F, 0F, 0F, 4, 5, 5, 0F); // Import barrelBase1-12
		gunModel[4].setRotationPoint(40F, -21.8F, -6.8F);
		gunModel[4].rotateAngleX = 0.52359878F;

		gunModel[5].addBox(0F, 0F, 0F, 4, 5, 5, 0F); // Import barrelBase1-2
		gunModel[5].setRotationPoint(40F, -24.3F, 2.6F);
		gunModel[5].rotateAngleX = -0.4712389F;

		gunModel[6].addBox(0F, 0F, 0F, 4, 5, 5, 0F); // Import barrelBase1-3
		gunModel[6].setRotationPoint(40F, -19.5F, 2.8F);
		gunModel[6].rotateAngleX = 0.50614548F;

		gunModel[7].addBox(0F, 0F, 0F, 4, 5, 5, 0F); // Import barrelBase1-4
		gunModel[7].setRotationPoint(40F, -17.5F, 4.7F);

		gunModel[8].addBox(0F, 0F, 0F, 4, 5, 5, 0F); // Import barrelBase1-5
		gunModel[8].setRotationPoint(40F, -15F, 5.4F);
		gunModel[8].rotateAngleX = -0.54105207F;

		gunModel[9].addBox(0F, 0F, 0F, 4, 5, 5, 0F); // Import barrelBase1-6
		gunModel[9].setRotationPoint(40F, -9.9F, 0.1F);
		gunModel[9].rotateAngleX = 0.52359878F;

		gunModel[10].addBox(0F, 0F, 0F, 4, 5, 5, 0F); // Import barrelBase1-7
		gunModel[10].setRotationPoint(40F, -10.5F, -2.5F);

		gunModel[11].addBox(0F, 0F, 0F, 4, 5, 5, 0F); // Import barrelBase1-8
		gunModel[11].setRotationPoint(40F, -12.4F, -4.2F);
		gunModel[11].rotateAngleX = -0.54105207F;

		gunModel[12].addBox(0F, 0F, 0F, 4, 5, 5, 0F); // Import barrelBase1-9
		gunModel[12].setRotationPoint(40F, -12.5F, -9.3F);
		gunModel[12].rotateAngleX = 0.52359878F;

		gunModel[13].addBox(0F, 0F, 0F, 4, 9, 9, 0F); // Import barrelBase2
		gunModel[13].setRotationPoint(40F, -19.5F, -4.5F);

		gunModel[14].addBox(0F, 0F, 0F, 3, 3, 4, 0F); // Import barrelBolt1
		gunModel[14].setRotationPoint(5F, -23.5F, -2F);

		gunModel[15].addBox(0F, 0F, 0F, 20, 2, 3, 0F); // Import barrelBolt2
		gunModel[15].setRotationPoint(8F, -22.5F, -1.5F);

		gunModel[16].addShapeBox(0F, 0F, 0F, 20, 1, 3, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import barrelBolt2-2
		gunModel[16].setRotationPoint(8F, -23.5F, -1.5F);

		gunModel[17].addBox(0F, 0F, 0F, 18, 15, 8, 0F); // Import body1
		gunModel[17].setRotationPoint(-13F, -24F, -4F);

		gunModel[18].addBox(0F, 0F, 0F, 35, 12, 8, 0F); // Import body2
		gunModel[18].setRotationPoint(5F, -21F, -4F);

		gunModel[19].addBox(0F, 0F, 0F, 18, 3, 8, 0F); // Import body3
		gunModel[19].setRotationPoint(-7F, -9F, -4F);

		gunModel[20].addBox(0F, 0F, 0F, 18, 3, 8, 0F); // Import body3-2
		gunModel[20].setRotationPoint(22F, -9F, -4F);

		gunModel[21].addBox(0F, 0F, 0F, 3, 10, 12, 0F); // Import body4
		gunModel[21].setRotationPoint(37F, -20F, -6F);

		gunModel[22].addBox(0F, 0F, 0F, 8, 3, 8, 0F); // Import body5
		gunModel[22].setRotationPoint(32F, -24F, -4F);

		gunModel[23].addShapeBox(0F, 0F, 0F, 12, 2, 9, 0F, 0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F); // Import boiler1
		gunModel[23].setRotationPoint(25F, -10F, -14F);

		gunModel[24].addBox(0F, 1.5F, 0F, 12, 3, 9, 0F); // Import boiler2
		gunModel[24].setRotationPoint(25F, -9F, -14F);

		gunModel[25].addShapeBox(0F, 5F, 0F, 12, 2, 9, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F); // Import boiler3
		gunModel[25].setRotationPoint(25F, -9F, -14F);

		gunModel[26].addShapeBox(0F, 0F, 0F, 2, 2, 10, 0F, 0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F); // Import boiler4
		gunModel[26].setRotationPoint(37F, -10.5F, -14.5F);

		gunModel[27].addShapeBox(0F, 0F, 0F, 2, 2, 10, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F); // Import boiler4-2
		gunModel[27].setRotationPoint(37F, -3.5F, -14.5F);

		gunModel[28].addShapeBox(0F, 0F, 0F, 2, 2, 10, 0F, 0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F); // Import boiler4-3
		gunModel[28].setRotationPoint(23F, -10.5F, -14.5F);

		gunModel[29].addShapeBox(0F, 0F, 0F, 2, 2, 10, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F); // Import boiler4-4
		gunModel[29].setRotationPoint(23F, -3.5F, -14.5F);

		gunModel[30].addBox(0F, 0F, 0F, 2, 4, 10, 0F); // Import boiler5
		gunModel[30].setRotationPoint(37F, -8F, -14.5F);

		gunModel[31].addBox(0F, 0F, 0F, 2, 4, 10, 0F); // Import boiler5-2
		gunModel[31].setRotationPoint(23F, -8F, -14.5F);

		gunModel[32].addBox(0F, 0F, 0F, 8, 2, 4, 0F); // Import boiler6
		gunModel[32].setRotationPoint(27F, -10F, -8F);

		gunModel[33].addBox(0F, 0F, 0F, 3, 2, 7, 0F); // Import boiler7
		gunModel[33].setRotationPoint(33F, -12.5F, -11F);

		gunModel[34].addShapeBox(0F, 0F, 0F, 3, 1, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import boiler8
		gunModel[34].setRotationPoint(33F, -13.5F, -11F);

		gunModel[35].addBox(0F, 0F, 0F, 4, 4, 9, 0F); // Import caseHinge
		gunModel[35].setRotationPoint(28F, -25F, -4.5F);

		gunModel[36].addBox(0F, 0F, 0F, 23, 1, 1, 0F); // Import caseRail1
		gunModel[36].setRotationPoint(5F, -21.5F, -3.5F);

		gunModel[37].addBox(0F, 0F, 0F, 23, 1, 1, 0F); // Import caseRail1-2
		gunModel[37].setRotationPoint(5F, -21.5F, 2.5F);

		gunModel[38].addShapeBox(0F, 0F, 0F, 6, 2, 1, 0F, -1.75F, 0F, 0F, -1.75F, 0F, 0F, -1.75F, 0F, 0F, -1.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import gearPart1
		gunModel[38].setRotationPoint(28F, -16.5F, 4.5F);

		gunModel[39].addShapeBox(0F, 0F, 0F, 6, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import gearPart2
		gunModel[39].setRotationPoint(28F, -14.5F, 4.5F);

		gunModel[40].addShapeBox(0F, 0F, 0F, 6, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.75F, 0F, 0F, -1.75F, 0F, 0F, -1.75F, 0F, 0F, -1.75F, 0F, 0F); // Import gearPart3
		gunModel[40].setRotationPoint(28F, -12.5F, 4.5F);

		gunModel[41].addShapeBox(0F, 0F, 0F, 6, 2, 1, 0F, -2F, -0.75F, 0F, -2F, -0.75F, 0F, -2F, -0.75F, 0F, -2F, -0.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import gearPart4
		gunModel[41].setRotationPoint(28F, -18.5F, 4F);

		gunModel[42].addShapeBox(0F, 0F, 0F, 10, 2, 1, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Import gearPart5
		gunModel[42].setRotationPoint(26F, -16.5F, 4F);

		gunModel[43].addShapeBox(0F, 0F, 0F, 8, 2, 1, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F); // Import gearPart6
		gunModel[43].setRotationPoint(27F, -14.5F, 4F);

		gunModel[44].addShapeBox(0F, 0F, 0F, 10, 2, 1, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F); // Import gearPart7
		gunModel[44].setRotationPoint(26F, -12.5F, 4F);

		gunModel[45].addShapeBox(0F, 0F, 0F, 6, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, -0.75F, 0F, -2F, -0.75F, 0F, -2F, -0.75F, 0F, -2F, -0.75F, 0F); // Import gearPart8
		gunModel[45].setRotationPoint(28F, -10.5F, 4F);

		gunModel[46].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import gearStub1
		gunModel[46].setRotationPoint(30F, -18.7F, 4F);

		gunModel[47].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F); // Import gearStub2
		gunModel[47].setRotationPoint(30F, -9.3F, 4F);

		gunModel[48].addShapeBox(0F, 0F, 0F, 1, 2, 1, 0F, 0.25F, -0.3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.25F, -0.3F, 0F, 0.25F, -0.3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.25F, -0.3F, 0F); // Import gearStub3
		gunModel[48].setRotationPoint(25.5F, -14.5F, 4F);

		gunModel[49].addShapeBox(0F, 0F, 0F, 1, 2, 1, 0F, 0F, 0F, 0F, 0.25F, -0.3F, 0F, 0.25F, -0.3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.25F, -0.3F, 0F, 0.25F, -0.3F, 0F, 0F, 0F, 0F); // Import gearStub4
		gunModel[49].setRotationPoint(35.5F, -14.5F, 4F);

		gunModel[50].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import gearStub5
		gunModel[50].setRotationPoint(27F, -16.7F, 4F);
		gunModel[50].rotateAngleZ = 0.6981317F;

		gunModel[51].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F); // Import gearStub6
		gunModel[51].setRotationPoint(33.2F, -10F, 4F);
		gunModel[51].rotateAngleZ = 0.78539816F;

		gunModel[52].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import gearStub7
		gunModel[52].setRotationPoint(34F, -17.8F, 4F);
		gunModel[52].rotateAngleZ = -0.78539816F;

		gunModel[53].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F); // Import gearStub8
		gunModel[53].setRotationPoint(27.3F, -11.5F, 4F);
		gunModel[53].rotateAngleZ = -0.78539816F;

		gunModel[54].addBox(0F, 0F, 0F, 5, 3, 8, 0F); // Import grip1
		gunModel[54].setRotationPoint(-12F, -9F, -4F);

		gunModel[55].addShapeBox(0F, 0F, 0F, 12, 18, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, -2F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 4F, -2F, 0F); // Import grip2
		gunModel[55].setRotationPoint(-12F, -6F, -4F);

		gunModel[56].addBox(0F, 0F, 0F, 3, 1, 6, 0F); // Import ironSightBase
		gunModel[56].setRotationPoint(36F, -25F, -3F);

		gunModel[57].addShapeBox(0F, 0F, 0F, 6, 2, 1, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import meter1
		gunModel[57].setRotationPoint(-1.5F, -19.5F, -5F);

		gunModel[58].addShapeBox(0F, 0F, 0F, 6, 2, 1, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import meter1-2
		gunModel[58].setRotationPoint(-8.5F, -18F, -5F);

		gunModel[59].addBox(0F, 0F, 0F, 6, 2, 1, 0F); // Import meter2
		gunModel[59].setRotationPoint(-1.5F, -17.5F, -5F);

		gunModel[60].addBox(0F, 0F, 0F, 6, 2, 1, 0F); // Import meter2-2
		gunModel[60].setRotationPoint(-8.5F, -16F, -5F);

		gunModel[61].addShapeBox(0F, 0F, 0F, 6, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F); // Import meter3
		gunModel[61].setRotationPoint(-1.5F, -15.5F, -5F);

		gunModel[62].addShapeBox(0F, 0F, 0F, 6, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F); // Import meter3-2
		gunModel[62].setRotationPoint(-8.5F, -14F, -5F);

		gunModel[63].addBox(0F, 0F, 0F, 15, 1, 4, 0F); // Import rail1
		gunModel[63].setRotationPoint(-11F, -25F, -2F);

		gunModel[64].addBox(0F, 0F, 0F, 15, 1, 6, 0F); // Import rail2
		gunModel[64].setRotationPoint(-11F, -26F, -3F);

		gunModel[65].addShapeBox(0F, 0F, 0F, 15, 1, 1, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import rail3
		gunModel[65].setRotationPoint(-11F, -27F, 2F);

		gunModel[66].addShapeBox(0F, 0F, 0F, 15, 1, 1, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import rail3-2
		gunModel[66].setRotationPoint(-11F, -27F, -3F);

		gunModel[67].addShapeBox(0F, 0F, 0F, 3, 17, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Import steamPipe1
		gunModel[67].setRotationPoint(33F, -30.5F, -5F);

		gunModel[68].addBox(0F, 0F, 0F, 3, 17, 1, 0F); // Import steamPipe1-2
		gunModel[68].setRotationPoint(33F, -30.5F, -6F);

		gunModel[69].addShapeBox(0F, 0F, 0F, 3, 17, 1, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import steamPipe1-3
		gunModel[69].setRotationPoint(33F, -30.5F, -7F);

		gunModel[70].addShapeBox(0F, 0F, 0F, 4, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Import steamPipe2
		gunModel[70].setRotationPoint(32.5F, -34.5F, -4.5F);

		gunModel[71].addShapeBox(0F, 0F, 0F, 4, 4, 1, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import steamPipe2-2
		gunModel[71].setRotationPoint(32.5F, -34.5F, -7.5F);

		gunModel[72].addBox(0F, 0F, 0F, 4, 4, 2, 0F); // Import steamPipe3
		gunModel[72].setRotationPoint(32.5F, -34.5F, -6.5F);

		gunModel[73].addShapeBox(0F, 0F, 0F, 30, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Import steamPipe4
		gunModel[73].setRotationPoint(3F, -11.5F, -7F);

		gunModel[74].addBox(0F, 0F, 0F, 30, 1, 3, 0F); // Import steamPipe4-2
		gunModel[74].setRotationPoint(3F, -12.5F, -7F);

		gunModel[75].addShapeBox(0F, 0F, 0F, 30, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import steamPipe4-3
		gunModel[75].setRotationPoint(3F, -13.5F, -7F);

		gunModel[76].addBox(0F, 0F, 0F, 3, 3, 3, 0F); // Import steamPipe5
		gunModel[76].setRotationPoint(0F, -13.5F, -7F);


		defaultScopeModel = new ModelRendererTurbo[3];
		defaultScopeModel[0] = new ModelRendererTurbo(this, 16, 6, textureX, textureY); // Import ironSight1
		defaultScopeModel[1] = new ModelRendererTurbo(this, 16, 6, textureX, textureY); // Import ironSight2
		defaultScopeModel[2] = new ModelRendererTurbo(this, 16, 1, textureX, textureY); // Import ironSight3

		defaultScopeModel[0].addBox(0F, 0F, 0F, 2, 6, 1, 0F); // Import ironSight1
		defaultScopeModel[0].setRotationPoint(36F, -31F, 1.5F);

		defaultScopeModel[1].addBox(0F, 0F, 0F, 2, 6, 1, 0F); // Import ironSight2
		defaultScopeModel[1].setRotationPoint(36F, -31F, -2.5F);

		defaultScopeModel[2].addBox(0F, 0F, 0F, 2, 3, 1, 0F); // Import ironSight3
		defaultScopeModel[2].setRotationPoint(36F, -28F, -0.5F);


		defaultStockModel = new ModelRendererTurbo[7];
		defaultStockModel[0] = new ModelRendererTurbo(this, 77, 146, textureX, textureY); // Import stockBolt
		defaultStockModel[1] = new ModelRendererTurbo(this, 77, 122, textureX, textureY); // Import stockPart1
		defaultStockModel[2] = new ModelRendererTurbo(this, 107, 121, textureX, textureY); // Import stockPart2
		defaultStockModel[3] = new ModelRendererTurbo(this, 113, 90, textureX, textureY); // Import stockPart3
		defaultStockModel[4] = new ModelRendererTurbo(this, 77, 64, textureX, textureY); // Import stockPart4
		defaultStockModel[5] = new ModelRendererTurbo(this, 98, 90, textureX, textureY); // Import stockPart5
		defaultStockModel[6] = new ModelRendererTurbo(this, 77, 90, textureX, textureY); // Import stockPart6

		defaultStockModel[0].addBox(0F, 0F, 0F, 2, 9, 9, 0F); // Import stockBolt
		defaultStockModel[0].setRotationPoint(-14F, -21F, -4.5F);

		defaultStockModel[1].addShapeBox(0F, 0F, 0F, 6, 15, 8, 0F, 0F, -6F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -6F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F); // Import stockPart1
		defaultStockModel[1].setRotationPoint(-19F, -24F, -4F);

		defaultStockModel[2].addShapeBox(0F, 0F, 0F, 2, 11, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F); // Import stockPart2
		defaultStockModel[2].setRotationPoint(-21F, -18F, -4F);

		defaultStockModel[3].addShapeBox(0F, 0F, 0F, 5, 12, 8, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F); // Import stockPart3
		defaultStockModel[3].setRotationPoint(-26F, -18F, -4F);

		defaultStockModel[4].addShapeBox(0F, 0F, 0F, 24, 17, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 6F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 6F, 0F); // Import stockPart4
		defaultStockModel[4].setRotationPoint(-50F, -21F, -4F);

		defaultStockModel[5].addBox(0F, 0F, 0F, 1, 21, 6, 0F); // Import stockPart5
		defaultStockModel[5].setRotationPoint(-51F, -20F, -3F);

		defaultStockModel[6].addBox(0F, 0F, 0F, 2, 23, 8, 0F); // Import stockPart6
		defaultStockModel[6].setRotationPoint(-53F, -21F, -4F);


		ammoModel = new ModelRendererTurbo[5];
		ammoModel[0] = new ModelRendererTurbo(this, 42, 123, textureX, textureY); // Import ammo1
		ammoModel[1] = new ModelRendererTurbo(this, 1, 186, textureX, textureY); // Import ammo2
		ammoModel[2] = new ModelRendererTurbo(this, 1, 202, textureX, textureY); // Import ammo3
		ammoModel[3] = new ModelRendererTurbo(this, 1, 128, textureX, textureY); // Import ammo4
		ammoModel[4] = new ModelRendererTurbo(this, 1, 159, textureX, textureY); // Import ammo5

		ammoModel[0].addBox(0F, 0F, 0F, 9, 5, 4, 0F); // Import ammo1
		ammoModel[0].setRotationPoint(12F, -20F, 4F);

		ammoModel[1].addShapeBox(0F, 0F, 0F, 11, 3, 12, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F); // Import ammo2
		ammoModel[1].setRotationPoint(11F, -15.5F, 4F);

		ammoModel[2].addBox(0F, 0F, 0F, 10, 1, 12, 0F); // Import ammo3
		ammoModel[2].setRotationPoint(11.5F, -14F, 3.5F);

		ammoModel[3].addBox(0F, 0F, 0F, 11, 18, 12, 0F); // Import ammo4
		ammoModel[3].setRotationPoint(11F, -13F, 4F);

		ammoModel[4].addBox(0F, 0F, 0F, 11, 14, 12, 0F); // Import ammo5
		ammoModel[4].setRotationPoint(11F, -9F, -8F);


		pumpModel = new ModelRendererTurbo[1];
		pumpModel[0] = new ModelRendererTurbo(this, 56, 21, textureX, textureY); // Import slider

		pumpModel[0].addBox(0F, 0F, 0F, 2, 2, 4, 0F); // Import slider
		pumpModel[0].setRotationPoint(20F, -21F, -8F);


		minigunBarrelModel = new ModelRendererTurbo[24];
		minigunBarrelModel[0] = new ModelRendererTurbo(this, 69, 30, textureX, textureY); // Import barrelBottomA
		minigunBarrelModel[1] = new ModelRendererTurbo(this, 69, 30, textureX, textureY); // Import barrelBottomB
		minigunBarrelModel[2] = new ModelRendererTurbo(this, 69, 30, textureX, textureY); // Import barrelBottomC
		minigunBarrelModel[3] = new ModelRendererTurbo(this, 69, 30, textureX, textureY); // Import barrelBottomD
		minigunBarrelModel[4] = new ModelRendererTurbo(this, 69, 21, textureX, textureY); // Import barrelMiddleA
		minigunBarrelModel[5] = new ModelRendererTurbo(this, 69, 21, textureX, textureY); // Import barrelMiddleB
		minigunBarrelModel[6] = new ModelRendererTurbo(this, 69, 21, textureX, textureY); // Import barrelMiddleC
		minigunBarrelModel[7] = new ModelRendererTurbo(this, 69, 21, textureX, textureY); // Import barrelMiddleD
		minigunBarrelModel[8] = new ModelRendererTurbo(this, 69, 12, textureX, textureY); // Import barrelTopA
		minigunBarrelModel[9] = new ModelRendererTurbo(this, 69, 12, textureX, textureY); // Import barrelTopB
		minigunBarrelModel[10] = new ModelRendererTurbo(this, 69, 12, textureX, textureY); // Import barrelTopC
		minigunBarrelModel[11] = new ModelRendererTurbo(this, 69, 12, textureX, textureY); // Import barrelTopD
		minigunBarrelModel[12] = new ModelRendererTurbo(this, 69, 48, textureX, textureY); // Import muzzleBottomA
		minigunBarrelModel[13] = new ModelRendererTurbo(this, 69, 48, textureX, textureY); // Import muzzleBottomB
		minigunBarrelModel[14] = new ModelRendererTurbo(this, 69, 48, textureX, textureY); // Import muzzleBottomC
		minigunBarrelModel[15] = new ModelRendererTurbo(this, 69, 48, textureX, textureY); // Import muzzleBottomD
		minigunBarrelModel[16] = new ModelRendererTurbo(this, 100, 39, textureX, textureY); // Import muzzleMiddleA
		minigunBarrelModel[17] = new ModelRendererTurbo(this, 100, 39, textureX, textureY); // Import muzzleMiddleB
		minigunBarrelModel[18] = new ModelRendererTurbo(this, 100, 39, textureX, textureY); // Import muzzleMiddleC
		minigunBarrelModel[19] = new ModelRendererTurbo(this, 100, 39, textureX, textureY); // Import muzzleMiddleD
		minigunBarrelModel[20] = new ModelRendererTurbo(this, 69, 39, textureX, textureY); // Import muzzleTopA
		minigunBarrelModel[21] = new ModelRendererTurbo(this, 69, 39, textureX, textureY); // Import muzzleTopB
		minigunBarrelModel[22] = new ModelRendererTurbo(this, 69, 39, textureX, textureY); // Import muzzleTopC
		minigunBarrelModel[23] = new ModelRendererTurbo(this, 69, 39, textureX, textureY); // Import muzzleTopD

		minigunBarrelModel[0].addShapeBox(0F, -5F, -3F, 30, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Import barrelBottomA
		minigunBarrelModel[0].setRotationPoint(44F, -15F, 0F);

		minigunBarrelModel[1].addShapeBox(0F, 1F, 3F, 30, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Import barrelBottomB
		minigunBarrelModel[1].setRotationPoint(44F, -15F, 0F);

		minigunBarrelModel[2].addShapeBox(0F, 7F, -3F, 30, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Import barrelBottomC
		minigunBarrelModel[2].setRotationPoint(44F, -15F, 0F);

		minigunBarrelModel[3].addShapeBox(0F, 1F, -9F, 30, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Import barrelBottomD
		minigunBarrelModel[3].setRotationPoint(44F, -15F, 0F);

		minigunBarrelModel[4].addBox(0F, -7F, -3F, 30, 2, 6, 0F); // Import barrelMiddleA
		minigunBarrelModel[4].setRotationPoint(44F, -15F, 0F);

		minigunBarrelModel[5].addBox(0F, -1F, 3F, 30, 2, 6, 0F); // Import barrelMiddleB
		minigunBarrelModel[5].setRotationPoint(44F, -15F, 0F);

		minigunBarrelModel[6].addBox(0F, 5F, -3F, 30, 2, 6, 0F); // Import barrelMiddleC
		minigunBarrelModel[6].setRotationPoint(44F, -15F, 0F);

		minigunBarrelModel[7].addBox(0F, -1F, -9F, 30, 2, 6, 0F); // Import barrelMiddleD
		minigunBarrelModel[7].setRotationPoint(44F, -15F, 0F);

		minigunBarrelModel[8].addShapeBox(0F, -9F, -3F, 30, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import barrelTopA
		minigunBarrelModel[8].setRotationPoint(44F, -15F, 0F);

		minigunBarrelModel[9].addShapeBox(0F, -3F, 3F, 30, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import barrelTopB
		minigunBarrelModel[9].setRotationPoint(44F, -15F, 0F);

		minigunBarrelModel[10].addShapeBox(0F, 3F, -3F, 30, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import barrelTopC
		minigunBarrelModel[10].setRotationPoint(44F, -15F, 0F);

		minigunBarrelModel[11].addShapeBox(0F, -3F, -9F, 30, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import barrelTopD
		minigunBarrelModel[11].setRotationPoint(44F, -15F, 0F);

		minigunBarrelModel[12].addShapeBox(30F, -4F, -3.5F, 8, 1, 7, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F); // Import muzzleBottomA
		minigunBarrelModel[12].setRotationPoint(44F, -15F, 0F);

		minigunBarrelModel[13].addShapeBox(30F, 2F, 2.5F, 8, 1, 7, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F); // Import muzzleBottomB
		minigunBarrelModel[13].setRotationPoint(44F, -15F, 0F);

		minigunBarrelModel[14].addShapeBox(30F, 8F, -3.5F, 8, 1, 7, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F); // Import muzzleBottomC
		minigunBarrelModel[14].setRotationPoint(44F, -15F, 0F);

		minigunBarrelModel[15].addShapeBox(30F, 2F, -9.5F, 8, 1, 7, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F); // Import muzzleBottomD
		minigunBarrelModel[15].setRotationPoint(44F, -15F, 0F);

		minigunBarrelModel[16].addBox(30F, -7.5F, -3.5F, 8, 3, 7, 0F); // Import muzzleMiddleA
		minigunBarrelModel[16].setRotationPoint(44F, -15F, 0F);

		minigunBarrelModel[17].addBox(30F, -1.5F, 2.5F, 8, 3, 7, 0F); // Import muzzleMiddleB
		minigunBarrelModel[17].setRotationPoint(44F, -15F, 0F);

		minigunBarrelModel[18].addBox(30F, 4.5F, -3.5F, 8, 3, 7, 0F); // Import muzzleMiddleC
		minigunBarrelModel[18].setRotationPoint(44F, -15F, 0F);

		minigunBarrelModel[19].addBox(30F, -1.5F, -9.5F, 8, 3, 7, 0F); // Import muzzleMiddleD
		minigunBarrelModel[19].setRotationPoint(44F, -15F, 0F);

		minigunBarrelModel[20].addShapeBox(30F, -9F, -3.5F, 8, 1, 7, 0F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F); // Import muzzleTopA
		minigunBarrelModel[20].setRotationPoint(44F, -15F, 0F);

		minigunBarrelModel[21].addShapeBox(30F, -3F, 2.5F, 8, 1, 7, 0F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F); // Import muzzleTopB
		minigunBarrelModel[21].setRotationPoint(44F, -15F, 0F);

		minigunBarrelModel[22].addShapeBox(30F, 3F, -3.5F, 8, 1, 7, 0F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F); // Import muzzleTopC
		minigunBarrelModel[22].setRotationPoint(44F, -15F, 0F);

		minigunBarrelModel[23].addShapeBox(30F, -3F, -9.5F, 8, 1, 7, 0F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F); // Import muzzleTopD
		minigunBarrelModel[23].setRotationPoint(44F, -15F, 0F);


		breakActionModel = new ModelRendererTurbo[3];
		breakActionModel[0] = new ModelRendererTurbo(this, 144, 64, textureX, textureY); // Import case
		breakActionModel[1] = new ModelRendererTurbo(this, 144, 76, textureX, textureY); // Import caseTop
		breakActionModel[2] = new ModelRendererTurbo(this, 144, 102, textureX, textureY); // Import lidCase

		breakActionModel[0].addBox(-25F, 0F, -4F, 25, 3, 8, 0F); // Import case
		breakActionModel[0].setRotationPoint(30F, -24F, 0F);

		breakActionModel[1].addShapeBox(-22F, -0.5F, -3.5F, 20, 1, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import caseTop
		breakActionModel[1].setRotationPoint(30F, -24F, 0F);

		breakActionModel[2].addBox(-26F, -1F, -3F, 2, 1, 6, 0F); // Import lidCase
		breakActionModel[2].setRotationPoint(30F, -24F, 0F);

		scopeAttachPoint = new Vector3f(-3F /16F, 24F /16F, 0F /16F);

		gunSlideDistance = 0F;
		animationType = EnumAnimationType.CUSTOM;

		/* ----Start of Reload Block---- */
		/* Reload Name: Amended Side Clip */
		rotateClipHorizontal = 120F;
		rotateClipVertical = 60F;
		translateClip = new Vector3f(0.5F, 0F, 0F);
		/* ----End of Reload Block---- */

		minigunBarrelOrigin = new Vector3f(0F, 0.85F, 0F);
		barrelBreakPoint = new Vector3f(2F, 1.25F, 0F);
		breakAngle = 45F;

		pumpTime = 5;
		pumpDelayAfterReload = 95;
		pumpHandleDistance = 1F;


		translateAll(0F, 0F, 0F);


		flipAll();
	}
}