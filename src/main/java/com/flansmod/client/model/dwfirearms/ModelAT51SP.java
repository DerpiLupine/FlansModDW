package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelAT51SP extends ModelGun
{
	int textureX = 512;
	int textureY = 256;

	public ModelAT51SP()
	{
		gunModel = new ModelRendererTurbo[47];
		gunModel[0] = new ModelRendererTurbo(this, 1, 40, textureX, textureY); // barrelBottom
		gunModel[1] = new ModelRendererTurbo(this, 1, 29, textureX, textureY); // barrelMiddle
		gunModel[2] = new ModelRendererTurbo(this, 48, 40, textureX, textureY); // barrelTop
		gunModel[3] = new ModelRendererTurbo(this, 96, 26, textureX, textureY); // body1
		gunModel[4] = new ModelRendererTurbo(this, 133, 67, textureX, textureY); // body10
		gunModel[5] = new ModelRendererTurbo(this, 96, 39, textureX, textureY); // body11
		gunModel[6] = new ModelRendererTurbo(this, 133, 54, textureX, textureY); // body12
		gunModel[7] = new ModelRendererTurbo(this, 121, 118, textureX, textureY); // body13
		gunModel[8] = new ModelRendererTurbo(this, 121, 118, textureX, textureY); // body13-2
		gunModel[9] = new ModelRendererTurbo(this, 121, 112, textureX, textureY); // body14
		gunModel[10] = new ModelRendererTurbo(this, 121, 112, textureX, textureY); // body14-2
		gunModel[11] = new ModelRendererTurbo(this, 120, 98, textureX, textureY); // body15
		gunModel[12] = new ModelRendererTurbo(this, 120, 98, textureX, textureY); // body15-2
		gunModel[13] = new ModelRendererTurbo(this, 131, 87, textureX, textureY); // body16
		gunModel[14] = new ModelRendererTurbo(this, 96, 87, textureX, textureY); // body17
		gunModel[15] = new ModelRendererTurbo(this, 96, 80, textureX, textureY); // body18
		gunModel[16] = new ModelRendererTurbo(this, 125, 98, textureX, textureY); // body19
		gunModel[17] = new ModelRendererTurbo(this, 96, 58, textureX, textureY); // body2
		gunModel[18] = new ModelRendererTurbo(this, 96, 1, textureX, textureY); // body3
		gunModel[19] = new ModelRendererTurbo(this, 96, 94, textureX, textureY); // body4
		gunModel[20] = new ModelRendererTurbo(this, 96, 121, textureX, textureY); // body5
		gunModel[21] = new ModelRendererTurbo(this, 133, 39, textureX, textureY); // body6
		gunModel[22] = new ModelRendererTurbo(this, 121, 121, textureX, textureY); // body7
		gunModel[23] = new ModelRendererTurbo(this, 121, 94, textureX, textureY); // body8
		gunModel[24] = new ModelRendererTurbo(this, 146, 98, textureX, textureY); // body9
		gunModel[25] = new ModelRendererTurbo(this, 19, 9, textureX, textureY); // canister
		gunModel[26] = new ModelRendererTurbo(this, 26, 49, textureX, textureY); // gasBlock1
		gunModel[27] = new ModelRendererTurbo(this, 1, 56, textureX, textureY); // gasBlock2
		gunModel[28] = new ModelRendererTurbo(this, 1, 56, textureX, textureY); // gasBlock2-2
		gunModel[29] = new ModelRendererTurbo(this, 1, 49, textureX, textureY); // gasBlock3
		gunModel[30] = new ModelRendererTurbo(this, 1, 136, textureX, textureY); // grip1
		gunModel[31] = new ModelRendererTurbo(this, 1, 111, textureX, textureY); // grip2
		gunModel[32] = new ModelRendererTurbo(this, 24, 139, textureX, textureY); // grip3
		gunModel[33] = new ModelRendererTurbo(this, 12, 1, textureX, textureY); // ironSight1
		gunModel[34] = new ModelRendererTurbo(this, 22, 19, textureX, textureY); // ironSight2
		gunModel[35] = new ModelRendererTurbo(this, 42, 18, textureX, textureY); // ironSight6
		gunModel[36] = new ModelRendererTurbo(this, 1, 9, textureX, textureY); // ironSight7
		gunModel[37] = new ModelRendererTurbo(this, 1, 9, textureX, textureY); // ironSight7-2
		gunModel[38] = new ModelRendererTurbo(this, 6, 9, textureX, textureY); // ironSight8
		gunModel[39] = new ModelRendererTurbo(this, 96, 108, textureX, textureY); // meterScreen
		gunModel[40] = new ModelRendererTurbo(this, 48, 29, textureX, textureY); // muzzle1
		gunModel[41] = new ModelRendererTurbo(this, 48, 29, textureX, textureY); // muzzle1-2
		gunModel[42] = new ModelRendererTurbo(this, 48, 29, textureX, textureY); // muzzle1-3
		gunModel[43] = new ModelRendererTurbo(this, 43, 9, textureX, textureY); // rail1
		gunModel[44] = new ModelRendererTurbo(this, 43, 1, textureX, textureY); // rail2
		gunModel[45] = new ModelRendererTurbo(this, 43, 15, textureX, textureY); // rail3
		gunModel[46] = new ModelRendererTurbo(this, 43, 15, textureX, textureY); // rail3-2

		gunModel[0].addShapeBox(0F, 0F, 0F, 16, 1, 7, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F); // barrelBottom
		gunModel[0].setRotationPoint(33F, -13F, -3.5F);

		gunModel[1].addBox(0F, 0F, 0F, 16, 3, 7, 0F); // barrelMiddle
		gunModel[1].setRotationPoint(33F, -16.5F, -3.5F);

		gunModel[2].addShapeBox(0F, 0F, 0F, 16, 1, 7, 0F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F); // barrelTop
		gunModel[2].setRotationPoint(33F, -18F, -3.5F);

		gunModel[3].addShapeBox(0F, 0F, 0F, 36, 2, 10, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body1
		gunModel[3].setRotationPoint(-12F, -23F, -5F);

		gunModel[4].addShapeBox(0F, 0F, 0F, 8, 2, 10, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body10
		gunModel[4].setRotationPoint(25F, -22F, -5F);

		gunModel[5].addBox(0F, 0F, 0F, 8, 8, 10, 0F); // body11
		gunModel[5].setRotationPoint(25F, -20F, -5F);

		gunModel[6].addShapeBox(0F, 0F, 0F, 8, 2, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // body12
		gunModel[6].setRotationPoint(25F, -12F, -5F);

		gunModel[7].addShapeBox(0F, 0F, 0F, 6, 1, 1, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body13
		gunModel[7].setRotationPoint(2F, -15F, 5F);

		gunModel[8].addShapeBox(0F, 0F, 0F, 6, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // body13-2
		gunModel[8].setRotationPoint(2F, -10F, 5F);

		gunModel[9].addBox(0F, 0F, 0F, 1, 4, 1, 0F); // body14
		gunModel[9].setRotationPoint(7F, -14F, 5F);

		gunModel[10].addBox(0F, 0F, 0F, 1, 4, 1, 0F); // body14-2
		gunModel[10].setRotationPoint(2F, -14F, 5F);

		gunModel[11].addShapeBox(0F, 0F, 0F, 1, 5, 1, 0F, 0F, -1.5F, -1F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, -1F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F); // body15
		gunModel[11].setRotationPoint(-12F, -21F, -6F);

		gunModel[12].addShapeBox(0F, 0F, 0F, 1, 5, 1, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, -1F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, -1F); // body15-2
		gunModel[12].setRotationPoint(-12F, -21F, 5F);

		gunModel[13].addShapeBox(0F, 0F, 0F, 10, 5, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F); // body16
		gunModel[13].setRotationPoint(-11F, -21F, 5F);

		gunModel[14].addShapeBox(0F, 0F, 0F, 16, 5, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F); // body17
		gunModel[14].setRotationPoint(8F, -21F, 5F);

		gunModel[15].addShapeBox(0F, 0F, 0F, 24, 5, 1, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body18
		gunModel[15].setRotationPoint(-11F, -21F, -6F);

		gunModel[16].addShapeBox(0F, 0F, 0F, 2, 5, 1, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body19
		gunModel[16].setRotationPoint(22F, -21F, -6F);

		gunModel[17].addBox(0F, 0F, 0F, 8, 11, 10, 0F); // body2
		gunModel[17].setRotationPoint(-12F, -21F, -5F);

		gunModel[18].addBox(0F, 0F, 0F, 28, 14, 10, 0F); // body3
		gunModel[18].setRotationPoint(-4F, -21F, -5F);

		gunModel[19].addShapeBox(0F, 0F, 0F, 1, 2, 11, 0F, 0F, 0.5F, -1.25F, 0F, 0.5F, -1.25F, 0F, 0.5F, -1.25F, 0F, 0.5F, -1.25F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body4
		gunModel[19].setRotationPoint(-13F, -23F, -5.5F);

		gunModel[20].addBox(0F, 0F, 0F, 1, 11, 11, 0F); // body5
		gunModel[20].setRotationPoint(-13F, -21F, -5.5F);

		gunModel[21].addBox(0F, 0F, 0F, 13, 3, 10, 0F); // body6
		gunModel[21].setRotationPoint(11F, -7F, -5F);

		gunModel[22].addBox(0F, 0F, 0F, 1, 11, 11, 0F); // body7
		gunModel[22].setRotationPoint(24F, -21F, -5.5F);

		gunModel[23].addShapeBox(0F, 0F, 0F, 1, 2, 11, 0F, 0F, 0.5F, -1.25F, 0F, 0.5F, -1.25F, 0F, 0.5F, -1.25F, 0F, 0.5F, -1.25F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body8
		gunModel[23].setRotationPoint(24F, -23F, -5.5F);

		gunModel[24].addShapeBox(0F, 0F, 0F, 8, 1, 8, 0F, 0F, 0.5F, -1F, 0F, 0.5F, -1F, 0F, 0.5F, -1F, 0F, 0.5F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body9
		gunModel[24].setRotationPoint(25F, -23F, -4F);

		gunModel[25].addShapeBox(0F, 0F, 0F, 9, 5, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F); // canister
		gunModel[25].setRotationPoint(-1F, -21F, 5F);

		gunModel[26].addBox(0F, 0F, 0F, 4, 1, 2, 0F); // gasBlock1
		gunModel[26].setRotationPoint(36F, -19F, -1F);

		gunModel[27].addShapeBox(0F, 0F, 0F, 8, 1, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // gasBlock2
		gunModel[27].setRotationPoint(33F, -23F, -2F);

		gunModel[28].addShapeBox(0F, 0F, 0F, 8, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // gasBlock2-2
		gunModel[28].setRotationPoint(33F, -20F, -2F);

		gunModel[29].addBox(0F, 0F, 0F, 8, 2, 4, 0F); // gasBlock3
		gunModel[29].setRotationPoint(33F, -22F, -2F);

		gunModel[30].addBox(0F, 0F, 0F, 3, 3, 8, 0F); // grip1
		gunModel[30].setRotationPoint(-7F, -10F, -4F);

		gunModel[31].addShapeBox(0F, 0F, 0F, 11, 16, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, -2F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 4F, -2F, 0F); // grip2
		gunModel[31].setRotationPoint(-7F, -7F, -4F);

		gunModel[32].addBox(0F, 0F, 0F, 9, 2, 6, 0F); // grip3
		gunModel[32].setRotationPoint(-10F, 6.5F, -3F);
		gunModel[32].rotateAngleZ = -0.17453293F;

		gunModel[33].addBox(0F, 0F, 0F, 10, 2, 5, 0F); // ironSight1
		gunModel[33].setRotationPoint(-10F, -24.5F, -2.5F);

		gunModel[34].addShapeBox(0F, 0F, 0F, 7, 1, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F); // ironSight2
		gunModel[34].setRotationPoint(-7F, -25.5F, -2.5F);

		gunModel[35].addBox(0F, 0F, 0F, 1, 2, 3, 0F); // ironSight6
		gunModel[35].setRotationPoint(24F, -25.5F, -1.5F);

		gunModel[36].addBox(0F, 0F, 0F, 1, 4, 1, 0F); // ironSight7
		gunModel[36].setRotationPoint(24F, -27.5F, 1.5F);

		gunModel[37].addBox(0F, 0F, 0F, 1, 4, 1, 0F); // ironSight7-2
		gunModel[37].setRotationPoint(24F, -27.5F, -2.5F);

		gunModel[38].addShapeBox(0F, 0F, 0F, 4, 3, 2, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // ironSight8
		gunModel[38].setRotationPoint(36F, -26F, -1F);

		gunModel[39].addShapeBox(0F, 0F, 0F, 11, 11, 1, 0F, 0F, 0F, 0F, -7F, 0F, 0F, -7F, 0F, 0F, 0F, 0F, 0F, 0F, -7F, 0F, -7F, -7F, 0F, -7F, -7F, 0F, 0F, -7F, 0F); // meterScreen
		gunModel[39].setRotationPoint(3F, -14F, 4.7F);

		gunModel[40].addShapeBox(0F, 0F, 0F, 2, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // muzzle1
		gunModel[40].setRotationPoint(49F, -18F, -3F);

		gunModel[41].addBox(0F, 0F, 0F, 2, 2, 6, 0F); // muzzle1-2
		gunModel[41].setRotationPoint(49F, -16F, -3F);

		gunModel[42].addShapeBox(0F, 0F, 0F, 2, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // muzzle1-3
		gunModel[42].setRotationPoint(49F, -14F, -3F);

		gunModel[43].addBox(0F, 0F, 0F, 18, 1, 4, 0F); // rail1
		gunModel[43].setRotationPoint(3F, -24F, -2F);

		gunModel[44].addBox(0F, 0F, 0F, 18, 1, 6, 0F); // rail2
		gunModel[44].setRotationPoint(3F, -25F, -3F);

		gunModel[45].addShapeBox(0F, 0F, 0F, 18, 1, 1, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // rail3
		gunModel[45].setRotationPoint(3F, -26F, 2F);

		gunModel[46].addShapeBox(0F, 0F, 0F, 18, 1, 1, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // rail3-2
		gunModel[46].setRotationPoint(3F, -26F, -3F);


		defaultScopeModel = new ModelRendererTurbo[4];
		defaultScopeModel[0] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // ironSight3
		defaultScopeModel[1] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // ironSight3-2
		defaultScopeModel[2] = new ModelRendererTurbo(this, 1, 15, textureX, textureY); // ironSight4
		defaultScopeModel[3] = new ModelRendererTurbo(this, 47, 19, textureX, textureY); // ironSight5

		defaultScopeModel[0].addBox(0F, 0F, 0F, 4, 6, 1, 0F); // ironSight3
		defaultScopeModel[0].setRotationPoint(-4.5F, -29F, -3.5F);

		defaultScopeModel[1].addBox(0F, 0F, 0F, 4, 6, 1, 0F); // ironSight3-2
		defaultScopeModel[1].setRotationPoint(-4.5F, -29F, 2.5F);

		defaultScopeModel[2].addBox(0F, 0F, 0F, 2, 2, 8, 0F); // ironSight4
		defaultScopeModel[2].setRotationPoint(-3.5F, -25F, -4F);

		defaultScopeModel[3].addShapeBox(0F, 0F, 0F, 4, 1, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F); // ironSight5
		defaultScopeModel[3].setRotationPoint(-4.5F, -30F, -2.5F);


		defaultStockModel = new ModelRendererTurbo[6];
		defaultStockModel[0] = new ModelRendererTurbo(this, 42, 99, textureX, textureY); // stock1
		defaultStockModel[1] = new ModelRendererTurbo(this, 42, 99, textureX, textureY); // stock1-2
		defaultStockModel[2] = new ModelRendererTurbo(this, 42, 99, textureX, textureY); // stock1-3
		defaultStockModel[3] = new ModelRendererTurbo(this, 42, 119, textureX, textureY); // stock2
		defaultStockModel[4] = new ModelRendererTurbo(this, 42, 119, textureX, textureY); // stock2-2
		defaultStockModel[5] = new ModelRendererTurbo(this, 42, 108, textureX, textureY); // stock3

		defaultStockModel[0].addShapeBox(0F, 0F, 0F, 3, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // stock1
		defaultStockModel[0].setRotationPoint(-16F, -18F, -3F);

		defaultStockModel[1].addBox(0F, 0F, 0F, 3, 2, 6, 0F); // stock1-2
		defaultStockModel[1].setRotationPoint(-16F, -16F, -3F);

		defaultStockModel[2].addShapeBox(0F, 0F, 0F, 3, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // stock1-3
		defaultStockModel[2].setRotationPoint(-16F, -14F, -3F);

		defaultStockModel[3].addShapeBox(0F, 0F, 0F, 2, 1, 7, 0F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F); // stock2
		defaultStockModel[3].setRotationPoint(-18F, -18F, -3.5F);

		defaultStockModel[4].addShapeBox(0F, 0F, 0F, 2, 1, 7, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F); // stock2-2
		defaultStockModel[4].setRotationPoint(-18F, -13F, -3.5F);

		defaultStockModel[5].addBox(0F, 0F, 0F, 2, 3, 7, 0F); // stock3
		defaultStockModel[5].setRotationPoint(-18F, -16.5F, -3.5F);


		ammoModel = new ModelRendererTurbo[6];
		ammoModel[0] = new ModelRendererTurbo(this, 1, 67, textureX, textureY); // ammo1
		ammoModel[1] = new ModelRendererTurbo(this, 47, 68, textureX, textureY); // ammo2
		ammoModel[2] = new ModelRendererTurbo(this, 30, 67, textureX, textureY); // ammo3
		ammoModel[3] = new ModelRendererTurbo(this, 1, 99, textureX, textureY); // ammo4
		ammoModel[4] = new ModelRendererTurbo(this, 12, 62, textureX, textureY); // bullet
		ammoModel[5] = new ModelRendererTurbo(this, 1, 62, textureX, textureY); // bulletTip

		ammoModel[0].addBox(0F, 0F, 0F, 8, 25, 6, 0F); // ammo1
		ammoModel[0].setRotationPoint(11.5F, -6F, -3F);

		ammoModel[1].addBox(0F, 0F, 0F, 2, 25, 5, 0F); // ammo2
		ammoModel[1].setRotationPoint(19.5F, -6F, -2.5F);

		ammoModel[2].addBox(0F, 0F, 0F, 2, 25, 6, 0F); // ammo3
		ammoModel[2].setRotationPoint(21.5F, -6F, -3F);

		ammoModel[3].addBox(0F, 0F, 0F, 13, 4, 7, 0F); // ammo4
		ammoModel[3].setRotationPoint(11F, 0F, -3.5F);

		ammoModel[4].addShapeBox(0F, 0F, 0F, 8, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // bullet
		ammoModel[4].setRotationPoint(12.5F, -7F, -1.5F);

		ammoModel[5].addShapeBox(0F, 0F, 0F, 2, 1, 3, 0F, 0F, 0F, -1F, 0F, -0.75F, -1F, 0F, -0.75F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F); // bulletTip
		ammoModel[5].setRotationPoint(20.5F, -7F, -1.5F);


		slideModel = new ModelRendererTurbo[1];
		slideModel[0] = new ModelRendererTurbo(this, 14, 16, textureX, textureY); // bolt

		slideModel[0].addBox(0F, 0F, 0F, 2, 3, 3, 0F); // bolt
		slideModel[0].setRotationPoint(20F, -20F, -8F);

		barrelAttachPoint = new Vector3f(51F /16F, 15F /16F, 0F /16F);
		stockAttachPoint = new Vector3f(-13F /16F, 15F /16F, 0F /16F);
		scopeAttachPoint = new Vector3f(13F /16F, 23F /16F, 0F /16F);

		gunSlideDistance = 0.5F;
		animationType = EnumAnimationType.BOTTOM_CLIP;


		translateAll(0F, 0F, 0F);


		flipAll();
	}
}
