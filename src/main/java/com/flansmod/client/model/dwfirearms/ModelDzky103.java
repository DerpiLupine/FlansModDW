package com.flansmod.client.model.dwfirearms; //Path where the model is located

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelDzky103 extends ModelGun //Same as Filename
{
	int textureX = 512;
	int textureY = 256;

	public ModelDzky103() //Same as Filename
	{
		gunModel = new ModelRendererTurbo[51];
		gunModel[0] = new ModelRendererTurbo(this, 120, 58, textureX, textureY); // Box 0
		gunModel[1] = new ModelRendererTurbo(this, 1, 28, textureX, textureY); // Box 4
		gunModel[2] = new ModelRendererTurbo(this, 32, 28, textureX, textureY); // Box 5
		gunModel[3] = new ModelRendererTurbo(this, 32, 41, textureX, textureY); // Box 6
		gunModel[4] = new ModelRendererTurbo(this, 1, 41, textureX, textureY); // Box 7
		gunModel[5] = new ModelRendererTurbo(this, 1, 55, textureX, textureY); // Box 8
		gunModel[6] = new ModelRendererTurbo(this, 34, 55, textureX, textureY); // Box 9
		gunModel[7] = new ModelRendererTurbo(this, 120, 92, textureX, textureY); // Box 11
		gunModel[8] = new ModelRendererTurbo(this, 120, 16, textureX, textureY); // Box 13
		gunModel[9] = new ModelRendererTurbo(this, 120, 26, textureX, textureY); // Box 14
		gunModel[10] = new ModelRendererTurbo(this, 120, 77, textureX, textureY); // Box 17
		gunModel[11] = new ModelRendererTurbo(this, 173, 77, textureX, textureY); // Box 18
		gunModel[12] = new ModelRendererTurbo(this, 120, 103, textureX, textureY); // Box 19
		gunModel[13] = new ModelRendererTurbo(this, 120, 16, textureX, textureY); // Box 20
		gunModel[14] = new ModelRendererTurbo(this, 120, 1, textureX, textureY); // Box 21
		gunModel[15] = new ModelRendererTurbo(this, 120, 8, textureX, textureY); // Box 22
		gunModel[16] = new ModelRendererTurbo(this, 120, 1, textureX, textureY); // Box 23
		gunModel[17] = new ModelRendererTurbo(this, 62, 12, textureX, textureY); // Box 24
		gunModel[18] = new ModelRendererTurbo(this, 150, 113, textureX, textureY); // Box 39
		gunModel[19] = new ModelRendererTurbo(this, 150, 130, textureX, textureY); // Box 6
		gunModel[20] = new ModelRendererTurbo(this, 120, 113, textureX, textureY); // Box 115
		gunModel[21] = new ModelRendererTurbo(this, 175, 117, textureX, textureY); // Box 116
		gunModel[22] = new ModelRendererTurbo(this, 150, 123, textureX, textureY); // Box 117
		gunModel[23] = new ModelRendererTurbo(this, 175, 113, textureX, textureY); // Box 118
		gunModel[24] = new ModelRendererTurbo(this, 184, 113, textureX, textureY); // Box 119
		gunModel[25] = new ModelRendererTurbo(this, 184, 120, textureX, textureY); // Box 120
		gunModel[26] = new ModelRendererTurbo(this, 129, 113, textureX, textureY); // Box 121
		gunModel[27] = new ModelRendererTurbo(this, 129, 121, textureX, textureY); // Box 122
		gunModel[28] = new ModelRendererTurbo(this, 175, 117, textureX, textureY); // Box 123
		gunModel[29] = new ModelRendererTurbo(this, 175, 113, textureX, textureY); // Box 124
		gunModel[30] = new ModelRendererTurbo(this, 120, 113, textureX, textureY); // Box 125
		gunModel[31] = new ModelRendererTurbo(this, 184, 120, textureX, textureY); // Box 126
		gunModel[32] = new ModelRendererTurbo(this, 184, 113, textureX, textureY); // Box 127
		gunModel[33] = new ModelRendererTurbo(this, 129, 113, textureX, textureY); // Box 128
		gunModel[34] = new ModelRendererTurbo(this, 129, 121, textureX, textureY); // Box 129
		gunModel[35] = new ModelRendererTurbo(this, 62, 12, textureX, textureY); // Box 25
		gunModel[36] = new ModelRendererTurbo(this, 209, 58, textureX, textureY); // Box 26
		gunModel[37] = new ModelRendererTurbo(this, 120, 37, textureX, textureY); // Box 31
		gunModel[38] = new ModelRendererTurbo(this, 120, 48, textureX, textureY); // Box 32
		gunModel[39] = new ModelRendererTurbo(this, 179, 45, textureX, textureY); // Box 36
		gunModel[40] = new ModelRendererTurbo(this, 179, 37, textureX, textureY); // Box 37
		gunModel[41] = new ModelRendererTurbo(this, 1, 23, textureX, textureY); // Box 1
		gunModel[42] = new ModelRendererTurbo(this, 1, 2, textureX, textureY); // Box 3
		gunModel[43] = new ModelRendererTurbo(this, 1, 2, textureX, textureY); // Box 4
		gunModel[44] = new ModelRendererTurbo(this, 1, 2, textureX, textureY); // Box 5
		gunModel[45] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 6
		gunModel[46] = new ModelRendererTurbo(this, 1, 13, textureX, textureY); // Box 7
		gunModel[47] = new ModelRendererTurbo(this, 62, 1, textureX, textureY); // Box 0
		gunModel[48] = new ModelRendererTurbo(this, 62, 1, textureX, textureY); // Box 1
		gunModel[49] = new ModelRendererTurbo(this, 62, 1, textureX, textureY); // Box 2
		gunModel[50] = new ModelRendererTurbo(this, 62, 1, textureX, textureY); // Box 3

		gunModel[0].addBox(0F, 0F, 0F, 36, 10, 8, 0F); // Box 0
		gunModel[0].setRotationPoint(-2F, -12.5F, -4F);

		gunModel[1].addShapeBox(0F, 0F, 0F, 9, 5, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 3F, 0F, 0F); // Box 4
		gunModel[1].setRotationPoint(-6F, -2.5F, -3F);

		gunModel[2].addShapeBox(0F, 0F, 0F, 2, 5, 6, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, -1F, -3F, 0F, -1F, 3F, 0F, 0F); // Box 5
		gunModel[2].setRotationPoint(3F, -2.5F, -3F);

		gunModel[3].addShapeBox(0F, 0F, 0F, 2, 7, 6, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 6
		gunModel[3].setRotationPoint(0F, 2.5F, -3F);

		gunModel[4].addShapeBox(0F, 0F, 0F, 9, 7, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F); // Box 7
		gunModel[4].setRotationPoint(-9F, 2.5F, -3F);

		gunModel[5].addBox(0F, 0F, 0F, 10, 2, 6, 0F); // Box 8
		gunModel[5].setRotationPoint(-10F, 9.5F, -3F);

		gunModel[6].addShapeBox(0F, 0F, 0F, 2, 2, 6, 0F, 0F, 0F, 0F, 0.5F, 0F, -1F, 0.5F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, -1F, 0.5F, 0F, -1F, 0F, 0F, 0F); // Box 9
		gunModel[6].setRotationPoint(0F, 9.5F, -3F);

		gunModel[7].addBox(0F, 0F, 0F, 18, 2, 8, 0F); // Box 11
		gunModel[7].setRotationPoint(16F, -2.5F, -4F);

		gunModel[8].addShapeBox(0F, 0F, 0F, 28, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 13
		gunModel[8].setRotationPoint(18F, -17.5F, -3.5F);

		gunModel[9].addBox(0F, 0F, 0F, 28, 3, 7, 0F); // Box 14
		gunModel[9].setRotationPoint(18F, -15.5F, -3.5F);

		gunModel[10].addBox(0F, 0F, 0F, 16, 5, 9, 0F); // Box 17
		gunModel[10].setRotationPoint(17F, -6.5F, -4.5F);

		gunModel[11].addBox(0F, 0F, 0F, 11, 7, 8, 0F); // Box 18
		gunModel[11].setRotationPoint(34F, -12.5F, -4F);

		gunModel[12].addShapeBox(0F, 0F, 0F, 11, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 19
		gunModel[12].setRotationPoint(34F, -5.5F, -4F);

		gunModel[13].addShapeBox(0F, 0F, 0F, 28, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 20
		gunModel[13].setRotationPoint(18F, -12.5F, -3.5F);

		gunModel[14].addShapeBox(0F, 0F, 0F, 50, 1, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F); // Box 21
		gunModel[14].setRotationPoint(46F, -13F, -2.5F);

		gunModel[15].addBox(0F, 0F, 0F, 50, 2, 5, 0F); // Box 22
		gunModel[15].setRotationPoint(46F, -15F, -2.5F);

		gunModel[16].addShapeBox(0F, 0F, 0F, 50, 1, 5, 0F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 23
		gunModel[16].setRotationPoint(46F, -16F, -2.5F);

		gunModel[17].addShapeBox(0F, 0F, 0F, 4, 2, 9, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 24
		gunModel[17].setRotationPoint(39F, -10.5F, -4.5F);

		gunModel[18].addShapeBox(0F, 0F, 0F, 6, 3, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F); // Box 39
		gunModel[18].setRotationPoint(38F, -5F, -3F);

		gunModel[19].addBox(0F, 0F, 0F, 3, 1, 3, 0F); // Box 6
		gunModel[19].setRotationPoint(39.5F, -2F, -1.5F);

		gunModel[20].addShapeBox(0F, 0F, 0F, 2, 15, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 5F, 0F, 0F, 5F, 0F, 0F, -5F, 0F, 0F, -5F); // Box 115
		gunModel[20].setRotationPoint(40F, 0.5F, -2.5F);

		gunModel[21].addShapeBox(0F, 0F, 0F, 2, 4, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 1.5F, 0F, 0F, 1.5F, 1F, 0F, -1.5F, 1F, 0F, -1.5F); // Box 116
		gunModel[21].setRotationPoint(40F, 0.5F, -3.5F);

		gunModel[22].addBox(0F, 0F, 0F, 5, 3, 3, 0F); // Box 117
		gunModel[22].setRotationPoint(38.5F, -1F, -1.5F);

		gunModel[23].addShapeBox(0F, 0F, 0F, 2, 1, 2, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F); // Box 118
		gunModel[23].setRotationPoint(40F, -0.5F, -3.5F);

		gunModel[24].addShapeBox(0F, 0F, 0F, 2, 4, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 1.5F, 0F, 0F, 1.5F, 1F, 0F, -1.5F, 1F, 0F, -1.5F); // Box 119
		gunModel[24].setRotationPoint(40F, 11.5F, -7F);

		gunModel[25].addShapeBox(0F, 0F, 0F, 2, 4, 2, 0F, 1F, 0F, -1.5F, 1F, 0F, -1.5F, 0F, 0F, 1.5F, 0F, 0F, 1.5F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 120
		gunModel[25].setRotationPoint(40F, 11.5F, -6.5F);

		gunModel[26].addBox(0F, 0F, 0F, 5, 2, 5, 0F); // Box 121
		gunModel[26].setRotationPoint(38.5F, 15.5F, -9F);

		gunModel[27].addShapeBox(0F, 0F, 0F, 5, 1, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F); // Box 122
		gunModel[27].setRotationPoint(38.5F, 17.5F, -9F);

		gunModel[28].addShapeBox(0F, 0F, 0F, 2, 4, 2, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, -1.5F, 1F, 0F, -1.5F, 0F, 0F, 1.5F, 0F, 0F, 1.5F); // Box 123
		gunModel[28].setRotationPoint(40F, 0.5F, 1.5F);

		gunModel[29].addShapeBox(0F, 0F, 0F, 2, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 124
		gunModel[29].setRotationPoint(40F, -0.5F, 1.5F);

		gunModel[30].addShapeBox(0F, 0F, 0F, 2, 15, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -5F, 0F, 0F, -5F, 0F, 0F, 5F, 0F, 0F, 5F); // Box 125
		gunModel[30].setRotationPoint(40F, 0.5F, 0.5F);

		gunModel[31].addShapeBox(0F, 0F, 0F, 2, 4, 2, 0F, 0F, 0F, 1.5F, 0F, 0F, 1.5F, 1F, 0F, -1.5F, 1F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F); // Box 126
		gunModel[31].setRotationPoint(40F, 11.5F, 4.5F);

		gunModel[32].addShapeBox(0F, 0F, 0F, 2, 4, 2, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, -1.5F, 1F, 0F, -1.5F, 0F, 0F, 1.5F, 0F, 0F, 1.5F); // Box 127
		gunModel[32].setRotationPoint(40F, 11.5F, 5F);

		gunModel[33].addBox(0F, 0F, 0F, 5, 2, 5, 0F); // Box 128
		gunModel[33].setRotationPoint(38.5F, 15.5F, 4F);

		gunModel[34].addShapeBox(0F, 0F, 0F, 5, 1, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F); // Box 129
		gunModel[34].setRotationPoint(38.5F, 17.5F, 4F);

		gunModel[35].addShapeBox(0F, 0F, 0F, 4, 2, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 25
		gunModel[35].setRotationPoint(39F, -8.5F, -4.5F);

		gunModel[36].addShapeBox(0F, 0F, 0F, 4, 10, 8, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 26
		gunModel[36].setRotationPoint(-6F, -12.5F, -4F);

		gunModel[37].addBox(0F, 0F, 0F, 3, 3, 7, 0F); // Box 31
		gunModel[37].setRotationPoint(-1F, -15.5F, -3.5F);

		gunModel[38].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 32
		gunModel[38].setRotationPoint(-1F, -17.5F, -3.5F);

		gunModel[39].addBox(0F, 0F, 0F, 16, 3, 1, 0F); // Box 36
		gunModel[39].setRotationPoint(2F, -15.5F, 2.5F);

		gunModel[40].addShapeBox(0F, 0F, 0F, 16, 2, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 37
		gunModel[40].setRotationPoint(2F, -17.5F, -1.5F);

		gunModel[41].addBox(0F, 0F, 0F, 15, 3, 1, 0F); // Box 1
		gunModel[41].setRotationPoint(4F, -12.5F, 4F);

		gunModel[42].addShapeBox(0F, 0F, 0F, 3, 6, 1, 0F, -3F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 3
		gunModel[42].setRotationPoint(14F, -18.5F, 4F);

		gunModel[43].addShapeBox(0F, 0F, 0F, 3, 6, 1, 0F, -3F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 4
		gunModel[43].setRotationPoint(10F, -18.5F, 4F);

		gunModel[44].addShapeBox(0F, 0F, 0F, 3, 6, 1, 0F, -3F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 5
		gunModel[44].setRotationPoint(6F, -18.5F, 4F);

		gunModel[45].addBox(0F, 0F, 0F, 21, 2, 9, 0F); // Box 6
		gunModel[45].setRotationPoint(7F, -20.5F, -4F);

		gunModel[46].addBox(0F, 0F, 0F, 21, 2, 7, 0F); // Box 7
		gunModel[46].setRotationPoint(7F, -22.5F, -3F);

		gunModel[47].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 0
		gunModel[47].setRotationPoint(7F, -24.5F, -3F);

		gunModel[48].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 1
		gunModel[48].setRotationPoint(13F, -24.5F, -3F);

		gunModel[49].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 2
		gunModel[49].setRotationPoint(19F, -24.5F, -3F);

		gunModel[50].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 3
		gunModel[50].setRotationPoint(25F, -24.5F, -3F);


		defaultBarrelModel = new ModelRendererTurbo[3];
		defaultBarrelModel[0] = new ModelRendererTurbo(this, 243, 1, textureX, textureY); // Box 30
		defaultBarrelModel[1] = new ModelRendererTurbo(this, 243, 11, textureX, textureY); // Box 31
		defaultBarrelModel[2] = new ModelRendererTurbo(this, 243, 1, textureX, textureY); // Box 32

		defaultBarrelModel[0].addShapeBox(0F, 0F, 0F, 15, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 30
		defaultBarrelModel[0].setRotationPoint(96F, -17.5F, -3.5F);

		defaultBarrelModel[1].addBox(0F, 0F, 0F, 15, 3, 7, 0F); // Box 31
		defaultBarrelModel[1].setRotationPoint(96F, -15.5F, -3.5F);

		defaultBarrelModel[2].addShapeBox(0F, 0F, 0F, 15, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 32
		defaultBarrelModel[2].setRotationPoint(96F, -12.5F, -3.5F);


		defaultScopeModel = new ModelRendererTurbo[30];
		defaultScopeModel[0] = new ModelRendererTurbo(this, 243, 22, textureX, textureY); // Box 42
		defaultScopeModel[1] = new ModelRendererTurbo(this, 295, 46, textureX, textureY); // Box 42
		defaultScopeModel[2] = new ModelRendererTurbo(this, 243, 35, textureX, textureY); // Box 42
		defaultScopeModel[3] = new ModelRendererTurbo(this, 312, 36, textureX, textureY); // Box 42
		defaultScopeModel[4] = new ModelRendererTurbo(this, 312, 36, textureX, textureY); // Box 42
		defaultScopeModel[5] = new ModelRendererTurbo(this, 312, 26, textureX, textureY); // Box 42
		defaultScopeModel[6] = new ModelRendererTurbo(this, 295, 39, textureX, textureY); // Box 42
		defaultScopeModel[7] = new ModelRendererTurbo(this, 295, 39, textureX, textureY); // Box 42
		defaultScopeModel[8] = new ModelRendererTurbo(this, 243, 35, textureX, textureY); // Box 42
		defaultScopeModel[9] = new ModelRendererTurbo(this, 295, 54, textureX, textureY); // Box 42
		defaultScopeModel[10] = new ModelRendererTurbo(this, 242, 52, textureX, textureY); // Box 42
		defaultScopeModel[11] = new ModelRendererTurbo(this, 295, 54, textureX, textureY); // Box 42
		defaultScopeModel[12] = new ModelRendererTurbo(this, 273, 109, textureX, textureY); // Box 42
		defaultScopeModel[13] = new ModelRendererTurbo(this, 242, 108, textureX, textureY); // Box 42
		defaultScopeModel[14] = new ModelRendererTurbo(this, 273, 109, textureX, textureY); // Box 42
		defaultScopeModel[15] = new ModelRendererTurbo(this, 277, 87, textureX, textureY); // Box 42
		defaultScopeModel[16] = new ModelRendererTurbo(this, 242, 86, textureX, textureY); // Box 42
		defaultScopeModel[17] = new ModelRendererTurbo(this, 277, 87, textureX, textureY); // Box 42
		defaultScopeModel[18] = new ModelRendererTurbo(this, 269, 98, textureX, textureY); // Box 42
		defaultScopeModel[19] = new ModelRendererTurbo(this, 242, 97, textureX, textureY); // Box 42
		defaultScopeModel[20] = new ModelRendererTurbo(this, 269, 98, textureX, textureY); // Box 42
		defaultScopeModel[21] = new ModelRendererTurbo(this, 289, 71, textureX, textureY); // Box 42
		defaultScopeModel[22] = new ModelRendererTurbo(this, 242, 69, textureX, textureY); // Box 42
		defaultScopeModel[23] = new ModelRendererTurbo(this, 289, 71, textureX, textureY); // Box 42
		defaultScopeModel[24] = new ModelRendererTurbo(this, 242, 119, textureX, textureY); // Box 42
		defaultScopeModel[25] = new ModelRendererTurbo(this, 263, 119, textureX, textureY); // Box 42
		defaultScopeModel[26] = new ModelRendererTurbo(this, 263, 119, textureX, textureY); // Box 42
		defaultScopeModel[27] = new ModelRendererTurbo(this, 242, 119, textureX, textureY); // Box 42
		defaultScopeModel[28] = new ModelRendererTurbo(this, 263, 119, textureX, textureY); // Box 42
		defaultScopeModel[29] = new ModelRendererTurbo(this, 263, 119, textureX, textureY); // Box 42

		defaultScopeModel[0].addBox(0F, 0F, 0F, 17, 3, 9, 0F); // Box 42
		defaultScopeModel[0].setRotationPoint(9F, -25F, -4.5F);

		defaultScopeModel[1].addBox(0F, 0F, 0F, 11, 1, 6, 0F); // Box 42
		defaultScopeModel[1].setRotationPoint(12F, -26F, -3F);

		defaultScopeModel[2].addShapeBox(0F, 0F, 0F, 13, 5, 11, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 42
		defaultScopeModel[2].setRotationPoint(11F, -36F, -5.5F);

		defaultScopeModel[3].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F); // Box 42
		defaultScopeModel[3].setRotationPoint(14F, -37.5F, -3.5F);

		defaultScopeModel[4].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F); // Box 42
		defaultScopeModel[4].setRotationPoint(19F, -37.5F, -3.5F);

		defaultScopeModel[5].addBox(0F, 0F, 0F, 3, 2, 7, 0F); // Box 42
		defaultScopeModel[5].setRotationPoint(16F, -37.5F, -3.5F);

		defaultScopeModel[6].addShapeBox(0F, 0F, 0F, 4, 2, 4, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 42
		defaultScopeModel[6].setRotationPoint(15.5F, -33F, -8.5F);

		defaultScopeModel[7].addShapeBox(0F, 0F, 0F, 4, 2, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 42
		defaultScopeModel[7].setRotationPoint(15.5F, -31F, -8.5F);

		defaultScopeModel[8].addShapeBox(0F, 0F, 0F, 13, 5, 11, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 42
		defaultScopeModel[8].setRotationPoint(11F, -31F, -5.5F);

		defaultScopeModel[9].addShapeBox(0F, 0F, 0F, 15, 3, 11, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 42
		defaultScopeModel[9].setRotationPoint(42F, -36.5F, -5.5F);

		defaultScopeModel[10].addBox(0F, 0F, 0F, 15, 5, 11, 0F); // Box 42
		defaultScopeModel[10].setRotationPoint(42F, -33.5F, -5.5F);

		defaultScopeModel[11].addShapeBox(0F, 0F, 0F, 15, 3, 11, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 42
		defaultScopeModel[11].setRotationPoint(42F, -28.5F, -5.5F);

		defaultScopeModel[12].addShapeBox(0F, 0F, 0F, 8, 2, 7, 0F, 0F, 0F, -2F, 0F, 2F, -1F, 0F, 2F, -1F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, -1F, 2F, 0F, -1F, 2F, 0F, 0F, 0F); // Box 42
		defaultScopeModel[12].setRotationPoint(34F, -34.5F, -3.5F);

		defaultScopeModel[13].addShapeBox(0F, 0F, 0F, 8, 3, 7, 0F, 0F, 0F, 0F, 0F, 1F, 2F, 0F, 1F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 2F, 0F, 1F, 2F, 0F, 0F, 0F); // Box 42
		defaultScopeModel[13].setRotationPoint(34F, -32.5F, -3.5F);

		defaultScopeModel[14].addShapeBox(0F, 0F, 0F, 8, 2, 7, 0F, 0F, 0F, 0F, 0F, -1F, 2F, 0F, -1F, 2F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 2F, -1F, 0F, 2F, -1F, 0F, 0F, -2F); // Box 42
		defaultScopeModel[14].setRotationPoint(34F, -29.5F, -3.5F);

		defaultScopeModel[15].addShapeBox(0F, 0F, 0F, 10, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 42
		defaultScopeModel[15].setRotationPoint(24F, -34.5F, -3.5F);

		defaultScopeModel[16].addBox(0F, 0F, 0F, 10, 3, 7, 0F); // Box 42
		defaultScopeModel[16].setRotationPoint(24F, -32.5F, -3.5F);

		defaultScopeModel[17].addShapeBox(0F, 0F, 0F, 10, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 42
		defaultScopeModel[17].setRotationPoint(24F, -29.5F, -3.5F);

		defaultScopeModel[18].addShapeBox(0F, 0F, 0F, 6, 2, 7, 0F, 0F, 2F, -1F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 2F, -1F, 0F, -1F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 2F); // Box 42
		defaultScopeModel[18].setRotationPoint(5F, -34.5F, -3.5F);

		defaultScopeModel[19].addShapeBox(0F, 0F, 0F, 6, 3, 7, 0F, 0F, 1F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 2F, 0F, 1F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 2F); // Box 42
		defaultScopeModel[19].setRotationPoint(5F, -32.5F, -3.5F);

		defaultScopeModel[20].addShapeBox(0F, 0F, 0F, 6, 2, 7, 0F, 0F, -1F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 2F, 0F, 2F, -1F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 2F, -1F); // Box 42
		defaultScopeModel[20].setRotationPoint(5F, -29.5F, -3.5F);

		defaultScopeModel[21].addShapeBox(0F, 0F, 0F, 12, 3, 11, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 42
		defaultScopeModel[21].setRotationPoint(-7F, -28.5F, -5.5F);

		defaultScopeModel[22].addBox(0F, 0F, 0F, 12, 5, 11, 0F); // Box 42
		defaultScopeModel[22].setRotationPoint(-7F, -33.5F, -5.5F);

		defaultScopeModel[23].addShapeBox(0F, 0F, 0F, 12, 3, 11, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 42
		defaultScopeModel[23].setRotationPoint(-7F, -36.5F, -5.5F);

		defaultScopeModel[24].addBox(0F, 0F, 0F, 1, 3, 9, 0F); // Box 42
		defaultScopeModel[24].setRotationPoint(-7.2F, -32.5F, -4.5F);

		defaultScopeModel[25].addShapeBox(0F, 0F, 0F, 1, 3, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 42
		defaultScopeModel[25].setRotationPoint(-7.2F, -29.5F, -4.5F);

		defaultScopeModel[26].addShapeBox(0F, 0F, 0F, 1, 3, 9, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 42
		defaultScopeModel[26].setRotationPoint(-7.2F, -35.5F, -4.5F);

		defaultScopeModel[27].addBox(0F, 0F, 0F, 1, 3, 9, 0F); // Box 42
		defaultScopeModel[27].setRotationPoint(56.2F, -32.5F, -4.5F);

		defaultScopeModel[28].addShapeBox(0F, 0F, 0F, 1, 3, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 42
		defaultScopeModel[28].setRotationPoint(56.2F, -29.5F, -4.5F);

		defaultScopeModel[29].addShapeBox(0F, 0F, 0F, 1, 3, 9, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 42
		defaultScopeModel[29].setRotationPoint(56.2F, -35.5F, -4.5F);


		defaultStockModel = new ModelRendererTurbo[19];
		defaultStockModel[0] = new ModelRendererTurbo(this, 1, 130, textureX, textureY); // Box 5
		defaultStockModel[1] = new ModelRendererTurbo(this, 1, 137, textureX, textureY); // Box 6
		defaultStockModel[2] = new ModelRendererTurbo(this, 1, 130, textureX, textureY); // Box 7
		defaultStockModel[3] = new ModelRendererTurbo(this, 1, 145, textureX, textureY); // Box 8
		defaultStockModel[4] = new ModelRendererTurbo(this, 1, 97, textureX, textureY); // Box 9
		defaultStockModel[5] = new ModelRendererTurbo(this, 1, 158, textureX, textureY); // Box 10
		defaultStockModel[6] = new ModelRendererTurbo(this, 26, 145, textureX, textureY); // Box 11
		defaultStockModel[7] = new ModelRendererTurbo(this, 1, 72, textureX, textureY); // Box 12
		defaultStockModel[8] = new ModelRendererTurbo(this, 1, 115, textureX, textureY); // Box 13
		defaultStockModel[9] = new ModelRendererTurbo(this, 1, 122, textureX, textureY); // Box 14
		defaultStockModel[10] = new ModelRendererTurbo(this, 1, 115, textureX, textureY); // Box 15
		defaultStockModel[11] = new ModelRendererTurbo(this, 1, 106, textureX, textureY); // Box 16
		defaultStockModel[12] = new ModelRendererTurbo(this, 52, 72, textureX, textureY); // Box 18
		defaultStockModel[13] = new ModelRendererTurbo(this, 76, 97, textureX, textureY); // Box 19
		defaultStockModel[14] = new ModelRendererTurbo(this, 76, 97, textureX, textureY); // Box 20
		defaultStockModel[15] = new ModelRendererTurbo(this, 54, 140, textureX, textureY); // Box 26
		defaultStockModel[16] = new ModelRendererTurbo(this, 54, 128, textureX, textureY); // Box 27
		defaultStockModel[17] = new ModelRendererTurbo(this, 54, 118, textureX, textureY); // Box 28
		defaultStockModel[18] = new ModelRendererTurbo(this, 54, 106, textureX, textureY); // Box 29

		defaultStockModel[0].addShapeBox(0F, 0F, 0F, 12, 1, 5, 0F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 5
		defaultStockModel[0].setRotationPoint(-18F, -8F, -2.5F);

		defaultStockModel[1].addBox(0F, 0F, 0F, 12, 2, 5, 0F); // Box 6
		defaultStockModel[1].setRotationPoint(-18F, -7F, -2.5F);

		defaultStockModel[2].addShapeBox(0F, 0F, 0F, 12, 1, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F); // Box 7
		defaultStockModel[2].setRotationPoint(-18F, -5F, -2.5F);

		defaultStockModel[3].addBox(0F, 0F, 0F, 5, 5, 7, 0F); // Box 8
		defaultStockModel[3].setRotationPoint(-23F, -7.5F, -3.5F);

		defaultStockModel[4].addShapeBox(0F, 0F, 0F, 30, 1, 7, 0F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 9
		defaultStockModel[4].setRotationPoint(-48F, -8.5F, -3.5F);

		defaultStockModel[5].addShapeBox(0F, 0F, 0F, 4, 5, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F); // Box 10
		defaultStockModel[5].setRotationPoint(-27F, -7.5F, -3.5F);

		defaultStockModel[6].addShapeBox(0F, 0F, 0F, 3, 8, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 6F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 6F, 0F); // Box 11
		defaultStockModel[6].setRotationPoint(-30F, -7.5F, -3.5F);

		defaultStockModel[7].addBox(0F, -2F, 0F, 18, 15, 7, 0F); // Box 12
		defaultStockModel[7].setRotationPoint(-48F, -5.5F, -3.5F);

		defaultStockModel[8].addShapeBox(0F, 0F, 0F, 21, 1, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F); // Box 13
		defaultStockModel[8].setRotationPoint(-30F, 7F, -2.5F);

		defaultStockModel[9].addBox(0F, 0F, 0F, 21, 2, 5, 0F); // Box 14
		defaultStockModel[9].setRotationPoint(-30F, 5F, -2.5F);

		defaultStockModel[10].addShapeBox(0F, 0F, 0F, 21, 1, 5, 0F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 15
		defaultStockModel[10].setRotationPoint(-30F, 4F, -2.5F);

		defaultStockModel[11].addShapeBox(0F, 0F, 0F, 18, 1, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F); // Box 16
		defaultStockModel[11].setRotationPoint(-48F, 7.5F, -3.5F);

		defaultStockModel[12].addShapeBox(0F, -2F, 0F, 2, 15, 7, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 18
		defaultStockModel[12].setRotationPoint(-50F, -5.5F, -3.5F);

		defaultStockModel[13].addShapeBox(0F, 0F, 0F, 2, 1, 7, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -2F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0F, -2F); // Box 19
		defaultStockModel[13].setRotationPoint(-50F, 7.5F, -3.5F);

		defaultStockModel[14].addShapeBox(0F, 0F, 0F, 2, 1, 7, 0F, 0F, 0F, -2F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0F, -2F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 20
		defaultStockModel[14].setRotationPoint(-50F, -8.5F, -3.5F);

		defaultStockModel[15].addShapeBox(0F, 0F, 0F, 4, 1, 7, 0F, 0F, 0.5F, -1.5F, 0F, -2.5F, -1.5F, 0F, -2.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F); // Box 26
		defaultStockModel[15].setRotationPoint(-22F, -12.5F, -3.5F);

		defaultStockModel[16].addShapeBox(0F, 0F, 0F, 4, 4, 7, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 27
		defaultStockModel[16].setRotationPoint(-22F, -11.5F, -3.5F);

		defaultStockModel[17].addShapeBox(0F, 0F, 0F, 18, 2, 7, 0F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 28
		defaultStockModel[17].setRotationPoint(-40F, -13.5F, -3.5F);

		defaultStockModel[18].addBox(0F, 0F, 0F, 18, 4, 7, 0F); // Box 29
		defaultStockModel[18].setRotationPoint(-40F, -11.5F, -3.5F);


		ammoModel = new ModelRendererTurbo[5];
		ammoModel[0] = new ModelRendererTurbo(this, 120, 135, textureX, textureY); // Box 12
		ammoModel[1] = new ModelRendererTurbo(this, 165, 135, textureX, textureY); // Box 33
		ammoModel[2] = new ModelRendererTurbo(this, 165, 141, textureX, textureY); // Box 34
		ammoModel[3] = new ModelRendererTurbo(this, 120, 152, textureX, textureY); // Box 35
		ammoModel[4] = new ModelRendererTurbo(this, 120, 161, textureX, textureY); // Box 36

		ammoModel[0].addBox(0F, 0F, 0F, 16, 10, 6, 0F); // Box 12
		ammoModel[0].setRotationPoint(17F, -7.5F, -3F);

		ammoModel[1].addShapeBox(0F, 0F, 0F, 10, 1, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 33
		ammoModel[1].setRotationPoint(18F, -8.5F, -2F);

		ammoModel[2].addShapeBox(0F, 0F, 0F, 4, 1, 4, 0F, 0F, 0F, -1F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 34
		ammoModel[2].setRotationPoint(28F, -8.5F, -2F);

		ammoModel[3].addBox(0F, 0F, 0F, 16, 2, 6, 0F); // Box 35
		ammoModel[3].setRotationPoint(17F, 3.5F, -3F);

		ammoModel[4].addBox(0F, 0F, 0F, 15, 2, 5, 0F); // Box 36
		ammoModel[4].setRotationPoint(17.5F, 2.5F, -2.5F);


		pumpModel = new ModelRendererTurbo[14];
		pumpModel[0] = new ModelRendererTurbo(this, 141, 37, textureX, textureY); // Box 15
		pumpModel[1] = new ModelRendererTurbo(this, 141, 48, textureX, textureY); // Box 16
		pumpModel[2] = new ModelRendererTurbo(this, 141, 48, textureX, textureY); // Box 27
		pumpModel[3] = new ModelRendererTurbo(this, 162, 37, textureX, textureY); // Box 29
		pumpModel[4] = new ModelRendererTurbo(this, 162, 48, textureX, textureY); // Box 30
		pumpModel[5] = new ModelRendererTurbo(this, 191, 16, textureX, textureY); // Box 33
		pumpModel[6] = new ModelRendererTurbo(this, 191, 23, textureX, textureY); // Box 34
		pumpModel[7] = new ModelRendererTurbo(this, 191, 16, textureX, textureY); // Box 35
		pumpModel[8] = new ModelRendererTurbo(this, 162, 48, textureX, textureY); // Box 38
		pumpModel[9] = new ModelRendererTurbo(this, 89, 1, textureX, textureY); // Box 21
		pumpModel[10] = new ModelRendererTurbo(this, 89, 7, textureX, textureY); // Box 22
		pumpModel[11] = new ModelRendererTurbo(this, 89, 7, textureX, textureY); // Box 23
		pumpModel[12] = new ModelRendererTurbo(this, 89, 1, textureX, textureY); // Box 24
		pumpModel[13] = new ModelRendererTurbo(this, 89, 1, textureX, textureY); // Box 25

		pumpModel[0].addBox(0F, 0F, 0F, 3, 3, 7, 0F); // Box 15
		pumpModel[0].setRotationPoint(-5F, -15.5F, -3.5F);

		pumpModel[1].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 16
		pumpModel[1].setRotationPoint(-5F, -17.5F, -3.5F);

		pumpModel[2].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 27
		pumpModel[2].setRotationPoint(-5F, -12.5F, -3.5F);

		pumpModel[3].addShapeBox(0F, 0F, 0F, 1, 3, 7, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 29
		pumpModel[3].setRotationPoint(-6F, -15.5F, -3.5F);

		pumpModel[4].addShapeBox(0F, 0F, 0F, 1, 2, 7, 0F, 0F, -0.5F, -2.5F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, -0.5F, -2.5F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 30
		pumpModel[4].setRotationPoint(-6F, -17.5F, -3.5F);

		pumpModel[5].addShapeBox(0F, 0F, 0F, 20, 1, 5, 0F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 33
		pumpModel[5].setRotationPoint(-2F, -16F, -2.5F);

		pumpModel[6].addBox(0F, 0F, 0F, 20, 2, 5, 0F); // Box 34
		pumpModel[6].setRotationPoint(-2F, -15F, -2.5F);

		pumpModel[7].addShapeBox(0F, 0F, 0F, 20, 1, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F); // Box 35
		pumpModel[7].setRotationPoint(-2F, -13F, -2.5F);

		pumpModel[8].addShapeBox(0F, 0F, 0F, 1, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -0.5F, -2.5F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, -0.5F, -2.5F); // Box 38
		pumpModel[8].setRotationPoint(-6F, -12.5F, -3.5F);

		pumpModel[9].addShapeBox(0F, 0F, 0F, 3, 1, 4, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 21
		pumpModel[9].setRotationPoint(-5F, -15.5F, -10.5F);

		pumpModel[10].addShapeBox(0F, 0F, 0F, 2, 1, 3, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 22
		pumpModel[10].setRotationPoint(-4.5F, -15F, -6.5F);

		pumpModel[11].addShapeBox(0F, 0F, 0F, 2, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 23
		pumpModel[11].setRotationPoint(-4.5F, -14F, -6.5F);

		pumpModel[12].addShapeBox(0F, 0F, 0F, 3, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 24
		pumpModel[12].setRotationPoint(-5F, -13.5F, -10.5F);

		pumpModel[13].addBox(0F, 0F, 0F, 3, 1, 4, 0F); // Box 25
		pumpModel[13].setRotationPoint(-5F, -14.5F, -10.5F);

		barrelAttachPoint = new Vector3f(96F /16F, 14F /16F, 0F /16F);
		scopeAttachPoint = new Vector3f(18F /16F, 22F /16F, 0F /16F);

		gunSlideDistance = 0F;
		animationType = EnumAnimationType.BOTTOM_CLIP;
		pumpDelay = 15;
		pumpTime = 10;
		pumpHandleDistance = 0.8F;

		translateAll(0F, 0F, 0F);


		flipAll();
	}
}