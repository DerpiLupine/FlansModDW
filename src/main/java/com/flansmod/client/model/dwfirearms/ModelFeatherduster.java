package com.flansmod.client.model.dwfirearms; //Path where the model is located

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelFeatherduster extends ModelGun //Same as Filename
{
	int textureX = 512;
	int textureY = 256;

	public ModelFeatherduster() //Same as Filename
	{
		gunModel = new ModelRendererTurbo[89];
		gunModel[0] = new ModelRendererTurbo(this, 202, 98, textureX, textureY); // Box 3
		gunModel[1] = new ModelRendererTurbo(this, 151, 62, textureX, textureY); // Box 4
		gunModel[2] = new ModelRendererTurbo(this, 145, 111, textureX, textureY); // Box 8
		gunModel[3] = new ModelRendererTurbo(this, 1, 13, textureX, textureY); // Box 10
		gunModel[4] = new ModelRendererTurbo(this, 202, 87, textureX, textureY); // Box 13
		gunModel[5] = new ModelRendererTurbo(this, 171, 51, textureX, textureY); // Box 17
		gunModel[6] = new ModelRendererTurbo(this, 100, 134, textureX, textureY); // Box 22
		gunModel[7] = new ModelRendererTurbo(this, 100, 125, textureX, textureY); // Box 23
		gunModel[8] = new ModelRendererTurbo(this, 171, 39, textureX, textureY); // Box 57
		gunModel[9] = new ModelRendererTurbo(this, 19, 4, textureX, textureY); // Box 74
		gunModel[10] = new ModelRendererTurbo(this, 1, 8, textureX, textureY); // Box 77
		gunModel[11] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 78
		gunModel[12] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 79
		gunModel[13] = new ModelRendererTurbo(this, 1, 8, textureX, textureY); // Box 80
		gunModel[14] = new ModelRendererTurbo(this, 1, 8, textureX, textureY); // Box 81
		gunModel[15] = new ModelRendererTurbo(this, 1, 8, textureX, textureY); // Box 82
		gunModel[16] = new ModelRendererTurbo(this, 10, 10, textureX, textureY); // Box 83
		gunModel[17] = new ModelRendererTurbo(this, 10, 10, textureX, textureY); // Box 84
		gunModel[18] = new ModelRendererTurbo(this, 10, 10, textureX, textureY); // Box 85
		gunModel[19] = new ModelRendererTurbo(this, 56, 8, textureX, textureY); // Box 86
		gunModel[20] = new ModelRendererTurbo(this, 1, 8, textureX, textureY); // Box 87
		gunModel[21] = new ModelRendererTurbo(this, 1, 8, textureX, textureY); // Box 88
		gunModel[22] = new ModelRendererTurbo(this, 100, 46, textureX, textureY); // Box 16
		gunModel[23] = new ModelRendererTurbo(this, 100, 16, textureX, textureY); // Box 17
		gunModel[24] = new ModelRendererTurbo(this, 100, 1, textureX, textureY); // Box 18
		gunModel[25] = new ModelRendererTurbo(this, 100, 16, textureX, textureY); // Box 19
		gunModel[26] = new ModelRendererTurbo(this, 231, 34, textureX, textureY); // Box 23
		gunModel[27] = new ModelRendererTurbo(this, 246, 26, textureX, textureY); // Box 24
		gunModel[28] = new ModelRendererTurbo(this, 231, 26, textureX, textureY); // Box 26
		gunModel[29] = new ModelRendererTurbo(this, 246, 34, textureX, textureY); // Box 27
		gunModel[30] = new ModelRendererTurbo(this, 196, 10, textureX, textureY); // Box 28
		gunModel[31] = new ModelRendererTurbo(this, 100, 30, textureX, textureY); // Box 29
		gunModel[32] = new ModelRendererTurbo(this, 175, 2, textureX, textureY); // Box 30
		gunModel[33] = new ModelRendererTurbo(this, 175, 6, textureX, textureY); // Box 31
		gunModel[34] = new ModelRendererTurbo(this, 175, 6, textureX, textureY); // Box 32
		gunModel[35] = new ModelRendererTurbo(this, 175, 2, textureX, textureY); // Box 33
		gunModel[36] = new ModelRendererTurbo(this, 238, 13, textureX, textureY); // Box 34
		gunModel[37] = new ModelRendererTurbo(this, 238, 2, textureX, textureY); // Box 35
		gunModel[38] = new ModelRendererTurbo(this, 238, 2, textureX, textureY); // Box 36
		gunModel[39] = new ModelRendererTurbo(this, 246, 26, textureX, textureY); // Box 40
		gunModel[40] = new ModelRendererTurbo(this, 231, 26, textureX, textureY); // Box 41
		gunModel[41] = new ModelRendererTurbo(this, 196, 10, textureX, textureY); // Box 42
		gunModel[42] = new ModelRendererTurbo(this, 246, 34, textureX, textureY); // Box 43
		gunModel[43] = new ModelRendererTurbo(this, 231, 34, textureX, textureY); // Box 44
		gunModel[44] = new ModelRendererTurbo(this, 175, 10, textureX, textureY); // Box 45
		gunModel[45] = new ModelRendererTurbo(this, 198, 19, textureX, textureY); // Box 49
		gunModel[46] = new ModelRendererTurbo(this, 198, 19, textureX, textureY); // Box 55
		gunModel[47] = new ModelRendererTurbo(this, 198, 25, textureX, textureY); // Box 66
		gunModel[48] = new ModelRendererTurbo(this, 198, 25, textureX, textureY); // Box 69
		gunModel[49] = new ModelRendererTurbo(this, 217, 12, textureX, textureY); // Box 70
		gunModel[50] = new ModelRendererTurbo(this, 217, 12, textureX, textureY); // Box 71
		gunModel[51] = new ModelRendererTurbo(this, 238, 13, textureX, textureY); // Box 0
		gunModel[52] = new ModelRendererTurbo(this, 238, 2, textureX, textureY); // Box 1
		gunModel[53] = new ModelRendererTurbo(this, 198, 25, textureX, textureY); // Box 2
		gunModel[54] = new ModelRendererTurbo(this, 198, 19, textureX, textureY); // Box 3
		gunModel[55] = new ModelRendererTurbo(this, 198, 19, textureX, textureY); // Box 4
		gunModel[56] = new ModelRendererTurbo(this, 238, 2, textureX, textureY); // Box 5
		gunModel[57] = new ModelRendererTurbo(this, 198, 25, textureX, textureY); // Box 6
		gunModel[58] = new ModelRendererTurbo(this, 175, 10, textureX, textureY); // Box 7
		gunModel[59] = new ModelRendererTurbo(this, 100, 143, textureX, textureY); // Box 14
		gunModel[60] = new ModelRendererTurbo(this, 19, 4, textureX, textureY); // Box 15
		gunModel[61] = new ModelRendererTurbo(this, 1, 24, textureX, textureY); // Box 0
		gunModel[62] = new ModelRendererTurbo(this, 34, 24, textureX, textureY); // Box 1
		gunModel[63] = new ModelRendererTurbo(this, 38, 13, textureX, textureY); // Box 2
		gunModel[64] = new ModelRendererTurbo(this, 53, 24, textureX, textureY); // Box 3
		gunModel[65] = new ModelRendererTurbo(this, 57, 13, textureX, textureY); // Box 4
		gunModel[66] = new ModelRendererTurbo(this, 34, 52, textureX, textureY); // Box 5
		gunModel[67] = new ModelRendererTurbo(this, 53, 52, textureX, textureY); // Box 6
		gunModel[68] = new ModelRendererTurbo(this, 1, 52, textureX, textureY); // Box 7
		gunModel[69] = new ModelRendererTurbo(this, 100, 63, textureX, textureY); // Box 8
		gunModel[70] = new ModelRendererTurbo(this, 163, 104, textureX, textureY); // Box 9
		gunModel[71] = new ModelRendererTurbo(this, 100, 84, textureX, textureY); // Box 11
		gunModel[72] = new ModelRendererTurbo(this, 100, 111, textureX, textureY); // Box 12
		gunModel[73] = new ModelRendererTurbo(this, 151, 73, textureX, textureY); // Box 14
		gunModel[74] = new ModelRendererTurbo(this, 49, 1, textureX, textureY); // Box 0
		gunModel[75] = new ModelRendererTurbo(this, 261, 2, textureX, textureY); // Box 1
		gunModel[76] = new ModelRendererTurbo(this, 261, 35, textureX, textureY); // Box 2
		gunModel[77] = new ModelRendererTurbo(this, 261, 20, textureX, textureY); // Box 3
		gunModel[78] = new ModelRendererTurbo(this, 100, 96, textureX, textureY); // Box 7
		gunModel[79] = new ModelRendererTurbo(this, 200, 47, textureX, textureY); // Box 10
		gunModel[80] = new ModelRendererTurbo(this, 227, 47, textureX, textureY); // Box 11
		gunModel[81] = new ModelRendererTurbo(this, 198, 69, textureX, textureY); // Box 14
		gunModel[82] = new ModelRendererTurbo(this, 163, 97, textureX, textureY); // Box 15
		gunModel[83] = new ModelRendererTurbo(this, 175, 19, textureX, textureY); // Box 16
		gunModel[84] = new ModelRendererTurbo(this, 175, 19, textureX, textureY); // Box 17
		gunModel[85] = new ModelRendererTurbo(this, 70, 5, textureX, textureY); // Box 18
		gunModel[86] = new ModelRendererTurbo(this, 53, 37, textureX, textureY); // Box 42
		gunModel[87] = new ModelRendererTurbo(this, 34, 37, textureX, textureY); // Box 43
		gunModel[88] = new ModelRendererTurbo(this, 1, 37, textureX, textureY); // Box 44

		gunModel[0].addShapeBox(0F, 0F, 0F, 14, 3, 9, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 3
		gunModel[0].setRotationPoint(-10F, -23F, -4.5F);

		gunModel[1].addShapeBox(0F, 0F, 0F, 14, 1, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F); // Box 4
		gunModel[1].setRotationPoint(-10F, -20F, -4.5F);

		gunModel[2].addShapeBox(0F, 0F, 0F, 5, 4, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F); // Box 8
		gunModel[2].setRotationPoint(17F, -13F, -4.5F);

		gunModel[3].addShapeBox(0F, 0F, 0F, 11, 3, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F); // Box 10
		gunModel[3].setRotationPoint(-9F, -7F, -3.5F);

		gunModel[4].addShapeBox(0F, 0F, 0F, 12, 1, 9, 0F, -1F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, -1F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 13
		gunModel[4].setRotationPoint(-8F, -24F, -4.5F);

		gunModel[5].addShapeBox(0F, 0F, 0F, 6, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 17
		gunModel[5].setRotationPoint(18F, -10F, -4F);

		gunModel[6].addShapeBox(0F, 0F, 0F, 38, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F); // Box 22
		gunModel[6].setRotationPoint(24F, -11F, -3F);

		gunModel[7].addBox(0F, 0F, 0F, 38, 2, 6, 0F); // Box 23
		gunModel[7].setRotationPoint(24F, -13F, -3F);

		gunModel[8].addBox(0F, 0F, 0F, 5, 3, 8, 0F); // Box 57
		gunModel[8].setRotationPoint(19F, -13F, -4F);

		gunModel[9].addShapeBox(0F, 0F, 0F, 11, 1, 7, 0F, 0F, 0F, -0.5F, -1F, 0F, -0.5F, -1F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 74
		gunModel[9].setRotationPoint(-7F, -26F, -3.5F);

		gunModel[10].addShapeBox(0F, 0F, 0F, 1, 1, 3, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 77
		gunModel[10].setRotationPoint(-3F, -29F, -3F);

		gunModel[11].addShapeBox(0F, 0F, 0F, 5, 2, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 78
		gunModel[11].setRotationPoint(-3F, -28F, -3F);

		gunModel[12].addShapeBox(0F, 0F, 0F, 5, 2, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 79
		gunModel[12].setRotationPoint(-3F, -28F, 1F);

		gunModel[13].addShapeBox(0F, 0F, 0F, 1, 1, 3, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 80
		gunModel[13].setRotationPoint(-3F, -29F, 1F);

		gunModel[14].addShapeBox(0F, 0F, 0F, 1, 1, 3, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 81
		gunModel[14].setRotationPoint(1F, -29F, 1F);

		gunModel[15].addShapeBox(0F, 0F, 0F, 1, 1, 3, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 82
		gunModel[15].setRotationPoint(1F, -29F, -3F);

		gunModel[16].addBox(0F, 0F, 0F, 3, 1, 1, 0F); // Box 83
		gunModel[16].setRotationPoint(-2F, -28.5F, -2.5F);

		gunModel[17].addBox(0F, 0F, 0F, 3, 1, 1, 0F); // Box 84
		gunModel[17].setRotationPoint(-2F, -28.5F, 1.5F);

		gunModel[18].addBox(0F, 0F, 0F, 3, 1, 1, 0F); // Box 85
		gunModel[18].setRotationPoint(58F, -28.5F, -0.5F);

		gunModel[19].addShapeBox(0F, 0F, 0F, 5, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 86
		gunModel[19].setRotationPoint(57F, -28F, -1F);

		gunModel[20].addShapeBox(0F, 0F, 0F, 1, 1, 3, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 87
		gunModel[20].setRotationPoint(61F, -29F, -1F);

		gunModel[21].addShapeBox(0F, 0F, 0F, 1, 1, 3, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 88
		gunModel[21].setRotationPoint(57F, -29F, -1F);

		gunModel[22].addShapeBox(0F, 0F, 0F, 23, 3, 12, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 16
		gunModel[22].setRotationPoint(37F, -25.5F, -6F);

		gunModel[23].addShapeBox(0F, 0F, 0F, 27, 3, 10, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 17
		gunModel[23].setRotationPoint(35F, -24F, -5F);

		gunModel[24].addBox(0F, 0F, 0F, 27, 4, 10, 0F); // Box 18
		gunModel[24].setRotationPoint(35F, -21F, -5F);

		gunModel[25].addShapeBox(0F, 0F, 0F, 27, 3, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F); // Box 19
		gunModel[25].setRotationPoint(35F, -17F, -5F);

		gunModel[26].addShapeBox(0F, 0F, 0F, 3, 3, 4, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F); // Box 23
		gunModel[26].setRotationPoint(34F, -25.5F, -6F);

		gunModel[27].addShapeBox(0F, 0F, 0F, 3, 3, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 24
		gunModel[27].setRotationPoint(34F, -25.5F, 2F);

		gunModel[28].addShapeBox(0F, 0F, 0F, 3, 3, 4, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F); // Box 26
		gunModel[28].setRotationPoint(34F, -15.5F, 2F);

		gunModel[29].addShapeBox(0F, 0F, 0F, 3, 3, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 27
		gunModel[29].setRotationPoint(34F, -15.5F, -6F);

		gunModel[30].addBox(0F, 0F, 0F, 3, 1, 7, 0F); // Box 28
		gunModel[30].setRotationPoint(34F, -13.5F, -3.5F);

		gunModel[31].addShapeBox(0F, 0F, 0F, 23, 3, 12, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F); // Box 29
		gunModel[31].setRotationPoint(37F, -15.5F, -6F);

		gunModel[32].addShapeBox(0F, 0F, 0F, 29, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 30
		gunModel[32].setRotationPoint(34F, -22.5F, -6F);

		gunModel[33].addShapeBox(0F, 0F, 0F, 29, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 31
		gunModel[33].setRotationPoint(34F, -16.5F, -6F);

		gunModel[34].addShapeBox(0F, 0F, 0F, 29, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 32
		gunModel[34].setRotationPoint(34F, -16.5F, 4.5F);

		gunModel[35].addShapeBox(0F, 0F, 0F, 29, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 33
		gunModel[35].setRotationPoint(34F, -22.5F, 4.5F);

		gunModel[36].addBox(0F, 0F, 0F, 3, 4, 8, 0F); // Box 34
		gunModel[36].setRotationPoint(63F, -21F, -4F);

		gunModel[37].addShapeBox(0F, 0F, 0F, 3, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 35
		gunModel[37].setRotationPoint(63F, -17F, -4F);

		gunModel[38].addShapeBox(0F, 0F, 0F, 3, 2, 8, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 36
		gunModel[38].setRotationPoint(63F, -23F, -4F);

		gunModel[39].addShapeBox(0F, 0F, 0F, 3, 3, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 40
		gunModel[39].setRotationPoint(60F, -25.5F, 2F);

		gunModel[40].addShapeBox(0F, 0F, 0F, 3, 3, 4, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F); // Box 41
		gunModel[40].setRotationPoint(60F, -15.5F, 2F);

		gunModel[41].addBox(0F, 0F, 0F, 3, 1, 7, 0F); // Box 42
		gunModel[41].setRotationPoint(60F, -13.5F, -3.5F);

		gunModel[42].addShapeBox(0F, 0F, 0F, 3, 3, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 43
		gunModel[42].setRotationPoint(60F, -15.5F, -6F);

		gunModel[43].addShapeBox(0F, 0F, 0F, 3, 3, 4, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F); // Box 44
		gunModel[43].setRotationPoint(60F, -25.5F, -6F);

		gunModel[44].addBox(0F, 0F, 0F, 3, 1, 7, 0F); // Box 45
		gunModel[44].setRotationPoint(60F, -25.5F, -3.5F);

		gunModel[45].addBox(0F, 0F, 0F, 3, 4, 1, 0F); // Box 49
		gunModel[45].setRotationPoint(63F, -21F, -5F);

		gunModel[46].addBox(0F, 0F, 0F, 3, 4, 1, 0F); // Box 55
		gunModel[46].setRotationPoint(63F, -21F, 4F);

		gunModel[47].addBox(0F, 0F, 0F, 3, 1, 4, 0F); // Box 66
		gunModel[47].setRotationPoint(63F, -24F, -2F);

		gunModel[48].addBox(0F, 0F, 0F, 3, 1, 4, 0F); // Box 69
		gunModel[48].setRotationPoint(63F, -15F, -2F);

		gunModel[49].addShapeBox(0F, 0F, 0F, 5, 2, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 70
		gunModel[49].setRotationPoint(62F, -21F, -2F);

		gunModel[50].addShapeBox(0F, 0F, 0F, 5, 2, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 71
		gunModel[50].setRotationPoint(62F, -19F, -2F);

		gunModel[51].addBox(0F, 0F, 0F, 3, 4, 8, 0F); // Box 0
		gunModel[51].setRotationPoint(32F, -21F, -4F);

		gunModel[52].addShapeBox(0F, 0F, 0F, 3, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 1
		gunModel[52].setRotationPoint(32F, -17F, -4F);

		gunModel[53].addBox(0F, 0F, 0F, 3, 1, 4, 0F); // Box 2
		gunModel[53].setRotationPoint(32F, -15F, -2F);

		gunModel[54].addBox(0F, 0F, 0F, 3, 4, 1, 0F); // Box 3
		gunModel[54].setRotationPoint(32F, -21F, -5F);

		gunModel[55].addBox(0F, 0F, 0F, 3, 4, 1, 0F); // Box 4
		gunModel[55].setRotationPoint(32F, -21F, 4F);

		gunModel[56].addShapeBox(0F, 0F, 0F, 3, 2, 8, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 5
		gunModel[56].setRotationPoint(32F, -23F, -4F);

		gunModel[57].addBox(0F, 0F, 0F, 3, 1, 4, 0F); // Box 6
		gunModel[57].setRotationPoint(32F, -24F, -2F);

		gunModel[58].addBox(0F, 0F, 0F, 3, 1, 7, 0F); // Box 7
		gunModel[58].setRotationPoint(34F, -25.5F, -3.5F);

		gunModel[59].addBox(0F, 0F, 0F, 38, 1, 3, 0F); // Box 14
		gunModel[59].setRotationPoint(24F, -9F, -1.5F);

		gunModel[60].addBox(0F, 0F, 0F, 11, 1, 7, 0F); // Box 15
		gunModel[60].setRotationPoint(-7F, -25F, -3.5F);

		gunModel[61].addShapeBox(0F, 0F, 0F, 9, 5, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 4F, 0F, 0F); // Box 0
		gunModel[61].setRotationPoint(-7F, -4F, -3.5F);

		gunModel[62].addShapeBox(0F, 0F, 0F, 2, 5, 7, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 4F, 0F, 0F, -4F, 0F, -1.5F, -4F, 0F, -1.5F, 4F, 0F, 0F); // Box 1
		gunModel[62].setRotationPoint(2F, -4F, -3.5F);

		gunModel[63].addShapeBox(0F, 0F, 0F, 2, 3, 7, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F); // Box 2
		gunModel[63].setRotationPoint(2F, -7F, -3.5F);

		gunModel[64].addShapeBox(0F, 0F, 0F, 1, 5, 7, 0F, -4F, 0F, -1.5F, 4F, 0F, 0F, 4F, 0F, 0F, -4F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F); // Box 3
		gunModel[64].setRotationPoint(-12F, -4F, -3.5F);

		gunModel[65].addShapeBox(0F, 0F, 0F, 1, 3, 7, 0F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, -2F, 0F, -1.5F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, -1.5F); // Box 4
		gunModel[65].setRotationPoint(-10F, -7F, -3.5F);

		gunModel[66].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F); // Box 5
		gunModel[66].setRotationPoint(-5F, 8F, -3.5F);

		gunModel[67].addShapeBox(0F, 0F, 0F, 1, 2, 7, 0F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F); // Box 6
		gunModel[67].setRotationPoint(-15F, 8F, -3.5F);

		gunModel[68].addBox(0F, 0F, 0F, 9, 2, 7, 0F); // Box 7
		gunModel[68].setRotationPoint(-14F, 8F, -3.5F);

		gunModel[69].addBox(0F, 0F, 0F, 14, 9, 11, 0F); // Box 8
		gunModel[69].setRotationPoint(-10F, -19F, -5.5F);

		gunModel[70].addShapeBox(0F, 0F, 0F, 16, 3, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 9
		gunModel[70].setRotationPoint(8F, -19F, -5.5F);

		gunModel[71].addShapeBox(0F, 0F, 0F, 28, 2, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 11
		gunModel[71].setRotationPoint(-10F, -9F, -4.5F);

		gunModel[72].addBox(0F, 0F, 0F, 13, 4, 9, 0F); // Box 12
		gunModel[72].setRotationPoint(4F, -13F, -4.5F);

		gunModel[73].addShapeBox(0F, 0F, 0F, 14, 1, 9, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 14
		gunModel[73].setRotationPoint(-10F, -10F, -4.5F);

		gunModel[74].addShapeBox(0F, 0F, 0F, 6, 2, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 0
		gunModel[74].setRotationPoint(56.5F, -27F, -1.5F);

		gunModel[75].addBox(0F, 0F, 0F, 8, 6, 11, 0F); // Box 1
		gunModel[75].setRotationPoint(24F, -22F, -5.5F);

		gunModel[76].addShapeBox(0F, 0F, 0F, 8, 3, 11, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 2
		gunModel[76].setRotationPoint(24F, -25F, -5.5F);

		gunModel[77].addShapeBox(0F, 0F, 0F, 8, 3, 11, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 3
		gunModel[77].setRotationPoint(24F, -16F, -5.5F);

		gunModel[78].addShapeBox(0F, 0F, 0F, 20, 3, 11, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 7
		gunModel[78].setRotationPoint(4F, -16F, -5.5F);

		gunModel[79].addShapeBox(0F, 0F, 0F, 2, 3, 11, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F); // Box 10
		gunModel[79].setRotationPoint(4F, -22F, -5.5F);

		gunModel[80].addShapeBox(0F, 0F, 0F, 2, 3, 11, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 11
		gunModel[80].setRotationPoint(4F, -25F, -5.5F);

		gunModel[81].addBox(0F, 0F, 0F, 4, 3, 11, 0F); // Box 14
		gunModel[81].setRotationPoint(4F, -19F, -5.5F);

		gunModel[82].addShapeBox(0F, 0F, 0F, 16, 3, 3, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 15
		gunModel[82].setRotationPoint(8F, -19F, 2.5F);

		gunModel[83].addShapeBox(0F, 0F, 0F, 2, 2, 9, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 16
		gunModel[83].setRotationPoint(25F, -26F, -4.5F);

		gunModel[84].addBox(0F, 0F, 0F, 2, 2, 9, 0F); // Box 17
		gunModel[84].setRotationPoint(25F, -24F, -4.5F);

		gunModel[85].addBox(0F, 0F, 0F, 12, 4, 1, 0F); // Box 18
		gunModel[85].setRotationPoint(4F, -14F, 4F);

		gunModel[86].addShapeBox(0F, 0F, 0F, 1, 7, 7, 0F, -3F, 0F, -1.5F, 3F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F); // Box 42
		gunModel[86].setRotationPoint(-15F, 1F, -3.5F);

		gunModel[87].addShapeBox(0F, 0F, 0F, 2, 7, 7, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, -1.5F, -3F, 0F, -1.5F, 3F, 0F, 0F); // Box 43
		gunModel[87].setRotationPoint(-2F, 1F, -3.5F);

		gunModel[88].addShapeBox(0F, 0F, 0F, 9, 7, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 3F, 0F, 0F); // Box 44
		gunModel[88].setRotationPoint(-11F, 1F, -3.5F);


		defaultStockModel = new ModelRendererTurbo[24];
		defaultStockModel[0] = new ModelRendererTurbo(this, 1, 92, textureX, textureY); // Box 30
		defaultStockModel[1] = new ModelRendererTurbo(this, 65, 153, textureX, textureY); // Box 30
		defaultStockModel[2] = new ModelRendererTurbo(this, 48, 107, textureX, textureY); // Box 30
		defaultStockModel[3] = new ModelRendererTurbo(this, 48, 100, textureX, textureY); // Box 30
		defaultStockModel[4] = new ModelRendererTurbo(this, 71, 107, textureX, textureY); // Box 30
		defaultStockModel[5] = new ModelRendererTurbo(this, 48, 137, textureX, textureY); // Box 30
		defaultStockModel[6] = new ModelRendererTurbo(this, 1, 62, textureX, textureY); // Box 30
		defaultStockModel[7] = new ModelRendererTurbo(this, 1, 85, textureX, textureY); // Box 30
		defaultStockModel[8] = new ModelRendererTurbo(this, 1, 75, textureX, textureY); // Box 30
		defaultStockModel[9] = new ModelRendererTurbo(this, 20, 145, textureX, textureY); // Box 30
		defaultStockModel[10] = new ModelRendererTurbo(this, 1, 130, textureX, textureY); // Box 30
		defaultStockModel[11] = new ModelRendererTurbo(this, 28, 156, textureX, textureY); // Box 30
		defaultStockModel[12] = new ModelRendererTurbo(this, 1, 145, textureX, textureY); // Box 30
		defaultStockModel[13] = new ModelRendererTurbo(this, 25, 131, textureX, textureY); // Box 30
		defaultStockModel[14] = new ModelRendererTurbo(this, 16, 132, textureX, textureY); // Box 30
		defaultStockModel[15] = new ModelRendererTurbo(this, 1, 125, textureX, textureY); // Box 30
		defaultStockModel[16] = new ModelRendererTurbo(this, 25, 140, textureX, textureY); // Box 30
		defaultStockModel[17] = new ModelRendererTurbo(this, 48, 123, textureX, textureY); // Box 30
		defaultStockModel[18] = new ModelRendererTurbo(this, 1, 156, textureX, textureY); // Box 30
		defaultStockModel[19] = new ModelRendererTurbo(this, 48, 146, textureX, textureY); // Box 30
		defaultStockModel[20] = new ModelRendererTurbo(this, 71, 123, textureX, textureY); // Box 30
		defaultStockModel[21] = new ModelRendererTurbo(this, 48, 146, textureX, textureY); // Box 30
		defaultStockModel[22] = new ModelRendererTurbo(this, 1, 114, textureX, textureY); // Box 30
		defaultStockModel[23] = new ModelRendererTurbo(this, 1, 100, textureX, textureY); // Box 30

		defaultStockModel[0].addBox(0F, 0F, 0F, 25, 2, 5, 0F); // Box 30
		defaultStockModel[0].setRotationPoint(-44F, -10.5F, -2.5F);

		defaultStockModel[1].addShapeBox(0F, 0F, 0F, 4, 2, 9, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 30
		defaultStockModel[1].setRotationPoint(-48F, -19.5F, -4.5F);

		defaultStockModel[2].addBox(0F, 0F, 0F, 4, 8, 7, 0F); // Box 30
		defaultStockModel[2].setRotationPoint(-48F, -10.5F, -3.5F);

		defaultStockModel[3].addShapeBox(0F, 0F, 0F, 12, 1, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 30
		defaultStockModel[3].setRotationPoint(-48F, 5.5F, -2.5F);

		defaultStockModel[4].addBox(0F, 0F, 0F, 4, 6, 9, 0F); // Box 30
		defaultStockModel[4].setRotationPoint(-48F, -17.5F, -4.5F);

		defaultStockModel[5].addShapeBox(0F, 0F, 0F, 4, 1, 7, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 30
		defaultStockModel[5].setRotationPoint(-48F, -11.5F, -3.5F);

		defaultStockModel[6].addBox(0F, 0F, 0F, 31, 5, 7, 0F); // Box 30
		defaultStockModel[6].setRotationPoint(-44F, -16.5F, -3.5F);

		defaultStockModel[7].addShapeBox(0F, 0F, 0F, 31, 1, 5, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 30
		defaultStockModel[7].setRotationPoint(-44F, -11.5F, -2.5F);

		defaultStockModel[8].addShapeBox(0F, 0F, 0F, 31, 2, 7, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 30
		defaultStockModel[8].setRotationPoint(-44F, -18.5F, -3.5F);

		defaultStockModel[9].addBox(0F, 0F, 0F, 8, 5, 5, 0F); // Box 30
		defaultStockModel[9].setRotationPoint(-44F, 0.5F, -2.5F);

		defaultStockModel[10].addBox(0F, 0F, 0F, 2, 9, 5, 0F); // Box 30
		defaultStockModel[10].setRotationPoint(-44F, -8.5F, -2.5F);

		defaultStockModel[11].addShapeBox(0F, 0F, 0F, 13, 3, 5, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 0F, 0F); // Box 30
		defaultStockModel[11].setRotationPoint(-36F, 0.5F, -2.5F);

		defaultStockModel[12].addBox(0F, 0F, 0F, 4, 5, 5, 0F); // Box 30
		defaultStockModel[12].setRotationPoint(-23F, -8.5F, -2.5F);

		defaultStockModel[13].addShapeBox(0F, 0F, 0F, 4, 3, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F); // Box 30
		defaultStockModel[13].setRotationPoint(-23F, -3.5F, -2.5F);

		defaultStockModel[14].addBox(0F, 0F, 0F, 1, 9, 3, 0F); // Box 30
		defaultStockModel[14].setRotationPoint(-42F, -8.5F, -1.5F);

		defaultStockModel[15].addBox(0F, 0F, 0F, 18, 1, 3, 0F); // Box 30
		defaultStockModel[15].setRotationPoint(-41F, -8.5F, -1.5F);

		defaultStockModel[16].addBox(0F, 0F, 0F, 8, 1, 3, 0F); // Box 30
		defaultStockModel[16].setRotationPoint(-41F, -0.5F, -1.5F);

		defaultStockModel[17].addBox(0F, 0F, 0F, 4, 8, 5, 0F); // Box 30
		defaultStockModel[17].setRotationPoint(-48F, -2.5F, -2.5F);

		defaultStockModel[18].addBox(0F, 0F, 0F, 9, 4, 4, 0F); // Box 30
		defaultStockModel[18].setRotationPoint(-19F, -11F, -2F);

		defaultStockModel[19].addShapeBox(0F, 0F, 0F, 3, 1, 7, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 30
		defaultStockModel[19].setRotationPoint(-13F, -10F, -3.5F);

		defaultStockModel[20].addBox(0F, 0F, 0F, 3, 9, 9, 0F); // Box 30
		defaultStockModel[20].setRotationPoint(-13F, -19F, -4.5F);

		defaultStockModel[21].addShapeBox(0F, 0F, 0F, 3, 1, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F); // Box 30
		defaultStockModel[21].setRotationPoint(-13F, -20F, -3.5F);

		defaultStockModel[22].addShapeBox(0F, 0F, 0F, 15, 2, 8, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 30
		defaultStockModel[22].setRotationPoint(-36F, -19.5F, -4F);

		defaultStockModel[23].addBox(0F, 0F, 0F, 15, 5, 8, 0F); // Box 30
		defaultStockModel[23].setRotationPoint(-36F, -17.5F, -4F);


		ammoModel = new ModelRendererTurbo[3];
		ammoModel[0] = new ModelRendererTurbo(this, 157, 168, textureX, textureY); // Box 122
		ammoModel[1] = new ModelRendererTurbo(this, 157, 148, textureX, textureY); // Box 123
		ammoModel[2] = new ModelRendererTurbo(this, 157, 158, textureX, textureY); // Box 124

		ammoModel[0].addBox(-19.2F, -1F, -3.5F, 16, 3, 7, 0F); // Box 122
		ammoModel[0].setRotationPoint(25F, -20.5F, 0F);

		ammoModel[1].addShapeBox(-19.2F, -3F, -3.5F, 16, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 123
		ammoModel[1].setRotationPoint(25F, -20.5F, 0F);

		ammoModel[2].addShapeBox(-19.2F, 2F, -3.5F, 16, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 124
		ammoModel[2].setRotationPoint(25F, -20.5F, 0F);


		pumpModel = new ModelRendererTurbo[5];
		pumpModel[0] = new ModelRendererTurbo(this, 189, 134, textureX, textureY); // Box 29
		pumpModel[1] = new ModelRendererTurbo(this, 189, 122, textureX, textureY); // Box 16
		pumpModel[2] = new ModelRendererTurbo(this, 189, 112, textureX, textureY); // Box 18
		pumpModel[3] = new ModelRendererTurbo(this, 238, 122, textureX, textureY); // Box 19
		pumpModel[4] = new ModelRendererTurbo(this, 238, 134, textureX, textureY); // Box 20

		pumpModel[0].addShapeBox(0F, 0F, 0F, 16, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 29
		pumpModel[0].setRotationPoint(45F, -9F, -4F);

		pumpModel[1].addBox(0F, 0F, 0F, 16, 3, 8, 0F); // Box 16
		pumpModel[1].setRotationPoint(45F, -12F, -4F);

		pumpModel[2].addShapeBox(0F, 0F, 0F, 9, 1, 8, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 18
		pumpModel[2].setRotationPoint(52F, -13F, -4F);

		pumpModel[3].addBox(0F, 0F, 0F, 4, 3, 8, 0F); // Box 19
		pumpModel[3].setRotationPoint(41F, -12F, -4F);

		pumpModel[4].addShapeBox(0F, 0F, 0F, 1, 3, 8, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 20
		pumpModel[4].setRotationPoint(44F, -9F, -4F);


		breakActionModel = new ModelRendererTurbo[4];
		breakActionModel[0] = new ModelRendererTurbo(this, 204, 148, textureX, textureY); // Box 13
		breakActionModel[1] = new ModelRendererTurbo(this, 100, 161, textureX, textureY); // Box 13
		breakActionModel[2] = new ModelRendererTurbo(this, 100, 174, textureX, textureY); // Box 13
		breakActionModel[3] = new ModelRendererTurbo(this, 100, 148, textureX, textureY); // Box 13

		breakActionModel[0].addShapeBox(-19F, -1F, -4.5F, 3, 3, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F); // Box 13
		breakActionModel[0].setRotationPoint(25F, -20.5F, 0F);

		breakActionModel[1].addShapeBox(-19F, -4F, -4.5F, 19, 3, 9, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 13
		breakActionModel[1].setRotationPoint(25F, -20.5F, 0F);

		breakActionModel[2].addBox(-16F, -1F, -4.5F, 16, 3, 9, 0F); // Box 13
		breakActionModel[2].setRotationPoint(25F, -20.5F, 0F);

		breakActionModel[3].addShapeBox(-17F, 2F, -4.5F, 17, 3, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 13
		breakActionModel[3].setRotationPoint(25F, -20.5F, 0F);

		stockAttachPoint = new Vector3f(-10F /16F, 17F /16F, 0F /16F);

		animationType = EnumAnimationType.BREAK_ACTION;
		barrelBreakPoint = new Vector3f(24 /16F, 23F /16F, 0F /16F);
		breakAngle = 35F;

		tiltGunTime = 0.100F;
		unloadClipTime = 0.375F;
		loadClipTime = 0.375F;
		untiltGunTime = 0.150F;


		translateAll(0F, 0F, 0F);

		flipAll();
	}
}