package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelStumbreonSV extends ModelGun
{
	int textureX = 512;
	int textureY = 256;

	public ModelStumbreonSV() 
	{
		gunModel = new ModelRendererTurbo[51];
		gunModel[0] = new ModelRendererTurbo(this, 136, 125, textureX, textureY); // Import ammoReceiver
		gunModel[1] = new ModelRendererTurbo(this, 136, 10, textureX, textureY); // Import barrel1
		gunModel[2] = new ModelRendererTurbo(this, 136, 1, textureX, textureY); // Import barrel2
		gunModel[3] = new ModelRendererTurbo(this, 136, 1, textureX, textureY); // Import barrel2
		gunModel[4] = new ModelRendererTurbo(this, 136, 19, textureX, textureY); // Import body1
		gunModel[5] = new ModelRendererTurbo(this, 158, 81, textureX, textureY); // Import body10
		gunModel[6] = new ModelRendererTurbo(this, 136, 76, textureX, textureY); // Import body11
		gunModel[7] = new ModelRendererTurbo(this, 195, 38, textureX, textureY); // Import body2
		gunModel[8] = new ModelRendererTurbo(this, 136, 38, textureX, textureY); // Import body3
		gunModel[9] = new ModelRendererTurbo(this, 234, 45, textureX, textureY); // Import body4
		gunModel[10] = new ModelRendererTurbo(this, 136, 52, textureX, textureY); // Import body5
		gunModel[11] = new ModelRendererTurbo(this, 136, 64, textureX, textureY); // Import body6
		gunModel[12] = new ModelRendererTurbo(this, 136, 72, textureX, textureY); // Import body7
		gunModel[13] = new ModelRendererTurbo(this, 179, 76, textureX, textureY); // Import body8
		gunModel[14] = new ModelRendererTurbo(this, 158, 76, textureX, textureY); // Import body9
		gunModel[15] = new ModelRendererTurbo(this, 136, 101, textureX, textureY); // Import bolt
		gunModel[16] = new ModelRendererTurbo(this, 136, 101, textureX, textureY); // Import bolt
		gunModel[17] = new ModelRendererTurbo(this, 47, 46, textureX, textureY); // Import canister1
		gunModel[18] = new ModelRendererTurbo(this, 26, 28, textureX, textureY); // Import canister2
		gunModel[19] = new ModelRendererTurbo(this, 26, 19, textureX, textureY); // Import canister3
		gunModel[20] = new ModelRendererTurbo(this, 26, 37, textureX, textureY); // Import canister4
		gunModel[21] = new ModelRendererTurbo(this, 175, 95, textureX, textureY); // Import gasBlock
		gunModel[22] = new ModelRendererTurbo(this, 175, 95, textureX, textureY); // Import gasBlock
		gunModel[23] = new ModelRendererTurbo(this, 136, 94, textureX, textureY); // Import gasBlock2
		gunModel[24] = new ModelRendererTurbo(this, 167, 89, textureX, textureY); // Import gasBlockConnector
		gunModel[25] = new ModelRendererTurbo(this, 1, 56, textureX, textureY); // Import grip
		gunModel[26] = new ModelRendererTurbo(this, 1, 84, textureX, textureY); // Import grip2
		gunModel[27] = new ModelRendererTurbo(this, 26, 1, textureX, textureY); // Import ironSight
		gunModel[28] = new ModelRendererTurbo(this, 26, 1, textureX, textureY); // Import ironSight
		gunModel[29] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Import ironSightBase
		gunModel[30] = new ModelRendererTurbo(this, 26, 6, textureX, textureY); // Import ironSightFront
		gunModel[31] = new ModelRendererTurbo(this, 1, 9, textureX, textureY); // Import ironSightFront2
		gunModel[32] = new ModelRendererTurbo(this, 136, 111, textureX, textureY); // Import lowerReceiver
		gunModel[33] = new ModelRendererTurbo(this, 64, 24, textureX, textureY); // Import meter1
		gunModel[34] = new ModelRendererTurbo(this, 64, 34, textureX, textureY); // Import meter1-2
		gunModel[35] = new ModelRendererTurbo(this, 64, 44, textureX, textureY); // Import meter2
		gunModel[36] = new ModelRendererTurbo(this, 21, 20, textureX, textureY); // Import pipe1
		gunModel[37] = new ModelRendererTurbo(this, 20, 12, textureX, textureY); // Import pipe2
		gunModel[38] = new ModelRendererTurbo(this, 12, 17, textureX, textureY); // Import pipe3
		gunModel[39] = new ModelRendererTurbo(this, 1, 19, textureX, textureY); // Import pipe3
		gunModel[40] = new ModelRendererTurbo(this, 43, 9, textureX, textureY); // Import rail1
		gunModel[41] = new ModelRendererTurbo(this, 43, 1, textureX, textureY); // Import rail2
		gunModel[42] = new ModelRendererTurbo(this, 43, 15, textureX, textureY); // Import rail3
		gunModel[43] = new ModelRendererTurbo(this, 43, 15, textureX, textureY); // Import rail3-2
		gunModel[44] = new ModelRendererTurbo(this, 7, 37, textureX, textureY); // Import receiverValve
		gunModel[45] = new ModelRendererTurbo(this, 7, 37, textureX, textureY); // Import receiverValve
		gunModel[46] = new ModelRendererTurbo(this, 7, 37, textureX, textureY); // Import receiverValve
		gunModel[47] = new ModelRendererTurbo(this, 7, 46, textureX, textureY); // Import receiverValve2
		gunModel[48] = new ModelRendererTurbo(this, 7, 46, textureX, textureY); // Import receiverValve2
		gunModel[49] = new ModelRendererTurbo(this, 7, 26, textureX, textureY); // Import receiverValve3
		gunModel[50] = new ModelRendererTurbo(this, 78, 183, textureX, textureY); // Import stockConnector

		gunModel[0].addShapeBox(0F, 0F, 0F, 16, 2, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F); // Import ammoReceiver
		gunModel[0].setRotationPoint(12.5F, -5F, -5F);

		gunModel[1].addBox(0F, 0F, 0F, 45, 2, 6, 0F); // Import barrel1
		gunModel[1].setRotationPoint(44F, -20F, -3F);

		gunModel[2].addShapeBox(0F, 0F, 0F, 45, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Import barrel2
		gunModel[2].setRotationPoint(44F, -18F, -3F);

		gunModel[3].addShapeBox(0F, 0F, 0F, 45, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import barrel2
		gunModel[3].setRotationPoint(44F, -22F, -3F);

		gunModel[4].addShapeBox(0F, 0F, 0F, 45, 9, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F); // Import body1
		gunModel[4].setRotationPoint(-3F, -14.5F, -4.5F);

		gunModel[5].addBox(0F, 0F, 0F, 6, 2, 1, 0F); // Import body10
		gunModel[5].setRotationPoint(9F, -14.5F, 4F);

		gunModel[6].addBox(0F, 0F, 0F, 6, 8, 9, 0F); // Import body11
		gunModel[6].setRotationPoint(-9F, -14.5F, -4.5F);

		gunModel[7].addBox(0F, 0F, 0F, 10, 4, 9, 0F); // Import body2
		gunModel[7].setRotationPoint(32F, -18.5F, -4.5F);

		gunModel[8].addBox(0F, 0F, 0F, 20, 4, 9, 0F); // Import body3
		gunModel[8].setRotationPoint(-3F, -18.5F, -4.5F);

		gunModel[9].addBox(0F, 0F, 0F, 15, 4, 2, 0F); // Import body4
		gunModel[9].setRotationPoint(17F, -18.5F, 2.5F);

		gunModel[10].addBox(0F, 0F, 0F, 45, 2, 9, 0F); // Import body5
		gunModel[10].setRotationPoint(-3F, -20.5F, -4.5F);

		gunModel[11].addShapeBox(0F, 0F, 0F, 45, 2, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 2F); // Import body6
		gunModel[11].setRotationPoint(-3F, -22.5F, -2.5F);

		gunModel[12].addBox(0F, 0F, 0F, 43, 2, 1, 0F); // Import body7
		gunModel[12].setRotationPoint(-2F, -18.5F, 4F);

		gunModel[13].addBox(0F, 0F, 0F, 9, 3, 1, 0F); // Import body8
		gunModel[13].setRotationPoint(-2F, -16.5F, 4F);

		gunModel[14].addShapeBox(0F, 0F, 0F, 9, 3, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F); // Import body9
		gunModel[14].setRotationPoint(-2F, -13.5F, 4F);

		gunModel[15].addBox(0F, 0F, 0F, 15, 2, 7, 0F); // Import bolt
		gunModel[15].setRotationPoint(17F, -16.5F, -4F);

		gunModel[16].addShapeBox(0F, 0F, 0F, 15, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import bolt
		gunModel[16].setRotationPoint(17F, -18.5F, -4F);

		gunModel[17].addBox(0F, 0F, 0F, 6, 6, 2, 0F); // Import canister1
		gunModel[17].setRotationPoint(-5F, -14.5F, -6.5F);

		gunModel[18].addShapeBox(0F, 0F, 0F, 10, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Import canister2
		gunModel[18].setRotationPoint(-6F, -10.5F, -11F);

		gunModel[19].addBox(0F, 0F, 0F, 10, 2, 6, 0F); // Import canister3
		gunModel[19].setRotationPoint(-6F, -12.5F, -11F);

		gunModel[20].addShapeBox(0F, 0F, 0F, 10, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import canister4
		gunModel[20].setRotationPoint(-6F, -14.5F, -11F);

		gunModel[21].addShapeBox(0F, 0F, 0F, 15, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Import gasBlock
		gunModel[21].setRotationPoint(60F, -11F, -2F);

		gunModel[22].addShapeBox(0F, 0F, 0F, 15, 1, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import gasBlock
		gunModel[22].setRotationPoint(60F, -14F, -2F);

		gunModel[23].addBox(0F, 0F, 0F, 15, 2, 4, 0F); // Import gasBlock2
		gunModel[23].setRotationPoint(60F, -13F, -2F);

		gunModel[24].addBox(0F, 0F, 0F, 5, 2, 2, 0F); // Import gasBlockConnector
		gunModel[24].setRotationPoint(67F, -16F, -1F);

		gunModel[25].addShapeBox(0F, 0F, 0F, 12, 17, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, -2F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 3F, -2F, 0F); // Import grip
		gunModel[25].setRotationPoint(-7.5F, -4F, -5F);

		gunModel[26].addShapeBox(0F, 0F, 0F, 13, 1, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F); // Import grip2
		gunModel[26].setRotationPoint(-8.5F, -5F, -5F);

		gunModel[27].addShapeBox(0F, 0F, 0F, 6, 2, 2, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import ironSight
		gunModel[27].setRotationPoint(-3F, -27.5F, 1F);

		gunModel[28].addShapeBox(0F, 0F, 0F, 6, 2, 2, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import ironSight
		gunModel[28].setRotationPoint(-3F, -27.5F, -3F);

		gunModel[29].addBox(0F, 0F, 0F, 6, 1, 6, 0F); // Import ironSightBase
		gunModel[29].setRotationPoint(-3F, -25.5F, -3F);

		gunModel[30].addBox(0F, 0F, 0F, 5, 2, 2, 0F); // Import ironSightFront
		gunModel[30].setRotationPoint(84F, -24F, -1F);

		gunModel[31].addShapeBox(0F, 0F, 0F, 3, 4, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F); // Import ironSightFront2
		gunModel[31].setRotationPoint(84F, -28F, -1F);

		gunModel[32].addBox(0F, 0F, 0F, 37, 3, 10, 0F); // Import lowerReceiver
		gunModel[32].setRotationPoint(-8.5F, -8F, -5F);

		gunModel[33].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Import meter1
		gunModel[33].setRotationPoint(-4F, -21.5F, -11.5F);

		gunModel[34].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import meter1-2
		gunModel[34].setRotationPoint(-4F, -26.5F, -11.5F);

		gunModel[35].addBox(0F, 0F, 0F, 2, 3, 7, 0F); // Import meter2
		gunModel[35].setRotationPoint(-4F, -24.5F, -11.5F);

		gunModel[36].addBox(0F, 0F, 0F, 3, 2, 2, 0F); // Import pipe1
		gunModel[36].setRotationPoint(4F, -12.5F, -9F);

		gunModel[37].addBox(0F, 0F, 0F, 2, 2, 4, 0F); // Import pipe2
		gunModel[37].setRotationPoint(7F, -12.5F, -9F);

		gunModel[38].addBox(0F, 0F, 0F, 2, 5, 2, 0F); // Import pipe3
		gunModel[38].setRotationPoint(-4F, -19.5F, -9F);

		gunModel[39].addBox(0F, 0F, 0F, 4, 4, 1, 0F); // Import pipe3
		gunModel[39].setRotationPoint(6F, -13.5F, -5.5F);

		gunModel[40].addBox(0F, 0F, 0F, 40, 1, 4, 0F); // Import rail1
		gunModel[40].setRotationPoint(-3F, -23.5F, -2F);

		gunModel[41].addBox(0F, 0F, 0F, 40, 1, 6, 0F); // Import rail2
		gunModel[41].setRotationPoint(-3F, -24.5F, -3F);

		gunModel[42].addShapeBox(0F, 0F, 0F, 34, 1, 1, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import rail3
		gunModel[42].setRotationPoint(3F, -25.5F, 2F);

		gunModel[43].addShapeBox(0F, 0F, 0F, 34, 1, 1, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import rail3-2
		gunModel[43].setRotationPoint(3F, -25.5F, -3F);

		gunModel[44].addShapeBox(0F, 0F, 0F, 3, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import receiverValve
		gunModel[44].setRotationPoint(-6F, -21F, -3F);

		gunModel[45].addShapeBox(0F, 0F, 0F, 3, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Import receiverValve
		gunModel[45].setRotationPoint(-6F, -17F, -3F);

		gunModel[46].addBox(0F, 0F, 0F, 3, 2, 6, 0F); // Import receiverValve
		gunModel[46].setRotationPoint(-6F, -19F, -3F);

		gunModel[47].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Import receiverValve2
		gunModel[47].setRotationPoint(-8F, -16.5F, -3.5F);

		gunModel[48].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import receiverValve2
		gunModel[48].setRotationPoint(-8F, -21.5F, -3.5F);

		gunModel[49].addBox(0F, 0F, 0F, 2, 3, 7, 0F); // Import receiverValve3
		gunModel[49].setRotationPoint(-8F, -19.5F, -3.5F);

		gunModel[50].addBox(0F, 0F, 0F, 5, 6, 10, 0F); // Import stockConnector
		gunModel[50].setRotationPoint(-10F, -14F, -5F);


		defaultStockModel = new ModelRendererTurbo[9];
		defaultStockModel[0] = new ModelRendererTurbo(this, 1, 156, textureX, textureY); // Import stock1
		defaultStockModel[1] = new ModelRendererTurbo(this, 55, 174, textureX, textureY); // Import stock2
		defaultStockModel[2] = new ModelRendererTurbo(this, 1, 144, textureX, textureY); // Import stock3
		defaultStockModel[3] = new ModelRendererTurbo(this, 55, 203, textureX, textureY); // Import stockBag
		defaultStockModel[4] = new ModelRendererTurbo(this, 69, 174, textureX, textureY); // Import stockBag2
		defaultStockModel[5] = new ModelRendererTurbo(this, 28, 175, textureX, textureY); // Import stockStrap1
		defaultStockModel[6] = new ModelRendererTurbo(this, 1, 174, textureX, textureY); // Import stockStrap1
		defaultStockModel[7] = new ModelRendererTurbo(this, 1, 201, textureX, textureY); // Import stockStrap2
		defaultStockModel[8] = new ModelRendererTurbo(this, 28, 201, textureX, textureY); // Import stockStrap2

		defaultStockModel[0].addShapeBox(0F, 0F, 0F, 33, 8, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 8F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 8F, 0F); // Import stock1
		defaultStockModel[0].setRotationPoint(-42F, -14.5F, -4.5F);

		defaultStockModel[1].addBox(0F, 0F, 0F, 2, 16, 9, 0F); // Import stock2
		defaultStockModel[1].setRotationPoint(-44F, -14.5F, -4.5F);

		defaultStockModel[2].addShapeBox(0F, 0F, 0F, 35, 2, 9, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import stock3
		defaultStockModel[2].setRotationPoint(-44F, -16.5F, -4.5F);

		defaultStockModel[3].addBox(0F, 0F, 0F, 13, 8, 2, 0F); // Import stockBag
		defaultStockModel[3].setRotationPoint(-38.5F, -11.5F, -6F);

		defaultStockModel[4].addShapeBox(0F, 0F, 0F, 13, 4, 3, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import stockBag2
		defaultStockModel[4].setRotationPoint(-38.5F, -14.5F, -6.5F);

		defaultStockModel[5].addShapeBox(0F, 0F, 0F, 3, 14, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -0.5F, 0F); // Import stockStrap1
		defaultStockModel[5].setRotationPoint(-29F, -14.5F, -5F);

		defaultStockModel[6].addShapeBox(0F, 0F, 0F, 3, 16, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -0.5F, 0F); // Import stockStrap1
		defaultStockModel[6].setRotationPoint(-38F, -14.5F, -5F);

		defaultStockModel[7].addShapeBox(0F, 0F, 0F, 3, 2, 10, 0F, 0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import stockStrap2
		defaultStockModel[7].setRotationPoint(-29F, -16.5F, -5F);

		defaultStockModel[8].addShapeBox(0F, 0F, 0F, 3, 2, 10, 0F, 0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import stockStrap2
		defaultStockModel[8].setRotationPoint(-38F, -16.5F, -5F);


		defaultGripModel = new ModelRendererTurbo[12];
		defaultGripModel[0] = new ModelRendererTurbo(this, 1, 112, textureX, textureY); // Import foreGrip1
		defaultGripModel[1] = new ModelRendererTurbo(this, 1, 127, textureX, textureY); // Import foreGrip2
		defaultGripModel[2] = new ModelRendererTurbo(this, 52, 114, textureX, textureY); // Import foreGrip3
		defaultGripModel[3] = new ModelRendererTurbo(this, 52, 125, textureX, textureY); // Import foreGrip3
		defaultGripModel[4] = new ModelRendererTurbo(this, 52, 114, textureX, textureY); // Import foreGrip3
		defaultGripModel[5] = new ModelRendererTurbo(this, 52, 114, textureX, textureY); // Import foreGrip3
		defaultGripModel[6] = new ModelRendererTurbo(this, 52, 114, textureX, textureY); // Import foreGrip3
		defaultGripModel[7] = new ModelRendererTurbo(this, 52, 125, textureX, textureY); // Import foreGrip3
		defaultGripModel[8] = new ModelRendererTurbo(this, 52, 125, textureX, textureY); // Import foreGrip3
		defaultGripModel[9] = new ModelRendererTurbo(this, 52, 125, textureX, textureY); // Import foreGrip3
		defaultGripModel[10] = new ModelRendererTurbo(this, 52, 114, textureX, textureY); // Import foreGrip3
		defaultGripModel[11] = new ModelRendererTurbo(this, 52, 125, textureX, textureY); // Import foreGrip3

		defaultGripModel[0].addBox(0F, 0F, 0F, 18, 7, 7, 0F); // Import foreGrip1
		defaultGripModel[0].setRotationPoint(42F, -17.5F, -3.5F);

		defaultGripModel[1].addShapeBox(0F, 0F, 0F, 18, 2, 5, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import foreGrip2
		defaultGripModel[1].setRotationPoint(42F, -10.5F, -2.5F);

		defaultGripModel[2].addBox(0F, 0F, 0F, 2, 3, 7, 0F); // Import foreGrip3
		defaultGripModel[2].setRotationPoint(46F, -20.5F, -3.5F);

		defaultGripModel[3].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import foreGrip3
		defaultGripModel[3].setRotationPoint(42F, -22.5F, -3.5F);

		defaultGripModel[4].addBox(0F, 0F, 0F, 2, 3, 7, 0F); // Import foreGrip3
		defaultGripModel[4].setRotationPoint(42F, -20.5F, -3.5F);

		defaultGripModel[5].addBox(0F, 0F, 0F, 2, 3, 7, 0F); // Import foreGrip3
		defaultGripModel[5].setRotationPoint(50F, -20.5F, -3.5F);

		defaultGripModel[6].addBox(0F, 0F, 0F, 2, 3, 7, 0F); // Import foreGrip3
		defaultGripModel[6].setRotationPoint(58F, -20.5F, -3.5F);

		defaultGripModel[7].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import foreGrip3
		defaultGripModel[7].setRotationPoint(58F, -22.5F, -3.5F);

		defaultGripModel[8].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import foreGrip3
		defaultGripModel[8].setRotationPoint(54F, -22.5F, -3.5F);

		defaultGripModel[9].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import foreGrip3
		defaultGripModel[9].setRotationPoint(50F, -22.5F, -3.5F);

		defaultGripModel[10].addBox(0F, 0F, 0F, 2, 3, 7, 0F); // Import foreGrip3
		defaultGripModel[10].setRotationPoint(54F, -20.5F, -3.5F);

		defaultGripModel[11].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import foreGrip3
		defaultGripModel[11].setRotationPoint(46F, -22.5F, -3.5F);


		ammoModel = new ModelRendererTurbo[5];
		ammoModel[0] = new ModelRendererTurbo(this, 48, 56, textureX, textureY); // Import ammoClip
		ammoModel[1] = new ModelRendererTurbo(this, 48, 99, textureX, textureY); // Import ammoClip2
		ammoModel[2] = new ModelRendererTurbo(this, 19, 105, textureX, textureY); // Import bullet
		ammoModel[3] = new ModelRendererTurbo(this, 33, 99, textureX, textureY); // Import bulletTip
		ammoModel[4] = new ModelRendererTurbo(this, 48, 85, textureX, textureY); // Import strap

		ammoModel[0].addBox(0F, 0F, 0F, 14, 20, 8, 0F); // Import ammoClip
		ammoModel[0].setRotationPoint(13.5F, -5F, -4F);
		ammoModel[0].rotateAngleZ = 0.06981317F;

		ammoModel[1].addBox(0F, 0F, 0F, 15, 2, 9, 0F); // Import ammoClip2
		ammoModel[1].setRotationPoint(14.5F, 15F, -4.5F);
		ammoModel[1].rotateAngleZ = 0.06981317F;

		ammoModel[2].addShapeBox(0F, 0F, 0F, 10, 1, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import bullet
		ammoModel[2].setRotationPoint(14F, -6F, -1.5F);
		ammoModel[2].rotateAngleZ = 0.06981317F;

		ammoModel[3].addShapeBox(0F, 0F, 0F, 3, 1, 4, 0F, 0F, 0F, -1F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F); // Import bulletTip
		ammoModel[3].setRotationPoint(24F, -6.7F, -1.5F);
		ammoModel[3].rotateAngleZ = 0.06981317F;

		ammoModel[4].addBox(0F, 0F, 0F, 15, 4, 9, 0F); // Import strap
		ammoModel[4].setRotationPoint(13.5F, 3F, -4.5F);
		ammoModel[4].rotateAngleZ = 0.06981317F;


		slideModel = new ModelRendererTurbo[2];
		slideModel[0] = new ModelRendererTurbo(this, 97, 48, textureX, textureY); // Import slider
		slideModel[1] = new ModelRendererTurbo(this, 83, 47, textureX, textureY); // Import slider2

		slideModel[0].addBox(0F, 0F, 0F, 3, 4, 2, 0F); // Import slider
		slideModel[0].setRotationPoint(29F, -18.5F, -4.2F);

		slideModel[1].addBox(0F, 0F, 0F, 2, 2, 5, 0F); // Import slider2
		slideModel[1].setRotationPoint(30F, -17.5F, -9F);

		barrelAttachPoint = new Vector3f(89F /16F, 19F /16F, 0F /16F);
		stockAttachPoint = new Vector3f(-9F /16F, 10F /16F, 0F /16F);
		scopeAttachPoint = new Vector3f(17F /16F, 23F /16F, 0F /16F);
		gripAttachPoint = new Vector3f(42 /16F, 15F /16F, 0F /16F);

		gunSlideDistance = 0.5F;
		animationType = EnumAnimationType.BOTTOM_CLIP;

		translateAll(0F, 0F, 0F);

		flipAll();
	}
}