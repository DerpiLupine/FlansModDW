package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelPUP762 extends ModelGun
{
	int textureX = 512;
	int textureY = 256;

	public ModelPUP762()
	{
		gunModel = new ModelRendererTurbo[9];
		gunModel[0] = new ModelRendererTurbo(this, 123, 57, textureX, textureY); // Box 15
		gunModel[1] = new ModelRendererTurbo(this, 123, 100, textureX, textureY); // Box 16
		gunModel[2] = new ModelRendererTurbo(this, 90, 93, textureX, textureY); // Box 2
		gunModel[3] = new ModelRendererTurbo(this, 123, 69, textureX, textureY); // Box 20
		gunModel[4] = new ModelRendererTurbo(this, 157, 37, textureX, textureY); // Box 3
		gunModel[5] = new ModelRendererTurbo(this, 157, 16, textureX, textureY); // Box 4
		gunModel[6] = new ModelRendererTurbo(this, 164, 58, textureX, textureY); // Box 5
		gunModel[7] = new ModelRendererTurbo(this, 90, 73, textureX, textureY); // Box 6
		gunModel[8] = new ModelRendererTurbo(this, 90, 57, textureX, textureY); // Box 8

		gunModel[0].addShapeBox(0F, 0F, 0F, 12, 3, 8, 0F,0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 15
		gunModel[0].setRotationPoint(8F, -18F, -4F);

		gunModel[1].addShapeBox(0F, 0F, 0F, 5, 3, 8, 0F,0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 16
		gunModel[1].setRotationPoint(-11F, -18F, -4F);

		gunModel[2].addBox(0F, 0F, 0F, 5, 14, 8, 0F); // Box 2
		gunModel[2].setRotationPoint(-11F, -15F, -4F);

		gunModel[3].addBox(0F, 0F, 0F, 10, 2, 8, 0F); // Box 20
		gunModel[3].setRotationPoint(-19F, -1F, -4F);

		gunModel[4].addBox(0F, 0F, 0F, 8, 12, 8, 0F); // Box 3
		gunModel[4].setRotationPoint(-6F, -13F, -4F);

		gunModel[5].addShapeBox(0F, 0F, 0F, 12, 12, 8, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F); // Box 4
		gunModel[5].setRotationPoint(2F, -13F, -4F);

		gunModel[6].addBox(0F, 0F, 0F, 6, 2, 8, 0F); // Box 5
		gunModel[6].setRotationPoint(8F, -15F, -4F);

		gunModel[7].addBox(0F, 0F, 0F, 6, 11, 8, 0F); // Box 6
		gunModel[7].setRotationPoint(14F, -15F, -4F);

		gunModel[8].addShapeBox(0F, 0F, 0F, 8, 7, 8, 0F,0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 8
		gunModel[8].setRotationPoint(-19F, -13F, -4F);


		defaultBarrelModel = new ModelRendererTurbo[6];
		defaultBarrelModel[0] = new ModelRendererTurbo(this, 90, 116, textureX, textureY); // barrel2
		defaultBarrelModel[1] = new ModelRendererTurbo(this, 90, 1, textureX, textureY); // barrel1
		defaultBarrelModel[2] = new ModelRendererTurbo(this, 90, 1, textureX, textureY); // barrel1
		defaultBarrelModel[3] = new ModelRendererTurbo(this, 90, 10, textureX, textureY); // muzzle
		defaultBarrelModel[4] = new ModelRendererTurbo(this, 90, 10, textureX, textureY); // muzzle
		defaultBarrelModel[5] = new ModelRendererTurbo(this, 90, 10, textureX, textureY); // muzzle

		defaultBarrelModel[0].addBox(0F, 0F, 0F, 64, 6, 2, 0F); // barrel2
		defaultBarrelModel[0].setRotationPoint(20F, -17F, -1F);

		defaultBarrelModel[1].addShapeBox(0F, 0F, 0F, 64, 6, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F); // barrel1
		defaultBarrelModel[1].setRotationPoint(20F, -17F, 1F);

		defaultBarrelModel[2].addShapeBox(0F, 0F, 0F, 64, 6, 2, 0F,0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // barrel1
		defaultBarrelModel[2].setRotationPoint(20F, -17F, -3F);

		defaultBarrelModel[3].addBox(0F, 0F, 0F, 10, 2, 7, 0F); // muzzle
		defaultBarrelModel[3].setRotationPoint(71F, -15F, -3.5F);

		defaultBarrelModel[4].addShapeBox(0F, 0F, 0F, 10, 2, 7, 0F,0F, 0.25F, -2.25F, 0F, 0.25F, -2.25F, 0F, 0.25F, -2.25F, 0F, 0.25F, -2.25F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // muzzle
		defaultBarrelModel[4].setRotationPoint(71F, -17F, -3.5F);

		defaultBarrelModel[5].addShapeBox(0F, 0F, 0F, 10, 2, 7, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.25F, -2.25F, 0F, 0.25F, -2.25F, 0F, 0.25F, -2.25F, 0F, 0.25F, -2.25F); // muzzle
		defaultBarrelModel[5].setRotationPoint(71F, -13F, -3.5F);


		defaultScopeModel = new ModelRendererTurbo[7];
		defaultScopeModel[0] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // ironSight
		defaultScopeModel[1] = new ModelRendererTurbo(this, 16, 2, textureX, textureY); // ironSight3
		defaultScopeModel[2] = new ModelRendererTurbo(this, 1, 8, textureX, textureY); // ironSight2
		defaultScopeModel[3] = new ModelRendererTurbo(this, 1, 8, textureX, textureY); // ironSight2
		defaultScopeModel[4] = new ModelRendererTurbo(this, 1, 8, textureX, textureY); // ironSight2
		defaultScopeModel[5] = new ModelRendererTurbo(this, 1, 8, textureX, textureY); // ironSight2
		defaultScopeModel[6] = new ModelRendererTurbo(this, 10, 8, textureX, textureY); // ironSight4

		defaultScopeModel[0].addBox(0F, 0F, 0F, 3, 2, 4, 0F); // ironSight
		defaultScopeModel[0].setRotationPoint(16F, -20F, -2F);

		defaultScopeModel[1].addBox(0F, 0F, 0F, 3, 1, 4, 0F); // ironSight3
		defaultScopeModel[1].setRotationPoint(16F, -25F, -2F);

		defaultScopeModel[2].addShapeBox(0F, 0F, 0F, 3, 3, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1.5F, 0F, 0F, 1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F); // ironSight2
		defaultScopeModel[2].setRotationPoint(16F, -25F, -2F);

		defaultScopeModel[3].addShapeBox(0F, 0F, 0F, 3, 3, 1, 0F,0F, 0F, 1.5F, 0F, 0F, 1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // ironSight2
		defaultScopeModel[3].setRotationPoint(16F, -22F, -2F);

		defaultScopeModel[4].addShapeBox(0F, 0F, 0F, 3, 3, 1, 0F,0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 1.5F, 0F, 0F, 1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // ironSight2
		defaultScopeModel[4].setRotationPoint(16F, -22F, 1F);

		defaultScopeModel[5].addShapeBox(0F, 0F, 0F, 3, 3, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 1.5F, 0F, 0F, 1.5F); // ironSight2
		defaultScopeModel[5].setRotationPoint(16F, -25F, 1F);

		defaultScopeModel[6].addBox(0F, 0F, 0F, 3, 2, 1, 0F); // ironSight4
		defaultScopeModel[6].setRotationPoint(16F, -22F, -0.5F);


		defaultStockModel = new ModelRendererTurbo[7];
		defaultStockModel[0] = new ModelRendererTurbo(this, 1, 53, textureX, textureY); // stock
		defaultStockModel[1] = new ModelRendererTurbo(this, 1, 98, textureX, textureY); // stock2
		defaultStockModel[2] = new ModelRendererTurbo(this, 1, 109, textureX, textureY); // stock3
		defaultStockModel[3] = new ModelRendererTurbo(this, 46, 98, textureX, textureY); // stock6
		defaultStockModel[4] = new ModelRendererTurbo(this, 1, 72, textureX, textureY); // stock4
		defaultStockModel[5] = new ModelRendererTurbo(this, 46, 98, textureX, textureY); // stock6
		defaultStockModel[6] = new ModelRendererTurbo(this, 22, 77, textureX, textureY); // stock5grip

		defaultStockModel[0].addShapeBox(0F, 0F, 0F, 28, 10, 8, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 6F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 6F, 0F); // stock
		defaultStockModel[0].setRotationPoint(-47F, -11F, -4F);

		defaultStockModel[1].addShapeBox(0F, 0F, 0F, 14, 2, 8, 0F,0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F); // stock2
		defaultStockModel[1].setRotationPoint(-45F, -13F, -4F);

		defaultStockModel[2].addBox(0F, 0F, 0F, 8, 3, 8, 0F); // stock3
		defaultStockModel[2].setRotationPoint(-47F, 3F, -4F);

		defaultStockModel[3].addShapeBox(0F, 0F, 0F, 2, 2, 8, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // stock6
		defaultStockModel[3].setRotationPoint(-49F, 6F, -4F);

		defaultStockModel[4].addBox(0F, 0F, 0F, 2, 17, 8, 0F); // stock4
		defaultStockModel[4].setRotationPoint(-49F, -11F, -4F);

		defaultStockModel[5].addShapeBox(0F, 0F, 0F, 2, 2, 8, 0F,0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // stock6
		defaultStockModel[5].setRotationPoint(-49F, -13F, -4F);

		defaultStockModel[6].addShapeBox(0F, 0F, 0F, 8, 12, 8, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, -2F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 4F, -2F, 0F); // stock5grip
		defaultStockModel[6].setRotationPoint(-26F, -1F, -4F);


		defaultGripModel = new ModelRendererTurbo[4];
		defaultGripModel[0] = new ModelRendererTurbo(this, 90, 48, textureX, textureY); // Box 10
		defaultGripModel[1] = new ModelRendererTurbo(this, 123, 80, textureX, textureY); // Box 11
		defaultGripModel[2] = new ModelRendererTurbo(this, 90, 37, textureX, textureY); // Box 7
		defaultGripModel[3] = new ModelRendererTurbo(this, 90, 20, textureX, textureY); // Box 9

		defaultGripModel[0].addBox(0F, 0F, 0F, 25, 1, 7, 0F); // Box 10
		defaultGripModel[0].setRotationPoint(20F, -13F, -3.5F);

		defaultGripModel[1].addBox(0F, 0F, 0F, 2, 11, 8, 0F); // Box 11
		defaultGripModel[1].setRotationPoint(45F, -15F, -4F);

		defaultGripModel[2].addBox(0F, 0F, 0F, 25, 2, 8, 0F); // Box 7
		defaultGripModel[2].setRotationPoint(20F, -15F, -4F);

		defaultGripModel[3].addBox(0F, 0F, 0F, 25, 8, 8, 0F); // Box 9
		defaultGripModel[3].setRotationPoint(20F, -12F, -4F);


		ammoModel = new ModelRendererTurbo[4];
		ammoModel[0] = new ModelRendererTurbo(this, 1, 34, textureX, textureY); // clip
		ammoModel[1] = new ModelRendererTurbo(this, 55, 46, textureX, textureY); // bullet
		ammoModel[2] = new ModelRendererTurbo(this, 55, 39, textureX, textureY); // bulletTip
		ammoModel[3] = new ModelRendererTurbo(this, 38, 35, textureX, textureY); // clip2

		ammoModel[0].addBox(0F, 0F, 0F, 12, 12, 6, 0F); // clip
		ammoModel[0].setRotationPoint(-6F, -6F, -3F);

		ammoModel[1].addShapeBox(0F, 0F, 0F, 10, 1, 5, 0F,0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // bullet
		ammoModel[1].setRotationPoint(-5F, -7F, -2.5F);

		ammoModel[2].addShapeBox(0F, 0F, 0F, 3, 1, 5, 0F,0F, 0F, -1.5F, 0F, -0.5F, -2F, 0F, -0.5F, -2F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F); // bulletTip
		ammoModel[2].setRotationPoint(5F, -7F, -2.5F);

		ammoModel[3].addBox(0F, 0F, 0F, 3, 12, 5, 0F); // clip2
		ammoModel[3].setRotationPoint(6F, -6F, -2.5F);


		pumpModel = new ModelRendererTurbo[8];
		pumpModel[0] = new ModelRendererTurbo(this, 60, 12, textureX, textureY); // bolt4
		pumpModel[1] = new ModelRendererTurbo(this, 1, 24, textureX, textureY); // bolt2
		pumpModel[2] = new ModelRendererTurbo(this, 1, 13, textureX, textureY); // bolt1
		pumpModel[3] = new ModelRendererTurbo(this, 60, 12, textureX, textureY); // bolt3
		pumpModel[4] = new ModelRendererTurbo(this, 60, 24, textureX, textureY); // handle1
		pumpModel[5] = new ModelRendererTurbo(this, 60, 24, textureX, textureY); // handle1
		pumpModel[6] = new ModelRendererTurbo(this, 60, 24, textureX, textureY); // handle1
		pumpModel[7] = new ModelRendererTurbo(this, 53, 13, textureX, textureY); // handle2

		pumpModel[0].addShapeBox(0F, 0F, 0F, 3, 3, 8, 0F,0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // bolt4
		pumpModel[0].setRotationPoint(-17F, -18F, -4F);

		pumpModel[1].addShapeBox(0F, 0F, 0F, 22, 2, 7, 0F,0F, 0.25F, -2.25F, 0F, 0.25F, -2.25F, 0F, 0.25F, -2.25F, 0F, 0.25F, -2.25F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // bolt2
		pumpModel[1].setRotationPoint(-14F, -17F, -3.5F);

		pumpModel[2].addBox(0F, 0F, 0F, 22, 3, 7, 0F); // bolt1
		pumpModel[2].setRotationPoint(-14F, -15F, -3.5F);

		pumpModel[3].addBox(0F, 0F, 0F, 3, 3, 8, 0F); // bolt3
		pumpModel[3].setRotationPoint(-17F, -15F, -4F);

		pumpModel[4].addShapeBox(0F, 0F, 0F, 3, 1, 5, 0F,-1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // handle1
		pumpModel[4].setRotationPoint(-14F, -16F, -7.5F);

		pumpModel[5].addShapeBox(0F, 0F, 0F, 3, 1, 5, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // handle1
		pumpModel[5].setRotationPoint(-14F, -14F, -7.5F);

		pumpModel[6].addBox(0F, 0F, 0F, 3, 1, 5, 0F); // handle1
		pumpModel[6].setRotationPoint(-14F, -15F, -7.5F);

		pumpModel[7].addBox(0F, 0F, 0F, 3, 3, 3, 0F); // handle2
		pumpModel[7].setRotationPoint(-14F, -16F, -10.5F);



		barrelAttachPoint = new Vector3f(0F /16F, 0F /16F, 0F /16F);
		stockAttachPoint = new Vector3f(0F /16F, 0F /16F, 0F /16F);
		scopeAttachPoint = new Vector3f(0F, -0.1F, 0F);
		gripAttachPoint = new Vector3f(0 /16F, 0F /16F, 0F /16F);

		gunSlideDistance = 0F;
		animationType = EnumAnimationType.BOTTOM_CLIP;
		pumpDelay = 15;
		pumpTime = 9;
		pumpHandleDistance = 0.7F;


		translateAll(10F, -6F, 0F);


		flipAll();
	}
}