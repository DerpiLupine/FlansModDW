package com.flansmod.client.model.dwfirearms;

import net.minecraft.entity.Entity;

import com.flansmod.client.model.ModelBullet;
import com.flansmod.client.tmt.ModelRendererTurbo;

public class ModelBulletDrone extends ModelBullet
{
	public ModelRendererTurbo[] bulletModel;

	public ModelBulletDrone()
	{
		int textureX = 64;
		int textureY = 32;

		bulletModel = new ModelRendererTurbo[6];
		bulletModel[0] = new ModelRendererTurbo(this, 13, 1, textureX, textureY); // Box 0
		bulletModel[1] = new ModelRendererTurbo(this, 0, 25, textureX, textureY); // Box 0
		bulletModel[2] = new ModelRendererTurbo(this, 13, 26, textureX, textureY); // Box 0
		bulletModel[3] = new ModelRendererTurbo(this, 13, 17, textureX, textureY); // Box 3
		bulletModel[4] = new ModelRendererTurbo(this, 13, 26, textureX, textureY); // Box 0
		bulletModel[5] = new ModelRendererTurbo(this, 13, 17, textureX, textureY); // Box 1

		bulletModel[0].addShapeBox(0F, 0F, 0F, 4, 11, 4, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -1.5F, -0.5F, 0F, -1.5F, -0.5F, 0F, -1.5F, 0F, -0.5F, -1.5F, 0F); // Box 0
		bulletModel[0].setRotationPoint(-2F, -5.5F, -2F);

		bulletModel[1].addShapeBox(0F, 0F, 0F, 3, 3, 3, 0F, 0.25F, 0F, 0.25F, 0.25F, 0F, 0.25F, 0.25F, 0F, 0.25F, 0.25F, 0F, 0.25F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F); // Box 0
		bulletModel[1].setRotationPoint(-1.25F, 4F, -1.25F);

		bulletModel[2].addShapeBox(0F, 0F, 0F, 4, 3, 2, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -0.5F, -2F, -0.5F); // Box 0
		bulletModel[2].setRotationPoint(-5.5F, -5.5F, -0.5F);

		bulletModel[3].addShapeBox(0F, 0F, 0F, 7, 4, 2, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -0.5F, -2F, -0.5F); // Box 3
		bulletModel[3].setRotationPoint(-8.5F, 0F, -0.5F);

		bulletModel[4].addShapeBox(0F, 0F, 0F, 4, 3, 2, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, -0.5F, -2F, 0F, -0.5F, -2F, -0.5F, 0F, 0F, -0.5F); // Box 0
		bulletModel[4].setRotationPoint(2F, -5.5F, -0.5F);

		bulletModel[5].addShapeBox(0F, 0F, 0F, 7, 4, 2, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, -0.5F, -2F, 0F, -0.5F, -2F, -0.5F, 0F, 0F, -0.5F); // Box 1
		bulletModel[5].setRotationPoint(2F, 0F, -0.5F);
	}

	@Override
	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
	{
		//GL11.glScalef(0.5F, 0.5F, 0.5F);
		for(ModelRendererTurbo mrt : bulletModel)
			mrt.render(f5);
	}
}