package com.flansmod.client.model.dwfirearms; //Path where the model is located

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelFlightchaser extends ModelGun //Same as Filename
{
	int textureX = 512;
	int textureY = 256;

	public ModelFlightchaser() //Same as Filename
	{
		gunModel = new ModelRendererTurbo[175];
		gunModel[0] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 2
		gunModel[1] = new ModelRendererTurbo(this, 1, 22, textureX, textureY); // Box 4
		gunModel[2] = new ModelRendererTurbo(this, 1, 41, textureX, textureY); // Box 5
		gunModel[3] = new ModelRendererTurbo(this, 1, 22, textureX, textureY); // Box 6
		gunModel[4] = new ModelRendererTurbo(this, 1, 41, textureX, textureY); // Box 7
		gunModel[5] = new ModelRendererTurbo(this, 127, 1, textureX, textureY); // Box 0
		gunModel[6] = new ModelRendererTurbo(this, 127, 25, textureX, textureY); // Box 2
		gunModel[7] = new ModelRendererTurbo(this, 127, 46, textureX, textureY); // Box 3
		gunModel[8] = new ModelRendererTurbo(this, 127, 25, textureX, textureY); // Box 4
		gunModel[9] = new ModelRendererTurbo(this, 127, 46, textureX, textureY); // Box 5
		gunModel[10] = new ModelRendererTurbo(this, 72, 25, textureX, textureY); // Box 6
		gunModel[11] = new ModelRendererTurbo(this, 72, 46, textureX, textureY); // Box 7
		gunModel[12] = new ModelRendererTurbo(this, 72, 1, textureX, textureY); // Box 8
		gunModel[13] = new ModelRendererTurbo(this, 72, 25, textureX, textureY); // Box 9
		gunModel[14] = new ModelRendererTurbo(this, 72, 46, textureX, textureY); // Box 10
		gunModel[15] = new ModelRendererTurbo(this, 271, 59, textureX, textureY); // Box 11
		gunModel[16] = new ModelRendererTurbo(this, 1, 55, textureX, textureY); // Box 12
		gunModel[17] = new ModelRendererTurbo(this, 96, 67, textureX, textureY); // Box 13
		gunModel[18] = new ModelRendererTurbo(this, 226, 58, textureX, textureY); // Box 14
		gunModel[19] = new ModelRendererTurbo(this, 168, 58, textureX, textureY); // Box 15
		gunModel[20] = new ModelRendererTurbo(this, 96, 87, textureX, textureY); // Box 16
		gunModel[21] = new ModelRendererTurbo(this, 1, 87, textureX, textureY); // Box 17
		gunModel[22] = new ModelRendererTurbo(this, 226, 94, textureX, textureY); // Box 18
		gunModel[23] = new ModelRendererTurbo(this, 171, 90, textureX, textureY); // Box 19
		gunModel[24] = new ModelRendererTurbo(this, 1, 110, textureX, textureY); // Box 20
		gunModel[25] = new ModelRendererTurbo(this, 171, 108, textureX, textureY); // Box 21
		gunModel[26] = new ModelRendererTurbo(this, 98, 110, textureX, textureY); // Box 22
		gunModel[27] = new ModelRendererTurbo(this, 1, 130, textureX, textureY); // Box 28
		gunModel[28] = new ModelRendererTurbo(this, 265, 94, textureX, textureY); // Box 29
		gunModel[29] = new ModelRendererTurbo(this, 226, 108, textureX, textureY); // Box 30
		gunModel[30] = new ModelRendererTurbo(this, 171, 128, textureX, textureY); // Box 31
		gunModel[31] = new ModelRendererTurbo(this, 259, 25, textureX, textureY); // Box 33
		gunModel[32] = new ModelRendererTurbo(this, 259, 44, textureX, textureY); // Box 34
		gunModel[33] = new ModelRendererTurbo(this, 259, 4, textureX, textureY); // Box 35
		gunModel[34] = new ModelRendererTurbo(this, 259, 25, textureX, textureY); // Box 36
		gunModel[35] = new ModelRendererTurbo(this, 259, 44, textureX, textureY); // Box 37
		gunModel[36] = new ModelRendererTurbo(this, 287, 114, textureX, textureY); // Box 53
		gunModel[37] = new ModelRendererTurbo(this, 1, 144, textureX, textureY); // Box 54
		gunModel[38] = new ModelRendererTurbo(this, 120, 132, textureX, textureY); // Box 113
		gunModel[39] = new ModelRendererTurbo(this, 120, 132, textureX, textureY); // Box 114
		gunModel[40] = new ModelRendererTurbo(this, 369, 1, textureX, textureY); // Box 72
		gunModel[41] = new ModelRendererTurbo(this, 369, 46, textureX, textureY); // Box 73
		gunModel[42] = new ModelRendererTurbo(this, 369, 46, textureX, textureY); // Box 76
		gunModel[43] = new ModelRendererTurbo(this, 401, 77, textureX, textureY); // Box 77
		gunModel[44] = new ModelRendererTurbo(this, 1, 167, textureX, textureY); // Box 86
		gunModel[45] = new ModelRendererTurbo(this, 1, 154, textureX, textureY); // Box 87
		gunModel[46] = new ModelRendererTurbo(this, 349, 103, textureX, textureY); // Box 88
		gunModel[47] = new ModelRendererTurbo(this, 349, 91, textureX, textureY); // Box 89
		gunModel[48] = new ModelRendererTurbo(this, 334, 95, textureX, textureY); // Box 90
		gunModel[49] = new ModelRendererTurbo(this, 52, 164, textureX, textureY); // Box 1
		gunModel[50] = new ModelRendererTurbo(this, 35, 182, textureX, textureY); // Box 2
		gunModel[51] = new ModelRendererTurbo(this, 18, 182, textureX, textureY); // Box 3
		gunModel[52] = new ModelRendererTurbo(this, 1, 177, textureX, textureY); // Box 5
		gunModel[53] = new ModelRendererTurbo(this, 60, 176, textureX, textureY); // Box 6
		gunModel[54] = new ModelRendererTurbo(this, 85, 176, textureX, textureY); // Box 7
		gunModel[55] = new ModelRendererTurbo(this, 60, 186, textureX, textureY); // Box 8
		gunModel[56] = new ModelRendererTurbo(this, 85, 186, textureX, textureY); // Box 9
		gunModel[57] = new ModelRendererTurbo(this, 60, 196, textureX, textureY); // Box 10
		gunModel[58] = new ModelRendererTurbo(this, 85, 196, textureX, textureY); // Box 11
		gunModel[59] = new ModelRendererTurbo(this, 369, 25, textureX, textureY); // Box 13
		gunModel[60] = new ModelRendererTurbo(this, 369, 25, textureX, textureY); // Box 15
		gunModel[61] = new ModelRendererTurbo(this, 461, 4, textureX, textureY); // Box 18
		gunModel[62] = new ModelRendererTurbo(this, 461, 27, textureX, textureY); // Box 19
		gunModel[63] = new ModelRendererTurbo(this, 461, 48, textureX, textureY); // Box 20
		gunModel[64] = new ModelRendererTurbo(this, 461, 27, textureX, textureY); // Box 21
		gunModel[65] = new ModelRendererTurbo(this, 461, 48, textureX, textureY); // Box 22
		gunModel[66] = new ModelRendererTurbo(this, 410, 46, textureX, textureY); // Box 23
		gunModel[67] = new ModelRendererTurbo(this, 410, 25, textureX, textureY); // Box 24
		gunModel[68] = new ModelRendererTurbo(this, 410, 1, textureX, textureY); // Box 25
		gunModel[69] = new ModelRendererTurbo(this, 410, 25, textureX, textureY); // Box 26
		gunModel[70] = new ModelRendererTurbo(this, 410, 46, textureX, textureY); // Box 27
		gunModel[71] = new ModelRendererTurbo(this, 120, 132, textureX, textureY); // Box 28
		gunModel[72] = new ModelRendererTurbo(this, 120, 132, textureX, textureY); // Box 29
		gunModel[73] = new ModelRendererTurbo(this, 120, 132, textureX, textureY); // Box 30
		gunModel[74] = new ModelRendererTurbo(this, 120, 132, textureX, textureY); // Box 31
		gunModel[75] = new ModelRendererTurbo(this, 145, 134, textureX, textureY); // Box 32
		gunModel[76] = new ModelRendererTurbo(this, 145, 134, textureX, textureY); // Box 33
		gunModel[77] = new ModelRendererTurbo(this, 145, 134, textureX, textureY); // Box 34
		gunModel[78] = new ModelRendererTurbo(this, 145, 134, textureX, textureY); // Box 35
		gunModel[79] = new ModelRendererTurbo(this, 177, 194, textureX, textureY); // Box 67
		gunModel[80] = new ModelRendererTurbo(this, 193, 185, textureX, textureY); // Box 68
		gunModel[81] = new ModelRendererTurbo(this, 186, 184, textureX, textureY); // Box 69
		gunModel[82] = new ModelRendererTurbo(this, 202, 190, textureX, textureY); // Box 70
		gunModel[83] = new ModelRendererTurbo(this, 202, 190, textureX, textureY); // Box 71
		gunModel[84] = new ModelRendererTurbo(this, 202, 186, textureX, textureY); // Box 72
		gunModel[85] = new ModelRendererTurbo(this, 202, 186, textureX, textureY); // Box 73
		gunModel[86] = new ModelRendererTurbo(this, 102, 144, textureX, textureY); // Box 74
		gunModel[87] = new ModelRendererTurbo(this, 177, 169, textureX, textureY); // Box 78
		gunModel[88] = new ModelRendererTurbo(this, 177, 169, textureX, textureY); // Box 134
		gunModel[89] = new ModelRendererTurbo(this, 186, 184, textureX, textureY); // Box 135
		gunModel[90] = new ModelRendererTurbo(this, 200, 196, textureX, textureY); // Box 136
		gunModel[91] = new ModelRendererTurbo(this, 213, 199, textureX, textureY); // Box 137
		gunModel[92] = new ModelRendererTurbo(this, 213, 202, textureX, textureY); // Box 138
		gunModel[93] = new ModelRendererTurbo(this, 177, 183, textureX, textureY); // Box 36
		gunModel[94] = new ModelRendererTurbo(this, 177, 178, textureX, textureY); // Box 37
		gunModel[95] = new ModelRendererTurbo(this, 177, 178, textureX, textureY); // Box 38
		gunModel[96] = new ModelRendererTurbo(this, 177, 178, textureX, textureY); // Box 39
		gunModel[97] = new ModelRendererTurbo(this, 177, 178, textureX, textureY); // Box 40
		gunModel[98] = new ModelRendererTurbo(this, 216, 148, textureX, textureY); // Box 41
		gunModel[99] = new ModelRendererTurbo(this, 216, 130, textureX, textureY); // Box 42
		gunModel[100] = new ModelRendererTurbo(this, 273, 148, textureX, textureY); // Box 43
		gunModel[101] = new ModelRendererTurbo(this, 273, 134, textureX, textureY); // Box 44
		gunModel[102] = new ModelRendererTurbo(this, 300, 133, textureX, textureY); // Box 45
		gunModel[103] = new ModelRendererTurbo(this, 323, 136, textureX, textureY); // Box 47
		gunModel[104] = new ModelRendererTurbo(this, 169, 148, textureX, textureY); // Box 48
		gunModel[105] = new ModelRendererTurbo(this, 169, 148, textureX, textureY); // Box 49
		gunModel[106] = new ModelRendererTurbo(this, 68, 86, textureX, textureY); // Box 50
		gunModel[107] = new ModelRendererTurbo(this, 74, 68, textureX, textureY); // Box 51
		gunModel[108] = new ModelRendererTurbo(this, 74, 77, textureX, textureY); // Box 53
		gunModel[109] = new ModelRendererTurbo(this, 257, 194, textureX, textureY); // Box 55
		gunModel[110] = new ModelRendererTurbo(this, 257, 184, textureX, textureY); // Box 56
		gunModel[111] = new ModelRendererTurbo(this, 257, 184, textureX, textureY); // Box 57
		gunModel[112] = new ModelRendererTurbo(this, 257, 170, textureX, textureY); // Box 60
		gunModel[113] = new ModelRendererTurbo(this, 218, 173, textureX, textureY); // Box 61
		gunModel[114] = new ModelRendererTurbo(this, 218, 173, textureX, textureY); // Box 62
		gunModel[115] = new ModelRendererTurbo(this, 218, 187, textureX, textureY); // Box 63
		gunModel[116] = new ModelRendererTurbo(this, 292, 184, textureX, textureY); // Box 64
		gunModel[117] = new ModelRendererTurbo(this, 292, 194, textureX, textureY); // Box 65
		gunModel[118] = new ModelRendererTurbo(this, 292, 184, textureX, textureY); // Box 66
		gunModel[119] = new ModelRendererTurbo(this, 356, 177, textureX, textureY); // Box 68
		gunModel[120] = new ModelRendererTurbo(this, 356, 192, textureX, textureY); // Box 69
		gunModel[121] = new ModelRendererTurbo(this, 309, 177, textureX, textureY); // Box 70
		gunModel[122] = new ModelRendererTurbo(this, 309, 192, textureX, textureY); // Box 71
		gunModel[123] = new ModelRendererTurbo(this, 99, 158, textureX, textureY); // Box 72
		gunModel[124] = new ModelRendererTurbo(this, 46, 146, textureX, textureY); // Box 73
		gunModel[125] = new ModelRendererTurbo(this, 46, 150, textureX, textureY); // Box 74
		gunModel[126] = new ModelRendererTurbo(this, 52, 154, textureX, textureY); // Box 75
		gunModel[127] = new ModelRendererTurbo(this, 52, 154, textureX, textureY); // Box 76
		gunModel[128] = new ModelRendererTurbo(this, 96, 67, textureX, textureY); // Box 80
		gunModel[129] = new ModelRendererTurbo(this, 96, 67, textureX, textureY); // Box 81
		gunModel[130] = new ModelRendererTurbo(this, 53, 86, textureX, textureY); // Box 82
		gunModel[131] = new ModelRendererTurbo(this, 126, 65, textureX, textureY); // Box 83
		gunModel[132] = new ModelRendererTurbo(this, 126, 65, textureX, textureY); // Box 84
		gunModel[133] = new ModelRendererTurbo(this, 55, 110, textureX, textureY); // Box 85
		gunModel[134] = new ModelRendererTurbo(this, 88, 110, textureX, textureY); // Box 86
		gunModel[135] = new ModelRendererTurbo(this, 141, 166, textureX, textureY); // Box 87
		gunModel[136] = new ModelRendererTurbo(this, 141, 166, textureX, textureY); // Box 88
		gunModel[137] = new ModelRendererTurbo(this, 256, 97, textureX, textureY); // Box 91
		gunModel[138] = new ModelRendererTurbo(this, 256, 97, textureX, textureY); // Box 92
		gunModel[139] = new ModelRendererTurbo(this, 256, 97, textureX, textureY); // Box 93
		gunModel[140] = new ModelRendererTurbo(this, 256, 97, textureX, textureY); // Box 94
		gunModel[141] = new ModelRendererTurbo(this, 256, 97, textureX, textureY); // Box 95
		gunModel[142] = new ModelRendererTurbo(this, 256, 97, textureX, textureY); // Box 96
		gunModel[143] = new ModelRendererTurbo(this, 256, 97, textureX, textureY); // Box 97
		gunModel[144] = new ModelRendererTurbo(this, 256, 97, textureX, textureY); // Box 98
		gunModel[145] = new ModelRendererTurbo(this, 169, 159, textureX, textureY); // Box 99
		gunModel[146] = new ModelRendererTurbo(this, 169, 159, textureX, textureY); // Box 100
		gunModel[147] = new ModelRendererTurbo(this, 193, 58, textureX, textureY); // Box 101
		gunModel[148] = new ModelRendererTurbo(this, 234, 159, textureX, textureY); // Box 102
		gunModel[149] = new ModelRendererTurbo(this, 234, 159, textureX, textureY); // Box 103
		gunModel[150] = new ModelRendererTurbo(this, 436, 69, textureX, textureY); // Box 109
		gunModel[151] = new ModelRendererTurbo(this, 471, 71, textureX, textureY); // Box 110
		gunModel[152] = new ModelRendererTurbo(this, 456, 70, textureX, textureY); // Box 111
		gunModel[153] = new ModelRendererTurbo(this, 471, 71, textureX, textureY); // Box 112
		gunModel[154] = new ModelRendererTurbo(this, 456, 70, textureX, textureY); // Box 113
		gunModel[155] = new ModelRendererTurbo(this, 436, 69, textureX, textureY); // Box 179
		gunModel[156] = new ModelRendererTurbo(this, 471, 71, textureX, textureY); // Box 180
		gunModel[157] = new ModelRendererTurbo(this, 456, 70, textureX, textureY); // Box 181
		gunModel[158] = new ModelRendererTurbo(this, 471, 71, textureX, textureY); // Box 182
		gunModel[159] = new ModelRendererTurbo(this, 456, 70, textureX, textureY); // Box 183
		gunModel[160] = new ModelRendererTurbo(this, 76, 221, textureX, textureY); // Box 32
		gunModel[161] = new ModelRendererTurbo(this, 1, 207, textureX, textureY); // Box 34
		gunModel[162] = new ModelRendererTurbo(this, 1, 218, textureX, textureY); // Box 35
		gunModel[163] = new ModelRendererTurbo(this, 1, 232, textureX, textureY); // Box 36
		gunModel[164] = new ModelRendererTurbo(this, 76, 207, textureX, textureY); // Box 38
		gunModel[165] = new ModelRendererTurbo(this, 36, 205, textureX, textureY); // Box 39
		gunModel[166] = new ModelRendererTurbo(this, 36, 218, textureX, textureY); // Box 40
		gunModel[167] = new ModelRendererTurbo(this, 36, 232, textureX, textureY); // Box 41
		gunModel[168] = new ModelRendererTurbo(this, 109, 207, textureX, textureY); // Box 42
		gunModel[169] = new ModelRendererTurbo(this, 57, 218, textureX, textureY); // Box 0
		gunModel[170] = new ModelRendererTurbo(this, 57, 235, textureX, textureY); // Box 1
		gunModel[171] = new ModelRendererTurbo(this, 130, 207, textureX, textureY); // Box 2
		gunModel[172] = new ModelRendererTurbo(this, 57, 207, textureX, textureY); // Box 3
		gunModel[173] = new ModelRendererTurbo(this, 330, 115, textureX, textureY); // Box 191
		gunModel[174] = new ModelRendererTurbo(this, 141, 144, textureX, textureY); // Box 192

		gunModel[0].addBox(0F, 0F, 0F, 20, 5, 15, 0F); // Box 2
		gunModel[0].setRotationPoint(24F, -28F, -7.5F);

		gunModel[1].addShapeBox(0F, 0F, 0F, 20, 3, 15, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 4
		gunModel[1].setRotationPoint(24F, -23F, -7.5F);

		gunModel[2].addShapeBox(0F, 0F, 0F, 20, 2, 11, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 5
		gunModel[2].setRotationPoint(24F, -20F, -5.5F);

		gunModel[3].addShapeBox(0F, 0F, 0F, 20, 3, 15, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 6
		gunModel[3].setRotationPoint(24F, -31F, -7.5F);

		gunModel[4].addShapeBox(0F, 0F, 0F, 20, 2, 11, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 7
		gunModel[4].setRotationPoint(24F, -33F, -5.5F);

		gunModel[5].addShapeBox(0F, 0F, 0F, 2, 6, 17, 0F, 0F, -0.5F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -1F, 0F, -0.5F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -1F); // Box 0
		gunModel[5].setRotationPoint(44F, -28.5F, -8.5F);

		gunModel[6].addShapeBox(0F, 0F, 0F, 2, 3, 17, 0F, 0F, -0.5F, -3F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, -0.5F, -3F, 0F, 0.5F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, -1F); // Box 2
		gunModel[6].setRotationPoint(44F, -31.5F, -8.5F);

		gunModel[7].addShapeBox(0F, 0F, 0F, 2, 2, 13, 0F, 0F, -0.5F, -4F, 0F, 0.5F, -4F, 0F, 0.5F, -4F, 0F, -0.5F, -4F, 0F, 0.5F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, -1F); // Box 3
		gunModel[7].setRotationPoint(44F, -33.5F, -6.5F);

		gunModel[8].addShapeBox(0F, 0F, 0F, 2, 3, 17, 0F, 0F, 0.5F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, -1F, 0F, -0.5F, -3F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, -0.5F, -3F); // Box 4
		gunModel[8].setRotationPoint(44F, -22.5F, -8.5F);

		gunModel[9].addShapeBox(0F, 0F, 0F, 2, 2, 13, 0F, 0F, 0.5F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, -1F, 0F, -0.5F, -4F, 0F, 0.5F, -4F, 0F, 0.5F, -4F, 0F, -0.5F, -4F); // Box 5
		gunModel[9].setRotationPoint(44F, -19.5F, -6.5F);

		gunModel[10].addShapeBox(0F, 0F, 0F, 10, 3, 17, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 6
		gunModel[10].setRotationPoint(46F, -22.5F, -8.5F);

		gunModel[11].addShapeBox(0F, 0F, 0F, 10, 2, 13, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, -4F, 0F, 0.5F, -4F, 0F, 0.5F, -4F, 0F, 0.5F, -4F); // Box 7
		gunModel[11].setRotationPoint(46F, -19.5F, -6.5F);

		gunModel[12].addBox(0F, 0F, 0F, 10, 6, 17, 0F); // Box 8
		gunModel[12].setRotationPoint(46F, -28.5F, -8.5F);

		gunModel[13].addShapeBox(0F, 0F, 0F, 10, 3, 17, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 9
		gunModel[13].setRotationPoint(46F, -31.5F, -8.5F);

		gunModel[14].addShapeBox(0F, 0F, 0F, 10, 2, 13, 0F, 0F, 0.5F, -4F, 0F, 0.5F, -4F, 0F, 0.5F, -4F, 0F, 0.5F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 10
		gunModel[14].setRotationPoint(46F, -33.5F, -6.5F);

		gunModel[15].addBox(0F, 0F, 0F, 1, 14, 15, 0F); // Box 11
		gunModel[15].setRotationPoint(23F, -33F, -7.5F);

		gunModel[16].addBox(0F, 0F, 0F, 20, 14, 16, 0F); // Box 12
		gunModel[16].setRotationPoint(3F, -33F, -7.5F);

		gunModel[17].addShapeBox(0F, 0F, 0F, 6, 2, 17, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 13
		gunModel[17].setRotationPoint(3F, -35F, -8.5F);

		gunModel[18].addShapeBox(0F, 0F, 0F, 5, 14, 17, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 14
		gunModel[18].setRotationPoint(-2F, -33F, -8.5F);

		gunModel[19].addBox(0F, 0F, 0F, 10, 10, 2, 0F); // Box 15
		gunModel[19].setRotationPoint(-12F, -29F, -8.5F);

		gunModel[20].addShapeBox(0F, 0F, 0F, 6, 5, 17, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -5F, 0F, 0F); // Box 16
		gunModel[20].setRotationPoint(1F, -19F, -8.5F);

		gunModel[21].addBox(0F, 0F, 0F, 17, 5, 17, 0F); // Box 17
		gunModel[21].setRotationPoint(7F, -19F, -8.5F);

		gunModel[22].addShapeBox(0F, 0F, 0F, 10, 4, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F); // Box 18
		gunModel[22].setRotationPoint(-4F, -11F, -4.5F);

		gunModel[23].addBox(0F, 0F, 0F, 18, 8, 9, 0F); // Box 19
		gunModel[23].setRotationPoint(-12F, -19F, -4.5F);

		gunModel[24].addShapeBox(0F, 0F, 0F, 18, 2, 17, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 20
		gunModel[24].setRotationPoint(6F, -14F, -8.5F);

		gunModel[25].addShapeBox(0F, 0F, 0F, 10, 2, 17, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 21
		gunModel[25].setRotationPoint(-12F, -31F, -8.5F);

		gunModel[26].addShapeBox(0F, 0F, 0F, 5, 2, 17, 0F, 0F, -4F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, -4F, -2F, 0F, 4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F); // Box 22
		gunModel[26].setRotationPoint(-2F, -35F, -8.5F);

		gunModel[27].addBox(0F, 0F, 0F, 50, 4, 9, 0F); // Box 28
		gunModel[27].setRotationPoint(-62F, -19F, -4.5F);

		gunModel[28].addShapeBox(0F, 0F, 0F, 6, 4, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F); // Box 29
		gunModel[28].setRotationPoint(-18F, -15F, -4.5F);

		gunModel[29].addShapeBox(0F, 0F, 0F, 13, 2, 17, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 30
		gunModel[29].setRotationPoint(-12F, -19F, -8.5F);

		gunModel[30].addShapeBox(0F, 0F, 0F, 5, 2, 17, 0F, 0F, 0F, 0F, 0F, -5F, 0F, 0F, -5F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 5F, -3F, 0F, 5F, -3F, 0F, 0F, -3F); // Box 31
		gunModel[30].setRotationPoint(1F, -19F, -8.5F);

		gunModel[31].addShapeBox(0F, 0F, 0F, 20, 3, 15, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 33
		gunModel[31].setRotationPoint(-62F, -23F, -7.5F);

		gunModel[32].addShapeBox(0F, 0F, 0F, 20, 2, 11, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 34
		gunModel[32].setRotationPoint(-62F, -20F, -5.5F);

		gunModel[33].addBox(0F, 0F, 0F, 20, 5, 15, 0F); // Box 35
		gunModel[33].setRotationPoint(-62F, -28F, -7.5F);

		gunModel[34].addShapeBox(0F, 0F, 0F, 20, 3, 15, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 36
		gunModel[34].setRotationPoint(-62F, -31F, -7.5F);

		gunModel[35].addShapeBox(0F, 0F, 0F, 20, 2, 11, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 37
		gunModel[35].setRotationPoint(-62F, -33F, -5.5F);

		gunModel[36].addBox(0F, 0F, 0F, 12, 3, 9, 0F); // Box 53
		gunModel[36].setRotationPoint(-58F, -15F, -4.5F);

		gunModel[37].addBox(0F, 0F, 0F, 15, 2, 7, 0F); // Box 54
		gunModel[37].setRotationPoint(-46F, -15F, -3.5F);

		gunModel[38].addShapeBox(0F, 0F, 0F, 2, 1, 10, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 113
		gunModel[38].setRotationPoint(-1.5F, -10F, -5F);

		gunModel[39].addShapeBox(0F, 0F, 0F, 2, 1, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 114
		gunModel[39].setRotationPoint(-1.5F, -9F, -5F);

		gunModel[40].addBox(0F, 0F, 0F, 3, 6, 17, 0F); // Box 72
		gunModel[40].setRotationPoint(-65F, -28.5F, -8.5F);

		gunModel[41].addShapeBox(0F, 0F, 0F, 3, 2, 13, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, -4F, 0F, 0.5F, -4F, 0F, 0.5F, -4F, 0F, 0.5F, -4F); // Box 73
		gunModel[41].setRotationPoint(-65F, -19.5F, -6.5F);

		gunModel[42].addShapeBox(0F, 0F, 0F, 3, 2, 13, 0F, 0F, 0.5F, -4F, 0F, 0.5F, -4F, 0F, 0.5F, -4F, 0F, 0.5F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 76
		gunModel[42].setRotationPoint(-65F, -33.5F, -6.5F);

		gunModel[43].addBox(0F, 0F, 0F, 9, 4, 8, 0F); // Box 77
		gunModel[43].setRotationPoint(-11F, -11F, -4F);

		gunModel[44].addShapeBox(0F, 0F, 0F, 18, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 86
		gunModel[44].setRotationPoint(24F, -14F, -3.5F);

		gunModel[45].addBox(0F, 0F, 0F, 18, 5, 7, 0F); // Box 87
		gunModel[45].setRotationPoint(24F, -19F, -3.5F);

		gunModel[46].addBox(0F, 0F, 0F, 4, 4, 5, 0F); // Box 88
		gunModel[46].setRotationPoint(6F, -12F, -2.5F);

		gunModel[47].addShapeBox(0F, 0F, 0F, 2, 5, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F); // Box 89
		gunModel[47].setRotationPoint(7F, -8F, -2.5F);

		gunModel[48].addShapeBox(0F, 0F, 0F, 2, 12, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, 4F, -1F, 0F, 4F, -1F, 0F, -4F, 0F, 0F); // Box 90
		gunModel[48].setRotationPoint(8F, -3F, -2.5F);

		gunModel[49].addBox(0F, 0F, 0F, 15, 3, 8, 0F); // Box 1
		gunModel[49].setRotationPoint(24F, -14.5F, -4F);

		gunModel[50].addBox(0F, 0F, 0F, 5, 15, 7, 0F); // Box 2
		gunModel[50].setRotationPoint(26F, -9.5F, -3.5F);

		gunModel[51].addShapeBox(0F, 0F, 0F, 1, 15, 7, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F); // Box 3
		gunModel[51].setRotationPoint(31F, -9.5F, -3.5F);

		gunModel[52].addShapeBox(0F, 0F, 0F, 1, 20, 7, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 5
		gunModel[52].setRotationPoint(25F, -11.5F, -3.5F);

		gunModel[53].addShapeBox(0F, 0F, 0F, 5, 2, 7, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 6
		gunModel[53].setRotationPoint(26F, -11.5F, -3.5F);

		gunModel[54].addShapeBox(0F, 0F, 0F, 1, 2, 7, 0F, -1F, 0F, 0F, 1F, 0F, -2F, 1F, 0F, -2F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F); // Box 7
		gunModel[54].setRotationPoint(31F, -11.5F, -3.5F);

		gunModel[55].addShapeBox(0F, 0F, 0F, 5, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F); // Box 8
		gunModel[55].setRotationPoint(26F, 5.5F, -3.5F);

		gunModel[56].addShapeBox(0F, 0F, 0F, 1, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, -2F, 1F, 0F, -2F, -1F, 0F, 0F); // Box 9
		gunModel[56].setRotationPoint(31F, 5.5F, -3.5F);

		gunModel[57].addShapeBox(0F, 0F, 0F, 5, 1, 7, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 10
		gunModel[57].setRotationPoint(26F, 7.5F, -3.5F);

		gunModel[58].addShapeBox(0F, 0F, 0F, 1, 1, 7, 0F, -1F, 0F, 0F, 1F, 0F, -2F, 1F, 0F, -2F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F); // Box 11
		gunModel[58].setRotationPoint(31F, 7.5F, -3.5F);

		gunModel[59].addShapeBox(0F, 0F, 0F, 3, 3, 17, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 13
		gunModel[59].setRotationPoint(-65F, -22.5F, -8.5F);

		gunModel[60].addShapeBox(0F, 0F, 0F, 3, 3, 17, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 15
		gunModel[60].setRotationPoint(-65F, -31.5F, -8.5F);

		gunModel[61].addBox(0F, 0F, 0F, 2, 5, 15, 0F); // Box 18
		gunModel[61].setRotationPoint(-67F, -28F, -7.5F);

		gunModel[62].addShapeBox(0F, 0F, 0F, 2, 3, 15, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 19
		gunModel[62].setRotationPoint(-67F, -23F, -7.5F);

		gunModel[63].addShapeBox(0F, 0F, 0F, 2, 2, 11, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 20
		gunModel[63].setRotationPoint(-67F, -20F, -5.5F);

		gunModel[64].addShapeBox(0F, 0F, 0F, 2, 3, 15, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 21
		gunModel[64].setRotationPoint(-67F, -31F, -7.5F);

		gunModel[65].addShapeBox(0F, 0F, 0F, 2, 2, 11, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 22
		gunModel[65].setRotationPoint(-67F, -33F, -5.5F);

		gunModel[66].addShapeBox(0F, 0F, 0F, 8, 2, 13, 0F, 0F, 0.5F, -4F, 0F, 0.5F, -4F, 0F, 0.5F, -4F, 0F, 0.5F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 23
		gunModel[66].setRotationPoint(-75F, -33.5F, -6.5F);

		gunModel[67].addShapeBox(0F, 0F, 0F, 8, 3, 17, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 24
		gunModel[67].setRotationPoint(-75F, -31.5F, -8.5F);

		gunModel[68].addBox(0F, 0F, 0F, 8, 6, 17, 0F); // Box 25
		gunModel[68].setRotationPoint(-75F, -28.5F, -8.5F);

		gunModel[69].addShapeBox(0F, 0F, 0F, 8, 3, 17, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 26
		gunModel[69].setRotationPoint(-75F, -22.5F, -8.5F);

		gunModel[70].addShapeBox(0F, 0F, 0F, 8, 2, 13, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, -4F, 0F, 0.5F, -4F, 0F, 0.5F, -4F, 0F, 0.5F, -4F); // Box 27
		gunModel[70].setRotationPoint(-75F, -19.5F, -6.5F);

		gunModel[71].addShapeBox(0F, 0F, 0F, 2, 1, 10, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 28
		gunModel[71].setRotationPoint(-15.5F, -14F, -5F);

		gunModel[72].addShapeBox(0F, 0F, 0F, 2, 1, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 29
		gunModel[72].setRotationPoint(-15.5F, -13F, -5F);

		gunModel[73].addShapeBox(0F, 0F, 0F, 2, 1, 10, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 30
		gunModel[73].setRotationPoint(-49F, -15F, -5F);

		gunModel[74].addShapeBox(0F, 0F, 0F, 2, 1, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 31
		gunModel[74].setRotationPoint(-49F, -14F, -5F);

		gunModel[75].addShapeBox(0F, 0F, 0F, 2, 1, 8, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 32
		gunModel[75].setRotationPoint(27.5F, -10F, -4F);

		gunModel[76].addShapeBox(0F, 0F, 0F, 2, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 33
		gunModel[76].setRotationPoint(27.5F, -9F, -4F);

		gunModel[77].addShapeBox(0F, 0F, 0F, 2, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 34
		gunModel[77].setRotationPoint(27.5F, 5F, -4F);

		gunModel[78].addShapeBox(0F, 0F, 0F, 2, 1, 8, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 35
		gunModel[78].setRotationPoint(27.5F, 4F, -4F);

		gunModel[79].addBox(0F, 0F, 0F, 10, 9, 1, 0F); // Box 67
		gunModel[79].setRotationPoint(9F, -26F, 8.5F);

		gunModel[80].addBox(0F, 0F, 0F, 2, 6, 2, 0F); // Box 68
		gunModel[80].setRotationPoint(12.5F, -24.5F, 9.5F);

		gunModel[81].addShapeBox(-2F, -4F, 0F, 2, 8, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F); // Box 69
		gunModel[81].setRotationPoint(13.5F, -21.5F, 11.5F);
		gunModel[81].rotateAngleZ = -0.43633231F;

		gunModel[82].addShapeBox(0F, 0F, 0F, 4, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 70
		gunModel[82].setRotationPoint(14.5F, -23.5F, 9.5F);

		gunModel[83].addShapeBox(0F, 0F, 0F, 4, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 71
		gunModel[83].setRotationPoint(14.5F, -21.5F, 9.5F);

		gunModel[84].addShapeBox(-0.5F, 0.5F, 0.5F, 1, 1, 2, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 72
		gunModel[84].setRotationPoint(12.5F, -24.5F, 9F);

		gunModel[85].addShapeBox(-0.5F, 1.5F, 0.5F, 1, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 73
		gunModel[85].setRotationPoint(12.5F, -24.5F, 9F);

		gunModel[86].addShapeBox(-1F, -4F, 1F, 1, 24, 36, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -24F, 0F, 0F, -24F, 0F, -16F, 0F, 0F, -16F, 0F, 0F, -16F, -24F, 0F, -16F, -24F); // Box 74
		gunModel[86].setRotationPoint(13.5F, -21.5F, 11.5F);
		gunModel[86].rotateAngleZ = -0.43633231F;

		gunModel[87].addBox(-2F, -5F, 1F, 2, 1, 12, 0F); // Box 78
		gunModel[87].setRotationPoint(13.5F, -21.5F, 11.5F);
		gunModel[87].rotateAngleZ = -0.43633231F;

		gunModel[88].addBox(-2F, 4F, 1F, 2, 1, 12, 0F); // Box 134
		gunModel[88].setRotationPoint(13.5F, -21.5F, 11.5F);
		gunModel[88].rotateAngleZ = -0.43633231F;

		gunModel[89].addShapeBox(-2F, -4F, 13F, 2, 8, 1, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 135
		gunModel[89].setRotationPoint(13.5F, -21.5F, 11.5F);
		gunModel[89].rotateAngleZ = -0.43633231F;

		gunModel[90].addShapeBox(-2F, 1F, 1F, 2, 4, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 136
		gunModel[90].setRotationPoint(13.5F, -21.5F, 11.5F);
		gunModel[90].rotateAngleZ = -0.43633231F;

		gunModel[91].addBox(-2.3F, 3F, 2.5F, 1, 1, 1, 0F); // Box 137
		gunModel[91].setRotationPoint(13.5F, -21.5F, 11.5F);
		gunModel[91].rotateAngleZ = -0.43633231F;

		gunModel[92].addBox(-2.3F, 3F, 1F, 1, 1, 1, 0F); // Box 138
		gunModel[92].setRotationPoint(13.5F, -21.5F, 11.5F);
		gunModel[92].rotateAngleZ = -0.43633231F;

		gunModel[93].addShapeBox(0F, 0F, 0F, 3, 9, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F); // Box 36
		gunModel[93].setRotationPoint(19F, -26F, 8.5F);

		gunModel[94].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 37
		gunModel[94].setRotationPoint(19.5F, -25.5F, 9F);

		gunModel[95].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 38
		gunModel[95].setRotationPoint(19.5F, -24.5F, 9F);

		gunModel[96].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 39
		gunModel[96].setRotationPoint(9.5F, -19.5F, 9F);

		gunModel[97].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 40
		gunModel[97].setRotationPoint(9.5F, -18.5F, 9F);

		gunModel[98].addShapeBox(0F, 0F, 0F, 20, 1, 8, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 41
		gunModel[98].setRotationPoint(-4F, -41F, -4F);

		gunModel[99].addBox(0F, 0F, 0F, 20, 9, 8, 0F); // Box 42
		gunModel[99].setRotationPoint(-4F, -40F, -4F);

		gunModel[100].addShapeBox(0F, 0F, 0F, 5, 1, 8, 0F, 0F, 0F, -1F, 0F, -4F, -1F, 0F, -4F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, 0F, 0F); // Box 43
		gunModel[100].setRotationPoint(16F, -41F, -4F);

		gunModel[101].addShapeBox(0F, 0F, 0F, 5, 5, 8, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 44
		gunModel[101].setRotationPoint(16F, -40F, -4F);

		gunModel[102].addShapeBox(0F, 0F, 0F, 3, 4, 8, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 45
		gunModel[102].setRotationPoint(-7F, -37F, -4F);

		gunModel[103].addBox(0F, 0F, 0F, 5, 3, 8, 0F); // Box 47
		gunModel[103].setRotationPoint(-12F, -34F, -4F);

		gunModel[104].addBox(0F, 0F, 0F, 13, 1, 9, 0F); // Box 48
		gunModel[104].setRotationPoint(-3F, -39F, -4.5F);

		gunModel[105].addBox(0F, 0F, 0F, 13, 1, 9, 0F); // Box 49
		gunModel[105].setRotationPoint(-3F, -37F, -4.5F);

		gunModel[106].addBox(0F, 0F, 0F, 6, 14, 1, 0F); // Box 50
		gunModel[106].setRotationPoint(3F, -33F, -8.5F);

		gunModel[107].addBox(0F, 0F, 0F, 6, 7, 1, 0F); // Box 51
		gunModel[107].setRotationPoint(10F, -33F, -8.5F);

		gunModel[108].addBox(0F, 0F, 0F, 8, 7, 1, 0F); // Box 53
		gunModel[108].setRotationPoint(9F, -26F, -8.5F);

		gunModel[109].addBox(0F, 0F, 0F, 10, 3, 7, 0F); // Box 55
		gunModel[109].setRotationPoint(10F, -33F, 10.5F);

		gunModel[110].addShapeBox(0F, 0F, 0F, 10, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 56
		gunModel[110].setRotationPoint(10F, -35F, 10.5F);

		gunModel[111].addShapeBox(0F, 0F, 0F, 10, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 57
		gunModel[111].setRotationPoint(10F, -30F, 10.5F);

		gunModel[112].addBox(0F, 0F, 0F, 5, 1, 12, 0F); // Box 60
		gunModel[112].setRotationPoint(11F, -36.5F, 3.5F);

		gunModel[113].addShapeBox(0F, 0F, 0F, 5, 3, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 61
		gunModel[113].setRotationPoint(11F, -33F, 8F);

		gunModel[114].addShapeBox(0F, 0F, 0F, 5, 3, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -2.5F, 0F, -0.5F, -2.5F); // Box 62
		gunModel[114].setRotationPoint(11F, -30F, 8F);

		gunModel[115].addShapeBox(0F, 0F, 0F, 5, 3, 14, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -2.5F, 0F, -0.5F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 63
		gunModel[115].setRotationPoint(11F, -36F, 4F);

		gunModel[116].addShapeBox(0F, 0F, 0F, 1, 2, 7, 0F, 0F, 0F, -2F, 0F, -1F, -2.5F, 0F, -1F, -2.5F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0.5F, -1F, 0F, 0.5F, -1F, 0F, 0F, 0F); // Box 64
		gunModel[116].setRotationPoint(20F, -35F, 10.5F);

		gunModel[117].addShapeBox(0F, 0F, 0F, 1, 3, 7, 0F, 0F, 0F, 0F, 0F, -0.5F, -1F, 0F, -0.5F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -1F, 0F, -0.5F, -1F, 0F, 0F, 0F); // Box 65
		gunModel[117].setRotationPoint(20F, -33F, 10.5F);

		gunModel[118].addShapeBox(0F, 0F, 0F, 1, 2, 7, 0F, 0F, 0F, 0F, 0F, 0.5F, -1F, 0F, 0.5F, -1F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, -1F, -2.5F, 0F, -1F, -2.5F, 0F, 0F, -2F); // Box 66
		gunModel[118].setRotationPoint(20F, -30F, 10.5F);

		gunModel[119].addBox(0F, 0F, 0F, 18, 3, 11, 0F); // Box 68
		gunModel[119].setRotationPoint(-61F, -12F, -5.5F);

		gunModel[120].addShapeBox(0F, 0F, 0F, 18, 1, 11, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, -1F, 0F, -1F); // Box 69
		gunModel[120].setRotationPoint(-61F, -9F, -5.5F);

		gunModel[121].addShapeBox(0F, 0F, 0F, 12, 3, 11, 0F, 0F, 0F, 0F, 0F, -8F, 0F, 0F, -8F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 8F, 0F, 0F, 8F, 0F, 0F, 0F, 0F); // Box 70
		gunModel[121].setRotationPoint(-43F, -12F, -5.5F);

		gunModel[122].addShapeBox(0F, 0F, 0F, 12, 1, 11, 0F, 0F, 0F, 0F, 0F, -8F, 0F, 0F, -8F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, -1F, 8F, -1F, -1F, 8F, -1F, 0F, 0F, -1F); // Box 71
		gunModel[122].setRotationPoint(-43F, -9F, -5.5F);

		gunModel[123].addShapeBox(0F, 0F, 0F, 4, 12, 5, 0F, -6F, 0F, 0F, 6F, 0F, 0F, 6F, 0F, 0F, -6F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 72
		gunModel[123].setRotationPoint(-36F, -16F, -2.5F);

		gunModel[124].addBox(0F, 0F, 0F, 15, 2, 1, 0F); // Box 73
		gunModel[124].setRotationPoint(-31F, -15F, -3.5F);

		gunModel[125].addBox(0F, 0F, 0F, 15, 2, 1, 0F); // Box 74
		gunModel[125].setRotationPoint(-31F, -15F, 2.5F);

		gunModel[126].addShapeBox(0F, 0F, 0F, 2, 1, 8, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 75
		gunModel[126].setRotationPoint(-30.5F, -14F, -4F);

		gunModel[127].addShapeBox(0F, 0F, 0F, 2, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 76
		gunModel[127].setRotationPoint(-30.5F, -13F, -4F);

		gunModel[128].addShapeBox(0F, 0F, 0F, 6, 2, 17, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 80
		gunModel[128].setRotationPoint(10F, -35F, -8.5F);

		gunModel[129].addShapeBox(0F, 0F, 0F, 6, 2, 17, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 81
		gunModel[129].setRotationPoint(17F, -35F, -8.5F);

		gunModel[130].addBox(0F, 0F, 0F, 6, 14, 1, 0F); // Box 82
		gunModel[130].setRotationPoint(17F, -33F, -8.5F);

		gunModel[131].addShapeBox(0F, 0F, 0F, 1, 2, 16, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 83
		gunModel[131].setRotationPoint(9F, -35F, -7.5F);

		gunModel[132].addShapeBox(0F, 0F, 0F, 1, 2, 16, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 84
		gunModel[132].setRotationPoint(16F, -35F, -7.5F);

		gunModel[133].addBox(0F, 0F, 0F, 15, 8, 1, 0F); // Box 85
		gunModel[133].setRotationPoint(7F, -23F, -9F);

		gunModel[134].addShapeBox(0F, 0F, 0F, 5, 8, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -5F, 0F); // Box 86
		gunModel[134].setRotationPoint(2F, -23F, -9F);

		gunModel[135].addShapeBox(0F, 0F, 0F, 2, 1, 12, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 87
		gunModel[135].setRotationPoint(-44F, -12.5F, -6F);

		gunModel[136].addShapeBox(0F, 0F, 0F, 2, 1, 12, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 88
		gunModel[136].setRotationPoint(-44F, -11.5F, -6F);

		gunModel[137].addShapeBox(0F, 0F, 0F, 2, 2, 3, 0F, 0F, -0.5F, -0.75F, 0F, -0.5F, -0.75F, 0F, -0.5F, -0.75F, 0F, -0.5F, -0.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 91
		gunModel[137].setRotationPoint(23F, -34F, -8.5F);

		gunModel[138].addShapeBox(0F, 0F, 0F, 2, 2, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -0.75F, 0F, -0.5F, -0.75F, 0F, -0.5F, -0.75F, 0F, -0.5F, -0.75F); // Box 92
		gunModel[138].setRotationPoint(23F, -32F, -8.5F);

		gunModel[139].addShapeBox(0F, 0F, 0F, 2, 2, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -0.75F, 0F, -0.5F, -0.75F, 0F, -0.5F, -0.75F, 0F, -0.5F, -0.75F); // Box 93
		gunModel[139].setRotationPoint(23F, -19F, -8.5F);

		gunModel[140].addShapeBox(0F, 0F, 0F, 2, 2, 3, 0F, 0F, -0.5F, -0.75F, 0F, -0.5F, -0.75F, 0F, -0.5F, -0.75F, 0F, -0.5F, -0.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 94
		gunModel[140].setRotationPoint(23F, -21F, -8.5F);

		gunModel[141].addShapeBox(0F, 0F, 0F, 2, 2, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -0.75F, 0F, -0.5F, -0.75F, 0F, -0.5F, -0.75F, 0F, -0.5F, -0.75F); // Box 95
		gunModel[141].setRotationPoint(23F, -19F, 5.5F);

		gunModel[142].addShapeBox(0F, 0F, 0F, 2, 2, 3, 0F, 0F, -0.5F, -0.75F, 0F, -0.5F, -0.75F, 0F, -0.5F, -0.75F, 0F, -0.5F, -0.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 96
		gunModel[142].setRotationPoint(23F, -21F, 5.5F);

		gunModel[143].addShapeBox(0F, 0F, 0F, 2, 2, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -0.75F, 0F, -0.5F, -0.75F, 0F, -0.5F, -0.75F, 0F, -0.5F, -0.75F); // Box 97
		gunModel[143].setRotationPoint(23F, -32F, 5.5F);

		gunModel[144].addShapeBox(0F, 0F, 0F, 2, 2, 3, 0F, 0F, -0.5F, -0.75F, 0F, -0.5F, -0.75F, 0F, -0.5F, -0.75F, 0F, -0.5F, -0.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 98
		gunModel[144].setRotationPoint(23F, -34F, 5.5F);

		gunModel[145].addShapeBox(0F, 0F, 0F, 30, 1, 2, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 99
		gunModel[145].setRotationPoint(-42F, -20F, -3.5F);

		gunModel[146].addShapeBox(0F, 0F, 0F, 30, 1, 2, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 100
		gunModel[146].setRotationPoint(-42F, -20F, 1.5F);

		gunModel[147].addBox(0F, 0F, 0F, 10, 10, 2, 0F); // Box 101
		gunModel[147].setRotationPoint(-12F, -29F, 6.5F);

		gunModel[148].addBox(0F, 0F, 0F, 10, 1, 2, 0F); // Box 102
		gunModel[148].setRotationPoint(-12F, -20F, -3.5F);

		gunModel[149].addBox(0F, 0F, 0F, 10, 1, 2, 0F); // Box 103
		gunModel[149].setRotationPoint(-12F, -20F, 1.5F);

		gunModel[150].addBox(0F, 0F, 0F, 2, 5, 15, 0F); // Box 109
		gunModel[150].setRotationPoint(54.5F, -28F, -7.5F);

		gunModel[151].addShapeBox(0F, 0F, 0F, 2, 3, 15, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 110
		gunModel[151].setRotationPoint(54.5F, -23F, -7.5F);

		gunModel[152].addShapeBox(0F, 0F, 0F, 2, 2, 11, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 111
		gunModel[152].setRotationPoint(54.5F, -20F, -5.5F);

		gunModel[153].addShapeBox(0F, 0F, 0F, 2, 3, 15, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 112
		gunModel[153].setRotationPoint(54.5F, -31F, -7.5F);

		gunModel[154].addShapeBox(0F, 0F, 0F, 2, 2, 11, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 113
		gunModel[154].setRotationPoint(54.5F, -33F, -5.5F);

		gunModel[155].addBox(0F, 0F, 0F, 2, 5, 15, 0F); // Box 179
		gunModel[155].setRotationPoint(-75.5F, -28F, -7.5F);

		gunModel[156].addShapeBox(0F, 0F, 0F, 2, 3, 15, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 180
		gunModel[156].setRotationPoint(-75.5F, -31F, -7.5F);

		gunModel[157].addShapeBox(0F, 0F, 0F, 2, 2, 11, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 181
		gunModel[157].setRotationPoint(-75.5F, -33F, -5.5F);

		gunModel[158].addShapeBox(0F, 0F, 0F, 2, 3, 15, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 182
		gunModel[158].setRotationPoint(-75.5F, -23F, -7.5F);

		gunModel[159].addShapeBox(0F, 0F, 0F, 2, 2, 11, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 183
		gunModel[159].setRotationPoint(-75.5F, -20F, -5.5F);

		gunModel[160].addShapeBox(0F, 0F, 0F, 12, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F); // Box 32
		gunModel[160].setRotationPoint(-11F, -7F, -4F);

		gunModel[161].addShapeBox(0F, 0F, 0F, 9, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 34
		gunModel[161].setRotationPoint(-8F, -5F, -4F);

		gunModel[162].addShapeBox(0F, 0F, 0F, 9, 5, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 4F, 0F, 0F); // Box 35
		gunModel[162].setRotationPoint(-8F, -3F, -4F);

		gunModel[163].addShapeBox(0F, 0F, 0F, 9, 9, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, -3F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 3F, -3F, 0F); // Box 36
		gunModel[163].setRotationPoint(-12F, 2F, -4F);

		gunModel[164].addShapeBox(0F, 0F, 0F, 8, 2, 8, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F); // Box 38
		gunModel[164].setRotationPoint(-15F, 11F, -4F);

		gunModel[165].addShapeBox(0F, 0F, 0F, 2, 4, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F); // Box 39
		gunModel[165].setRotationPoint(1F, -7F, -4F);

		gunModel[166].addShapeBox(0F, 0F, 0F, 2, 5, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 4F, 0F, 0F, -4F, 0F, -1.5F, -4F, 0F, -1.5F, 4F, 0F, 0F); // Box 40
		gunModel[166].setRotationPoint(1F, -3F, -4F);

		gunModel[167].addShapeBox(0F, 0F, 0F, 2, 9, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 4F, 0F, 0F, -4F, 0F, -1.5F, -4F, 0F, -1.5F, 4F, 0F, 0F); // Box 41
		gunModel[167].setRotationPoint(-3F, 2F, -4F);

		gunModel[168].addShapeBox(0F, 0F, 0F, 2, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F); // Box 42
		gunModel[168].setRotationPoint(-7F, 11F, -4F);

		gunModel[169].addShapeBox(0F, 0F, 0F, 1, 5, 8, 0F, -4F, 0F, -1.5F, 4F, 0F, 0F, 4F, 0F, 0F, -4F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F); // Box 0
		gunModel[169].setRotationPoint(-13F, -3F, -4F);

		gunModel[170].addShapeBox(0F, 0F, 0F, 1, 6, 8, 0F, -3F, 0F, -1.5F, 3F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F); // Box 1
		gunModel[170].setRotationPoint(-16F, 2F, -4F);

		gunModel[171].addShapeBox(0F, 0F, 0F, 1, 2, 8, 0F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F); // Box 2
		gunModel[171].setRotationPoint(-16F, 8F, -4F);

		gunModel[172].addShapeBox(0F, 0F, 0F, 1, 2, 8, 0F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F); // Box 3
		gunModel[172].setRotationPoint(-9F, -5F, -4F);

		gunModel[173].addBox(0F, 0F, 0F, 3, 2, 8, 0F); // Box 191
		gunModel[173].setRotationPoint(-7F, -33F, -4F);

		gunModel[174].addShapeBox(0F, 0F, 0F, 1, 9, 12, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -6F, 0F, 0F, -6F, 0F, -6F, 0F, 0F, -6F, 0F, 0F, -6F, -6F, 0F, -6F, -6F); // Box 192
		gunModel[174].setRotationPoint(-4.2F, -40F, -3F);


		ammoModel = new ModelRendererTurbo[18];
		ammoModel[0] = new ModelRendererTurbo(this, 168, 44, textureX, textureY); // Box 27
		ammoModel[1] = new ModelRendererTurbo(this, 168, 25, textureX, textureY); // Box 27
		ammoModel[2] = new ModelRendererTurbo(this, 168, 4, textureX, textureY); // Box 27
		ammoModel[3] = new ModelRendererTurbo(this, 168, 25, textureX, textureY); // Box 27
		ammoModel[4] = new ModelRendererTurbo(this, 168, 44, textureX, textureY); // Box 27
		ammoModel[5] = new ModelRendererTurbo(this, 330, 1, textureX, textureY); // Box 27
		ammoModel[6] = new ModelRendererTurbo(this, 330, 25, textureX, textureY); // Box 27
		ammoModel[7] = new ModelRendererTurbo(this, 330, 46, textureX, textureY); // Box 27
		ammoModel[8] = new ModelRendererTurbo(this, 330, 25, textureX, textureY); // Box 27
		ammoModel[9] = new ModelRendererTurbo(this, 330, 46, textureX, textureY); // Box 27
		ammoModel[10] = new ModelRendererTurbo(this, 330, 46, textureX, textureY); // Box 27
		ammoModel[11] = new ModelRendererTurbo(this, 330, 25, textureX, textureY); // Box 27
		ammoModel[12] = new ModelRendererTurbo(this, 330, 1, textureX, textureY); // Box 27
		ammoModel[13] = new ModelRendererTurbo(this, 330, 25, textureX, textureY); // Box 27
		ammoModel[14] = new ModelRendererTurbo(this, 330, 46, textureX, textureY); // Box 27
		ammoModel[15] = new ModelRendererTurbo(this, 79, 144, textureX, textureY); // Box 27
		ammoModel[16] = new ModelRendererTurbo(this, 79, 149, textureX, textureY); // Box 27
		ammoModel[17] = new ModelRendererTurbo(this, 79, 149, textureX, textureY); // Box 27

		ammoModel[0].addShapeBox(0F, 0F, 0F, 30, 2, 11, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 27
		ammoModel[0].setRotationPoint(-42F, -33F, -5.5F);

		ammoModel[1].addShapeBox(0F, 0F, 0F, 30, 3, 15, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 27
		ammoModel[1].setRotationPoint(-42F, -31F, -7.5F);

		ammoModel[2].addBox(0F, 0F, 0F, 30, 5, 15, 0F); // Box 27
		ammoModel[2].setRotationPoint(-42F, -28F, -7.5F);

		ammoModel[3].addShapeBox(0F, 0F, 0F, 30, 3, 15, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 27
		ammoModel[3].setRotationPoint(-42F, -23F, -7.5F);

		ammoModel[4].addShapeBox(0F, 0F, 0F, 30, 2, 11, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 27
		ammoModel[4].setRotationPoint(-42F, -20F, -5.5F);

		ammoModel[5].addBox(0F, 0F, 0F, 2, 6, 17, 0F); // Box 27
		ammoModel[5].setRotationPoint(-41F, -28.5F, -8.5F);

		ammoModel[6].addShapeBox(0F, 0F, 0F, 2, 3, 17, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 27
		ammoModel[6].setRotationPoint(-41F, -31.5F, -8.5F);

		ammoModel[7].addShapeBox(0F, 0F, 0F, 2, 2, 13, 0F, 0F, 0.5F, -4F, 0F, 0.5F, -4F, 0F, 0.5F, -4F, 0F, 0.5F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 27
		ammoModel[7].setRotationPoint(-41F, -33.5F, -6.5F);

		ammoModel[8].addShapeBox(0F, 0F, 0F, 2, 3, 17, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 27
		ammoModel[8].setRotationPoint(-41F, -22.5F, -8.5F);

		ammoModel[9].addShapeBox(0F, 0F, 0F, 2, 2, 13, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, -4F, 0F, 0.5F, -4F, 0F, 0.5F, -4F, 0F, 0.5F, -4F); // Box 27
		ammoModel[9].setRotationPoint(-41F, -19.5F, -6.5F);

		ammoModel[10].addShapeBox(0F, 0F, 0F, 2, 2, 13, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, -4F, 0F, 0.5F, -4F, 0F, 0.5F, -4F, 0F, 0.5F, -4F); // Box 27
		ammoModel[10].setRotationPoint(-14F, -19.5F, -6.5F);

		ammoModel[11].addShapeBox(0F, 0F, 0F, 2, 3, 17, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 27
		ammoModel[11].setRotationPoint(-14F, -22.5F, -8.5F);

		ammoModel[12].addBox(0F, 0F, 0F, 2, 6, 17, 0F); // Box 27
		ammoModel[12].setRotationPoint(-14F, -28.5F, -8.5F);

		ammoModel[13].addShapeBox(0F, 0F, 0F, 2, 3, 17, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 27
		ammoModel[13].setRotationPoint(-14F, -31.5F, -8.5F);

		ammoModel[14].addShapeBox(0F, 0F, 0F, 2, 2, 13, 0F, 0F, 0.5F, -4F, 0F, 0.5F, -4F, 0F, 0.5F, -4F, 0F, 0.5F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 27
		ammoModel[14].setRotationPoint(-14F, -33.5F, -6.5F);

		ammoModel[15].addShapeBox(0F, 0F, 0F, 17, 3, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 27
		ammoModel[15].setRotationPoint(-35F, -27F, 9.5F);

		ammoModel[16].addBox(0F, 0F, 0F, 3, 3, 2, 0F); // Box 27
		ammoModel[16].setRotationPoint(-21F, -27F, 7.5F);

		ammoModel[17].addBox(0F, 0F, 0F, 3, 3, 2, 0F); // Box 27
		ammoModel[17].setRotationPoint(-35F, -27F, 7.5F);

		animationType = EnumAnimationType.GENERIC;

		translateAll(0F, 0F, 0F);


		flipAll();
	}
}