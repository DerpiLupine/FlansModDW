package com.flansmod.client.model.dwfirearms; //Path where the model is located

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelDawnfieldLR8S extends ModelGun //Same as Filename
{
	int textureX = 512;
	int textureY = 256;

	public ModelDawnfieldLR8S() //Same as Filename
	{
		gunModel = new ModelRendererTurbo[96];
		gunModel[0] = new ModelRendererTurbo(this, 48, 12, textureX, textureY); // barrelLeft
		gunModel[1] = new ModelRendererTurbo(this, 59, 12, textureX, textureY); // barrelMiddle
		gunModel[2] = new ModelRendererTurbo(this, 48, 12, textureX, textureY); // barrelRight
		gunModel[3] = new ModelRendererTurbo(this, 201, 8, textureX, textureY); // body10
		gunModel[4] = new ModelRendererTurbo(this, 100, 1, textureX, textureY); // body2
		gunModel[5] = new ModelRendererTurbo(this, 207, 125, textureX, textureY); // foreRail1
		gunModel[6] = new ModelRendererTurbo(this, 206, 105, textureX, textureY); // foreRail2
		gunModel[7] = new ModelRendererTurbo(this, 100, 151, textureX, textureY); // foreRail3
		gunModel[8] = new ModelRendererTurbo(this, 100, 130, textureX, textureY); // foreRail4
		gunModel[9] = new ModelRendererTurbo(this, 100, 140, textureX, textureY); // foreRail5
		gunModel[10] = new ModelRendererTurbo(this, 1, 122, textureX, textureY); // railPart0
		gunModel[11] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // railPart11
		gunModel[12] = new ModelRendererTurbo(this, 1, 112, textureX, textureY); // railPart7
		gunModel[13] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // railPart2
		gunModel[14] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // railPart3
		gunModel[15] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // railPart4
		gunModel[16] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // railPart5
		gunModel[17] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // railPart10
		gunModel[18] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // railPart9
		gunModel[19] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // railPart8
		gunModel[20] = new ModelRendererTurbo(this, 207, 144, textureX, textureY); // Box 0
		gunModel[21] = new ModelRendererTurbo(this, 100, 43, textureX, textureY); // Box 4
		gunModel[22] = new ModelRendererTurbo(this, 100, 120, textureX, textureY); // Box 5
		gunModel[23] = new ModelRendererTurbo(this, 207, 133, textureX, textureY); // Box 6
		gunModel[24] = new ModelRendererTurbo(this, 100, 111, textureX, textureY); // Box 7
		gunModel[25] = new ModelRendererTurbo(this, 207, 153, textureX, textureY); // Box 8
		gunModel[26] = new ModelRendererTurbo(this, 207, 117, textureX, textureY); // Box 12
		gunModel[27] = new ModelRendererTurbo(this, 1, 89, textureX, textureY); // Box 48
		gunModel[28] = new ModelRendererTurbo(this, 125, 81, textureX, textureY); // Box 1
		gunModel[29] = new ModelRendererTurbo(this, 125, 91, textureX, textureY); // Box 2
		gunModel[30] = new ModelRendererTurbo(this, 100, 81, textureX, textureY); // Box 6
		gunModel[31] = new ModelRendererTurbo(this, 100, 91, textureX, textureY); // Box 7
		gunModel[32] = new ModelRendererTurbo(this, 180, 5, textureX, textureY); // Box 8
		gunModel[33] = new ModelRendererTurbo(this, 145, 70, textureX, textureY); // Box 9
		gunModel[34] = new ModelRendererTurbo(this, 167, 31, textureX, textureY); // Box 10
		gunModel[35] = new ModelRendererTurbo(this, 167, 20, textureX, textureY); // Box 11
		gunModel[36] = new ModelRendererTurbo(this, 159, 5, textureX, textureY); // Box 12
		gunModel[37] = new ModelRendererTurbo(this, 20, 102, textureX, textureY); // Box 13
		gunModel[38] = new ModelRendererTurbo(this, 1, 54, textureX, textureY); // Box 14
		gunModel[39] = new ModelRendererTurbo(this, 1, 78, textureX, textureY); // Box 15
		gunModel[40] = new ModelRendererTurbo(this, 57, 54, textureX, textureY); // Box 16
		gunModel[41] = new ModelRendererTurbo(this, 57, 78, textureX, textureY); // Box 17
		gunModel[42] = new ModelRendererTurbo(this, 38, 54, textureX, textureY); // Box 19
		gunModel[43] = new ModelRendererTurbo(this, 1, 144, textureX, textureY); // Box 69
		gunModel[44] = new ModelRendererTurbo(this, 28, 154, textureX, textureY); // Box 69
		gunModel[45] = new ModelRendererTurbo(this, 59, 156, textureX, textureY); // Box 69
		gunModel[46] = new ModelRendererTurbo(this, 1, 166, textureX, textureY); // Box 69
		gunModel[47] = new ModelRendererTurbo(this, 28, 170, textureX, textureY); // Box 69
		gunModel[48] = new ModelRendererTurbo(this, 1, 154, textureX, textureY); // Box 69
		gunModel[49] = new ModelRendererTurbo(this, 100, 70, textureX, textureY); // Box 31
		gunModel[50] = new ModelRendererTurbo(this, 167, 52, textureX, textureY); // Box 0
		gunModel[51] = new ModelRendererTurbo(this, 167, 49, textureX, textureY); // Box 1
		gunModel[52] = new ModelRendererTurbo(this, 167, 45, textureX, textureY); // Box 2
		gunModel[53] = new ModelRendererTurbo(this, 1, 45, textureX, textureY); // Box 5
		gunModel[54] = new ModelRendererTurbo(this, 1, 35, textureX, textureY); // Box 6
		gunModel[55] = new ModelRendererTurbo(this, 46, 34, textureX, textureY); // Box 7
		gunModel[56] = new ModelRendererTurbo(this, 46, 44, textureX, textureY); // Box 8
		gunModel[57] = new ModelRendererTurbo(this, 46, 44, textureX, textureY); // Box 9
		gunModel[58] = new ModelRendererTurbo(this, 46, 34, textureX, textureY); // Box 10
		gunModel[59] = new ModelRendererTurbo(this, 1, 31, textureX, textureY); // Box 11
		gunModel[60] = new ModelRendererTurbo(this, 1, 31, textureX, textureY); // Box 12
		gunModel[61] = new ModelRendererTurbo(this, 20, 24, textureX, textureY); // Box 13
		gunModel[62] = new ModelRendererTurbo(this, 11, 27, textureX, textureY); // Box 14
		gunModel[63] = new ModelRendererTurbo(this, 18, 27, textureX, textureY); // Box 15
		gunModel[64] = new ModelRendererTurbo(this, 6, 21, textureX, textureY); // Box 16
		gunModel[65] = new ModelRendererTurbo(this, 1, 21, textureX, textureY); // Box 17
		gunModel[66] = new ModelRendererTurbo(this, 6, 25, textureX, textureY); // Box 18
		gunModel[67] = new ModelRendererTurbo(this, 20, 21, textureX, textureY); // Box 20
		gunModel[68] = new ModelRendererTurbo(this, 11, 21, textureX, textureY); // Box 21
		gunModel[69] = new ModelRendererTurbo(this, 27, 21, textureX, textureY); // Box 22
		gunModel[70] = new ModelRendererTurbo(this, 56, 21, textureX, textureY); // Box 23
		gunModel[71] = new ModelRendererTurbo(this, 44, 21, textureX, textureY); // Box 24
		gunModel[72] = new ModelRendererTurbo(this, 51, 21, textureX, textureY); // Box 25
		gunModel[73] = new ModelRendererTurbo(this, 39, 102, textureX, textureY); // Box 26
		gunModel[74] = new ModelRendererTurbo(this, 1, 101, textureX, textureY); // Box 27
		gunModel[75] = new ModelRendererTurbo(this, 38, 90, textureX, textureY); // Box 28
		gunModel[76] = new ModelRendererTurbo(this, 100, 101, textureX, textureY); // Box 5
		gunModel[77] = new ModelRendererTurbo(this, 52, 135, textureX, textureY); // Box 12
		gunModel[78] = new ModelRendererTurbo(this, 100, 175, textureX, textureY); // Box 13
		gunModel[79] = new ModelRendererTurbo(this, 53, 191, textureX, textureY); // Box 14
		gunModel[80] = new ModelRendererTurbo(this, 34, 191, textureX, textureY); // Box 15
		gunModel[81] = new ModelRendererTurbo(this, 1, 192, textureX, textureY); // Box 16
		gunModel[82] = new ModelRendererTurbo(this, 51, 223, textureX, textureY); // Box 17
		gunModel[83] = new ModelRendererTurbo(this, 1, 225, textureX, textureY); // Box 18
		gunModel[84] = new ModelRendererTurbo(this, 26, 225, textureX, textureY); // Box 19
		gunModel[85] = new ModelRendererTurbo(this, 34, 213, textureX, textureY); // Box 20
		gunModel[86] = new ModelRendererTurbo(this, 236, 133, textureX, textureY); // Box 21
		gunModel[87] = new ModelRendererTurbo(this, 100, 162, textureX, textureY); // Box 22
		gunModel[88] = new ModelRendererTurbo(this, 159, 162, textureX, textureY); // Box 23
		gunModel[89] = new ModelRendererTurbo(this, 1, 132, textureX, textureY); // Box 24
		gunModel[90] = new ModelRendererTurbo(this, 146, 90, textureX, textureY); // Box 26
		gunModel[91] = new ModelRendererTurbo(this, 1, 212, textureX, textureY); // Box 27
		gunModel[92] = new ModelRendererTurbo(this, 52, 145, textureX, textureY); // Box 28
		gunModel[93] = new ModelRendererTurbo(this, 59, 172, textureX, textureY); // Box 29
		gunModel[94] = new ModelRendererTurbo(this, 38, 78, textureX, textureY); // Box 34
		gunModel[95] = new ModelRendererTurbo(this, 190, 55, textureX, textureY); // Box 143

		gunModel[0].addShapeBox(0F, 0F, 0F, 1, 6, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F); // barrelLeft
		gunModel[0].setRotationPoint(68.5F, -21.5F, 1F);

		gunModel[1].addBox(0F, 0F, 0F, 1, 6, 2, 0F); // barrelMiddle
		gunModel[1].setRotationPoint(68.5F, -21.5F, -1F);

		gunModel[2].addShapeBox(0F, 0F, 0F, 1, 6, 2, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // barrelRight
		gunModel[2].setRotationPoint(68.5F, -21.5F, -3F);

		gunModel[3].addShapeBox(0F, 0F, 0F, 24, 8, 1, 0F, 0F, 0F, 0F, -12F, 0F, 0F, -12F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, -12F, -4F, 0F, -12F, -4F, 0F, 0F, -4F, 0F); // body10
		gunModel[3].setRotationPoint(49.5F, -13F, -3.2F);

		gunModel[4].addBox(0F, 0F, 0F, 18, 8, 8, 0F); // body2
		gunModel[4].setRotationPoint(-11F, -16F, -4F);

		gunModel[5].addShapeBox(0F, 0F, 0F, 45, 1, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // foreRail1
		gunModel[5].setRotationPoint(24F, -9F, -3F);

		gunModel[6].addBox(0F, 0F, 0F, 45, 4, 6, 0F); // foreRail2
		gunModel[6].setRotationPoint(24F, -13F, -3F);

		gunModel[7].addShapeBox(0F, 0F, 0F, 45, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // foreRail3
		gunModel[7].setRotationPoint(24F, -16F, -4F);

		gunModel[8].addBox(0F, 0F, 0F, 45, 1, 8, 0F); // foreRail4
		gunModel[8].setRotationPoint(24F, -17F, -4F);

		gunModel[9].addShapeBox(0F, 0F, 0F, 45, 2, 8, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // foreRail5
		gunModel[9].setRotationPoint(24F, -23F, -4F);

		gunModel[10].addBox(0F, 0F, 0F, 21, 2, 7, 0F); // railPart0
		gunModel[10].setRotationPoint(4F, -24F, -3.5F);

		gunModel[11].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // railPart11
		gunModel[11].setRotationPoint(44F, -6F, -3.5F);

		gunModel[12].addBox(0F, 0F, 0F, 21, 2, 7, 0F); // railPart7
		gunModel[12].setRotationPoint(26F, -8F, -3.5F);

		gunModel[13].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // railPart2
		gunModel[13].setRotationPoint(4F, -26F, -3.5F);

		gunModel[14].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // railPart3
		gunModel[14].setRotationPoint(10F, -26F, -3.5F);

		gunModel[15].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // railPart4
		gunModel[15].setRotationPoint(16F, -26F, -3.5F);

		gunModel[16].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // railPart5
		gunModel[16].setRotationPoint(22F, -26F, -3.5F);

		gunModel[17].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // railPart10
		gunModel[17].setRotationPoint(38F, -6F, -3.5F);

		gunModel[18].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // railPart9
		gunModel[18].setRotationPoint(32F, -6F, -3.5F);

		gunModel[19].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // railPart8
		gunModel[19].setRotationPoint(26F, -6F, -3.5F);

		gunModel[20].addShapeBox(0F, 0F, 0F, 26, 1, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 0
		gunModel[20].setRotationPoint(23F, -8F, -3.5F);

		gunModel[21].addBox(0F, 0F, 0F, 25, 5, 7, 0F); // Box 4
		gunModel[21].setRotationPoint(-1F, -21F, -4F);

		gunModel[22].addBox(0F, 0F, 0F, 45, 1, 8, 0F); // Box 5
		gunModel[22].setRotationPoint(24F, -21F, -4F);

		gunModel[23].addShapeBox(0F, 0F, 0F, 6, 2, 8, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 6
		gunModel[23].setRotationPoint(61F, -19F, -4F);

		gunModel[24].addBox(0F, 0F, 0F, 45, 1, 7, 0F); // Box 7
		gunModel[24].setRotationPoint(24F, -20F, -3.5F);

		gunModel[25].addShapeBox(0F, 0F, 0F, 25, 2, 8, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 8
		gunModel[25].setRotationPoint(-1F, -23F, -4F);

		gunModel[26].addShapeBox(0F, 0F, 0F, 45, 1, 6, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 12
		gunModel[26].setRotationPoint(24F, -14F, -3F);

		gunModel[27].addBox(0F, 0F, 0F, 10, 3, 8, 0F); // Box 48
		gunModel[27].setRotationPoint(-3F, -8F, -4F);

		gunModel[28].addShapeBox(0F, 0F, 0F, 5, 2, 7, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 1
		gunModel[28].setRotationPoint(-11F, -21F, -3.5F);

		gunModel[29].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, -2F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, -2F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 2
		gunModel[29].setRotationPoint(-9F, -23F, -3.5F);

		gunModel[30].addBox(0F, 0F, 0F, 5, 2, 7, 0F); // Box 6
		gunModel[30].setRotationPoint(-6F, -21F, -3.5F);

		gunModel[31].addShapeBox(0F, 0F, 0F, 5, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 7
		gunModel[31].setRotationPoint(-6F, -23F, -3.5F);

		gunModel[32].addShapeBox(0F, 0F, 0F, 2, 4, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F); // Box 8
		gunModel[32].setRotationPoint(7F, -12F, -4F);

		gunModel[33].addBox(0F, 0F, 0F, 17, 3, 7, 0F); // Box 9
		gunModel[33].setRotationPoint(7F, -11F, -3.5F);

		gunModel[34].addBox(0F, 0F, 0F, 15, 5, 8, 0F); // Box 10
		gunModel[34].setRotationPoint(9F, -16F, -4F);

		gunModel[35].addBox(0F, 0F, 0F, 25, 3, 7, 0F); // Box 11
		gunModel[35].setRotationPoint(24F, -11F, -3.5F);

		gunModel[36].addBox(0F, 0F, 0F, 2, 4, 8, 0F); // Box 12
		gunModel[36].setRotationPoint(7F, -16F, -4F);

		gunModel[37].addShapeBox(0F, 0F, 0F, 1, 1, 8, 0F, 1F, 0F, -1F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 13
		gunModel[37].setRotationPoint(-5F, -8F, -4F);

		gunModel[38].addShapeBox(0F, 0F, 0F, 10, 15, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 8F, -3F, 0F, -8F, 0F, 0F, -8F, 0F, 0F, 8F, -3F, 0F); // Box 14
		gunModel[38].setRotationPoint(-4F, -5F, -4F);

		gunModel[39].addShapeBox(0F, 0F, 0F, 10, 2, 8, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F); // Box 15
		gunModel[39].setRotationPoint(-12F, 10F, -4F);

		gunModel[40].addShapeBox(0F, 0F, 0F, 1, 12, 8, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 8F, 0F, -1F, -8F, 0F, 0F, -8F, 0F, 0F, 8F, 0F, -1F); // Box 16
		gunModel[40].setRotationPoint(-5F, -5F, -4F);

		gunModel[41].addShapeBox(0F, 0F, 0F, 1, 2, 8, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -0.5F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -1F); // Box 17
		gunModel[41].setRotationPoint(-13F, 7F, -4F);

		gunModel[42].addShapeBox(0F, 0F, 0F, 1, 15, 8, 0F, -8F, 0F, 0F, 8F, 0F, -1F, 8F, 0F, -1F, -8F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 19
		gunModel[42].setRotationPoint(-2F, -5F, -4F);

		gunModel[43].addShapeBox(0F, 0F, 0F, 18, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 69
		gunModel[43].setRotationPoint(-44F, -18F, -3.5F);

		gunModel[44].addBox(0F, 0F, 0F, 5, 5, 10, 0F); // Box 69
		gunModel[44].setRotationPoint(-49F, -17.5F, -5F);

		gunModel[45].addShapeBox(0F, 0F, 0F, 5, 3, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F); // Box 69
		gunModel[45].setRotationPoint(-49F, -12.5F, -5F);

		gunModel[46].addBox(0F, 0F, 0F, 3, 15, 10, 0F); // Box 69
		gunModel[46].setRotationPoint(-49F, -9.5F, -5F);

		gunModel[47].addShapeBox(0F, 0F, 0F, 5, 1, 10, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 69
		gunModel[47].setRotationPoint(-49F, -18.5F, -5F);

		gunModel[48].addShapeBox(0F, 0F, 0F, 3, 1, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 69
		gunModel[48].setRotationPoint(-49F, 5.5F, -5F);

		gunModel[49].addBox(0F, 0F, 0F, 10, 3, 7, 0F); // Box 31
		gunModel[49].setRotationPoint(-11F, -19F, -3.5F);

		gunModel[50].addBox(0F, 0F, 0F, 10, 5, 1, 0F); // Box 0
		gunModel[50].setRotationPoint(-1F, -21F, 3F);

		gunModel[51].addBox(0F, 0F, 0F, 15, 1, 1, 0F); // Box 1
		gunModel[51].setRotationPoint(9F, -21F, 3F);

		gunModel[52].addBox(0F, 0F, 0F, 15, 2, 1, 0F); // Box 2
		gunModel[52].setRotationPoint(9F, -18F, 3F);

		gunModel[53].addShapeBox(0F, 0F, 0F, 15, 1, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 5
		gunModel[53].setRotationPoint(37F, -26F, -3.5F);

		gunModel[54].addBox(0F, 0F, 0F, 15, 2, 7, 0F); // Box 6
		gunModel[54].setRotationPoint(37F, -25F, -3.5F);

		gunModel[55].addShapeBox(0F, 0F, 0F, 3, 1, 8, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 7
		gunModel[55].setRotationPoint(38F, -23F, -4F);

		gunModel[56].addBox(0F, 0F, 0F, 3, 1, 8, 0F); // Box 8
		gunModel[56].setRotationPoint(38F, -22F, -4F);

		gunModel[57].addBox(0F, 0F, 0F, 3, 1, 8, 0F); // Box 9
		gunModel[57].setRotationPoint(48F, -22F, -4F);

		gunModel[58].addShapeBox(0F, 0F, 0F, 3, 1, 8, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 10
		gunModel[58].setRotationPoint(48F, -23F, -4F);

		gunModel[59].addShapeBox(0F, 0F, 0F, 3, 1, 2, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 11
		gunModel[59].setRotationPoint(34F, -25F, -2.5F);

		gunModel[60].addShapeBox(0F, 0F, 0F, 3, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 12
		gunModel[60].setRotationPoint(34F, -24F, -2.5F);

		gunModel[61].addBox(0F, 0F, 0F, 2, 1, 1, 0F); // Box 13
		gunModel[61].setRotationPoint(32F, -24.5F, -2F);

		gunModel[62].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F, 0F, -0.5F, 0F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, -0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0.5F, 0F); // Box 14
		gunModel[62].setRotationPoint(30F, -24.5F, -4F);

		gunModel[63].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F, 0F, -2F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, -1F, 0F, 2F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, -1F); // Box 15
		gunModel[63].setRotationPoint(28F, -24F, -4F);

		gunModel[64].addShapeBox(0F, 0F, 0F, 1, 2, 1, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 16
		gunModel[64].setRotationPoint(27F, -22F, -5F);

		gunModel[65].addShapeBox(0F, 0F, 0F, 1, 8, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F); // Box 17
		gunModel[65].setRotationPoint(27F, -20F, -5F);

		gunModel[66].addShapeBox(0F, 0F, 0F, 1, 3, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, -1F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 2F, -1F, 0F); // Box 18
		gunModel[66].setRotationPoint(26F, -12F, -5F);

		gunModel[67].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F); // Box 20
		gunModel[67].setRotationPoint(22F, -10F, -5F);

		gunModel[68].addBox(0F, 0F, 0F, 3, 1, 1, 0F); // Box 21
		gunModel[68].setRotationPoint(19F, -10F, -4.5F);

		gunModel[69].addBox(0F, 0F, 0F, 7, 5, 1, 0F); // Box 22
		gunModel[69].setRotationPoint(9F, -11F, -4.5F);

		gunModel[70].addShapeBox(0F, 0F, 0F, 1, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F); // Box 23
		gunModel[70].setRotationPoint(16F, -8F, -4.5F);

		gunModel[71].addBox(0F, 0F, 0F, 2, 4, 1, 0F); // Box 24
		gunModel[71].setRotationPoint(17F, -11F, -4.5F);

		gunModel[72].addBox(0F, 0F, 0F, 1, 3, 1, 0F); // Box 25
		gunModel[72].setRotationPoint(16F, -11F, -4.5F);

		gunModel[73].addShapeBox(0F, 0F, 0F, 2, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F); // Box 26
		gunModel[73].setRotationPoint(-5F, -8F, -4F);

		gunModel[74].addShapeBox(0F, 0F, 0F, 1, 2, 8, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 27
		gunModel[74].setRotationPoint(-5F, -7F, -4F);

		gunModel[75].addBox(0F, 0F, 0F, 1, 2, 8, 0F); // Box 28
		gunModel[75].setRotationPoint(-4F, -7F, -4F);

		gunModel[76].addBox(0F, 0F, 0F, 39, 2, 7, 0F); // Box 5
		gunModel[76].setRotationPoint(24F, -19F, -3.5F);

		gunModel[77].addBox(0F, 0F, 0F, 15, 2, 7, 0F); // Box 12
		gunModel[77].setRotationPoint(-26F, -18F, -3.5F);

		gunModel[78].addBox(0F, 0F, 0F, 14, 8, 8, 0F); // Box 13
		gunModel[78].setRotationPoint(-25F, -16F, -4F);

		gunModel[79].addShapeBox(0F, 0F, 0F, 1, 12, 8, 0F, -8F, 0F, 0F, 8F, 0F, -1F, 8F, 0F, -1F, -8F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 14
		gunModel[79].setRotationPoint(-22F, -8F, -4F);

		gunModel[80].addShapeBox(0F, 0F, 0F, 1, 12, 8, 0F, 0F, 0F, 0F, 8F, 0F, 0F, 8F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 15
		gunModel[80].setRotationPoint(-23F, -8F, -4F);

		gunModel[81].addBox(0F, 0F, 0F, 8, 11, 8, 0F); // Box 16
		gunModel[81].setRotationPoint(-46F, -8F, -4F);

		gunModel[82].addBox(0F, 0F, 0F, 15, 7, 8, 0F); // Box 17
		gunModel[82].setRotationPoint(-38F, -8F, -4F);

		gunModel[83].addShapeBox(0F, 0F, 0F, 4, 5, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 0F, 0F); // Box 18
		gunModel[83].setRotationPoint(-38F, -1F, -4F);

		gunModel[84].addShapeBox(0F, 0F, 0F, 4, 5, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F); // Box 19
		gunModel[84].setRotationPoint(-27F, -1F, -4F);

		gunModel[85].addBox(0F, 0F, 0F, 7, 1, 8, 0F); // Box 20
		gunModel[85].setRotationPoint(-34F, -1F, -4F);

		gunModel[86].addBox(0F, 0F, 0F, 2, 2, 8, 0F); // Box 21
		gunModel[86].setRotationPoint(67F, -19F, -4F);

		gunModel[87].addBox(0F, 0F, 0F, 21, 4, 8, 0F); // Box 22
		gunModel[87].setRotationPoint(-46F, -12F, -4F);

		gunModel[88].addShapeBox(0F, 0F, 0F, 4, 4, 8, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 23
		gunModel[88].setRotationPoint(-29F, -16F, -4F);

		gunModel[89].addBox(0F, 0F, 0F, 18, 4, 7, 0F); // Box 24
		gunModel[89].setRotationPoint(-44F, -16F, -3.5F);

		gunModel[90].addShapeBox(0F, 0F, 0F, 17, 3, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F); // Box 26
		gunModel[90].setRotationPoint(7F, -8F, -3.5F);

		gunModel[91].addShapeBox(0F, 0F, 0F, 8, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F); // Box 27
		gunModel[91].setRotationPoint(-46F, 3F, -4F);

		gunModel[92].addShapeBox(0F, 0F, 0F, 15, 1, 7, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 28
		gunModel[92].setRotationPoint(-26F, -19F, -3.5F);

		gunModel[93].addShapeBox(0F, 0F, 0F, 9, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 29
		gunModel[93].setRotationPoint(-14F, -8F, -4F);

		gunModel[94].addShapeBox(0F, 0F, 0F, 1, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 34
		gunModel[94].setRotationPoint(-2F, 10F, -4F);

		gunModel[95].addBox(0F, 0F, 0F, 2, 2, 1, 0F); // Box 143
		gunModel[95].setRotationPoint(22F, -20F, 3F);


		defaultScopeModel = new ModelRendererTurbo[21];
		defaultScopeModel[0] = new ModelRendererTurbo(this, 39, 1, textureX, textureY); // Box 35
		defaultScopeModel[1] = new ModelRendererTurbo(this, 58, 1, textureX, textureY); // Box 35
		defaultScopeModel[2] = new ModelRendererTurbo(this, 37, 16, textureX, textureY); // Box 35
		defaultScopeModel[3] = new ModelRendererTurbo(this, 2, 11, textureX, textureY); // Box 35
		defaultScopeModel[4] = new ModelRendererTurbo(this, 2, 11, textureX, textureY); // Box 35
		defaultScopeModel[5] = new ModelRendererTurbo(this, 39, 1, textureX, textureY); // Box 35
		defaultScopeModel[6] = new ModelRendererTurbo(this, 58, 1, textureX, textureY); // Box 35
		defaultScopeModel[7] = new ModelRendererTurbo(this, 2, 15, textureX, textureY); // Box 35
		defaultScopeModel[8] = new ModelRendererTurbo(this, 1, 11, textureX, textureY); // Box 35
		defaultScopeModel[9] = new ModelRendererTurbo(this, 32, 13, textureX, textureY); // Box 35
		defaultScopeModel[10] = new ModelRendererTurbo(this, 32, 13, textureX, textureY); // Box 35
		defaultScopeModel[11] = new ModelRendererTurbo(this, 14, 15, textureX, textureY); // Box 35
		defaultScopeModel[12] = new ModelRendererTurbo(this, 14, 15, textureX, textureY); // Box 35
		defaultScopeModel[13] = new ModelRendererTurbo(this, 22, 3, textureX, textureY); // Box 35
		defaultScopeModel[14] = new ModelRendererTurbo(this, 22, 3, textureX, textureY); // Box 35
		defaultScopeModel[15] = new ModelRendererTurbo(this, 2, 15, textureX, textureY); // Box 35
		defaultScopeModel[16] = new ModelRendererTurbo(this, 2, 15, textureX, textureY); // Box 35
		defaultScopeModel[17] = new ModelRendererTurbo(this, 22, 3, textureX, textureY); // Box 35
		defaultScopeModel[18] = new ModelRendererTurbo(this, 22, 3, textureX, textureY); // Box 35
		defaultScopeModel[19] = new ModelRendererTurbo(this, 2, 15, textureX, textureY); // Box 35
		defaultScopeModel[20] = new ModelRendererTurbo(this, 22, 14, textureX, textureY); // Box 35

		defaultScopeModel[0].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 35
		defaultScopeModel[0].setRotationPoint(1.5F, -26F, -3.5F);

		defaultScopeModel[1].addBox(0F, 0F, 0F, 4, 2, 7, 0F); // Box 35
		defaultScopeModel[1].setRotationPoint(-0.5F, -24F, -3.5F);

		defaultScopeModel[2].addBox(0F, 0F, 0F, 2, 1, 3, 0F); // Box 35
		defaultScopeModel[2].setRotationPoint(-0.5F, -28F, -1.5F);

		defaultScopeModel[3].addBox(0F, 0F, 0F, 2, 2, 1, 0F); // Box 35
		defaultScopeModel[3].setRotationPoint(-0.5F, -30F, -1.5F);

		defaultScopeModel[4].addBox(0F, 0F, 0F, 2, 2, 1, 0F); // Box 35
		defaultScopeModel[4].setRotationPoint(-0.5F, -30F, 0.5F);

		defaultScopeModel[5].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 35
		defaultScopeModel[5].setRotationPoint(64.5F, -25.5F, -3.5F);

		defaultScopeModel[6].addBox(0F, 0F, 0F, 4, 2, 7, 0F); // Box 35
		defaultScopeModel[6].setRotationPoint(62.5F, -23.5F, -3.5F);

		defaultScopeModel[7].addBox(0F, 0F, 0F, 2, 2, 1, 0F); // Box 35
		defaultScopeModel[7].setRotationPoint(62.5F, -25.5F, -2.5F);

		defaultScopeModel[8].addShapeBox(0F, 0F, 0F, 2, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 35
		defaultScopeModel[8].setRotationPoint(62.5F, -26.5F, -4F);

		defaultScopeModel[9].addBox(0F, 0F, 0F, 2, 4, 1, 0F); // Box 35
		defaultScopeModel[9].setRotationPoint(62.5F, -30.5F, -4F);

		defaultScopeModel[10].addBox(0F, 0F, 0F, 2, 4, 1, 0F); // Box 35
		defaultScopeModel[10].setRotationPoint(62.5F, -30.5F, 3F);

		defaultScopeModel[11].addShapeBox(0F, 0F, 0F, 2, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 35
		defaultScopeModel[11].setRotationPoint(62.5F, -32.5F, 3F);

		defaultScopeModel[12].addShapeBox(0F, 0F, 0F, 2, 2, 1, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 35
		defaultScopeModel[12].setRotationPoint(62.5F, -32.5F, -4F);

		defaultScopeModel[13].addShapeBox(0F, 0F, 0F, 2, 1, 6, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 35
		defaultScopeModel[13].setRotationPoint(62.5F, -25.5F, -3F);

		defaultScopeModel[14].addShapeBox(0F, 0F, 0F, 2, 1, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 35
		defaultScopeModel[14].setRotationPoint(62.5F, -24.5F, -3F);

		defaultScopeModel[15].addBox(0F, 0F, 0F, 2, 2, 1, 0F); // Box 35
		defaultScopeModel[15].setRotationPoint(62.5F, -25.5F, 1.5F);

		defaultScopeModel[16].addBox(0F, 0F, 0F, 2, 2, 1, 0F); // Box 35
		defaultScopeModel[16].setRotationPoint(-0.5F, -26F, -2.5F);

		defaultScopeModel[17].addShapeBox(0F, 0F, 0F, 2, 1, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 35
		defaultScopeModel[17].setRotationPoint(-0.5F, -25F, -3F);

		defaultScopeModel[18].addShapeBox(0F, 0F, 0F, 2, 1, 6, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 35
		defaultScopeModel[18].setRotationPoint(-0.5F, -26F, -3F);

		defaultScopeModel[19].addBox(0F, 0F, 0F, 2, 2, 1, 0F); // Box 35
		defaultScopeModel[19].setRotationPoint(-0.5F, -26F, 1.5F);

		defaultScopeModel[20].addBox(0F, 0F, 0F, 2, 1, 5, 0F); // Box 35
		defaultScopeModel[20].setRotationPoint(-0.5F, -27F, -2.5F);


		ammoModel = new ModelRendererTurbo[20];
		ammoModel[0] = new ModelRendererTurbo(this, 217, 45, textureX, textureY); // Box 20
		ammoModel[1] = new ModelRendererTurbo(this, 234, 45, textureX, textureY); // Box 21
		ammoModel[2] = new ModelRendererTurbo(this, 200, 45, textureX, textureY); // Box 22
		ammoModel[3] = new ModelRendererTurbo(this, 251, 46, textureX, textureY); // Box 23
		ammoModel[4] = new ModelRendererTurbo(this, 251, 46, textureX, textureY); // Box 24
		ammoModel[5] = new ModelRendererTurbo(this, 247, 79, textureX, textureY); // Box 27
		ammoModel[6] = new ModelRendererTurbo(this, 200, 78, textureX, textureY); // Box 28
		ammoModel[7] = new ModelRendererTurbo(this, 200, 87, textureX, textureY); // Box 29
		ammoModel[8] = new ModelRendererTurbo(this, 229, 87, textureX, textureY); // Box 30
		ammoModel[9] = new ModelRendererTurbo(this, 200, 70, textureX, textureY); // Box 45
		ammoModel[10] = new ModelRendererTurbo(this, 251, 71, textureX, textureY); // Box 46
		ammoModel[11] = new ModelRendererTurbo(this, 234, 70, textureX, textureY); // Box 48
		ammoModel[12] = new ModelRendererTurbo(this, 251, 71, textureX, textureY); // Box 49
		ammoModel[13] = new ModelRendererTurbo(this, 217, 70, textureX, textureY); // Box 50
		ammoModel[14] = new ModelRendererTurbo(this, 234, 45, textureX, textureY); // Box 6
		ammoModel[15] = new ModelRendererTurbo(this, 234, 70, textureX, textureY); // Box 7
		ammoModel[16] = new ModelRendererTurbo(this, 251, 71, textureX, textureY); // Box 8
		ammoModel[17] = new ModelRendererTurbo(this, 251, 46, textureX, textureY); // Box 9
		ammoModel[18] = new ModelRendererTurbo(this, 264, 45, textureX, textureY); // Box 10
		ammoModel[19] = new ModelRendererTurbo(this, 264, 70, textureX, textureY); // Box 11

		ammoModel[0].addBox(0F, 0F, 0F, 2, 18, 6, 0F); // Box 20
		ammoModel[0].setRotationPoint(-38F, -7F, -3F);

		ammoModel[1].addBox(0F, 0F, 0F, 2, 18, 6, 0F); // Box 21
		ammoModel[1].setRotationPoint(-28F, -7F, -3F);

		ammoModel[2].addBox(0F, 0F, 0F, 2, 18, 6, 0F); // Box 22
		ammoModel[2].setRotationPoint(-25F, -7F, -3F);

		ammoModel[3].addBox(0F, 0F, 0F, 1, 18, 5, 0F); // Box 23
		ammoModel[3].setRotationPoint(-29F, -7F, -2.5F);

		ammoModel[4].addBox(0F, 0F, 0F, 1, 18, 5, 0F); // Box 24
		ammoModel[4].setRotationPoint(-26F, -7F, -2.5F);

		ammoModel[5].addBox(0F, 0F, 0F, 15, 1, 6, 0F); // Box 27
		ammoModel[5].setRotationPoint(-38F, -8F, -3F);

		ammoModel[6].addShapeBox(0F, 0F, 0F, 15, 2, 6, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 28
		ammoModel[6].setRotationPoint(-38F, -10F, -3F);

		ammoModel[7].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 29
		ammoModel[7].setRotationPoint(-37F, -11F, -1.5F);

		ammoModel[8].addShapeBox(0F, 0F, 0F, 4, 1, 3, 0F, 0F, 0F, -0.5F, 0F, -0.5F, -1F, 0F, -0.5F, -1F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F); // Box 30
		ammoModel[8].setRotationPoint(-28F, -11F, -1.5F);

		ammoModel[9].addShapeBox(0F, 0F, 0F, 2, 1, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F, 0F, 0F, -1F); // Box 45
		ammoModel[9].setRotationPoint(-25F, 11F, -3F);

		ammoModel[10].addShapeBox(0F, 0F, 0F, 1, 1, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 46
		ammoModel[10].setRotationPoint(-26F, 11F, -2.5F);

		ammoModel[11].addShapeBox(0F, 0F, 0F, 2, 1, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 48
		ammoModel[11].setRotationPoint(-28F, 11F, -3F);

		ammoModel[12].addShapeBox(0F, 0F, 0F, 1, 1, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 49
		ammoModel[12].setRotationPoint(-29F, 11F, -2.5F);

		ammoModel[13].addShapeBox(0F, 0F, 0F, 2, 1, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, -1F, 0F, -1F); // Box 50
		ammoModel[13].setRotationPoint(-38F, 11F, -3F);

		ammoModel[14].addBox(0F, 0F, 0F, 2, 18, 6, 0F); // Box 6
		ammoModel[14].setRotationPoint(-31F, -7F, -3F);

		ammoModel[15].addShapeBox(0F, 0F, 0F, 2, 1, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 7
		ammoModel[15].setRotationPoint(-31F, 11F, -3F);

		ammoModel[16].addShapeBox(0F, 0F, 0F, 1, 1, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 8
		ammoModel[16].setRotationPoint(-32F, 11F, -2.5F);

		ammoModel[17].addBox(0F, 0F, 0F, 1, 18, 5, 0F); // Box 9
		ammoModel[17].setRotationPoint(-32F, -7F, -2.5F);

		ammoModel[18].addBox(0F, 0F, 0F, 4, 18, 6, 0F); // Box 10
		ammoModel[18].setRotationPoint(-36F, -7F, -3F);

		ammoModel[19].addShapeBox(0F, 0F, 0F, 4, 1, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 11
		ammoModel[19].setRotationPoint(-36F, 11F, -3F);


		pumpModel = new ModelRendererTurbo[2];
		pumpModel[0] = new ModelRendererTurbo(this, 61, 35, textureX, textureY); // Box 3
		pumpModel[1] = new ModelRendererTurbo(this, 61, 35, textureX, textureY); // Box 4

		pumpModel[0].addShapeBox(0F, 0F, 0F, 2, 1, 5, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 3
		pumpModel[0].setRotationPoint(20F, -20F, 3F);

		pumpModel[1].addShapeBox(0F, 0F, 0F, 2, 1, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 4
		pumpModel[1].setRotationPoint(20F, -19F, 3F);
		
		barrelAttachPoint = new Vector3f(94F /16F, 11F /16F, 0F /16F);
		scopeAttachPoint = new Vector3f(16F /16F, 20F /16F, 0F /16F);
		gripAttachPoint = new Vector3f(40 /16F, 3F /16F, 0F /16F);

		gunSlideDistance = 0.8F;
		animationType = EnumAnimationType.CUSTOM;

		/* ----Start of Reload Block---- */
		/* Reload Name: Amended Side Clip */
		rotateGunVertical = 70F;
		tiltGun = 10F;
		translateGun = new Vector3f(0.5F, -0.2F, 0F);

		rotateClipVertical = -90F;
		translateClip = new Vector3f(-2F, -2F, 0F);
		/* ----End of Reload Block---- */

		translateAll(0F, 0F, 0F);


		flipAll();
	}
}