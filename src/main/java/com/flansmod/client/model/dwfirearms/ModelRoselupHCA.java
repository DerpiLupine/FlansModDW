package com.flansmod.client.model.dwfirearms; //Path where the model is located

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelRoselupHCA extends ModelGun //Same as Filename
{
	int textureX = 512;
	int textureY = 256;

	public ModelRoselupHCA() //Same as Filename
	{
		gunModel = new ModelRendererTurbo[76];
		gunModel[0] = new ModelRendererTurbo(this, 96, 132, textureX, textureY); // ammo4
		gunModel[1] = new ModelRendererTurbo(this, 96, 80, textureX, textureY); // body1
		gunModel[2] = new ModelRendererTurbo(this, 141, 93, textureX, textureY); // body2
		gunModel[3] = new ModelRendererTurbo(this, 96, 93, textureX, textureY); // body3
		gunModel[4] = new ModelRendererTurbo(this, 141, 98, textureX, textureY); // body4
		gunModel[5] = new ModelRendererTurbo(this, 215, 114, textureX, textureY); // body5
		gunModel[6] = new ModelRendererTurbo(this, 96, 64, textureX, textureY); // body6
		gunModel[7] = new ModelRendererTurbo(this, 96, 33, textureX, textureY); // lowerBarrelTop
		gunModel[8] = new ModelRendererTurbo(this, 96, 1, textureX, textureY); // mainBarrelTop
		gunModel[9] = new ModelRendererTurbo(this, 96, 11, textureX, textureY); // mainBarrelMiddle
		gunModel[10] = new ModelRendererTurbo(this, 96, 22, textureX, textureY); // mainBarrelBottom
		gunModel[11] = new ModelRendererTurbo(this, 96, 54, textureX, textureY); // lowerBarrelBottom
		gunModel[12] = new ModelRendererTurbo(this, 96, 43, textureX, textureY); // lowerBarrelMiddle
		gunModel[13] = new ModelRendererTurbo(this, 47, 46, textureX, textureY); // canister1
		gunModel[14] = new ModelRendererTurbo(this, 26, 24, textureX, textureY); // canister2
		gunModel[15] = new ModelRendererTurbo(this, 26, 15, textureX, textureY); // canister3
		gunModel[16] = new ModelRendererTurbo(this, 26, 33, textureX, textureY); // canister4
		gunModel[17] = new ModelRendererTurbo(this, 1, 14, textureX, textureY); // ironSight4
		gunModel[18] = new ModelRendererTurbo(this, 64, 24, textureX, textureY); // meter1
		gunModel[19] = new ModelRendererTurbo(this, 64, 34, textureX, textureY); // meter1-2
		gunModel[20] = new ModelRendererTurbo(this, 64, 44, textureX, textureY); // meter2
		gunModel[21] = new ModelRendererTurbo(this, 10, 11, textureX, textureY); // pipe1
		gunModel[22] = new ModelRendererTurbo(this, 32, 8, textureX, textureY); // pipe2
		gunModel[23] = new ModelRendererTurbo(this, 10, 16, textureX, textureY); // pipe3
		gunModel[24] = new ModelRendererTurbo(this, 1, 22, textureX, textureY); // ringPart1
		gunModel[25] = new ModelRendererTurbo(this, 1, 22, textureX, textureY); // ringPart1-2
		gunModel[26] = new ModelRendererTurbo(this, 1, 22, textureX, textureY); // ringPart1-3
		gunModel[27] = new ModelRendererTurbo(this, 1, 22, textureX, textureY); // ringPart1-4
		gunModel[28] = new ModelRendererTurbo(this, 1, 42, textureX, textureY); // ringPart2
		gunModel[29] = new ModelRendererTurbo(this, 1, 42, textureX, textureY); // ringPart2-2
		gunModel[30] = new ModelRendererTurbo(this, 1, 32, textureX, textureY); // ringPart3
		gunModel[31] = new ModelRendererTurbo(this, 1, 32, textureX, textureY); // ringPart3-2
		gunModel[32] = new ModelRendererTurbo(this, 22, 42, textureX, textureY); // ringPart4
		gunModel[33] = new ModelRendererTurbo(this, 171, 134, textureX, textureY); // Box 0
		gunModel[34] = new ModelRendererTurbo(this, 96, 186, textureX, textureY); // Box 1
		gunModel[35] = new ModelRendererTurbo(this, 171, 134, textureX, textureY); // Box 2
		gunModel[36] = new ModelRendererTurbo(this, 96, 164, textureX, textureY); // Box 3
		gunModel[37] = new ModelRendererTurbo(this, 186, 116, textureX, textureY); // Box 4
		gunModel[38] = new ModelRendererTurbo(this, 123, 116, textureX, textureY); // Box 5
		gunModel[39] = new ModelRendererTurbo(this, 186, 104, textureX, textureY); // Box 6
		gunModel[40] = new ModelRendererTurbo(this, 123, 104, textureX, textureY); // Box 7
		gunModel[41] = new ModelRendererTurbo(this, 96, 103, textureX, textureY); // Box 14
		gunModel[42] = new ModelRendererTurbo(this, 96, 117, textureX, textureY); // Box 18
		gunModel[43] = new ModelRendererTurbo(this, 47, 9, textureX, textureY); // rail1
		gunModel[44] = new ModelRendererTurbo(this, 43, 1, textureX, textureY); // rail2
		gunModel[45] = new ModelRendererTurbo(this, 53, 15, textureX, textureY); // rail3
		gunModel[46] = new ModelRendererTurbo(this, 53, 15, textureX, textureY); // rail3-2
		gunModel[47] = new ModelRendererTurbo(this, 177, 64, textureX, textureY); // Box 29
		gunModel[48] = new ModelRendererTurbo(this, 177, 64, textureX, textureY); // Box 30
		gunModel[49] = new ModelRendererTurbo(this, 206, 65, textureX, textureY); // Box 32
		gunModel[50] = new ModelRendererTurbo(this, 177, 71, textureX, textureY); // Box 33
		gunModel[51] = new ModelRendererTurbo(this, 190, 93, textureX, textureY); // canister1
		gunModel[52] = new ModelRendererTurbo(this, 190, 93, textureX, textureY); // canister1
		gunModel[53] = new ModelRendererTurbo(this, 172, 93, textureX, textureY); // canister2
		gunModel[54] = new ModelRendererTurbo(this, 172, 93, textureX, textureY); // canister2
		gunModel[55] = new ModelRendererTurbo(this, 172, 93, textureX, textureY); // canister2
		gunModel[56] = new ModelRendererTurbo(this, 172, 93, textureX, textureY); // canister2
		gunModel[57] = new ModelRendererTurbo(this, 181, 93, textureX, textureY); // canister3
		gunModel[58] = new ModelRendererTurbo(this, 181, 93, textureX, textureY); // canister3
		gunModel[59] = new ModelRendererTurbo(this, 177, 71, textureX, textureY); // Box 36
		gunModel[60] = new ModelRendererTurbo(this, 190, 93, textureX, textureY); // Box 37
		gunModel[61] = new ModelRendererTurbo(this, 172, 93, textureX, textureY); // Box 38
		gunModel[62] = new ModelRendererTurbo(this, 181, 93, textureX, textureY); // Box 39
		gunModel[63] = new ModelRendererTurbo(this, 172, 93, textureX, textureY); // Box 40
		gunModel[64] = new ModelRendererTurbo(this, 195, 93, textureX, textureY); // Box 41
		gunModel[65] = new ModelRendererTurbo(this, 195, 97, textureX, textureY); // Box 42
		gunModel[66] = new ModelRendererTurbo(this, 159, 190, textureX, textureY); // Box 46
		gunModel[67] = new ModelRendererTurbo(this, 159, 190, textureX, textureY); // Box 47
		gunModel[68] = new ModelRendererTurbo(this, 159, 190, textureX, textureY); // Box 48
		gunModel[69] = new ModelRendererTurbo(this, 178, 183, textureX, textureY); // Box 49
		gunModel[70] = new ModelRendererTurbo(this, 178, 183, textureX, textureY); // Box 50
		gunModel[71] = new ModelRendererTurbo(this, 178, 183, textureX, textureY); // Box 51
		gunModel[72] = new ModelRendererTurbo(this, 178, 187, textureX, textureY); // Box 52
		gunModel[73] = new ModelRendererTurbo(this, 147, 144, textureX, textureY); // Box 53
		gunModel[74] = new ModelRendererTurbo(this, 162, 132, textureX, textureY); // Box 54
		gunModel[75] = new ModelRendererTurbo(this, 147, 132, textureX, textureY); // Box 55

		gunModel[0].addBox(0F, 0F, 0F, 13, 7, 24, 0F); // ammo4
		gunModel[0].setRotationPoint(18F, -6F, -12F);

		gunModel[1].addShapeBox(0F, 0F, 0F, 39, 2, 10, 0F, -1F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, -1F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body1
		gunModel[1].setRotationPoint(-3F, -23F, -5F);

		gunModel[2].addBox(0F, 0F, 0F, 12, 1, 3, 0F); // body2
		gunModel[2].setRotationPoint(19F, -21F, -5F);

		gunModel[3].addBox(0F, 0F, 0F, 20, 7, 2, 0F); // body3
		gunModel[3].setRotationPoint(-1F, -21F, -5F);

		gunModel[4].addBox(0F, 0F, 0F, 12, 1, 2, 0F); // body4
		gunModel[4].setRotationPoint(19F, -15F, -5F);

		gunModel[5].addBox(0F, 0F, 0F, 5, 7, 10, 0F); // body5
		gunModel[5].setRotationPoint(31F, -21F, -5F);

		gunModel[6].addBox(0F, 0F, 0F, 32, 7, 8, 0F); // body6
		gunModel[6].setRotationPoint(-1F, -21F, -3F);

		gunModel[7].addShapeBox(0F, 0F, 0F, 55, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // lowerBarrelTop
		gunModel[7].setRotationPoint(36F, -14F, -3.5F);

		gunModel[8].addShapeBox(0F, 0F, 0F, 55, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // mainBarrelTop
		gunModel[8].setRotationPoint(36F, -22F, -3.5F);

		gunModel[9].addBox(0F, 0F, 0F, 55, 3, 7, 0F); // mainBarrelMiddle
		gunModel[9].setRotationPoint(36F, -20F, -3.5F);

		gunModel[10].addShapeBox(0F, 0F, 0F, 55, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // mainBarrelBottom
		gunModel[10].setRotationPoint(36F, -17F, -3.5F);

		gunModel[11].addShapeBox(0F, 0F, 0F, 55, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // lowerBarrelBottom
		gunModel[11].setRotationPoint(36F, -9F, -3.5F);

		gunModel[12].addBox(0F, 0F, 0F, 55, 3, 7, 0F); // lowerBarrelMiddle
		gunModel[12].setRotationPoint(36F, -12F, -3.5F);

		gunModel[13].addBox(0F, 0F, 0F, 6, 6, 2, 0F); // canister1
		gunModel[13].setRotationPoint(2F, -21F, -6.5F);

		gunModel[14].addShapeBox(0F, 0F, 0F, 10, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // canister2
		gunModel[14].setRotationPoint(1F, -17F, -11F);

		gunModel[15].addBox(0F, 0F, 0F, 10, 2, 6, 0F); // canister3
		gunModel[15].setRotationPoint(1F, -19F, -11F);

		gunModel[16].addShapeBox(0F, 0F, 0F, 10, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // canister4
		gunModel[16].setRotationPoint(1F, -21F, -11F);

		gunModel[17].addShapeBox(0F, 0F, 0F, 2, 5, 2, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // ironSight4
		gunModel[17].setRotationPoint(86F, -27.5F, -1F);

		gunModel[18].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // meter1
		gunModel[18].setRotationPoint(3F, -23F, -10F);

		gunModel[19].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // meter1-2
		gunModel[19].setRotationPoint(3F, -28F, -10F);

		gunModel[20].addBox(0F, 0F, 0F, 2, 3, 7, 0F); // meter2
		gunModel[20].setRotationPoint(3F, -26F, -10F);

		gunModel[21].addBox(0F, 0F, 0F, 3, 2, 2, 0F); // pipe1
		gunModel[21].setRotationPoint(11F, -19F, -9F);

		gunModel[22].addBox(0F, 0F, 0F, 2, 2, 4, 0F); // pipe2
		gunModel[22].setRotationPoint(14F, -19F, -9F);

		gunModel[23].addBox(0F, 0F, 0F, 4, 4, 1, 0F); // pipe3
		gunModel[23].setRotationPoint(13F, -20F, -5.5F);

		gunModel[24].addShapeBox(0F, 0F, 0F, 2, 1, 8, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F); // ringPart1
		gunModel[24].setRotationPoint(86F, -8F, -4F);

		gunModel[25].addShapeBox(0F, 0F, 0F, 2, 1, 8, 0F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F); // ringPart1-2
		gunModel[25].setRotationPoint(86F, -14F, -4F);

		gunModel[26].addShapeBox(0F, 0F, 0F, 2, 1, 8, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F); // ringPart1-3
		gunModel[26].setRotationPoint(86F, -16F, -4F);

		gunModel[27].addShapeBox(0F, 0F, 0F, 2, 1, 8, 0F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F); // ringPart1-4
		gunModel[27].setRotationPoint(86F, -22F, -4F);

		gunModel[28].addBox(0F, 0F, 0F, 2, 4, 8, 0F); // ringPart2
		gunModel[28].setRotationPoint(86F, -12.5F, -4F);

		gunModel[29].addBox(0F, 0F, 0F, 2, 4, 8, 0F); // ringPart2-2
		gunModel[29].setRotationPoint(86F, -20.5F, -4F);

		gunModel[30].addShapeBox(0F, 0F, 0F, 4, 1, 8, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F); // ringPart3
		gunModel[30].setRotationPoint(36F, -16F, -4F);

		gunModel[31].addShapeBox(0F, 0F, 0F, 4, 1, 8, 0F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F); // ringPart3-2
		gunModel[31].setRotationPoint(36F, -22F, -4F);

		gunModel[32].addBox(0F, 0F, 0F, 4, 4, 8, 0F); // ringPart4
		gunModel[32].setRotationPoint(36F, -20.5F, -4F);

		gunModel[33].addShapeBox(0F, 0F, 0F, 13, 5, 24, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 0
		gunModel[33].setRotationPoint(18F, -11F, -12F);

		gunModel[34].addShapeBox(0F, 0F, 0F, 13, 3, 21, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 1
		gunModel[34].setRotationPoint(18F, -14F, -12F);

		gunModel[35].addShapeBox(0F, 0F, 0F, 13, 5, 24, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 2
		gunModel[35].setRotationPoint(18F, 1F, -12F);

		gunModel[36].addShapeBox(0F, 0F, 0F, 13, 3, 18, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -5F, 0F, 0F, -5F, 0F, 0F, -5F, 0F, 0F, -5F); // Box 3
		gunModel[36].setRotationPoint(18F, 6F, -9F);

		gunModel[37].addBox(0F, 0F, 0F, 5, 6, 9, 0F); // Box 4
		gunModel[37].setRotationPoint(31F, -14F, -4.5F);

		gunModel[38].addBox(0F, 0F, 0F, 22, 6, 9, 0F); // Box 5
		gunModel[38].setRotationPoint(-4F, -14F, -4.5F);

		gunModel[39].addShapeBox(0F, 0F, 0F, 5, 2, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 6
		gunModel[39].setRotationPoint(31F, -8F, -4.5F);

		gunModel[40].addShapeBox(0F, 0F, 0F, 22, 2, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 7
		gunModel[40].setRotationPoint(-4F, -8F, -4.5F);

		gunModel[41].addShapeBox(0F, 0F, 0F, 3, 3, 10, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 14
		gunModel[41].setRotationPoint(-4F, -21F, -5F);

		gunModel[42].addBox(0F, 0F, 0F, 3, 4, 10, 0F); // Box 18
		gunModel[42].setRotationPoint(-4F, -18F, -5F);

		gunModel[43].addBox(0F, 0F, 0F, 18, 1, 4, 0F); // rail1
		gunModel[43].setRotationPoint(14F, -24F, -2F);

		gunModel[44].addBox(0F, 0F, 0F, 18, 1, 6, 0F); // rail2
		gunModel[44].setRotationPoint(14F, -25F, -3F);

		gunModel[45].addShapeBox(0F, 0F, 0F, 18, 1, 1, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // rail3
		gunModel[45].setRotationPoint(14F, -26F, 2F);

		gunModel[46].addShapeBox(0F, 0F, 0F, 18, 1, 1, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // rail3-2
		gunModel[46].setRotationPoint(14F, -26F, -3F);

		gunModel[47].addShapeBox(0F, 0F, 0F, 12, 4, 2, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 29
		gunModel[47].setRotationPoint(20F, -20F, 4.5F);

		gunModel[48].addShapeBox(0F, 0F, 0F, 12, 4, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F); // Box 30
		gunModel[48].setRotationPoint(20F, -20F, 6.5F);

		gunModel[49].addBox(0F, 0F, 0F, 2, 2, 3, 0F); // Box 32
		gunModel[49].setRotationPoint(32F, -19F, 4.5F);

		gunModel[50].addShapeBox(0F, 0F, 0F, 16, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 33
		gunModel[50].setRotationPoint(4F, -18F, 5.5F);

		gunModel[51].addBox(0F, 0F, 0F, 1, 6, 1, 0F); // canister1
		gunModel[51].setRotationPoint(4F, -17F, 6F);

		gunModel[52].addBox(0F, 0F, 0F, 1, 6, 1, 0F); // canister1
		gunModel[52].setRotationPoint(8F, -17F, 6F);

		gunModel[53].addShapeBox(0F, 0F, 0F, 1, 5, 3, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // canister2
		gunModel[53].setRotationPoint(9F, -16.5F, 5F);

		gunModel[54].addShapeBox(0F, 0F, 0F, 1, 5, 3, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // canister2
		gunModel[54].setRotationPoint(7F, -16.5F, 5F);

		gunModel[55].addShapeBox(0F, 0F, 0F, 1, 5, 3, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // canister2
		gunModel[55].setRotationPoint(5F, -16.5F, 5F);

		gunModel[56].addShapeBox(0F, 0F, 0F, 1, 5, 3, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // canister2
		gunModel[56].setRotationPoint(3F, -16.5F, 5F);

		gunModel[57].addBox(0F, 0F, 0F, 1, 5, 3, 0F); // canister3
		gunModel[57].setRotationPoint(8F, -16.5F, 5F);

		gunModel[58].addBox(0F, 0F, 0F, 1, 5, 3, 0F); // canister3
		gunModel[58].setRotationPoint(4F, -16.5F, 5F);

		gunModel[59].addShapeBox(0F, 0F, 0F, 16, 1, 2, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 36
		gunModel[59].setRotationPoint(4F, -19F, 5.5F);

		gunModel[60].addBox(0F, 0F, 0F, 1, 6, 1, 0F); // Box 37
		gunModel[60].setRotationPoint(12F, -17F, 6F);

		gunModel[61].addShapeBox(0F, 0F, 0F, 1, 5, 3, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 38
		gunModel[61].setRotationPoint(13F, -16.5F, 5F);

		gunModel[62].addBox(0F, 0F, 0F, 1, 5, 3, 0F); // Box 39
		gunModel[62].setRotationPoint(12F, -16.5F, 5F);

		gunModel[63].addShapeBox(0F, 0F, 0F, 1, 5, 3, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 40
		gunModel[63].setRotationPoint(11F, -16.5F, 5F);

		gunModel[64].addShapeBox(0F, 0F, 0F, 10, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F); // Box 41
		gunModel[64].setRotationPoint(3.5F, -11F, 6.5F);

		gunModel[65].addBox(0F, 0F, 0F, 10, 2, 2, 0F); // Box 42
		gunModel[65].setRotationPoint(3.5F, -11F, 4.5F);

		gunModel[66].addBox(0F, 0F, 0F, 1, 2, 6, 0F); // Box 46
		gunModel[66].setRotationPoint(17.5F, -11F, -11F);

		gunModel[67].addShapeBox(0F, 0F, 0F, 1, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 47
		gunModel[67].setRotationPoint(17.5F, -13F, -11F);

		gunModel[68].addShapeBox(0F, 0F, 0F, 1, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 48
		gunModel[68].setRotationPoint(17.5F, -9F, -11F);

		gunModel[69].addBox(0F, 0F, 0F, 10, 2, 1, 0F); // Box 49
		gunModel[69].setRotationPoint(8.5F, -11F, -5F);

		gunModel[70].addShapeBox(0F, 0F, 0F, 10, 2, 1, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 50
		gunModel[70].setRotationPoint(8.5F, -13F, -5F);

		gunModel[71].addShapeBox(0F, 0F, 0F, 10, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 51
		gunModel[71].setRotationPoint(8.5F, -9F, -5F);

		gunModel[72].addBox(0F, 0F, 0F, 1, 6, 3, 0F); // Box 52
		gunModel[72].setRotationPoint(7.5F, -13F, -7F);

		gunModel[73].addBox(0F, 0F, 0F, 2, 3, 5, 0F); // Box 53
		gunModel[73].setRotationPoint(15F, -6F, -2.5F);

		gunModel[74].addShapeBox(0F, 0F, 0F, 2, 2, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F); // Box 54
		gunModel[74].setRotationPoint(15F, -3F, -2.5F);

		gunModel[75].addBox(0F, 0F, 0F, 2, 6, 5, 0F); // Box 55
		gunModel[75].setRotationPoint(14F, -1F, -2.5F);


		defaultStockModel = new ModelRendererTurbo[8];
		defaultStockModel[0] = new ModelRendererTurbo(this, 1, 165, textureX, textureY); // acidCanister
		defaultStockModel[1] = new ModelRendererTurbo(this, 1, 94, textureX, textureY); // acidCanister
		defaultStockModel[2] = new ModelRendererTurbo(this, 40, 119, textureX, textureY); // acidCanister
		defaultStockModel[3] = new ModelRendererTurbo(this, 40, 119, textureX, textureY); // stock3
		defaultStockModel[4] = new ModelRendererTurbo(this, 1, 119, textureX, textureY); // acidCanister
		defaultStockModel[5] = new ModelRendererTurbo(this, 1, 107, textureX, textureY); // acidCanister
		defaultStockModel[6] = new ModelRendererTurbo(this, 1, 137, textureX, textureY); // acidCanister
		defaultStockModel[7] = new ModelRendererTurbo(this, 24, 137, textureX, textureY); // acidCanister

		defaultStockModel[0].addShapeBox(0F, 0F, 0F, 26, 8, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 10F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 10F, 0F); // acidCanister
		defaultStockModel[0].setRotationPoint(-40F, -11F, -4.5F);

		defaultStockModel[1].addShapeBox(0F, 0F, 0F, 28, 3, 9, 0F, 0F, 0F, -1.5F, -3F, 0F, -1.5F, -3F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // acidCanister
		defaultStockModel[1].setRotationPoint(-42F, -14F, -4.5F);

		defaultStockModel[2].addShapeBox(0F, 0F, 0F, 10, 2, 9, 0F, 0F, 0F, 0F, 0F, 5F, 0F, 0F, 5F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -5F, -1F, 0F, -5F, -1F, 0F, 0F, -1F); // acidCanister
		defaultStockModel[2].setRotationPoint(-14F, -3F, -4.5F);

		defaultStockModel[3].addShapeBox(0F, 0F, 0F, 10, 2, 9, 0F, 0F, -5F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, -5F, -1.5F, 0F, 5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 5F, 0F); // stock3
		defaultStockModel[3].setRotationPoint(-14F, -18F, -4.5F);

		defaultStockModel[4].addShapeBox(0F, 0F, 0F, 10, 8, 9, 0F, 0F, 0F, 0F, 0F, 5F, 0F, 0F, 5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -5F, 0F, 0F, -5F, 0F, 0F, 0F, 0F); // acidCanister
		defaultStockModel[4].setRotationPoint(-14F, -11F, -4.5F);

		defaultStockModel[5].addShapeBox(0F, 0F, 0F, 26, 2, 9, 0F, 0F, -10F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -10F, 0F, 0F, 10F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 10F, -1F); // acidCanister
		defaultStockModel[5].setRotationPoint(-40F, -3F, -4.5F);

		defaultStockModel[6].addBox(0F, 0F, 0F, 2, 18, 9, 0F); // acidCanister
		defaultStockModel[6].setRotationPoint(-42F, -11F, -4.5F);

		defaultStockModel[7].addShapeBox(0F, 0F, 0F, 2, 2, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // acidCanister
		defaultStockModel[7].setRotationPoint(-42F, 7F, -4.5F);


		ammoModel = new ModelRendererTurbo[3];
		ammoModel[0] = new ModelRendererTurbo(this, 24, 149, textureX, textureY); // Box 43
		ammoModel[1] = new ModelRendererTurbo(this, 24, 149, textureX, textureY); // Box 44
		ammoModel[2] = new ModelRendererTurbo(this, 24, 149, textureX, textureY); // Box 45

		ammoModel[0].addShapeBox(0F, 0F, 0F, 10, 2, 5, 0F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 43
		ammoModel[0].setRotationPoint(19.5F, -13F, -10.5F);

		ammoModel[1].addBox(0F, 0F, 0F, 10, 2, 5, 0F); // Box 44
		ammoModel[1].setRotationPoint(19.5F, -11F, -10.5F);

		ammoModel[2].addShapeBox(0F, 0F, 0F, 10, 2, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F); // Box 45
		ammoModel[2].setRotationPoint(19.5F, -9F, -10.5F);


		slideModel = new ModelRendererTurbo[1];
		slideModel[0] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // bolt

		slideModel[0].addShapeBox(0F, 0F, 0F, 12, 5, 1, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // bolt
		slideModel[0].setRotationPoint(19F, -20F, -4.5F);


		pumpModel = new ModelRendererTurbo[21];
		pumpModel[0] = new ModelRendererTurbo(this, 1, 82, textureX, textureY); // pump1
		pumpModel[1] = new ModelRendererTurbo(this, 1, 70, textureX, textureY); // pump1-2
		pumpModel[2] = new ModelRendererTurbo(this, 1, 55, textureX, textureY); // pump2
		pumpModel[3] = new ModelRendererTurbo(this, 72, 160, textureX, textureY); // Box 8
		pumpModel[4] = new ModelRendererTurbo(this, 72, 170, textureX, textureY); // Box 9
		pumpModel[5] = new ModelRendererTurbo(this, 72, 160, textureX, textureY); // Box 10
		pumpModel[6] = new ModelRendererTurbo(this, 72, 198, textureX, textureY); // Box 11
		pumpModel[7] = new ModelRendererTurbo(this, 72, 183, textureX, textureY); // Box 12
		pumpModel[8] = new ModelRendererTurbo(this, 72, 198, textureX, textureY); // Box 13
		pumpModel[9] = new ModelRendererTurbo(this, 1, 183, textureX, textureY); // Box 16
		pumpModel[10] = new ModelRendererTurbo(this, 1, 199, textureX, textureY); // Box 17
		pumpModel[11] = new ModelRendererTurbo(this, 204, 164, textureX, textureY); // Box 19
		pumpModel[12] = new ModelRendererTurbo(this, 204, 164, textureX, textureY); // Box 20
		pumpModel[13] = new ModelRendererTurbo(this, 204, 164, textureX, textureY); // Box 21
		pumpModel[14] = new ModelRendererTurbo(this, 204, 164, textureX, textureY); // Box 22
		pumpModel[15] = new ModelRendererTurbo(this, 159, 173, textureX, textureY); // Box 23
		pumpModel[16] = new ModelRendererTurbo(this, 159, 173, textureX, textureY); // Box 24
		pumpModel[17] = new ModelRendererTurbo(this, 159, 164, textureX, textureY); // Box 25
		pumpModel[18] = new ModelRendererTurbo(this, 201, 173, textureX, textureY); // Box 26
		pumpModel[19] = new ModelRendererTurbo(this, 178, 173, textureX, textureY); // Box 27
		pumpModel[20] = new ModelRendererTurbo(this, 178, 173, textureX, textureY); // Box 28

		pumpModel[0].addShapeBox(0F, 0F, 0F, 22, 2, 9, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // pump1
		pumpModel[0].setRotationPoint(58F, -15F, -4.5F);

		pumpModel[1].addShapeBox(0F, 0F, 0F, 22, 2, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // pump1-2
		pumpModel[1].setRotationPoint(58F, -8F, -4.5F);

		pumpModel[2].addBox(0F, 0F, 0F, 22, 5, 9, 0F); // pump2
		pumpModel[2].setRotationPoint(58F, -13F, -4.5F);

		pumpModel[3].addShapeBox(0F, 0F, 0F, 3, 1, 8, 0F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F); // Box 8
		pumpModel[3].setRotationPoint(80F, -14F, -4F);

		pumpModel[4].addBox(0F, 0F, 0F, 3, 4, 8, 0F); // Box 9
		pumpModel[4].setRotationPoint(80F, -12.5F, -4F);

		pumpModel[5].addShapeBox(0F, 0F, 0F, 3, 1, 8, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F); // Box 10
		pumpModel[5].setRotationPoint(80F, -8F, -4F);

		pumpModel[6].addShapeBox(0F, 0F, 0F, 1, 2, 9, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 11
		pumpModel[6].setRotationPoint(83F, -15F, -4.5F);

		pumpModel[7].addBox(0F, 0F, 0F, 1, 5, 9, 0F); // Box 12
		pumpModel[7].setRotationPoint(83F, -13F, -4.5F);

		pumpModel[8].addShapeBox(0F, 0F, 0F, 1, 2, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 13
		pumpModel[8].setRotationPoint(83F, -8F, -4.5F);

		pumpModel[9].addBox(0F, 0F, 0F, 18, 5, 10, 0F); // Box 16
		pumpModel[9].setRotationPoint(60F, -12.5F, -5F);

		pumpModel[10].addShapeBox(0F, 0F, 0F, 18, 2, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 17
		pumpModel[10].setRotationPoint(60F, -7.5F, -5F);

		pumpModel[11].addShapeBox(0F, 0F, 0F, 3, 2, 6, 0F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F); // Box 19
		pumpModel[11].setRotationPoint(61F, -3.5F, -3F);

		pumpModel[12].addShapeBox(0F, 0F, 0F, 3, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F); // Box 20
		pumpModel[12].setRotationPoint(64F, -3.5F, -3F);

		pumpModel[13].addShapeBox(0F, 0F, 0F, 3, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F); // Box 21
		pumpModel[13].setRotationPoint(64F, -1.5F, -3F);

		pumpModel[14].addShapeBox(0F, 0F, 0F, 3, 2, 6, 0F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 1F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, -1.5F); // Box 22
		pumpModel[14].setRotationPoint(61F, -1.5F, -3F);

		pumpModel[15].addShapeBox(0F, 0F, 0F, 3, 10, 6, 0F, 1F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F); // Box 23
		pumpModel[15].setRotationPoint(61F, 0.5F, -3F);

		pumpModel[16].addShapeBox(0F, 0F, 0F, 3, 10, 6, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F); // Box 24
		pumpModel[16].setRotationPoint(64F, 0.5F, -3F);

		pumpModel[17].addBox(0F, 0F, 0F, 16, 2, 6, 0F); // Box 25
		pumpModel[17].setRotationPoint(61F, -5.5F, -3F);

		pumpModel[18].addShapeBox(0F, 0F, 0F, 3, 3, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -1F, -0.5F, 0F, -1F, -0.5F, 0F, -1F, -1F, 0F, -1F); // Box 26
		pumpModel[18].setRotationPoint(74F, -3.5F, -3F);

		pumpModel[19].addShapeBox(0F, 0F, 0F, 4, 2, 7, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 27
		pumpModel[19].setRotationPoint(62F, -5F, -3.5F);

		pumpModel[20].addShapeBox(0F, 0F, 0F, 4, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 28
		pumpModel[20].setRotationPoint(62F, -3F, -3.5F);

		barrelAttachPoint = new Vector3f(91F /16F, 18.5F /16F, 0F /16F);
		scopeAttachPoint = new Vector3f(24F /16F, 23F /16F, 0F /16F);

		gunSlideDistance = 0.5F;
		animationType = EnumAnimationType.STRIKER;

		numBulletsInReloadAnimation = 16;
		tiltGunTime = 0.100F;
		unloadClipTime = 0.0F;
		loadClipTime = 0.800F;
		untiltGunTime = 0.100F;

		pumpTime = 7;
		pumpDelayAfterReload = 195;
		pumpHandleDistance = 1.2F;

		

		translateAll(0F, 0F, 0F);


		flipAll();
	}
}