package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.ModelAttachment;
import com.flansmod.client.tmt.ModelRendererTurbo;

import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelLPMAngledGrip extends ModelAttachment 
{
	int textureX = 512;
	int textureY = 256;

	public ModelLPMAngledGrip()
	{
		attachmentModel = new ModelRendererTurbo[8];
		attachmentModel[0] = new ModelRendererTurbo(this, 1, 81, textureX, textureY); // angledGrip1
		attachmentModel[1] = new ModelRendererTurbo(this, 1, 91, textureX, textureY); // angledGrip2
		attachmentModel[2] = new ModelRendererTurbo(this, 1, 102, textureX, textureY); // angledGrip3
		attachmentModel[3] = new ModelRendererTurbo(this, 38, 104, textureX, textureY); // angledGrip4
		attachmentModel[4] = new ModelRendererTurbo(this, 1, 115, textureX, textureY); // angledGrip5
		attachmentModel[5] = new ModelRendererTurbo(this, 1, 115, textureX, textureY); // angledGrip5
		attachmentModel[6] = new ModelRendererTurbo(this, 36, 115, textureX, textureY); // angledGrip6
		attachmentModel[7] = new ModelRendererTurbo(this, 36, 115, textureX, textureY); // angledGrip6

		attachmentModel[0].addShapeBox(-12F, 4F, -4F, 25, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // angledGrip1
		attachmentModel[0].setRotationPoint(0F, 0F, 0F);

		attachmentModel[1].addBox(-12F, 2F, -4F, 15, 2, 8, 0F); // angledGrip2
		attachmentModel[1].setRotationPoint(0F, 0F, 0F);

		attachmentModel[2].addBox(3F, 0F, -4F, 10, 4, 8, 0F); // angledGrip3
		attachmentModel[2].setRotationPoint(0F, 0F, 0F);

		attachmentModel[3].addShapeBox(11F, 5F, -3.5F, 2, 3, 7, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // angledGrip4
		attachmentModel[3].setRotationPoint(0F, 0F, 0F);

		attachmentModel[4].addShapeBox(-7F, 1F, -3.5F, 10, 2, 7, 0F, 0F, -8F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, -8F, -2F, 0F, 8F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 8F, 0F); // angledGrip5
		attachmentModel[4].setRotationPoint(0F, 0F, 0F);

		attachmentModel[5].addShapeBox(-7F, 3F, -3.5F, 10, 2, 7, 0F, 0F, -8F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -8F, 0F, 0F, 8F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 8F, -2F); // angledGrip5
		attachmentModel[5].setRotationPoint(0F, 0F, 0F);

		attachmentModel[6].addShapeBox(-12F, 3F, -3.5F, 5, 2, 7, 0F, 0F, 0F, 0F, 0F, -8F, 0F, 0F, -8F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 8F, -2F, 0F, 8F, -2F, 0F, 0F, -2F); // angledGrip6
		attachmentModel[6].setRotationPoint(0F, 0F, 0F);

		attachmentModel[7].addShapeBox(-12F, 1F, -3.5F, 5, 2, 7, 0F, 0F, 0F, -2F, 0F, -8F, -2F, 0F, -8F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 8F, 0F, 0F, 8F, 0F, 0F, 0F, 0F); // angledGrip6
		attachmentModel[7].setRotationPoint(0F, 0F, 0F);

		renderOffset = 0F;

		flipAll();
	}
}