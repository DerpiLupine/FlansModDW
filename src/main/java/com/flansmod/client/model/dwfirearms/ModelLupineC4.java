package com.flansmod.client.model.dwfirearms;

import net.minecraft.client.model.ModelBase;
import net.minecraft.entity.Entity;

import com.flansmod.client.tmt.ModelRendererTurbo;
import org.lwjgl.opengl.GL11;

public class ModelLupineC4 extends ModelBase
{
	public ModelRendererTurbo[] lupinec4Model;

	int textureX = 128;
	int textureY = 64;

	public ModelLupineC4()
	{
		lupinec4Model = new ModelRendererTurbo[7];
		lupinec4Model[0] = new ModelRendererTurbo(this, 1, 39, textureX, textureY); // Box 0
		lupinec4Model[1] = new ModelRendererTurbo(this, 1, 39, textureX, textureY); // Box 1
		lupinec4Model[2] = new ModelRendererTurbo(this, 1, 26, textureX, textureY); // Box 2
		lupinec4Model[3] = new ModelRendererTurbo(this, 1, 15, textureX, textureY); // Box 3
		lupinec4Model[4] = new ModelRendererTurbo(this, 1, 6, textureX, textureY); // Box 4
		lupinec4Model[5] = new ModelRendererTurbo(this, 32, 44, textureX, textureY); // Box 5
		lupinec4Model[6] = new ModelRendererTurbo(this, 32, 48, textureX, textureY); // Box 6

		lupinec4Model[0].addBox(0F, -1F, -9F, 6, 6, 18, 0F); // Box 0
		lupinec4Model[0].setRotationPoint(0F, 0F, 0F);

		lupinec4Model[1].addBox(-6F, -1F, -9F, 6, 6, 18, 0F); // Box 1
		lupinec4Model[1].setRotationPoint(0F, 0F, 0F);

		lupinec4Model[2].addBox(-6.5F, -1.5F, -7F, 13, 7, 5, 0F); // Box 2
		lupinec4Model[2].setRotationPoint(0F, 0F, 0F);

		lupinec4Model[3].addBox(-6.5F, -1.5F, 4F, 13, 7, 3, 0F); // Box 3
		lupinec4Model[3].setRotationPoint(0F, 0F, 0F);

		lupinec4Model[4].addBox(-4F, 5F, -7.5F, 8, 2, 6, 0F); // Box 4
		lupinec4Model[4].setRotationPoint(0F, 0F, 0F);

		lupinec4Model[5].addBox(-3F, 5F, -1.5F, 5, 1, 2, 0F); // Box 5
		lupinec4Model[5].setRotationPoint(0F, 0F, 0F);

		lupinec4Model[6].addBox(2F, 5F, -1.5F, 1, 1, 7, 0F); // Box 6
		lupinec4Model[6].setRotationPoint(0F, 0F, 0F);
	}

	@Override
	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
	{
		for(ModelRendererTurbo mineModelBit : lupinec4Model)
		{
			GL11.glPushMatrix();
			GL11.glScalef(0.33F, 0.33F, 0.33F);
			mineModelBit.render(f5);
			GL11.glPopMatrix();
		}
	}
}