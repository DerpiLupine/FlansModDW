package com.flansmod.client.model.dwfirearms; //Path where the model is located

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelHeavyDupletMG extends ModelGun //Same as Filename
{
	int textureX = 512;
	int textureY = 256;

	public ModelHeavyDupletMG() //Same as Filename
	{
		gunModel = new ModelRendererTurbo[167];
		gunModel[0] = new ModelRendererTurbo(this, 96, 81, textureX, textureY); // body1
		gunModel[1] = new ModelRendererTurbo(this, 111, 94, textureX, textureY); // body2
		gunModel[2] = new ModelRendererTurbo(this, 96, 94, textureX, textureY); // body3
		gunModel[3] = new ModelRendererTurbo(this, 111, 99, textureX, textureY); // body4
		gunModel[4] = new ModelRendererTurbo(this, 201, 34, textureX, textureY); // body5
		gunModel[5] = new ModelRendererTurbo(this, 96, 63, textureX, textureY); // body6
		gunModel[6] = new ModelRendererTurbo(this, 96, 32, textureX, textureY); // lowerBarrelTop
		gunModel[7] = new ModelRendererTurbo(this, 96, 1, textureX, textureY); // mainBarrelTop
		gunModel[8] = new ModelRendererTurbo(this, 96, 11, textureX, textureY); // mainBarrelMiddle
		gunModel[9] = new ModelRendererTurbo(this, 96, 22, textureX, textureY); // mainBarrelBottom
		gunModel[10] = new ModelRendererTurbo(this, 96, 53, textureX, textureY); // lowerBarrelBottom
		gunModel[11] = new ModelRendererTurbo(this, 96, 42, textureX, textureY); // lowerBarrelMiddle
		gunModel[12] = new ModelRendererTurbo(this, 20, 1, textureX, textureY); // ironSight4
		gunModel[13] = new ModelRendererTurbo(this, 1, 40, textureX, textureY); // pump1
		gunModel[14] = new ModelRendererTurbo(this, 1, 40, textureX, textureY); // pump1-2
		gunModel[15] = new ModelRendererTurbo(this, 1, 27, textureX, textureY); // pump2
		gunModel[16] = new ModelRendererTurbo(this, 201, 11, textureX, textureY); // ringPart1
		gunModel[17] = new ModelRendererTurbo(this, 201, 1, textureX, textureY); // ringPart1-2
		gunModel[18] = new ModelRendererTurbo(this, 201, 11, textureX, textureY); // ringPart1-3
		gunModel[19] = new ModelRendererTurbo(this, 201, 1, textureX, textureY); // ringPart1-4
		gunModel[20] = new ModelRendererTurbo(this, 201, 21, textureX, textureY); // ringPart2
		gunModel[21] = new ModelRendererTurbo(this, 201, 21, textureX, textureY); // ringPart2-2
		gunModel[22] = new ModelRendererTurbo(this, 1, 9, textureX, textureY); // ringPart3
		gunModel[23] = new ModelRendererTurbo(this, 1, 9, textureX, textureY); // ringPart3-2
		gunModel[24] = new ModelRendererTurbo(this, 34, 4, textureX, textureY); // ringPart4
		gunModel[25] = new ModelRendererTurbo(this, 196, 118, textureX, textureY); // Box 4
		gunModel[26] = new ModelRendererTurbo(this, 123, 118, textureX, textureY); // Box 5
		gunModel[27] = new ModelRendererTurbo(this, 196, 105, textureX, textureY); // Box 6
		gunModel[28] = new ModelRendererTurbo(this, 123, 105, textureX, textureY); // Box 7
		gunModel[29] = new ModelRendererTurbo(this, 238, 139, textureX, textureY); // Box 8
		gunModel[30] = new ModelRendererTurbo(this, 217, 137, textureX, textureY); // Box 9
		gunModel[31] = new ModelRendererTurbo(this, 238, 139, textureX, textureY); // Box 10
		gunModel[32] = new ModelRendererTurbo(this, 198, 137, textureX, textureY); // Box 11
		gunModel[33] = new ModelRendererTurbo(this, 179, 135, textureX, textureY); // Box 12
		gunModel[34] = new ModelRendererTurbo(this, 198, 137, textureX, textureY); // Box 13
		gunModel[35] = new ModelRendererTurbo(this, 96, 104, textureX, textureY); // Box 14
		gunModel[36] = new ModelRendererTurbo(this, 1, 183, textureX, textureY); // Box 16
		gunModel[37] = new ModelRendererTurbo(this, 1, 199, textureX, textureY); // Box 17
		gunModel[38] = new ModelRendererTurbo(this, 96, 118, textureX, textureY); // Box 18
		gunModel[39] = new ModelRendererTurbo(this, 192, 96, textureX, textureY); // Box 29
		gunModel[40] = new ModelRendererTurbo(this, 192, 96, textureX, textureY); // Box 30
		gunModel[41] = new ModelRendererTurbo(this, 244, 96, textureX, textureY); // Box 32
		gunModel[42] = new ModelRendererTurbo(this, 221, 99, textureX, textureY); // Box 33
		gunModel[43] = new ModelRendererTurbo(this, 162, 95, textureX, textureY); // canister1
		gunModel[44] = new ModelRendererTurbo(this, 162, 95, textureX, textureY); // canister1
		gunModel[45] = new ModelRendererTurbo(this, 144, 94, textureX, textureY); // canister2
		gunModel[46] = new ModelRendererTurbo(this, 144, 94, textureX, textureY); // canister2
		gunModel[47] = new ModelRendererTurbo(this, 144, 94, textureX, textureY); // canister2
		gunModel[48] = new ModelRendererTurbo(this, 144, 94, textureX, textureY); // canister2
		gunModel[49] = new ModelRendererTurbo(this, 153, 94, textureX, textureY); // canister3
		gunModel[50] = new ModelRendererTurbo(this, 153, 94, textureX, textureY); // canister3
		gunModel[51] = new ModelRendererTurbo(this, 221, 99, textureX, textureY); // Box 36
		gunModel[52] = new ModelRendererTurbo(this, 162, 95, textureX, textureY); // Box 37
		gunModel[53] = new ModelRendererTurbo(this, 144, 94, textureX, textureY); // Box 38
		gunModel[54] = new ModelRendererTurbo(this, 153, 94, textureX, textureY); // Box 39
		gunModel[55] = new ModelRendererTurbo(this, 144, 94, textureX, textureY); // Box 40
		gunModel[56] = new ModelRendererTurbo(this, 167, 94, textureX, textureY); // Box 41
		gunModel[57] = new ModelRendererTurbo(this, 167, 98, textureX, textureY); // Box 42
		gunModel[58] = new ModelRendererTurbo(this, 1, 199, textureX, textureY); // Box 0
		gunModel[59] = new ModelRendererTurbo(this, 268, 1, textureX, textureY); // Box 1
		gunModel[60] = new ModelRendererTurbo(this, 268, 11, textureX, textureY); // Box 2
		gunModel[61] = new ModelRendererTurbo(this, 268, 1, textureX, textureY); // Box 3
		gunModel[62] = new ModelRendererTurbo(this, 268, 1, textureX, textureY); // Box 4
		gunModel[63] = new ModelRendererTurbo(this, 268, 11, textureX, textureY); // Box 5
		gunModel[64] = new ModelRendererTurbo(this, 268, 1, textureX, textureY); // Box 6
		gunModel[65] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 7
		gunModel[66] = new ModelRendererTurbo(this, 234, 95, textureX, textureY); // Box 9
		gunModel[67] = new ModelRendererTurbo(this, 234, 95, textureX, textureY); // Box 10
		gunModel[68] = new ModelRendererTurbo(this, 96, 148, textureX, textureY); // Box 11
		gunModel[69] = new ModelRendererTurbo(this, 96, 94, textureX, textureY); // Box 12
		gunModel[70] = new ModelRendererTurbo(this, 111, 99, textureX, textureY); // Box 13
		gunModel[71] = new ModelRendererTurbo(this, 111, 94, textureX, textureY); // Box 14
		gunModel[72] = new ModelRendererTurbo(this, 127, 137, textureX, textureY); // Box 100
		gunModel[73] = new ModelRendererTurbo(this, 127, 137, textureX, textureY); // Box 102
		gunModel[74] = new ModelRendererTurbo(this, 127, 137, textureX, textureY); // Box 103
		gunModel[75] = new ModelRendererTurbo(this, 127, 137, textureX, textureY); // Box 104
		gunModel[76] = new ModelRendererTurbo(this, 196, 88, textureX, textureY); // Box 105
		gunModel[77] = new ModelRendererTurbo(this, 223, 117, textureX, textureY); // Box 106
		gunModel[78] = new ModelRendererTurbo(this, 196, 88, textureX, textureY); // Box 107
		gunModel[79] = new ModelRendererTurbo(this, 169, 81, textureX, textureY); // Box 108
		gunModel[80] = new ModelRendererTurbo(this, 221, 95, textureX, textureY); // Box 109
		gunModel[81] = new ModelRendererTurbo(this, 221, 95, textureX, textureY); // Box 110
		gunModel[82] = new ModelRendererTurbo(this, 150, 136, textureX, textureY); // Box 111
		gunModel[83] = new ModelRendererTurbo(this, 47, 132, textureX, textureY); // Box 5
		gunModel[84] = new ModelRendererTurbo(this, 223, 105, textureX, textureY); // Box 15
		gunModel[85] = new ModelRendererTurbo(this, 96, 133, textureX, textureY); // Box 16
		gunModel[86] = new ModelRendererTurbo(this, 167, 98, textureX, textureY); // Box 3
		gunModel[87] = new ModelRendererTurbo(this, 167, 94, textureX, textureY); // Box 4
		gunModel[88] = new ModelRendererTurbo(this, 162, 95, textureX, textureY); // Box 5
		gunModel[89] = new ModelRendererTurbo(this, 162, 95, textureX, textureY); // Box 6
		gunModel[90] = new ModelRendererTurbo(this, 162, 95, textureX, textureY); // Box 7
		gunModel[91] = new ModelRendererTurbo(this, 144, 94, textureX, textureY); // Box 8
		gunModel[92] = new ModelRendererTurbo(this, 153, 94, textureX, textureY); // Box 9
		gunModel[93] = new ModelRendererTurbo(this, 144, 94, textureX, textureY); // Box 10
		gunModel[94] = new ModelRendererTurbo(this, 144, 94, textureX, textureY); // Box 11
		gunModel[95] = new ModelRendererTurbo(this, 153, 94, textureX, textureY); // Box 12
		gunModel[96] = new ModelRendererTurbo(this, 144, 94, textureX, textureY); // Box 13
		gunModel[97] = new ModelRendererTurbo(this, 144, 94, textureX, textureY); // Box 14
		gunModel[98] = new ModelRendererTurbo(this, 153, 94, textureX, textureY); // Box 15
		gunModel[99] = new ModelRendererTurbo(this, 144, 94, textureX, textureY); // Box 16
		gunModel[100] = new ModelRendererTurbo(this, 221, 99, textureX, textureY); // Box 17
		gunModel[101] = new ModelRendererTurbo(this, 221, 99, textureX, textureY); // Box 18
		gunModel[102] = new ModelRendererTurbo(this, 234, 95, textureX, textureY); // Box 19
		gunModel[103] = new ModelRendererTurbo(this, 234, 95, textureX, textureY); // Box 20
		gunModel[104] = new ModelRendererTurbo(this, 221, 95, textureX, textureY); // Box 21
		gunModel[105] = new ModelRendererTurbo(this, 221, 95, textureX, textureY); // Box 22
		gunModel[106] = new ModelRendererTurbo(this, 192, 96, textureX, textureY); // Box 23
		gunModel[107] = new ModelRendererTurbo(this, 192, 96, textureX, textureY); // Box 24
		gunModel[108] = new ModelRendererTurbo(this, 244, 96, textureX, textureY); // Box 25
		gunModel[109] = new ModelRendererTurbo(this, 96, 133, textureX, textureY); // Box 34
		gunModel[110] = new ModelRendererTurbo(this, 96, 133, textureX, textureY); // Box 35
		gunModel[111] = new ModelRendererTurbo(this, 96, 133, textureX, textureY); // Box 36
		gunModel[112] = new ModelRendererTurbo(this, 1, 51, textureX, textureY); // Box 41
		gunModel[113] = new ModelRendererTurbo(this, 1, 51, textureX, textureY); // Box 42
		gunModel[114] = new ModelRendererTurbo(this, 1, 51, textureX, textureY); // Box 43
		gunModel[115] = new ModelRendererTurbo(this, 1, 51, textureX, textureY); // Box 44
		gunModel[116] = new ModelRendererTurbo(this, 1, 55, textureX, textureY); // Box 45
		gunModel[117] = new ModelRendererTurbo(this, 1, 59, textureX, textureY); // Box 46
		gunModel[118] = new ModelRendererTurbo(this, 40, 137, textureX, textureY); // Box 81
		gunModel[119] = new ModelRendererTurbo(this, 40, 137, textureX, textureY); // Box 82
		gunModel[120] = new ModelRendererTurbo(this, 40, 131, textureX, textureY); // Box 83
		gunModel[121] = new ModelRendererTurbo(this, 238, 61, textureX, textureY); // Box 103
		gunModel[122] = new ModelRendererTurbo(this, 201, 57, textureX, textureY); // Box 104
		gunModel[123] = new ModelRendererTurbo(this, 201, 57, textureX, textureY); // Box 105
		gunModel[124] = new ModelRendererTurbo(this, 201, 57, textureX, textureY); // Box 106
		gunModel[125] = new ModelRendererTurbo(this, 201, 78, textureX, textureY); // Box 107
		gunModel[126] = new ModelRendererTurbo(this, 201, 78, textureX, textureY); // Box 108
		gunModel[127] = new ModelRendererTurbo(this, 201, 78, textureX, textureY); // Box 109
		gunModel[128] = new ModelRendererTurbo(this, 201, 78, textureX, textureY); // Box 110
		gunModel[129] = new ModelRendererTurbo(this, 201, 78, textureX, textureY); // Box 111
		gunModel[130] = new ModelRendererTurbo(this, 201, 78, textureX, textureY); // Box 112
		gunModel[131] = new ModelRendererTurbo(this, 201, 72, textureX, textureY); // Box 113
		gunModel[132] = new ModelRendererTurbo(this, 201, 72, textureX, textureY); // Box 114
		gunModel[133] = new ModelRendererTurbo(this, 201, 72, textureX, textureY); // Box 115
		gunModel[134] = new ModelRendererTurbo(this, 218, 72, textureX, textureY); // Box 116
		gunModel[135] = new ModelRendererTurbo(this, 218, 72, textureX, textureY); // Box 117
		gunModel[136] = new ModelRendererTurbo(this, 218, 72, textureX, textureY); // Box 118
		gunModel[137] = new ModelRendererTurbo(this, 53, 220, textureX, textureY); // Box 119
		gunModel[138] = new ModelRendererTurbo(this, 18, 243, textureX, textureY); // Box 120
		gunModel[139] = new ModelRendererTurbo(this, 1, 232, textureX, textureY); // Box 121
		gunModel[140] = new ModelRendererTurbo(this, 1, 209, textureX, textureY); // Box 122
		gunModel[141] = new ModelRendererTurbo(this, 36, 209, textureX, textureY); // Box 123
		gunModel[142] = new ModelRendererTurbo(this, 18, 232, textureX, textureY); // Box 124
		gunModel[143] = new ModelRendererTurbo(this, 18, 232, textureX, textureY); // Box 125
		gunModel[144] = new ModelRendererTurbo(this, 53, 209, textureX, textureY); // Box 126
		gunModel[145] = new ModelRendererTurbo(this, 67, 10, textureX, textureY); // Box 128
		gunModel[146] = new ModelRendererTurbo(this, 67, 1, textureX, textureY); // Box 129
		gunModel[147] = new ModelRendererTurbo(this, 67, 1, textureX, textureY); // Box 130
		gunModel[148] = new ModelRendererTurbo(this, 38, 78, textureX, textureY); // mounter
		gunModel[149] = new ModelRendererTurbo(this, 67, 25, textureX, textureY); // ironSight2
		gunModel[150] = new ModelRendererTurbo(this, 90, 25, textureX, textureY); // ironSight1
		gunModel[151] = new ModelRendererTurbo(this, 80, 43, textureX, textureY); // largeScope2
		gunModel[152] = new ModelRendererTurbo(this, 80, 43, textureX, textureY); // largeScope2
		gunModel[153] = new ModelRendererTurbo(this, 80, 43, textureX, textureY); // largeScope2
		gunModel[154] = new ModelRendererTurbo(this, 67, 43, textureX, textureY); // largeScope1
		gunModel[155] = new ModelRendererTurbo(this, 80, 43, textureX, textureY); // largeScope2
		gunModel[156] = new ModelRendererTurbo(this, 80, 43, textureX, textureY); // largeScope2
		gunModel[157] = new ModelRendererTurbo(this, 80, 43, textureX, textureY); // largeScope2
		gunModel[158] = new ModelRendererTurbo(this, 67, 37, textureX, textureY); // smallScope1
		gunModel[159] = new ModelRendererTurbo(this, 80, 37, textureX, textureY); // smallScope2
		gunModel[160] = new ModelRendererTurbo(this, 80, 37, textureX, textureY); // smallScope2
		gunModel[161] = new ModelRendererTurbo(this, 80, 37, textureX, textureY); // smallScope2
		gunModel[162] = new ModelRendererTurbo(this, 80, 37, textureX, textureY); // smallScope2
		gunModel[163] = new ModelRendererTurbo(this, 80, 37, textureX, textureY); // smallScope2
		gunModel[164] = new ModelRendererTurbo(this, 80, 37, textureX, textureY); // smallScope2
		gunModel[165] = new ModelRendererTurbo(this, 67, 37, textureX, textureY); // smallScope1
		gunModel[166] = new ModelRendererTurbo(this, 67, 43, textureX, textureY); // largeScope1

		gunModel[0].addShapeBox(0F, 0F, 0F, 26, 2, 10, 0F, -1F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, -1F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body1
		gunModel[0].setRotationPoint(-11F, -23F, -5F);

		gunModel[1].addBox(0F, 0F, 0F, 12, 1, 3, 0F); // body2
		gunModel[1].setRotationPoint(16F, -20F, -8F);

		gunModel[2].addBox(0F, 0F, 0F, 4, 6, 3, 0F); // body3
		gunModel[2].setRotationPoint(12F, -20F, -8F);

		gunModel[3].addBox(0F, 0F, 0F, 12, 1, 2, 0F); // body4
		gunModel[3].setRotationPoint(16F, -15F, -7F);

		gunModel[4].addBox(0F, 0F, 0F, 5, 6, 16, 0F); // body5
		gunModel[4].setRotationPoint(28F, -20F, -8F);

		gunModel[5].addBox(0F, 0F, 0F, 37, 7, 10, 0F); // body6
		gunModel[5].setRotationPoint(-9F, -21F, -5F);

		gunModel[6].addShapeBox(0F, 0F, 0F, 42, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // lowerBarrelTop
		gunModel[6].setRotationPoint(41F, -21F, 0.5F);

		gunModel[7].addShapeBox(0F, 0F, 0F, 42, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // mainBarrelTop
		gunModel[7].setRotationPoint(41F, -21F, -7.5F);

		gunModel[8].addBox(0F, 0F, 0F, 45, 3, 7, 0F); // mainBarrelMiddle
		gunModel[8].setRotationPoint(38F, -19F, -7.5F);

		gunModel[9].addShapeBox(0F, 0F, 0F, 42, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // mainBarrelBottom
		gunModel[9].setRotationPoint(41F, -16F, -7.5F);

		gunModel[10].addShapeBox(0F, 0F, 0F, 42, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // lowerBarrelBottom
		gunModel[10].setRotationPoint(41F, -16F, 0.5F);

		gunModel[11].addBox(0F, 0F, 0F, 42, 3, 7, 0F); // lowerBarrelMiddle
		gunModel[11].setRotationPoint(41F, -19F, 0.5F);

		gunModel[12].addBox(0F, 0F, 0F, 2, 5, 2, 0F); // ironSight4
		gunModel[12].setRotationPoint(85F, -27.5F, -1F);

		gunModel[13].addShapeBox(0F, 0F, 0F, 10, 2, 8, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // pump1
		gunModel[13].setRotationPoint(63F, -14F, -4F);

		gunModel[14].addShapeBox(0F, 0F, 0F, 10, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // pump1-2
		gunModel[14].setRotationPoint(63F, -8F, -4F);

		gunModel[15].addBox(0F, 0F, 0F, 10, 4, 8, 0F); // pump2
		gunModel[15].setRotationPoint(63F, -12F, -4F);

		gunModel[16].addShapeBox(0F, 0F, 0F, 25, 1, 8, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F); // ringPart1
		gunModel[16].setRotationPoint(45F, -15F, 0F);

		gunModel[17].addShapeBox(0F, 0F, 0F, 25, 1, 8, 0F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F); // ringPart1-2
		gunModel[17].setRotationPoint(45F, -21F, 0F);

		gunModel[18].addShapeBox(0F, 0F, 0F, 25, 1, 8, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F); // ringPart1-3
		gunModel[18].setRotationPoint(45F, -15F, -8F);

		gunModel[19].addShapeBox(0F, 0F, 0F, 25, 1, 8, 0F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F); // ringPart1-4
		gunModel[19].setRotationPoint(45F, -21F, -8F);

		gunModel[20].addBox(0F, 0F, 0F, 25, 4, 8, 0F); // ringPart2
		gunModel[20].setRotationPoint(45F, -19.5F, 0F);

		gunModel[21].addBox(0F, 0F, 0F, 25, 4, 8, 0F); // ringPart2-2
		gunModel[21].setRotationPoint(45F, -19.5F, -8F);

		gunModel[22].addShapeBox(0F, 0F, 0F, 8, 1, 16, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F); // ringPart3
		gunModel[22].setRotationPoint(33F, -15F, -8F);

		gunModel[23].addShapeBox(0F, 0F, 0F, 8, 1, 16, 0F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F); // ringPart3-2
		gunModel[23].setRotationPoint(33F, -21F, -8F);

		gunModel[24].addBox(0F, 0F, 0F, 8, 4, 16, 0F); // ringPart4
		gunModel[24].setRotationPoint(33F, -19.5F, -8F);

		gunModel[25].addBox(0F, 0F, 0F, 4, 5, 9, 0F); // Box 4
		gunModel[25].setRotationPoint(29F, -13F, -4.5F);

		gunModel[26].addBox(0F, 0F, 0F, 27, 5, 9, 0F); // Box 5
		gunModel[26].setRotationPoint(-12F, -13F, -4.5F);

		gunModel[27].addShapeBox(0F, 0F, 0F, 4, 2, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 6
		gunModel[27].setRotationPoint(29F, -8F, -4.5F);

		gunModel[28].addShapeBox(0F, 0F, 0F, 27, 2, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 7
		gunModel[28].setRotationPoint(-12F, -8F, -4.5F);

		gunModel[29].addShapeBox(0F, 0F, 0F, 3, 1, 7, 0F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F); // Box 8
		gunModel[29].setRotationPoint(73F, -13F, -3.5F);

		gunModel[30].addBox(0F, 0F, 0F, 3, 3, 7, 0F); // Box 9
		gunModel[30].setRotationPoint(73F, -11.5F, -3.5F);

		gunModel[31].addShapeBox(0F, 0F, 0F, 3, 1, 7, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F); // Box 10
		gunModel[31].setRotationPoint(73F, -8F, -3.5F);

		gunModel[32].addShapeBox(0F, 0F, 0F, 1, 2, 8, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 11
		gunModel[32].setRotationPoint(76F, -14F, -4F);

		gunModel[33].addBox(0F, 0F, 0F, 1, 4, 8, 0F); // Box 12
		gunModel[33].setRotationPoint(76F, -12F, -4F);

		gunModel[34].addShapeBox(0F, 0F, 0F, 1, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 13
		gunModel[34].setRotationPoint(76F, -8F, -4F);

		gunModel[35].addShapeBox(0F, 0F, 0F, 3, 3, 10, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 14
		gunModel[35].setRotationPoint(-12F, -21F, -5F);

		gunModel[36].addBox(0F, 0F, 0F, 30, 7, 8, 0F); // Box 16
		gunModel[36].setRotationPoint(33F, -13.5F, -4F);

		gunModel[37].addShapeBox(0F, 0F, 0F, 30, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 17
		gunModel[37].setRotationPoint(33F, -6.5F, -4F);

		gunModel[38].addBox(0F, 0F, 0F, 3, 4, 10, 0F); // Box 18
		gunModel[38].setRotationPoint(-12F, -18F, -5F);

		gunModel[39].addShapeBox(0F, 0F, 0F, 12, 4, 2, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 29
		gunModel[39].setRotationPoint(16F, -23.5F, -8.5F);

		gunModel[40].addShapeBox(0F, 0F, 0F, 12, 4, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F); // Box 30
		gunModel[40].setRotationPoint(16F, -23.5F, -6.5F);

		gunModel[41].addBox(0F, 0F, 0F, 2, 2, 4, 0F); // Box 32
		gunModel[41].setRotationPoint(28F, -22.5F, -7.5F);

		gunModel[42].addShapeBox(0F, 0F, 0F, 9, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 33
		gunModel[42].setRotationPoint(0F, -18.5F, -7.5F);

		gunModel[43].addBox(0F, 0F, 0F, 1, 6, 1, 0F); // canister1
		gunModel[43].setRotationPoint(0F, -17.5F, -7F);

		gunModel[44].addBox(0F, 0F, 0F, 1, 6, 1, 0F); // canister1
		gunModel[44].setRotationPoint(4F, -17.5F, -7F);

		gunModel[45].addShapeBox(0F, 0F, 0F, 1, 5, 3, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // canister2
		gunModel[45].setRotationPoint(5F, -17F, -8F);

		gunModel[46].addShapeBox(0F, 0F, 0F, 1, 5, 3, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // canister2
		gunModel[46].setRotationPoint(3F, -17F, -8F);

		gunModel[47].addShapeBox(0F, 0F, 0F, 1, 5, 3, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // canister2
		gunModel[47].setRotationPoint(1F, -17F, -8F);

		gunModel[48].addShapeBox(0F, 0F, 0F, 1, 5, 3, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // canister2
		gunModel[48].setRotationPoint(-1F, -17F, -8F);

		gunModel[49].addBox(0F, 0F, 0F, 1, 5, 3, 0F); // canister3
		gunModel[49].setRotationPoint(4F, -17F, -8F);

		gunModel[50].addBox(0F, 0F, 0F, 1, 5, 3, 0F); // canister3
		gunModel[50].setRotationPoint(0F, -17F, -8F);

		gunModel[51].addShapeBox(0F, 0F, 0F, 9, 1, 2, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 36
		gunModel[51].setRotationPoint(0F, -19.5F, -7.5F);

		gunModel[52].addBox(0F, 0F, 0F, 1, 6, 1, 0F); // Box 37
		gunModel[52].setRotationPoint(8F, -17.5F, -7F);

		gunModel[53].addShapeBox(0F, 0F, 0F, 1, 5, 3, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 38
		gunModel[53].setRotationPoint(9F, -17F, -8F);

		gunModel[54].addBox(0F, 0F, 0F, 1, 5, 3, 0F); // Box 39
		gunModel[54].setRotationPoint(8F, -17F, -8F);

		gunModel[55].addShapeBox(0F, 0F, 0F, 1, 5, 3, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 40
		gunModel[55].setRotationPoint(7F, -17F, -8F);

		gunModel[56].addShapeBox(0F, 0F, 0F, 10, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 41
		gunModel[56].setRotationPoint(-0.5F, -11.5F, -7.5F);

		gunModel[57].addBox(0F, 0F, 0F, 10, 2, 2, 0F); // Box 42
		gunModel[57].setRotationPoint(-0.5F, -11.5F, -6.5F);

		gunModel[58].addShapeBox(0F, 0F, 0F, 30, 1, 8, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 0
		gunModel[58].setRotationPoint(33F, -14.5F, -4F);

		gunModel[59].addShapeBox(0F, 0F, 0F, 12, 1, 8, 0F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F); // Box 1
		gunModel[59].setRotationPoint(83F, -21F, 0F);

		gunModel[60].addBox(0F, 0F, 0F, 12, 4, 8, 0F); // Box 2
		gunModel[60].setRotationPoint(83F, -19.5F, 0F);

		gunModel[61].addShapeBox(0F, 0F, 0F, 12, 1, 8, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F); // Box 3
		gunModel[61].setRotationPoint(83F, -15F, 0F);

		gunModel[62].addShapeBox(0F, 0F, 0F, 12, 1, 8, 0F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F); // Box 4
		gunModel[62].setRotationPoint(83F, -21F, -8F);

		gunModel[63].addBox(0F, 0F, 0F, 12, 4, 8, 0F); // Box 5
		gunModel[63].setRotationPoint(83F, -19.5F, -8F);

		gunModel[64].addShapeBox(0F, 0F, 0F, 12, 1, 8, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F); // Box 6
		gunModel[64].setRotationPoint(83F, -15F, -8F);

		gunModel[65].addBox(0F, 0F, 0F, 5, 3, 4, 0F); // Box 7
		gunModel[65].setRotationPoint(85F, -22.5F, -2F);

		gunModel[66].addShapeBox(0F, 0F, 0F, 3, 1, 2, 0F, 0F, 0F, -0.5F, 0F, 3F, -0.5F, 0F, 3F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F); // Box 9
		gunModel[66].setRotationPoint(9F, -19.5F, -7.5F);

		gunModel[67].addShapeBox(0F, 0F, 0F, 3, 1, 2, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 3F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 3F, -0.5F); // Box 10
		gunModel[67].setRotationPoint(9F, -21.5F, -7.5F);

		gunModel[68].addShapeBox(0F, 0F, 0F, 45, 1, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 11
		gunModel[68].setRotationPoint(-12F, -14F, -5F);

		gunModel[69].addBox(0F, 0F, 0F, 4, 6, 3, 0F); // Box 12
		gunModel[69].setRotationPoint(12F, -20F, 5F);

		gunModel[70].addBox(0F, 0F, 0F, 12, 1, 2, 0F); // Box 13
		gunModel[70].setRotationPoint(16F, -15F, 5F);

		gunModel[71].addBox(0F, 0F, 0F, 12, 1, 3, 0F); // Box 14
		gunModel[71].setRotationPoint(16F, -20F, 5F);

		gunModel[72].addShapeBox(0F, 0F, 0F, 2, 1, 9, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 100
		gunModel[72].setRotationPoint(58F, -11F, -4.5F);

		gunModel[73].addShapeBox(0F, 0F, 0F, 2, 1, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 102
		gunModel[73].setRotationPoint(58F, -10F, -4.5F);

		gunModel[74].addShapeBox(0F, 0F, 0F, 2, 1, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 103
		gunModel[74].setRotationPoint(36F, -10F, -4.5F);

		gunModel[75].addShapeBox(0F, 0F, 0F, 2, 1, 9, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 104
		gunModel[75].setRotationPoint(36F, -11F, -4.5F);

		gunModel[76].addShapeBox(0F, 0F, 0F, 15, 2, 3, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 105
		gunModel[76].setRotationPoint(15F, -23F, -5F);

		gunModel[77].addBox(0F, 0F, 0F, 14, 2, 9, 0F); // Box 106
		gunModel[77].setRotationPoint(15F, -13F, -4.5F);

		gunModel[78].addShapeBox(0F, 0F, 0F, 15, 2, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 107
		gunModel[78].setRotationPoint(15F, -23F, 2F);

		gunModel[79].addBox(0F, 0F, 0F, 3, 2, 10, 0F); // Box 108
		gunModel[79].setRotationPoint(30F, -23F, -5F);

		gunModel[80].addShapeBox(0F, 0F, 0F, 4, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 109
		gunModel[80].setRotationPoint(12F, -21.5F, -7.5F);

		gunModel[81].addShapeBox(0F, 0F, 0F, 4, 1, 2, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 110
		gunModel[81].setRotationPoint(12F, -22.5F, -7.5F);

		gunModel[82].addBox(0F, 0F, 0F, 5, 1, 10, 0F); // Box 111
		gunModel[82].setRotationPoint(28F, -21F, -5F);

		gunModel[83].addBox(0F, 0F, 0F, 3, 4, 12, 0F); // Box 5
		gunModel[83].setRotationPoint(29F, -13.5F, 4F);

		gunModel[84].addShapeBox(0F, 0F, 0F, 14, 2, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 15
		gunModel[84].setRotationPoint(15F, -11F, -4.5F);

		gunModel[85].addShapeBox(0F, 0F, 0F, 3, 2, 12, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 16
		gunModel[85].setRotationPoint(29F, -9.5F, 4F);

		gunModel[86].addBox(0F, 0F, 0F, 10, 2, 2, 0F); // Box 3
		gunModel[86].setRotationPoint(-0.5F, -11.5F, 4.5F);

		gunModel[87].addShapeBox(0F, 0F, 0F, 10, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F); // Box 4
		gunModel[87].setRotationPoint(-0.5F, -11.5F, 6.5F);

		gunModel[88].addBox(0F, 0F, 0F, 1, 6, 1, 0F); // Box 5
		gunModel[88].setRotationPoint(0F, -17.5F, 6F);

		gunModel[89].addBox(0F, 0F, 0F, 1, 6, 1, 0F); // Box 6
		gunModel[89].setRotationPoint(4F, -17.5F, 6F);

		gunModel[90].addBox(0F, 0F, 0F, 1, 6, 1, 0F); // Box 7
		gunModel[90].setRotationPoint(8F, -17.5F, 6F);

		gunModel[91].addShapeBox(0F, 0F, 0F, 1, 5, 3, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 8
		gunModel[91].setRotationPoint(9F, -17F, 5F);

		gunModel[92].addBox(0F, 0F, 0F, 1, 5, 3, 0F); // Box 9
		gunModel[92].setRotationPoint(8F, -17F, 5F);

		gunModel[93].addShapeBox(0F, 0F, 0F, 1, 5, 3, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 10
		gunModel[93].setRotationPoint(7F, -17F, 5F);

		gunModel[94].addShapeBox(0F, 0F, 0F, 1, 5, 3, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 11
		gunModel[94].setRotationPoint(5F, -17F, 5F);

		gunModel[95].addBox(0F, 0F, 0F, 1, 5, 3, 0F); // Box 12
		gunModel[95].setRotationPoint(4F, -17F, 5F);

		gunModel[96].addShapeBox(0F, 0F, 0F, 1, 5, 3, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 13
		gunModel[96].setRotationPoint(3F, -17F, 5F);

		gunModel[97].addShapeBox(0F, 0F, 0F, 1, 5, 3, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 14
		gunModel[97].setRotationPoint(1F, -17F, 5F);

		gunModel[98].addBox(0F, 0F, 0F, 1, 5, 3, 0F); // Box 15
		gunModel[98].setRotationPoint(0F, -17F, 5F);

		gunModel[99].addShapeBox(0F, 0F, 0F, 1, 5, 3, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 16
		gunModel[99].setRotationPoint(-1F, -17F, 5F);

		gunModel[100].addShapeBox(0F, 0F, 0F, 9, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 17
		gunModel[100].setRotationPoint(0F, -18.5F, 5.5F);

		gunModel[101].addShapeBox(0F, 0F, 0F, 9, 1, 2, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 18
		gunModel[101].setRotationPoint(0F, -19.5F, 5.5F);

		gunModel[102].addShapeBox(0F, 0F, 0F, 3, 1, 2, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 3F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 3F, -0.5F); // Box 19
		gunModel[102].setRotationPoint(9F, -21.5F, 5.5F);

		gunModel[103].addShapeBox(0F, 0F, 0F, 3, 1, 2, 0F, 0F, 0F, -0.5F, 0F, 3F, -0.5F, 0F, 3F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F); // Box 20
		gunModel[103].setRotationPoint(9F, -19.5F, 5.5F);

		gunModel[104].addShapeBox(0F, 0F, 0F, 4, 1, 2, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 21
		gunModel[104].setRotationPoint(12F, -22.5F, 5.5F);

		gunModel[105].addShapeBox(0F, 0F, 0F, 4, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 22
		gunModel[105].setRotationPoint(12F, -21.5F, 5.5F);

		gunModel[106].addShapeBox(0F, 0F, 0F, 12, 4, 2, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 23
		gunModel[106].setRotationPoint(16F, -23.5F, 4.5F);

		gunModel[107].addShapeBox(0F, 0F, 0F, 12, 4, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F); // Box 24
		gunModel[107].setRotationPoint(16F, -23.5F, 6.5F);

		gunModel[108].addBox(0F, 0F, 0F, 2, 2, 4, 0F); // Box 25
		gunModel[108].setRotationPoint(28F, -22.5F, 3.5F);

		gunModel[109].addShapeBox(0F, 0F, 0F, 3, 2, 12, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 34
		gunModel[109].setRotationPoint(29F, -9.5F, -16F);

		gunModel[110].addBox(0F, 0F, 0F, 3, 2, 12, 0F); // Box 35
		gunModel[110].setRotationPoint(29F, -11.5F, -16F);

		gunModel[111].addShapeBox(0F, 0F, 0F, 3, 2, 12, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 36
		gunModel[111].setRotationPoint(29F, -13.5F, -16F);

		gunModel[112].addShapeBox(0F, 0F, 0F, 14, 1, 2, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 41
		gunModel[112].setRotationPoint(15F, -8F, 0.5F);

		gunModel[113].addShapeBox(0F, 0F, 0F, 14, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 42
		gunModel[113].setRotationPoint(15F, -7F, 0.5F);

		gunModel[114].addShapeBox(0F, 0F, 0F, 14, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 43
		gunModel[114].setRotationPoint(15F, -7F, -2.5F);

		gunModel[115].addShapeBox(0F, 0F, 0F, 14, 1, 2, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 44
		gunModel[115].setRotationPoint(15F, -8F, -2.5F);

		gunModel[116].addBox(0F, 0F, 0F, 10, 1, 2, 0F); // Box 45
		gunModel[116].setRotationPoint(17F, -7F, -1F);

		gunModel[117].addBox(0F, 0F, 0F, 10, 1, 4, 0F); // Box 46
		gunModel[117].setRotationPoint(17F, -6F, -2F);

		gunModel[118].addShapeBox(0F, 0F, 0F, 4, 1, 3, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 81
		gunModel[118].setRotationPoint(28.5F, -15F, 9.5F);

		gunModel[119].addShapeBox(0F, 0F, 0F, 4, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 82
		gunModel[119].setRotationPoint(28.5F, -12F, 9.5F);

		gunModel[120].addBox(0F, 0F, 0F, 4, 2, 3, 0F); // Box 83
		gunModel[120].setRotationPoint(28.5F, -14F, 9.5F);

		gunModel[121].addShapeBox(0F, 0F, 0F, 7, 7, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F); // Box 103
		gunModel[121].setRotationPoint(33.5F, -21F, 6F);

		gunModel[122].addShapeBox(-3F, -3F, -1F, 6, 2, 12, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 104
		gunModel[122].setRotationPoint(37F, -17.5F, 9F);
		gunModel[122].rotateAngleX = 0.17453293F;

		gunModel[123].addBox(-3F, -1F, -1F, 6, 2, 12, 0F); // Box 105
		gunModel[123].setRotationPoint(37F, -17.5F, 9F);
		gunModel[123].rotateAngleX = 0.17453293F;

		gunModel[124].addShapeBox(-3F, 1F, -1F, 6, 2, 12, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F); // Box 106
		gunModel[124].setRotationPoint(37F, -17.5F, 9F);
		gunModel[124].rotateAngleX = 0.17453293F;

		gunModel[125].addShapeBox(-3F, -3F, 12F, 6, 2, 1, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 107
		gunModel[125].setRotationPoint(37F, -17.5F, 9F);
		gunModel[125].rotateAngleX = 0.17453293F;

		gunModel[126].addBox(-3F, -1F, 12F, 6, 2, 1, 0F); // Box 108
		gunModel[126].setRotationPoint(37F, -17.5F, 9F);
		gunModel[126].rotateAngleX = 0.17453293F;

		gunModel[127].addShapeBox(-3F, 1F, 12F, 6, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F); // Box 109
		gunModel[127].setRotationPoint(37F, -17.5F, 9F);
		gunModel[127].rotateAngleX = 0.17453293F;

		gunModel[128].addShapeBox(-3F, -3F, 14F, 6, 2, 1, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 110
		gunModel[128].setRotationPoint(37F, -17.5F, 9F);
		gunModel[128].rotateAngleX = 0.17453293F;

		gunModel[129].addBox(-3F, -1F, 14F, 6, 2, 1, 0F); // Box 111
		gunModel[129].setRotationPoint(37F, -17.5F, 9F);
		gunModel[129].rotateAngleX = 0.17453293F;

		gunModel[130].addShapeBox(-3F, 1F, 14F, 6, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F); // Box 112
		gunModel[130].setRotationPoint(37F, -17.5F, 9F);
		gunModel[130].rotateAngleX = 0.17453293F;

		gunModel[131].addShapeBox(-2.5F, -3F, 11F, 5, 2, 3, 0F, -1.5F, -0.5F, 0F, -1.5F, -0.5F, 0F, -1.5F, -0.5F, 0F, -1.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 113
		gunModel[131].setRotationPoint(37F, -17.5F, 9F);
		gunModel[131].rotateAngleX = 0.17453293F;

		gunModel[132].addBox(-2.5F, -1F, 11F, 5, 2, 3, 0F); // Box 114
		gunModel[132].setRotationPoint(37F, -17.5F, 9F);
		gunModel[132].rotateAngleX = 0.17453293F;

		gunModel[133].addShapeBox(-2.5F, 1F, 11F, 5, 2, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, -0.5F, 0F, -1.5F, -0.5F, 0F, -1.5F, -0.5F, 0F, -1.5F, -0.5F, 0F); // Box 115
		gunModel[133].setRotationPoint(37F, -17.5F, 9F);
		gunModel[133].rotateAngleX = 0.17453293F;

		gunModel[134].addShapeBox(-3F, -3F, 15F, 6, 2, 1, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, -1F, 0F, -2F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 116
		gunModel[134].setRotationPoint(37F, -17.5F, 9F);
		gunModel[134].rotateAngleX = 0.17453293F;

		gunModel[135].addShapeBox(-3F, -1F, 15F, 6, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 117
		gunModel[135].setRotationPoint(37F, -17.5F, 9F);
		gunModel[135].rotateAngleX = 0.17453293F;

		gunModel[136].addShapeBox(-3F, 1F, 15F, 6, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, -1F, 0F, -2F, -1F, 0F); // Box 118
		gunModel[136].setRotationPoint(37F, -17.5F, 9F);
		gunModel[136].rotateAngleX = 0.17453293F;

		gunModel[137].addBox(0F, 0F, 0F, 13, 4, 9, 0F); // Box 119
		gunModel[137].setRotationPoint(-6F, -8F, -4.5F);

		gunModel[138].addShapeBox(0F, 0F, 0F, 1, 4, 7, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 120
		gunModel[138].setRotationPoint(-7F, -8F, -3.5F);

		gunModel[139].addShapeBox(0F, 0F, 0F, 1, 11, 7, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 8F, 0F, -1F, -8F, 0F, 0F, -8F, 0F, 0F, 8F, 0F, -1F); // Box 121
		gunModel[139].setRotationPoint(-7F, -4F, -3.5F);

		gunModel[140].addShapeBox(0F, 0F, 0F, 10, 15, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 8F, -4F, 0F, -8F, 0F, 0F, -8F, 0F, 0F, 8F, -4F, 0F); // Box 122
		gunModel[140].setRotationPoint(-6F, -4F, -3.5F);

		gunModel[141].addShapeBox(0F, 0F, 0F, 1, 15, 7, 0F, -8F, 0F, 0F, 8F, 0F, -1F, 8F, 0F, -1F, -8F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 123
		gunModel[141].setRotationPoint(-4F, -4F, -3.5F);

		gunModel[142].addShapeBox(0F, 0F, 0F, 1, 3, 7, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -1F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, -1F); // Box 124
		gunModel[142].setRotationPoint(-15F, 7F, -3.5F);

		gunModel[143].addShapeBox(0F, 0F, 0F, 1, 3, 7, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 125
		gunModel[143].setRotationPoint(-4F, 11F, -3.5F);

		gunModel[144].addShapeBox(0F, 0F, 0F, 10, 3, 7, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, 0F, 0F); // Box 126
		gunModel[144].setRotationPoint(-14F, 7F, -3.5F);

		gunModel[145].addBox(0F, 0F, 0F, 5, 1, 6, 0F); // Box 128
		gunModel[145].setRotationPoint(5F, -26F, -3F);

		gunModel[146].addShapeBox(0F, 0F, 0F, 2, 1, 7, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 129
		gunModel[146].setRotationPoint(6.5F, -25.5F, -3.5F);

		gunModel[147].addShapeBox(0F, 0F, 0F, 2, 1, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 130
		gunModel[147].setRotationPoint(6.5F, -24.5F, -3.5F);

		gunModel[148].addBox(0F, 0F, 0F, 9, 3, 6, 0F); // mounter
		gunModel[148].setRotationPoint(3F, -25F, -3F);

		gunModel[149].addBox(0F, 0F, 0F, 1, 1, 10, 0F); // ironSight2
		gunModel[149].setRotationPoint(7.5F, -32.5F, -5F);

		gunModel[150].addBox(0F, 0F, 0F, 1, 10, 1, 0F); // ironSight1
		gunModel[150].setRotationPoint(7.5F, -37F, -0.5F);

		gunModel[151].addShapeBox(0F, 0F, 0F, 2, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F); // largeScope2
		gunModel[151].setRotationPoint(7F, -30F, -5.7F);
		gunModel[151].rotateAngleX = 0.78539816F;

		gunModel[152].addShapeBox(0F, 0F, 0F, 2, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F); // largeScope2
		gunModel[152].setRotationPoint(7F, -34F, -5.8F);

		gunModel[153].addShapeBox(0F, 0F, 0F, 2, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F); // largeScope2
		gunModel[153].setRotationPoint(7F, -37F, -3F);
		gunModel[153].rotateAngleX = -0.78539816F;

		gunModel[154].addShapeBox(0F, 0F, 0F, 2, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F); // largeScope1
		gunModel[154].setRotationPoint(7F, -38F, -2F);

		gunModel[155].addShapeBox(0F, 0F, 0F, 2, 4, 1, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // largeScope2
		gunModel[155].setRotationPoint(7F, -36.3F, 2.3F);
		gunModel[155].rotateAngleX = 0.78539816F;

		gunModel[156].addShapeBox(0F, 0F, 0F, 2, 4, 1, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // largeScope2
		gunModel[156].setRotationPoint(7F, -34F, 4.85F);

		gunModel[157].addShapeBox(0F, 0F, 0F, 2, 4, 1, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // largeScope2
		gunModel[157].setRotationPoint(7F, -29.7F, 4.15F);
		gunModel[157].rotateAngleX = -0.78539816F;

		gunModel[158].addShapeBox(0F, 0F, 0F, 2, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F); // smallScope1
		gunModel[158].setRotationPoint(85F, -37F, -2F);

		gunModel[159].addShapeBox(0F, 0F, 0F, 2, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F); // smallScope2
		gunModel[159].setRotationPoint(85F, -37F, -2.2F);
		gunModel[159].rotateAngleX = -0.78539816F;

		gunModel[160].addShapeBox(0F, 0F, 0F, 2, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F); // smallScope2
		gunModel[160].setRotationPoint(85F, -34F, -5F);

		gunModel[161].addShapeBox(0F, 0F, 0F, 2, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F); // smallScope2
		gunModel[161].setRotationPoint(85F, -29.8F, -4.7F);
		gunModel[161].rotateAngleX = 0.78539816F;

		gunModel[162].addShapeBox(0F, 0F, 0F, 2, 4, 1, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // smallScope2
		gunModel[162].setRotationPoint(85F, -30.7F, 4.3F);
		gunModel[162].rotateAngleX = -0.78539816F;

		gunModel[163].addShapeBox(0F, 0F, 0F, 2, 4, 1, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // smallScope2
		gunModel[163].setRotationPoint(85F, -34F, 4F);

		gunModel[164].addShapeBox(0F, 0F, 0F, 2, 4, 1, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // smallScope2
		gunModel[164].setRotationPoint(85F, -36.3F, 1.3F);
		gunModel[164].rotateAngleX = 0.78539816F;

		gunModel[165].addShapeBox(0F, 0F, 0F, 2, 1, 4, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // smallScope1
		gunModel[165].setRotationPoint(85F, -28F, -2F);

		gunModel[166].addShapeBox(0F, 0F, 0F, 2, 1, 4, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // largeScope1
		gunModel[166].setRotationPoint(7F, -27.2F, -2F);


		defaultStockModel = new ModelRendererTurbo[8];
		defaultStockModel[0] = new ModelRendererTurbo(this, 1, 165, textureX, textureY); // acidCanister
		defaultStockModel[1] = new ModelRendererTurbo(this, 1, 94, textureX, textureY); // acidCanister
		defaultStockModel[2] = new ModelRendererTurbo(this, 40, 119, textureX, textureY); // acidCanister
		defaultStockModel[3] = new ModelRendererTurbo(this, 40, 119, textureX, textureY); // stock3
		defaultStockModel[4] = new ModelRendererTurbo(this, 1, 119, textureX, textureY); // acidCanister
		defaultStockModel[5] = new ModelRendererTurbo(this, 1, 107, textureX, textureY); // acidCanister
		defaultStockModel[6] = new ModelRendererTurbo(this, 1, 137, textureX, textureY); // acidCanister
		defaultStockModel[7] = new ModelRendererTurbo(this, 24, 137, textureX, textureY); // acidCanister

		defaultStockModel[0].addShapeBox(0F, 0F, 0F, 26, 8, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 10F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 10F, 0F); // acidCanister
		defaultStockModel[0].setRotationPoint(-48F, -11F, -4.5F);

		defaultStockModel[1].addShapeBox(0F, 0F, 0F, 28, 3, 9, 0F, 0F, 0F, -1.5F, -3F, 0F, -1.5F, -3F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // acidCanister
		defaultStockModel[1].setRotationPoint(-50F, -14F, -4.5F);

		defaultStockModel[2].addShapeBox(0F, 0F, 0F, 10, 2, 9, 0F, 0F, 0F, 0F, 0F, 5F, 0F, 0F, 5F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -5F, -1F, 0F, -5F, -1F, 0F, 0F, -1F); // acidCanister
		defaultStockModel[2].setRotationPoint(-22F, -3F, -4.5F);

		defaultStockModel[3].addShapeBox(0F, 0F, 0F, 10, 2, 9, 0F, 0F, -5F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, -5F, -1.5F, 0F, 5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 5F, 0F); // stock3
		defaultStockModel[3].setRotationPoint(-22F, -18F, -4.5F);

		defaultStockModel[4].addShapeBox(0F, 0F, 0F, 10, 8, 9, 0F, 0F, 0F, 0F, 0F, 5F, 0F, 0F, 5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -5F, 0F, 0F, -5F, 0F, 0F, 0F, 0F); // acidCanister
		defaultStockModel[4].setRotationPoint(-22F, -11F, -4.5F);

		defaultStockModel[5].addShapeBox(0F, 0F, 0F, 26, 2, 9, 0F, 0F, -10F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -10F, 0F, 0F, 10F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 10F, -1F); // acidCanister
		defaultStockModel[5].setRotationPoint(-48F, -3F, -4.5F);

		defaultStockModel[6].addBox(0F, 0F, 0F, 2, 18, 9, 0F); // acidCanister
		defaultStockModel[6].setRotationPoint(-50F, -11F, -4.5F);

		defaultStockModel[7].addShapeBox(0F, 0F, 0F, 2, 2, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // acidCanister
		defaultStockModel[7].setRotationPoint(-50F, 7F, -4.5F);


		ammoModel = new ModelRendererTurbo[47];
		ammoModel[0] = new ModelRendererTurbo(this, 96, 160, textureX, textureY); // ammo4
		ammoModel[1] = new ModelRendererTurbo(this, 96, 188, textureX, textureY); // Box 3
		ammoModel[2] = new ModelRendererTurbo(this, 157, 174, textureX, textureY); // Box 4
		ammoModel[3] = new ModelRendererTurbo(this, 96, 160, textureX, textureY); // Box 0
		ammoModel[4] = new ModelRendererTurbo(this, 96, 188, textureX, textureY); // Box 1
		ammoModel[5] = new ModelRendererTurbo(this, 157, 160, textureX, textureY); // Box 2
		ammoModel[6] = new ModelRendererTurbo(this, 157, 174, textureX, textureY); // Box 37
		ammoModel[7] = new ModelRendererTurbo(this, 1, 70, textureX, textureY); // Box 85
		ammoModel[8] = new ModelRendererTurbo(this, 1, 65, textureX, textureY); // Box 86
		ammoModel[9] = new ModelRendererTurbo(this, 1, 70, textureX, textureY); // Box 87
		ammoModel[10] = new ModelRendererTurbo(this, 1, 65, textureX, textureY); // Box 88
		ammoModel[11] = new ModelRendererTurbo(this, 1, 65, textureX, textureY); // Box 89
		ammoModel[12] = new ModelRendererTurbo(this, 1, 70, textureX, textureY); // Box 90
		ammoModel[13] = new ModelRendererTurbo(this, 96, 207, textureX, textureY); // Box 47
		ammoModel[14] = new ModelRendererTurbo(this, 96, 207, textureX, textureY); // Box 48
		ammoModel[15] = new ModelRendererTurbo(this, 1, 65, textureX, textureY); // Box 49
		ammoModel[16] = new ModelRendererTurbo(this, 1, 70, textureX, textureY); // Box 50
		ammoModel[17] = new ModelRendererTurbo(this, 1, 70, textureX, textureY); // Box 51
		ammoModel[18] = new ModelRendererTurbo(this, 1, 65, textureX, textureY); // Box 52
		ammoModel[19] = new ModelRendererTurbo(this, 1, 65, textureX, textureY); // Box 53
		ammoModel[20] = new ModelRendererTurbo(this, 1, 70, textureX, textureY); // Box 54
		ammoModel[21] = new ModelRendererTurbo(this, 1, 65, textureX, textureY); // Box 55
		ammoModel[22] = new ModelRendererTurbo(this, 1, 70, textureX, textureY); // Box 56
		ammoModel[23] = new ModelRendererTurbo(this, 1, 70, textureX, textureY); // Box 57
		ammoModel[24] = new ModelRendererTurbo(this, 1, 65, textureX, textureY); // Box 58
		ammoModel[25] = new ModelRendererTurbo(this, 1, 65, textureX, textureY); // Box 59
		ammoModel[26] = new ModelRendererTurbo(this, 1, 70, textureX, textureY); // Box 60
		ammoModel[27] = new ModelRendererTurbo(this, 1, 65, textureX, textureY); // Box 61
		ammoModel[28] = new ModelRendererTurbo(this, 1, 70, textureX, textureY); // Box 62
		ammoModel[29] = new ModelRendererTurbo(this, 1, 70, textureX, textureY); // Box 63
		ammoModel[30] = new ModelRendererTurbo(this, 1, 70, textureX, textureY); // Box 64
		ammoModel[31] = new ModelRendererTurbo(this, 1, 65, textureX, textureY); // Box 65
		ammoModel[32] = new ModelRendererTurbo(this, 1, 65, textureX, textureY); // Box 66
		ammoModel[33] = new ModelRendererTurbo(this, 1, 70, textureX, textureY); // Box 67
		ammoModel[34] = new ModelRendererTurbo(this, 1, 70, textureX, textureY); // Box 68
		ammoModel[35] = new ModelRendererTurbo(this, 1, 70, textureX, textureY); // Box 69
		ammoModel[36] = new ModelRendererTurbo(this, 1, 65, textureX, textureY); // Box 70
		ammoModel[37] = new ModelRendererTurbo(this, 1, 65, textureX, textureY); // Box 71
		ammoModel[38] = new ModelRendererTurbo(this, 1, 65, textureX, textureY); // Box 72
		ammoModel[39] = new ModelRendererTurbo(this, 1, 65, textureX, textureY); // Box 73
		ammoModel[40] = new ModelRendererTurbo(this, 1, 65, textureX, textureY); // Box 74
		ammoModel[41] = new ModelRendererTurbo(this, 1, 65, textureX, textureY); // Box 75
		ammoModel[42] = new ModelRendererTurbo(this, 1, 70, textureX, textureY); // Box 76
		ammoModel[43] = new ModelRendererTurbo(this, 1, 70, textureX, textureY); // Box 77
		ammoModel[44] = new ModelRendererTurbo(this, 1, 70, textureX, textureY); // Box 78
		ammoModel[45] = new ModelRendererTurbo(this, 38, 66, textureX, textureY); // Box 84
		ammoModel[46] = new ModelRendererTurbo(this, 38, 66, textureX, textureY); // Box 85

		ammoModel[0].addBox(0F, 0F, 0F, 14, 12, 16, 0F); // ammo4
		ammoModel[0].setRotationPoint(15F, -8F, 2.5F);

		ammoModel[1].addShapeBox(0F, 0F, 0F, 14, 2, 16, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 3
		ammoModel[1].setRotationPoint(15F, -10F, 2.5F);

		ammoModel[2].addBox(0F, 0F, 0F, 14, 1, 3, 0F); // Box 4
		ammoModel[2].setRotationPoint(15F, -11F, 12F);

		ammoModel[3].addBox(0F, 0F, 0F, 14, 12, 16, 0F); // Box 0
		ammoModel[3].setRotationPoint(15F, -8F, -18.5F);

		ammoModel[4].addShapeBox(0F, 0F, 0F, 14, 2, 16, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 1
		ammoModel[4].setRotationPoint(15F, -10F, -18.5F);

		ammoModel[5].addBox(0F, 0F, 0F, 14, 9, 5, 0F); // Box 2
		ammoModel[5].setRotationPoint(15F, -5F, -2.5F);

		ammoModel[6].addBox(0F, 0F, 0F, 14, 1, 3, 0F); // Box 37
		ammoModel[6].setRotationPoint(15F, -11F, -15F);

		ammoModel[7].addShapeBox(0F, 0F, 0F, 3, 1, 3, 0F, 0F, 0F, -1F, 0F, -0.75F, -1.25F, 0F, -0.75F, -1.25F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0.25F, -0.75F, 0F, 0.25F, -0.75F, 0F, 0F, 0F); // Box 85
		ammoModel[7].setRotationPoint(25F, -18.5F, 5F);

		ammoModel[8].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 86
		ammoModel[8].setRotationPoint(16F, -18.5F, 5F);

		ammoModel[9].addShapeBox(0F, 0F, 0F, 3, 1, 3, 0F, 0F, 0F, 0F, 0F, -0.25F, -0.75F, 0F, -0.25F, -0.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.25F, -0.75F, 0F, -0.25F, -0.75F, 0F, 0F, 0F); // Box 87
		ammoModel[9].setRotationPoint(25F, -17.5F, 5F);

		ammoModel[10].addBox(0F, 0F, 0F, 9, 1, 3, 0F); // Box 88
		ammoModel[10].setRotationPoint(16F, -17.5F, 5F);

		ammoModel[11].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 89
		ammoModel[11].setRotationPoint(16F, -16.5F, 5F);

		ammoModel[12].addShapeBox(0F, 0F, 0F, 3, 1, 3, 0F, 0F, 0F, 0F, 0F, 0.25F, -0.75F, 0F, 0.25F, -0.75F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -0.75F, -1.25F, 0F, -0.75F, -1.25F, 0F, 0F, -1F); // Box 90
		ammoModel[12].setRotationPoint(25F, -16.5F, 5F);

		ammoModel[13].addShapeBox(0F, 0F, 0F, 14, 1, 16, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 47
		ammoModel[13].setRotationPoint(15F, 4F, 2.5F);

		ammoModel[14].addShapeBox(0F, 0F, 0F, 14, 1, 16, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 48
		ammoModel[14].setRotationPoint(15F, 4F, -18.5F);

		ammoModel[15].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 49
		ammoModel[15].setRotationPoint(16F, -14.5F, 7F);

		ammoModel[16].addShapeBox(0F, 0F, 0F, 3, 1, 3, 0F, 0F, 0F, 0F, 0F, 0.25F, -0.75F, 0F, 0.25F, -0.75F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -0.75F, -1.25F, 0F, -0.75F, -1.25F, 0F, 0F, -1F); // Box 50
		ammoModel[16].setRotationPoint(25F, -14.5F, 7F);

		ammoModel[17].addShapeBox(0F, 0F, 0F, 3, 1, 3, 0F, 0F, 0F, 0F, 0F, -0.25F, -0.75F, 0F, -0.25F, -0.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.25F, -0.75F, 0F, -0.25F, -0.75F, 0F, 0F, 0F); // Box 51
		ammoModel[17].setRotationPoint(25F, -15.5F, 7F);

		ammoModel[18].addBox(0F, 0F, 0F, 9, 1, 3, 0F); // Box 52
		ammoModel[18].setRotationPoint(16F, -15.5F, 7F);

		ammoModel[19].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 53
		ammoModel[19].setRotationPoint(16F, -16.5F, 7F);

		ammoModel[20].addShapeBox(0F, 0F, 0F, 3, 1, 3, 0F, 0F, 0F, -1F, 0F, -0.75F, -1.25F, 0F, -0.75F, -1.25F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0.25F, -0.75F, 0F, 0.25F, -0.75F, 0F, 0F, 0F); // Box 54
		ammoModel[20].setRotationPoint(25F, -16.5F, 7F);

		ammoModel[21].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 55
		ammoModel[21].setRotationPoint(16F, -11.5F, 7F);

		ammoModel[22].addShapeBox(0F, 0F, 0F, 3, 1, 3, 0F, 0F, 0F, 0F, 0F, 0.25F, -0.75F, 0F, 0.25F, -0.75F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -0.75F, -1.25F, 0F, -0.75F, -1.25F, 0F, 0F, -1F); // Box 56
		ammoModel[22].setRotationPoint(25F, -11.5F, 7F);

		ammoModel[23].addShapeBox(0F, 0F, 0F, 3, 1, 3, 0F, 0F, 0F, 0F, 0F, -0.25F, -0.75F, 0F, -0.25F, -0.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.25F, -0.75F, 0F, -0.25F, -0.75F, 0F, 0F, 0F); // Box 57
		ammoModel[23].setRotationPoint(25F, -12.5F, 7F);

		ammoModel[24].addBox(0F, 0F, 0F, 9, 1, 3, 0F); // Box 58
		ammoModel[24].setRotationPoint(16F, -12.5F, 7F);

		ammoModel[25].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 59
		ammoModel[25].setRotationPoint(16F, -13.5F, 7F);

		ammoModel[26].addShapeBox(0F, 0F, 0F, 3, 1, 3, 0F, 0F, 0F, -1F, 0F, -0.75F, -1.25F, 0F, -0.75F, -1.25F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0.25F, -0.75F, 0F, 0.25F, -0.75F, 0F, 0F, 0F); // Box 60
		ammoModel[26].setRotationPoint(25F, -13.5F, 7F);

		ammoModel[27].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 61
		ammoModel[27].setRotationPoint(16F, -11.5F, -10F);

		ammoModel[28].addShapeBox(0F, 0F, 0F, 3, 1, 3, 0F, 0F, 0F, 0F, 0F, 0.25F, -0.75F, 0F, 0.25F, -0.75F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -0.75F, -1.25F, 0F, -0.75F, -1.25F, 0F, 0F, -1F); // Box 62
		ammoModel[28].setRotationPoint(25F, -11.5F, -10F);

		ammoModel[29].addShapeBox(0F, 0F, 0F, 3, 1, 3, 0F, 0F, 0F, 0F, 0F, -0.25F, -0.75F, 0F, -0.25F, -0.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.25F, -0.75F, 0F, -0.25F, -0.75F, 0F, 0F, 0F); // Box 63
		ammoModel[29].setRotationPoint(25F, -12.5F, -10F);

		ammoModel[30].addShapeBox(0F, 0F, 0F, 3, 1, 3, 0F, 0F, 0F, -1F, 0F, -0.75F, -1.25F, 0F, -0.75F, -1.25F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0.25F, -0.75F, 0F, 0.25F, -0.75F, 0F, 0F, 0F); // Box 64
		ammoModel[30].setRotationPoint(25F, -13.5F, -10F);

		ammoModel[31].addBox(0F, 0F, 0F, 9, 1, 3, 0F); // Box 65
		ammoModel[31].setRotationPoint(16F, -12.5F, -10F);

		ammoModel[32].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 66
		ammoModel[32].setRotationPoint(16F, -13.5F, -10F);

		ammoModel[33].addShapeBox(0F, 0F, 0F, 3, 1, 3, 0F, 0F, 0F, 0F, 0F, 0.25F, -0.75F, 0F, 0.25F, -0.75F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -0.75F, -1.25F, 0F, -0.75F, -1.25F, 0F, 0F, -1F); // Box 67
		ammoModel[33].setRotationPoint(25F, -14.5F, -10F);

		ammoModel[34].addShapeBox(0F, 0F, 0F, 3, 1, 3, 0F, 0F, 0F, 0F, 0F, -0.25F, -0.75F, 0F, -0.25F, -0.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.25F, -0.75F, 0F, -0.25F, -0.75F, 0F, 0F, 0F); // Box 68
		ammoModel[34].setRotationPoint(25F, -15.5F, -10F);

		ammoModel[35].addShapeBox(0F, 0F, 0F, 3, 1, 3, 0F, 0F, 0F, -1F, 0F, -0.75F, -1.25F, 0F, -0.75F, -1.25F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0.25F, -0.75F, 0F, 0.25F, -0.75F, 0F, 0F, 0F); // Box 69
		ammoModel[35].setRotationPoint(25F, -16.5F, -10F);

		ammoModel[36].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 70
		ammoModel[36].setRotationPoint(16F, -16.5F, -10F);

		ammoModel[37].addBox(0F, 0F, 0F, 9, 1, 3, 0F); // Box 71
		ammoModel[37].setRotationPoint(16F, -15.5F, -10F);

		ammoModel[38].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 72
		ammoModel[38].setRotationPoint(16F, -14.5F, -10F);

		ammoModel[39].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 73
		ammoModel[39].setRotationPoint(16F, -18.5F, -8F);

		ammoModel[40].addBox(0F, 0F, 0F, 9, 1, 3, 0F); // Box 74
		ammoModel[40].setRotationPoint(16F, -17.5F, -8F);

		ammoModel[41].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 75
		ammoModel[41].setRotationPoint(16F, -16.5F, -8F);

		ammoModel[42].addShapeBox(0F, 0F, 0F, 3, 1, 3, 0F, 0F, 0F, 0F, 0F, 0.25F, -0.75F, 0F, 0.25F, -0.75F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -0.75F, -1.25F, 0F, -0.75F, -1.25F, 0F, 0F, -1F); // Box 76
		ammoModel[42].setRotationPoint(25F, -16.5F, -8F);

		ammoModel[43].addShapeBox(0F, 0F, 0F, 3, 1, 3, 0F, 0F, 0F, 0F, 0F, -0.25F, -0.75F, 0F, -0.25F, -0.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.25F, -0.75F, 0F, -0.25F, -0.75F, 0F, 0F, 0F); // Box 77
		ammoModel[43].setRotationPoint(25F, -17.5F, -8F);

		ammoModel[44].addShapeBox(0F, 0F, 0F, 3, 1, 3, 0F, 0F, 0F, -1F, 0F, -0.75F, -1.25F, 0F, -0.75F, -1.25F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0.25F, -0.75F, 0F, 0.25F, -0.75F, 0F, 0F, 0F); // Box 78
		ammoModel[44].setRotationPoint(25F, -18.5F, -8F);

		ammoModel[45].addBox(0F, 0F, 0F, 13, 1, 4, 0F); // Box 84
		ammoModel[45].setRotationPoint(15.5F, -11F, 6.5F);

		ammoModel[46].addBox(0F, 0F, 0F, 13, 1, 4, 0F); // Box 85
		ammoModel[46].setRotationPoint(15.5F, -11F, -10.5F);


		slideModel = new ModelRendererTurbo[3];
		slideModel[0] = new ModelRendererTurbo(this, 75, 53, textureX, textureY); // Box 100
		slideModel[1] = new ModelRendererTurbo(this, 75, 61, textureX, textureY); // Box 101
		slideModel[2] = new ModelRendererTurbo(this, 38, 72, textureX, textureY); // Box 102

		slideModel[0].addBox(0F, 0F, 0F, 3, 3, 4, 0F); // Box 100
		slideModel[0].setRotationPoint(27F, -24F, -2F);

		slideModel[1].addShapeBox(0F, 0F, 0F, 3, 1, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 101
		slideModel[1].setRotationPoint(27F, -25F, -2F);

		slideModel[2].addShapeBox(0F, 0F, 0F, 12, 1, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 102
		slideModel[2].setRotationPoint(15F, -22F, -2F);


		breakActionModel = new ModelRendererTurbo[21];
		breakActionModel[0] = new ModelRendererTurbo(this, 38, 53, textureX, textureY); // Box 1
		breakActionModel[1] = new ModelRendererTurbo(this, 38, 60, textureX, textureY); // Box 13
		breakActionModel[2] = new ModelRendererTurbo(this, 45, 34, textureX, textureY); // Box 17
		breakActionModel[3] = new ModelRendererTurbo(this, 45, 27, textureX, textureY); // Box 18
		breakActionModel[4] = new ModelRendererTurbo(this, 38, 27, textureX, textureY); // Box 19
		breakActionModel[5] = new ModelRendererTurbo(this, 38, 27, textureX, textureY); // Box 21
		breakActionModel[6] = new ModelRendererTurbo(this, 38, 27, textureX, textureY); // Box 22
		breakActionModel[7] = new ModelRendererTurbo(this, 38, 27, textureX, textureY); // Box 23
		breakActionModel[8] = new ModelRendererTurbo(this, 45, 27, textureX, textureY); // Box 26
		breakActionModel[9] = new ModelRendererTurbo(this, 45, 34, textureX, textureY); // Box 27
		breakActionModel[10] = new ModelRendererTurbo(this, 38, 27, textureX, textureY); // Box 28
		breakActionModel[11] = new ModelRendererTurbo(this, 38, 27, textureX, textureY); // Box 29
		breakActionModel[12] = new ModelRendererTurbo(this, 38, 27, textureX, textureY); // Box 30
		breakActionModel[13] = new ModelRendererTurbo(this, 38, 27, textureX, textureY); // Box 31
		breakActionModel[14] = new ModelRendererTurbo(this, 38, 53, textureX, textureY); // Box 32
		breakActionModel[15] = new ModelRendererTurbo(this, 38, 60, textureX, textureY); // Box 33
		breakActionModel[16] = new ModelRendererTurbo(this, 1, 59, textureX, textureY); // Box 38
		breakActionModel[17] = new ModelRendererTurbo(this, 1, 59, textureX, textureY); // Box 39
		breakActionModel[18] = new ModelRendererTurbo(this, 38, 27, textureX, textureY); // Box 40
		breakActionModel[19] = new ModelRendererTurbo(this, 1, 75, textureX, textureY); // Box 79
		breakActionModel[20] = new ModelRendererTurbo(this, 1, 75, textureX, textureY); // Box 80

		breakActionModel[0].addShapeBox(-16F, 0F, 11.5F, 14, 2, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 1
		breakActionModel[0].setRotationPoint(31F, -13F, 0F);

		breakActionModel[1].addBox(-16F, 2F, 11.5F, 14, 1, 4, 0F); // Box 13
		breakActionModel[1].setRotationPoint(31F, -13F, 0F);

		breakActionModel[2].addBox(-19F, 1F, 11F, 3, 2, 5, 0F); // Box 17
		breakActionModel[2].setRotationPoint(31F, -13F, 0F);

		breakActionModel[3].addShapeBox(-19F, 0F, 11F, 3, 1, 5, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 18
		breakActionModel[3].setRotationPoint(31F, -13F, 0F);

		breakActionModel[4].addShapeBox(-18F, 3F, 11F, 1, 11, 2, 0F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F); // Box 19
		breakActionModel[4].setRotationPoint(31F, -13F, 0F);

		breakActionModel[5].addShapeBox(-18F, 3F, 14F, 1, 11, 2, 0F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F); // Box 21
		breakActionModel[5].setRotationPoint(31F, -13F, 0F);

		breakActionModel[6].addShapeBox(-17F, 3F, 11F, 1, 11, 2, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F); // Box 22
		breakActionModel[6].setRotationPoint(31F, -13F, 0F);

		breakActionModel[7].addShapeBox(-17F, 3F, 14F, 1, 11, 2, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F); // Box 23
		breakActionModel[7].setRotationPoint(31F, -13F, 0F);

		breakActionModel[8].addShapeBox(-19F, 0F, -16F, 3, 1, 5, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 26
		breakActionModel[8].setRotationPoint(31F, -13F, 0F);

		breakActionModel[9].addBox(-19F, 1F, -16F, 3, 2, 5, 0F); // Box 27
		breakActionModel[9].setRotationPoint(31F, -13F, 0F);

		breakActionModel[10].addShapeBox(-17F, 3F, -13F, 1, 11, 2, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F); // Box 28
		breakActionModel[10].setRotationPoint(31F, -13F, 0F);

		breakActionModel[11].addShapeBox(-17F, 3F, -16F, 1, 11, 2, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F); // Box 29
		breakActionModel[11].setRotationPoint(31F, -13F, 0F);

		breakActionModel[12].addShapeBox(-18F, 3F, -13F, 1, 11, 2, 0F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F); // Box 30
		breakActionModel[12].setRotationPoint(31F, -13F, 0F);

		breakActionModel[13].addShapeBox(-18F, 3F, -16F, 1, 11, 2, 0F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F); // Box 31
		breakActionModel[13].setRotationPoint(31F, -13F, 0F);

		breakActionModel[14].addShapeBox(-16F, 0F, -15.5F, 14, 2, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 32
		breakActionModel[14].setRotationPoint(31F, -13F, 0F);

		breakActionModel[15].addBox(-16F, 2F, -15.5F, 14, 1, 4, 0F); // Box 33
		breakActionModel[15].setRotationPoint(31F, -13F, 0F);

		breakActionModel[16].addShapeBox(-17F, 14F, -16F, 1, 2, 32, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F); // Box 38
		breakActionModel[16].setRotationPoint(31F, -13F, 0F);

		breakActionModel[17].addShapeBox(-18F, 14F, -16F, 1, 2, 32, 0F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F); // Box 39
		breakActionModel[17].setRotationPoint(31F, -13F, 0F);

		breakActionModel[18].addShapeBox(-18F, 13F, -12F, 2, 1, 24, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 40
		breakActionModel[18].setRotationPoint(31F, -13F, 0F);

		breakActionModel[19].addShapeBox(-0.5F, -9F, 10F, 1, 8, 2, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F); // Box 79
		breakActionModel[19].setRotationPoint(31F, -13F, 0F);
		breakActionModel[19].rotateAngleZ = 0.34906585F;

		breakActionModel[20].addShapeBox(-1.5F, -9F, 10F, 1, 8, 2, 0F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F); // Box 80
		breakActionModel[20].setRotationPoint(31F, -13F, 0F);
		breakActionModel[20].rotateAngleZ = 0.34906585F;

		stockAttachPoint = new Vector3f(-12F /16F, 12F /16F, 0F /16F);

		gunSlideDistance = 0.8F;
		animationType = EnumAnimationType.CUSTOM;

		/* ----Start of Reload Block---- */
		/* Reload Name: Amended Side Clip */
		rotateClipHorizontal = 0F;
		rotateClipVertical = 20F;
		translateClip = new Vector3f(0F, -4F, 0F);
		/* ----End of Reload Block---- */

		barrelBreakPoint = new Vector3f(30.5 /16F, 13F /16F, 0F /16F);
		breakAngle = 20F;

		translateAll(0F, 0F, 0F);


		flipAll();
	}
}