package com.flansmod.client.model.dwfirearms; //Path where the model is located

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelSFOrel extends ModelGun //Same as Filename
{
	int textureX = 512;
	int textureY = 256;

	public ModelSFOrel() //Same as Filename
	{
		gunModel = new ModelRendererTurbo[54];
		gunModel[0] = new ModelRendererTurbo(this, 72, 32, textureX, textureY); // scope4
		gunModel[1] = new ModelRendererTurbo(this, 72, 53, textureX, textureY); // scope4
		gunModel[2] = new ModelRendererTurbo(this, 99, 143, textureX, textureY); // scope4
		gunModel[3] = new ModelRendererTurbo(this, 99, 149, textureX, textureY); // scope4
		gunModel[4] = new ModelRendererTurbo(this, 72, 88, textureX, textureY); // scope4
		gunModel[5] = new ModelRendererTurbo(this, 72, 103, textureX, textureY); // scope4
		gunModel[6] = new ModelRendererTurbo(this, 72, 118, textureX, textureY); // scope4
		gunModel[7] = new ModelRendererTurbo(this, 72, 129, textureX, textureY); // scope4
		gunModel[8] = new ModelRendererTurbo(this, 72, 141, textureX, textureY); // scope4
		gunModel[9] = new ModelRendererTurbo(this, 112, 146, textureX, textureY); // scope4
		gunModel[10] = new ModelRendererTurbo(this, 72, 75, textureX, textureY); // scope4
		gunModel[11] = new ModelRendererTurbo(this, 72, 64, textureX, textureY); // scope4
		gunModel[12] = new ModelRendererTurbo(this, 185, 2, textureX, textureY); // scope4
		gunModel[13] = new ModelRendererTurbo(this, 185, 2, textureX, textureY); // scope4
		gunModel[14] = new ModelRendererTurbo(this, 185, 2, textureX, textureY); // scope4
		gunModel[15] = new ModelRendererTurbo(this, 210, 16, textureX, textureY); // scope4
		gunModel[16] = new ModelRendererTurbo(this, 185, 11, textureX, textureY); // scope4
		gunModel[17] = new ModelRendererTurbo(this, 185, 21, textureX, textureY); // scope4
		gunModel[18] = new ModelRendererTurbo(this, 185, 11, textureX, textureY); // scope4
		gunModel[19] = new ModelRendererTurbo(this, 1, 67, textureX, textureY); // scope4
		gunModel[20] = new ModelRendererTurbo(this, 1, 46, textureX, textureY); // scope4
		gunModel[21] = new ModelRendererTurbo(this, 1, 79, textureX, textureY); // scope4
		gunModel[22] = new ModelRendererTurbo(this, 144, 106, textureX, textureY); // scope4
		gunModel[23] = new ModelRendererTurbo(this, 144, 88, textureX, textureY); // scope4
		gunModel[24] = new ModelRendererTurbo(this, 144, 119, textureX, textureY); // scope4
		gunModel[25] = new ModelRendererTurbo(this, 144, 119, textureX, textureY); // scope4
		gunModel[26] = new ModelRendererTurbo(this, 181, 123, textureX, textureY); // scope4
		gunModel[27] = new ModelRendererTurbo(this, 181, 123, textureX, textureY); // scope4
		gunModel[28] = new ModelRendererTurbo(this, 1, 28, textureX, textureY); // scope4
		gunModel[29] = new ModelRendererTurbo(this, 1, 16, textureX, textureY); // scope4
		gunModel[30] = new ModelRendererTurbo(this, 72, 20, textureX, textureY); // scope4
		gunModel[31] = new ModelRendererTurbo(this, 72, 11, textureX, textureY); // scope4
		gunModel[32] = new ModelRendererTurbo(this, 72, 2, textureX, textureY); // scope4
		gunModel[33] = new ModelRendererTurbo(this, 184, 43, textureX, textureY); // scope4
		gunModel[34] = new ModelRendererTurbo(this, 184, 43, textureX, textureY); // scope4
		gunModel[35] = new ModelRendererTurbo(this, 184, 32, textureX, textureY); // scope4
		gunModel[36] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // scope4
		gunModel[37] = new ModelRendererTurbo(this, 1, 79, textureX, textureY); // scope4
		gunModel[38] = new ModelRendererTurbo(this, 144, 135, textureX, textureY); // scope4
		gunModel[39] = new ModelRendererTurbo(this, 144, 135, textureX, textureY); // scope4
		gunModel[40] = new ModelRendererTurbo(this, 144, 135, textureX, textureY); // scope4
		gunModel[41] = new ModelRendererTurbo(this, 144, 135, textureX, textureY); // scope4
		gunModel[42] = new ModelRendererTurbo(this, 179, 135, textureX, textureY); // scope4
		gunModel[43] = new ModelRendererTurbo(this, 1, 91, textureX, textureY); // scope4
		gunModel[44] = new ModelRendererTurbo(this, 221, 21, textureX, textureY); // scope4
		gunModel[45] = new ModelRendererTurbo(this, 221, 26, textureX, textureY); // scope4
		gunModel[46] = new ModelRendererTurbo(this, 210, 23, textureX, textureY); // scope4
		gunModel[47] = new ModelRendererTurbo(this, 210, 23, textureX, textureY); // scope4
		gunModel[48] = new ModelRendererTurbo(this, 1, 79, textureX, textureY); // scope4
		gunModel[49] = new ModelRendererTurbo(this, 1, 35, textureX, textureY); // scope4
		gunModel[50] = new ModelRendererTurbo(this, 51, 32, textureX, textureY); // scope4
		gunModel[51] = new ModelRendererTurbo(this, 51, 32, textureX, textureY); // scope4
		gunModel[52] = new ModelRendererTurbo(this, 51, 32, textureX, textureY); // scope4
		gunModel[53] = new ModelRendererTurbo(this, 51, 32, textureX, textureY); // scope4

		gunModel[0].addBox(0F, 0F, 0F, 25, 10, 10, 0F); // scope4
		gunModel[0].setRotationPoint(-10F, -15.5F, -5F);

		gunModel[1].addShapeBox(0F, 0F, 0F, 40, 2, 8, 0F, -1F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, -1F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope4
		gunModel[1].setRotationPoint(-8F, -21.5F, -4F);

		gunModel[2].addShapeBox(0F, 0F, 0F, 3, 2, 3, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope4
		gunModel[2].setRotationPoint(-9F, -20.5F, -1.5F);

		gunModel[3].addBox(0F, 0F, 0F, 12, 3, 1, 0F); // scope4
		gunModel[3].setRotationPoint(14F, -14.5F, 4.5F);

		gunModel[4].addBox(0F, 0F, 0F, 20, 5, 9, 0F); // scope4
		gunModel[4].setRotationPoint(15F, -10.5F, -4.5F);

		gunModel[5].addBox(0F, 0F, 0F, 20, 4, 10, 0F); // scope4
		gunModel[5].setRotationPoint(15F, -14.5F, -5F);

		gunModel[6].addBox(0F, 0F, 0F, 20, 1, 9, 0F); // scope4
		gunModel[6].setRotationPoint(15F, -15.5F, -4F);

		gunModel[7].addShapeBox(0F, 0F, 0F, 14, 2, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope4
		gunModel[7].setRotationPoint(15F, -17.5F, -4F);

		gunModel[8].addShapeBox(0F, 0F, 0F, 3, 2, 10, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope4
		gunModel[8].setRotationPoint(29F, -17.5F, -5F);

		gunModel[9].addBox(0F, 0F, 0F, 6, 1, 1, 0F); // scope4
		gunModel[9].setRotationPoint(29F, -15.5F, -5F);

		gunModel[10].addShapeBox(0F, 0F, 0F, 25, 2, 10, 0F, -1F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, -1F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope4
		gunModel[10].setRotationPoint(-10F, -17.5F, -5F);

		gunModel[11].addShapeBox(0F, 0F, 0F, 41, 2, 8, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope4
		gunModel[11].setRotationPoint(-9F, -19.5F, -4F);

		gunModel[12].addShapeBox(0F, 0F, 0F, 12, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope4
		gunModel[12].setRotationPoint(58F, -20.5F, -3F);

		gunModel[13].addBox(0F, 0F, 0F, 12, 2, 6, 0F); // scope4
		gunModel[13].setRotationPoint(58F, -18.5F, -3F);

		gunModel[14].addShapeBox(0F, 0F, 0F, 12, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // scope4
		gunModel[14].setRotationPoint(58F, -16.5F, -3F);

		gunModel[15].addBox(0F, 0F, 0F, 5, 1, 3, 0F); // scope4
		gunModel[15].setRotationPoint(65F, -14.5F, -1.5F);

		gunModel[16].addShapeBox(0F, 0F, 0F, 5, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope4
		gunModel[16].setRotationPoint(65F, -13.5F, -3.5F);

		gunModel[17].addBox(0F, 0F, 0F, 5, 3, 7, 0F); // scope4
		gunModel[17].setRotationPoint(65F, -11.5F, -3.5F);

		gunModel[18].addShapeBox(0F, 0F, 0F, 5, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // scope4
		gunModel[18].setRotationPoint(65F, -8.5F, -3.5F);

		gunModel[19].addShapeBox(0F, 0F, 0F, 12, 2, 9, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F); // scope4
		gunModel[19].setRotationPoint(-9F, -4.5F, -4.5F);

		gunModel[20].addShapeBox(0F, 0F, 0F, 11, 11, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, -2F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 4F, -2F, 0F); // scope4
		gunModel[20].setRotationPoint(-8F, -0.5F, -4.5F);

		gunModel[21].addShapeBox(0F, 0F, 0F, 11, 2, 9, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, -2F, 0F); // scope4
		gunModel[21].setRotationPoint(-12F, 10.5F, -4.5F);

		gunModel[22].addBox(0F, 0F, 0F, 14, 5, 7, 0F); // scope4
		gunModel[22].setRotationPoint(44F, -20.5F, -3.5F);

		gunModel[23].addBox(0F, 0F, 0F, 23, 10, 7, 0F); // scope4
		gunModel[23].setRotationPoint(35F, -15.5F, -3.5F);

		gunModel[24].addShapeBox(0F, 0F, 0F, 14, 13, 2, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope4
		gunModel[24].setRotationPoint(44F, -19.5F, -5.5F);

		gunModel[25].addShapeBox(0F, 0F, 0F, 14, 13, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F); // scope4
		gunModel[25].setRotationPoint(44F, -19.5F, 3.5F);

		gunModel[26].addShapeBox(0F, 0F, 0F, 9, 9, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F); // scope4
		gunModel[26].setRotationPoint(35F, -15.5F, 3.5F);

		gunModel[27].addShapeBox(0F, 0F, 0F, 9, 9, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope4
		gunModel[27].setRotationPoint(35F, -15.5F, -5.5F);

		gunModel[28].addBox(0F, 0F, 0F, 9, 1, 5, 0F); // scope4
		gunModel[28].setRotationPoint(32F, -23.5F, -2.5F);

		gunModel[29].addShapeBox(0F, 0F, 0F, 12, 2, 9, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope4
		gunModel[29].setRotationPoint(32F, -22.5F, -4.5F);

		gunModel[30].addShapeBox(0F, 0F, 0F, 50, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // scope4
		gunModel[30].setRotationPoint(58F, -9F, -3F);

		gunModel[31].addBox(0F, 0F, 0F, 50, 2, 6, 0F); // scope4
		gunModel[31].setRotationPoint(58F, -11F, -3F);

		gunModel[32].addShapeBox(0F, 0F, 0F, 50, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope4
		gunModel[32].setRotationPoint(58F, -13F, -3F);

		gunModel[33].addShapeBox(0F, 0F, 0F, 8, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope4
		gunModel[33].setRotationPoint(92F, -13.5F, -3.5F);

		gunModel[34].addShapeBox(0F, 0F, 0F, 8, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // scope4
		gunModel[34].setRotationPoint(92F, -8.5F, -3.5F);

		gunModel[35].addBox(0F, 0F, 0F, 8, 3, 7, 0F); // scope4
		gunModel[35].setRotationPoint(92F, -11.5F, -3.5F);

		gunModel[36].addBox(0F, 0F, 0F, 12, 5, 9, 0F); // scope4
		gunModel[36].setRotationPoint(32F, -20.5F, -4.5F);

		gunModel[37].addShapeBox(0F, 0F, 0F, 11, 2, 9, 0F, 0F, 2F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, -2F, 0F); // scope4
		gunModel[37].setRotationPoint(-12F, 12.5F, -4.5F);

		gunModel[38].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // scope4
		gunModel[38].setRotationPoint(36F, -5.5F, -3.5F);

		gunModel[39].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // scope4
		gunModel[39].setRotationPoint(42F, -5.5F, -3.5F);

		gunModel[40].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // scope4
		gunModel[40].setRotationPoint(48F, -5.5F, -3.5F);

		gunModel[41].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // scope4
		gunModel[41].setRotationPoint(54F, -5.5F, -3.5F);

		gunModel[42].addBox(0F, 0F, 0F, 3, 5, 9, 0F); // scope4
		gunModel[42].setRotationPoint(12F, -5.5F, -4.5F);

		gunModel[43].addBox(0F, 0F, 0F, 12, 3, 8, 0F); // scope4
		gunModel[43].setRotationPoint(-8F, -5.5F, -4F);

		gunModel[44].addShapeBox(0F, 0F, 0F, 4, 1, 3, 0F, 0F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope4
		gunModel[44].setRotationPoint(92F, -21.5F, -1.5F);

		gunModel[45].addShapeBox(0F, 0F, 0F, 4, 2, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F); // scope4
		gunModel[45].setRotationPoint(92F, -20.5F, -1.5F);

		gunModel[46].addBox(0F, 0F, 0F, 2, 5, 3, 0F); // scope4
		gunModel[46].setRotationPoint(92F, -18.5F, -1.5F);

		gunModel[47].addShapeBox(0F, 0F, 0F, 2, 5, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, 0F); // scope4
		gunModel[47].setRotationPoint(95F, -18.5F, -1.5F);

		gunModel[48].addBox(0F, 0F, 0F, 11, 2, 9, 0F); // scope4
		gunModel[48].setRotationPoint(-8F, -2.5F, -4.5F);

		gunModel[49].addBox(0F, 0F, 0F, 21, 3, 7, 0F); // scope4
		gunModel[49].setRotationPoint(4F, -22.5F, -3.5F);

		gunModel[50].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope4
		gunModel[50].setRotationPoint(4F, -24.5F, -3.5F);

		gunModel[51].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope4
		gunModel[51].setRotationPoint(10F, -24.5F, -3.5F);

		gunModel[52].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope4
		gunModel[52].setRotationPoint(16F, -24.5F, -3.5F);

		gunModel[53].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope4
		gunModel[53].setRotationPoint(22F, -24.5F, -3.5F);


		defaultScopeModel = new ModelRendererTurbo[30];
		defaultScopeModel[0] = new ModelRendererTurbo(this, 243, 22, textureX, textureY); // scope4
		defaultScopeModel[1] = new ModelRendererTurbo(this, 295, 46, textureX, textureY); // scope4
		defaultScopeModel[2] = new ModelRendererTurbo(this, 243, 35, textureX, textureY); // scope4
		defaultScopeModel[3] = new ModelRendererTurbo(this, 312, 36, textureX, textureY); // scope4
		defaultScopeModel[4] = new ModelRendererTurbo(this, 312, 36, textureX, textureY); // scope4
		defaultScopeModel[5] = new ModelRendererTurbo(this, 312, 26, textureX, textureY); // scope4
		defaultScopeModel[6] = new ModelRendererTurbo(this, 295, 39, textureX, textureY); // scope4
		defaultScopeModel[7] = new ModelRendererTurbo(this, 295, 39, textureX, textureY); // scope4
		defaultScopeModel[8] = new ModelRendererTurbo(this, 243, 35, textureX, textureY); // scope4
		defaultScopeModel[9] = new ModelRendererTurbo(this, 295, 54, textureX, textureY); // scope4
		defaultScopeModel[10] = new ModelRendererTurbo(this, 242, 52, textureX, textureY); // scope4
		defaultScopeModel[11] = new ModelRendererTurbo(this, 295, 54, textureX, textureY); // scope4
		defaultScopeModel[12] = new ModelRendererTurbo(this, 273, 109, textureX, textureY); // scope4
		defaultScopeModel[13] = new ModelRendererTurbo(this, 242, 108, textureX, textureY); // scope4
		defaultScopeModel[14] = new ModelRendererTurbo(this, 273, 109, textureX, textureY); // scope4
		defaultScopeModel[15] = new ModelRendererTurbo(this, 277, 87, textureX, textureY); // scope4
		defaultScopeModel[16] = new ModelRendererTurbo(this, 242, 86, textureX, textureY); // scope4
		defaultScopeModel[17] = new ModelRendererTurbo(this, 277, 87, textureX, textureY); // scope4
		defaultScopeModel[18] = new ModelRendererTurbo(this, 269, 98, textureX, textureY); // scope4
		defaultScopeModel[19] = new ModelRendererTurbo(this, 242, 97, textureX, textureY); // scope4
		defaultScopeModel[20] = new ModelRendererTurbo(this, 269, 98, textureX, textureY); // scope4
		defaultScopeModel[21] = new ModelRendererTurbo(this, 289, 71, textureX, textureY); // scope4
		defaultScopeModel[22] = new ModelRendererTurbo(this, 242, 69, textureX, textureY); // scope4
		defaultScopeModel[23] = new ModelRendererTurbo(this, 289, 71, textureX, textureY); // scope4
		defaultScopeModel[24] = new ModelRendererTurbo(this, 242, 119, textureX, textureY); // scope4
		defaultScopeModel[25] = new ModelRendererTurbo(this, 263, 119, textureX, textureY); // scope4
		defaultScopeModel[26] = new ModelRendererTurbo(this, 263, 119, textureX, textureY); // scope4
		defaultScopeModel[27] = new ModelRendererTurbo(this, 242, 119, textureX, textureY); // scope4
		defaultScopeModel[28] = new ModelRendererTurbo(this, 263, 119, textureX, textureY); // scope4
		defaultScopeModel[29] = new ModelRendererTurbo(this, 263, 119, textureX, textureY); // scope4

		defaultScopeModel[0].addBox(0F, 0F, 0F, 17, 3, 9, 0F); // scope4
		defaultScopeModel[0].setRotationPoint(6F, -25F, -4.5F);

		defaultScopeModel[1].addBox(0F, 0F, 0F, 11, 1, 6, 0F); // scope4
		defaultScopeModel[1].setRotationPoint(9F, -26F, -3F);

		defaultScopeModel[2].addShapeBox(0F, 0F, 0F, 13, 5, 11, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope4
		defaultScopeModel[2].setRotationPoint(8F, -36F, -5.5F);

		defaultScopeModel[3].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F); // scope4
		defaultScopeModel[3].setRotationPoint(11F, -37.5F, -3.5F);

		defaultScopeModel[4].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F); // scope4
		defaultScopeModel[4].setRotationPoint(16F, -37.5F, -3.5F);

		defaultScopeModel[5].addBox(0F, 0F, 0F, 3, 2, 7, 0F); // scope4
		defaultScopeModel[5].setRotationPoint(13F, -37.5F, -3.5F);

		defaultScopeModel[6].addShapeBox(0F, 0F, 0F, 4, 2, 4, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope4
		defaultScopeModel[6].setRotationPoint(12.5F, -33F, -8.5F);

		defaultScopeModel[7].addShapeBox(0F, 0F, 0F, 4, 2, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // scope4
		defaultScopeModel[7].setRotationPoint(12.5F, -31F, -8.5F);

		defaultScopeModel[8].addShapeBox(0F, 0F, 0F, 13, 5, 11, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // scope4
		defaultScopeModel[8].setRotationPoint(8F, -31F, -5.5F);

		defaultScopeModel[9].addShapeBox(0F, 0F, 0F, 15, 3, 11, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope4
		defaultScopeModel[9].setRotationPoint(39F, -36.5F, -5.5F);

		defaultScopeModel[10].addBox(0F, 0F, 0F, 15, 5, 11, 0F); // scope4
		defaultScopeModel[10].setRotationPoint(39F, -33.5F, -5.5F);

		defaultScopeModel[11].addShapeBox(0F, 0F, 0F, 15, 3, 11, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // scope4
		defaultScopeModel[11].setRotationPoint(39F, -28.5F, -5.5F);

		defaultScopeModel[12].addShapeBox(0F, 0F, 0F, 8, 2, 7, 0F, 0F, 0F, -2F, 0F, 2F, -1F, 0F, 2F, -1F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, -1F, 2F, 0F, -1F, 2F, 0F, 0F, 0F); // scope4
		defaultScopeModel[12].setRotationPoint(31F, -34.5F, -3.5F);

		defaultScopeModel[13].addShapeBox(0F, 0F, 0F, 8, 3, 7, 0F, 0F, 0F, 0F, 0F, 1F, 2F, 0F, 1F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 2F, 0F, 1F, 2F, 0F, 0F, 0F); // scope4
		defaultScopeModel[13].setRotationPoint(31F, -32.5F, -3.5F);

		defaultScopeModel[14].addShapeBox(0F, 0F, 0F, 8, 2, 7, 0F, 0F, 0F, 0F, 0F, -1F, 2F, 0F, -1F, 2F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 2F, -1F, 0F, 2F, -1F, 0F, 0F, -2F); // scope4
		defaultScopeModel[14].setRotationPoint(31F, -29.5F, -3.5F);

		defaultScopeModel[15].addShapeBox(0F, 0F, 0F, 10, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope4
		defaultScopeModel[15].setRotationPoint(21F, -34.5F, -3.5F);

		defaultScopeModel[16].addBox(0F, 0F, 0F, 10, 3, 7, 0F); // scope4
		defaultScopeModel[16].setRotationPoint(21F, -32.5F, -3.5F);

		defaultScopeModel[17].addShapeBox(0F, 0F, 0F, 10, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // scope4
		defaultScopeModel[17].setRotationPoint(21F, -29.5F, -3.5F);

		defaultScopeModel[18].addShapeBox(0F, 0F, 0F, 6, 2, 7, 0F, 0F, 2F, -1F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 2F, -1F, 0F, -1F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 2F); // scope4
		defaultScopeModel[18].setRotationPoint(2F, -34.5F, -3.5F);

		defaultScopeModel[19].addShapeBox(0F, 0F, 0F, 6, 3, 7, 0F, 0F, 1F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 2F, 0F, 1F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 2F); // scope4
		defaultScopeModel[19].setRotationPoint(2F, -32.5F, -3.5F);

		defaultScopeModel[20].addShapeBox(0F, 0F, 0F, 6, 2, 7, 0F, 0F, -1F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 2F, 0F, 2F, -1F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 2F, -1F); // scope4
		defaultScopeModel[20].setRotationPoint(2F, -29.5F, -3.5F);

		defaultScopeModel[21].addShapeBox(0F, 0F, 0F, 12, 3, 11, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // scope4
		defaultScopeModel[21].setRotationPoint(-10F, -28.5F, -5.5F);

		defaultScopeModel[22].addBox(0F, 0F, 0F, 12, 5, 11, 0F); // scope4
		defaultScopeModel[22].setRotationPoint(-10F, -33.5F, -5.5F);

		defaultScopeModel[23].addShapeBox(0F, 0F, 0F, 12, 3, 11, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope4
		defaultScopeModel[23].setRotationPoint(-10F, -36.5F, -5.5F);

		defaultScopeModel[24].addBox(0F, 0F, 0F, 1, 3, 9, 0F); // scope4
		defaultScopeModel[24].setRotationPoint(-10.2F, -32.5F, -4.5F);

		defaultScopeModel[25].addShapeBox(0F, 0F, 0F, 1, 3, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // scope4
		defaultScopeModel[25].setRotationPoint(-10.2F, -29.5F, -4.5F);

		defaultScopeModel[26].addShapeBox(0F, 0F, 0F, 1, 3, 9, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope4
		defaultScopeModel[26].setRotationPoint(-10.2F, -35.5F, -4.5F);

		defaultScopeModel[27].addBox(0F, 0F, 0F, 1, 3, 9, 0F); // scope4
		defaultScopeModel[27].setRotationPoint(53.2F, -32.5F, -4.5F);

		defaultScopeModel[28].addShapeBox(0F, 0F, 0F, 1, 3, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // scope4
		defaultScopeModel[28].setRotationPoint(53.2F, -29.5F, -4.5F);

		defaultScopeModel[29].addShapeBox(0F, 0F, 0F, 1, 3, 9, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope4
		defaultScopeModel[29].setRotationPoint(53.2F, -35.5F, -4.5F);


		defaultStockModel = new ModelRendererTurbo[16];
		defaultStockModel[0] = new ModelRendererTurbo(this, 72, 154, textureX, textureY); // scope4
		defaultStockModel[1] = new ModelRendererTurbo(this, 153, 154, textureX, textureY); // scope4
		defaultStockModel[2] = new ModelRendererTurbo(this, 72, 185, textureX, textureY); // scope4
		defaultStockModel[3] = new ModelRendererTurbo(this, 73, 175, textureX, textureY); // scope4
		defaultStockModel[4] = new ModelRendererTurbo(this, 154, 177, textureX, textureY); // scope4
		defaultStockModel[5] = new ModelRendererTurbo(this, 142, 187, textureX, textureY); // scope4
		defaultStockModel[6] = new ModelRendererTurbo(this, 1, 177, textureX, textureY); // scope4
		defaultStockModel[7] = new ModelRendererTurbo(this, 1, 164, textureX, textureY); // scope4
		defaultStockModel[8] = new ModelRendererTurbo(this, 1, 188, textureX, textureY); // scope4
		defaultStockModel[9] = new ModelRendererTurbo(this, 47, 211, textureX, textureY); // scope4
		defaultStockModel[10] = new ModelRendererTurbo(this, 47, 211, textureX, textureY); // scope4
		defaultStockModel[11] = new ModelRendererTurbo(this, 24, 188, textureX, textureY); // scope4
		defaultStockModel[12] = new ModelRendererTurbo(this, 47, 188, textureX, textureY); // scope4
		defaultStockModel[13] = new ModelRendererTurbo(this, 24, 210, textureX, textureY); // scope4
		defaultStockModel[14] = new ModelRendererTurbo(this, 1, 208, textureX, textureY); // scope4
		defaultStockModel[15] = new ModelRendererTurbo(this, 47, 211, textureX, textureY); // scope4

		defaultStockModel[0].addShapeBox(0F, 0F, 0F, 33, 13, 7, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, -8F, 0F, 0F, -8F, 0F, 0F, 2F, 0F); // scope4
		defaultStockModel[0].setRotationPoint(-43F, -12.5F, -3.5F);

		defaultStockModel[1].addBox(0F, 0F, 0F, 2, 15, 7, 0F); // scope4
		defaultStockModel[1].setRotationPoint(-45F, -12.5F, -3.5F);

		defaultStockModel[2].addShapeBox(0F, 0F, 0F, 27, 2, 7, 0F, -1F, 0F, -1.5F, 0F, 0.8F, -1.5F, 0F, 0.8F, -1.5F, -1F, 0F, -1.5F, 0F, 0F, 0F, 0F, -0.8F, 0F, 0F, -0.8F, 0F, 0F, 0F, 0F); // scope4
		defaultStockModel[2].setRotationPoint(-45F, -14.5F, -3.5F);

		defaultStockModel[3].addShapeBox(0F, 0F, 0F, 33, 2, 7, 0F, 0F, -10F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -10F, 0F, 0F, 10F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 10F, -1.5F); // scope4
		defaultStockModel[3].setRotationPoint(-43F, -7.5F, -3.5F);

		defaultStockModel[4].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, -1F, 0F, -1.5F); // scope4
		defaultStockModel[4].setRotationPoint(-45F, 2.5F, -3.5F);

		defaultStockModel[5].addShapeBox(0F, 0F, 0F, 8, 2, 7, 0F, 0F, -0.3F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, -0.3F, -1.5F, 0F, -0.3F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.3F, 0F); // scope4
		defaultStockModel[5].setRotationPoint(-18F, -15F, -3.5F);

		defaultStockModel[6].addShapeBox(0F, 0F, 0F, 20, 2, 8, 0F, -1F, 0F, -1.5F, -1F, 0F, -1.5F, -1F, 0F, -1.5F, -1F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope4
		defaultStockModel[6].setRotationPoint(-40F, -16.5F, -4F);

		defaultStockModel[7].addBox(0F, 0F, 0F, 20, 4, 8, 0F); // scope4
		defaultStockModel[7].setRotationPoint(-40F, -14.5F, -4F);

		defaultStockModel[8].addShapeBox(0F, 0F, 0F, 3, 11, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F); // scope4
		defaultStockModel[8].setRotationPoint(-39F, -10.5F, -4F);

		defaultStockModel[9].addShapeBox(0F, 0F, 0F, 3, 3, 8, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, -2F, 0F, -1F, -2F, 0F, -1F, -2F, 0F, 0F, -2F); // scope4
		defaultStockModel[9].setRotationPoint(-39F, 0.5F, -4F);

		defaultStockModel[10].addShapeBox(0F, 0F, 0F, 3, 3, 8, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, -2F, 0F, -1F, -2F, 0F, -1F, -2F, 0F, 0F, -2F); // scope4
		defaultStockModel[10].setRotationPoint(-31F, -1.5F, -4F);

		defaultStockModel[11].addShapeBox(0F, 0F, 0F, 3, 9, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 1F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 1F, 0F); // scope4
		defaultStockModel[11].setRotationPoint(-33F, -10.5F, -4F);

		defaultStockModel[12].addShapeBox(0F, 0F, 0F, 3, 6, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 2F, 0F, -2F, 1F, 0F, -2F, 1F, 0F, 2F, 2F, 0F); // scope4
		defaultStockModel[12].setRotationPoint(-25F, -10.5F, -4F);

		defaultStockModel[13].addShapeBox(0F, 0F, 0F, 3, 4, 8, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -0.5F, -2F, 0F, -1.5F, -2F, 0F, -1.5F, -2F, 0F, -0.5F, -2F); // scope4
		defaultStockModel[13].setRotationPoint(-27F, -3.5F, -4F);

		defaultStockModel[14].addShapeBox(0F, 0F, 0F, 3, 6, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 1F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 1F, 0F); // scope4
		defaultStockModel[14].setRotationPoint(-24F, -10.5F, -4F);

		defaultStockModel[15].addShapeBox(0F, 0F, 0F, 3, 3, 8, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, -2F, 0F, -1F, -2F, 0F, -1F, -2F, 0F, 0F, -2F); // scope4
		defaultStockModel[15].setRotationPoint(-22F, -4.5F, -4F);


		ammoModel = new ModelRendererTurbo[12];
		ammoModel[0] = new ModelRendererTurbo(this, 43, 127, textureX, textureY); // scope4
		ammoModel[1] = new ModelRendererTurbo(this, 43, 133, textureX, textureY); // scope4
		ammoModel[2] = new ModelRendererTurbo(this, 43, 103, textureX, textureY); // scope4
		ammoModel[3] = new ModelRendererTurbo(this, 1, 127, textureX, textureY); // scope4
		ammoModel[4] = new ModelRendererTurbo(this, 26, 104, textureX, textureY); // scope4
		ammoModel[5] = new ModelRendererTurbo(this, 43, 103, textureX, textureY); // scope4
		ammoModel[6] = new ModelRendererTurbo(this, 1, 103, textureX, textureY); // scope4
		ammoModel[7] = new ModelRendererTurbo(this, 26, 104, textureX, textureY); // scope4
		ammoModel[8] = new ModelRendererTurbo(this, 26, 104, textureX, textureY); // scope4
		ammoModel[9] = new ModelRendererTurbo(this, 1, 150, textureX, textureY); // scope4
		ammoModel[10] = new ModelRendererTurbo(this, 22, 139, textureX, textureY); // scope4
		ammoModel[11] = new ModelRendererTurbo(this, 22, 129, textureX, textureY); // scope4

		ammoModel[0].addShapeBox(0F, 0F, 0F, 10, 1, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope4
		ammoModel[0].setRotationPoint(17F, -8.5F, -2F);

		ammoModel[1].addShapeBox(0F, 0F, 0F, 4, 1, 4, 0F, 0F, 0F, -1F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F); // scope4
		ammoModel[1].setRotationPoint(27F, -8.5F, -2F);

		ammoModel[2].addBox(0F, 0F, 0F, 3, 15, 8, 0F); // scope4
		ammoModel[2].setRotationPoint(16F, -7.5F, -4F);

		ammoModel[3].addBox(0F, 0F, 0F, 3, 15, 7, 0F); // scope4
		ammoModel[3].setRotationPoint(28F, -7.5F, -3.5F);

		ammoModel[4].addShapeBox(0F, 0F, 0F, 1, 15, 7, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // scope4
		ammoModel[4].setRotationPoint(31F, -7.5F, -3.5F);

		ammoModel[5].addBox(0F, 0F, 0F, 3, 15, 8, 0F); // scope4
		ammoModel[5].setRotationPoint(20F, -7.5F, -4F);

		ammoModel[6].addBox(0F, 0F, 0F, 4, 15, 8, 0F); // scope4
		ammoModel[6].setRotationPoint(24F, -7.5F, -4F);

		ammoModel[7].addBox(0F, 0F, 0F, 1, 15, 7, 0F); // scope4
		ammoModel[7].setRotationPoint(19F, -7.5F, -3.5F);

		ammoModel[8].addBox(0F, 0F, 0F, 1, 15, 7, 0F); // scope4
		ammoModel[8].setRotationPoint(23F, -7.5F, -3.5F);

		ammoModel[9].addShapeBox(0F, 0F, 0F, 12, 5, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F); // scope4
		ammoModel[9].setRotationPoint(16F, 7.5F, -4F);

		ammoModel[10].addShapeBox(0F, 0F, 0F, 3, 3, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F); // scope4
		ammoModel[10].setRotationPoint(28F, 7.5F, -3.5F);

		ammoModel[11].addShapeBox(0F, 0F, 0F, 1, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.25F, -1F, 0F, 0.25F, -1F, 0F, 0.5F, 0F); // scope4
		ammoModel[11].setRotationPoint(31F, 7.5F, -3.5F);


		slideModel = new ModelRendererTurbo[2];
		slideModel[0] = new ModelRendererTurbo(this, 44, 9, textureX, textureY); // scope4
		slideModel[1] = new ModelRendererTurbo(this, 44, 4, textureX, textureY); // scope4

		slideModel[0].addBox(0F, 0F, 0F, 1, 3, 3, 0F); // scope4
		slideModel[0].setRotationPoint(28F, -17.5F, -7F);

		slideModel[1].addBox(0F, 0F, 0F, 3, 3, 1, 0F); // scope4
		slideModel[1].setRotationPoint(25F, -17.5F, -4.5F);


		pumpModel = new ModelRendererTurbo[2];
		pumpModel[0] = new ModelRendererTurbo(this, 44, 16, textureX, textureY); // scope4
		pumpModel[1] = new ModelRendererTurbo(this, 44, 16, textureX, textureY); // scope4

		pumpModel[0].addShapeBox(0F, 0F, 0F, 2, 1, 4, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope4
		pumpModel[0].setRotationPoint(27F, -17.5F, 4F);

		pumpModel[1].addShapeBox(0F, 0F, 0F, 2, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // scope4
		pumpModel[1].setRotationPoint(27F, -16.5F, 4F);

		barrelAttachPoint = new Vector3f(108F /16F, 10F /16F, 0F /16F);
		stockAttachPoint = new Vector3f(-10F /16F, 10F /16F, 0F /16F);
		scopeAttachPoint = new Vector3f(15F /16F, 22F /16F, 0F /16F);
		gripAttachPoint = new Vector3f(47 /16F, 6F /16F, 0F /16F);

		gunSlideDistance = 0.8F;
		animationType = EnumAnimationType.BOTTOM_CLIP;

		pumpDelayAfterReload = 65;
		pumpTime = 8;
		pumpHandleDistance = 0.8F;

		translateAll(0F, 0F, 0F);


		flipAll();
	}
}