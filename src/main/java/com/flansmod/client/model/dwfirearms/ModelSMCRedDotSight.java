package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.ModelAttachment;
import com.flansmod.client.tmt.ModelRendererTurbo;

import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelSMCRedDotSight extends ModelAttachment
{
	int textureX = 512;
	int textureY = 256;

	public ModelSMCRedDotSight()
	{
		attachmentModel = new ModelRendererTurbo[23];
		attachmentModel[0] = new ModelRendererTurbo(this, 1, 66, textureX, textureY); // scope
		attachmentModel[1] = new ModelRendererTurbo(this, 1, 54, textureX, textureY); // scope
		attachmentModel[2] = new ModelRendererTurbo(this, 30, 76, textureX, textureY); // Box 6
		attachmentModel[3] = new ModelRendererTurbo(this, 40, 71, textureX, textureY); // Box 11
		attachmentModel[4] = new ModelRendererTurbo(this, 40, 67, textureX, textureY); // Box 12
		attachmentModel[5] = new ModelRendererTurbo(this, 32, 71, textureX, textureY); // Box 13
		attachmentModel[6] = new ModelRendererTurbo(this, 32, 68, textureX, textureY); // Box 14
		attachmentModel[7] = new ModelRendererTurbo(this, 85, 44, textureX, textureY); // Box 19
		attachmentModel[8] = new ModelRendererTurbo(this, 55, 71, textureX, textureY); // Box 0
		attachmentModel[9] = new ModelRendererTurbo(this, 70, 71, textureX, textureY); // Box 1
		attachmentModel[10] = new ModelRendererTurbo(this, 55, 71, textureX, textureY); // Box 2
		attachmentModel[11] = new ModelRendererTurbo(this, 70, 71, textureX, textureY); // Box 3
		attachmentModel[12] = new ModelRendererTurbo(this, 54, 77, textureX, textureY); // Box 4
		attachmentModel[13] = new ModelRendererTurbo(this, 66, 64, textureX, textureY); // Box 8
		attachmentModel[14] = new ModelRendererTurbo(this, 55, 64, textureX, textureY); // Box 10
		attachmentModel[15] = new ModelRendererTurbo(this, 55, 53, textureX, textureY); // Box 11
		attachmentModel[16] = new ModelRendererTurbo(this, 66, 64, textureX, textureY); // Box 12
		attachmentModel[17] = new ModelRendererTurbo(this, 55, 64, textureX, textureY); // Box 13
		attachmentModel[18] = new ModelRendererTurbo(this, 55, 53, textureX, textureY); // Box 14
		attachmentModel[19] = new ModelRendererTurbo(this, 1, 76, textureX, textureY); // Box 18
		attachmentModel[20] = new ModelRendererTurbo(this, 41, 78, textureX, textureY); // Box 19
		attachmentModel[21] = new ModelRendererTurbo(this, 41, 78, textureX, textureY); // Box 20
		attachmentModel[22] = new ModelRendererTurbo(this, 22, 78, textureX, textureY); // Box 21

		attachmentModel[0].addShapeBox(-9F, -4F, -4F, 11, 1, 8, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope
		attachmentModel[0].setRotationPoint(0F, 0F, 0F);

		attachmentModel[1].addBox(-9F, -3F, -4F, 18, 3, 8, 0F); // scope
		attachmentModel[1].setRotationPoint(0F, 0F, 0F);

		attachmentModel[2].addShapeBox(2F, -6F, -4F, 1, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F); // Box 6
		attachmentModel[2].setRotationPoint(0F, 0F, 0F);

		attachmentModel[3].addBox(-5F, -5F, 2F, 5, 2, 2, 0F); // Box 11
		attachmentModel[3].setRotationPoint(0F, 0F, 0F);

		attachmentModel[4].addShapeBox(-5F, -6F, 2F, 5, 1, 2, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 12
		attachmentModel[4].setRotationPoint(0F, 0F, 0F);

		attachmentModel[5].addShapeBox(-3.5F, -4.5F, 4F, 2, 1, 1, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 13
		attachmentModel[5].setRotationPoint(0F, 0F, 0F);

		attachmentModel[6].addShapeBox(-3.5F, -3.5F, 4F, 2, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 14
		attachmentModel[6].setRotationPoint(0F, 0F, 0F);

		attachmentModel[7].addShapeBox(5.5F, -13.5F, -3.5F, 1, 21, 21, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -14F, 0F, 0F, -14F, 0F, -14F, 0F, 0F, -14F, 0F, 0F, -14F, -14F, 0F, -14F, -14F); // Box 19
		attachmentModel[7].setRotationPoint(0F, 0F, 0F);

		attachmentModel[8].addShapeBox(3F, -10F, -6F, 6, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 2F, 1F, 0F, 2F); // Box 0
		attachmentModel[8].setRotationPoint(0F, 0F, 0F);

		attachmentModel[9].addShapeBox(3F, -14F, -6F, 6, 4, 1, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 1
		attachmentModel[9].setRotationPoint(0F, 0F, 0F);

		attachmentModel[10].addShapeBox(3F, -10F, 5F, 6, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 1F, 0F, -2F); // Box 2
		attachmentModel[10].setRotationPoint(0F, 0F, 0F);

		attachmentModel[11].addShapeBox(3F, -14F, 5F, 6, 4, 1, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 3
		attachmentModel[11].setRotationPoint(0F, 0F, 0F);

		attachmentModel[12].addShapeBox(3F, -15F, -4F, 6, 1, 8, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 4
		attachmentModel[12].setRotationPoint(0F, 0F, 0F);

		attachmentModel[13].addShapeBox(4F, -15F, 6F, 4, 5, 1, 0F, 0F, 0F, 2.5F, 0F, 0F, 2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 8
		attachmentModel[13].setRotationPoint(0F, 0F, 0F);

		attachmentModel[14].addShapeBox(4F, -10F, 6F, 4, 5, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2.5F, 0F, 0F, 2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F); // Box 10
		attachmentModel[14].setRotationPoint(0F, 0F, 0F);

		attachmentModel[15].addShapeBox(4F, -16F, -4.5F, 4, 1, 9, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 11
		attachmentModel[15].setRotationPoint(0F, 0F, 0F);

		attachmentModel[16].addShapeBox(4F, -15F, -7F, 4, 5, 1, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, 2.5F, 0F, 0F, 2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 12
		attachmentModel[16].setRotationPoint(0F, 0F, 0F);

		attachmentModel[17].addShapeBox(4F, -10F, -7F, 4, 5, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, 2.5F, 0F, 0F, 2.5F); // Box 13
		attachmentModel[17].setRotationPoint(0F, 0F, 0F);

		attachmentModel[18].addShapeBox(4F, -5F, -4.5F, 4, 1, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 14
		attachmentModel[18].setRotationPoint(0F, 0F, 0F);

		attachmentModel[19].addBox(3F, -6F, -4F, 6, 3, 8, 0F); // Box 18
		attachmentModel[19].setRotationPoint(0F, 0F, 0F);

		attachmentModel[20].addShapeBox(-4.5F, -4.5F, -2F, 1, 1, 4, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 19
		attachmentModel[20].setRotationPoint(0F, 0F, 0F);

		attachmentModel[21].addShapeBox(-1.5F, -4.5F, -2F, 1, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 20
		attachmentModel[21].setRotationPoint(0F, 0F, 0F);

		attachmentModel[22].addBox(-3.5F, -4.5F, -2F, 2, 1, 4, 0F); // Box 21
		attachmentModel[22].setRotationPoint(0F, 0F, 0F);

		renderOffset = 0F;

		flipAll();
	}
}