package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.ModelAttachment;
import com.flansmod.client.tmt.ModelRendererTurbo;

import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelLPMHoloSight extends ModelAttachment 
{
	int textureX = 512;
	int textureY = 256;

	public ModelLPMHoloSight()
	{	
		attachmentModel = new ModelRendererTurbo[17];
		attachmentModel[0] = new ModelRendererTurbo(this, 1, 101, textureX, textureY); // mounter
		attachmentModel[1] = new ModelRendererTurbo(this, 67, 140, textureX, textureY); // innerFrame1
		attachmentModel[2] = new ModelRendererTurbo(this, 67, 131, textureX, textureY); // innerFrame2
		attachmentModel[3] = new ModelRendererTurbo(this, 60, 88, textureX, textureY); // sight
		attachmentModel[4] = new ModelRendererTurbo(this, 67, 151, textureX, textureY); // innerFrame1
		attachmentModel[5] = new ModelRendererTurbo(this, 67, 131, textureX, textureY); // innerFrame2
		attachmentModel[6] = new ModelRendererTurbo(this, 1, 114, textureX, textureY); // mounterBase
		attachmentModel[7] = new ModelRendererTurbo(this, 40, 115, textureX, textureY); // control
		attachmentModel[8] = new ModelRendererTurbo(this, 1, 127, textureX, textureY); // outerFrame1
		attachmentModel[9] = new ModelRendererTurbo(this, 24, 140, textureX, textureY); // outerFrame2
		attachmentModel[10] = new ModelRendererTurbo(this, 1, 140, textureX, textureY); // outerFrame2
		attachmentModel[11] = new ModelRendererTurbo(this, 1, 147, textureX, textureY); // outerFrame3
		attachmentModel[12] = new ModelRendererTurbo(this, 24, 147, textureX, textureY); // outerFrame3
		attachmentModel[13] = new ModelRendererTurbo(this, 1, 153, textureX, textureY); // outerFrame4
		attachmentModel[14] = new ModelRendererTurbo(this, 28, 153, textureX, textureY); // outerFrame4
		attachmentModel[15] = new ModelRendererTurbo(this, 1, 158, textureX, textureY); // outerFrame5
		attachmentModel[16] = new ModelRendererTurbo(this, 34, 158, textureX, textureY); // outerFrame5

		attachmentModel[0].addBox(-10F, -3F, -4.5F, 20, 3, 9, 0F); // mounter
		attachmentModel[0].setRotationPoint(0F, 0F, 0F);

		attachmentModel[1].addShapeBox(-8F, -15F, -4.5F, 8, 1, 9, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // innerFrame1
		attachmentModel[1].setRotationPoint(0F, 0F, 0F);

		attachmentModel[2].addShapeBox(-8F, -14F, -4.5F, 8, 7, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F); // innerFrame2
		attachmentModel[2].setRotationPoint(0F, 0F, 0F);

		attachmentModel[3].addShapeBox(-6F, -14F, -3.5F, 1, 21, 21, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -14F, 0F, 0F, -14F, 0F, -14F, 0F, 0F, -14F, 0F, 0F, -14F, -14F, 0F, -14F, -14F); // sight
		attachmentModel[3].setRotationPoint(0F, 0F, 0F);

		attachmentModel[4].addBox(-9F, -7F, -4.5F, 9, 1, 9, 0F); // innerFrame1
		attachmentModel[4].setRotationPoint(0F, 0F, 0F);

		attachmentModel[5].addShapeBox(-8F, -14F, 3.5F, 8, 7, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F); // innerFrame2
		attachmentModel[5].setRotationPoint(0F, 0F, 0F);

		attachmentModel[6].addShapeBox(-9F, -6F, -4.5F, 10, 3, 9, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // mounterBase
		attachmentModel[6].setRotationPoint(0F, 0F, 0F);

		attachmentModel[7].addBox(-9.5F, -6.5F, -4F, 1, 3, 8, 0F); // control
		attachmentModel[7].setRotationPoint(0F, 0F, 0F);

		attachmentModel[8].addShapeBox(-9F, -16F, -5.5F, 10, 1, 11, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // outerFrame1
		attachmentModel[8].setRotationPoint(0F, 0F, 0F);

		attachmentModel[9].addBox(-9F, -15F, -5.5F, 10, 5, 1, 0F); // outerFrame2
		attachmentModel[9].setRotationPoint(0F, 0F, 0F);

		attachmentModel[10].addBox(-9F, -15F, 4.5F, 10, 5, 1, 0F); // outerFrame2
		attachmentModel[10].setRotationPoint(0F, 0F, 0F);

		attachmentModel[11].addShapeBox(-9F, -10F, -5.5F, 10, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F); // outerFrame3
		attachmentModel[11].setRotationPoint(0F, 0F, 0F);

		attachmentModel[12].addShapeBox(-9F, -10F, 4.5F, 10, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F); // outerFrame3
		attachmentModel[12].setRotationPoint(0F, 0F, 0F);

		attachmentModel[13].addShapeBox(-10F, -6F, -5.5F, 12, 3, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F); // outerFrame4
		attachmentModel[13].setRotationPoint(0F, 0F, 0F);

		attachmentModel[14].addShapeBox(-10F, -6F, 4.5F, 12, 3, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F); // outerFrame4
		attachmentModel[14].setRotationPoint(0F, 0F, 0F);

		attachmentModel[15].addBox(-10F, -3F, -5.5F, 15, 2, 1, 0F); // outerFrame5
		attachmentModel[15].setRotationPoint(0F, 0F, 0F);

		attachmentModel[16].addBox(-10F, -3F, 4.5F, 15, 2, 1, 0F); // outerFrame5
		attachmentModel[16].setRotationPoint(0F, 0F, 0F);

		renderOffset = 0F;

		flipAll();
	}
}