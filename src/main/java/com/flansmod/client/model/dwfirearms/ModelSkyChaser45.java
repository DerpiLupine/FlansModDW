package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelSkyChaser45 extends ModelGun
{
	int textureX = 512;
	int textureY = 256;

	public ModelSkyChaser45()
	{
		gunModel = new ModelRendererTurbo[33];
		gunModel[0] = new ModelRendererTurbo(this, 170, 73, textureX, textureY); // ammoReceiver
		gunModel[1] = new ModelRendererTurbo(this, 90, 1, textureX, textureY); // barrel
		gunModel[2] = new ModelRendererTurbo(this, 90, 1, textureX, textureY); // barrel
		gunModel[3] = new ModelRendererTurbo(this, 125, 1, textureX, textureY); // barrel2
		gunModel[4] = new ModelRendererTurbo(this, 37, 2, textureX, textureY); // barrelPin
		gunModel[5] = new ModelRendererTurbo(this, 37, 2, textureX, textureY); // barrelPin
		gunModel[6] = new ModelRendererTurbo(this, 37, 8, textureX, textureY); // barrelPin2
		gunModel[7] = new ModelRendererTurbo(this, 90, 98, textureX, textureY); // body3
		gunModel[8] = new ModelRendererTurbo(this, 170, 87, textureX, textureY); // body4
		gunModel[9] = new ModelRendererTurbo(this, 1, 20, textureX, textureY); // grip
		gunModel[10] = new ModelRendererTurbo(this, 1, 32, textureX, textureY); // grip2
		gunModel[11] = new ModelRendererTurbo(this, 53, 32, textureX, textureY); // grip3
		gunModel[12] = new ModelRendererTurbo(this, 34, 32, textureX, textureY); // Box 0
		gunModel[13] = new ModelRendererTurbo(this, 1, 49, textureX, textureY); // Box 1
		gunModel[14] = new ModelRendererTurbo(this, 55, 50, textureX, textureY); // Box 2
		gunModel[15] = new ModelRendererTurbo(this, 34, 49, textureX, textureY); // Box 3
		gunModel[16] = new ModelRendererTurbo(this, 34, 65, textureX, textureY); // Box 4
		gunModel[17] = new ModelRendererTurbo(this, 55, 63, textureX, textureY); // Box 5
		gunModel[18] = new ModelRendererTurbo(this, 1, 63, textureX, textureY); // Box 6
		gunModel[19] = new ModelRendererTurbo(this, 41, 127, textureX, textureY); // Box 7
		gunModel[20] = new ModelRendererTurbo(this, 41, 117, textureX, textureY); // Box 8
		gunModel[21] = new ModelRendererTurbo(this, 20, 119, textureX, textureY); // Box 9
		gunModel[22] = new ModelRendererTurbo(this, 41, 138, textureX, textureY); // Box 10
		gunModel[23] = new ModelRendererTurbo(this, 42, 22, textureX, textureY); // Box 11
		gunModel[24] = new ModelRendererTurbo(this, 1, 121, textureX, textureY); // Box 12
		gunModel[25] = new ModelRendererTurbo(this, 170, 61, textureX, textureY); // Box 13
		gunModel[26] = new ModelRendererTurbo(this, 90, 57, textureX, textureY); // Box 1
		gunModel[27] = new ModelRendererTurbo(this, 115, 63, textureX, textureY); // Box 2
		gunModel[28] = new ModelRendererTurbo(this, 153, 36, textureX, textureY); // Box 3
		gunModel[29] = new ModelRendererTurbo(this, 178, 37, textureX, textureY); // Box 5
		gunModel[30] = new ModelRendererTurbo(this, 136, 62, textureX, textureY); // Box 6
		gunModel[31] = new ModelRendererTurbo(this, 89, 10, textureX, textureY); // Box 7
		gunModel[32] = new ModelRendererTurbo(this, 89, 26, textureX, textureY); // Box 8

		gunModel[0].addBox(0F, 0F, 0F, 13, 5, 8, 0F); // ammoReceiver
		gunModel[0].setRotationPoint(21F, -12F, -4F);

		gunModel[1].addShapeBox(0F, 0F, 0F, 11, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // barrel
		gunModel[1].setRotationPoint(35F, -19.5F, -3F);

		gunModel[2].addShapeBox(0F, 0F, 0F, 11, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // barrel
		gunModel[2].setRotationPoint(35F, -23.5F, -3F);

		gunModel[3].addBox(0F, 0F, 0F, 11, 2, 6, 0F); // barrel2
		gunModel[3].setRotationPoint(35F, -21.5F, -3F);

		gunModel[4].addShapeBox(0F, 0F, 0F, 10, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // barrelPin
		gunModel[4].setRotationPoint(35.4F, -12F, -2F);

		gunModel[5].addShapeBox(0F, 0F, 0F, 10, 1, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // barrelPin
		gunModel[5].setRotationPoint(35.4F, -15F, -2F);

		gunModel[6].addBox(0F, 0F, 0F, 10, 2, 4, 0F); // barrelPin2
		gunModel[6].setRotationPoint(35.4F, -14F, -2F);

		gunModel[7].addBox(0F, 0F, 0F, 39, 4, 8, 0F); // body3
		gunModel[7].setRotationPoint(-9F, -16F, -4F);

		gunModel[8].addShapeBox(0F, 0F, 0F, 14, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // body4
		gunModel[8].setRotationPoint(5F, -12F, -4F);

		gunModel[9].addShapeBox(0F, 0F, 0F, 12, 3, 8, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // grip
		gunModel[9].setRotationPoint(-7F, -12F, -4F);

		gunModel[10].addShapeBox(0F, 0F, 0F, 8, 8, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 4F, 0F, 0F); // grip2
		gunModel[10].setRotationPoint(-6F, -9F, -4F);

		gunModel[11].addShapeBox(0F, 0F, 0F, 2, 8, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 4F, 0F, 0F, -4F, 0F, -1F, -4F, 0F, -1F, 4F, 0F, 0F); // grip3
		gunModel[11].setRotationPoint(2F, -9F, -4F);

		gunModel[12].addShapeBox(0F, 0F, 0F, 1, 8, 8, 0F, -4F, 0F, -1F, 4F, 0F, 0F, 4F, 0F, 0F, -4F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 0
		gunModel[12].setRotationPoint(-11F, -9F, -4F);

		gunModel[13].addShapeBox(0F, 0F, 0F, 8, 5, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, -1F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 2F, -1F, 0F); // Box 1
		gunModel[13].setRotationPoint(-10F, -1F, -4F);

		gunModel[14].addShapeBox(0F, 0F, 0F, 1, 4, 8, 0F, -2F, 0F, -1F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 2
		gunModel[14].setRotationPoint(-13F, -1F, -4F);

		gunModel[15].addShapeBox(0F, 0F, 0F, 2, 5, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, -1F, -2F, 0F, -1F, 2F, 0F, 0F); // Box 3
		gunModel[15].setRotationPoint(-2F, -1F, -4F);

		gunModel[16].addShapeBox(0F, 0F, 0F, 2, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 4
		gunModel[16].setRotationPoint(-4F, 4F, -4F);

		gunModel[17].addShapeBox(0F, 0F, 0F, 1, 4, 8, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 5
		gunModel[17].setRotationPoint(-13F, 3F, -4F);

		gunModel[18].addShapeBox(0F, 0F, 0F, 8, 4, 8, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F); // Box 6
		gunModel[18].setRotationPoint(-12F, 4F, -4F);

		gunModel[19].addBox(0F, 0F, 0F, 23, 2, 8, 0F); // Box 7
		gunModel[19].setRotationPoint(-4F, 6F, -4F);

		gunModel[20].addShapeBox(0F, 0F, 0F, 23, 1, 8, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 8
		gunModel[20].setRotationPoint(-4F, 5F, -4F);

		gunModel[21].addBox(0F, 0F, 0F, 2, 20, 8, 0F); // Box 9
		gunModel[21].setRotationPoint(19F, -12F, -4F);

		gunModel[22].addShapeBox(0F, 0F, 0F, 25, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 10
		gunModel[22].setRotationPoint(-4F, 8F, -3.5F);

		gunModel[23].addShapeBox(0F, 0F, 0F, 8, 2, 7, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, -1F, -1F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, -1F, -1F, -1F); // Box 11
		gunModel[23].setRotationPoint(-12F, 8F, -3.5F);

		gunModel[24].addShapeBox(0F, 0F, 0F, 1, 18, 8, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 12
		gunModel[24].setRotationPoint(18F, -12F, -4F);

		gunModel[25].addShapeBox(0F, 0F, 0F, 13, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F); // Box 13
		gunModel[25].setRotationPoint(21F, -7F, -4F);

		gunModel[26].addShapeBox(0F, 0F, 0F, 4, 7, 8, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 1
		gunModel[26].setRotationPoint(-13F, -23F, -4F);

		gunModel[27].addShapeBox(0F, 0F, 0F, 2, 1, 8, 0F, -0.5F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, -0.5F, 0.5F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 2
		gunModel[27].setRotationPoint(-11F, -24F, -4F);

		gunModel[28].addShapeBox(0F, 0F, 0F, 4, 5, 8, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 3
		gunModel[28].setRotationPoint(7F, -21F, -4F);

		gunModel[29].addShapeBox(0F, 0F, 0F, 4, 4, 8, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 5
		gunModel[29].setRotationPoint(30F, -16F, -4F);

		gunModel[30].addShapeBox(0F, 0F, 0F, 4, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 6
		gunModel[30].setRotationPoint(-13F, -16F, -4F);

		gunModel[31].addShapeBox(0F, 0F, 0F, 16, 7, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 7
		gunModel[31].setRotationPoint(-9F, -23F, -4F);

		gunModel[32].addShapeBox(0F, 0F, 0F, 16, 1, 8, 0F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 8
		gunModel[32].setRotationPoint(-9F, -24F, -4F);


		defaultScopeModel = new ModelRendererTurbo[4];
		defaultScopeModel[0] = new ModelRendererTurbo(this, 1, 8, textureX, textureY); // ironSight
		defaultScopeModel[1] = new ModelRendererTurbo(this, 1, 14, textureX, textureY); // ironSight
		defaultScopeModel[2] = new ModelRendererTurbo(this, 14, 13, textureX, textureY); // ironSight2
		defaultScopeModel[3] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // ironSight3

		defaultScopeModel[0].addBox(0F, 0F, 0F, 4, 3, 2, 0F); // ironSight
		defaultScopeModel[0].setRotationPoint(-7F, -27F, -3F);

		defaultScopeModel[1].addBox(0F, 0F, 0F, 4, 3, 2, 0F); // ironSight
		defaultScopeModel[1].setRotationPoint(-7F, -27F, 1F);

		defaultScopeModel[2].addBox(0F, 0F, 0F, 2, 2, 4, 0F); // ironSight2
		defaultScopeModel[2].setRotationPoint(-6F, -26F, -2F);

		defaultScopeModel[3].addBox(0F, 0F, 0F, 4, 1, 5, 0F); // ironSight3
		defaultScopeModel[3].setRotationPoint(-4F, -25F, -2.5F);


		ammoModel = new ModelRendererTurbo[8];
		ammoModel[0] = new ModelRendererTurbo(this, 1, 99, textureX, textureY); // ammo
		ammoModel[1] = new ModelRendererTurbo(this, 45, 82, textureX, textureY); // bullet
		ammoModel[2] = new ModelRendererTurbo(this, 70, 82, textureX, textureY); // bulletTip
		ammoModel[3] = new ModelRendererTurbo(this, 24, 76, textureX, textureY); // clip1
		ammoModel[4] = new ModelRendererTurbo(this, 1, 76, textureX, textureY); // clip1
		ammoModel[5] = new ModelRendererTurbo(this, 40, 105, textureX, textureY); // clip2
		ammoModel[6] = new ModelRendererTurbo(this, 40, 96, textureX, textureY); // clip3
		ammoModel[7] = new ModelRendererTurbo(this, 75, 88, textureX, textureY); // Box 59

		ammoModel[0].addBox(0F, 0F, 0F, 3, 15, 5, 0F); // ammo
		ammoModel[0].setRotationPoint(24.5F, -9F, -2.5F);

		ammoModel[1].addShapeBox(0F, 0F, 0F, 8, 1, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // bullet
		ammoModel[1].setRotationPoint(22.5F, -12F, -2F);

		ammoModel[2].addShapeBox(0F, 0F, 0F, 2, 1, 4, 0F, 0F, 0F, -1F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // bulletTip
		ammoModel[2].setRotationPoint(30.5F, -12F, -2F);

		ammoModel[3].addBox(0F, 0F, 0F, 3, 15, 6, 0F); // clip1
		ammoModel[3].setRotationPoint(21.5F, -9F, -3F);

		ammoModel[4].addBox(0F, 0F, 0F, 5, 15, 6, 0F); // clip1
		ammoModel[4].setRotationPoint(27.5F, -9F, -3F);

		ammoModel[5].addBox(0F, 0F, 0F, 11, 4, 6, 0F); // clip2
		ammoModel[5].setRotationPoint(21.5F, 6F, -3F);

		ammoModel[6].addBox(0F, 0F, 0F, 11, 2, 6, 0F); // clip3
		ammoModel[6].setRotationPoint(21.5F, -11F, -3F);

		ammoModel[7].addShapeBox(0F, 0F, 0F, 1, 21, 6, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 59
		ammoModel[7].setRotationPoint(32.5F, -11F, -3F);


		slideModel = new ModelRendererTurbo[2];
		slideModel[0] = new ModelRendererTurbo(this, 89, 36, textureX, textureY); // Box 15
		slideModel[1] = new ModelRendererTurbo(this, 89, 50, textureX, textureY); // Box 16

		slideModel[0].addShapeBox(0F, 0F, 0F, 12, 7, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 15
		slideModel[0].setRotationPoint(22F, -22.5F, -3F);

		slideModel[1].addShapeBox(0F, 0F, 0F, 12, 1, 6, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 16
		slideModel[1].setRotationPoint(22F, -23.5F, -3F);


		pumpModel = new ModelRendererTurbo[12];
		pumpModel[0] = new ModelRendererTurbo(this, 138, 11, textureX, textureY); // body1
		pumpModel[1] = new ModelRendererTurbo(this, 90, 89, textureX, textureY); // body10
		pumpModel[2] = new ModelRendererTurbo(this, 138, 27, textureX, textureY); // body2
		pumpModel[3] = new ModelRendererTurbo(this, 127, 76, textureX, textureY); // body5
		pumpModel[4] = new ModelRendererTurbo(this, 90, 73, textureX, textureY); // body9
		pumpModel[5] = new ModelRendererTurbo(this, 14, 9, textureX, textureY); // ironSightFront
		pumpModel[6] = new ModelRendererTurbo(this, 20, 2, textureX, textureY); // ironSightFront2
		pumpModel[7] = new ModelRendererTurbo(this, 127, 88, textureX, textureY); // Box 14
		pumpModel[8] = new ModelRendererTurbo(this, 126, 36, textureX, textureY); // Box 17
		pumpModel[9] = new ModelRendererTurbo(this, 126, 43, textureX, textureY); // Box 9
		pumpModel[10] = new ModelRendererTurbo(this, 183, 11, textureX, textureY); // Box 10
		pumpModel[11] = new ModelRendererTurbo(this, 183, 27, textureX, textureY); // Box 11

		pumpModel[0].addShapeBox(0F, 0F, 0F, 15, 7, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body1
		pumpModel[0].setRotationPoint(7F, -23F, -3.5F);

		pumpModel[1].addShapeBox(0F, 0F, 0F, 11, 1, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body10
		pumpModel[1].setRotationPoint(34F, -24F, -3.5F);

		pumpModel[2].addShapeBox(0F, 0F, 0F, 15, 1, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body2
		pumpModel[2].setRotationPoint(7F, -24F, -3.5F);

		pumpModel[3].addBox(0F, 0F, 0F, 14, 4, 7, 0F); // body5
		pumpModel[3].setRotationPoint(31F, -16F, -3.5F);

		pumpModel[4].addBox(0F, 0F, 0F, 11, 7, 7, 0F); // body9
		pumpModel[4].setRotationPoint(34F, -23F, -3.5F);

		pumpModel[5].addShapeBox(0F, 0F, 0F, 3, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F); // ironSightFront
		pumpModel[5].setRotationPoint(38F, -27F, -0.5F);

		pumpModel[6].addBox(0F, 0F, 0F, 4, 1, 4, 0F); // ironSightFront2
		pumpModel[6].setRotationPoint(38F, -25F, -2F);

		pumpModel[7].addShapeBox(0F, 0F, 0F, 14, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 14
		pumpModel[7].setRotationPoint(31F, -12F, -3.5F);

		pumpModel[8].addShapeBox(0F, 0F, 0F, 12, 5, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 17
		pumpModel[8].setRotationPoint(22F, -21F, -3.5F);

		pumpModel[9].addShapeBox(0F, 0F, 0F, 12, 5, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 9
		pumpModel[9].setRotationPoint(22F, -21F, 2.5F);

		pumpModel[10].addShapeBox(0F, 0F, 0F, 12, 7, 7, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F); // Box 10
		pumpModel[10].setRotationPoint(-12F, -23F, -3.5F);

		pumpModel[11].addShapeBox(0F, 0F, 0F, 10, 1, 7, 0F, -0.5F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, -0.5F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 11
		pumpModel[11].setRotationPoint(-10F, -24F, -3.5F);

		barrelAttachPoint = new Vector3f(46F /16F, 20.5F /16F, 0F /16F);

		gunSlideDistance = 1F;
		animationType = EnumAnimationType.BOTTOM_CLIP;

		translateAll(0F, 0F, 0F);


		flipAll();
	}
}