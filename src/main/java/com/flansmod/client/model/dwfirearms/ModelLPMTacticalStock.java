package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.ModelAttachment;
import com.flansmod.client.tmt.ModelRendererTurbo;

import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelLPMTacticalStock extends ModelAttachment
{
	int textureX = 512;
	int textureY = 256;

	public ModelLPMTacticalStock()
	{
		attachmentModel = new ModelRendererTurbo[14];
		attachmentModel[0] = new ModelRendererTurbo(this, 1, 32, textureX, textureY); // stock2
		attachmentModel[1] = new ModelRendererTurbo(this, 1, 41, textureX, textureY); // stock3
		attachmentModel[2] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // stockPipe1
		attachmentModel[3] = new ModelRendererTurbo(this, 1, 11, textureX, textureY); // stockPipe2
		attachmentModel[4] = new ModelRendererTurbo(this, 72, 11, textureX, textureY); // Box 68
		attachmentModel[5] = new ModelRendererTurbo(this, 72, 20, textureX, textureY); // Box 69
		attachmentModel[6] = new ModelRendererTurbo(this, 72, 2, textureX, textureY); // Box 70
		attachmentModel[7] = new ModelRendererTurbo(this, 1, 22, textureX, textureY); // Box 71
		attachmentModel[8] = new ModelRendererTurbo(this, 99, 29, textureX, textureY); // Box 72
		attachmentModel[9] = new ModelRendererTurbo(this, 72, 29, textureX, textureY); // Box 73
		attachmentModel[10] = new ModelRendererTurbo(this, 32, 32, textureX, textureY); // Box 74
		attachmentModel[11] = new ModelRendererTurbo(this, 1, 56, textureX, textureY); // Box 75
		attachmentModel[12] = new ModelRendererTurbo(this, 20, 43, textureX, textureY); // Box 76
		attachmentModel[13] = new ModelRendererTurbo(this, 20, 61, textureX, textureY); // Box 77

		attachmentModel[0].addShapeBox(-34F, 6.5F, -2.5F, 10, 3, 5, 0F, 0F, -9F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -9F, 0F, 0F, 9F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 9F, 0F); // stock2
		attachmentModel[0].setRotationPoint(0F, 0F, 0F);

		attachmentModel[1].addBox(-40F, -1.5F, -3.5F, 2, 7, 7, 0F); // stock3
		attachmentModel[1].setRotationPoint(0F, 0F, 0F);

		attachmentModel[2].addShapeBox(-40F, -3.5F, -3.5F, 28, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // stockPipe1
		attachmentModel[2].setRotationPoint(0F, 0F, 0F);

		attachmentModel[3].addBox(-38F, -1.5F, -3.5F, 26, 3, 7, 0F); // stockPipe2
		attachmentModel[3].setRotationPoint(0F, 0F, 0F);

		attachmentModel[4].addBox(-12F, -1F, -3F, 12, 2, 6, 0F); // Box 68
		attachmentModel[4].setRotationPoint(0F, 0F, 0F);

		attachmentModel[5].addShapeBox(-12F, 1F, -3F, 12, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 69
		attachmentModel[5].setRotationPoint(0F, 0F, 0F);

		attachmentModel[6].addShapeBox(-12F, -3F, -3F, 12, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 70
		attachmentModel[6].setRotationPoint(0F, 0F, 0F);

		attachmentModel[7].addBox(-38F, 1.5F, -2.5F, 26, 4, 5, 0F); // Box 71
		attachmentModel[7].setRotationPoint(0F, 0F, 0F);

		attachmentModel[8].addShapeBox(-24F, 5.5F, -2.5F, 1, 4, 5, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 72
		attachmentModel[8].setRotationPoint(0F, 0F, 0F);

		attachmentModel[9].addBox(-23F, 5.5F, -2.5F, 8, 4, 5, 0F); // Box 73
		attachmentModel[9].setRotationPoint(0F, 0F, 0F);

		attachmentModel[10].addBox(-36F, 15.5F, -2.5F, 2, 3, 5, 0F); // Box 74
		attachmentModel[10].setRotationPoint(0F, 0F, 0F);

		attachmentModel[11].addBox(-38F, 10.5F, -3.5F, 2, 7, 7, 0F); // Box 75
		attachmentModel[11].setRotationPoint(0F, 0F, 0F);

		attachmentModel[12].addShapeBox(-40F, 5.5F, -3.5F, 2, 5, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F); // Box 76
		attachmentModel[12].setRotationPoint(0F, 0F, 0F);

		attachmentModel[13].addShapeBox(-38F, 17.5F, -3.5F, 2, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F); // Box 77
		attachmentModel[13].setRotationPoint(0F, 0F, 0F);

		renderOffset = 0F;

		flipAll();
	}
}