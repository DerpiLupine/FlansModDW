package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.ModelAttachment;
import com.flansmod.client.tmt.ModelRendererTurbo;

import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelLPMSharedMuzzleBrake extends ModelAttachment 
{
	int textureX = 512;
	int textureY = 256;

	public ModelLPMSharedMuzzleBrake()
	{	
		attachmentModel = new ModelRendererTurbo[9];
		attachmentModel[0] = new ModelRendererTurbo(this, 1, 51, textureX, textureY); // Box 68
		attachmentModel[1] = new ModelRendererTurbo(this, 1, 72, textureX, textureY); // Box 69
		attachmentModel[2] = new ModelRendererTurbo(this, 1, 61, textureX, textureY); // Box 70
		attachmentModel[3] = new ModelRendererTurbo(this, 20, 77, textureX, textureY); // Box 71
		attachmentModel[4] = new ModelRendererTurbo(this, 20, 71, textureX, textureY); // Box 72
		attachmentModel[5] = new ModelRendererTurbo(this, 29, 71, textureX, textureY); // Box 73
		attachmentModel[6] = new ModelRendererTurbo(this, 29, 77, textureX, textureY); // Box 74
		attachmentModel[7] = new ModelRendererTurbo(this, 20, 77, textureX, textureY); // Box 75
		attachmentModel[8] = new ModelRendererTurbo(this, 20, 71, textureX, textureY); // Box 76

		attachmentModel[0].addShapeBox(0F, -3.5F, -3.5F, 14, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 68
		attachmentModel[0].setRotationPoint(0F, 0F, 0F);

		attachmentModel[1].addBox(0F, -1.5F, -3.5F, 2, 3, 7, 0F); // Box 69
		attachmentModel[1].setRotationPoint(0F, 0F, 0F);

		attachmentModel[2].addShapeBox(0F, 1.5F, -3.5F, 14, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 70
		attachmentModel[2].setRotationPoint(0F, 0F, 0F);

		attachmentModel[3].addBox(4F, -1.5F, -3.5F, 2, 3, 2, 0F); // Box 71
		attachmentModel[3].setRotationPoint(0F, 0F, 0F);

		attachmentModel[4].addBox(4F, -1.5F, 1.5F, 2, 3, 2, 0F); // Box 72
		attachmentModel[4].setRotationPoint(0F, 0F, 0F);

		attachmentModel[5].addBox(12F, -1.5F, 1.5F, 2, 3, 2, 0F); // Box 73
		attachmentModel[5].setRotationPoint(0F, 0F, 0F);

		attachmentModel[6].addBox(12F, -1.5F, -3.5F, 2, 3, 2, 0F); // Box 74
		attachmentModel[6].setRotationPoint(0F, 0F, 0F);

		attachmentModel[7].addBox(8F, -1.5F, -3.5F, 2, 3, 2, 0F); // Box 75
		attachmentModel[7].setRotationPoint(0F, 0F, 0F);

		attachmentModel[8].addBox(8F, -1.5F, 1.5F, 2, 3, 2, 0F); // Box 76
		attachmentModel[8].setRotationPoint(0F, 0F, 0F);

		renderOffset = 0F;

		flipAll();
	}
}