package com.flansmod.client.model.dwfirearms;

import net.minecraft.client.model.ModelBase;
import net.minecraft.entity.Entity;

import com.flansmod.client.tmt.ModelRendererTurbo;
import org.lwjgl.opengl.GL11;

public class ModelFragGrenadeHDLPM extends ModelBase 
{
	public ModelRendererTurbo[] grenadeModel;
	public ModelRendererTurbo[] pinModel;

	int textureX = 256;
	int textureY = 128;

	public ModelFragGrenadeHDLPM()
	{
		grenadeModel = new ModelRendererTurbo[26];
		grenadeModel[0] = new ModelRendererTurbo(this, 26, 1, textureX, textureY); // canisterBack
		grenadeModel[1] = new ModelRendererTurbo(this, 26, 26, textureX, textureY); // canisterFarBack
		grenadeModel[2] = new ModelRendererTurbo(this, 1, 51, textureX, textureY); // canisterFarFront
		grenadeModel[3] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // canisterFront
		grenadeModel[4] = new ModelRendererTurbo(this, 1, 26, textureX, textureY); // canisterMiddle
		grenadeModel[5] = new ModelRendererTurbo(this, 22, 63, textureX, textureY); // lidTop1
		grenadeModel[6] = new ModelRendererTurbo(this, 22, 49, textureX, textureY); // lidTop2
		grenadeModel[7] = new ModelRendererTurbo(this, 22, 49, textureX, textureY); // lidTop3
		grenadeModel[8] = new ModelRendererTurbo(this, 22, 49, textureX, textureY); // lidTop4
		grenadeModel[9] = new ModelRendererTurbo(this, 22, 63, textureX, textureY); // lidTop5
		grenadeModel[10] = new ModelRendererTurbo(this, 49, 66, textureX, textureY); // lidBottom1
		grenadeModel[11] = new ModelRendererTurbo(this, 49, 66, textureX, textureY); // lidBottom5
		grenadeModel[12] = new ModelRendererTurbo(this, 49, 50, textureX, textureY); // lidBottom2
		grenadeModel[13] = new ModelRendererTurbo(this, 49, 50, textureX, textureY); // lidBottom4
		grenadeModel[14] = new ModelRendererTurbo(this, 49, 50, textureX, textureY); // lidBottom3
		grenadeModel[15] = new ModelRendererTurbo(this, 47, 26, textureX, textureY); // canisterTop1
		grenadeModel[16] = new ModelRendererTurbo(this, 47, 26, textureX, textureY); // canisterTop5
		grenadeModel[17] = new ModelRendererTurbo(this, 47, 37, textureX, textureY); // canisterTop2
		grenadeModel[18] = new ModelRendererTurbo(this, 47, 37, textureX, textureY); // canisterTop4
		grenadeModel[19] = new ModelRendererTurbo(this, 47, 37, textureX, textureY); // canisterTop3
		grenadeModel[20] = new ModelRendererTurbo(this, 51, 17, textureX, textureY); // cap2
		grenadeModel[21] = new ModelRendererTurbo(this, 51, 17, textureX, textureY); // cap1
		grenadeModel[22] = new ModelRendererTurbo(this, 51, 17, textureX, textureY); // cap3
		grenadeModel[23] = new ModelRendererTurbo(this, 68, 17, textureX, textureY); // topPart1
		grenadeModel[24] = new ModelRendererTurbo(this, 68, 11, textureX, textureY); // topPart2
		grenadeModel[25] = new ModelRendererTurbo(this, 63, 9, textureX, textureY); // pinHolder

		grenadeModel[0].addShapeBox(-2F, -10F, -4F, 2, 14, 10, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // canisterBack
		grenadeModel[0].setRotationPoint(0F, 0F, 0F);

		grenadeModel[1].addShapeBox(-4F, -10F, -3F, 2, 14, 8, 0F, -0.3F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, -0.3F, 0F, -2.5F, -0.3F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, -0.3F, 0F, -2.5F); // canisterFarBack
		grenadeModel[1].setRotationPoint(0F, 0F, 0F);

		grenadeModel[2].addShapeBox(4F, -10F, -3F, 2, 14, 8, 0F, 0F, 0F, 0F, -0.3F, 0F, -2.5F, -0.3F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, -0.3F, 0F, -2.5F, -0.3F, 0F, -2.5F, 0F, 0F, 0F); // canisterFarFront
		grenadeModel[2].setRotationPoint(0F, 0F, 0F);

		grenadeModel[3].addShapeBox(2F, -10F, -4F, 2, 14, 10, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // canisterFront
		grenadeModel[3].setRotationPoint(0F, 0F, 0F);

		grenadeModel[4].addBox(0F, -10F, -4F, 2, 14, 10, 0F); // canisterMiddle
		grenadeModel[4].setRotationPoint(0F, 0F, 0F);

		grenadeModel[5].addShapeBox(4F, -12F, -3.5F, 2, 2, 9, 0F, 0F, 0F, 0F, 0.2F, 0F, -2.5F, 0.2F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0.2F, 0F, -2.5F, 0.2F, 0F, -2.5F, 0F, 0F, 0F); // lidTop1
		grenadeModel[5].setRotationPoint(0F, 0F, 0F);

		grenadeModel[6].addShapeBox(2F, -12F, -4.5F, 2, 2, 11, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // lidTop2
		grenadeModel[6].setRotationPoint(0F, 0F, 0F);

		grenadeModel[7].addBox(0F, -12F, -4.5F, 2, 2, 11, 0F); // lidTop3
		grenadeModel[7].setRotationPoint(0F, 0F, 0F);

		grenadeModel[8].addShapeBox(-2F, -12F, -4.5F, 2, 2, 11, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // lidTop4
		grenadeModel[8].setRotationPoint(0F, 0F, 0F);

		grenadeModel[9].addShapeBox(-4F, -12F, -3.5F, 2, 2, 9, 0F, 0.2F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0.2F, 0F, -2.5F, 0.2F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0.2F, 0F, -2.5F); // lidTop5
		grenadeModel[9].setRotationPoint(0F, 0F, 0F);

		grenadeModel[10].addShapeBox(4F, 4F, -3.5F, 2, 4, 9, 0F, 0F, 0F, 0F, 0.2F, 0F, -2.5F, 0.2F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0.2F, 0F, -2.5F, 0.2F, 0F, -2.5F, 0F, 0F, 0F); // lidBottom1
		grenadeModel[10].setRotationPoint(0F, 0F, 0F);

		grenadeModel[11].addShapeBox(-4F, 4F, -3.5F, 2, 4, 9, 0F, 0.2F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0.2F, 0F, -2.5F, 0.2F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0.2F, 0F, -2.5F); // lidBottom5
		grenadeModel[11].setRotationPoint(0F, 0F, 0F);

		grenadeModel[12].addShapeBox(2F, 4F, -4.5F, 2, 4, 11, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // lidBottom2
		grenadeModel[12].setRotationPoint(0F, 0F, 0F);

		grenadeModel[13].addShapeBox(-2F, 4F, -4.5F, 2, 4, 11, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // lidBottom4
		grenadeModel[13].setRotationPoint(0F, 0F, 0F);

		grenadeModel[14].addBox(0F, 4F, -4.5F, 2, 4, 11, 0F); // lidBottom3
		grenadeModel[14].setRotationPoint(0F, 0F, 0F);

		grenadeModel[15].addShapeBox(4F, -13F, -3.5F, 2, 1, 9, 0F, 0F, 0F, -2F, -1F, 0F, -3.5F, -1F, 0F, -3.5F, 0F, 0F, -2F, 0F, 0F, 0F, 0.2F, 0F, -2.5F, 0.2F, 0F, -2.5F, 0F, 0F, 0F); // canisterTop1
		grenadeModel[15].setRotationPoint(0F, 0F, 0F);

		grenadeModel[16].addShapeBox(-4F, -13F, -3.5F, 2, 1, 9, 0F, -1F, 0F, -3.5F, 0F, 0F, -2F, 0F, 0F, -2F, -1F, 0F, -3.5F, 0.2F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0.2F, 0F, -2.5F); // canisterTop5
		grenadeModel[16].setRotationPoint(0F, 0F, 0F);

		grenadeModel[17].addShapeBox(2F, -13F, -4.5F, 2, 1, 11, 0F, 0F, 0F, -1.5F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // canisterTop2
		grenadeModel[17].setRotationPoint(0F, 0F, 0F);

		grenadeModel[18].addShapeBox(-2F, -13F, -4.5F, 2, 1, 11, 0F, 0F, 0F, -3F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -3F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // canisterTop4
		grenadeModel[18].setRotationPoint(0F, 0F, 0F);

		grenadeModel[19].addShapeBox(0F, -13F, -4.5F, 2, 1, 11, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // canisterTop3
		grenadeModel[19].setRotationPoint(0F, 0F, 0F);

		grenadeModel[20].addBox(0F, -15F, -2F, 2, 2, 6, 0F); // cap2
		grenadeModel[20].setRotationPoint(0F, 0F, 0F);

		grenadeModel[21].addShapeBox(-2F, -15F, -2F, 2, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F); // cap1
		grenadeModel[21].setRotationPoint(0F, 0F, 0F);

		grenadeModel[22].addShapeBox(2F, -15F, -2F, 2, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F); // cap3
		grenadeModel[22].setRotationPoint(0F, 0F, 0F);

		grenadeModel[23].addBox(0F, -17F, -3F, 2, 2, 6, 0F); // topPart1
		grenadeModel[23].setRotationPoint(0F, 0F, 0F);

		grenadeModel[24].addBox(0F, -18F, -3F, 2, 1, 4, 0F); // topPart2
		grenadeModel[24].setRotationPoint(0F, 0F, 0F);

		grenadeModel[25].addBox(0.5F, -18.5F, 1.5F, 1, 2, 1, 0F); // pinHolder
		grenadeModel[25].setRotationPoint(0F, 0F, 0F);


		pinModel = new ModelRendererTurbo[7];
		pinModel[0] = new ModelRendererTurbo(this, 56, 8, textureX, textureY); // leverPart1
		pinModel[1] = new ModelRendererTurbo(this, 51, 1, textureX, textureY); // leverPart2
		pinModel[2] = new ModelRendererTurbo(this, 56, 1, textureX, textureY); // leverPart3
		pinModel[3] = new ModelRendererTurbo(this, 51, 13, textureX, textureY); // ring1
		pinModel[4] = new ModelRendererTurbo(this, 61, 1, textureX, textureY); // ring2
		pinModel[5] = new ModelRendererTurbo(this, 51, 13, textureX, textureY); // ring4
		pinModel[6] = new ModelRendererTurbo(this, 61, 1, textureX, textureY); // ring3

		pinModel[0].addShapeBox(0.5F, -17.5F, -5F, 1, 2, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // leverPart1
		pinModel[0].setRotationPoint(0F, 0F, 0F);

		pinModel[1].addShapeBox(0.5F, -17.5F, -6F, 1, 10, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // leverPart2
		pinModel[1].setRotationPoint(0F, 0F, 0F);

		pinModel[2].addShapeBox(0.5F, -8.5F, -6F, 1, 5, 1, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // leverPart3
		pinModel[2].setRotationPoint(0F, 0F, 0F);

		pinModel[3].addShapeBox(-1.5F, -14.5F, -11.5F, 5, 1, 1, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, -1F, -0.5F, 0F, -1F, -0.5F, 0F, 0F, -0.5F, -0.25F, 0F, -0.5F, -0.25F); // ring1
		pinModel[3].setRotationPoint(0F, 0F, 0F);
		pinModel[3].rotateAngleX = -0.78539816F;

		pinModel[4].addShapeBox(-1.5F, -14.5F, -10.75F, 1, 1, 4, 0F, 0F, 0F, 0F, -0.3F, 0F, 0F, -0.3F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, -0.3F, -0.5F, 0F, -0.3F, -0.5F, 0F, 0F, -0.5F, 0F); // ring2
		pinModel[4].setRotationPoint(0F, 0F, 0F);
		pinModel[4].rotateAngleX = -0.78539816F;

		pinModel[5].addShapeBox(-1.5F, -14.5F, -6.75F, 5, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -0.3F, -1F, 0F, -0.3F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -1F, -0.5F, -0.3F, -1F, -0.5F, -0.3F); // ring4
		pinModel[5].setRotationPoint(0F, 0F, 0F);
		pinModel[5].rotateAngleX = -0.78539816F;

		pinModel[6].addShapeBox(2.75F, -14.5F, -10.75F, 1, 1, 4, 0F, 0F, 0F, 0F, -0.3F, 0F, 0F, -0.3F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, -0.3F, -0.5F, 0F, -0.3F, -0.5F, 0F, 0F, -0.5F, 0F); // ring3
		pinModel[6].setRotationPoint(0F, 0F, 0F);
		pinModel[6].rotateAngleX = -0.78539816F;

		for(int i = 0; i <= 25; i++)
			grenadeModel[i].rotateAngleZ = 3.14159265F;

		for(int i = 0; i <= 6; i++)
			pinModel[i].rotateAngleZ = 3.14159265F;
	}

	@Override
	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
	{
		for(ModelRendererTurbo mineModelBit : grenadeModel)
		{
			GL11.glPushMatrix();
			GL11.glScalef(0.33F, 0.33F, 0.33F);
			mineModelBit.render(f5);
			GL11.glPopMatrix();
		}
	}
}