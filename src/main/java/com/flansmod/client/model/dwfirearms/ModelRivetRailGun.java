package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelRivetRailGun extends ModelGun
{
	int textureX = 512;
	int textureY = 256;

	public ModelRivetRailGun()
	{
		gunModel = new ModelRendererTurbo[60];
		gunModel[0] = new ModelRendererTurbo(this, 22, 69, textureX, textureY); // Import barrelGuard1
		gunModel[1] = new ModelRendererTurbo(this, 1, 56, textureX, textureY); // Import barrelGuard2
		gunModel[2] = new ModelRendererTurbo(this, 47, 56, textureX, textureY); // Import barrelGuard3
		gunModel[3] = new ModelRendererTurbo(this, 22, 56, textureX, textureY); // Import barrelGuard4
		gunModel[4] = new ModelRendererTurbo(this, 22, 79, textureX, textureY); // Import barrelGuard5
		gunModel[5] = new ModelRendererTurbo(this, 161, 53, textureX, textureY); // Import body1
		gunModel[6] = new ModelRendererTurbo(this, 161, 66, textureX, textureY); // Import body2
		gunModel[7] = new ModelRendererTurbo(this, 161, 1, textureX, textureY); // Import body3
		gunModel[8] = new ModelRendererTurbo(this, 211, 78, textureX, textureY); // Import body4
		gunModel[9] = new ModelRendererTurbo(this, 188, 78, textureX, textureY); // Import body5
		gunModel[10] = new ModelRendererTurbo(this, 161, 78, textureX, textureY); // Import body6
		gunModel[11] = new ModelRendererTurbo(this, 161, 22, textureX, textureY); // Import body7
		gunModel[12] = new ModelRendererTurbo(this, 198, 22, textureX, textureY); // Import body8
		gunModel[13] = new ModelRendererTurbo(this, 66, 34, textureX, textureY); // Import bolt1
		gunModel[14] = new ModelRendererTurbo(this, 66, 46, textureX, textureY); // Import canister1
		gunModel[15] = new ModelRendererTurbo(this, 14, 28, textureX, textureY); // Import canister2
		gunModel[16] = new ModelRendererTurbo(this, 14, 37, textureX, textureY); // Import canister3
		gunModel[17] = new ModelRendererTurbo(this, 14, 46, textureX, textureY); // Import canister4
		gunModel[18] = new ModelRendererTurbo(this, 206, 45, textureX, textureY); // Import gearPart1
		gunModel[19] = new ModelRendererTurbo(this, 206, 45, textureX, textureY); // Import gearPart1-2
		gunModel[20] = new ModelRendererTurbo(this, 176, 45, textureX, textureY); // Import gearPart2
		gunModel[21] = new ModelRendererTurbo(this, 176, 45, textureX, textureY); // Import gearPart2-2
		gunModel[22] = new ModelRendererTurbo(this, 191, 45, textureX, textureY); // Import gearPart3
		gunModel[23] = new ModelRendererTurbo(this, 191, 45, textureX, textureY); // Import gearPart3-2
		gunModel[24] = new ModelRendererTurbo(this, 161, 45, textureX, textureY); // Import gearPart4
		gunModel[25] = new ModelRendererTurbo(this, 161, 45, textureX, textureY); // Import gearPart4-2
		gunModel[26] = new ModelRendererTurbo(this, 161, 41, textureX, textureY); // Import gearPart5
		gunModel[27] = new ModelRendererTurbo(this, 161, 41, textureX, textureY); // Import gearPart5-2
		gunModel[28] = new ModelRendererTurbo(this, 185, 41, textureX, textureY); // Import gearPart6
		gunModel[29] = new ModelRendererTurbo(this, 185, 41, textureX, textureY); // Import gearPart6-2
		gunModel[30] = new ModelRendererTurbo(this, 209, 41, textureX, textureY); // Import gearPart7
		gunModel[31] = new ModelRendererTurbo(this, 209, 41, textureX, textureY); // Import gearPart7-2
		gunModel[32] = new ModelRendererTurbo(this, 233, 41, textureX, textureY); // Import gearPart8
		gunModel[33] = new ModelRendererTurbo(this, 233, 41, textureX, textureY); // Import gearPart8-2
		gunModel[34] = new ModelRendererTurbo(this, 166, 50, textureX, textureY); // Import gearStub1
		gunModel[35] = new ModelRendererTurbo(this, 166, 50, textureX, textureY); // Import gearStub1-10
		gunModel[36] = new ModelRendererTurbo(this, 166, 50, textureX, textureY); // Import gearStub1-11
		gunModel[37] = new ModelRendererTurbo(this, 166, 50, textureX, textureY); // Import gearStub1-12
		gunModel[38] = new ModelRendererTurbo(this, 166, 50, textureX, textureY); // Import gearStub1-2
		gunModel[39] = new ModelRendererTurbo(this, 166, 50, textureX, textureY); // Import gearStub1-3
		gunModel[40] = new ModelRendererTurbo(this, 166, 50, textureX, textureY); // Import gearStub1-4
		gunModel[41] = new ModelRendererTurbo(this, 166, 50, textureX, textureY); // Import gearStub1-5
		gunModel[42] = new ModelRendererTurbo(this, 166, 50, textureX, textureY); // Import gearStub1-6
		gunModel[43] = new ModelRendererTurbo(this, 166, 50, textureX, textureY); // Import gearStub1-7
		gunModel[44] = new ModelRendererTurbo(this, 166, 50, textureX, textureY); // Import gearStub1-8
		gunModel[45] = new ModelRendererTurbo(this, 166, 50, textureX, textureY); // Import gearStub1-9
		gunModel[46] = new ModelRendererTurbo(this, 161, 49, textureX, textureY); // Import gearStub2
		gunModel[47] = new ModelRendererTurbo(this, 161, 49, textureX, textureY); // Import gearStub2-2
		gunModel[48] = new ModelRendererTurbo(this, 161, 49, textureX, textureY); // Import gearStub2-3
		gunModel[49] = new ModelRendererTurbo(this, 161, 49, textureX, textureY); // Import gearStub2-4
		gunModel[50] = new ModelRendererTurbo(this, 47, 24, textureX, textureY); // Import meter1
		gunModel[51] = new ModelRendererTurbo(this, 47, 34, textureX, textureY); // Import meter1-2
		gunModel[52] = new ModelRendererTurbo(this, 47, 44, textureX, textureY); // Import meter2
		gunModel[53] = new ModelRendererTurbo(this, 3, 37, textureX, textureY); // Import pipe1
		gunModel[54] = new ModelRendererTurbo(this, 1, 48, textureX, textureY); // Import pipe2
		gunModel[55] = new ModelRendererTurbo(this, 3, 42, textureX, textureY); // Import pipe3
		gunModel[56] = new ModelRendererTurbo(this, 44, 9, textureX, textureY); // Import rail1
		gunModel[57] = new ModelRendererTurbo(this, 44, 1, textureX, textureY); // Import rail2
		gunModel[58] = new ModelRendererTurbo(this, 44, 15, textureX, textureY); // Import rail3
		gunModel[59] = new ModelRendererTurbo(this, 44, 15, textureX, textureY); // Import rail3-2

		gunModel[0].addShapeBox(0F, 0F, 0F, 6, 1, 8, 0F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F); // Import barrelGuard1
		gunModel[0].setRotationPoint(38F, -23.5F, -4F);

		gunModel[1].addBox(0F, 0F, 0F, 2, 28, 8, 0F); // Import barrelGuard2
		gunModel[1].setRotationPoint(38F, -22F, -4F);

		gunModel[2].addShapeBox(0F, 0F, 0F, 2, 1, 8, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F); // Import barrelGuard3
		gunModel[2].setRotationPoint(38F, 6.5F, -4F);

		gunModel[3].addBox(0F, 0F, 0F, 4, 4, 8, 0F); // Import barrelGuard4
		gunModel[3].setRotationPoint(40F, -15F, -4F);

		gunModel[4].addShapeBox(0F, 0F, 0F, 4, 1, 8, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F); // Import barrelGuard5
		gunModel[4].setRotationPoint(40F, -10.5F, -4F);

		gunModel[5].addBox(0F, 0F, 0F, 32, 4, 8, 0F); // Import body1
		gunModel[5].setRotationPoint(-12F, -23F, -4F);

		gunModel[6].addShapeBox(0F, 0F, 0F, 32, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F); // Import body2
		gunModel[6].setRotationPoint(-12F, -19F, -4F);

		gunModel[7].addBox(0F, 0F, 0F, 40, 10, 10, 0F); // Import body3
		gunModel[7].setRotationPoint(-12F, -16F, -5F);

		gunModel[8].addShapeBox(0F, 0F, 0F, 3, 4, 8, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import body4
		gunModel[8].setRotationPoint(-15F, -23F, -4F);

		gunModel[9].addShapeBox(0F, 0F, 0F, 3, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F); // Import body5
		gunModel[9].setRotationPoint(-15F, -19F, -4F);

		gunModel[10].addShapeBox(0F, 0F, 0F, 3, 10, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F); // Import body6
		gunModel[10].setRotationPoint(-15F, -16F, -5F);

		gunModel[11].addBox(0F, 0F, 0F, 10, 5, 8, 0F); // Import body7
		gunModel[11].setRotationPoint(28F, -16F, -4F);

		gunModel[12].addShapeBox(0F, 0F, 0F, 10, 1, 8, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F); // Import body8
		gunModel[12].setRotationPoint(28F, -10.5F, -4F);

		gunModel[13].addBox(0F, 0F, 0F, 2, 2, 9, 0F); // Import bolt1
		gunModel[13].setRotationPoint(29F, -14.5F, -4.5F);

		gunModel[14].addBox(0F, 0F, 0F, 6, 6, 2, 0F); // Import canister1
		gunModel[14].setRotationPoint(-9F, -19F, -6.5F);

		gunModel[15].addShapeBox(0F, 0F, 0F, 10, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Import canister2
		gunModel[15].setRotationPoint(-10F, -15F, -11F);

		gunModel[16].addBox(0F, 0F, 0F, 10, 2, 6, 0F); // Import canister3
		gunModel[16].setRotationPoint(-10F, -17F, -11F);

		gunModel[17].addShapeBox(0F, 0F, 0F, 10, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import canister4
		gunModel[17].setRotationPoint(-10F, -19F, -11F);

		gunModel[18].addShapeBox(0F, 0F, 0F, 6, 2, 1, 0F, -1.75F, 0F, 0F, -1.75F, 0F, 0F, -1.75F, 0F, 0F, -1.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import gearPart1
		gunModel[18].setRotationPoint(20F, -16.5F, 5.5F);

		gunModel[19].addShapeBox(0F, 0F, 0F, 6, 2, 1, 0F, -1.75F, 0F, 0F, -1.75F, 0F, 0F, -1.75F, 0F, 0F, -1.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import gearPart1-2
		gunModel[19].setRotationPoint(10F, -14.5F, 5.5F);

		gunModel[20].addShapeBox(0F, 0F, 0F, 6, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import gearPart2
		gunModel[20].setRotationPoint(20F, -14.5F, 5.5F);

		gunModel[21].addShapeBox(0F, 0F, 0F, 6, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import gearPart2-2
		gunModel[21].setRotationPoint(10F, -12.5F, 5.5F);

		gunModel[22].addShapeBox(0F, 0F, 0F, 6, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.75F, 0F, 0F, -1.75F, 0F, 0F, -1.75F, 0F, 0F, -1.75F, 0F, 0F); // Import gearPart3
		gunModel[22].setRotationPoint(20F, -12.5F, 5.5F);

		gunModel[23].addShapeBox(0F, 0F, 0F, 6, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.75F, 0F, 0F, -1.75F, 0F, 0F, -1.75F, 0F, 0F, -1.75F, 0F, 0F); // Import gearPart3-2
		gunModel[23].setRotationPoint(10F, -10.5F, 5.5F);

		gunModel[24].addShapeBox(0F, 0F, 0F, 6, 2, 1, 0F, -2F, -0.75F, 0F, -2F, -0.75F, 0F, -2F, -0.75F, 0F, -2F, -0.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import gearPart4
		gunModel[24].setRotationPoint(20F, -18.5F, 5F);

		gunModel[25].addShapeBox(0F, 0F, 0F, 6, 2, 1, 0F, -2F, -0.75F, 0F, -2F, -0.75F, 0F, -2F, -0.75F, 0F, -2F, -0.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import gearPart4-2
		gunModel[25].setRotationPoint(10F, -16.5F, 5F);

		gunModel[26].addShapeBox(0F, 0F, 0F, 10, 2, 1, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Import gearPart5
		gunModel[26].setRotationPoint(18F, -16.5F, 5F);

		gunModel[27].addShapeBox(0F, 0F, 0F, 10, 2, 1, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Import gearPart5-2
		gunModel[27].setRotationPoint(8F, -14.5F, 5F);

		gunModel[28].addShapeBox(0F, 0F, 0F, 8, 2, 1, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F); // Import gearPart6
		gunModel[28].setRotationPoint(19F, -14.5F, 5F);

		gunModel[29].addShapeBox(0F, 0F, 0F, 8, 2, 1, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F); // Import gearPart6-2
		gunModel[29].setRotationPoint(9F, -12.5F, 5F);

		gunModel[30].addShapeBox(0F, 0F, 0F, 10, 2, 1, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F); // Import gearPart7
		gunModel[30].setRotationPoint(18F, -12.5F, 5F);

		gunModel[31].addShapeBox(0F, 0F, 0F, 10, 2, 1, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F); // Import gearPart7-2
		gunModel[31].setRotationPoint(8F, -10.5F, 5F);

		gunModel[32].addShapeBox(0F, 0F, 0F, 6, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, -0.75F, 0F, -2F, -0.75F, 0F, -2F, -0.75F, 0F, -2F, -0.75F, 0F); // Import gearPart8
		gunModel[32].setRotationPoint(20F, -10.5F, 5F);

		gunModel[33].addShapeBox(0F, 0F, 0F, 6, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, -0.75F, 0F, -2F, -0.75F, 0F, -2F, -0.75F, 0F, -2F, -0.75F, 0F); // Import gearPart8-2
		gunModel[33].setRotationPoint(10F, -8.5F, 5F);

		gunModel[34].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import gearStub1
		gunModel[34].setRotationPoint(26F, -17.8F, 5F);
		gunModel[34].rotateAngleZ = -0.78539816F;

		gunModel[35].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F); // Import gearStub1-10
		gunModel[35].setRotationPoint(15.2F, -8F, 5F);
		gunModel[35].rotateAngleZ = 0.78539816F;

		gunModel[36].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F); // Import gearStub1-11
		gunModel[36].setRotationPoint(12F, -7.3F, 5F);

		gunModel[37].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F); // Import gearStub1-12
		gunModel[37].setRotationPoint(9.3F, -9.5F, 5F);
		gunModel[37].rotateAngleZ = -0.78539816F;

		gunModel[38].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import gearStub1-2
		gunModel[38].setRotationPoint(22F, -18.7F, 5F);

		gunModel[39].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import gearStub1-3
		gunModel[39].setRotationPoint(19F, -16.7F, 5F);
		gunModel[39].rotateAngleZ = 0.6981317F;

		gunModel[40].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F); // Import gearStub1-4
		gunModel[40].setRotationPoint(25.2F, -10F, 5F);
		gunModel[40].rotateAngleZ = 0.78539816F;

		gunModel[41].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F); // Import gearStub1-5
		gunModel[41].setRotationPoint(22F, -9.3F, 5F);

		gunModel[42].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F); // Import gearStub1-6
		gunModel[42].setRotationPoint(19.3F, -11.5F, 5F);
		gunModel[42].rotateAngleZ = -0.78539816F;

		gunModel[43].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import gearStub1-7
		gunModel[43].setRotationPoint(16F, -15.8F, 5F);
		gunModel[43].rotateAngleZ = -0.78539816F;

		gunModel[44].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import gearStub1-8
		gunModel[44].setRotationPoint(12F, -16.7F, 5F);

		gunModel[45].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import gearStub1-9
		gunModel[45].setRotationPoint(9F, -14.7F, 5F);
		gunModel[45].rotateAngleZ = 0.6981317F;

		gunModel[46].addShapeBox(0F, 0F, 0F, 1, 2, 1, 0F, 0F, 0F, 0F, 0.25F, -0.3F, 0F, 0.25F, -0.3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.25F, -0.3F, 0F, 0.25F, -0.3F, 0F, 0F, 0F, 0F); // Import gearStub2
		gunModel[46].setRotationPoint(27.5F, -14.5F, 5F);

		gunModel[47].addShapeBox(0F, 0F, 0F, 1, 2, 1, 0F, 0.25F, -0.3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.25F, -0.3F, 0F, 0.25F, -0.3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.25F, -0.3F, 0F); // Import gearStub2-2
		gunModel[47].setRotationPoint(17.5F, -14.5F, 5F);

		gunModel[48].addShapeBox(0F, 0F, 0F, 1, 2, 1, 0F, 0F, 0F, 0F, 0.25F, -0.3F, 0F, 0.25F, -0.3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.25F, -0.3F, 0F, 0.25F, -0.3F, 0F, 0F, 0F, 0F); // Import gearStub2-3
		gunModel[48].setRotationPoint(17.5F, -12.5F, 5F);

		gunModel[49].addShapeBox(0F, 0F, 0F, 1, 2, 1, 0F, 0.25F, -0.3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.25F, -0.3F, 0F, 0.25F, -0.3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.25F, -0.3F, 0F); // Import gearStub2-4
		gunModel[49].setRotationPoint(7.5F, -12.5F, 5F);

		gunModel[50].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Import meter1
		gunModel[50].setRotationPoint(-8F, -21F, -11F);

		gunModel[51].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import meter1-2
		gunModel[51].setRotationPoint(-8F, -26F, -11F);

		gunModel[52].addBox(0F, 0F, 0F, 2, 3, 7, 0F); // Import meter2
		gunModel[52].setRotationPoint(-8F, -24F, -11F);

		gunModel[53].addBox(0F, 0F, 0F, 3, 2, 2, 0F); // Import pipe1
		gunModel[53].setRotationPoint(0F, -17F, -9F);

		gunModel[54].addBox(0F, 0F, 0F, 2, 2, 4, 0F); // Import pipe2
		gunModel[54].setRotationPoint(3F, -17F, -9F);

		gunModel[55].addBox(0F, 0F, 0F, 4, 4, 1, 0F); // Import pipe3
		gunModel[55].setRotationPoint(2F, -18F, -5.5F);

		gunModel[56].addBox(0F, 0F, 0F, 18, 1, 4, 0F); // Import rail1
		gunModel[56].setRotationPoint(1F, -24F, -2F);

		gunModel[57].addBox(0F, 0F, 0F, 18, 1, 6, 0F); // Import rail2
		gunModel[57].setRotationPoint(1F, -25F, -3F);

		gunModel[58].addShapeBox(0F, 0F, 0F, 18, 1, 1, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import rail3
		gunModel[58].setRotationPoint(1F, -26F, 2F);

		gunModel[59].addShapeBox(0F, 0F, 0F, 18, 1, 1, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import rail3-2
		gunModel[59].setRotationPoint(1F, -26F, -3F);


		defaultBarrelModel = new ModelRendererTurbo[3];
		defaultBarrelModel[0] = new ModelRendererTurbo(this, 96, 22, textureX, textureY); // Import mainBarrelBottom
		defaultBarrelModel[1] = new ModelRendererTurbo(this, 96, 11, textureX, textureY); // Import mainBarrelMiddle
		defaultBarrelModel[2] = new ModelRendererTurbo(this, 96, 1, textureX, textureY); // Import mainBarrelTop

		defaultBarrelModel[0].addShapeBox(0F, 0F, 0F, 24, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Import mainBarrelBottom
		defaultBarrelModel[0].setRotationPoint(20F, -17F, -3.5F);

		defaultBarrelModel[1].addBox(0F, 0F, 0F, 24, 3, 7, 0F); // Import mainBarrelMiddle
		defaultBarrelModel[1].setRotationPoint(20F, -20F, -3.5F);

		defaultBarrelModel[2].addShapeBox(0F, 0F, 0F, 24, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import mainBarrelTop
		defaultBarrelModel[2].setRotationPoint(20F, -22F, -3.5F);


		defaultStockModel = new ModelRendererTurbo[26];
		defaultStockModel[0] = new ModelRendererTurbo(this, 66, 34, textureX, textureY); // Import bolt2
		defaultStockModel[1] = new ModelRendererTurbo(this, 96, 40, textureX, textureY); // Import pipeStock1
		defaultStockModel[2] = new ModelRendererTurbo(this, 96, 40, textureX, textureY); // Import pipeStock1-2
		defaultStockModel[3] = new ModelRendererTurbo(this, 96, 46, textureX, textureY); // Import pipeStock2
		defaultStockModel[4] = new ModelRendererTurbo(this, 115, 53, textureX, textureY); // Import stockCanister1
		defaultStockModel[5] = new ModelRendererTurbo(this, 115, 63, textureX, textureY); // Import stockCanister2
		defaultStockModel[6] = new ModelRendererTurbo(this, 115, 74, textureX, textureY); // Import stockCanister3
		defaultStockModel[7] = new ModelRendererTurbo(this, 96, 74, textureX, textureY); // Import stockCanister4
		defaultStockModel[8] = new ModelRendererTurbo(this, 96, 53, textureX, textureY); // Import stockCanister5
		defaultStockModel[9] = new ModelRendererTurbo(this, 96, 64, textureX, textureY); // Import stockCanister6
		defaultStockModel[10] = new ModelRendererTurbo(this, 115, 84, textureX, textureY); // Import stockCanister7
		defaultStockModel[11] = new ModelRendererTurbo(this, 115, 84, textureX, textureY); // Import stockCanister7-2
		defaultStockModel[12] = new ModelRendererTurbo(this, 115, 84, textureX, textureY); // Import stockCanister7-3
		defaultStockModel[13] = new ModelRendererTurbo(this, 142, 87, textureX, textureY); // Import stockPart1
		defaultStockModel[14] = new ModelRendererTurbo(this, 123, 90, textureX, textureY); // Import stockPart2
		defaultStockModel[15] = new ModelRendererTurbo(this, 96, 84, textureX, textureY); // Import stockPart3
		defaultStockModel[16] = new ModelRendererTurbo(this, 109, 110, textureX, textureY); // Import strap1
		defaultStockModel[17] = new ModelRendererTurbo(this, 109, 110, textureX, textureY); // Import strap1-2
		defaultStockModel[18] = new ModelRendererTurbo(this, 117, 127, textureX, textureY); // Import strap2
		defaultStockModel[19] = new ModelRendererTurbo(this, 117, 127, textureX, textureY); // Import strap2-2
		defaultStockModel[20] = new ModelRendererTurbo(this, 117, 127, textureX, textureY); // Import strap2-3
		defaultStockModel[21] = new ModelRendererTurbo(this, 117, 127, textureX, textureY); // Import strap2-4
		defaultStockModel[22] = new ModelRendererTurbo(this, 96, 121, textureX, textureY); // Import strap3
		defaultStockModel[23] = new ModelRendererTurbo(this, 96, 121, textureX, textureY); // Import strap3-2
		defaultStockModel[24] = new ModelRendererTurbo(this, 96, 109, textureX, textureY); // Import strap4
		defaultStockModel[25] = new ModelRendererTurbo(this, 96, 109, textureX, textureY); // Import strap4-2

		defaultStockModel[0].addBox(0F, 0F, 0F, 2, 2, 9, 0F); // Import bolt2
		defaultStockModel[0].setRotationPoint(-40F, -19F, -4.5F);

		defaultStockModel[1].addShapeBox(0F, 0F, 0F, 22, 1, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import pipeStock1
		defaultStockModel[1].setRotationPoint(-37F, -20F, -2F);

		defaultStockModel[2].addShapeBox(0F, 0F, 0F, 22, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Import pipeStock1-2
		defaultStockModel[2].setRotationPoint(-37F, -17F, -2F);

		defaultStockModel[3].addBox(0F, 0F, 0F, 22, 2, 4, 0F); // Import pipeStock2
		defaultStockModel[3].setRotationPoint(-37F, -19F, -2F);

		defaultStockModel[4].addShapeBox(0F, 0F, 0F, 13, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import stockCanister1
		defaultStockModel[4].setRotationPoint(-36F, -16F, -3.5F);

		defaultStockModel[5].addBox(0F, 0F, 0F, 13, 3, 7, 0F); // Import stockCanister2
		defaultStockModel[5].setRotationPoint(-36F, -14F, -3.5F);

		defaultStockModel[6].addShapeBox(0F, 0F, 0F, 13, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Import stockCanister3
		defaultStockModel[6].setRotationPoint(-36F, -11F, -3.5F);

		defaultStockModel[7].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, -2F, 0F, -2F, -2F, 0F, -2F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F); // Import stockCanister4
		defaultStockModel[7].setRotationPoint(-23F, -16F, -3.5F);

		defaultStockModel[8].addShapeBox(0F, 0F, 0F, 2, 3, 7, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F); // Import stockCanister5
		defaultStockModel[8].setRotationPoint(-23F, -14F, -3.5F);

		defaultStockModel[9].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, -2F, -2F, 0F, -2F, -2F, 0F, 0F, -2F); // Import stockCanister6
		defaultStockModel[9].setRotationPoint(-23F, -11F, -3.5F);

		defaultStockModel[10].addShapeBox(0F, 0F, 0F, 5, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import stockCanister7
		defaultStockModel[10].setRotationPoint(-21F, -14F, -1.5F);

		defaultStockModel[11].addBox(0F, 0F, 0F, 5, 1, 3, 0F); // Import stockCanister7-2
		defaultStockModel[11].setRotationPoint(-21F, -13F, -1.5F);

		defaultStockModel[12].addShapeBox(0F, 0F, 0F, 5, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Import stockCanister7-3
		defaultStockModel[12].setRotationPoint(-21F, -12F, -1.5F);

		defaultStockModel[13].addBox(0F, 0F, 0F, 1, 5, 5, 0F); // Import stockPart1
		defaultStockModel[13].setRotationPoint(-16F, -15F, -2.5F);

		defaultStockModel[14].addBox(0F, 0F, 0F, 5, 10, 8, 0F); // Import stockPart2
		defaultStockModel[14].setRotationPoint(-42F, -21F, -4F);

		defaultStockModel[15].addShapeBox(0F, 0F, 0F, 5, 16, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F); // Import stockPart3
		defaultStockModel[15].setRotationPoint(-42F, -11F, -4F);

		defaultStockModel[16].addShapeBox(0F, 0F, 0F, 2, 1, 5, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import strap1
		defaultStockModel[16].setRotationPoint(-34F, -20.5F, -2.5F);

		defaultStockModel[17].addShapeBox(0F, 0F, 0F, 2, 1, 5, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import strap1-2
		defaultStockModel[17].setRotationPoint(-27F, -20.5F, -2.5F);

		defaultStockModel[18].addShapeBox(0F, 0F, 0F, 2, 5, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1.5F, 0F, 0F, 1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F); // Import strap2
		defaultStockModel[18].setRotationPoint(-34F, -19.5F, -2.5F);

		defaultStockModel[19].addShapeBox(0F, 0F, 0F, 2, 5, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 1.5F, 0F, 0F, 1.5F); // Import strap2-2
		defaultStockModel[19].setRotationPoint(-34F, -19.5F, 1.5F);

		defaultStockModel[20].addShapeBox(0F, 0F, 0F, 2, 5, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 1.5F, 0F, 0F, 1.5F); // Import strap2-3
		defaultStockModel[20].setRotationPoint(-27F, -19.5F, 1.5F);

		defaultStockModel[21].addShapeBox(0F, 0F, 0F, 2, 5, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1.5F, 0F, 0F, 1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F); // Import strap2-4
		defaultStockModel[21].setRotationPoint(-27F, -19.5F, -2.5F);

		defaultStockModel[22].addBox(0F, 0F, 0F, 2, 4, 8, 0F); // Import strap3
		defaultStockModel[22].setRotationPoint(-34F, -14.5F, -4F);

		defaultStockModel[23].addBox(0F, 0F, 0F, 2, 4, 8, 0F); // Import strap3-2
		defaultStockModel[23].setRotationPoint(-27F, -14.5F, -4F);

		defaultStockModel[24].addShapeBox(0F, 0F, 0F, 2, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Import strap4
		defaultStockModel[24].setRotationPoint(-34F, -10.5F, -4F);

		defaultStockModel[25].addShapeBox(0F, 0F, 0F, 2, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Import strap4-2
		defaultStockModel[25].setRotationPoint(-27F, -10.5F, -4F);


		defaultGripModel = new ModelRendererTurbo[2];
		defaultGripModel[0] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Import grip1
		defaultGripModel[1] = new ModelRendererTurbo(this, 1, 140, textureX, textureY); // Import grip2

		defaultGripModel[0].addShapeBox(0F, 0F, 0F, 11, 16, 10, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, -2F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, -2F, 0F); // Import grip1
		defaultGripModel[0].setRotationPoint(-14F, -3F, -5F);

		defaultGripModel[1].addBox(0F, 0F, 0F, 11, 3, 10, 0F); // Import grip2
		defaultGripModel[1].setRotationPoint(-12F, -6F, -5F);


		ammoModel = new ModelRendererTurbo[6];
		ammoModel[0] = new ModelRendererTurbo(this, 1, 122, textureX, textureY); // Import ammo1
		ammoModel[1] = new ModelRendererTurbo(this, 1, 93, textureX, textureY); // Import ammo2
		ammoModel[2] = new ModelRendererTurbo(this, 1, 110, textureX, textureY); // Import ammo3
		ammoModel[3] = new ModelRendererTurbo(this, 36, 132, textureX, textureY); // Import spike
		ammoModel[4] = new ModelRendererTurbo(this, 1, 132, textureX, textureY); // Import spike2
		ammoModel[5] = new ModelRendererTurbo(this, 53, 132, textureX, textureY); // Import spike3

		ammoModel[0].addBox(0F, 0F, 0F, 21, 1, 8, 0F); // Import ammo1
		ammoModel[0].setRotationPoint(6F, -6F, -4F);

		ammoModel[1].addBox(0F, 0F, 0F, 21, 6, 10, 0F); // Import ammo2
		ammoModel[1].setRotationPoint(6F, -5F, -5F);

		ammoModel[2].addShapeBox(0F, 0F, 0F, 21, 1, 10, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F); // Import ammo3
		ammoModel[2].setRotationPoint(6F, 1.5F, -5F);

		ammoModel[3].addShapeBox(0F, 0F, 0F, 4, 1, 4, 0F, 0F, 0F, -1F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Import spike
		ammoModel[3].setRotationPoint(21.5F, -7F, -1.5F);

		ammoModel[4].addShapeBox(0F, 0F, 0F, 13, 1, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import spike2
		ammoModel[4].setRotationPoint(8.5F, -7F, -1.5F);

		ammoModel[5].addShapeBox(0F, 0F, 0F, 1, 1, 5, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import spike3
		ammoModel[5].setRotationPoint(7.5F, -7F, -2.5F);


		slideModel = new ModelRendererTurbo[2];
		slideModel[0] = new ModelRendererTurbo(this, 1, 28, textureX, textureY); // Import slider1
		slideModel[1] = new ModelRendererTurbo(this, 9, 28, textureX, textureY); // Import slider2

		slideModel[0].addBox(0F, 0F, 0F, 2, 2, 3, 0F); // Import slider1
		slideModel[0].setRotationPoint(15.5F, -22.5F, 5F);

		slideModel[1].addBox(0F, 0F, 0F, 1, 1, 1, 0F); // Import slider2
		slideModel[1].setRotationPoint(16F, -22F, 4F);

		stockAttachPoint = new Vector3f(-15F /16F, 15F /16F, 0F /16F);
		scopeAttachPoint = new Vector3f(11F /16F, 23F /16F, 0F /16F);

		gunSlideDistance = 1F;
		animationType = EnumAnimationType.BOTTOM_CLIP;


		translateAll(0F, 0F, 0F);


		flipAll();
	}
}