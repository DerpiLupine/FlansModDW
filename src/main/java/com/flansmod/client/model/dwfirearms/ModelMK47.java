package com.flansmod.client.model.dwfirearms; //Path where the model is located

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelMK47 extends ModelGun //Same as Filename
{
	int textureX = 512;
	int textureY = 256;

	public ModelMK47() //Same as Filename
	{
		gunModel = new ModelRendererTurbo[32];
		gunModel[0] = new ModelRendererTurbo(this, 96, 1, textureX, textureY); // barrel1
		gunModel[1] = new ModelRendererTurbo(this, 96, 10, textureX, textureY); // barrel2
		gunModel[2] = new ModelRendererTurbo(this, 96, 1, textureX, textureY); // barrel3
		gunModel[3] = new ModelRendererTurbo(this, 96, 61, textureX, textureY); // body1
		gunModel[4] = new ModelRendererTurbo(this, 96, 91, textureX, textureY); // body2
		gunModel[5] = new ModelRendererTurbo(this, 96, 164, textureX, textureY); // body3
		gunModel[6] = new ModelRendererTurbo(this, 210, 61, textureX, textureY); // body4
		gunModel[7] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // ironSightBack
		gunModel[8] = new ModelRendererTurbo(this, 35, 9, textureX, textureY); // rail1
		gunModel[9] = new ModelRendererTurbo(this, 35, 1, textureX, textureY); // rail2
		gunModel[10] = new ModelRendererTurbo(this, 35, 15, textureX, textureY); // rail3
		gunModel[11] = new ModelRendererTurbo(this, 35, 15, textureX, textureY); // rail3-2
		gunModel[12] = new ModelRendererTurbo(this, 1, 51, textureX, textureY); // canister1
		gunModel[13] = new ModelRendererTurbo(this, 1, 33, textureX, textureY); // canister2
		gunModel[14] = new ModelRendererTurbo(this, 1, 24, textureX, textureY); // canister3
		gunModel[15] = new ModelRendererTurbo(this, 1, 42, textureX, textureY); // canister4
		gunModel[16] = new ModelRendererTurbo(this, 34, 24, textureX, textureY); // meter1
		gunModel[17] = new ModelRendererTurbo(this, 34, 34, textureX, textureY); // meter1-2
		gunModel[18] = new ModelRendererTurbo(this, 34, 44, textureX, textureY); // meter2
		gunModel[19] = new ModelRendererTurbo(this, 18, 58, textureX, textureY); // pipe1
		gunModel[20] = new ModelRendererTurbo(this, 18, 51, textureX, textureY); // pipe2
		gunModel[21] = new ModelRendererTurbo(this, 1, 60, textureX, textureY); // pipe3
		gunModel[22] = new ModelRendererTurbo(this, 96, 44, textureX, textureY); // Box 15
		gunModel[23] = new ModelRendererTurbo(this, 185, 31, textureX, textureY); // Box 16
		gunModel[24] = new ModelRendererTurbo(this, 185, 36, textureX, textureY); // Box 17
		gunModel[25] = new ModelRendererTurbo(this, 138, 23, textureX, textureY); // Box 19
		gunModel[26] = new ModelRendererTurbo(this, 53, 27, textureX, textureY); // Box 20
		gunModel[27] = new ModelRendererTurbo(this, 53, 24, textureX, textureY); // Box 21
		gunModel[28] = new ModelRendererTurbo(this, 163, 49, textureX, textureY); // Box 112
		gunModel[29] = new ModelRendererTurbo(this, 163, 44, textureX, textureY); // Box 61
		gunModel[30] = new ModelRendererTurbo(this, 154, 44, textureX, textureY); // Box 62
		gunModel[31] = new ModelRendererTurbo(this, 137, 44, textureX, textureY); // Box 63

		gunModel[0].addShapeBox(0F, 0F, 0F, 50, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // barrel1
		gunModel[0].setRotationPoint(33F, -21F, -3F);

		gunModel[1].addBox(0F, 0F, 0F, 50, 2, 6, 0F); // barrel2
		gunModel[1].setRotationPoint(33F, -19F, -3F);

		gunModel[2].addShapeBox(0F, 0F, 0F, 50, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // barrel3
		gunModel[2].setRotationPoint(33F, -17F, -3F);

		gunModel[3].addBox(0F, 0F, 0F, 22, 10, 8, 0F); // body1
		gunModel[3].setRotationPoint(-8F, -19F, -4F);

		gunModel[4].addShapeBox(0F, 0F, 0F, 41, 3, 8, 0F, -2F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, -2F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body2
		gunModel[4].setRotationPoint(-8F, -22F, -4F);

		gunModel[5].addShapeBox(0F, 0F, 0F, 41, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // body3
		gunModel[5].setRotationPoint(-8F, -9F, -4F);

		gunModel[6].addBox(0F, 0F, 0F, 5, 10, 8, 0F); // body4
		gunModel[6].setRotationPoint(28F, -19F, -4F);

		gunModel[7].addBox(0F, 0F, 0F, 6, 2, 4, 0F); // ironSightBack
		gunModel[7].setRotationPoint(-4F, -23F, -2F);

		gunModel[8].addBox(0F, 0F, 0F, 18, 1, 4, 0F); // rail1
		gunModel[8].setRotationPoint(7F, -23F, -2F);

		gunModel[9].addBox(0F, 0F, 0F, 18, 1, 6, 0F); // rail2
		gunModel[9].setRotationPoint(7F, -24F, -3F);

		gunModel[10].addShapeBox(0F, 0F, 0F, 18, 1, 1, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // rail3
		gunModel[10].setRotationPoint(7F, -25F, 2F);

		gunModel[11].addShapeBox(0F, 0F, 0F, 18, 1, 1, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // rail3-2
		gunModel[11].setRotationPoint(7F, -25F, -3F);

		gunModel[12].addBox(0F, 0F, 0F, 6, 6, 2, 0F); // canister1
		gunModel[12].setRotationPoint(-4F, -20F, -6F);

		gunModel[13].addShapeBox(0F, 0F, 0F, 10, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // canister2
		gunModel[13].setRotationPoint(-5F, -16F, -10.5F);

		gunModel[14].addBox(0F, 0F, 0F, 10, 2, 6, 0F); // canister3
		gunModel[14].setRotationPoint(-5F, -18F, -10.5F);

		gunModel[15].addShapeBox(0F, 0F, 0F, 10, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // canister4
		gunModel[15].setRotationPoint(-5F, -20F, -10.5F);

		gunModel[16].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // meter1
		gunModel[16].setRotationPoint(-3F, -22F, -9.5F);

		gunModel[17].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // meter1-2
		gunModel[17].setRotationPoint(-3F, -27F, -9.5F);

		gunModel[18].addBox(0F, 0F, 0F, 2, 3, 7, 0F); // meter2
		gunModel[18].setRotationPoint(-3F, -25F, -9.5F);

		gunModel[19].addBox(0F, 0F, 0F, 3, 2, 2, 0F); // pipe1
		gunModel[19].setRotationPoint(5F, -18F, -8.5F);

		gunModel[20].addBox(0F, 0F, 0F, 2, 2, 4, 0F); // pipe2
		gunModel[20].setRotationPoint(8F, -18F, -8.5F);

		gunModel[21].addBox(0F, 0F, 0F, 4, 4, 1, 0F); // pipe3
		gunModel[21].setRotationPoint(7F, -19F, -5F);

		gunModel[22].addBox(0F, 0F, 0F, 14, 10, 6, 0F); // Box 15
		gunModel[22].setRotationPoint(14F, -19F, -2F);

		gunModel[23].addBox(0F, 0F, 0F, 14, 2, 2, 0F); // Box 16
		gunModel[23].setRotationPoint(14F, -19F, -4F);

		gunModel[24].addBox(0F, 0F, 0F, 14, 5, 2, 0F); // Box 17
		gunModel[24].setRotationPoint(14F, -14F, -4F);

		gunModel[25].addBox(0F, 0F, 0F, 2, 3, 2, 0F); // Box 19
		gunModel[25].setRotationPoint(26F, -17F, -4F);

		gunModel[26].addShapeBox(0F, 0F, 0F, 12, 2, 1, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 20
		gunModel[26].setRotationPoint(14F, -14F, -5F);

		gunModel[27].addShapeBox(0F, 0F, 0F, 12, 1, 1, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -0.25F, -1F, 0F, -0.25F, -1F, 0F, -0.25F, -1F, 0F, -0.25F); // Box 21
		gunModel[27].setRotationPoint(14F, -12F, -5.5F);

		gunModel[28].addShapeBox(0F, 0F, 0F, 16, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, 0F); // Box 112
		gunModel[28].setRotationPoint(13F, -19F, 4F);

		gunModel[29].addShapeBox(0F, 0F, 0F, 16, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, 0F); // Box 61
		gunModel[29].setRotationPoint(13F, -13F, 4F);

		gunModel[30].addBox(0F, 0F, 0F, 1, 5, 3, 0F); // Box 62
		gunModel[30].setRotationPoint(13F, -18F, 4F);

		gunModel[31].addBox(0F, 0F, 0F, 1, 5, 7, 0F); // Box 63
		gunModel[31].setRotationPoint(28F, -18F, 4F);


		defaultBarrelModel = new ModelRendererTurbo[6];
		defaultBarrelModel[0] = new ModelRendererTurbo(this, 221, 1, textureX, textureY); // muzzle
		defaultBarrelModel[1] = new ModelRendererTurbo(this, 221, 1, textureX, textureY); // muzzle
		defaultBarrelModel[2] = new ModelRendererTurbo(this, 239, 11, textureX, textureY); // muzzle2
		defaultBarrelModel[3] = new ModelRendererTurbo(this, 230, 11, textureX, textureY); // muzzle2
		defaultBarrelModel[4] = new ModelRendererTurbo(this, 221, 11, textureX, textureY); // muzzle2
		defaultBarrelModel[5] = new ModelRendererTurbo(this, 248, 11, textureX, textureY); // muzzle2

		defaultBarrelModel[0].addShapeBox(0F, 0F, 0F, 11, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // muzzle
		defaultBarrelModel[0].setRotationPoint(83F, -21.5F, -3.5F);

		defaultBarrelModel[1].addShapeBox(0F, 0F, 0F, 11, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // muzzle
		defaultBarrelModel[1].setRotationPoint(83F, -16.5F, -3.5F);

		defaultBarrelModel[2].addBox(0F, 0F, 0F, 2, 3, 2, 0F); // muzzle2
		defaultBarrelModel[2].setRotationPoint(83F, -19.5F, -3.5F);

		defaultBarrelModel[3].addBox(0F, 0F, 0F, 2, 3, 2, 0F); // muzzle2
		defaultBarrelModel[3].setRotationPoint(92F, -19.5F, -3.5F);

		defaultBarrelModel[4].addBox(0F, 0F, 0F, 2, 3, 2, 0F); // muzzle2
		defaultBarrelModel[4].setRotationPoint(83F, -19.5F, 1.5F);

		defaultBarrelModel[5].addBox(0F, 0F, 0F, 2, 3, 2, 0F); // muzzle2
		defaultBarrelModel[5].setRotationPoint(92F, -19.5F, 1.5F);


		defaultScopeModel = new ModelRendererTurbo[28];
		defaultScopeModel[0] = new ModelRendererTurbo(this, 1, 122, textureX, textureY); // Box 0
		defaultScopeModel[1] = new ModelRendererTurbo(this, 68, 113, textureX, textureY); // knob1
		defaultScopeModel[2] = new ModelRendererTurbo(this, 68, 113, textureX, textureY); // knob2
		defaultScopeModel[3] = new ModelRendererTurbo(this, 68, 113, textureX, textureY); // knob3
		defaultScopeModel[4] = new ModelRendererTurbo(this, 1, 148, textureX, textureY); // scopeMain1
		defaultScopeModel[5] = new ModelRendererTurbo(this, 1, 148, textureX, textureY); // Box 70
		defaultScopeModel[6] = new ModelRendererTurbo(this, 1, 133, textureX, textureY); // Box 71
		defaultScopeModel[7] = new ModelRendererTurbo(this, 1, 162, textureX, textureY); // Box 72
		defaultScopeModel[8] = new ModelRendererTurbo(this, 1, 162, textureX, textureY); // Box 73
		defaultScopeModel[9] = new ModelRendererTurbo(this, 1, 162, textureX, textureY); // Box 74
		defaultScopeModel[10] = new ModelRendererTurbo(this, 62, 148, textureX, textureY); // Box 75
		defaultScopeModel[11] = new ModelRendererTurbo(this, 62, 133, textureX, textureY); // Box 76
		defaultScopeModel[12] = new ModelRendererTurbo(this, 62, 148, textureX, textureY); // Box 77
		defaultScopeModel[13] = new ModelRendererTurbo(this, 62, 148, textureX, textureY); // Box 78
		defaultScopeModel[14] = new ModelRendererTurbo(this, 62, 133, textureX, textureY); // Box 79
		defaultScopeModel[15] = new ModelRendererTurbo(this, 62, 148, textureX, textureY); // Box 80
		defaultScopeModel[16] = new ModelRendererTurbo(this, 1, 175, textureX, textureY); // Box 81
		defaultScopeModel[17] = new ModelRendererTurbo(this, 1, 175, textureX, textureY); // Box 82
		defaultScopeModel[18] = new ModelRendererTurbo(this, 1, 175, textureX, textureY); // Box 83
		defaultScopeModel[19] = new ModelRendererTurbo(this, 62, 148, textureX, textureY); // Box 84
		defaultScopeModel[20] = new ModelRendererTurbo(this, 62, 133, textureX, textureY); // Box 85
		defaultScopeModel[21] = new ModelRendererTurbo(this, 62, 148, textureX, textureY); // Box 86
		defaultScopeModel[22] = new ModelRendererTurbo(this, 62, 148, textureX, textureY); // Box 87
		defaultScopeModel[23] = new ModelRendererTurbo(this, 62, 133, textureX, textureY); // Box 88
		defaultScopeModel[24] = new ModelRendererTurbo(this, 62, 148, textureX, textureY); // Box 89
		defaultScopeModel[25] = new ModelRendererTurbo(this, 44, 124, textureX, textureY); // Box 93
		defaultScopeModel[26] = new ModelRendererTurbo(this, 1, 188, textureX, textureY); // Box 94
		defaultScopeModel[27] = new ModelRendererTurbo(this, 1, 188, textureX, textureY); // Box 95

		defaultScopeModel[0].addBox(0F, 0F, 0F, 14, 3, 7, 0F); // Box 0
		defaultScopeModel[0].setRotationPoint(8F, -25F, -3.5F);

		defaultScopeModel[1].addShapeBox(0F, 0F, 0F, 2, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F); // knob1
		defaultScopeModel[1].setRotationPoint(12F, -38F, -3F);

		defaultScopeModel[2].addBox(0F, 0F, 0F, 2, 2, 6, 0F); // knob2
		defaultScopeModel[2].setRotationPoint(14F, -38F, -3F);

		defaultScopeModel[3].addShapeBox(0F, 0F, 0F, 2, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F); // knob3
		defaultScopeModel[3].setRotationPoint(16F, -38F, -3F);

		defaultScopeModel[4].addShapeBox(0F, 0F, 0F, 20, 3, 10, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scopeMain1
		defaultScopeModel[4].setRotationPoint(5F, -37F, -5F);

		defaultScopeModel[5].addShapeBox(0F, 0F, 0F, 20, 3, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 70
		defaultScopeModel[5].setRotationPoint(5F, -30F, -5F);

		defaultScopeModel[6].addBox(0F, 0F, 0F, 20, 4, 10, 0F); // Box 71
		defaultScopeModel[6].setRotationPoint(5F, -34F, -5F);

		defaultScopeModel[7].addShapeBox(0F, 0F, 0F, 20, 3, 9, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 72
		defaultScopeModel[7].setRotationPoint(25F, -36.5F, -4.5F);

		defaultScopeModel[8].addBox(0F, 0F, 0F, 20, 3, 9, 0F); // Box 73
		defaultScopeModel[8].setRotationPoint(25F, -33.5F, -4.5F);

		defaultScopeModel[9].addShapeBox(0F, 0F, 0F, 20, 3, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 74
		defaultScopeModel[9].setRotationPoint(25F, -30.5F, -4.5F);

		defaultScopeModel[10].addShapeBox(0F, 0F, 0F, 2, 3, 10, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 75
		defaultScopeModel[10].setRotationPoint(41F, -37F, -5F);

		defaultScopeModel[11].addBox(0F, 0F, 0F, 2, 4, 10, 0F); // Box 76
		defaultScopeModel[11].setRotationPoint(41F, -34F, -5F);

		defaultScopeModel[12].addShapeBox(0F, 0F, 0F, 2, 3, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 77
		defaultScopeModel[12].setRotationPoint(41F, -30F, -5F);

		defaultScopeModel[13].addShapeBox(0F, 0F, 0F, 2, 3, 10, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 78
		defaultScopeModel[13].setRotationPoint(33F, -37F, -5F);

		defaultScopeModel[14].addBox(0F, 0F, 0F, 2, 4, 10, 0F); // Box 79
		defaultScopeModel[14].setRotationPoint(33F, -34F, -5F);

		defaultScopeModel[15].addShapeBox(0F, 0F, 0F, 2, 3, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 80
		defaultScopeModel[15].setRotationPoint(33F, -30F, -5F);

		defaultScopeModel[16].addShapeBox(0F, 0F, 0F, 10, 3, 9, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 81
		defaultScopeModel[16].setRotationPoint(-5F, -36.5F, -4.5F);

		defaultScopeModel[17].addBox(0F, 0F, 0F, 10, 3, 9, 0F); // Box 82
		defaultScopeModel[17].setRotationPoint(-5F, -33.5F, -4.5F);

		defaultScopeModel[18].addShapeBox(0F, 0F, 0F, 10, 3, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 83
		defaultScopeModel[18].setRotationPoint(-5F, -30.5F, -4.5F);

		defaultScopeModel[19].addShapeBox(0F, 0F, 0F, 2, 3, 10, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 84
		defaultScopeModel[19].setRotationPoint(-4.5F, -37F, -5F);

		defaultScopeModel[20].addBox(0F, 0F, 0F, 2, 4, 10, 0F); // Box 85
		defaultScopeModel[20].setRotationPoint(-4.5F, -34F, -5F);

		defaultScopeModel[21].addShapeBox(0F, 0F, 0F, 2, 3, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 86
		defaultScopeModel[21].setRotationPoint(-4.5F, -30F, -5F);

		defaultScopeModel[22].addShapeBox(0F, 0F, 0F, 2, 3, 10, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 87
		defaultScopeModel[22].setRotationPoint(-2.5F, -37F, -5F);

		defaultScopeModel[23].addBox(0F, 0F, 0F, 2, 4, 10, 0F); // Box 88
		defaultScopeModel[23].setRotationPoint(-2.5F, -34F, -5F);

		defaultScopeModel[24].addShapeBox(0F, 0F, 0F, 2, 3, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 89
		defaultScopeModel[24].setRotationPoint(-2.5F, -30F, -5F);

		defaultScopeModel[25].addBox(0F, 0F, 0F, 14, 3, 5, 0F); // Box 93
		defaultScopeModel[25].setRotationPoint(8F, -28F, -2.5F);

		defaultScopeModel[26].addShapeBox(0F, 0F, 0F, 15, 2, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 94
		defaultScopeModel[26].setRotationPoint(2F, -26.25F, 2F);

		defaultScopeModel[27].addShapeBox(0F, 0F, 0F, 15, 2, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 95
		defaultScopeModel[27].setRotationPoint(2F, -28.25F, 2F);


		defaultStockModel = new ModelRendererTurbo[8];
		defaultStockModel[0] = new ModelRendererTurbo(this, 96, 124, textureX, textureY); // Box 32
		defaultStockModel[1] = new ModelRendererTurbo(this, 163, 125, textureX, textureY); // Box 33
		defaultStockModel[2] = new ModelRendererTurbo(this, 96, 142, textureX, textureY); // Box 34
		defaultStockModel[3] = new ModelRendererTurbo(this, 96, 153, textureX, textureY); // Box 35
		defaultStockModel[4] = new ModelRendererTurbo(this, 163, 153, textureX, textureY); // Box 36
		defaultStockModel[5] = new ModelRendererTurbo(this, 200, 142, textureX, textureY); // Box 40
		defaultStockModel[6] = new ModelRendererTurbo(this, 179, 142, textureX, textureY); // Box 41
		defaultStockModel[7] = new ModelRendererTurbo(this, 179, 142, textureX, textureY); // Box 42

		defaultStockModel[0].addShapeBox(0F, 0F, 0F, 25, 9, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 8F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 8F, 0F); // Box 32
		defaultStockModel[0].setRotationPoint(-41F, -17F, -4F);

		defaultStockModel[1].addShapeBox(0F, 0F, 0F, 8, 8, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F); // Box 33
		defaultStockModel[1].setRotationPoint(-16F, -17F, -4F);

		defaultStockModel[2].addShapeBox(0F, 0F, 0F, 33, 2, 8, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 34
		defaultStockModel[2].setRotationPoint(-41F, -19F, -4F);

		defaultStockModel[3].addShapeBox(0F, 0F, 0F, 25, 2, 8, 0F, 0F, 0F, 0F, 0F, 8F, 0F, 0F, 8F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -8F, -1F, 0F, -8F, -1F, 0F, 0F, -1F); // Box 35
		defaultStockModel[3].setRotationPoint(-41F, 0F, -4F);

		defaultStockModel[4].addShapeBox(0F, 0F, 0F, 8, 2, 8, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, 0F, -1F); // Box 36
		defaultStockModel[4].setRotationPoint(-16F, -8F, -4F);

		defaultStockModel[5].addShapeBox(0F, 0F, 0F, 2, 17, 8, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 40
		defaultStockModel[5].setRotationPoint(-43F, -17F, -4F);

		defaultStockModel[6].addShapeBox(0F, 0F, 0F, 2, 2, 8, 0F, 0F, -0.5F, -2F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, -0.5F, -2F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 41
		defaultStockModel[6].setRotationPoint(-43F, -19F, -4F);

		defaultStockModel[7].addShapeBox(0F, 0F, 0F, 2, 2, 8, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -0.5F, -2F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, -0.5F, -2F); // Box 42
		defaultStockModel[7].setRotationPoint(-43F, 0F, -4F);


		defaultGripModel = new ModelRendererTurbo[12];
		defaultGripModel[0] = new ModelRendererTurbo(this, 117, 19, textureX, textureY); // barrelHolder
		defaultGripModel[1] = new ModelRendererTurbo(this, 117, 19, textureX, textureY); // barrelHolder
		defaultGripModel[2] = new ModelRendererTurbo(this, 96, 19, textureX, textureY); // barrelHolder2
		defaultGripModel[3] = new ModelRendererTurbo(this, 96, 19, textureX, textureY); // barrelHolder2
		defaultGripModel[4] = new ModelRendererTurbo(this, 157, 63, textureX, textureY); // body5
		defaultGripModel[5] = new ModelRendererTurbo(this, 96, 80, textureX, textureY); // body6
		defaultGripModel[6] = new ModelRendererTurbo(this, 96, 29, textureX, textureY); // body7
		defaultGripModel[7] = new ModelRendererTurbo(this, 96, 103, textureX, textureY); // body8
		defaultGripModel[8] = new ModelRendererTurbo(this, 96, 114, textureX, textureY); // body9
		defaultGripModel[9] = new ModelRendererTurbo(this, 96, 182, textureX, textureY); // lowerBarrel
		defaultGripModel[10] = new ModelRendererTurbo(this, 96, 182, textureX, textureY); // lowerBarrel
		defaultGripModel[11] = new ModelRendererTurbo(this, 96, 175, textureX, textureY); // lowerBarrel2

		defaultGripModel[0].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // barrelHolder
		defaultGripModel[0].setRotationPoint(45F, -21F, -3.5F);

		defaultGripModel[1].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // barrelHolder
		defaultGripModel[1].setRotationPoint(78F, -21F, -3.5F);

		defaultGripModel[2].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // barrelHolder2
		defaultGripModel[2].setRotationPoint(45F, -19F, -3.5F);

		defaultGripModel[3].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // barrelHolder2
		defaultGripModel[3].setRotationPoint(78F, -19F, -3.5F);

		defaultGripModel[4].addShapeBox(0F, 0F, 0F, 18, 8, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F); // body5
		defaultGripModel[4].setRotationPoint(33F, -17F, -4F);

		defaultGripModel[5].addShapeBox(0F, 0F, 0F, 18, 2, 8, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -2F, -1F, 0F, -2F, -1F, 0F, 0F, -1F); // body6
		defaultGripModel[5].setRotationPoint(33F, -9F, -4F);

		defaultGripModel[6].addBox(0F, 0F, 0F, 30, 6, 8, 0F); // body7
		defaultGripModel[6].setRotationPoint(51F, -17F, -4F);

		defaultGripModel[7].addShapeBox(0F, 0F, 0F, 30, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // body8
		defaultGripModel[7].setRotationPoint(51F, -11F, -4F);

		defaultGripModel[8].addShapeBox(0F, 0F, 0F, 45, 1, 8, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body9
		defaultGripModel[8].setRotationPoint(33F, -18F, -4F);

		defaultGripModel[9].addShapeBox(0F, 0F, 0F, 30, 1, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // lowerBarrel
		defaultGripModel[9].setRotationPoint(52F, -14F, -2F);

		defaultGripModel[10].addShapeBox(0F, 0F, 0F, 30, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // lowerBarrel
		defaultGripModel[10].setRotationPoint(52F, -11F, -2F);

		defaultGripModel[11].addBox(0F, 0F, 0F, 30, 2, 4, 0F); // lowerBarrel2
		defaultGripModel[11].setRotationPoint(52F, -13F, -2F);


		ammoModel = new ModelRendererTurbo[7];
		ammoModel[0] = new ModelRendererTurbo(this, 1, 90, textureX, textureY); // Box 113
		ammoModel[1] = new ModelRendererTurbo(this, 50, 90, textureX, textureY); // Box 57
		ammoModel[2] = new ModelRendererTurbo(this, 48, 66, textureX, textureY); // Box 58
		ammoModel[3] = new ModelRendererTurbo(this, 1, 66, textureX, textureY); // Box 59
		ammoModel[4] = new ModelRendererTurbo(this, 1, 113, textureX, textureY); // Box 60
		ammoModel[5] = new ModelRendererTurbo(this, 36, 116, textureX, textureY); // Box 64
		ammoModel[6] = new ModelRendererTurbo(this, 57, 116, textureX, textureY); // Box 65

		ammoModel[0].addBox(0F, 0F, 0F, 7, 5, 17, 0F); // Box 113
		ammoModel[0].setRotationPoint(14.5F, -18F, 4F);

		ammoModel[1].addShapeBox(0F, 0F, 0F, 1, 5, 17, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F); // Box 57
		ammoModel[1].setRotationPoint(13.5F, -18F, 4F);

		ammoModel[2].addShapeBox(0F, 0F, 0F, 1, 6, 17, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F); // Box 58
		ammoModel[2].setRotationPoint(27.5F, -18.5F, 4F);

		ammoModel[3].addBox(0F, 0F, 0F, 6, 6, 17, 0F); // Box 59
		ammoModel[3].setRotationPoint(21.5F, -18.5F, 4F);

		ammoModel[4].addBox(0F, 0F, 0F, 15, 6, 2, 0F); // Box 60
		ammoModel[4].setRotationPoint(13.5F, -18.5F, 21F);

		ammoModel[5].addShapeBox(0F, 0F, 0F, 9, 4, 1, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 64
		ammoModel[5].setRotationPoint(14.5F, -17.4F, 3F);

		ammoModel[6].addShapeBox(0F, 0F, 0F, 4, 4, 1, 0F, 0F, -1F, 0F, 0F, -1.5F, -0.5F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1.5F, -0.5F, 0F, -1F, 0F, 0F, 0F, 0F); // Box 65
		ammoModel[6].setRotationPoint(23.5F, -17.4F, 3F);


		slideModel = new ModelRendererTurbo[5];
		slideModel[0] = new ModelRendererTurbo(this, 1, 8, textureX, textureY); // Box 18
		slideModel[1] = new ModelRendererTurbo(this, 1, 13, textureX, textureY); // Box 66
		slideModel[2] = new ModelRendererTurbo(this, 1, 13, textureX, textureY); // Box 67
		slideModel[3] = new ModelRendererTurbo(this, 18, 13, textureX, textureY); // Box 69
		slideModel[4] = new ModelRendererTurbo(this, 22, 2, textureX, textureY); // Box 70

		slideModel[0].addShapeBox(0F, 0F, 0F, 12, 3, 1, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 18
		slideModel[0].setRotationPoint(14F, -17F, -3.5F);

		slideModel[1].addShapeBox(0F, 0F, 0F, 2, 4, 6, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F); // Box 66
		slideModel[1].setRotationPoint(23F, -22.5F, -10.5F);

		slideModel[2].addShapeBox(0F, 0F, 0F, 2, 4, 6, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F); // Box 67
		slideModel[2].setRotationPoint(25F, -22.5F, -10.5F);

		slideModel[3].addBox(0F, 0F, 0F, 2, 5, 2, 0F); // Box 69
		slideModel[3].setRotationPoint(24F, -21.5F, -6.5F);

		slideModel[4].addBox(0F, 0F, 0F, 2, 2, 3, 0F); // Box 70
		slideModel[4].setRotationPoint(24F, -16.5F, -6.5F);

		barrelAttachPoint = new Vector3f(83F /16F, 18F /16F, 0F /16F);
		scopeAttachPoint = new Vector3f(18F /16F, 22F /16F, 0F /16F);

		gunSlideDistance = 1F;
		animationType = EnumAnimationType.CUSTOM;

		/* ----Start of Reload Block---- */
		/* Reload Name: Amended Side Clip */
		rotateClipHorizontal = 120F;
		rotateClipVertical = 60F;
		translateClip = new Vector3f(0.5F, 0F, 0F);
		/* ----End of Reload Block---- */

		translateAll(0F, 0F, 0F);


		flipAll();
	}
}