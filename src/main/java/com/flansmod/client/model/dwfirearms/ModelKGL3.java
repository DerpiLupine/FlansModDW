package com.flansmod.client.model.dwfirearms; //Path where the model is located

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelKGL3 extends ModelGun //Same as Filename
{
	int textureX = 512;
	int textureY = 256;

	public ModelKGL3() //Same as Filename
	{
		gunModel = new ModelRendererTurbo[39];
		gunModel[0] = new ModelRendererTurbo(this, 80, 19, textureX, textureY); // Box 1
		gunModel[1] = new ModelRendererTurbo(this, 80, 71, textureX, textureY); // Box 2
		gunModel[2] = new ModelRendererTurbo(this, 80, 31, textureX, textureY); // Box 4
		gunModel[3] = new ModelRendererTurbo(this, 1, 91, textureX, textureY); // Box 7
		gunModel[4] = new ModelRendererTurbo(this, 1, 101, textureX, textureY); // Box 8
		gunModel[5] = new ModelRendererTurbo(this, 30, 101, textureX, textureY); // Box 9
		gunModel[6] = new ModelRendererTurbo(this, 34, 91, textureX, textureY); // Box 10
		gunModel[7] = new ModelRendererTurbo(this, 238, 1, textureX, textureY); // Box 31
		gunModel[8] = new ModelRendererTurbo(this, 238, 12, textureX, textureY); // Box 32
		gunModel[9] = new ModelRendererTurbo(this, 238, 12, textureX, textureY); // Box 33
		gunModel[10] = new ModelRendererTurbo(this, 80, 1, textureX, textureY); // Box 34
		gunModel[11] = new ModelRendererTurbo(this, 80, 1, textureX, textureY); // Box 35
		gunModel[12] = new ModelRendererTurbo(this, 80, 1, textureX, textureY); // Box 36
		gunModel[13] = new ModelRendererTurbo(this, 121, 100, textureX, textureY); // Box 102
		gunModel[14] = new ModelRendererTurbo(this, 121, 100, textureX, textureY); // Box 103
		gunModel[15] = new ModelRendererTurbo(this, 80, 10, textureX, textureY); // Box 97
		gunModel[16] = new ModelRendererTurbo(this, 80, 10, textureX, textureY); // Box 98
		gunModel[17] = new ModelRendererTurbo(this, 80, 10, textureX, textureY); // Box 99
		gunModel[18] = new ModelRendererTurbo(this, 130, 59, textureX, textureY); // Box 103
		gunModel[19] = new ModelRendererTurbo(this, 238, 12, textureX, textureY); // Box 0
		gunModel[20] = new ModelRendererTurbo(this, 238, 1, textureX, textureY); // Box 1
		gunModel[21] = new ModelRendererTurbo(this, 238, 12, textureX, textureY); // Box 2
		gunModel[22] = new ModelRendererTurbo(this, 80, 59, textureX, textureY); // Box 3
		gunModel[23] = new ModelRendererTurbo(this, 109, 59, textureX, textureY); // Box 4
		gunModel[24] = new ModelRendererTurbo(this, 80, 87, textureX, textureY); // Box 5
		gunModel[25] = new ModelRendererTurbo(this, 129, 87, textureX, textureY); // Box 6
		gunModel[26] = new ModelRendererTurbo(this, 80, 100, textureX, textureY); // Box 7
		gunModel[27] = new ModelRendererTurbo(this, 80, 43, textureX, textureY); // Box 8
		gunModel[28] = new ModelRendererTurbo(this, 80, 51, textureX, textureY); // Box 9
		gunModel[29] = new ModelRendererTurbo(this, 183, 28, textureX, textureY); // Box 12
		gunModel[30] = new ModelRendererTurbo(this, 194, 28, textureX, textureY); // Box 15
		gunModel[31] = new ModelRendererTurbo(this, 53, 1, textureX, textureY); // Box 16
		gunModel[32] = new ModelRendererTurbo(this, 53, 11, textureX, textureY); // Box 17
		gunModel[33] = new ModelRendererTurbo(this, 53, 1, textureX, textureY); // Box 18
		gunModel[34] = new ModelRendererTurbo(this, 1, 21, textureX, textureY); // Box 19
		gunModel[35] = new ModelRendererTurbo(this, 18, 17, textureX, textureY); // Box 20
		gunModel[36] = new ModelRendererTurbo(this, 18, 17, textureX, textureY); // Box 21
		gunModel[37] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 22
		gunModel[38] = new ModelRendererTurbo(this, 11, 3, textureX, textureY); // Box 23

		gunModel[0].addBox(0F, 0F, 0F, 25, 3, 8, 0F); // Box 1
		gunModel[0].setRotationPoint(-16F, -21F, -4F);

		gunModel[1].addBox(0F, 0F, 0F, 40, 7, 8, 0F); // Box 2
		gunModel[1].setRotationPoint(-16F, -18F, -4F);

		gunModel[2].addShapeBox(0F, 0F, 0F, 19, 3, 8, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 4
		gunModel[2].setRotationPoint(-10F, -24F, -4F);

		gunModel[3].addShapeBox(0F, 0F, 0F, 10, 3, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F); // Box 7
		gunModel[3].setRotationPoint(-16F, -9F, -3F);

		gunModel[4].addShapeBox(0F, 0F, 0F, 8, 13, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, -2F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 4F, -2F, 0F); // Box 8
		gunModel[4].setRotationPoint(-14F, -6F, -3F);

		gunModel[5].addShapeBox(0F, 0F, 0F, 1, 13, 6, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 4F, 0F, 0F, -4F, 0F, -1F, -4F, 0F, -1F, 4F, 0F, 0F); // Box 9
		gunModel[5].setRotationPoint(-6F, -6F, -3F);

		gunModel[6].addShapeBox(0F, 0F, 0F, 1, 3, 6, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 10
		gunModel[6].setRotationPoint(-6F, -9F, -3F);

		gunModel[7].addBox(0F, 0F, 0F, 2, 3, 7, 0F); // Box 31
		gunModel[7].setRotationPoint(62F, -21.5F, -3.5F);

		gunModel[8].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 32
		gunModel[8].setRotationPoint(62F, -18.5F, -3.5F);

		gunModel[9].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 33
		gunModel[9].setRotationPoint(62F, -23.5F, -3.5F);

		gunModel[10].addBox(0F, 0F, 0F, 45, 2, 6, 0F); // Box 34
		gunModel[10].setRotationPoint(24F, -21F, -3F);

		gunModel[11].addShapeBox(0F, 0F, 0F, 45, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 35
		gunModel[11].setRotationPoint(24F, -23F, -3F);

		gunModel[12].addShapeBox(0F, 0F, 0F, 45, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 36
		gunModel[12].setRotationPoint(24F, -19F, -3F);

		gunModel[13].addShapeBox(0F, 0F, 0F, 2, 1, 9, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 102
		gunModel[13].setRotationPoint(-15F, -20F, -4.5F);

		gunModel[14].addShapeBox(0F, 0F, 0F, 2, 1, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 103
		gunModel[14].setRotationPoint(-15F, -19F, -4.5F);

		gunModel[15].addShapeBox(0F, 0F, 0F, 45, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 97
		gunModel[15].setRotationPoint(24F, -10.5F, -3F);

		gunModel[16].addShapeBox(0F, 0F, 0F, 45, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 98
		gunModel[16].setRotationPoint(24F, -14.5F, -3F);

		gunModel[17].addBox(0F, 0F, 0F, 45, 2, 6, 0F); // Box 99
		gunModel[17].setRotationPoint(24F, -12.5F, -3F);

		gunModel[18].addBox(0F, 0F, 0F, 2, 3, 8, 0F); // Box 103
		gunModel[18].setRotationPoint(22F, -21F, -4F);

		gunModel[19].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 0
		gunModel[19].setRotationPoint(62F, -10F, -3.5F);

		gunModel[20].addBox(0F, 0F, 0F, 2, 3, 7, 0F); // Box 1
		gunModel[20].setRotationPoint(62F, -13F, -3.5F);

		gunModel[21].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 2
		gunModel[21].setRotationPoint(62F, -15F, -3.5F);

		gunModel[22].addShapeBox(0F, 0F, 0F, 6, 3, 8, 0F, 0F, -2F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, -2F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 3
		gunModel[22].setRotationPoint(-16F, -24F, -4F);

		gunModel[23].addShapeBox(0F, 0F, 0F, 2, 3, 8, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 4
		gunModel[23].setRotationPoint(22F, -24F, -4F);

		gunModel[24].addShapeBox(0F, 0F, 0F, 16, 4, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 5
		gunModel[24].setRotationPoint(8F, -11F, -4F);

		gunModel[25].addShapeBox(0F, 0F, 0F, 5, 4, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F); // Box 6
		gunModel[25].setRotationPoint(3F, -11F, -4F);

		gunModel[26].addBox(0F, 0F, 0F, 12, 2, 8, 0F); // Box 7
		gunModel[26].setRotationPoint(-16F, -11F, -4F);

		gunModel[27].addShapeBox(0F, 0F, 0F, 13, 3, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 8
		gunModel[27].setRotationPoint(9F, -24F, 0F);

		gunModel[28].addBox(0F, 0F, 0F, 13, 3, 4, 0F); // Box 9
		gunModel[28].setRotationPoint(9F, -21F, 0F);

		gunModel[29].addBox(0F, 0F, 0F, 2, 2, 3, 0F); // Box 12
		gunModel[29].setRotationPoint(62F, -17F, -1.5F);

		gunModel[30].addBox(0F, 0F, 0F, 2, 2, 3, 0F); // Box 15
		gunModel[30].setRotationPoint(62F, -8F, -1.5F);

		gunModel[31].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 16
		gunModel[31].setRotationPoint(49F, -18.5F, -3.5F);

		gunModel[32].addBox(0F, 0F, 0F, 2, 3, 7, 0F); // Box 17
		gunModel[32].setRotationPoint(49F, -21.5F, -3.5F);

		gunModel[33].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 18
		gunModel[33].setRotationPoint(49F, -23.5F, -3.5F);

		gunModel[34].addBox(0F, 0F, 0F, 2, 2, 7, 0F); // Box 19
		gunModel[34].setRotationPoint(48F, -25.5F, -3.5F);

		gunModel[35].addShapeBox(0F, 0F, 0F, 2, 1, 5, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 20
		gunModel[35].setRotationPoint(49F, -25F, -2.5F);

		gunModel[36].addShapeBox(0F, 0F, 0F, 2, 1, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 21
		gunModel[36].setRotationPoint(49F, -24F, -2.5F);

		gunModel[37].addBox(0F, 0F, 0F, 1, 12, 7, 0F); // Box 22
		gunModel[37].setRotationPoint(48F, -37.5F, -3.5F);

		gunModel[38].addBox(0F, 0F, 0F, 2, 3, 1, 0F); // Box 23
		gunModel[38].setRotationPoint(66F, -26F, -0.5F);


		defaultStockModel = new ModelRendererTurbo[8];
		defaultStockModel[0] = new ModelRendererTurbo(this, 39, 63, textureX, textureY); // Box 82
		defaultStockModel[1] = new ModelRendererTurbo(this, 1, 40, textureX, textureY); // Box 83
		defaultStockModel[2] = new ModelRendererTurbo(this, 1, 31, textureX, textureY); // Box 84
		defaultStockModel[3] = new ModelRendererTurbo(this, 28, 82, textureX, textureY); // Box 85
		defaultStockModel[4] = new ModelRendererTurbo(this, 53, 82, textureX, textureY); // Box 86
		defaultStockModel[5] = new ModelRendererTurbo(this, 1, 78, textureX, textureY); // Box 87
		defaultStockModel[6] = new ModelRendererTurbo(this, 1, 57, textureX, textureY); // Box 88
		defaultStockModel[7] = new ModelRendererTurbo(this, 26, 56, textureX, textureY); // Box 90

		defaultStockModel[0].addShapeBox(0F, 0F, 0F, 5, 7, 7, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F); // Box 82
		defaultStockModel[0].setRotationPoint(-21F, -18F, -3.5F);

		defaultStockModel[1].addShapeBox(0F, 0F, 0F, 25, 8, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 10F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 10F, 0F); // Box 83
		defaultStockModel[1].setRotationPoint(-46F, -18F, -3.5F);

		defaultStockModel[2].addShapeBox(0F, 0F, 0F, 25, 1, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 84
		defaultStockModel[2].setRotationPoint(-46F, -19F, -3.5F);

		defaultStockModel[3].addShapeBox(0F, 0F, 0F, 5, 1, 7, 0F, 0F, -2F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, -2F, -1F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F); // Box 85
		defaultStockModel[3].setRotationPoint(-21F, -21F, -3.5F);

		defaultStockModel[4].addShapeBox(0F, 0F, 0F, 6, 1, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 86
		defaultStockModel[4].setRotationPoint(-53F, -19F, -3.5F);

		defaultStockModel[5].addShapeBox(0F, 0F, 0F, 6, 5, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F); // Box 87
		defaultStockModel[5].setRotationPoint(-53F, -18F, -3.5F);

		defaultStockModel[6].addBox(0F, 0F, 0F, 5, 13, 7, 0F); // Box 88
		defaultStockModel[6].setRotationPoint(-52F, -13F, -3.5F);

		defaultStockModel[7].addBox(0F, 0F, 0F, 1, 16, 5, 0F); // Box 90
		defaultStockModel[7].setRotationPoint(-47F, -17F, -2.5F);


		ammoModel = new ModelRendererTurbo[6];
		ammoModel[0] = new ModelRendererTurbo(this, 184, 45, textureX, textureY); // Box 24
		ammoModel[1] = new ModelRendererTurbo(this, 184, 34, textureX, textureY); // Box 25
		ammoModel[2] = new ModelRendererTurbo(this, 184, 45, textureX, textureY); // Box 26
		ammoModel[3] = new ModelRendererTurbo(this, 221, 45, textureX, textureY); // Box 27
		ammoModel[4] = new ModelRendererTurbo(this, 221, 34, textureX, textureY); // Box 28
		ammoModel[5] = new ModelRendererTurbo(this, 221, 45, textureX, textureY); // Box 30

		ammoModel[0].addShapeBox(0F, 0F, 0F, 11, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 24
		ammoModel[0].setRotationPoint(9F, -15F, -3.5F);

		ammoModel[1].addBox(0F, 0F, 0F, 11, 3, 7, 0F); // Box 25
		ammoModel[1].setRotationPoint(9F, -13F, -3.5F);

		ammoModel[2].addShapeBox(0F, 0F, 0F, 11, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 26
		ammoModel[2].setRotationPoint(9F, -10F, -3.5F);

		ammoModel[3].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -2F, 0F, -2F, -3F, 0F, -2F, -3F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 1F, -2F, 0F, 1F, -2F, 0F, 0F, 0F); // Box 27
		ammoModel[3].setRotationPoint(20F, -15F, -3.5F);

		ammoModel[4].addShapeBox(0F, 0F, 0F, 3, 3, 7, 0F, 0F, 0F, 0F, 0F, -1F, -2F, 0F, -1F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, -2F, 0F, -1F, -2F, 0F, 0F, 0F); // Box 28
		ammoModel[4].setRotationPoint(20F, -13F, -3.5F);

		ammoModel[5].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 1F, -2F, 0F, 1F, -2F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, -2F, -3F, 0F, -2F, -3F, 0F, 0F, -2F); // Box 30
		ammoModel[5].setRotationPoint(20F, -10F, -3.5F);


		pumpModel = new ModelRendererTurbo[7];
		pumpModel[0] = new ModelRendererTurbo(this, 183, 1, textureX, textureY); // Box 93
		pumpModel[1] = new ModelRendererTurbo(this, 183, 11, textureX, textureY); // Box 94
		pumpModel[2] = new ModelRendererTurbo(this, 183, 1, textureX, textureY); // Box 95
		pumpModel[3] = new ModelRendererTurbo(this, 18, 1, textureX, textureY); // Box 10
		pumpModel[4] = new ModelRendererTurbo(this, 18, 9, textureX, textureY); // Box 11
		pumpModel[5] = new ModelRendererTurbo(this, 222, 22, textureX, textureY); // Box 13
		pumpModel[6] = new ModelRendererTurbo(this, 183, 22, textureX, textureY); // Box 14

		pumpModel[0].addShapeBox(0F, 0F, 0F, 20, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 93
		pumpModel[0].setRotationPoint(41F, -10F, -3.5F);

		pumpModel[1].addBox(0F, 0F, 0F, 20, 3, 7, 0F); // Box 94
		pumpModel[1].setRotationPoint(41F, -13F, -3.5F);

		pumpModel[2].addShapeBox(0F, 0F, 0F, 20, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 95
		pumpModel[2].setRotationPoint(41F, -15F, -3.5F);

		pumpModel[3].addShapeBox(0F, 0F, 0F, 13, 3, 4, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 10
		pumpModel[3].setRotationPoint(9F, -23.5F, -3.5F);

		pumpModel[4].addBox(0F, 0F, 0F, 13, 3, 4, 0F); // Box 11
		pumpModel[4].setRotationPoint(9F, -20.5F, -3.5F);

		pumpModel[5].addBox(0F, 0F, 0F, 2, 2, 3, 0F); // Box 13
		pumpModel[5].setRotationPoint(41F, -17F, -1.5F);

		pumpModel[6].addBox(0F, 0F, 0F, 17, 3, 2, 0F); // Box 14
		pumpModel[6].setRotationPoint(24F, -17F, -1F);

		stockAttachPoint = new Vector3f(-16F /16F, 15F /16F, 0F /16F);

		gunSlideDistance = 0F;
		animationType = EnumAnimationType.SHOTGUN;

		numBulletsInReloadAnimation = 3;
		tiltGunTime = 0.159F;
		unloadClipTime = 0.0F;
		loadClipTime = 0.708F;
		untiltGunTime = 0.133F;

		pumpDelay = 15;
		pumpTime = 15;
		pumpDelayAfterReload = 90;
		pumpHandleDistance = 1.2F;

		translateAll(0F, 0F, 0F);


		flipAll();
	}
}