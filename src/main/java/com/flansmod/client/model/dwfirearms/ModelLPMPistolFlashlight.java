package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.ModelAttachment;
import com.flansmod.client.tmt.ModelRendererTurbo;

import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelLPMPistolFlashlight extends ModelAttachment 
{
	int textureX = 512;
	int textureY = 256;

	public ModelLPMPistolFlashlight()
	{
		attachmentModel = new ModelRendererTurbo[12];
		attachmentModel[0] = new ModelRendererTurbo(this, 287, 97, textureX, textureY); // flashLight1
		attachmentModel[1] = new ModelRendererTurbo(this, 330, 87, textureX, textureY); // flashLight2
		attachmentModel[2] = new ModelRendererTurbo(this, 287, 87, textureX, textureY); // flashLight3
		attachmentModel[3] = new ModelRendererTurbo(this, 261, 112, textureX, textureY); // flashLight4
		attachmentModel[4] = new ModelRendererTurbo(this, 290, 114, textureX, textureY); // light1
		attachmentModel[5] = new ModelRendererTurbo(this, 290, 114, textureX, textureY); // light1
		attachmentModel[6] = new ModelRendererTurbo(this, 279, 113, textureX, textureY); // light2
		attachmentModel[7] = new ModelRendererTurbo(this, 240, 87, textureX, textureY); // mounterBase
		attachmentModel[8] = new ModelRendererTurbo(this, 240, 100, textureX, textureY); // mounterBase3
		attachmentModel[9] = new ModelRendererTurbo(this, 322, 113, textureX, textureY); // Box 10
		attachmentModel[10] = new ModelRendererTurbo(this, 240, 111, textureX, textureY); // Box 11
		attachmentModel[11] = new ModelRendererTurbo(this, 301, 113, textureX, textureY); // Box 12

		attachmentModel[0].addShapeBox(-7F, 6F, -3.5F, 14, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // flashLight1
		attachmentModel[0].setRotationPoint(0F, 0F, 0F);

		attachmentModel[1].addShapeBox(-4F, 10F, -3.5F, 11, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // flashLight2
		attachmentModel[1].setRotationPoint(0F, 0F, 0F);

		attachmentModel[2].addBox(-7F, 8F, -3.5F, 14, 2, 7, 0F); // flashLight3
		attachmentModel[2].setRotationPoint(0F, 0F, 0F);

		attachmentModel[3].addBox(-7F, 10F, -2.5F, 3, 2, 5, 0F); // flashLight4
		attachmentModel[3].setRotationPoint(0F, 0F, 0F);

		attachmentModel[4].addShapeBox(11F, 7F, -2F, 1, 1, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // light1
		attachmentModel[4].setRotationPoint(0F, 0F, 0F);

		attachmentModel[5].addShapeBox(11F, 10F, -2F, 1, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // light1
		attachmentModel[5].setRotationPoint(0F, 0F, 0F);

		attachmentModel[6].addBox(11F, 8F, -2F, 1, 2, 4, 0F); // light2
		attachmentModel[6].setRotationPoint(0F, 0F, 0F);

		attachmentModel[7].addBox(-7F, 0F, -4.5F, 14, 3, 9, 0F); // mounterBase
		attachmentModel[7].setRotationPoint(0F, 0F, 0F);

		attachmentModel[8].addBox(-7F, 3F, -2.5F, 14, 3, 5, 0F); // mounterBase3
		attachmentModel[8].setRotationPoint(0F, 0F, 0F);

		attachmentModel[9].addShapeBox(6.5F, 6.5F, -2.5F, 5, 1, 5, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 10
		attachmentModel[9].setRotationPoint(0F, 0F, 0F);

		attachmentModel[10].addBox(6.5F, 7.5F, -2.5F, 5, 3, 5, 0F); // Box 11
		attachmentModel[10].setRotationPoint(0F, 0F, 0F);

		attachmentModel[11].addShapeBox(6.5F, 10.5F, -2.5F, 5, 1, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 12
		attachmentModel[11].setRotationPoint(0F, 0F, 0F);

		renderOffset = 0F;

		flipAll();
	}
}