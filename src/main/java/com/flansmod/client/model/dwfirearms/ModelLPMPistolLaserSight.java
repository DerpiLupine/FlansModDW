package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.ModelAttachment;
import com.flansmod.client.tmt.ModelRendererTurbo;

import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelLPMPistolLaserSight extends ModelAttachment 
{
	int textureX = 512;
	int textureY = 256;

	public ModelLPMPistolLaserSight()
	{
		attachmentModel = new ModelRendererTurbo[8];
		attachmentModel[0] = new ModelRendererTurbo(this, 267, 136, textureX, textureY); // flashLight2
		attachmentModel[1] = new ModelRendererTurbo(this, 287, 122, textureX, textureY); // flashLight3
		attachmentModel[2] = new ModelRendererTurbo(this, 303, 137, textureX, textureY); // flashLight4
		attachmentModel[3] = new ModelRendererTurbo(this, 294, 136, textureX, textureY); // light2
		attachmentModel[4] = new ModelRendererTurbo(this, 240, 120, textureX, textureY); // mounterBase
		attachmentModel[5] = new ModelRendererTurbo(this, 240, 145, textureX, textureY); // Box 0
		attachmentModel[6] = new ModelRendererTurbo(this, 240, 143, textureX, textureY); // Box 1
		attachmentModel[7] = new ModelRendererTurbo(this, 240, 133, textureX, textureY); // Box 8

		attachmentModel[0].addShapeBox(-4F, 8F, -2.5F, 8, 1, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // flashLight2
		attachmentModel[0].setRotationPoint(0F, 0F, 0F);

		attachmentModel[1].addBox(-7F, 3F, -2.5F, 11, 5, 5, 0F); // flashLight3
		attachmentModel[1].setRotationPoint(0F, 0F, 0F);

		attachmentModel[2].addBox(-7F, 8F, -1.5F, 3, 2, 3, 0F); // flashLight4
		attachmentModel[2].setRotationPoint(0F, 0F, 0F);

		attachmentModel[3].addBox(3.5F, 4.5F, -1.5F, 1, 3, 3, 0F); // light2
		attachmentModel[3].setRotationPoint(0F, 0F, 0F);

		attachmentModel[4].addBox(-7F, 0F, -4.5F, 14, 3, 9, 0F); // mounterBase
		attachmentModel[4].setRotationPoint(0F, 0F, 0F);

		attachmentModel[5].addShapeBox(4.5F, 5.5F, 0F, 50, 1, 0, 0F, 0F, 0F, 0F, 50F, 0F, 0F, 50F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 50F, 0F, 0F, 50F, 0F, 0F, 0F, 0F, 0F); // Box 0
		attachmentModel[5].setRotationPoint(0F, 0F, 0F);

		attachmentModel[6].addShapeBox(4.5F, 6F, -0.5F, 50, 0, 1, 0F, 0F, 0F, 0F, 50F, 0F, 0F, 50F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 50F, 0F, 0F, 50F, 0F, 0F, 0F, 0F, 0F); // Box 1
		attachmentModel[6].setRotationPoint(0F, 0F, 0F);

		attachmentModel[7].addBox(-5F, 3F, -3F, 7, 3, 6, 0F); // Box 8
		attachmentModel[7].setRotationPoint(0F, 0F, 0F);

		renderOffset = 0F;
		
		flipAll();
	}
}