package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelMBTAutoCrossbow extends ModelGun
{
	int textureX = 512;
	int textureY = 256;

	public ModelMBTAutoCrossbow()
	{
		gunModel = new ModelRendererTurbo[50];
		gunModel[0] = new ModelRendererTurbo(this, 1, 59, textureX, textureY); // grip1
		gunModel[1] = new ModelRendererTurbo(this, 1, 24, textureX, textureY); // Box 59
		gunModel[2] = new ModelRendererTurbo(this, 1, 34, textureX, textureY); // Box 60
		gunModel[3] = new ModelRendererTurbo(this, 1, 48, textureX, textureY); // Box 61
		gunModel[4] = new ModelRendererTurbo(this, 30, 24, textureX, textureY); // Box 62
		gunModel[5] = new ModelRendererTurbo(this, 30, 48, textureX, textureY); // Box 63
		gunModel[6] = new ModelRendererTurbo(this, 30, 34, textureX, textureY); // Box 64
		gunModel[7] = new ModelRendererTurbo(this, 47, 24, textureX, textureY); // Box 65
		gunModel[8] = new ModelRendererTurbo(this, 47, 34, textureX, textureY); // Box 66
		gunModel[9] = new ModelRendererTurbo(this, 47, 49, textureX, textureY); // Box 67
		gunModel[10] = new ModelRendererTurbo(this, 118, 16, textureX, textureY); // body3
		gunModel[11] = new ModelRendererTurbo(this, 118, 89, textureX, textureY); // body4
		gunModel[12] = new ModelRendererTurbo(this, 118, 89, textureX, textureY); // body5
		gunModel[13] = new ModelRendererTurbo(this, 118, 78, textureX, textureY); // body6
		gunModel[14] = new ModelRendererTurbo(this, 118, 67, textureX, textureY); // body7
		gunModel[15] = new ModelRendererTurbo(this, 118, 106, textureX, textureY); // body8
		gunModel[16] = new ModelRendererTurbo(this, 118, 99, textureX, textureY); // body9
		gunModel[17] = new ModelRendererTurbo(this, 1, 71, textureX, textureY); // bow1
		gunModel[18] = new ModelRendererTurbo(this, 66, 103, textureX, textureY); // bow1
		gunModel[19] = new ModelRendererTurbo(this, 26, 71, textureX, textureY); // bow2
		gunModel[20] = new ModelRendererTurbo(this, 26, 71, textureX, textureY); // bow2
		gunModel[21] = new ModelRendererTurbo(this, 49, 73, textureX, textureY); // bow3
		gunModel[22] = new ModelRendererTurbo(this, 49, 73, textureX, textureY); // bow3
		gunModel[23] = new ModelRendererTurbo(this, 1, 88, textureX, textureY); // bow4
		gunModel[24] = new ModelRendererTurbo(this, 1, 88, textureX, textureY); // bow4
		gunModel[25] = new ModelRendererTurbo(this, 20, 89, textureX, textureY); // bow5
		gunModel[26] = new ModelRendererTurbo(this, 20, 89, textureX, textureY); // bow5
		gunModel[27] = new ModelRendererTurbo(this, 39, 89, textureX, textureY); // bow6
		gunModel[28] = new ModelRendererTurbo(this, 39, 89, textureX, textureY); // bow6
		gunModel[29] = new ModelRendererTurbo(this, 143, 106, textureX, textureY); // body9
		gunModel[30] = new ModelRendererTurbo(this, 126, 4, textureX, textureY); // barrel
		gunModel[31] = new ModelRendererTurbo(this, 56, 2, textureX, textureY); // railBase
		gunModel[32] = new ModelRendererTurbo(this, 47, 12, textureX, textureY); // railBolt
		gunModel[33] = new ModelRendererTurbo(this, 47, 12, textureX, textureY); // railBolt
		gunModel[34] = new ModelRendererTurbo(this, 1, 68, textureX, textureY); // string
		gunModel[35] = new ModelRendererTurbo(this, 1, 68, textureX, textureY); // string
		gunModel[36] = new ModelRendererTurbo(this, 71, 71, textureX, textureY); // stringHolder
		gunModel[37] = new ModelRendererTurbo(this, 71, 82, textureX, textureY); // stringHolder2
		gunModel[38] = new ModelRendererTurbo(this, 47, 12, textureX, textureY); // Box 10
		gunModel[39] = new ModelRendererTurbo(this, 47, 12, textureX, textureY); // Box 11
		gunModel[40] = new ModelRendererTurbo(this, 56, 2, textureX, textureY); // underRail1
		gunModel[41] = new ModelRendererTurbo(this, 70, 14, textureX, textureY); // Box 13
		gunModel[42] = new ModelRendererTurbo(this, 70, 14, textureX, textureY); // Box 14
		gunModel[43] = new ModelRendererTurbo(this, 70, 14, textureX, textureY); // Box 15
		gunModel[44] = new ModelRendererTurbo(this, 70, 14, textureX, textureY); // Box 16
		gunModel[45] = new ModelRendererTurbo(this, 70, 14, textureX, textureY); // Box 17
		gunModel[46] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // ironSightBlock
		gunModel[47] = new ModelRendererTurbo(this, 91, 14, textureX, textureY); // ironSightBlock2
		gunModel[48] = new ModelRendererTurbo(this, 1, 15, textureX, textureY); // ironSightBlock3
		gunModel[49] = new ModelRendererTurbo(this, 1, 15, textureX, textureY); // ironSightBlock3

		gunModel[0].addShapeBox(0F, 0F, 0F, 12, 2, 6, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // grip1
		gunModel[0].setRotationPoint(-12F, -9F, -3F);

		gunModel[1].addShapeBox(0F, 0F, 0F, 8, 3, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 59
		gunModel[1].setRotationPoint(-10F, -7F, -3F);

		gunModel[2].addShapeBox(0F, 0F, 0F, 8, 7, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 3F, 0F, 0F); // Box 60
		gunModel[2].setRotationPoint(-10F, -4F, -3F);

		gunModel[3].addShapeBox(0F, 0F, 0F, 8, 4, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F); // Box 61
		gunModel[3].setRotationPoint(-13F, 3F, -3F);

		gunModel[4].addShapeBox(0F, 0F, 0F, 2, 3, 6, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 62
		gunModel[4].setRotationPoint(-2F, -7F, -3F);

		gunModel[5].addShapeBox(0F, 0F, 0F, 2, 4, 6, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.25F, -1F, 0F, 0.25F, -1F, 0F, 0F, 0F); // Box 63
		gunModel[5].setRotationPoint(-5F, 3F, -3F);

		gunModel[6].addShapeBox(0F, 0F, 0F, 2, 7, 6, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, -1F, -3F, 0F, -1F, 3F, 0F, 0F); // Box 64
		gunModel[6].setRotationPoint(-2F, -4F, -3F);

		gunModel[7].addShapeBox(0F, 0F, 0F, 1, 3, 6, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 65
		gunModel[7].setRotationPoint(-11F, -7F, -3F);

		gunModel[8].addShapeBox(0F, 0F, 0F, 1, 7, 6, 0F, -3F, 0F, -1F, 3F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 66
		gunModel[8].setRotationPoint(-14F, -4F, -3F);

		gunModel[9].addShapeBox(0F, 0F, 0F, 1, 3, 6, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -0.25F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.25F, -1F); // Box 67
		gunModel[9].setRotationPoint(-14F, 3F, -3F);

		gunModel[10].addBox(0F, 0F, 0F, 35, 12, 8, 0F); // body3
		gunModel[10].setRotationPoint(-15F, -21F, -4F);

		gunModel[11].addBox(0F, 0F, 0F, 45, 6, 3, 0F); // body4
		gunModel[11].setRotationPoint(20F, -21F, -4F);

		gunModel[12].addBox(0F, 0F, 0F, 45, 6, 3, 0F); // body5
		gunModel[12].setRotationPoint(20F, -21F, 1F);

		gunModel[13].addBox(0F, 0F, 0F, 45, 3, 7, 0F); // body6
		gunModel[13].setRotationPoint(20F, -15F, -3.5F);

		gunModel[14].addBox(0F, 0F, 0F, 45, 2, 8, 0F); // body7
		gunModel[14].setRotationPoint(20F, -12F, -4F);

		gunModel[15].addShapeBox(0F, 0F, 0F, 4, 4, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F); // body8
		gunModel[15].setRotationPoint(65F, -20F, -4F);

		gunModel[16].addBox(0F, 0F, 0F, 45, 4, 2, 0F); // body9
		gunModel[16].setRotationPoint(20F, -19F, -1F);

		gunModel[17].addBox(0F, 0F, 0F, 2, 6, 10, 0F); // bow1
		gunModel[17].setRotationPoint(63F, -21F, -14F);

		gunModel[18].addBox(0F, 0F, 0F, 2, 6, 10, 0F); // bow1
		gunModel[18].setRotationPoint(63F, -21F, 4F);

		gunModel[19].addShapeBox(0F, 0F, 0F, 1, 6, 10, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F); // bow2
		gunModel[19].setRotationPoint(62F, -21F, -14F);

		gunModel[20].addShapeBox(0F, 0F, 0F, 1, 6, 10, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F); // bow2
		gunModel[20].setRotationPoint(62F, -21F, 4F);

		gunModel[21].addShapeBox(0F, 0F, 0F, 2, 6, 8, 0F, 4F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // bow3
		gunModel[21].setRotationPoint(63F, -21F, -22F);

		gunModel[22].addShapeBox(0F, 0F, 0F, 2, 6, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, 4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, 4F, 0F, 0F); // bow3
		gunModel[22].setRotationPoint(63F, -21F, 14F);

		gunModel[23].addShapeBox(0F, 0F, 0F, 1, 6, 8, 0F, 0F, -1F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, 4F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, 4F, -1F, 0F); // bow4
		gunModel[23].setRotationPoint(62F, -21F, 14F);

		gunModel[24].addShapeBox(0F, 0F, 0F, 1, 6, 8, 0F, 4F, -1F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 4F, -1F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F); // bow4
		gunModel[24].setRotationPoint(62F, -21F, -22F);

		gunModel[25].addShapeBox(0F, 0F, 0F, 2, 6, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -8F, 0F, 0F, 8F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -8F, 0F, 0F, 8F, 0F, 0F); // bow5
		gunModel[25].setRotationPoint(59F, -21F, 22F);

		gunModel[26].addShapeBox(0F, 0F, 0F, 2, 6, 7, 0F, 8F, 0F, 0F, -8F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 8F, 0F, 0F, -8F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // bow5
		gunModel[26].setRotationPoint(59F, -21F, -29F);

		gunModel[27].addShapeBox(0F, 0F, 0F, 1, 6, 7, 0F, 8F, -1F, 0F, -8F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 8F, -1F, 0F, -8F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F); // bow6
		gunModel[27].setRotationPoint(58F, -21F, -29F);

		gunModel[28].addShapeBox(0F, 0F, 0F, 1, 6, 7, 0F, 0F, -1F, 0F, 0F, 0F, 0F, -8F, 0F, 0F, 8F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, -8F, 0F, 0F, 8F, -1F, 0F); // bow6
		gunModel[28].setRotationPoint(58F, -21F, 22F);

		gunModel[29].addShapeBox(0F, 0F, 0F, 4, 1, 8, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body9
		gunModel[29].setRotationPoint(65F, -24F, -4F);

		gunModel[30].addBox(0F, 0F, 0F, 4, 3, 8, 0F); // barrel
		gunModel[30].setRotationPoint(65F, -23F, -4F);

		gunModel[31].addBox(0F, 0F, 0F, 27, 2, 7, 0F); // railBase
		gunModel[31].setRotationPoint(-11F, -23F, -3.5F);

		gunModel[32].addBox(0F, 0F, 0F, 2, 2, 9, 0F); // railBolt
		gunModel[32].setRotationPoint(13F, -22F, -4.5F);

		gunModel[33].addBox(0F, 0F, 0F, 2, 2, 9, 0F); // railBolt
		gunModel[33].setRotationPoint(-10F, -22F, -4.5F);

		gunModel[34].addBox(0F, 0F, 0F, 42, 1, 1, 0F); // string
		gunModel[34].setRotationPoint(19F, -22F, 3F);
		gunModel[34].rotateAngleY = 0.64577182F;
		gunModel[34].rotateAngleZ = -0.08726646F;

		gunModel[35].addBox(0F, 0F, 0F, 42, 1, 1, 0F); // string
		gunModel[35].setRotationPoint(19F, -22F, -4F);
		gunModel[35].rotateAngleY = -0.64577182F;
		gunModel[35].rotateAngleZ = -0.08726646F;

		gunModel[36].addBox(0F, 0F, 0F, 2, 2, 8, 0F); // stringHolder
		gunModel[36].setRotationPoint(18F, -23F, -4F);

		gunModel[37].addShapeBox(0F, 0F, 0F, 1, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F); // stringHolder2
		gunModel[37].setRotationPoint(17F, -23F, -3F);

		gunModel[38].addBox(0F, 0F, 0F, 2, 2, 9, 0F); // Box 10
		gunModel[38].setRotationPoint(45F, -11F, -4.5F);

		gunModel[39].addBox(0F, 0F, 0F, 2, 2, 9, 0F); // Box 11
		gunModel[39].setRotationPoint(22F, -11F, -4.5F);

		gunModel[40].addBox(0F, 0F, 0F, 27, 2, 7, 0F); // underRail1
		gunModel[40].setRotationPoint(21F, -10F, -3.5F);

		gunModel[41].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 13
		gunModel[41].setRotationPoint(45F, -8F, -3.5F);

		gunModel[42].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 14
		gunModel[42].setRotationPoint(39F, -8F, -3.5F);

		gunModel[43].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 15
		gunModel[43].setRotationPoint(33F, -8F, -3.5F);

		gunModel[44].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 16
		gunModel[44].setRotationPoint(27F, -8F, -3.5F);

		gunModel[45].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 17
		gunModel[45].setRotationPoint(21F, -8F, -3.5F);

		gunModel[46].addBox(0F, 0F, 0F, 20, 6, 7, 0F); // ironSightBlock
		gunModel[46].setRotationPoint(-4F, -29F, -3.5F);

		gunModel[47].addBox(0F, 0F, 0F, 2, 4, 5, 0F); // ironSightBlock2
		gunModel[47].setRotationPoint(-6F, -27F, -2.5F);

		gunModel[48].addShapeBox(0F, 0F, 0F, 20, 2, 2, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // ironSightBlock3
		gunModel[48].setRotationPoint(-4F, -31F, -3.5F);

		gunModel[49].addShapeBox(0F, 0F, 0F, 20, 2, 2, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // ironSightBlock3
		gunModel[49].setRotationPoint(-4F, -31F, 1.5F);


		defaultStockModel = new ModelRendererTurbo[9];
		defaultStockModel[0] = new ModelRendererTurbo(this, 1, 150, textureX, textureY); // Box 82
		defaultStockModel[1] = new ModelRendererTurbo(this, 1, 112, textureX, textureY); // Box 83
		defaultStockModel[2] = new ModelRendererTurbo(this, 1, 103, textureX, textureY); // Box 84
		defaultStockModel[3] = new ModelRendererTurbo(this, 39, 141, textureX, textureY); // Box 85
		defaultStockModel[4] = new ModelRendererTurbo(this, 26, 156, textureX, textureY); // Box 86
		defaultStockModel[5] = new ModelRendererTurbo(this, 39, 128, textureX, textureY); // Box 87
		defaultStockModel[6] = new ModelRendererTurbo(this, 1, 129, textureX, textureY); // Box 88
		defaultStockModel[7] = new ModelRendererTurbo(this, 19, 150, textureX, textureY); // Box 89
		defaultStockModel[8] = new ModelRendererTurbo(this, 26, 128, textureX, textureY); // Box 90

		defaultStockModel[0].addShapeBox(0F, 0F, 0F, 5, 7, 7, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F); // Box 82
		defaultStockModel[0].setRotationPoint(-20F, -17F, -3.5F);

		defaultStockModel[1].addShapeBox(0F, 0F, 0F, 25, 8, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 10F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 10F, 0F); // Box 83
		defaultStockModel[1].setRotationPoint(-45F, -17F, -3.5F);

		defaultStockModel[2].addShapeBox(0F, 0F, 0F, 25, 1, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 84
		defaultStockModel[2].setRotationPoint(-45F, -18F, -3.5F);

		defaultStockModel[3].addShapeBox(0F, 0F, 0F, 5, 1, 7, 0F, 0F, -2F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, -2F, -1F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F); // Box 85
		defaultStockModel[3].setRotationPoint(-20F, -20F, -3.5F);

		defaultStockModel[4].addShapeBox(0F, 0F, 0F, 6, 1, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 86
		defaultStockModel[4].setRotationPoint(-52F, -18F, -3.5F);

		defaultStockModel[5].addShapeBox(0F, 0F, 0F, 6, 5, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F); // Box 87
		defaultStockModel[5].setRotationPoint(-52F, -17F, -3.5F);

		defaultStockModel[6].addBox(0F, 0F, 0F, 5, 13, 7, 0F); // Box 88
		defaultStockModel[6].setRotationPoint(-51F, -12F, -3.5F);

		defaultStockModel[7].addShapeBox(0F, 0F, 0F, 1, 1, 5, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 89
		defaultStockModel[7].setRotationPoint(-46F, -17F, -2.5F);

		defaultStockModel[8].addBox(0F, 0F, 0F, 1, 16, 5, 0F); // Box 90
		defaultStockModel[8].setRotationPoint(-46F, -16F, -2.5F);


		ammoModel = new ModelRendererTurbo[4];
		ammoModel[0] = new ModelRendererTurbo(this, 118, 53, textureX, textureY); // Box 5
		ammoModel[1] = new ModelRendererTurbo(this, 118, 44, textureX, textureY); // Box 6
		ammoModel[2] = new ModelRendererTurbo(this, 118, 37, textureX, textureY); // Box 8
		ammoModel[3] = new ModelRendererTurbo(this, 151, 11, textureX, textureY); // Box 9

		ammoModel[0].addBox(0F, 0F, 0F, 45, 5, 8, 0F); // Box 5
		ammoModel[0].setRotationPoint(20F, -26F, -4F);

		ammoModel[1].addShapeBox(0F, 0F, 0F, 45, 1, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // Box 6
		ammoModel[1].setRotationPoint(20F, -27F, -3.5F);

		ammoModel[2].addBox(0F, 0F, 0F, 45, 1, 5, 0F); // Box 8
		ammoModel[2].setRotationPoint(20F, -28F, -2.5F);

		ammoModel[3].addBox(0F, 0F, 0F, 4, 1, 3, 0F); // Box 9
		ammoModel[3].setRotationPoint(21F, -29F, -1.5F);

		stockAttachPoint = new Vector3f(-15F /16F, 15.5F /16F, 0F /16F);
		gripAttachPoint = new Vector3f(35 /16F, 9F /16F, 0F /16F);

		gunSlideDistance = 0F;
		animationType = EnumAnimationType.CUSTOM;

		/* ----Start of Reload Block---- */
		/* Reload Name: Alternate Pistol Clip */
		rotateGunVertical = -15F;
		
		rotateClipVertical = -15F;
		translateClip = new Vector3f(-6F, 0.25F, 1.5F);
		/* ----End of Reload Block---- */

		translateAll(0F, 0F, 0F);


		flipAll();
	}
}