package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelRurhicvtsy87 extends ModelGun
{
	int textureX = 512;
	int textureY = 256;

	public ModelRurhicvtsy87()
	{
		gunModel = new ModelRendererTurbo[35];
		gunModel[0] = new ModelRendererTurbo(this, 159, 42, textureX, textureY); // body1
		gunModel[1] = new ModelRendererTurbo(this, 1, 56, textureX, textureY); // grip1
		gunModel[2] = new ModelRendererTurbo(this, 88, 19, textureX, textureY); // mainBarrelBottom
		gunModel[3] = new ModelRendererTurbo(this, 88, 10, textureX, textureY); // mainBarrelMiddle
		gunModel[4] = new ModelRendererTurbo(this, 88, 1, textureX, textureY); // mainBarrelTop
		gunModel[5] = new ModelRendererTurbo(this, 163, 1, textureX, textureY); // pin1
		gunModel[6] = new ModelRendererTurbo(this, 163, 1, textureX, textureY); // pin1-2
		gunModel[7] = new ModelRendererTurbo(this, 163, 1, textureX, textureY); // pin1-3
		gunModel[8] = new ModelRendererTurbo(this, 1, 45, textureX, textureY); // Box 0
		gunModel[9] = new ModelRendererTurbo(this, 44, 45, textureX, textureY); // Box 1
		gunModel[10] = new ModelRendererTurbo(this, 55, 56, textureX, textureY); // Box 2
		gunModel[11] = new ModelRendererTurbo(this, 1, 68, textureX, textureY); // Box 3
		gunModel[12] = new ModelRendererTurbo(this, 38, 68, textureX, textureY); // Box 4
		gunModel[13] = new ModelRendererTurbo(this, 1, 88, textureX, textureY); // Box 5
		gunModel[14] = new ModelRendererTurbo(this, 59, 88, textureX, textureY); // Box 6
		gunModel[15] = new ModelRendererTurbo(this, 59, 68, textureX, textureY); // Box 7
		gunModel[16] = new ModelRendererTurbo(this, 38, 88, textureX, textureY); // Box 8
		gunModel[17] = new ModelRendererTurbo(this, 34, 56, textureX, textureY); // Box 10
		gunModel[18] = new ModelRendererTurbo(this, 1, 24, textureX, textureY); // Box 11
		gunModel[19] = new ModelRendererTurbo(this, 88, 41, textureX, textureY); // Box 12
		gunModel[20] = new ModelRendererTurbo(this, 46, 34, textureX, textureY); // Box 15
		gunModel[21] = new ModelRendererTurbo(this, 1, 34, textureX, textureY); // Box 16
		gunModel[22] = new ModelRendererTurbo(this, 88, 72, textureX, textureY); // Box 0
		gunModel[23] = new ModelRendererTurbo(this, 139, 74, textureX, textureY); // Box 1
		gunModel[24] = new ModelRendererTurbo(this, 48, 9, textureX, textureY); // Box 20
		gunModel[25] = new ModelRendererTurbo(this, 48, 1, textureX, textureY); // Box 21
		gunModel[26] = new ModelRendererTurbo(this, 28, 22, textureX, textureY); // Box 22
		gunModel[27] = new ModelRendererTurbo(this, 28, 28, textureX, textureY); // Box 23
		gunModel[28] = new ModelRendererTurbo(this, 33, 3, textureX, textureY); // Box 25
		gunModel[29] = new ModelRendererTurbo(this, 33, 3, textureX, textureY); // Box 26
		gunModel[30] = new ModelRendererTurbo(this, 33, 3, textureX, textureY); // Box 27
		gunModel[31] = new ModelRendererTurbo(this, 48, 26, textureX, textureY); // Box 28
		gunModel[32] = new ModelRendererTurbo(this, 48, 21, textureX, textureY); // Box 29
		gunModel[33] = new ModelRendererTurbo(this, 59, 22, textureX, textureY); // Box 30
		gunModel[34] = new ModelRendererTurbo(this, 70, 22, textureX, textureY); // Box 31

		gunModel[0].addShapeBox(0F, 0F, 0F, 25, 2, 8, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body1
		gunModel[0].setRotationPoint(-10F, -16F, -4F);

		gunModel[1].addShapeBox(0F, 0F, 0F, 8, 3, 8, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F); // grip1
		gunModel[1].setRotationPoint(-7F, -11F, -4F);

		gunModel[2].addShapeBox(0F, 0F, 0F, 22, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // mainBarrelBottom
		gunModel[2].setRotationPoint(17F, -21.5F, -3F);

		gunModel[3].addBox(0F, 0F, 0F, 22, 2, 6, 0F); // mainBarrelMiddle
		gunModel[3].setRotationPoint(17F, -23.5F, -3F);

		gunModel[4].addShapeBox(0F, 0F, 0F, 22, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // mainBarrelTop
		gunModel[4].setRotationPoint(17F, -25.5F, -3F);

		gunModel[5].addShapeBox(0F, 0F, 0F, 12, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // pin1
		gunModel[5].setRotationPoint(26.5F, -19F, -1.5F);

		gunModel[6].addBox(0F, 0F, 0F, 12, 1, 3, 0F); // pin1-2
		gunModel[6].setRotationPoint(26.5F, -18F, -1.5F);

		gunModel[7].addShapeBox(0F, 0F, 0F, 12, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // pin1-3
		gunModel[7].setRotationPoint(26.5F, -17F, -1.5F);

		gunModel[8].addShapeBox(0F, 0F, 0F, 13, 2, 8, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 0
		gunModel[8].setRotationPoint(-7F, -13F, -4F);

		gunModel[9].addShapeBox(0F, 0F, 0F, 2, 2, 8, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F); // Box 1
		gunModel[9].setRotationPoint(-9F, -13F, -4F);

		gunModel[10].addShapeBox(0F, 0F, 0F, 2, 3, 8, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 2F, 0F, -2F, -2F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, -2F); // Box 2
		gunModel[10].setRotationPoint(-9F, -11F, -4F);

		gunModel[11].addShapeBox(0F, 0F, 0F, 10, 11, 8, 0F, -4F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 3
		gunModel[11].setRotationPoint(-13F, -8F, -4F);

		gunModel[12].addShapeBox(0F, 0F, 0F, 2, 11, 8, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 4F, 0F, -2F, -4F, 0F, 0F, -4F, 0F, 0F, 4F, 0F, -2F); // Box 4
		gunModel[12].setRotationPoint(-11F, -8F, -4F);

		gunModel[13].addBox(0F, 0F, 0F, 10, 4, 8, 0F); // Box 5
		gunModel[13].setRotationPoint(-13F, 3F, -4F);

		gunModel[14].addShapeBox(0F, 0F, 0F, 2, 4, 8, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, -2F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, -2F); // Box 6
		gunModel[14].setRotationPoint(-15F, 3F, -4F);

		gunModel[15].addShapeBox(0F, 0F, 0F, 2, 11, 8, 0F, -4F, 0F, 0F, 4F, 0F, -2F, 4F, 0F, -2F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F); // Box 7
		gunModel[15].setRotationPoint(-3F, -8F, -4F);

		gunModel[16].addShapeBox(0F, 0F, 0F, 2, 4, 8, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F); // Box 8
		gunModel[16].setRotationPoint(-3F, 3F, -4F);

		gunModel[17].addShapeBox(0F, 0F, 0F, 2, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, -2F, -3F, 0F, -2F, 3F, 0F, 0F); // Box 10
		gunModel[17].setRotationPoint(4F, -11F, -4F);

		gunModel[18].addShapeBox(0F, 0F, 0F, 5, 1, 8, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, -1F); // Box 11
		gunModel[18].setRotationPoint(-15F, -17F, -4F);

		gunModel[19].addBox(0F, 0F, 0F, 27, 3, 8, 0F); // Box 12
		gunModel[19].setRotationPoint(-10F, -19F, -4F);

		gunModel[20].addShapeBox(0F, 0F, 0F, 2, 1, 8, 0F, 1F, 0F, -2F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F); // Box 15
		gunModel[20].setRotationPoint(-9F, -14F, -4F);

		gunModel[21].addShapeBox(0F, 0F, 0F, 14, 1, 8, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 16
		gunModel[21].setRotationPoint(-7F, -14F, -4F);

		gunModel[22].addBox(0F, 0F, 0F, 18, 3, 7, 0F); // Box 0
		gunModel[22].setRotationPoint(15F, -19F, -3.5F);

		gunModel[23].addShapeBox(0F, 0F, 0F, 18, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F); // Box 1
		gunModel[23].setRotationPoint(15F, -16F, -3.5F);

		gunModel[24].addBox(0F, 0F, 0F, 9, 5, 6, 0F); // Box 20
		gunModel[24].setRotationPoint(7F, -24.5F, -3F);

		gunModel[25].addShapeBox(0F, 0F, 0F, 9, 1, 6, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 21
		gunModel[25].setRotationPoint(7F, -25.5F, -3F);

		gunModel[26].addShapeBox(0F, 0F, 0F, 5, 2, 3, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 22
		gunModel[26].setRotationPoint(-15F, -19F, -4F);

		gunModel[27].addShapeBox(0F, 0F, 0F, 5, 2, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 23
		gunModel[27].setRotationPoint(-15F, -19F, 1F);

		gunModel[28].addShapeBox(0F, 0F, 0F, 4, 1, 2, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 25
		gunModel[28].setRotationPoint(-14F, -25F, -1F);

		gunModel[29].addShapeBox(0F, 0F, 0F, 4, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F); // Box 26
		gunModel[29].setRotationPoint(-14F, -23F, -1F);

		gunModel[30].addBox(0F, 0F, 0F, 4, 1, 2, 0F); // Box 27
		gunModel[30].setRotationPoint(-14F, -24F, -1F);

		gunModel[31].addBox(0F, 0F, 0F, 14, 3, 1, 0F); // Box 28
		gunModel[31].setRotationPoint(2.5F, -19F, 3.5F);

		gunModel[32].addShapeBox(0F, 0F, 0F, 4, 3, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F); // Box 29
		gunModel[32].setRotationPoint(3F, -19F, 4F);

		gunModel[33].addBox(0F, 0F, 0F, 4, 2, 1, 0F); // Box 30
		gunModel[33].setRotationPoint(-4F, -18F, 3.5F);

		gunModel[34].addBox(0F, 0F, 0F, 2, 2, 1, 0F); // Box 31
		gunModel[34].setRotationPoint(0F, -18F, 4F);


		ammoModel = new ModelRendererTurbo[5];
		ammoModel[0] = new ModelRendererTurbo(this, 59, 107, textureX, textureY); // bullet
		ammoModel[1] = new ModelRendererTurbo(this, 59, 102, textureX, textureY); // bulletTip
		ammoModel[2] = new ModelRendererTurbo(this, 1, 102, textureX, textureY); // grip3
		ammoModel[3] = new ModelRendererTurbo(this, 40, 102, textureX, textureY); // Box 9
		ammoModel[4] = new ModelRendererTurbo(this, 1, 112, textureX, textureY); // Box 24

		ammoModel[0].addShapeBox(0F, 0F, 0F, 6, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // bullet
		ammoModel[0].setRotationPoint(-7F, -11F, -1.5F);

		ammoModel[1].addShapeBox(0F, 0F, 0F, 2, 1, 3, 0F, 0F, 0F, -1F, 0F, -0.5F, -1F, 0F, -0.5F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F); // bulletTip
		ammoModel[1].setRotationPoint(-1F, -11F, -1.5F);

		ammoModel[2].addBox(0F, 0F, 0F, 12, 2, 7, 0F); // grip3
		ammoModel[2].setRotationPoint(-14F, 7F, -3.5F);

		ammoModel[3].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F); // Box 9
		ammoModel[3].setRotationPoint(-2F, 7F, -3.5F);

		ammoModel[4].addShapeBox(0F, 0F, 0F, 10, 17, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 5F, 0F, 0F, -5F, 0F, 0F, -5F, 0F, 0F, 5F, 0F, 0F); // Box 24
		ammoModel[4].setRotationPoint(-8F, -10F, -2.5F);


		slideModel = new ModelRendererTurbo[21];
		slideModel[0] = new ModelRendererTurbo(this, 37, 7, textureX, textureY); // ironSight1
		slideModel[1] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // ironSight2
		slideModel[2] = new ModelRendererTurbo(this, 20, 13, textureX, textureY); // Box 13
		slideModel[3] = new ModelRendererTurbo(this, 1, 13, textureX, textureY); // Box 14
		slideModel[4] = new ModelRendererTurbo(this, 88, 28, textureX, textureY); // Box 17
		slideModel[5] = new ModelRendererTurbo(this, 88, 63, textureX, textureY); // Box 2
		slideModel[6] = new ModelRendererTurbo(this, 137, 63, textureX, textureY); // Box 3
		slideModel[7] = new ModelRendererTurbo(this, 137, 53, textureX, textureY); // Box 4
		slideModel[8] = new ModelRendererTurbo(this, 88, 53, textureX, textureY); // Box 5
		slideModel[9] = new ModelRendererTurbo(this, 145, 15, textureX, textureY); // Box 6
		slideModel[10] = new ModelRendererTurbo(this, 167, 17, textureX, textureY); // Box 7
		slideModel[11] = new ModelRendererTurbo(this, 145, 1, textureX, textureY); // Box 8
		slideModel[12] = new ModelRendererTurbo(this, 170, 6, textureX, textureY); // Box 9
		slideModel[13] = new ModelRendererTurbo(this, 139, 28, textureX, textureY); // Box 10
		slideModel[14] = new ModelRendererTurbo(this, 190, 29, textureX, textureY); // Box 11
		slideModel[15] = new ModelRendererTurbo(this, 190, 35, textureX, textureY); // Box 12
		slideModel[16] = new ModelRendererTurbo(this, 37, 16, textureX, textureY); // Box 13
		slideModel[17] = new ModelRendererTurbo(this, 37, 11, textureX, textureY); // Box 14
		slideModel[18] = new ModelRendererTurbo(this, 22, 8, textureX, textureY); // Box 16
		slideModel[19] = new ModelRendererTurbo(this, 1, 7, textureX, textureY); // Box 18
		slideModel[20] = new ModelRendererTurbo(this, 22, 2, textureX, textureY); // Box 19

		slideModel[0].addShapeBox(0F, 0F, 0F, 4, 2, 1, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // ironSight1
		slideModel[0].setRotationPoint(33F, -28F, -0.5F);

		slideModel[1].addShapeBox(0F, 0F, 0F, 7, 2, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // ironSight2
		slideModel[1].setRotationPoint(-9F, -27F, -3.5F);

		slideModel[2].addShapeBox(0F, 0F, 0F, 5, 4, 3, 0F, -2F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 13
		slideModel[2].setRotationPoint(-15F, -23F, -4F);

		slideModel[3].addShapeBox(0F, 0F, 0F, 5, 4, 3, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 14
		slideModel[3].setRotationPoint(-15F, -23F, 1F);

		slideModel[4].addBox(0F, 0F, 0F, 17, 4, 8, 0F); // Box 17
		slideModel[4].setRotationPoint(-10F, -23F, -4F);

		slideModel[5].addShapeBox(0F, 0F, 0F, 17, 1, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 2
		slideModel[5].setRotationPoint(-10F, -26F, -3.5F);

		slideModel[6].addShapeBox(0F, 0F, 0F, 17, 1, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 3
		slideModel[6].setRotationPoint(16F, -26F, -3.5F);

		slideModel[7].addBox(0F, 0F, 0F, 17, 2, 7, 0F); // Box 4
		slideModel[7].setRotationPoint(-10F, -25F, -3.5F);

		slideModel[8].addBox(0F, 0F, 0F, 17, 2, 7, 0F); // Box 5
		slideModel[8].setRotationPoint(16F, -25F, -3.5F);

		slideModel[9].addBox(0F, 0F, 0F, 5, 3, 5, 0F); // Box 6
		slideModel[9].setRotationPoint(33F, -19F, -2.5F);

		slideModel[10].addShapeBox(0F, 0F, 0F, 5, 1, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 7
		slideModel[10].setRotationPoint(33F, -16F, -2.5F);

		slideModel[11].addBox(0F, 0F, 0F, 5, 6, 7, 0F); // Box 8
		slideModel[11].setRotationPoint(33F, -25F, -3.5F);

		slideModel[12].addShapeBox(0F, 0F, 0F, 5, 1, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 9
		slideModel[12].setRotationPoint(33F, -26F, -3.5F);

		slideModel[13].addBox(0F, 0F, 0F, 17, 4, 8, 0F); // Box 10
		slideModel[13].setRotationPoint(16F, -23F, -4F);

		slideModel[14].addBox(0F, 0F, 0F, 9, 4, 1, 0F); // Box 11
		slideModel[14].setRotationPoint(7F, -23F, -4F);

		slideModel[15].addBox(0F, 0F, 0F, 9, 4, 1, 0F); // Box 12
		slideModel[15].setRotationPoint(7F, -23F, 3F);

		slideModel[16].addShapeBox(0F, 0F, 0F, 3, 2, 2, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F); // Box 13
		slideModel[16].setRotationPoint(-13F, -25F, 1.5F);

		slideModel[17].addShapeBox(0F, 0F, 0F, 3, 2, 2, 0F, -1F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 14
		slideModel[17].setRotationPoint(-13F, -25F, -3.5F);

		slideModel[18].addShapeBox(0F, 0F, 0F, 2, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 16
		slideModel[18].setRotationPoint(-9F, -28F, -3.5F);

		slideModel[19].addShapeBox(0F, 0F, 0F, 7, 2, 3, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 18
		slideModel[19].setRotationPoint(-9F, -27F, 0.5F);

		slideModel[20].addShapeBox(0F, 0F, 0F, 2, 1, 3, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 2F, 0F, -0.5F, 2F, 0F, 0F, 0F, 0F, 0F); // Box 19
		slideModel[20].setRotationPoint(-9F, -28F, 0.5F);

		barrelAttachPoint = new Vector3f(39F /16F, 22.5F /16F, 0F /16F);
		gripAttachPoint = new Vector3f(25 /16F, 18F /16F, 0F /16F);

		gunSlideDistance = 1.2F;
		animationType = EnumAnimationType.CUSTOM;

		/* ----Start of Reload Block---- */
		/* Reload Name: Alternate Pistol Clip */
		rotateGunVertical = 10F;
		translateGun = new Vector3f(0.15F, 0.25F, 0F);

		rotateClipVertical = 5F;
		translateClip = new Vector3f(-0.5F, -3F, 0F);
		/* ----End of Reload Block---- */


		translateAll(0F, 0F, 0F);


		flipAll();
	}
}