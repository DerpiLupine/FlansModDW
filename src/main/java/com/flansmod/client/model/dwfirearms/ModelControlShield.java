package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelControlShield extends ModelGun
{
	int textureX = 512;
	int textureY = 256;

	public ModelControlShield()
	{
		gunModel = new ModelRendererTurbo[7];
		gunModel[0] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 0
		gunModel[1] = new ModelRendererTurbo(this, 44, 15, textureX, textureY); // Box 2
		gunModel[2] = new ModelRendererTurbo(this, 44, 15, textureX, textureY); // Box 3
		gunModel[3] = new ModelRendererTurbo(this, 53, 8, textureX, textureY); // Box 4
		gunModel[4] = new ModelRendererTurbo(this, 53, 8, textureX, textureY); // Box 5
		gunModel[5] = new ModelRendererTurbo(this, 53, 31, textureX, textureY); // Box 6
		gunModel[6] = new ModelRendererTurbo(this, 53, 43, textureX, textureY); // Box 7

		gunModel[0].addBox(0F, 0F, 0F, 1, 40, 20, 0F); // Box 0
		gunModel[0].setRotationPoint(2F, -16F, -10F);

		gunModel[1].addShapeBox(0F, 0F, 0F, 2, 44, 2, 0F,0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 2
		gunModel[1].setRotationPoint(1.5F, -18F, -12F);

		gunModel[2].addShapeBox(0F, 0F, 0F, 2, 44, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F); // Box 3
		gunModel[2].setRotationPoint(1.5F, -18F, 10F);

		gunModel[3].addBox(0F, 0F, 0F, 2, 2, 20, 0F); // Box 4
		gunModel[3].setRotationPoint(1.5F, -18F, -10F);

		gunModel[4].addBox(0F, 0F, 0F, 2, 2, 20, 0F); // Box 5
		gunModel[4].setRotationPoint(1.5F, 24F, -10F);

		gunModel[5].addBox(0F, 0F, 0F, 3, 9, 22, 0F); // Box 6
		gunModel[5].setRotationPoint(1F, -5F, -11F);

		gunModel[6].addBox(0F, 0F, 0F, 1, 7, 2, 0F); // Box 7
		gunModel[6].setRotationPoint(0F, -4F, -8F);

		translateAll(0F, 0F, 0F);

		//THIS IS A SHIELD.
		//Owner:"DerpiWolf"


		flipAll();
	}
}