package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.ModelAttachment;
import com.flansmod.client.tmt.ModelRendererTurbo;

import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelLPMRedDotSight extends ModelAttachment
{
	int textureX = 512;
	int textureY = 256;

	public ModelLPMRedDotSight()
	{
		attachmentModel = new ModelRendererTurbo[13];
		attachmentModel[0] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // mounter
		attachmentModel[1] = new ModelRendererTurbo(this, 1, 14, textureX, textureY); // mounterBase
		attachmentModel[2] = new ModelRendererTurbo(this, 1, 26, textureX, textureY); // mounterBase3
		attachmentModel[3] = new ModelRendererTurbo(this, 36, 14, textureX, textureY); // mounterBase2
		attachmentModel[4] = new ModelRendererTurbo(this, 24, 27, textureX, textureY); // scope4
		attachmentModel[5] = new ModelRendererTurbo(this, 45, 30, textureX, textureY); // scope3
		attachmentModel[6] = new ModelRendererTurbo(this, 45, 30, textureX, textureY); // scope3
		attachmentModel[7] = new ModelRendererTurbo(this, 45, 30, textureX, textureY); // scope3
		attachmentModel[8] = new ModelRendererTurbo(this, 45, 30, textureX, textureY); // scope3
		attachmentModel[9] = new ModelRendererTurbo(this, 38, 31, textureX, textureY); // sight
		attachmentModel[10] = new ModelRendererTurbo(this, 22, 36, textureX, textureY); // scope1
		attachmentModel[11] = new ModelRendererTurbo(this, 22, 36, textureX, textureY); // scope1
		attachmentModel[12] = new ModelRendererTurbo(this, 1, 36, textureX, textureY); // scope2

		attachmentModel[0].addBox(-10F, -3F, -4.5F, 20, 3, 9, 0F); // mounter
		attachmentModel[0].setRotationPoint(0F, 0F, 0F);

		attachmentModel[1].addShapeBox(-10F, -5F, -4.5F, 8, 2, 9, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // mounterBase
		attachmentModel[1].setRotationPoint(0F, 0F, 0F);

		attachmentModel[2].addBox(7F, -6F, -4F, 3, 1, 8, 0F); // mounterBase3
		attachmentModel[2].setRotationPoint(0F, 0F, 0F);

		attachmentModel[3].addBox(7F, -5F, -4.5F, 3, 2, 9, 0F); // mounterBase2
		attachmentModel[3].setRotationPoint(0F, 0F, 0F);

		attachmentModel[4].addShapeBox(7F, -15F, -3.5F, 3, 1, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope4
		attachmentModel[4].setRotationPoint(0F, 0F, 0F);

		attachmentModel[5].addShapeBox(7F, -14F, -3.5F, 3, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F); // scope3
		attachmentModel[5].setRotationPoint(0F, 0F, 0F);

		attachmentModel[6].addShapeBox(7F, -10F, 4.5F, 3, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F); // scope3
		attachmentModel[6].setRotationPoint(0F, 0F, 0F);

		attachmentModel[7].addShapeBox(7F, -14F, 4.5F, 3, 4, 1, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope3
		attachmentModel[7].setRotationPoint(0F, 0F, 0F);

		attachmentModel[8].addShapeBox(7F, -10F, -3.5F, 3, 4, 1, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope3
		attachmentModel[8].setRotationPoint(0F, 0F, 0F);

		attachmentModel[9].addBox(8F, -10.5F, -0.5F, 1, 1, 1, 0F); // sight
		attachmentModel[9].setRotationPoint(0F, 0F, 0F);

		attachmentModel[10].addShapeBox(-1F, -4.5F, -3.5F, 2, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F); // scope1
		attachmentModel[10].setRotationPoint(0F, 0F, 0F);

		attachmentModel[11].addShapeBox(4F, -4.5F, -3.5F, 2, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F); // scope1
		attachmentModel[11].setRotationPoint(0F, 0F, 0F);

		attachmentModel[12].addBox(1F, -4.5F, -3.5F, 3, 2, 7, 0F); // scope2
		attachmentModel[12].setRotationPoint(0F, 0F, 0F);


		renderOffset = 0F;

		flipAll();
	}
}