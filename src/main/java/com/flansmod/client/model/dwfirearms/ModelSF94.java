package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelSF94 extends ModelGun
{
	int textureX = 512;
	int textureY = 256;

	public ModelSF94()
	{
		gunModel = new ModelRendererTurbo[42];
		gunModel[0] = new ModelRendererTurbo(this, 72, 32, textureX, textureY); // Import body1
		gunModel[1] = new ModelRendererTurbo(this, 72, 53, textureX, textureY); // Import body10
		gunModel[2] = new ModelRendererTurbo(this, 99, 143, textureX, textureY); // Import body11
		gunModel[3] = new ModelRendererTurbo(this, 99, 149, textureX, textureY); // Import body12
		gunModel[4] = new ModelRendererTurbo(this, 72, 88, textureX, textureY); // Import body2
		gunModel[5] = new ModelRendererTurbo(this, 72, 103, textureX, textureY); // Import body3
		gunModel[6] = new ModelRendererTurbo(this, 72, 118, textureX, textureY); // Import body4
		gunModel[7] = new ModelRendererTurbo(this, 72, 129, textureX, textureY); // Import body5
		gunModel[8] = new ModelRendererTurbo(this, 72, 141, textureX, textureY); // Import body6
		gunModel[9] = new ModelRendererTurbo(this, 112, 146, textureX, textureY); // Import body7
		gunModel[10] = new ModelRendererTurbo(this, 72, 75, textureX, textureY); // Import body8
		gunModel[11] = new ModelRendererTurbo(this, 72, 64, textureX, textureY); // Import body9
		gunModel[12] = new ModelRendererTurbo(this, 153, 2, textureX, textureY); // Import gasBlock1
		gunModel[13] = new ModelRendererTurbo(this, 153, 2, textureX, textureY); // Import gasBlock1-2
		gunModel[14] = new ModelRendererTurbo(this, 153, 2, textureX, textureY); // Import gasBlock1-3
		gunModel[15] = new ModelRendererTurbo(this, 153, 32, textureX, textureY); // Import gasBlockConnector
		gunModel[16] = new ModelRendererTurbo(this, 153, 11, textureX, textureY); // Import gasBlockConnector2
		gunModel[17] = new ModelRendererTurbo(this, 153, 21, textureX, textureY); // Import gasBlockConnector2-2
		gunModel[18] = new ModelRendererTurbo(this, 153, 11, textureX, textureY); // Import gasBlockConnector2-3
		gunModel[19] = new ModelRendererTurbo(this, 1, 63, textureX, textureY); // Import grip1
		gunModel[20] = new ModelRendererTurbo(this, 1, 35, textureX, textureY); // Import grip2
		gunModel[21] = new ModelRendererTurbo(this, 1, 77, textureX, textureY); // Import grip3
		gunModel[22] = new ModelRendererTurbo(this, 144, 106, textureX, textureY); // Import handGuard1
		gunModel[23] = new ModelRendererTurbo(this, 144, 88, textureX, textureY); // Import handGuard2
		gunModel[24] = new ModelRendererTurbo(this, 144, 119, textureX, textureY); // Import handGuard3
		gunModel[25] = new ModelRendererTurbo(this, 144, 119, textureX, textureY); // Import handGuard3-2
		gunModel[26] = new ModelRendererTurbo(this, 181, 123, textureX, textureY); // Import handGuard4
		gunModel[27] = new ModelRendererTurbo(this, 181, 123, textureX, textureY); // Import handGuard4-2
		gunModel[28] = new ModelRendererTurbo(this, 72, 20, textureX, textureY); // Import mainBarrelBottom
		gunModel[29] = new ModelRendererTurbo(this, 72, 11, textureX, textureY); // Import mainBarrelMiddle
		gunModel[30] = new ModelRendererTurbo(this, 72, 2, textureX, textureY); // Import mainBarrelTop
		gunModel[31] = new ModelRendererTurbo(this, 72, 29, textureX, textureY); // Import pipeCleaner
		gunModel[32] = new ModelRendererTurbo(this, 178, 11, textureX, textureY); // Import ring1
		gunModel[33] = new ModelRendererTurbo(this, 178, 11, textureX, textureY); // Import ring1-2
		gunModel[34] = new ModelRendererTurbo(this, 178, 21, textureX, textureY); // Import ring2
		gunModel[35] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Import scopeBase
		gunModel[36] = new ModelRendererTurbo(this, 1, 77, textureX, textureY); // Import grip4
		gunModel[37] = new ModelRendererTurbo(this, 144, 135, textureX, textureY); // Import Box71
		gunModel[38] = new ModelRendererTurbo(this, 144, 135, textureX, textureY); // Import Box72
		gunModel[39] = new ModelRendererTurbo(this, 144, 135, textureX, textureY); // Import Box73
		gunModel[40] = new ModelRendererTurbo(this, 144, 135, textureX, textureY); // Import Box74
		gunModel[41] = new ModelRendererTurbo(this, 72, 88, textureX, textureY); // Import clipPart

		gunModel[0].addBox(0F, 0F, 0F, 25, 10, 10, 0F); // Import body1
		gunModel[0].setRotationPoint(-10F, -17F, -5F);

		gunModel[1].addShapeBox(0F, 0F, 0F, 40, 2, 8, 0F, -1F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, -1F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import body10
		gunModel[1].setRotationPoint(-8F, -23F, -4F);

		gunModel[2].addShapeBox(0F, 0F, 0F, 3, 2, 3, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import body11
		gunModel[2].setRotationPoint(-9F, -22F, -1.5F);

		gunModel[3].addBox(0F, 0F, 0F, 12, 3, 1, 0F); // Import body12
		gunModel[3].setRotationPoint(14F, -16F, 4.5F);

		gunModel[4].addBox(0F, 0F, 0F, 20, 5, 9, 0F); // Import body2
		gunModel[4].setRotationPoint(15F, -12F, -4.5F);

		gunModel[5].addBox(0F, 0F, 0F, 20, 4, 10, 0F); // Import body3
		gunModel[5].setRotationPoint(15F, -16F, -5F);

		gunModel[6].addBox(0F, 0F, 0F, 20, 1, 9, 0F); // Import body4
		gunModel[6].setRotationPoint(15F, -17F, -4F);

		gunModel[7].addShapeBox(0F, 0F, 0F, 14, 2, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import body5
		gunModel[7].setRotationPoint(15F, -19F, -4F);

		gunModel[8].addShapeBox(0F, 0F, 0F, 3, 2, 10, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import body6
		gunModel[8].setRotationPoint(29F, -19F, -5F);

		gunModel[9].addBox(0F, 0F, 0F, 6, 1, 1, 0F); // Import body7
		gunModel[9].setRotationPoint(29F, -17F, -5F);

		gunModel[10].addShapeBox(0F, 0F, 0F, 25, 2, 10, 0F, -1F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, -1F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import body8
		gunModel[10].setRotationPoint(-10F, -19F, -5F);

		gunModel[11].addShapeBox(0F, 0F, 0F, 41, 2, 8, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import body9
		gunModel[11].setRotationPoint(-9F, -21F, -4F);

		gunModel[12].addShapeBox(0F, 0F, 0F, 12, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import gasBlock1
		gunModel[12].setRotationPoint(58F, -22F, -3F);

		gunModel[13].addBox(0F, 0F, 0F, 12, 2, 6, 0F); // Import gasBlock1-2
		gunModel[13].setRotationPoint(58F, -20F, -3F);

		gunModel[14].addShapeBox(0F, 0F, 0F, 12, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Import gasBlock1-3
		gunModel[14].setRotationPoint(58F, -18F, -3F);

		gunModel[15].addBox(0F, 0F, 0F, 5, 1, 3, 0F); // Import gasBlockConnector
		gunModel[15].setRotationPoint(65F, -16F, -1.5F);

		gunModel[16].addShapeBox(0F, 0F, 0F, 5, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import gasBlockConnector2
		gunModel[16].setRotationPoint(65F, -15F, -3.5F);

		gunModel[17].addBox(0F, 0F, 0F, 5, 3, 7, 0F); // Import gasBlockConnector2-2
		gunModel[17].setRotationPoint(65F, -13F, -3.5F);

		gunModel[18].addShapeBox(0F, 0F, 0F, 5, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Import gasBlockConnector2-3
		gunModel[18].setRotationPoint(65F, -10F, -3.5F);

		gunModel[19].addShapeBox(0F, 0F, 0F, 12, 3, 10, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F); // Import grip1
		gunModel[19].setRotationPoint(-7F, -7F, -5F);

		gunModel[20].addShapeBox(0F, 0F, 0F, 11, 12, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 4F, 0F, 0F); // Import grip2
		gunModel[20].setRotationPoint(-6F, -4F, -5F);

		gunModel[21].addShapeBox(0F, 0F, 0F, 11, 2, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F); // Import grip3
		gunModel[21].setRotationPoint(-10F, 8F, -5F);

		gunModel[22].addBox(0F, 0F, 0F, 14, 5, 7, 0F); // Import handGuard1
		gunModel[22].setRotationPoint(44F, -22F, -3.5F);

		gunModel[23].addBox(0F, 0F, 0F, 23, 10, 7, 0F); // Import handGuard2
		gunModel[23].setRotationPoint(35F, -17F, -3.5F);

		gunModel[24].addShapeBox(0F, 0F, 0F, 14, 13, 2, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import handGuard3
		gunModel[24].setRotationPoint(44F, -21F, -5.5F);

		gunModel[25].addShapeBox(0F, 0F, 0F, 14, 13, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F); // Import handGuard3-2
		gunModel[25].setRotationPoint(44F, -21F, 3.5F);

		gunModel[26].addShapeBox(0F, 0F, 0F, 9, 9, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F); // Import handGuard4
		gunModel[26].setRotationPoint(35F, -17F, 3.5F);

		gunModel[27].addShapeBox(0F, 0F, 0F, 9, 9, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import handGuard4-2
		gunModel[27].setRotationPoint(35F, -17F, -5.5F);

		gunModel[28].addShapeBox(0F, 0F, 0F, 34, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Import mainBarrelBottom
		gunModel[28].setRotationPoint(58F, -10.5F, -3F);

		gunModel[29].addBox(0F, 0F, 0F, 34, 2, 6, 0F); // Import mainBarrelMiddle
		gunModel[29].setRotationPoint(58F, -12.5F, -3F);

		gunModel[30].addShapeBox(0F, 0F, 0F, 34, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import mainBarrelTop
		gunModel[30].setRotationPoint(58F, -14.5F, -3F);

		gunModel[31].addBox(0F, 0F, 0F, 31, 1, 1, 0F); // Import pipeCleaner
		gunModel[31].setRotationPoint(58F, -8F, -0.5F);

		gunModel[32].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import ring1
		gunModel[32].setRotationPoint(86F, -15F, -3.5F);

		gunModel[33].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Import ring1-2
		gunModel[33].setRotationPoint(86F, -10F, -3.5F);

		gunModel[34].addBox(0F, 0F, 0F, 3, 3, 7, 0F); // Import ring2
		gunModel[34].setRotationPoint(86F, -13F, -3.5F);

		gunModel[35].addBox(0F, 0F, 0F, 12, 5, 9, 0F); // Import scopeBase
		gunModel[35].setRotationPoint(32F, -22F, -4.5F);

		gunModel[36].addShapeBox(0F, 0F, 0F, 11, 2, 10, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F); // Import grip4
		gunModel[36].setRotationPoint(-10F, 10F, -5F);

		gunModel[37].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Import Box71
		gunModel[37].setRotationPoint(36F, -7F, -3.5F);

		gunModel[38].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Import Box72
		gunModel[38].setRotationPoint(42F, -7F, -3.5F);

		gunModel[39].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Import Box73
		gunModel[39].setRotationPoint(48F, -7F, -3.5F);

		gunModel[40].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Import Box74
		gunModel[40].setRotationPoint(54F, -7F, -3.5F);

		gunModel[41].addBox(0F, 0F, 0F, 3, 5, 9, 0F); // Import clipPart
		gunModel[41].setRotationPoint(12F, -7F, -4.5F);


		defaultScopeModel = new ModelRendererTurbo[6];
		defaultScopeModel[0] = new ModelRendererTurbo(this, 35, 2, textureX, textureY); // Import ironSight1
		defaultScopeModel[1] = new ModelRendererTurbo(this, 35, 2, textureX, textureY); // Import ironSight1-2
		defaultScopeModel[2] = new ModelRendererTurbo(this, 35, 20, textureX, textureY); // Import ironSight2
		defaultScopeModel[3] = new ModelRendererTurbo(this, 30, 28, textureX, textureY); // Import ironSight3
		defaultScopeModel[4] = new ModelRendererTurbo(this, 1, 28, textureX, textureY); // Import ironSight4
		defaultScopeModel[5] = new ModelRendererTurbo(this, 1, 16, textureX, textureY); // Import ironSightBase

		defaultScopeModel[0].addBox(0F, 0F, 0F, 3, 6, 1, 0F); // Import ironSight1
		defaultScopeModel[0].setRotationPoint(41F, -30F, 2F);

		defaultScopeModel[1].addBox(0F, 0F, 0F, 3, 6, 1, 0F); // Import ironSight1-2
		defaultScopeModel[1].setRotationPoint(41F, -30F, -3F);

		defaultScopeModel[2].addBox(0F, 0F, 0F, 2, 2, 1, 0F); // Import ironSight2
		defaultScopeModel[2].setRotationPoint(41F, -27F, -0.5F);

		defaultScopeModel[3].addBox(0F, 0F, 0F, 3, 2, 4, 0F); // Import ironSight3
		defaultScopeModel[3].setRotationPoint(41F, -25.5F, -2F);

		defaultScopeModel[4].addBox(0F, 0F, 0F, 9, 1, 5, 0F); // Import ironSight4
		defaultScopeModel[4].setRotationPoint(32F, -25F, -2.5F);

		defaultScopeModel[5].addShapeBox(0F, 0F, 0F, 12, 2, 9, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import ironSightBase
		defaultScopeModel[5].setRotationPoint(32F, -24F, -4.5F);


		defaultStockModel = new ModelRendererTurbo[13];
		defaultStockModel[0] = new ModelRendererTurbo(this, 72, 217, textureX, textureY); // Import stock1
		defaultStockModel[1] = new ModelRendererTurbo(this, 72, 191, textureX, textureY); // Import stock2
		defaultStockModel[2] = new ModelRendererTurbo(this, 111, 200, textureX, textureY); // Import stock3
		defaultStockModel[3] = new ModelRendererTurbo(this, 111, 214, textureX, textureY); // Import stock4
		defaultStockModel[4] = new ModelRendererTurbo(this, 111, 214, textureX, textureY); // Import stock4-2
		defaultStockModel[5] = new ModelRendererTurbo(this, 136, 206, textureX, textureY); // Import stock5
		defaultStockModel[6] = new ModelRendererTurbo(this, 136, 206, textureX, textureY); // Import stock5-2
		defaultStockModel[7] = new ModelRendererTurbo(this, 166, 173, textureX, textureY); // Import stockEnd1
		defaultStockModel[8] = new ModelRendererTurbo(this, 166, 173, textureX, textureY); // Import stockEnd1-2
		defaultStockModel[9] = new ModelRendererTurbo(this, 166, 160, textureX, textureY); // Import stockEnd2
		defaultStockModel[10] = new ModelRendererTurbo(this, 72, 160, textureX, textureY); // Import stockPipe1
		defaultStockModel[11] = new ModelRendererTurbo(this, 72, 170, textureX, textureY); // Import stockPipe2
		defaultStockModel[12] = new ModelRendererTurbo(this, 72, 181, textureX, textureY); // Import stockPipe3

		defaultStockModel[0].addBox(0F, 0F, 0F, 16, 5, 3, 0F); // Import stock1
		defaultStockModel[0].setRotationPoint(-41F, -8.5F, -1.5F);

		defaultStockModel[1].addShapeBox(0F, 0F, 0F, 12, 18, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -8F, 0F, 0F, -8F, 0F, 0F, 0F, 0F); // Import stock2
		defaultStockModel[1].setRotationPoint(-53F, -13F, -3.5F);

		defaultStockModel[2].addBox(0F, 0F, 0F, 4, 5, 8, 0F); // Import stock3
		defaultStockModel[2].setRotationPoint(-24F, -14.5F, -4F);

		defaultStockModel[3].addBox(0F, 0F, 0F, 18, 2, 8, 0F); // Import stock4
		defaultStockModel[3].setRotationPoint(-38F, -16.5F, -4F);

		defaultStockModel[4].addShapeBox(0F, 0F, 0F, 18, 2, 8, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import stock4-2
		defaultStockModel[4].setRotationPoint(-38F, -18.5F, -4F);

		defaultStockModel[5].addShapeBox(0F, 0F, 0F, 6, 6, 1, 0F, -3F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import stock5
		defaultStockModel[5].setRotationPoint(-41F, -14.5F, 3F);

		defaultStockModel[6].addShapeBox(0F, 0F, 0F, 6, 6, 1, 0F, -3F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import stock5-2
		defaultStockModel[6].setRotationPoint(-41F, -14.5F, -4F);

		defaultStockModel[7].addShapeBox(0F, 0F, 0F, 4, 2, 8, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import stockEnd1
		defaultStockModel[7].setRotationPoint(-14F, -15.5F, -4F);

		defaultStockModel[8].addShapeBox(0F, 0F, 0F, 4, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Import stockEnd1-2
		defaultStockModel[8].setRotationPoint(-14F, -9.5F, -4F);

		defaultStockModel[9].addBox(0F, 0F, 0F, 4, 4, 8, 0F); // Import stockEnd2
		defaultStockModel[9].setRotationPoint(-14F, -13.5F, -4F);

		defaultStockModel[10].addShapeBox(0F, 0F, 0F, 39, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import stockPipe1
		defaultStockModel[10].setRotationPoint(-53F, -15F, -3.5F);

		defaultStockModel[11].addBox(0F, 0F, 0F, 27, 3, 7, 0F); // Import stockPipe2
		defaultStockModel[11].setRotationPoint(-41F, -13F, -3.5F);

		defaultStockModel[12].addShapeBox(0F, 0F, 0F, 27, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Import stockPipe3
		defaultStockModel[12].setRotationPoint(-41F, -10F, -3.5F);


		ammoModel = new ModelRendererTurbo[6];
		ammoModel[0] = new ModelRendererTurbo(this, 16, 167, textureX, textureY); // bullet
		ammoModel[1] = new ModelRendererTurbo(this, 1, 167, textureX, textureY); // bulletTip
		ammoModel[2] = new ModelRendererTurbo(this, 1, 152, textureX, textureY); // clip1
		ammoModel[3] = new ModelRendererTurbo(this, 1, 134, textureX, textureY); // Box 0
		ammoModel[4] = new ModelRendererTurbo(this, 1, 113, textureX, textureY); // Box 2
		ammoModel[5] = new ModelRendererTurbo(this, 1, 90, textureX, textureY); // Box 3

		ammoModel[0].addShapeBox(0F, 0F, 0F, 6, 1, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // bullet
		ammoModel[0].setRotationPoint(16F, -13F, -2F);

		ammoModel[1].addShapeBox(0F, 0F, 0F, 3, 1, 4, 0F, 0F, 0F, -1F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // bulletTip
		ammoModel[1].setRotationPoint(22F, -13F, -2F);

		ammoModel[2].addBox(0F, 0F, 0F, 11, 6, 8, 0F); // clip1
		ammoModel[2].setRotationPoint(15F, -12F, -4F);

		ammoModel[3].addShapeBox(0F, 0F, 0F, 11, 9, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 1F, -3F, 0F, 1F, -3F, 0F, -1F, 0F, 0F); // Box 0
		ammoModel[3].setRotationPoint(15F, -6F, -4F);

		ammoModel[4].addShapeBox(0F, 0F, 0F, 11, 12, 8, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, -4F, -2F, 0F, 3F, -7F, 0F, 3F, -7F, 0F, -4F, -2F, 0F); // Box 2
		ammoModel[4].setRotationPoint(16F, 3F, -4F);

		ammoModel[5].addShapeBox(0F, 0F, 0F, 11, 14, 8, 0F, 0F, 0F, 0F, -1F, 5F, 0F, -1F, 5F, 0F, 0F, 0F, 0F, -8F, -3F, 0F, 6F, -10F, 0F, 6F, -10F, 0F, -8F, -3F, 0F); // Box 3
		ammoModel[5].setRotationPoint(20F, 13F, -4F);


		slideModel = new ModelRendererTurbo[2];
		slideModel[0] = new ModelRendererTurbo(this, 48, 10, textureX, textureY); // Import bolt1
		slideModel[1] = new ModelRendererTurbo(this, 48, 5, textureX, textureY); // Import bolt2

		slideModel[0].addBox(0F, 0F, 0F, 1, 3, 3, 0F); // Import bolt1
		slideModel[0].setRotationPoint(28F, -19F, -7F);

		slideModel[1].addBox(0F, 0F, 0F, 3, 3, 1, 0F); // Import bolt2
		slideModel[1].setRotationPoint(25F, -19F, -4.5F);

		barrelAttachPoint = new Vector3f(92F /16F, 11.5F /16F, 0F /16F);
		stockAttachPoint = new Vector3f(-10F /16F, 12F /16F, 0F /16F);
		scopeAttachPoint = new Vector3f(40F /16F, 22F /16F, 0F /16F);
		gripAttachPoint = new Vector3f(46 /16F, 8F /16F, 0F /16F);

		gunSlideDistance = 0.5F;
		animationType = EnumAnimationType.BOTTOM_CLIP;


		translateAll(0F, 0F, 0F);


		flipAll();
	}
}