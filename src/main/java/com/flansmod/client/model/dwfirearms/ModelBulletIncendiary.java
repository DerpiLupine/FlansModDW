package com.flansmod.client.model.dwfirearms;

import net.minecraft.entity.Entity;

import com.flansmod.client.model.ModelBullet;
import com.flansmod.client.tmt.ModelRendererTurbo;

public class ModelBulletIncendiary extends ModelBullet 
{
	public ModelRendererTurbo[] bulletModel;
	
	public ModelBulletIncendiary()
	{
		int textureX = 64;
		int textureY = 32;

		bulletModel = new ModelRendererTurbo[1];
		
		bulletModel[0] = new ModelRendererTurbo(this, 0, 5, textureX, textureY);
		bulletModel[0].addShapeBox(0F, 0F, 0F, 1, 3, 1, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -1.5F, -0.5F, 0F, -1.5F, -0.5F, 0F, -1.5F, 0F, -0.5F, -1.5F, 0F);
	}
	
	@Override
	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
	{
		//GL11.glScalef(0.5F, 0.5F, 0.5F);
		for(ModelRendererTurbo mrt : bulletModel)
			mrt.render(f5);
	}

	//uses the bullet map texture.
}