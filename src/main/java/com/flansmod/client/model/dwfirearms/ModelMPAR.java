package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelMPAR extends ModelGun
{
	int textureX = 512;
	int textureY = 256;

	public ModelMPAR()	
	{
		gunModel = new ModelRendererTurbo[68];
		gunModel[0] = new ModelRendererTurbo(this, 112, 1, textureX, textureY); // barrelMainLeft
		gunModel[1] = new ModelRendererTurbo(this, 112, 10, textureX, textureY); // barrelMainMiddle
		gunModel[2] = new ModelRendererTurbo(this, 112, 1, textureX, textureY); // barrelMainRight
		gunModel[3] = new ModelRendererTurbo(this, 112, 120, textureX, textureY); // body1
		gunModel[4] = new ModelRendererTurbo(this, 200, 141, textureX, textureY); // body6
		gunModel[5] = new ModelRendererTurbo(this, 185, 95, textureX, textureY); // Import GU,body8
		gunModel[6] = new ModelRendererTurbo(this, 112, 19, textureX, textureY); // Import GU,body9
		gunModel[7] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // railPart3
		gunModel[8] = new ModelRendererTurbo(this, 79, 11, textureX, textureY); // railPart1
		gunModel[9] = new ModelRendererTurbo(this, 58, 11, textureX, textureY); // railPart12
		gunModel[10] = new ModelRendererTurbo(this, 79, 11, textureX, textureY); // railPart2
		gunModel[11] = new ModelRendererTurbo(this, 79, 11, textureX, textureY); // railPart3
		gunModel[12] = new ModelRendererTurbo(this, 79, 11, textureX, textureY); // railPart4
		gunModel[13] = new ModelRendererTurbo(this, 79, 11, textureX, textureY); // railPart5
		gunModel[14] = new ModelRendererTurbo(this, 58, 11, textureX, textureY); // railPart10
		gunModel[15] = new ModelRendererTurbo(this, 58, 11, textureX, textureY); // railPart11
		gunModel[16] = new ModelRendererTurbo(this, 1, 81, textureX, textureY); // Box 32
		gunModel[17] = new ModelRendererTurbo(this, 40, 81, textureX, textureY); // Box 33
		gunModel[18] = new ModelRendererTurbo(this, 1, 27, textureX, textureY); // Box 34
		gunModel[19] = new ModelRendererTurbo(this, 1, 39, textureX, textureY); // Box 35
		gunModel[20] = new ModelRendererTurbo(this, 1, 70, textureX, textureY); // Box 38
		gunModel[21] = new ModelRendererTurbo(this, 40, 27, textureX, textureY); // Box 39
		gunModel[22] = new ModelRendererTurbo(this, 38, 39, textureX, textureY); // Box 40
		gunModel[23] = new ModelRendererTurbo(this, 36, 70, textureX, textureY); // Box 42
		gunModel[24] = new ModelRendererTurbo(this, 112, 148, textureX, textureY); // Box 0
		gunModel[25] = new ModelRendererTurbo(this, 112, 133, textureX, textureY); // Box 1
		gunModel[26] = new ModelRendererTurbo(this, 112, 59, textureX, textureY); // Box 3
		gunModel[27] = new ModelRendererTurbo(this, 112, 107, textureX, textureY); // Box 4
		gunModel[28] = new ModelRendererTurbo(this, 112, 175, textureX, textureY); // Box 6
		gunModel[29] = new ModelRendererTurbo(this, 112, 161, textureX, textureY); // Box 7
		gunModel[30] = new ModelRendererTurbo(this, 112, 190, textureX, textureY); // Box 8
		gunModel[31] = new ModelRendererTurbo(this, 112, 87, textureX, textureY); // Box 9
		gunModel[32] = new ModelRendererTurbo(this, 112, 97, textureX, textureY); // Box 10
		gunModel[33] = new ModelRendererTurbo(this, 161, 61, textureX, textureY); // Box 11
		gunModel[34] = new ModelRendererTurbo(this, 112, 76, textureX, textureY); // Box 12
		gunModel[35] = new ModelRendererTurbo(this, 145, 190, textureX, textureY); // Box 20
		gunModel[36] = new ModelRendererTurbo(this, 145, 161, textureX, textureY); // Box 21
		gunModel[37] = new ModelRendererTurbo(this, 145, 175, textureX, textureY); // Box 22
		gunModel[38] = new ModelRendererTurbo(this, 112, 45, textureX, textureY); // Box 23
		gunModel[39] = new ModelRendererTurbo(this, 1, 11, textureX, textureY); // Box 24
		gunModel[40] = new ModelRendererTurbo(this, 58, 11, textureX, textureY); // Box 25
		gunModel[41] = new ModelRendererTurbo(this, 191, 141, textureX, textureY); // Box 28
		gunModel[42] = new ModelRendererTurbo(this, 184, 64, textureX, textureY); // Box 29
		gunModel[43] = new ModelRendererTurbo(this, 184, 54, textureX, textureY); // Box 30
		gunModel[44] = new ModelRendererTurbo(this, 1, 93, textureX, textureY); // Box 32
		gunModel[45] = new ModelRendererTurbo(this, 173, 151, textureX, textureY); // Box 33
		gunModel[46] = new ModelRendererTurbo(this, 198, 151, textureX, textureY); // Box 34
		gunModel[47] = new ModelRendererTurbo(this, 80, 61, textureX, textureY); // Box 38
		gunModel[48] = new ModelRendererTurbo(this, 71, 62, textureX, textureY); // Box 39
		gunModel[49] = new ModelRendererTurbo(this, 71, 62, textureX, textureY); // Box 40
		gunModel[50] = new ModelRendererTurbo(this, 26, 94, textureX, textureY); // Box 45
		gunModel[51] = new ModelRendererTurbo(this, 79, 11, textureX, textureY); // Box 49
		gunModel[52] = new ModelRendererTurbo(this, 79, 11, textureX, textureY); // Box 50
		gunModel[53] = new ModelRendererTurbo(this, 181, 133, textureX, textureY); // Box 58
		gunModel[54] = new ModelRendererTurbo(this, 61, 29, textureX, textureY); // Box 94
		gunModel[55] = new ModelRendererTurbo(this, 71, 62, textureX, textureY); // Box 118
		gunModel[56] = new ModelRendererTurbo(this, 80, 61, textureX, textureY); // Box 119
		gunModel[57] = new ModelRendererTurbo(this, 71, 62, textureX, textureY); // Box 120
		gunModel[58] = new ModelRendererTurbo(this, 112, 33, textureX, textureY); // Box 121
		gunModel[59] = new ModelRendererTurbo(this, 112, 242, textureX, textureY); // Box 128
		gunModel[60] = new ModelRendererTurbo(this, 1, 55, textureX, textureY); // Box 131
		gunModel[61] = new ModelRendererTurbo(this, 38, 55, textureX, textureY); // Box 132
		gunModel[62] = new ModelRendererTurbo(this, 196, 162, textureX, textureY); // Box 133
		gunModel[63] = new ModelRendererTurbo(this, 196, 162, textureX, textureY); // Box 134
		gunModel[64] = new ModelRendererTurbo(this, 196, 162, textureX, textureY); // Box 135
		gunModel[65] = new ModelRendererTurbo(this, 173, 45, textureX, textureY); // Box 136
		gunModel[66] = new ModelRendererTurbo(this, 26, 94, textureX, textureY); // Box 2
		gunModel[67] = new ModelRendererTurbo(this, 26, 94, textureX, textureY); // Box 3

		gunModel[0].addShapeBox(0F, 0F, 0F, 45, 6, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F); // barrelMainLeft
		gunModel[0].setRotationPoint(39F, -17F, 1F);

		gunModel[1].addBox(0F, 0F, 0F, 45, 6, 2, 0F); // barrelMainMiddle
		gunModel[1].setRotationPoint(39F, -17F, -1F);

		gunModel[2].addShapeBox(0F, 0F, 0F, 45, 6, 2, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // barrelMainRight
		gunModel[2].setRotationPoint(39F, -17F, -3F);

		gunModel[3].addShapeBox(0F, 0F, 0F, 47, 2, 9, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body1
		gunModel[3].setRotationPoint(-14F, -21F, -4.5F);

		gunModel[4].addShapeBox(0F, 0F, 0F, 4, 4, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, 0F, 0F, 0F); // body6
		gunModel[4].setRotationPoint(4F, -16F, -5F);

		gunModel[5].addShapeBox(0F, 0F, 0F, 4, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F); // Import GU,body8
		gunModel[5].setRotationPoint(-14F, -8F, -4F);

		gunModel[6].addBox(0F, 0F, 0F, 47, 1, 10, 0F); // Import GU,body9
		gunModel[6].setRotationPoint(-14F, -17F, -5F);

		gunModel[7].addBox(0F, 0F, 0F, 39, 2, 7, 0F); // railPart3
		gunModel[7].setRotationPoint(-7F, -22F, -3.5F);

		gunModel[8].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // railPart1
		gunModel[8].setRotationPoint(5F, -24F, -3.5F);

		gunModel[9].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // railPart12
		gunModel[9].setRotationPoint(53F, -2F, -3.5F);

		gunModel[10].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // railPart2
		gunModel[10].setRotationPoint(11F, -24F, -3.5F);

		gunModel[11].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // railPart3
		gunModel[11].setRotationPoint(17F, -24F, -3.5F);

		gunModel[12].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // railPart4
		gunModel[12].setRotationPoint(23F, -24F, -3.5F);

		gunModel[13].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // railPart5
		gunModel[13].setRotationPoint(29F, -24F, -3.5F);

		gunModel[14].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // railPart10
		gunModel[14].setRotationPoint(41F, -2F, -3.5F);

		gunModel[15].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // railPart11
		gunModel[15].setRotationPoint(47F, -2F, -3.5F);

		gunModel[16].addShapeBox(0F, 0F, 0F, 11, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F); // Box 32
		gunModel[16].setRotationPoint(-11F, -5F, -4F);

		gunModel[17].addBox(0F, 0F, 0F, 6, 3, 8, 0F); // Box 33
		gunModel[17].setRotationPoint(0F, -5F, -4F);

		gunModel[18].addShapeBox(0F, 0F, 0F, 10, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 34
		gunModel[18].setRotationPoint(-10F, -2F, -4F);

		gunModel[19].addShapeBox(0F, 0F, 0F, 10, 7, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 4F, 0F, 0F); // Box 35
		gunModel[19].setRotationPoint(-10F, 1F, -4F);

		gunModel[20].addShapeBox(0F, 0F, 0F, 9, 2, 8, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F); // Box 38
		gunModel[20].setRotationPoint(-16F, 14F, -4F);

		gunModel[21].addShapeBox(0F, 0F, 0F, 2, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F); // Box 39
		gunModel[21].setRotationPoint(0F, -2F, -4F);

		gunModel[22].addShapeBox(0F, 0F, 0F, 2, 7, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 4F, 0F, 0F, -4F, 0F, -1.5F, -4F, 0F, -1.5F, 4F, 0F, 0F); // Box 40
		gunModel[22].setRotationPoint(0F, 1F, -4F);

		gunModel[23].addShapeBox(0F, 0F, 0F, 2, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F); // Box 42
		gunModel[23].setRotationPoint(-7F, 14F, -4F);

		gunModel[24].addBox(0F, 0F, 0F, 22, 4, 8, 0F); // Box 0
		gunModel[24].setRotationPoint(-14F, -12F, -4F);

		gunModel[25].addShapeBox(0F, 0F, 0F, 29, 4, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F); // Box 1
		gunModel[25].setRotationPoint(4F, -12F, -5F);

		gunModel[26].addShapeBox(0F, 0F, 0F, 16, 8, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F); // Box 3
		gunModel[26].setRotationPoint(14F, -5F, -4F);

		gunModel[27].addBox(0F, 0F, 0F, 47, 4, 8, 0F); // Box 4
		gunModel[27].setRotationPoint(-14F, -16F, -3F);

		gunModel[28].addBox(0F, 0F, 0F, 6, 4, 10, 0F); // Box 6
		gunModel[28].setRotationPoint(34F, -16F, -5F);

		gunModel[29].addShapeBox(0F, 0F, 0F, 6, 3, 10, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 7
		gunModel[29].setRotationPoint(34F, -19F, -5F);

		gunModel[30].addShapeBox(0F, 0F, 0F, 6, 3, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 8
		gunModel[30].setRotationPoint(34F, -12F, -5F);

		gunModel[31].addShapeBox(0F, 0F, 0F, 29, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 9
		gunModel[31].setRotationPoint(34F, -21F, -3.5F);

		gunModel[32].addBox(0F, 0F, 0F, 29, 2, 7, 0F); // Box 10
		gunModel[32].setRotationPoint(34F, -19F, -3.5F);

		gunModel[33].addShapeBox(0F, 0F, 0F, 3, 6, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F); // Box 11
		gunModel[33].setRotationPoint(30F, -5F, -4F);

		gunModel[34].addBox(0F, 0F, 0F, 29, 3, 7, 0F); // Box 12
		gunModel[34].setRotationPoint(34F, -11F, -3.5F);

		gunModel[35].addShapeBox(0F, 0F, 0F, 15, 3, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 20
		gunModel[35].setRotationPoint(57F, -12F, -5F);

		gunModel[36].addShapeBox(0F, 0F, 0F, 15, 3, 10, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 21
		gunModel[36].setRotationPoint(57F, -19F, -5F);

		gunModel[37].addBox(0F, 0F, 0F, 15, 4, 10, 0F); // Box 22
		gunModel[37].setRotationPoint(57F, -16F, -5F);

		gunModel[38].addBox(0F, 0F, 0F, 22, 4, 8, 0F); // Box 23
		gunModel[38].setRotationPoint(34F, -8F, -4F);

		gunModel[39].addShapeBox(0F, 0F, 0F, 21, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 24
		gunModel[39].setRotationPoint(35F, -4F, -3.5F);

		gunModel[40].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 25
		gunModel[40].setRotationPoint(35F, -2F, -3.5F);

		gunModel[41].addShapeBox(0F, 0F, 0F, 2, 4, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 28
		gunModel[41].setRotationPoint(31F, -16F, -5F);

		gunModel[42].addShapeBox(0F, 0F, 0F, 4, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F); // Box 29
		gunModel[42].setRotationPoint(56F, -7F, -4F);

		gunModel[43].addBox(0F, 0F, 0F, 4, 1, 8, 0F); // Box 30
		gunModel[43].setRotationPoint(56F, -8F, -4F);

		gunModel[44].addBox(0F, 0F, 0F, 5, 11, 7, 0F); // Box 32
		gunModel[44].setRotationPoint(63F, -19F, -3.5F);

		gunModel[45].addShapeBox(0F, 0F, 0F, 5, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 33
		gunModel[45].setRotationPoint(63F, -21F, -3.5F);

		gunModel[46].addShapeBox(0F, 0F, 0F, 8, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 34
		gunModel[46].setRotationPoint(60F, -8F, -3.5F);

		gunModel[47].addBox(0F, 0F, 0F, 2, 7, 3, 0F); // Box 38
		gunModel[47].setRotationPoint(72F, -17.5F, -1.5F);

		gunModel[48].addShapeBox(0F, 0F, 0F, 2, 7, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F); // Box 39
		gunModel[48].setRotationPoint(72F, -17.5F, 1.5F);

		gunModel[49].addShapeBox(0F, 0F, 0F, 2, 7, 2, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 40
		gunModel[49].setRotationPoint(72F, -17.5F, -3.5F);

		gunModel[50].addShapeBox(0F, 0F, 0F, 8, 6, 2, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 45
		gunModel[50].setRotationPoint(7F, -17F, -4F);

		gunModel[51].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 49
		gunModel[51].setRotationPoint(-7F, -24F, -3.5F);

		gunModel[52].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 50
		gunModel[52].setRotationPoint(-1F, -24F, -3.5F);

		gunModel[53].addBox(0F, 0F, 0F, 18, 4, 2, 0F); // Box 58
		gunModel[53].setRotationPoint(-14F, -16F, -5F);

		gunModel[54].addBox(0F, 0F, 0F, 6, 3, 6, 0F); // Box 94
		gunModel[54].setRotationPoint(-13F, -24F, -3F);

		gunModel[55].addShapeBox(0F, 0F, 0F, 2, 7, 2, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 118
		gunModel[55].setRotationPoint(76F, -17.5F, -3.5F);

		gunModel[56].addBox(0F, 0F, 0F, 2, 7, 3, 0F); // Box 119
		gunModel[56].setRotationPoint(76F, -17.5F, -1.5F);

		gunModel[57].addShapeBox(0F, 0F, 0F, 2, 7, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F); // Box 120
		gunModel[57].setRotationPoint(76F, -17.5F, 1.5F);

		gunModel[58].addBox(0F, 0F, 0F, 43, 3, 8, 0F); // Box 121
		gunModel[58].setRotationPoint(-10F, -8F, -4F);

		gunModel[59].addBox(0F, 0F, 0F, 47, 2, 9, 0F); // Box 128
		gunModel[59].setRotationPoint(-14F, -19F, -4.5F);

		gunModel[60].addShapeBox(0F, 0F, 0F, 10, 6, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, -3F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 2F, -3F, 0F); // Box 131
		gunModel[60].setRotationPoint(-14F, 8F, -4F);

		gunModel[61].addShapeBox(0F, 0F, 0F, 2, 6, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, -1.5F, -3F, 0F, -1.5F, 3F, 0F, 0F); // Box 132
		gunModel[61].setRotationPoint(-4F, 8F, -4F);

		gunModel[62].addShapeBox(0F, 0F, 0F, 1, 3, 9, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 133
		gunModel[62].setRotationPoint(33F, -18.5F, -4.5F);

		gunModel[63].addBox(0F, 0F, 0F, 1, 3, 9, 0F); // Box 134
		gunModel[63].setRotationPoint(33F, -15.5F, -4.5F);

		gunModel[64].addShapeBox(0F, 0F, 0F, 1, 3, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 135
		gunModel[64].setRotationPoint(33F, -12.5F, -4.5F);

		gunModel[65].addBox(0F, 0F, 0F, 1, 7, 6, 0F); // Box 136
		gunModel[65].setRotationPoint(33F, -11.5F, -3F);

		gunModel[66].addShapeBox(0F, 0F, 0F, 8, 6, 2, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 2
		gunModel[66].setRotationPoint(15F, -17F, -4F);

		gunModel[67].addShapeBox(0F, 0F, 0F, 8, 6, 2, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 3
		gunModel[67].setRotationPoint(23F, -17F, -4F);


		defaultBarrelModel = new ModelRendererTurbo[3];
		defaultBarrelModel[0] = new ModelRendererTurbo(this, 71, 72, textureX, textureY); // Box 41
		defaultBarrelModel[1] = new ModelRendererTurbo(this, 71, 83, textureX, textureY); // Box 42
		defaultBarrelModel[2] = new ModelRendererTurbo(this, 71, 83, textureX, textureY); // Box 43

		defaultBarrelModel[0].addBox(0F, 0F, 0F, 12, 7, 3, 0F); // Box 41
		defaultBarrelModel[0].setRotationPoint(84F, -17.5F, -1.5F);

		defaultBarrelModel[1].addShapeBox(0F, 0F, 0F, 12, 7, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F); // Box 42
		defaultBarrelModel[1].setRotationPoint(84F, -17.5F, 1.5F);

		defaultBarrelModel[2].addShapeBox(0F, 0F, 0F, 12, 7, 2, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 43
		defaultBarrelModel[2].setRotationPoint(84F, -17.5F, -3.5F);


		defaultScopeModel = new ModelRendererTurbo[21];
		defaultScopeModel[0] = new ModelRendererTurbo(this, 80, 39, textureX, textureY); // Box 106
		defaultScopeModel[1] = new ModelRendererTurbo(this, 64, 23, textureX, textureY); // Box 107
		defaultScopeModel[2] = new ModelRendererTurbo(this, 86, 29, textureX, textureY); // Box 108
		defaultScopeModel[3] = new ModelRendererTurbo(this, 95, 22, textureX, textureY); // Box 110
		defaultScopeModel[4] = new ModelRendererTurbo(this, 73, 23, textureX, textureY); // Box 111
		defaultScopeModel[5] = new ModelRendererTurbo(this, 73, 26, textureX, textureY); // Box 112
		defaultScopeModel[6] = new ModelRendererTurbo(this, 84, 22, textureX, textureY); // Box 113
		defaultScopeModel[7] = new ModelRendererTurbo(this, 75, 44, textureX, textureY); // Box 115
		defaultScopeModel[8] = new ModelRendererTurbo(this, 68, 40, textureX, textureY); // Box 116
		defaultScopeModel[9] = new ModelRendererTurbo(this, 61, 39, textureX, textureY); // Box 117
		defaultScopeModel[10] = new ModelRendererTurbo(this, 71, 44, textureX, textureY); // Box 118
		defaultScopeModel[11] = new ModelRendererTurbo(this, 61, 44, textureX, textureY); // Box 119
		defaultScopeModel[12] = new ModelRendererTurbo(this, 66, 44, textureX, textureY); // Box 120
		defaultScopeModel[13] = new ModelRendererTurbo(this, 53, 22, textureX, textureY); // Box 93
		defaultScopeModel[14] = new ModelRendererTurbo(this, 37, 26, textureX, textureY); // Box 95
		defaultScopeModel[15] = new ModelRendererTurbo(this, 61, 48, textureX, textureY); // Box 0
		defaultScopeModel[16] = new ModelRendererTurbo(this, 80, 39, textureX, textureY); // Box 1
		defaultScopeModel[17] = new ModelRendererTurbo(this, 93, 44, textureX, textureY); // Box 4
		defaultScopeModel[18] = new ModelRendererTurbo(this, 93, 44, textureX, textureY); // Box 6
		defaultScopeModel[19] = new ModelRendererTurbo(this, 80, 44, textureX, textureY); // Box 7
		defaultScopeModel[20] = new ModelRendererTurbo(this, 80, 44, textureX, textureY); // Box 8

		defaultScopeModel[0].addShapeBox(0F, 0F, 0F, 7, 1, 3, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 106
		defaultScopeModel[0].setRotationPoint(63F, -27F, -1.5F);

		defaultScopeModel[1].addShapeBox(0F, 0F, 0F, 2, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F); // Box 107
		defaultScopeModel[1].setRotationPoint(67F, -31F, -0.5F);

		defaultScopeModel[2].addShapeBox(0F, 0F, 0F, 4, 3, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F); // Box 108
		defaultScopeModel[2].setRotationPoint(-11F, -27F, -3F);

		defaultScopeModel[3].addBox(0F, 0F, 0F, 4, 5, 1, 0F); // Box 110
		defaultScopeModel[3].setRotationPoint(-11F, -34F, 2F);

		defaultScopeModel[4].addShapeBox(0F, 0F, 0F, 4, 1, 1, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 111
		defaultScopeModel[4].setRotationPoint(-11F, -35F, 2F);

		defaultScopeModel[5].addShapeBox(0F, 0F, 0F, 4, 1, 1, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 112
		defaultScopeModel[5].setRotationPoint(-11F, -35F, -3F);

		defaultScopeModel[6].addBox(0F, 0F, 0F, 4, 5, 1, 0F); // Box 113
		defaultScopeModel[6].setRotationPoint(-11F, -34F, -3F);

		defaultScopeModel[7].addShapeBox(0F, 0F, 0F, 1, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F, 1F); // Box 115
		defaultScopeModel[7].setRotationPoint(-10F, -32F, -2F);

		defaultScopeModel[8].addBox(0F, 0F, 0F, 1, 1, 2, 0F); // Box 116
		defaultScopeModel[8].setRotationPoint(-10F, -34F, -1F);

		defaultScopeModel[9].addBox(0F, 0F, 0F, 1, 2, 2, 0F); // Box 117
		defaultScopeModel[9].setRotationPoint(-10F, -31F, -1F);

		defaultScopeModel[10].addShapeBox(0F, 0F, 0F, 1, 2, 1, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 118
		defaultScopeModel[10].setRotationPoint(-10F, -34F, -2F);

		defaultScopeModel[11].addShapeBox(0F, 0F, 0F, 1, 2, 1, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 119
		defaultScopeModel[11].setRotationPoint(-10F, -34F, 1F);

		defaultScopeModel[12].addShapeBox(0F, 0F, 0F, 1, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 120
		defaultScopeModel[12].setRotationPoint(-10F, -32F, 1F);

		defaultScopeModel[13].addBox(0F, 0F, 0F, 2, 9, 3, 0F); // Box 93
		defaultScopeModel[13].setRotationPoint(68F, -25F, -1.5F);

		defaultScopeModel[14].addShapeBox(0F, 0F, 0F, 2, 5, 3, 0F, -4F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 95
		defaultScopeModel[14].setRotationPoint(59F, -26F, -1.5F);

		defaultScopeModel[15].addBox(0F, 0F, 0F, 4, 2, 6, 0F); // Box 0
		defaultScopeModel[15].setRotationPoint(-11F, -29F, -3F);

		defaultScopeModel[16].addBox(0F, 0F, 0F, 7, 1, 3, 0F); // Box 1
		defaultScopeModel[16].setRotationPoint(63F, -26F, -1.5F);

		defaultScopeModel[17].addShapeBox(0F, 0F, 0F, 4, 2, 2, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 4
		defaultScopeModel[17].setRotationPoint(-11F, -29F, -4.5F);

		defaultScopeModel[18].addShapeBox(0F, 0F, 0F, 4, 2, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 6
		defaultScopeModel[18].setRotationPoint(-11F, -27F, -4.5F);

		defaultScopeModel[19].addShapeBox(0F, 0F, 0F, 4, 2, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 7
		defaultScopeModel[19].setRotationPoint(-11F, -27F, 2.5F);

		defaultScopeModel[20].addShapeBox(0F, 0F, 0F, 4, 2, 2, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 8
		defaultScopeModel[20].setRotationPoint(-11F, -29F, 2.5F);


		defaultStockModel = new ModelRendererTurbo[30];
		defaultStockModel[0] = new ModelRendererTurbo(this, 1, 214, textureX, textureY); // stockPart1
		defaultStockModel[1] = new ModelRendererTurbo(this, 1, 135, textureX, textureY); // stockPart2
		defaultStockModel[2] = new ModelRendererTurbo(this, 58, 135, textureX, textureY); // stockPart3
		defaultStockModel[3] = new ModelRendererTurbo(this, 1, 182, textureX, textureY); // stockPart4
		defaultStockModel[4] = new ModelRendererTurbo(this, 1, 112, textureX, textureY); // stockPart7
		defaultStockModel[5] = new ModelRendererTurbo(this, 1, 125, textureX, textureY); // Box 53
		defaultStockModel[6] = new ModelRendererTurbo(this, 58, 125, textureX, textureY); // Box 54
		defaultStockModel[7] = new ModelRendererTurbo(this, 55, 166, textureX, textureY); // Box 55
		defaultStockModel[8] = new ModelRendererTurbo(this, 28, 152, textureX, textureY); // Box 56
		defaultStockModel[9] = new ModelRendererTurbo(this, 1, 149, textureX, textureY); // Box 88
		defaultStockModel[10] = new ModelRendererTurbo(this, 28, 166, textureX, textureY); // Box 89
		defaultStockModel[11] = new ModelRendererTurbo(this, 55, 154, textureX, textureY); // Box 90
		defaultStockModel[12] = new ModelRendererTurbo(this, 28, 185, textureX, textureY); // Box 91
		defaultStockModel[13] = new ModelRendererTurbo(this, 45, 202, textureX, textureY); // Box 92
		defaultStockModel[14] = new ModelRendererTurbo(this, 176, 206, textureX, textureY); // Box 98
		defaultStockModel[15] = new ModelRendererTurbo(this, 112, 204, textureX, textureY); // Box 99
		defaultStockModel[16] = new ModelRendererTurbo(this, 175, 205, textureX, textureY); // Box 100
		defaultStockModel[17] = new ModelRendererTurbo(this, 147, 210, textureX, textureY); // Box 101
		defaultStockModel[18] = new ModelRendererTurbo(this, 199, 205, textureX, textureY); // Box 102
		defaultStockModel[19] = new ModelRendererTurbo(this, 172, 223, textureX, textureY); // Box 0
		defaultStockModel[20] = new ModelRendererTurbo(this, 112, 223, textureX, textureY); // Box 1
		defaultStockModel[21] = new ModelRendererTurbo(this, 170, 221, textureX, textureY); // Box 2
		defaultStockModel[22] = new ModelRendererTurbo(this, 145, 227, textureX, textureY); // Box 3
		defaultStockModel[23] = new ModelRendererTurbo(this, 194, 221, textureX, textureY); // Box 4
		defaultStockModel[24] = new ModelRendererTurbo(this, 66, 104, textureX, textureY); // Box 115
		defaultStockModel[25] = new ModelRendererTurbo(this, 66, 115, textureX, textureY); // Box 116
		defaultStockModel[26] = new ModelRendererTurbo(this, 66, 115, textureX, textureY); // Box 117
		defaultStockModel[27] = new ModelRendererTurbo(this, 45, 189, textureX, textureY); // Box 122
		defaultStockModel[28] = new ModelRendererTurbo(this, 45, 189, textureX, textureY); // Box 123
		defaultStockModel[29] = new ModelRendererTurbo(this, 45, 189, textureX, textureY); // Box 124

		defaultStockModel[0].addBox(0F, 0F, 0F, 22, 7, 10, 0F); // stockPart1
		defaultStockModel[0].setRotationPoint(-48F, -19F, -5F);

		defaultStockModel[1].addBox(0F, 0F, 0F, 10, 5, 8, 0F); // stockPart2
		defaultStockModel[1].setRotationPoint(-36F, -12F, -4F);

		defaultStockModel[2].addShapeBox(0F, 0F, 0F, 12, 5, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 10F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 10F, 0F); // stockPart3
		defaultStockModel[2].setRotationPoint(-48F, -12F, -4F);

		defaultStockModel[3].addBox(0F, 0F, 0F, 3, 21, 10, 0F); // stockPart4
		defaultStockModel[3].setRotationPoint(-56F, -19F, -5F);

		defaultStockModel[4].addShapeBox(0F, 0F, 0F, 22, 2, 10, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // stockPart7
		defaultStockModel[4].setRotationPoint(-48F, -21F, -5F);

		defaultStockModel[5].addShapeBox(0F, 0F, 0F, 10, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 53
		defaultStockModel[5].setRotationPoint(-36F, -7F, -4F);

		defaultStockModel[6].addShapeBox(0F, 0F, 0F, 12, 1, 8, 0F, 0F, -10F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -10F, 0F, 0F, 10F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 10F, -1F); // Box 54
		defaultStockModel[6].setRotationPoint(-48F, -7F, -4F);

		defaultStockModel[7].addShapeBox(0F, 0F, 0F, 3, 2, 10, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 55
		defaultStockModel[7].setRotationPoint(-56F, -21F, -5F);

		defaultStockModel[8].addShapeBox(0F, 0F, 0F, 3, 3, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 56
		defaultStockModel[8].setRotationPoint(-56F, 2F, -5F);

		defaultStockModel[9].addBox(0F, 0F, 0F, 3, 22, 10, 0F); // Box 88
		defaultStockModel[9].setRotationPoint(-51F, -19F, -5F);

		defaultStockModel[10].addShapeBox(0F, 0F, 0F, 3, 2, 10, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 89
		defaultStockModel[10].setRotationPoint(-51F, -21F, -5F);

		defaultStockModel[11].addShapeBox(0F, 0F, 0F, 3, 1, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 90
		defaultStockModel[11].setRotationPoint(-51F, 3F, -5F);

		defaultStockModel[12].addBox(0F, 0F, 0F, 2, 22, 6, 0F); // Box 91
		defaultStockModel[12].setRotationPoint(-53F, -19F, -3F);

		defaultStockModel[13].addBox(0F, 0F, 0F, 12, 5, 6, 0F); // Box 92
		defaultStockModel[13].setRotationPoint(-42F, -6F, -3F);

		defaultStockModel[14].addShapeBox(0F, 0F, 0F, 5, 2, 12, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 98
		defaultStockModel[14].setRotationPoint(-40F, -21.5F, -6F);

		defaultStockModel[15].addBox(0F, 0F, 0F, 5, 7, 12, 0F); // Box 99
		defaultStockModel[15].setRotationPoint(-40F, -19.5F, -6F);

		defaultStockModel[16].addShapeBox(0F, 0F, 0F, 5, 11, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 1.5F, 0F, 0F, 1.5F); // Box 100
		defaultStockModel[16].setRotationPoint(-40F, -12.5F, -6F);

		defaultStockModel[17].addShapeBox(0F, 0F, 0F, 5, 1, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F); // Box 101
		defaultStockModel[17].setRotationPoint(-40F, -1.5F, -4.5F);

		defaultStockModel[18].addShapeBox(0F, 0F, 0F, 5, 11, 1, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 1.5F, 0F, 0F, 1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 102
		defaultStockModel[18].setRotationPoint(-40F, -12.5F, 3.5F);

		defaultStockModel[19].addShapeBox(0F, 0F, 0F, 5, 2, 11, 0F, -0.5F, 0F, -2F, 0.5F, 0F, -2F, 0.5F, 0F, -2F, -0.5F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 0
		defaultStockModel[19].setRotationPoint(-34.5F, -21.5F, -5.5F);

		defaultStockModel[20].addShapeBox(0F, 0F, 0F, 5, 7, 11, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0.5F, 0F, 0F); // Box 1
		defaultStockModel[20].setRotationPoint(-34.5F, -19.5F, -5.5F);

		defaultStockModel[21].addShapeBox(0F, 0F, 0F, 5, 11, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1.5F, 0F, -1.5F, -1.5F, 0F, -1.5F, -1.5F, 0F, 1.5F, 1.5F, 0F, 1.5F); // Box 2
		defaultStockModel[21].setRotationPoint(-35F, -12.5F, -5.5F);

		defaultStockModel[22].addShapeBox(0F, 0F, 0F, 5, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, -0.5F, 0F, -1F, -0.5F, 0F, -1F, 0F, 0F, -1F); // Box 3
		defaultStockModel[22].setRotationPoint(-36.5F, -1.5F, -4F);

		defaultStockModel[23].addShapeBox(0F, 0F, 0F, 5, 11, 1, 0F, -1.5F, 0F, -1.5F, 1.5F, 0F, -1.5F, 1.5F, 0F, 1.5F, -1.5F, 0F, 1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 4
		defaultStockModel[23].setRotationPoint(-36.5F, -12.5F, 3F);

		defaultStockModel[24].addBox(0F, 0F, 0F, 10, 3, 7, 0F); // Box 115
		defaultStockModel[24].setRotationPoint(-26F, -16.5F, -3.5F);

		defaultStockModel[25].addShapeBox(0F, 0F, 0F, 10, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 116
		defaultStockModel[25].setRotationPoint(-26F, -18.5F, -3.5F);

		defaultStockModel[26].addShapeBox(0F, 0F, 0F, 10, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 117
		defaultStockModel[26].setRotationPoint(-26F, -13.5F, -3.5F);

		defaultStockModel[27].addBox(0F, 0F, 0F, 2, 3, 9, 0F); // Box 122
		defaultStockModel[27].setRotationPoint(-16F, -16.5F, -4.5F);

		defaultStockModel[28].addShapeBox(0F, 0F, 0F, 2, 3, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 123
		defaultStockModel[28].setRotationPoint(-16F, -13.5F, -4.5F);

		defaultStockModel[29].addShapeBox(0F, 0F, 0F, 2, 3, 9, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 124
		defaultStockModel[29].setRotationPoint(-16F, -19.5F, -4.5F);


		ammoModel = new ModelRendererTurbo[8];
		ammoModel[0] = new ModelRendererTurbo(this, 227, 1, textureX, textureY); // clipPart1
		ammoModel[1] = new ModelRendererTurbo(this, 275, 2, textureX, textureY); // clipPart2
		ammoModel[2] = new ModelRendererTurbo(this, 256, 1, textureX, textureY); // clipPart3
		ammoModel[3] = new ModelRendererTurbo(this, 227, 28, textureX, textureY); // clipPart4
		ammoModel[4] = new ModelRendererTurbo(this, 274, 29, textureX, textureY); // clipPart5
		ammoModel[5] = new ModelRendererTurbo(this, 227, 40, textureX, textureY); // bulletPart
		ammoModel[6] = new ModelRendererTurbo(this, 254, 40, textureX, textureY); // Box 31
		ammoModel[7] = new ModelRendererTurbo(this, 227, 46, textureX, textureY); // Box 9

		ammoModel[0].addBox(0F, 0F, 0F, 8, 20, 6, 0F); // clipPart1
		ammoModel[0].setRotationPoint(15F, -4F, -3F);

		ammoModel[1].addBox(0F, 0F, 0F, 3, 17, 5, 0F); // clipPart2
		ammoModel[1].setRotationPoint(23F, -4F, -2.5F);

		ammoModel[2].addBox(0F, 0F, 0F, 3, 20, 6, 0F); // clipPart3
		ammoModel[2].setRotationPoint(26F, -4F, -3F);

		ammoModel[3].addBox(0F, 0F, 0F, 16, 4, 7, 0F); // clipPart4
		ammoModel[3].setRotationPoint(14F, 16F, -3.5F);

		ammoModel[4].addShapeBox(0F, 0F, 0F, 6, 3, 7, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // clipPart5
		ammoModel[4].setRotationPoint(16F, 13F, -3.5F);

		ammoModel[5].addShapeBox(0F, 0F, 0F, 9, 1, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // bulletPart
		ammoModel[5].setRotationPoint(16F, -5F, -2F);

		ammoModel[6].addShapeBox(0F, 0F, 0F, 3, 1, 4, 0F, 0F, 0F, -1F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 31
		ammoModel[6].setRotationPoint(25F, -5F, -2F);

		ammoModel[7].addBox(0F, 0F, 0F, 3, 3, 6, 0F); // Box 9
		ammoModel[7].setRotationPoint(23F, 13F, -3F);


		slideModel = new ModelRendererTurbo[3];
		slideModel[0] = new ModelRendererTurbo(this, 26, 103, textureX, textureY); // Box 27
		slideModel[1] = new ModelRendererTurbo(this, 47, 96, textureX, textureY); // Box 46
		slideModel[2] = new ModelRendererTurbo(this, 47, 96, textureX, textureY); // Box 48

		slideModel[0].addShapeBox(0F, 0F, 0F, 15, 6, 2, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 27
		slideModel[0].setRotationPoint(16F, -17F, -4.5F);

		slideModel[1].addShapeBox(0F, 0F, 0F, 1, 2, 4, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F); // Box 46
		slideModel[1].setRotationPoint(28F, -15F, -8.5F);

		slideModel[2].addShapeBox(0F, 0F, 0F, 1, 2, 4, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F); // Box 48
		slideModel[2].setRotationPoint(29F, -15F, -8.5F);

		barrelAttachPoint = new Vector3f(84F / 16F, 14F / 16F, 0F / 16F);
		stockAttachPoint = new Vector3f(-14F / 16F, 14F / 16F, 0F / 16F);
		scopeAttachPoint = new Vector3f(15F / 16F, 20.5F / 16F, 0F / 16F);
		gripAttachPoint = new Vector3f(45 / 16F, 4F / 16F, 0F / 16F);

		gunSlideDistance = 0.8F;
		animationType = EnumAnimationType.BOTTOM_CLIP;

		/* ----Start of Reload Block---- */
		/* Reload Name: Stock Reload */
		/* ----End of Reload Block---- */

		flipAll();
		translateAll(0F, 0F, 0F);

		///<summary>
		///Owner = "DerpiWolf"
		///</summary>
	}
}