package com.flansmod.client.model.dwfirearms; //Path where the model is located

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelCinderfireRifle extends ModelGun //Same as Filename
{
	int textureX = 512;
	int textureY = 256;

	public ModelCinderfireRifle() //Same as Filename
	{
		gunModel = new ModelRendererTurbo[68];
		gunModel[0] = new ModelRendererTurbo(this, 1, 80, textureX, textureY); // acidCanister
		gunModel[1] = new ModelRendererTurbo(this, 1, 97, textureX, textureY); // acidCanister
		gunModel[2] = new ModelRendererTurbo(this, 38, 120, textureX, textureY); // acidCanister
		gunModel[3] = new ModelRendererTurbo(this, 38, 120, textureX, textureY); // stock3
		gunModel[4] = new ModelRendererTurbo(this, 1, 121, textureX, textureY); // acidCanister
		gunModel[5] = new ModelRendererTurbo(this, 1, 109, textureX, textureY); // acidCanister
		gunModel[6] = new ModelRendererTurbo(this, 1, 138, textureX, textureY); // acidCanister
		gunModel[7] = new ModelRendererTurbo(this, 28, 138, textureX, textureY); // acidCanister
		gunModel[8] = new ModelRendererTurbo(this, 104, 68, textureX, textureY); // Box 0
		gunModel[9] = new ModelRendererTurbo(this, 28, 149, textureX, textureY); // Box 1
		gunModel[10] = new ModelRendererTurbo(this, 203, 57, textureX, textureY); // Box 2
		gunModel[11] = new ModelRendererTurbo(this, 104, 10, textureX, textureY); // Box 3
		gunModel[12] = new ModelRendererTurbo(this, 104, 119, textureX, textureY); // Box 4
		gunModel[13] = new ModelRendererTurbo(this, 104, 51, textureX, textureY); // Box 13
		gunModel[14] = new ModelRendererTurbo(this, 125, 54, textureX, textureY); // Box 14
		gunModel[15] = new ModelRendererTurbo(this, 143, 93, textureX, textureY); // Box 15
		gunModel[16] = new ModelRendererTurbo(this, 164, 93, textureX, textureY); // Box 16
		gunModel[17] = new ModelRendererTurbo(this, 104, 106, textureX, textureY); // Box 17
		gunModel[18] = new ModelRendererTurbo(this, 104, 93, textureX, textureY); // Box 18
		gunModel[19] = new ModelRendererTurbo(this, 104, 1, textureX, textureY); // Box 19
		gunModel[20] = new ModelRendererTurbo(this, 104, 1, textureX, textureY); // Box 20
		gunModel[21] = new ModelRendererTurbo(this, 104, 1, textureX, textureY); // Box 21
		gunModel[22] = new ModelRendererTurbo(this, 210, 28, textureX, textureY); // Box 30
		gunModel[23] = new ModelRendererTurbo(this, 191, 27, textureX, textureY); // Box 31
		gunModel[24] = new ModelRendererTurbo(this, 210, 28, textureX, textureY); // Box 32
		gunModel[25] = new ModelRendererTurbo(this, 191, 18, textureX, textureY); // Box 33
		gunModel[26] = new ModelRendererTurbo(this, 208, 18, textureX, textureY); // Box 34
		gunModel[27] = new ModelRendererTurbo(this, 191, 18, textureX, textureY); // Box 35
		gunModel[28] = new ModelRendererTurbo(this, 43, 9, textureX, textureY); // rail1
		gunModel[29] = new ModelRendererTurbo(this, 43, 1, textureX, textureY); // rail2
		gunModel[30] = new ModelRendererTurbo(this, 43, 15, textureX, textureY); // rail3
		gunModel[31] = new ModelRendererTurbo(this, 43, 15, textureX, textureY); // rail3-2
		gunModel[32] = new ModelRendererTurbo(this, 191, 68, textureX, textureY); // Box 36
		gunModel[33] = new ModelRendererTurbo(this, 92, 1, textureX, textureY); // Box 37
		gunModel[34] = new ModelRendererTurbo(this, 170, 105, textureX, textureY); // Box 38
		gunModel[35] = new ModelRendererTurbo(this, 104, 79, textureX, textureY); // Box 39
		gunModel[36] = new ModelRendererTurbo(this, 141, 121, textureX, textureY); // Box 40
		gunModel[37] = new ModelRendererTurbo(this, 191, 14, textureX, textureY); // Box 41
		gunModel[38] = new ModelRendererTurbo(this, 191, 10, textureX, textureY); // Box 42
		gunModel[39] = new ModelRendererTurbo(this, 22, 29, textureX, textureY); // Box 43
		gunModel[40] = new ModelRendererTurbo(this, 1, 29, textureX, textureY); // Box 45
		gunModel[41] = new ModelRendererTurbo(this, 143, 104, textureX, textureY); // Box 47
		gunModel[42] = new ModelRendererTurbo(this, 185, 96, textureX, textureY); // Box 49
		gunModel[43] = new ModelRendererTurbo(this, 1, 20, textureX, textureY); // Box 51
		gunModel[44] = new ModelRendererTurbo(this, 30, 25, textureX, textureY); // Box 52
		gunModel[45] = new ModelRendererTurbo(this, 30, 25, textureX, textureY); // Box 53
		gunModel[46] = new ModelRendererTurbo(this, 30, 25, textureX, textureY); // Box 54
		gunModel[47] = new ModelRendererTurbo(this, 30, 25, textureX, textureY); // Box 55
		gunModel[48] = new ModelRendererTurbo(this, 41, 22, textureX, textureY); // Box 56
		gunModel[49] = new ModelRendererTurbo(this, 30, 21, textureX, textureY); // Box 57
		gunModel[50] = new ModelRendererTurbo(this, 30, 21, textureX, textureY); // Box 58
		gunModel[51] = new ModelRendererTurbo(this, 24, 44, textureX, textureY); // gearPart1
		gunModel[52] = new ModelRendererTurbo(this, 24, 52, textureX, textureY); // gearPart2
		gunModel[53] = new ModelRendererTurbo(this, 24, 48, textureX, textureY); // gearPart3
		gunModel[54] = new ModelRendererTurbo(this, 1, 44, textureX, textureY); // gearPart4
		gunModel[55] = new ModelRendererTurbo(this, 1, 48, textureX, textureY); // gearPart5
		gunModel[56] = new ModelRendererTurbo(this, 1, 52, textureX, textureY); // gearPart6
		gunModel[57] = new ModelRendererTurbo(this, 1, 48, textureX, textureY); // gearPart7
		gunModel[58] = new ModelRendererTurbo(this, 1, 44, textureX, textureY); // gearPart8
		gunModel[59] = new ModelRendererTurbo(this, 39, 48, textureX, textureY); // gearStub1
		gunModel[60] = new ModelRendererTurbo(this, 39, 48, textureX, textureY); // gearStub2
		gunModel[61] = new ModelRendererTurbo(this, 39, 44, textureX, textureY); // gearStub3
		gunModel[62] = new ModelRendererTurbo(this, 39, 44, textureX, textureY); // gearStub4
		gunModel[63] = new ModelRendererTurbo(this, 39, 48, textureX, textureY); // gearStub5
		gunModel[64] = new ModelRendererTurbo(this, 39, 48, textureX, textureY); // gearStub6
		gunModel[65] = new ModelRendererTurbo(this, 39, 48, textureX, textureY); // gearStub7
		gunModel[66] = new ModelRendererTurbo(this, 39, 48, textureX, textureY); // gearStub8
		gunModel[67] = new ModelRendererTurbo(this, 104, 133, textureX, textureY); // Box 203

		gunModel[0].addShapeBox(0F, 0F, 0F, 23, 8, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 10F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 10F, 0F); // acidCanister
		gunModel[0].setRotationPoint(-37F, -11F, -4F);

		gunModel[1].addShapeBox(0F, 0F, 0F, 28, 3, 8, 0F, 0F, 0F, -1.5F, -3F, 0F, -1.5F, -3F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // acidCanister
		gunModel[1].setRotationPoint(-42F, -14F, -4F);

		gunModel[2].addShapeBox(0F, 0F, 0F, 10, 2, 8, 0F, 0F, 0F, 0F, 0F, 5F, 0F, 0F, 5F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -5F, -1F, 0F, -5F, -1F, 0F, 0F, -1F); // acidCanister
		gunModel[2].setRotationPoint(-14F, -3F, -4F);

		gunModel[3].addShapeBox(0F, 0F, 0F, 10, 2, 8, 0F, 0F, -5F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, -5F, -1.5F, 0F, 5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 5F, 0F); // stock3
		gunModel[3].setRotationPoint(-14F, -18F, -4F);

		gunModel[4].addShapeBox(0F, 0F, 0F, 10, 8, 8, 0F, 0F, 0F, 0F, 0F, 5F, 0F, 0F, 5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -5F, 0F, 0F, -5F, 0F, 0F, 0F, 0F); // acidCanister
		gunModel[4].setRotationPoint(-14F, -11F, -4F);

		gunModel[5].addShapeBox(0F, 0F, 0F, 23, 2, 8, 0F, 0F, -10F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -10F, 0F, 0F, 10F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 10F, -1F); // acidCanister
		gunModel[5].setRotationPoint(-37F, -3F, -4F);

		gunModel[6].addBox(0F, 0F, 0F, 5, 18, 8, 0F); // acidCanister
		gunModel[6].setRotationPoint(-42F, -11F, -4F);

		gunModel[7].addShapeBox(0F, 0F, 0F, 5, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // acidCanister
		gunModel[7].setRotationPoint(-42F, 7F, -4F);

		gunModel[8].addShapeBox(0F, 0F, 0F, 35, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 0
		gunModel[8].setRotationPoint(-4F, -8F, -4F);

		gunModel[9].addShapeBox(0F, 0F, 0F, 5, 2, 8, 0F, 0F, -5F, -1.5F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, -5F, -1.5F, 0F, 5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 5F, 0F); // Box 1
		gunModel[9].setRotationPoint(-4F, -23F, -4F);

		gunModel[10].addShapeBox(0F, 0F, 0F, 7, 2, 8, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 2
		gunModel[10].setRotationPoint(1F, -23F, -4F);

		gunModel[11].addBox(0F, 0F, 0F, 35, 8, 8, 0F); // Box 3
		gunModel[11].setRotationPoint(-4F, -16F, -4F);

		gunModel[12].addShapeBox(0F, 0F, 0F, 10, 5, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 5F, 0F, 0F); // Box 4
		gunModel[12].setRotationPoint(1F, -21F, -4F);

		gunModel[13].addShapeBox(0F, 0F, 0F, 2, 8, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F); // Box 13
		gunModel[13].setRotationPoint(31F, -16F, -4F);

		gunModel[14].addShapeBox(0F, 0F, 0F, 2, 5, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F); // Box 14
		gunModel[14].setRotationPoint(33F, -16F, -4F);

		gunModel[15].addShapeBox(0F, 0F, 0F, 2, 2, 8, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -3F, -1F, 0F, -3F, -1F, 0F, 0F, -1F); // Box 15
		gunModel[15].setRotationPoint(31F, -8F, -4F);

		gunModel[16].addShapeBox(0F, 0F, 0F, 2, 2, 8, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, 0F, -1F); // Box 16
		gunModel[16].setRotationPoint(33F, -11F, -4F);

		gunModel[17].addBox(0F, 0F, 0F, 11, 4, 8, 0F); // Box 17
		gunModel[17].setRotationPoint(35F, -16F, -4F);

		gunModel[18].addShapeBox(0F, 0F, 0F, 11, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 18
		gunModel[18].setRotationPoint(35F, -12F, -4F);

		gunModel[19].addShapeBox(0F, 0F, 0F, 55, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 19
		gunModel[19].setRotationPoint(46F, -22F, -3F);

		gunModel[20].addShapeBox(0F, 0F, 0F, 55, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 20
		gunModel[20].setRotationPoint(46F, -18F, -3F);

		gunModel[21].addBox(0F, 0F, 0F, 55, 2, 6, 0F); // Box 21
		gunModel[21].setRotationPoint(46F, -20F, -3F);

		gunModel[22].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 30
		gunModel[22].setRotationPoint(101F, -17.5F, -3.5F);

		gunModel[23].addBox(0F, 0F, 0F, 2, 3, 7, 0F); // Box 31
		gunModel[23].setRotationPoint(101F, -20.5F, -3.5F);

		gunModel[24].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 32
		gunModel[24].setRotationPoint(101F, -22.5F, -3.5F);

		gunModel[25].addShapeBox(0F, 0F, 0F, 2, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 33
		gunModel[25].setRotationPoint(103F, -18F, -3F);

		gunModel[26].addBox(0F, 0F, 0F, 2, 2, 6, 0F); // Box 34
		gunModel[26].setRotationPoint(103F, -20F, -3F);

		gunModel[27].addShapeBox(0F, 0F, 0F, 2, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 35
		gunModel[27].setRotationPoint(103F, -22F, -3F);

		gunModel[28].addBox(0F, 0F, 0F, 18, 1, 4, 0F); // rail1
		gunModel[28].setRotationPoint(27F, -24F, -2F);

		gunModel[29].addBox(0F, 0F, 0F, 18, 1, 6, 0F); // rail2
		gunModel[29].setRotationPoint(27F, -25F, -3F);

		gunModel[30].addShapeBox(0F, 0F, 0F, 18, 1, 1, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // rail3
		gunModel[30].setRotationPoint(27F, -26F, 2F);

		gunModel[31].addShapeBox(0F, 0F, 0F, 18, 1, 1, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // rail3-2
		gunModel[31].setRotationPoint(27F, -26F, -3F);

		gunModel[32].addShapeBox(0F, 0F, 0F, 20, 2, 8, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 36
		gunModel[32].setRotationPoint(26F, -23F, -4F);

		gunModel[33].addShapeBox(0F, 0F, 0F, 2, 4, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 37
		gunModel[33].setRotationPoint(101F, -26.5F, -1.5F);

		gunModel[34].addShapeBox(0F, 0F, 0F, 3, 3, 10, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 38
		gunModel[34].setRotationPoint(8F, -24F, -5F);

		gunModel[35].addBox(0F, 0F, 0F, 20, 5, 8, 0F); // Box 39
		gunModel[35].setRotationPoint(26F, -21F, -4F);

		gunModel[36].addBox(0F, 0F, 0F, 15, 3, 8, 0F); // Box 40
		gunModel[36].setRotationPoint(11F, -19F, -4F);

		gunModel[37].addBox(0F, 0F, 0F, 15, 2, 1, 0F); // Box 41
		gunModel[37].setRotationPoint(11F, -21F, -4F);

		gunModel[38].addBox(0F, 0F, 0F, 15, 2, 1, 0F); // Box 42
		gunModel[38].setRotationPoint(11F, -21F, 3F);

		gunModel[39].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 43
		gunModel[39].setRotationPoint(11F, -22.5F, -3.5F);

		gunModel[40].addBox(0F, 0F, 0F, 3, 2, 7, 0F); // Box 45
		gunModel[40].setRotationPoint(11F, -20.5F, -3.5F);

		gunModel[41].addBox(0F, 0F, 0F, 3, 4, 10, 0F); // Box 47
		gunModel[41].setRotationPoint(8F, -21F, -5F);

		gunModel[42].addBox(0F, 0F, 0F, 5, 1, 6, 0F); // Box 49
		gunModel[42].setRotationPoint(3F, -24F, -3F);

		gunModel[43].addShapeBox(0F, 0F, 0F, 8, 2, 6, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 51
		gunModel[43].setRotationPoint(3F, -26F, -3F);

		gunModel[44].addShapeBox(0F, 0F, 0F, 4, 2, 1, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 52
		gunModel[44].setRotationPoint(3F, -27.5F, -3.5F);

		gunModel[45].addShapeBox(0F, 0F, 0F, 4, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 53
		gunModel[45].setRotationPoint(3F, -25.5F, -3.5F);

		gunModel[46].addShapeBox(0F, 0F, 0F, 4, 2, 1, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 54
		gunModel[46].setRotationPoint(3F, -27.5F, 2.5F);

		gunModel[47].addShapeBox(0F, 0F, 0F, 4, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 55
		gunModel[47].setRotationPoint(3F, -25.5F, 2.5F);

		gunModel[48].addBox(0F, 0F, 0F, 3, 1, 5, 0F); // Box 56
		gunModel[48].setRotationPoint(4.5F, -27F, -2.5F);

		gunModel[49].addBox(0F, 0F, 0F, 3, 2, 1, 0F); // Box 57
		gunModel[49].setRotationPoint(4.5F, -29F, -2.5F);

		gunModel[50].addBox(0F, 0F, 0F, 3, 2, 1, 0F); // Box 58
		gunModel[50].setRotationPoint(4.5F, -29F, 1.5F);

		gunModel[51].addShapeBox(0F, 0F, 0F, 6, 2, 1, 0F, -1.75F, 0F, 0F, -1.75F, 0F, 0F, -1.75F, 0F, 0F, -1.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // gearPart1
		gunModel[51].setRotationPoint(0F, -17F, -5.5F);

		gunModel[52].addShapeBox(0F, 0F, 0F, 6, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // gearPart2
		gunModel[52].setRotationPoint(0F, -15F, -5.5F);

		gunModel[53].addShapeBox(0F, 0F, 0F, 6, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.75F, 0F, 0F, -1.75F, 0F, 0F, -1.75F, 0F, 0F, -1.75F, 0F, 0F); // gearPart3
		gunModel[53].setRotationPoint(0F, -13F, -5.5F);

		gunModel[54].addShapeBox(0F, 0F, 0F, 6, 2, 1, 0F, -2F, -0.75F, 0F, -2F, -0.75F, 0F, -2F, -0.75F, 0F, -2F, -0.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // gearPart4
		gunModel[54].setRotationPoint(0F, -19F, -5F);

		gunModel[55].addShapeBox(0F, 0F, 0F, 10, 2, 1, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // gearPart5
		gunModel[55].setRotationPoint(-2F, -17F, -5F);

		gunModel[56].addShapeBox(0F, 0F, 0F, 8, 2, 1, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F); // gearPart6
		gunModel[56].setRotationPoint(-1F, -15F, -5F);

		gunModel[57].addShapeBox(0F, 0F, 0F, 10, 2, 1, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F); // gearPart7
		gunModel[57].setRotationPoint(-2F, -13F, -5F);

		gunModel[58].addShapeBox(0F, 0F, 0F, 6, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, -0.75F, 0F, -2F, -0.75F, 0F, -2F, -0.75F, 0F, -2F, -0.75F, 0F); // gearPart8
		gunModel[58].setRotationPoint(0F, -11F, -5F);

		gunModel[59].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // gearStub1
		gunModel[59].setRotationPoint(2F, -19.2F, -5F);

		gunModel[60].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F); // gearStub2
		gunModel[60].setRotationPoint(2F, -9.8F, -5F);

		gunModel[61].addShapeBox(0F, 0F, 0F, 1, 2, 1, 0F, 0.25F, -0.3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.25F, -0.3F, 0F, 0.25F, -0.3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.25F, -0.3F, 0F); // gearStub3
		gunModel[61].setRotationPoint(-2.5F, -15.2F, -5F);

		gunModel[62].addShapeBox(0F, 0F, 0F, 1, 2, 1, 0F, 0F, 0F, 0F, 0.25F, -0.3F, 0F, 0.25F, -0.3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.25F, -0.3F, 0F, 0.25F, -0.3F, 0F, 0F, 0F, 0F); // gearStub4
		gunModel[62].setRotationPoint(7.5F, -15.2F, -5F);

		gunModel[63].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // gearStub5
		gunModel[63].setRotationPoint(-1F, -17.2F, -5F);
		gunModel[63].rotateAngleZ = 0.6981317F;

		gunModel[64].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F); // gearStub6
		gunModel[64].setRotationPoint(5.2F, -10.5F, -5F);
		gunModel[64].rotateAngleZ = 0.78539816F;

		gunModel[65].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // gearStub7
		gunModel[65].setRotationPoint(6F, -18.3F, -5F);
		gunModel[65].rotateAngleZ = -0.78539816F;

		gunModel[66].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F); // gearStub8
		gunModel[66].setRotationPoint(-0.699999999999999F, -12F, -5F);
		gunModel[66].rotateAngleZ = -0.78539816F;

		gunModel[67].addBox(0F, 0F, 0F, 17, 3, 8, 0F); // Box 203
		gunModel[67].setRotationPoint(10F, -8F, -4F);


		defaultGripModel = new ModelRendererTurbo[6];
		defaultGripModel[0] = new ModelRendererTurbo(this, 104, 39, textureX, textureY); // Box 22
		defaultGripModel[1] = new ModelRendererTurbo(this, 104, 27, textureX, textureY); // Box 23
		defaultGripModel[2] = new ModelRendererTurbo(this, 146, 56, textureX, textureY); // Box 25
		defaultGripModel[3] = new ModelRendererTurbo(this, 161, 85, textureX, textureY); // Box 26
		defaultGripModel[4] = new ModelRendererTurbo(this, 191, 38, textureX, textureY); // Box 27
		defaultGripModel[5] = new ModelRendererTurbo(this, 191, 38, textureX, textureY); // Box 28

		defaultGripModel[0].addShapeBox(0F, 0F, 0F, 30, 4, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 22
		defaultGripModel[0].setRotationPoint(46F, -14F, -3.5F);

		defaultGripModel[1].addBox(0F, 0F, 0F, 35, 4, 7, 0F); // Box 23
		defaultGripModel[1].setRotationPoint(46F, -18F, -3.5F);

		defaultGripModel[2].addShapeBox(0F, 0F, 0F, 5, 4, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, -2F, -2F, 0F, -2F, -2F, 0F, 0F, -2F); // Box 25
		defaultGripModel[2].setRotationPoint(76F, -14F, -3.5F);

		defaultGripModel[3].addBox(0F, 0F, 0F, 20, 4, 3, 0F); // Box 26
		defaultGripModel[3].setRotationPoint(81F, -16F, -1.5F);

		defaultGripModel[4].addShapeBox(0F, 0F, 0F, 2, 1, 4, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 27
		defaultGripModel[4].setRotationPoint(98F, -15F, -2F);

		defaultGripModel[5].addShapeBox(0F, 0F, 0F, 2, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 28
		defaultGripModel[5].setRotationPoint(98F, -14F, -2F);


		ammoModel = new ModelRendererTurbo[55];
		ammoModel[0] = new ModelRendererTurbo(this, 1, 56, textureX, textureY); // Box 29
		ammoModel[1] = new ModelRendererTurbo(this, 1, 39, textureX, textureY); // Box 0
		ammoModel[2] = new ModelRendererTurbo(this, 1, 39, textureX, textureY); // Box 78
		ammoModel[3] = new ModelRendererTurbo(this, 1, 39, textureX, textureY); // Box 79
		ammoModel[4] = new ModelRendererTurbo(this, 28, 39, textureX, textureY); // Box 80
		ammoModel[5] = new ModelRendererTurbo(this, 28, 39, textureX, textureY); // Box 81
		ammoModel[6] = new ModelRendererTurbo(this, 28, 39, textureX, textureY); // Box 82
		ammoModel[7] = new ModelRendererTurbo(this, 18, 62, textureX, textureY); // Box 83
		ammoModel[8] = new ModelRendererTurbo(this, 29, 62, textureX, textureY); // Box 84
		ammoModel[9] = new ModelRendererTurbo(this, 28, 39, textureX, textureY); // Box 85
		ammoModel[10] = new ModelRendererTurbo(this, 1, 39, textureX, textureY); // Box 86
		ammoModel[11] = new ModelRendererTurbo(this, 28, 39, textureX, textureY); // Box 87
		ammoModel[12] = new ModelRendererTurbo(this, 1, 39, textureX, textureY); // Box 88
		ammoModel[13] = new ModelRendererTurbo(this, 1, 39, textureX, textureY); // Box 89
		ammoModel[14] = new ModelRendererTurbo(this, 28, 39, textureX, textureY); // Box 90
		ammoModel[15] = new ModelRendererTurbo(this, 18, 57, textureX, textureY); // Box 163
		ammoModel[16] = new ModelRendererTurbo(this, 18, 57, textureX, textureY); // Box 164
		ammoModel[17] = new ModelRendererTurbo(this, 28, 39, textureX, textureY); // Box 167
		ammoModel[18] = new ModelRendererTurbo(this, 1, 39, textureX, textureY); // Box 168
		ammoModel[19] = new ModelRendererTurbo(this, 1, 39, textureX, textureY); // Box 169
		ammoModel[20] = new ModelRendererTurbo(this, 28, 39, textureX, textureY); // Box 170
		ammoModel[21] = new ModelRendererTurbo(this, 1, 39, textureX, textureY); // Box 171
		ammoModel[22] = new ModelRendererTurbo(this, 28, 39, textureX, textureY); // Box 172
		ammoModel[23] = new ModelRendererTurbo(this, 28, 39, textureX, textureY); // Box 173
		ammoModel[24] = new ModelRendererTurbo(this, 28, 39, textureX, textureY); // Box 174
		ammoModel[25] = new ModelRendererTurbo(this, 28, 39, textureX, textureY); // Box 175
		ammoModel[26] = new ModelRendererTurbo(this, 1, 39, textureX, textureY); // Box 176
		ammoModel[27] = new ModelRendererTurbo(this, 1, 39, textureX, textureY); // Box 177
		ammoModel[28] = new ModelRendererTurbo(this, 1, 39, textureX, textureY); // Box 178
		ammoModel[29] = new ModelRendererTurbo(this, 28, 39, textureX, textureY); // Box 179
		ammoModel[30] = new ModelRendererTurbo(this, 1, 39, textureX, textureY); // Box 180
		ammoModel[31] = new ModelRendererTurbo(this, 1, 39, textureX, textureY); // Box 181
		ammoModel[32] = new ModelRendererTurbo(this, 28, 39, textureX, textureY); // Box 182
		ammoModel[33] = new ModelRendererTurbo(this, 1, 39, textureX, textureY); // Box 183
		ammoModel[34] = new ModelRendererTurbo(this, 28, 39, textureX, textureY); // Box 184
		ammoModel[35] = new ModelRendererTurbo(this, 28, 39, textureX, textureY); // Box 185
		ammoModel[36] = new ModelRendererTurbo(this, 28, 39, textureX, textureY); // Box 186
		ammoModel[37] = new ModelRendererTurbo(this, 28, 39, textureX, textureY); // Box 187
		ammoModel[38] = new ModelRendererTurbo(this, 1, 39, textureX, textureY); // Box 188
		ammoModel[39] = new ModelRendererTurbo(this, 1, 39, textureX, textureY); // Box 189
		ammoModel[40] = new ModelRendererTurbo(this, 1, 39, textureX, textureY); // Box 190
		ammoModel[41] = new ModelRendererTurbo(this, 28, 39, textureX, textureY); // Box 191
		ammoModel[42] = new ModelRendererTurbo(this, 1, 39, textureX, textureY); // Box 192
		ammoModel[43] = new ModelRendererTurbo(this, 1, 39, textureX, textureY); // Box 193
		ammoModel[44] = new ModelRendererTurbo(this, 28, 39, textureX, textureY); // Box 194
		ammoModel[45] = new ModelRendererTurbo(this, 1, 39, textureX, textureY); // Box 195
		ammoModel[46] = new ModelRendererTurbo(this, 28, 39, textureX, textureY); // Box 196
		ammoModel[47] = new ModelRendererTurbo(this, 28, 39, textureX, textureY); // Box 197
		ammoModel[48] = new ModelRendererTurbo(this, 28, 39, textureX, textureY); // Box 198
		ammoModel[49] = new ModelRendererTurbo(this, 28, 39, textureX, textureY); // Box 199
		ammoModel[50] = new ModelRendererTurbo(this, 1, 39, textureX, textureY); // Box 200
		ammoModel[51] = new ModelRendererTurbo(this, 1, 39, textureX, textureY); // Box 201
		ammoModel[52] = new ModelRendererTurbo(this, 1, 39, textureX, textureY); // Box 202
		ammoModel[53] = new ModelRendererTurbo(this, 18, 57, textureX, textureY); // Box 204
		ammoModel[54] = new ModelRendererTurbo(this, 18, 57, textureX, textureY); // Box 205

		ammoModel[0].addBox(0F, 0F, 0F, 1, 12, 7, 0F); // Box 29
		ammoModel[0].setRotationPoint(11F, -17.75F, -3.5F);

		ammoModel[1].addShapeBox(0F, 0F, 0F, 10, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 0
		ammoModel[1].setRotationPoint(12F, -18F, -3F);

		ammoModel[2].addShapeBox(0F, 0F, 0F, 10, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 78
		ammoModel[2].setRotationPoint(12F, -16F, -3F);

		ammoModel[3].addBox(0F, 0F, 0F, 10, 1, 3, 0F); // Box 79
		ammoModel[3].setRotationPoint(12F, -17F, -3F);

		ammoModel[4].addShapeBox(0F, 0F, 0F, 4, 1, 3, 0F, 0F, 0F, -1F, 0F, -0.75F, -1.25F, 0F, -0.75F, -1.25F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0.25F, -0.75F, 0F, 0.25F, -0.75F, 0F, 0F, 0F); // Box 80
		ammoModel[4].setRotationPoint(22F, -18F, -3F);

		ammoModel[5].addShapeBox(0F, 0F, 0F, 4, 1, 3, 0F, 0F, 0F, 0F, 0F, 0.25F, -0.75F, 0F, 0.25F, -0.75F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -0.75F, -1.25F, 0F, -0.75F, -1.25F, 0F, 0F, -1F); // Box 81
		ammoModel[5].setRotationPoint(22F, -16F, -3F);

		ammoModel[6].addShapeBox(0F, 0F, 0F, 4, 1, 3, 0F, 0F, 0F, 0F, 0F, -0.25F, -0.75F, 0F, -0.25F, -0.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.25F, -0.75F, 0F, -0.25F, -0.75F, 0F, 0F, 0F); // Box 82
		ammoModel[6].setRotationPoint(22F, -17F, -3F);

		ammoModel[7].addBox(0F, 0F, 0F, 4, 12, 1, 0F); // Box 83
		ammoModel[7].setRotationPoint(12F, -17.75F, 2.5F);

		ammoModel[8].addBox(0F, 0F, 0F, 4, 12, 1, 0F); // Box 84
		ammoModel[8].setRotationPoint(12F, -17.75F, -3.5F);

		ammoModel[9].addShapeBox(0F, 0F, 0F, 4, 1, 3, 0F, 0F, 0F, -1F, 0F, -0.75F, -1.25F, 0F, -0.75F, -1.25F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0.25F, -0.75F, 0F, 0.25F, -0.75F, 0F, 0F, 0F); // Box 85
		ammoModel[9].setRotationPoint(22F, -17.5F, 0F);

		ammoModel[10].addShapeBox(0F, 0F, 0F, 10, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 86
		ammoModel[10].setRotationPoint(12F, -17.5F, 0F);

		ammoModel[11].addShapeBox(0F, 0F, 0F, 4, 1, 3, 0F, 0F, 0F, 0F, 0F, -0.25F, -0.75F, 0F, -0.25F, -0.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.25F, -0.75F, 0F, -0.25F, -0.75F, 0F, 0F, 0F); // Box 87
		ammoModel[11].setRotationPoint(22F, -16.5F, 0F);

		ammoModel[12].addBox(0F, 0F, 0F, 10, 1, 3, 0F); // Box 88
		ammoModel[12].setRotationPoint(12F, -16.5F, 0F);

		ammoModel[13].addShapeBox(0F, 0F, 0F, 10, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 89
		ammoModel[13].setRotationPoint(12F, -15.5F, 0F);

		ammoModel[14].addShapeBox(0F, 0F, 0F, 4, 1, 3, 0F, 0F, 0F, 0F, 0F, 0.25F, -0.75F, 0F, 0.25F, -0.75F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -0.75F, -1.25F, 0F, -0.75F, -1.25F, 0F, 0F, -1F); // Box 90
		ammoModel[14].setRotationPoint(22F, -15.5F, 0F);

		ammoModel[15].addShapeBox(0F, 0F, 0F, 5, 1, 3, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 163
		ammoModel[15].setRotationPoint(11F, -18.75F, 0.5F);

		ammoModel[16].addShapeBox(0F, 0F, 0F, 5, 1, 3, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 164
		ammoModel[16].setRotationPoint(11F, -18.75F, -3.5F);

		ammoModel[17].addShapeBox(0F, 0F, 0F, 4, 1, 3, 0F, 0F, 0F, -1F, 0F, -0.75F, -1.25F, 0F, -0.75F, -1.25F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0.25F, -0.75F, 0F, 0.25F, -0.75F, 0F, 0F, 0F); // Box 167
		ammoModel[17].setRotationPoint(22F, -14.5F, 0F);

		ammoModel[18].addShapeBox(0F, 0F, 0F, 10, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 168
		ammoModel[18].setRotationPoint(12F, -14.5F, 0F);

		ammoModel[19].addBox(0F, 0F, 0F, 10, 1, 3, 0F); // Box 169
		ammoModel[19].setRotationPoint(12F, -13.5F, 0F);

		ammoModel[20].addShapeBox(0F, 0F, 0F, 4, 1, 3, 0F, 0F, 0F, 0F, 0F, -0.25F, -0.75F, 0F, -0.25F, -0.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.25F, -0.75F, 0F, -0.25F, -0.75F, 0F, 0F, 0F); // Box 170
		ammoModel[20].setRotationPoint(22F, -13.5F, 0F);

		ammoModel[21].addShapeBox(0F, 0F, 0F, 10, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 171
		ammoModel[21].setRotationPoint(12F, -12.5F, 0F);

		ammoModel[22].addShapeBox(0F, 0F, 0F, 4, 1, 3, 0F, 0F, 0F, 0F, 0F, 0.25F, -0.75F, 0F, 0.25F, -0.75F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -0.75F, -1.25F, 0F, -0.75F, -1.25F, 0F, 0F, -1F); // Box 172
		ammoModel[22].setRotationPoint(22F, -12.5F, 0F);

		ammoModel[23].addShapeBox(0F, 0F, 0F, 4, 1, 3, 0F, 0F, 0F, -1F, 0F, -0.75F, -1.25F, 0F, -0.75F, -1.25F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0.25F, -0.75F, 0F, 0.25F, -0.75F, 0F, 0F, 0F); // Box 173
		ammoModel[23].setRotationPoint(22F, -15F, -3F);

		ammoModel[24].addShapeBox(0F, 0F, 0F, 4, 1, 3, 0F, 0F, 0F, 0F, 0F, -0.25F, -0.75F, 0F, -0.25F, -0.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.25F, -0.75F, 0F, -0.25F, -0.75F, 0F, 0F, 0F); // Box 174
		ammoModel[24].setRotationPoint(22F, -14F, -3F);

		ammoModel[25].addShapeBox(0F, 0F, 0F, 4, 1, 3, 0F, 0F, 0F, 0F, 0F, 0.25F, -0.75F, 0F, 0.25F, -0.75F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -0.75F, -1.25F, 0F, -0.75F, -1.25F, 0F, 0F, -1F); // Box 175
		ammoModel[25].setRotationPoint(22F, -13F, -3F);

		ammoModel[26].addShapeBox(0F, 0F, 0F, 10, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 176
		ammoModel[26].setRotationPoint(12F, -13F, -3F);

		ammoModel[27].addBox(0F, 0F, 0F, 10, 1, 3, 0F); // Box 177
		ammoModel[27].setRotationPoint(12F, -14F, -3F);

		ammoModel[28].addShapeBox(0F, 0F, 0F, 10, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 178
		ammoModel[28].setRotationPoint(12F, -15F, -3F);

		ammoModel[29].addShapeBox(0F, 0F, 0F, 4, 1, 3, 0F, 0F, 0F, -1F, 0F, -0.75F, -1.25F, 0F, -0.75F, -1.25F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0.25F, -0.75F, 0F, 0.25F, -0.75F, 0F, 0F, 0F); // Box 179
		ammoModel[29].setRotationPoint(22F, -11.5F, 0F);

		ammoModel[30].addShapeBox(0F, 0F, 0F, 10, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 180
		ammoModel[30].setRotationPoint(12F, -11.5F, 0F);

		ammoModel[31].addBox(0F, 0F, 0F, 10, 1, 3, 0F); // Box 181
		ammoModel[31].setRotationPoint(12F, -10.5F, 0F);

		ammoModel[32].addShapeBox(0F, 0F, 0F, 4, 1, 3, 0F, 0F, 0F, 0F, 0F, -0.25F, -0.75F, 0F, -0.25F, -0.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.25F, -0.75F, 0F, -0.25F, -0.75F, 0F, 0F, 0F); // Box 182
		ammoModel[32].setRotationPoint(22F, -10.5F, 0F);

		ammoModel[33].addShapeBox(0F, 0F, 0F, 10, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 183
		ammoModel[33].setRotationPoint(12F, -9.5F, 0F);

		ammoModel[34].addShapeBox(0F, 0F, 0F, 4, 1, 3, 0F, 0F, 0F, 0F, 0F, 0.25F, -0.75F, 0F, 0.25F, -0.75F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -0.75F, -1.25F, 0F, -0.75F, -1.25F, 0F, 0F, -1F); // Box 184
		ammoModel[34].setRotationPoint(22F, -9.5F, 0F);

		ammoModel[35].addShapeBox(0F, 0F, 0F, 4, 1, 3, 0F, 0F, 0F, -1F, 0F, -0.75F, -1.25F, 0F, -0.75F, -1.25F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0.25F, -0.75F, 0F, 0.25F, -0.75F, 0F, 0F, 0F); // Box 185
		ammoModel[35].setRotationPoint(22F, -12F, -3F);

		ammoModel[36].addShapeBox(0F, 0F, 0F, 4, 1, 3, 0F, 0F, 0F, 0F, 0F, -0.25F, -0.75F, 0F, -0.25F, -0.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.25F, -0.75F, 0F, -0.25F, -0.75F, 0F, 0F, 0F); // Box 186
		ammoModel[36].setRotationPoint(22F, -11F, -3F);

		ammoModel[37].addShapeBox(0F, 0F, 0F, 4, 1, 3, 0F, 0F, 0F, 0F, 0F, 0.25F, -0.75F, 0F, 0.25F, -0.75F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -0.75F, -1.25F, 0F, -0.75F, -1.25F, 0F, 0F, -1F); // Box 187
		ammoModel[37].setRotationPoint(22F, -10F, -3F);

		ammoModel[38].addShapeBox(0F, 0F, 0F, 10, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 188
		ammoModel[38].setRotationPoint(12F, -10F, -3F);

		ammoModel[39].addBox(0F, 0F, 0F, 10, 1, 3, 0F); // Box 189
		ammoModel[39].setRotationPoint(12F, -11F, -3F);

		ammoModel[40].addShapeBox(0F, 0F, 0F, 10, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 190
		ammoModel[40].setRotationPoint(12F, -12F, -3F);

		ammoModel[41].addShapeBox(0F, 0F, 0F, 4, 1, 3, 0F, 0F, 0F, -1F, 0F, -0.75F, -1.25F, 0F, -0.75F, -1.25F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0.25F, -0.75F, 0F, 0.25F, -0.75F, 0F, 0F, 0F); // Box 191
		ammoModel[41].setRotationPoint(22F, -8.5F, 0F);

		ammoModel[42].addShapeBox(0F, 0F, 0F, 10, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 192
		ammoModel[42].setRotationPoint(12F, -8.5F, 0F);

		ammoModel[43].addBox(0F, 0F, 0F, 10, 1, 3, 0F); // Box 193
		ammoModel[43].setRotationPoint(12F, -7.5F, 0F);

		ammoModel[44].addShapeBox(0F, 0F, 0F, 4, 1, 3, 0F, 0F, 0F, 0F, 0F, -0.25F, -0.75F, 0F, -0.25F, -0.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.25F, -0.75F, 0F, -0.25F, -0.75F, 0F, 0F, 0F); // Box 194
		ammoModel[44].setRotationPoint(22F, -7.5F, 0F);

		ammoModel[45].addShapeBox(0F, 0F, 0F, 10, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 195
		ammoModel[45].setRotationPoint(12F, -6.5F, 0F);

		ammoModel[46].addShapeBox(0F, 0F, 0F, 4, 1, 3, 0F, 0F, 0F, 0F, 0F, 0.25F, -0.75F, 0F, 0.25F, -0.75F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -0.75F, -1.25F, 0F, -0.75F, -1.25F, 0F, 0F, -1F); // Box 196
		ammoModel[46].setRotationPoint(22F, -6.5F, 0F);

		ammoModel[47].addShapeBox(0F, 0F, 0F, 4, 1, 3, 0F, 0F, 0F, -1F, 0F, -0.75F, -1.25F, 0F, -0.75F, -1.25F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0.25F, -0.75F, 0F, 0.25F, -0.75F, 0F, 0F, 0F); // Box 197
		ammoModel[47].setRotationPoint(22F, -9F, -3F);

		ammoModel[48].addShapeBox(0F, 0F, 0F, 4, 1, 3, 0F, 0F, 0F, 0F, 0F, -0.25F, -0.75F, 0F, -0.25F, -0.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.25F, -0.75F, 0F, -0.25F, -0.75F, 0F, 0F, 0F); // Box 198
		ammoModel[48].setRotationPoint(22F, -8F, -3F);

		ammoModel[49].addShapeBox(0F, 0F, 0F, 4, 1, 3, 0F, 0F, 0F, 0F, 0F, 0.25F, -0.75F, 0F, 0.25F, -0.75F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -0.75F, -1.25F, 0F, -0.75F, -1.25F, 0F, 0F, -1F); // Box 199
		ammoModel[49].setRotationPoint(22F, -7F, -3F);

		ammoModel[50].addShapeBox(0F, 0F, 0F, 10, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 200
		ammoModel[50].setRotationPoint(12F, -7F, -3F);

		ammoModel[51].addBox(0F, 0F, 0F, 10, 1, 3, 0F); // Box 201
		ammoModel[51].setRotationPoint(12F, -8F, -3F);

		ammoModel[52].addShapeBox(0F, 0F, 0F, 10, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 202
		ammoModel[52].setRotationPoint(12F, -9F, -3F);

		ammoModel[53].addShapeBox(0F, 0F, 0F, 5, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F); // Box 204
		ammoModel[53].setRotationPoint(11F, -5.75F, 0.5F);

		ammoModel[54].addShapeBox(0F, 0F, 0F, 5, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 205
		ammoModel[54].setRotationPoint(11F, -5.75F, -3.5F);


		slideModel = new ModelRendererTurbo[3];
		slideModel[0] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 44
		slideModel[1] = new ModelRendererTurbo(this, 1, 11, textureX, textureY); // Box 46
		slideModel[2] = new ModelRendererTurbo(this, 33, 2, textureX, textureY); // Box 50

		slideModel[0].addShapeBox(0F, 0F, 0F, 12, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 44
		slideModel[0].setRotationPoint(14F, -22F, -3.5F);

		slideModel[1].addBox(0F, 0F, 0F, 12, 1, 7, 0F); // Box 46
		slideModel[1].setRotationPoint(14F, -20F, -3.5F);

		slideModel[2].addBox(0F, 0F, 0F, 1, 3, 2, 0F); // Box 50
		slideModel[2].setRotationPoint(25F, -20.5F, -6F);

		scopeAttachPoint = new Vector3f(37F /16F, 23F /16F, 0F /16F);

		gunSlideDistance = 0.8F;
		animationType = EnumAnimationType.BOTTOM_CLIP;

		tiltGunTime = 0.333F;
		unloadClipTime = 0.0F;
		loadClipTime = 0.333F;
		untiltGunTime = 0.333F;

		translateAll(0F, 0F, 0F);


		flipAll();
	}
}