package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.ModelAttachment;
import com.flansmod.client.tmt.ModelRendererTurbo;

import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelLPMLongBarrel extends ModelAttachment 
{
	int textureX = 512;
	int textureY = 256;

	public ModelLPMLongBarrel()
	{	
		attachmentModel = new ModelRendererTurbo[9];
		attachmentModel[0] = new ModelRendererTurbo(this, 1, 101, textureX, textureY); // Box 0
		attachmentModel[1] = new ModelRendererTurbo(this, 1, 92, textureX, textureY); // Box 1
		attachmentModel[2] = new ModelRendererTurbo(this, 1, 83, textureX, textureY); // Box 2
		attachmentModel[3] = new ModelRendererTurbo(this, 1, 121, textureX, textureY); // Box 3
		attachmentModel[4] = new ModelRendererTurbo(this, 1, 110, textureX, textureY); // Box 4
		attachmentModel[5] = new ModelRendererTurbo(this, 1, 131, textureX, textureY); // Box 5
		attachmentModel[6] = new ModelRendererTurbo(this, 32, 110, textureX, textureY); // Box 6
		attachmentModel[7] = new ModelRendererTurbo(this, 32, 121, textureX, textureY); // Box 7
		attachmentModel[8] = new ModelRendererTurbo(this, 32, 131, textureX, textureY); // Box 8

		attachmentModel[0].addShapeBox(1F, -3F, -3F, 30, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 0
		attachmentModel[0].setRotationPoint(0F, 0F, 0F);

		attachmentModel[1].addShapeBox(1F, 1F, -3F, 30, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 1
		attachmentModel[1].setRotationPoint(0F, 0F, 0F);

		attachmentModel[2].addBox(1F, -1F, -3F, 30, 2, 6, 0F); // Box 2
		attachmentModel[2].setRotationPoint(0F, 0F, 0F);

		attachmentModel[3].addShapeBox(3F, -3.5F, -3.5F, 8, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 3
		attachmentModel[3].setRotationPoint(0F, 0F, 0F);

		attachmentModel[4].addBox(3F, -1.5F, -3.5F, 8, 3, 7, 0F); // Box 4
		attachmentModel[4].setRotationPoint(0F, 0F, 0F);

		attachmentModel[5].addShapeBox(3F, 1.5F, -3.5F, 8, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 5
		attachmentModel[5].setRotationPoint(0F, 0F, 0F);

		attachmentModel[6].addBox(-1F, -1.5F, -3.5F, 2, 3, 7, 0F); // Box 6
		attachmentModel[6].setRotationPoint(0F, 0F, 0F);

		attachmentModel[7].addShapeBox(-1F, -3.5F, -3.5F, 2, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 7
		attachmentModel[7].setRotationPoint(0F, 0F, 0F);

		attachmentModel[8].addShapeBox(-1F, 1.5F, -3.5F, 2, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 8
		attachmentModel[8].setRotationPoint(0F, 0F, 0F);

		renderOffset = 0F;

		flipAll();
	}
}