package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelBanana extends ModelGun
{
	int textureX = 128;
	int textureY = 64;

	public ModelBanana()
	{
		gunModel = new ModelRendererTurbo[11];
		gunModel[0] = new ModelRendererTurbo(this, 0, 0, textureX, textureY); // Box 1
		gunModel[1] = new ModelRendererTurbo(this, 0, 0, textureX, textureY); // Box 2
		gunModel[2] = new ModelRendererTurbo(this, 0, 0, textureX, textureY); // Box 3
		gunModel[3] = new ModelRendererTurbo(this, 0, 0, textureX, textureY); // Box 4
		gunModel[4] = new ModelRendererTurbo(this, 0, 0, textureX, textureY); // Box 8
		gunModel[5] = new ModelRendererTurbo(this, 0, 0, textureX, textureY); // Box 9
		gunModel[6] = new ModelRendererTurbo(this, 0, 0, textureX, textureY); // Box 10
		gunModel[7] = new ModelRendererTurbo(this, 0, 0, textureX, textureY); // Box 11
		gunModel[8] = new ModelRendererTurbo(this, 0, 0, textureX, textureY); // Box 13
		gunModel[9] = new ModelRendererTurbo(this, 0, 0, textureX, textureY); // Box 16
		gunModel[10] = new ModelRendererTurbo(this, 0, 13, textureX, textureY); // Box 17

		gunModel[0].addShapeBox(0F, 0F, 0F, 16, 8, 4, 0F,0F, -1F, 0F, 0F, -3F, -2F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -3F, -2F, 0F, -2F, 0F, 0F, 0F, 0F); // Box 1
		gunModel[0].setRotationPoint(20F, -16F, -3F);

		gunModel[1].addShapeBox(0F, 0F, 0F, 8, 8, 4, 0F,0F, -2F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F); // Box 2
		gunModel[1].setRotationPoint(0F, -16F, 1F);

		gunModel[2].addShapeBox(0F, 0F, 0F, 12, 8, 4, 0F,0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 3
		gunModel[2].setRotationPoint(8F, -16F, -3F);

		gunModel[3].addShapeBox(0F, 0F, 0F, 12, 8, 4, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F); // Box 4
		gunModel[3].setRotationPoint(8F, -16F, 1F);

		gunModel[4].addShapeBox(0F, 0F, 0F, 16, 8, 4, 0F,0F, 0F, 0F, 0F, -2F, 0F, 0F, -3F, -2F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -3F, -2F, 0F, -1F, 0F); // Box 8
		gunModel[4].setRotationPoint(20F, -16F, 1F);

		gunModel[5].addShapeBox(0F, 0F, 0F, 8, 8, 4, 0F,0F, -3F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 9
		gunModel[5].setRotationPoint(0F, -16F, -3F);

		gunModel[6].addShapeBox(0F, 0F, 0F, 4, 7, 4, 0F,0F, -3F, -2F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 2F, 0F, -2F, 0F, -1F, 0F, 0F, 0F, 0F, 2F, 0F, 0F); // Box 10
		gunModel[6].setRotationPoint(-4F, -14F, -3F);

		gunModel[7].addShapeBox(0F, 0F, 0F, 4, 7, 4, 0F,0F, -2F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -3F, -2F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 2F, 0F, -2F); // Box 11
		gunModel[7].setRotationPoint(-4F, -14F, 1F);

		gunModel[8].addShapeBox(0F, 0F, 0F, 6, 9, 2, 0F,0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, -1F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 1F, 2F, 0F, 0F); // Box 13
		gunModel[8].setRotationPoint(-6F, -8F, 1F);

		gunModel[9].addShapeBox(0F, 0F, 0F, 6, 9, 2, 0F,0F, -1F, 0F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, -1F, 0F, 2F, 0F, 0F, -2F, 0F, 1F, -2F, 0F, 0F, 2F, 0F, 0F); // Box 16
		gunModel[9].setRotationPoint(-6F, -8F, -1F);

		gunModel[10].addBox(0F, 0F, 0F, 4, 4, 2, 0F); // Box 17
		gunModel[10].setRotationPoint(-7F, 1F, 0F);



		barrelAttachPoint = new Vector3f(0F /16F, 0F /16F, 0F /16F);
		stockAttachPoint = new Vector3f(0F /16F, 0F /16F, 0F /16F);
		scopeAttachPoint = new Vector3f(0F /16F, 0F /16F, 0F /16F);
		gripAttachPoint = new Vector3f(0 /16F, 0F /16F, 0F /16F);

		gunSlideDistance = 0F;
		animationType = EnumAnimationType.NONE;


		translateAll(0F, 0F, 0F);


		flipAll();
	}
}