package com.flansmod.client.model.dwfirearms; //Path where the model is located

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelTimemarker extends ModelGun //Same as Filename
{
	int textureX = 512;
	int textureY = 256;

	public ModelTimemarker() //Same as Filename
	{
		gunModel = new ModelRendererTurbo[52];
		gunModel[0] = new ModelRendererTurbo(this, 102, 59, textureX, textureY); // barrelFront
		gunModel[1] = new ModelRendererTurbo(this, 127, 47, textureX, textureY); // barrelFront2
		gunModel[2] = new ModelRendererTurbo(this, 56, 17, textureX, textureY); // barrelRail
		gunModel[3] = new ModelRendererTurbo(this, 54, 84, textureX, textureY); // body2
		gunModel[4] = new ModelRendererTurbo(this, 154, 49, textureX, textureY); // body3
		gunModel[5] = new ModelRendererTurbo(this, 77, 100, textureX, textureY); // body4
		gunModel[6] = new ModelRendererTurbo(this, 77, 93, textureX, textureY); // body5
		gunModel[7] = new ModelRendererTurbo(this, 56, 30, textureX, textureY); // body6
		gunModel[8] = new ModelRendererTurbo(this, 113, 30, textureX, textureY); // body7
		gunModel[9] = new ModelRendererTurbo(this, 95, 74, textureX, textureY); // body8
		gunModel[10] = new ModelRendererTurbo(this, 134, 73, textureX, textureY); // body9
		gunModel[11] = new ModelRendererTurbo(this, 56, 21, textureX, textureY); // crane1
		gunModel[12] = new ModelRendererTurbo(this, 56, 21, textureX, textureY); // crane1-2
		gunModel[13] = new ModelRendererTurbo(this, 56, 21, textureX, textureY); // crane1-3
		gunModel[14] = new ModelRendererTurbo(this, 137, 19, textureX, textureY); // hammer2
		gunModel[15] = new ModelRendererTurbo(this, 56, 9, textureX, textureY); // mainBarrelBottom
		gunModel[16] = new ModelRendererTurbo(this, 56, 1, textureX, textureY); // mainBarrelMiddle
		gunModel[17] = new ModelRendererTurbo(this, 72, 70, textureX, textureY); // Box 2
		gunModel[18] = new ModelRendererTurbo(this, 56, 59, textureX, textureY); // Box 4
		gunModel[19] = new ModelRendererTurbo(this, 135, 39, textureX, textureY); // Box 7
		gunModel[20] = new ModelRendererTurbo(this, 154, 39, textureX, textureY); // Box 8
		gunModel[21] = new ModelRendererTurbo(this, 77, 100, textureX, textureY); // Box 10
		gunModel[22] = new ModelRendererTurbo(this, 77, 93, textureX, textureY); // Box 11
		gunModel[23] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 12
		gunModel[24] = new ModelRendererTurbo(this, 1, 14, textureX, textureY); // Box 13
		gunModel[25] = new ModelRendererTurbo(this, 1, 29, textureX, textureY); // Box 14
		gunModel[26] = new ModelRendererTurbo(this, 55, 69, textureX, textureY); // Box 17
		gunModel[27] = new ModelRendererTurbo(this, 102, 59, textureX, textureY); // Box 18
		gunModel[28] = new ModelRendererTurbo(this, 86, 87, textureX, textureY); // Box 19
		gunModel[29] = new ModelRendererTurbo(this, 117, 87, textureX, textureY); // Box 20
		gunModel[30] = new ModelRendererTurbo(this, 117, 87, textureX, textureY); // Box 21
		gunModel[31] = new ModelRendererTurbo(this, 137, 93, textureX, textureY); // Box 22
		gunModel[32] = new ModelRendererTurbo(this, 137, 82, textureX, textureY); // Box 23
		gunModel[33] = new ModelRendererTurbo(this, 85, 106, textureX, textureY); // Box 24
		gunModel[34] = new ModelRendererTurbo(this, 85, 106, textureX, textureY); // Box 25
		gunModel[35] = new ModelRendererTurbo(this, 144, 106, textureX, textureY); // Box 26
		gunModel[36] = new ModelRendererTurbo(this, 144, 106, textureX, textureY); // Box 27
		gunModel[37] = new ModelRendererTurbo(this, 123, 106, textureX, textureY); // Box 28
		gunModel[38] = new ModelRendererTurbo(this, 104, 106, textureX, textureY); // Box 29
		gunModel[39] = new ModelRendererTurbo(this, 56, 9, textureX, textureY); // Box 30
		gunModel[40] = new ModelRendererTurbo(this, 54, 106, textureX, textureY); // Box 31
		gunModel[41] = new ModelRendererTurbo(this, 163, 108, textureX, textureY); // Box 32
		gunModel[42] = new ModelRendererTurbo(this, 163, 108, textureX, textureY); // Box 33
		gunModel[43] = new ModelRendererTurbo(this, 129, 64, textureX, textureY); // Box 34
		gunModel[44] = new ModelRendererTurbo(this, 138, 64, textureX, textureY); // Box 35
		gunModel[45] = new ModelRendererTurbo(this, 1, 14, textureX, textureY); // Box 36
		gunModel[46] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 37
		gunModel[47] = new ModelRendererTurbo(this, 56, 47, textureX, textureY); // Box 38
		gunModel[48] = new ModelRendererTurbo(this, 56, 38, textureX, textureY); // Box 39
		gunModel[49] = new ModelRendererTurbo(this, 79, 59, textureX, textureY); // Box 40
		gunModel[50] = new ModelRendererTurbo(this, 79, 59, textureX, textureY); // Box 41
		gunModel[51] = new ModelRendererTurbo(this, 137, 25, textureX, textureY); // Box 48

		gunModel[0].addShapeBox(0F, 0F, 0F, 6, 2, 7, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // barrelFront
		gunModel[0].setRotationPoint(24F, -27F, -3.5F);

		gunModel[1].addBox(0F, 0F, 0F, 6, 9, 7, 0F); // barrelFront2
		gunModel[1].setRotationPoint(24F, -25F, -3.5F);

		gunModel[2].addBox(0F, 0F, 0F, 35, 1, 2, 0F); // barrelRail
		gunModel[2].setRotationPoint(30F, -28F, -1F);

		gunModel[3].addBox(0F, 0F, 0F, 5, 15, 6, 0F); // body2
		gunModel[3].setRotationPoint(6F, -26F, -3F);

		gunModel[4].addBox(0F, 0F, 0F, 7, 8, 6, 0F); // body3
		gunModel[4].setRotationPoint(-1F, -19F, -3F);

		gunModel[5].addShapeBox(0F, 0F, 0F, 2, 3, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F); // body4
		gunModel[5].setRotationPoint(4F, -22F, 1F);

		gunModel[6].addShapeBox(0F, 0F, 0F, 1, 4, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F); // body5
		gunModel[6].setRotationPoint(5F, -26F, 1F);

		gunModel[7].addShapeBox(0F, 0F, 0F, 22, 1, 6, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body6
		gunModel[7].setRotationPoint(8F, -28F, -3F);

		gunModel[8].addBox(0F, 0F, 0F, 16, 1, 6, 0F); // body7
		gunModel[8].setRotationPoint(8F, -27F, -3F);

		gunModel[9].addShapeBox(0F, 0F, 0F, 13, 3, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F); // body8
		gunModel[9].setRotationPoint(11F, -14F, -3F);

		gunModel[10].addShapeBox(0F, 0F, 0F, 6, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, 0F, 0F, 0F); // body9
		gunModel[10].setRotationPoint(24F, -14F, -3F);

		gunModel[11].addShapeBox(0F, 0F, 0F, 34, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // crane1
		gunModel[11].setRotationPoint(30F, -22F, -3F);

		gunModel[12].addBox(0F, 0F, 0F, 34, 2, 6, 0F); // crane1-2
		gunModel[12].setRotationPoint(30F, -20F, -3F);

		gunModel[13].addShapeBox(0F, 0F, 0F, 34, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // crane1-3
		gunModel[13].setRotationPoint(30F, -18F, -3F);

		gunModel[14].addShapeBox(0F, 0F, 0F, 5, 3, 2, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F, -3F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, -3F, 0F); // hammer2
		gunModel[14].setRotationPoint(-4F, -24F, -1F);

		gunModel[15].addShapeBox(0F, 0F, 0F, 35, 2, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F); // mainBarrelBottom
		gunModel[15].setRotationPoint(30F, -23.5F, -2.5F);

		gunModel[16].addBox(0F, 0F, 0F, 35, 2, 5, 0F); // mainBarrelMiddle
		gunModel[16].setRotationPoint(30F, -25.5F, -2.5F);

		gunModel[17].addBox(0F, 0F, 0F, 4, 6, 7, 0F); // Box 2
		gunModel[17].setRotationPoint(58F, -20F, -3.5F);

		gunModel[18].addShapeBox(0F, 0F, 0F, 4, 2, 7, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F); // Box 4
		gunModel[18].setRotationPoint(58F, -22.5F, -3.5F);

		gunModel[19].addShapeBox(0F, 0F, 0F, 3, 1, 6, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 7
		gunModel[19].setRotationPoint(5F, -27F, -3F);

		gunModel[20].addShapeBox(0F, 0F, 0F, 2, 1, 6, 0F, -1F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, -1F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 8
		gunModel[20].setRotationPoint(6F, -28F, -3F);

		gunModel[21].addShapeBox(0F, 0F, 0F, 2, 3, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F); // Box 10
		gunModel[21].setRotationPoint(4F, -22F, -3F);

		gunModel[22].addShapeBox(0F, 0F, 0F, 1, 4, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F); // Box 11
		gunModel[22].setRotationPoint(5F, -26F, -3F);

		gunModel[23].addShapeBox(0F, 0F, 0F, 2, 2, 10, 0F, 0F, -0.5F, -4F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, -0.5F, -4F, 0F, 0.5F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, -1F); // Box 12
		gunModel[23].setRotationPoint(8.5F, -26F, -5F);

		gunModel[24].addShapeBox(0F, 0F, 0F, 2, 2, 12, 0F, 0F, -0.5F, -2F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, -0.5F, -2F, 0F, 0.5F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, -1F); // Box 13
		gunModel[24].setRotationPoint(8.5F, -24F, -6F);

		gunModel[25].addShapeBox(0F, 0F, 0F, 2, 4, 12, 0F, 0F, -0.5F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -1F, 0F, -0.5F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -1F); // Box 14
		gunModel[25].setRotationPoint(8.5F, -22F, -6F);

		gunModel[26].addShapeBox(0F, 0F, 0F, 2, 8, 6, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F); // Box 17
		gunModel[26].setRotationPoint(-3F, -19F, -3F);

		gunModel[27].addShapeBox(0F, 0F, 0F, 6, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 18
		gunModel[27].setRotationPoint(24F, -16F, -3.5F);

		gunModel[28].addShapeBox(0F, 0F, 0F, 8, 11, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F); // Box 19
		gunModel[28].setRotationPoint(-8.5F, -4F, -3.5F);

		gunModel[29].addShapeBox(0F, 0F, 0F, 2, 11, 7, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 1F, 0F, -2F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, -2F); // Box 20
		gunModel[29].setRotationPoint(-10.5F, -4F, -3.5F);

		gunModel[30].addShapeBox(0F, 0F, 0F, 2, 11, 7, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, -2F, -1F, 0F, -2F, 1F, 0F, 0F); // Box 21
		gunModel[30].setRotationPoint(-0.5F, -4F, -3.5F);

		gunModel[31].addShapeBox(0F, 0F, 0F, 8, 5, 7, 0F, -2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 22
		gunModel[31].setRotationPoint(-8.5F, -9F, -3.5F);

		gunModel[32].addShapeBox(0F, 0F, 0F, 8, 3, 7, 0F, -3F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 23
		gunModel[32].setRotationPoint(-6.5F, -12F, -3.5F);

		gunModel[33].addShapeBox(0F, 0F, 0F, 2, 5, 7, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, -2F, -2F, 0F, -2F, 2F, 0F, 0F); // Box 24
		gunModel[33].setRotationPoint(1.5F, -9F, -3.5F);

		gunModel[34].addShapeBox(0F, 0F, 0F, 2, 5, 7, 0F, -2F, 0F, -2F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F); // Box 25
		gunModel[34].setRotationPoint(-10.5F, -9F, -3.5F);

		gunModel[35].addShapeBox(0F, 0F, 0F, 2, 3, 7, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, -2F, -3F, 0F, -2F, 3F, 0F, 0F); // Box 26
		gunModel[35].setRotationPoint(4.5F, -12F, -3.5F);

		gunModel[36].addShapeBox(0F, 0F, 0F, 2, 3, 7, 0F, -3F, 0F, -2F, 3F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F); // Box 27
		gunModel[36].setRotationPoint(-8.5F, -12F, -3.5F);

		gunModel[37].addShapeBox(0F, 0F, 0F, 3, 3, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 5F, 0F, 0F, 5F, 0F, 0F, 0F, 0F, 0F); // Box 28
		gunModel[37].setRotationPoint(-3.5F, -15F, -3.5F);

		gunModel[38].addShapeBox(0F, 0F, 0F, 2, 3, 7, 0F, -0.5F, -1F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -1F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F); // Box 29
		gunModel[38].setRotationPoint(-5.5F, -15F, -3.5F);

		gunModel[39].addShapeBox(0F, 0F, 0F, 35, 2, 5, 0F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 30
		gunModel[39].setRotationPoint(30F, -27.5F, -2.5F);

		gunModel[40].addShapeBox(0F, 0F, 0F, 8, 1, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 31
		gunModel[40].setRotationPoint(-9.5F, 7F, -3.5F);

		gunModel[41].addShapeBox(0F, 0F, 0F, 2, 1, 7, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, -1F, 0F, -2F, 0F, 0F, -1F, 0F, 0F, -1F, -1F, 0F, -2F); // Box 32
		gunModel[41].setRotationPoint(-11.5F, 7F, -3.5F);

		gunModel[42].addShapeBox(0F, 0F, 0F, 2, 1, 7, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, -1F, -1F, 0F, -2F, -1F, 0F, -2F, 0F, 0F, -1F); // Box 33
		gunModel[42].setRotationPoint(-1.5F, 7F, -3.5F);

		gunModel[43].addShapeBox(0F, 0F, 0F, 2, 6, 2, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 34
		gunModel[43].setRotationPoint(1F, -25F, -1F);

		gunModel[44].addBox(0F, 0F, 0F, 3, 6, 2, 0F); // Box 35
		gunModel[44].setRotationPoint(3F, -25F, -1F);

		gunModel[45].addShapeBox(0F, 0F, 0F, 2, 2, 12, 0F, 0F, 0.5F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, -1F, 0F, -0.5F, -2F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, -0.5F, -2F); // Box 36
		gunModel[45].setRotationPoint(8.5F, -18F, -6F);

		gunModel[46].addShapeBox(0F, 0F, 0F, 2, 2, 10, 0F, 0F, 0.5F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, -1F, 0F, -0.5F, -4F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, -0.5F, -4F); // Box 37
		gunModel[46].setRotationPoint(8.5F, -16F, -5F);

		gunModel[47].addBox(0F, 0F, 0F, 28, 4, 7, 0F); // Box 38
		gunModel[47].setRotationPoint(30F, -18F, -3.5F);

		gunModel[48].addShapeBox(0F, 0F, 0F, 32, 1, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 39
		gunModel[48].setRotationPoint(30F, -14F, -3.5F);

		gunModel[49].addShapeBox(0F, 0F, 0F, 4, 2, 7, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 40
		gunModel[49].setRotationPoint(30F, -20F, -3.5F);

		gunModel[50].addShapeBox(0F, 0F, 0F, 4, 2, 7, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 41
		gunModel[50].setRotationPoint(44F, -20F, -3.5F);

		gunModel[51].addShapeBox(0F, 0F, 0F, 4, 2, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 48
		gunModel[51].setRotationPoint(60F, -16F, -1F);


		defaultScopeModel = new ModelRendererTurbo[3];
		defaultScopeModel[0] = new ModelRendererTurbo(this, 30, 41, textureX, textureY); // ironSight1
		defaultScopeModel[1] = new ModelRendererTurbo(this, 30, 41, textureX, textureY); // ironSight1-2
		defaultScopeModel[2] = new ModelRendererTurbo(this, 43, 42, textureX, textureY); // ironSight2

		defaultScopeModel[0].addBox(0F, 0F, 0F, 4, 2, 2, 0F); // ironSight1
		defaultScopeModel[0].setRotationPoint(7.1F, -29F, 0.5F);

		defaultScopeModel[1].addBox(0F, 0F, 0F, 4, 2, 2, 0F); // ironSight1-2
		defaultScopeModel[1].setRotationPoint(7.1F, -29F, -2.5F);

		defaultScopeModel[2].addShapeBox(0F, 0F, 0F, 4, 2, 1, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // ironSight2
		defaultScopeModel[2].setRotationPoint(61F, -30F, -0.5F);


		ammoModel = new ModelRendererTurbo[18];
		ammoModel[0] = new ModelRendererTurbo(this, 1, 46, textureX, textureY); // bullet1
		ammoModel[1] = new ModelRendererTurbo(this, 1, 46, textureX, textureY); // bullet1-2
		ammoModel[2] = new ModelRendererTurbo(this, 1, 46, textureX, textureY); // bullet1-3
		ammoModel[3] = new ModelRendererTurbo(this, 26, 46, textureX, textureY); // bullet2
		ammoModel[4] = new ModelRendererTurbo(this, 26, 46, textureX, textureY); // bullet2-2
		ammoModel[5] = new ModelRendererTurbo(this, 26, 46, textureX, textureY); // bullet2-3
		ammoModel[6] = new ModelRendererTurbo(this, 1, 51, textureX, textureY); // bullet3
		ammoModel[7] = new ModelRendererTurbo(this, 1, 51, textureX, textureY); // bullet3-2
		ammoModel[8] = new ModelRendererTurbo(this, 1, 51, textureX, textureY); // bullet3-3
		ammoModel[9] = new ModelRendererTurbo(this, 26, 51, textureX, textureY); // bullet4
		ammoModel[10] = new ModelRendererTurbo(this, 26, 51, textureX, textureY); // bullet4-2
		ammoModel[11] = new ModelRendererTurbo(this, 26, 51, textureX, textureY); // bullet4-3
		ammoModel[12] = new ModelRendererTurbo(this, 1, 56, textureX, textureY); // bullet5
		ammoModel[13] = new ModelRendererTurbo(this, 1, 56, textureX, textureY); // bullet5-2
		ammoModel[14] = new ModelRendererTurbo(this, 1, 56, textureX, textureY); // bullet5-3
		ammoModel[15] = new ModelRendererTurbo(this, 26, 56, textureX, textureY); // bullet6
		ammoModel[16] = new ModelRendererTurbo(this, 26, 56, textureX, textureY); // bullet6-2
		ammoModel[17] = new ModelRendererTurbo(this, 26, 56, textureX, textureY); // bullet6-3

		ammoModel[0].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // bullet1
		ammoModel[0].setRotationPoint(11.3F, -25.5F, -1.5F);

		ammoModel[1].addBox(0F, 0F, 0F, 9, 1, 3, 0F); // bullet1-2
		ammoModel[1].setRotationPoint(11.3F, -24.5F, -1.5F);

		ammoModel[2].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // bullet1-3
		ammoModel[2].setRotationPoint(11.3F, -23.5F, -1.5F);

		ammoModel[3].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // bullet2
		ammoModel[3].setRotationPoint(11.3F, -23.5F, -5F);

		ammoModel[4].addBox(0F, 0F, 0F, 9, 1, 3, 0F); // bullet2-2
		ammoModel[4].setRotationPoint(11.3F, -22.5F, -5F);

		ammoModel[5].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // bullet2-3
		ammoModel[5].setRotationPoint(11.3F, -21.5F, -5F);

		ammoModel[6].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // bullet3
		ammoModel[6].setRotationPoint(11.3F, -19.5F, -5F);

		ammoModel[7].addBox(0F, 0F, 0F, 9, 1, 3, 0F); // bullet3-2
		ammoModel[7].setRotationPoint(11.3F, -18.5F, -5F);

		ammoModel[8].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // bullet3-3
		ammoModel[8].setRotationPoint(11.3F, -17.5F, -5F);

		ammoModel[9].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // bullet4
		ammoModel[9].setRotationPoint(11.3F, -17.5F, -1.5F);

		ammoModel[10].addBox(0F, 0F, 0F, 9, 1, 3, 0F); // bullet4-2
		ammoModel[10].setRotationPoint(11.3F, -16.5F, -1.5F);

		ammoModel[11].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // bullet4-3
		ammoModel[11].setRotationPoint(11.3F, -15.5F, -1.5F);

		ammoModel[12].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // bullet5
		ammoModel[12].setRotationPoint(11.3F, -19.5F, 2F);

		ammoModel[13].addBox(0F, 0F, 0F, 9, 1, 3, 0F); // bullet5-2
		ammoModel[13].setRotationPoint(11.3F, -18.5F, 2F);

		ammoModel[14].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // bullet5-3
		ammoModel[14].setRotationPoint(11.3F, -17.5F, 2F);

		ammoModel[15].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // bullet6
		ammoModel[15].setRotationPoint(11.3F, -23.5F, 2F);

		ammoModel[16].addBox(0F, 0F, 0F, 9, 1, 3, 0F); // bullet6-2
		ammoModel[16].setRotationPoint(11.3F, -22.5F, 2F);

		ammoModel[17].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // bullet6-3
		ammoModel[17].setRotationPoint(11.3F, -21.5F, 2F);


		revolverBarrelModel = new ModelRendererTurbo[7];
		revolverBarrelModel[0] = new ModelRendererTurbo(this, 1, 93, textureX, textureY); // canister1
		revolverBarrelModel[1] = new ModelRendererTurbo(this, 1, 93, textureX, textureY); // canister1-2
		revolverBarrelModel[2] = new ModelRendererTurbo(this, 1, 78, textureX, textureY); // canister2
		revolverBarrelModel[3] = new ModelRendererTurbo(this, 1, 78, textureX, textureY); // canister2-2
		revolverBarrelModel[4] = new ModelRendererTurbo(this, 1, 61, textureX, textureY); // canister3
		revolverBarrelModel[5] = new ModelRendererTurbo(this, 1, 29, textureX, textureY); // Box 5
		revolverBarrelModel[6] = new ModelRendererTurbo(this, 1, 29, textureX, textureY); // Box 6

		revolverBarrelModel[0].addShapeBox(0F, 0F, 0F, 11, 2, 10, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // canister1
		revolverBarrelModel[0].setRotationPoint(12.5F, -26F, -5F);

		revolverBarrelModel[1].addShapeBox(0F, 0F, 0F, 11, 2, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // canister1-2
		revolverBarrelModel[1].setRotationPoint(12.5F, -16F, -5F);

		revolverBarrelModel[2].addShapeBox(0F, 0F, 0F, 11, 2, 12, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // canister2
		revolverBarrelModel[2].setRotationPoint(12.5F, -24F, -6F);

		revolverBarrelModel[3].addShapeBox(0F, 0F, 0F, 11, 2, 12, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // canister2-2
		revolverBarrelModel[3].setRotationPoint(12.5F, -18F, -6F);

		revolverBarrelModel[4].addBox(0F, 0F, 0F, 11, 4, 12, 0F); // canister3
		revolverBarrelModel[4].setRotationPoint(12.5F, -22F, -6F);

		revolverBarrelModel[5].addBox(0F, 0F, 0F, 1, 4, 1, 0F); // Box 5
		revolverBarrelModel[5].setRotationPoint(11.5F, -22F, -6F);

		revolverBarrelModel[6].addBox(0F, 0F, 0F, 1, 4, 1, 0F); // Box 6
		revolverBarrelModel[6].setRotationPoint(11.5F, -22F, 5F);

		barrelAttachPoint = new Vector3f(65F /16F, 24.5F /16F, 0F /16F);

		animationType = EnumAnimationType.REVOLVER;

		revolverFlipAngle = -20F;
		
		translateAll(0F, 0F, 0F);


		flipAll();
	}
}