package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.ModelAttachment;
import com.flansmod.client.tmt.ModelRendererTurbo;

import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelLPMKatAssaultStock extends ModelAttachment 
{
	int textureX = 512;
	int textureY = 256;

	public ModelLPMKatAssaultStock()
	{	
		attachmentModel = new ModelRendererTurbo[19];
		attachmentModel[0] = new ModelRendererTurbo(this, 203, 49, textureX, textureY); // Box 82
		attachmentModel[1] = new ModelRendererTurbo(this, 203, 11, textureX, textureY); // Box 83
		attachmentModel[2] = new ModelRendererTurbo(this, 203, 2, textureX, textureY); // Box 84
		attachmentModel[3] = new ModelRendererTurbo(this, 241, 40, textureX, textureY); // Box 85
		attachmentModel[4] = new ModelRendererTurbo(this, 228, 55, textureX, textureY); // Box 86
		attachmentModel[5] = new ModelRendererTurbo(this, 241, 27, textureX, textureY); // Box 87
		attachmentModel[6] = new ModelRendererTurbo(this, 203, 28, textureX, textureY); // Box 88
		attachmentModel[7] = new ModelRendererTurbo(this, 221, 49, textureX, textureY); // Box 89
		attachmentModel[8] = new ModelRendererTurbo(this, 228, 27, textureX, textureY); // Box 90
		attachmentModel[9] = new ModelRendererTurbo(this, 203, 64, textureX, textureY); // Box 32
		attachmentModel[10] = new ModelRendererTurbo(this, 246, 64, textureX, textureY); // Box 33
		attachmentModel[11] = new ModelRendererTurbo(this, 203, 76, textureX, textureY); // Box 34
		attachmentModel[12] = new ModelRendererTurbo(this, 203, 88, textureX, textureY); // Box 35
		attachmentModel[13] = new ModelRendererTurbo(this, 203, 102, textureX, textureY); // Box 36
		attachmentModel[14] = new ModelRendererTurbo(this, 203, 118, textureX, textureY); // Box 38
		attachmentModel[15] = new ModelRendererTurbo(this, 240, 76, textureX, textureY); // Box 39
		attachmentModel[16] = new ModelRendererTurbo(this, 240, 88, textureX, textureY); // Box 40
		attachmentModel[17] = new ModelRendererTurbo(this, 240, 102, textureX, textureY); // Box 41
		attachmentModel[18] = new ModelRendererTurbo(this, 240, 118, textureX, textureY); // Box 42

		attachmentModel[0].addShapeBox(-5F, -2F, -3.5F, 5, 7, 7, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F); // Box 82
		attachmentModel[0].setRotationPoint(0F, 0F, 0F);

		attachmentModel[1].addShapeBox(-30F, -2F, -3.5F, 25, 8, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 10F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 10F, 0F); // Box 83
		attachmentModel[1].setRotationPoint(0F, 0F, 0F);

		attachmentModel[2].addShapeBox(-30F, -3F, -3.5F, 25, 1, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 84
		attachmentModel[2].setRotationPoint(0F, 0F, 0F);

		attachmentModel[3].addShapeBox(-5F, -5F, -3.5F, 5, 1, 7, 0F, 0F, -2F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, -2F, -1F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F); // Box 85
		attachmentModel[3].setRotationPoint(0F, 0F, 0F);

		attachmentModel[4].addShapeBox(-37F, -3F, -3.5F, 6, 1, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 86
		attachmentModel[4].setRotationPoint(0F, 0F, 0F);

		attachmentModel[5].addShapeBox(-37F, -2F, -3.5F, 6, 5, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F); // Box 87
		attachmentModel[5].setRotationPoint(0F, 0F, 0F);

		attachmentModel[6].addBox(-36F, 3F, -3.5F, 5, 13, 7, 0F); // Box 88
		attachmentModel[6].setRotationPoint(0F, 0F, 0F);

		attachmentModel[7].addShapeBox(-31F, -2F, -2.5F, 1, 1, 5, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 89
		attachmentModel[7].setRotationPoint(0F, 0F, 0F);

		attachmentModel[8].addBox(-31F, -1F, -2.5F, 1, 16, 5, 0F); // Box 90
		attachmentModel[8].setRotationPoint(0F, 0F, 0F);

		attachmentModel[9].addShapeBox(1F, 6F, -4F, 11, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F); // Box 32
		attachmentModel[9].setRotationPoint(0F, 0F, 0F);

		attachmentModel[10].addBox(12F, 6F, -4F, 6, 3, 8, 0F); // Box 33
		attachmentModel[10].setRotationPoint(0F, 0F, 0F);

		attachmentModel[11].addShapeBox(3F, 9F, -4F, 10, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 34
		attachmentModel[11].setRotationPoint(0F, 0F, 0F);

		attachmentModel[12].addShapeBox(3F, 12F, -4F, 10, 5, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F); // Box 35
		attachmentModel[12].setRotationPoint(0F, 0F, 0F);

		attachmentModel[13].addShapeBox(1F, 17F, -4F, 10, 7, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, -3F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 3F, -3F, 0F); // Box 36
		attachmentModel[13].setRotationPoint(0F, 0F, 0F);

		attachmentModel[14].addShapeBox(-2F, 24F, -4F, 10, 2, 8, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F); // Box 38
		attachmentModel[14].setRotationPoint(0F, 0F, 0F);

		attachmentModel[15].addShapeBox(13F, 9F, -4F, 2, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F); // Box 39
		attachmentModel[15].setRotationPoint(0F, 0F, 0F);

		attachmentModel[16].addShapeBox(13F, 12F, -4F, 2, 5, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, -1.5F, -2F, 0F, -1.5F, 2F, 0F, 0F); // Box 40
		attachmentModel[16].setRotationPoint(0F, 0F, 0F);

		attachmentModel[17].addShapeBox(11F, 17F, -4F, 2, 7, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, -1.5F, -3F, 0F, -1.5F, 3F, 0F, 0F); // Box 41
		attachmentModel[17].setRotationPoint(0F, 0F, 0F);

		attachmentModel[18].addShapeBox(8F, 24F, -4F, 2, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F); // Box 42
		attachmentModel[18].setRotationPoint(0F, 0F, 0F);

		renderOffset = 0F;

		flipAll();
	}
}