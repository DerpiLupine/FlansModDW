package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.ModelAttachment;
import com.flansmod.client.tmt.ModelRendererTurbo;

import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelLPMShortScope extends ModelAttachment
{
	int textureX = 512;
	int textureY = 256;

	public ModelLPMShortScope()
	{
		attachmentModel = new ModelRendererTurbo[50];
		attachmentModel[0] = new ModelRendererTurbo(this, 1, 46, textureX, textureY); // mounter
		attachmentModel[1] = new ModelRendererTurbo(this, 1, 59, textureX, textureY); // mounterBase2
		attachmentModel[2] = new ModelRendererTurbo(this, 85, 27, textureX, textureY); // scope4
		attachmentModel[3] = new ModelRendererTurbo(this, 98, 21, textureX, textureY); // scope3
		attachmentModel[4] = new ModelRendererTurbo(this, 85, 21, textureX, textureY); // scope3
		attachmentModel[5] = new ModelRendererTurbo(this, 85, 15, textureX, textureY); // scope3
		attachmentModel[6] = new ModelRendererTurbo(this, 98, 15, textureX, textureY); // scope3
		attachmentModel[7] = new ModelRendererTurbo(this, 85, 45, textureX, textureY); // scope1
		attachmentModel[8] = new ModelRendererTurbo(this, 85, 62, textureX, textureY); // scope2
		attachmentModel[9] = new ModelRendererTurbo(this, 85, 36, textureX, textureY); // Box 13
		attachmentModel[10] = new ModelRendererTurbo(this, 85, 8, textureX, textureY); // Box 16
		attachmentModel[11] = new ModelRendererTurbo(this, 85, 1, textureX, textureY); // Box 17
		attachmentModel[12] = new ModelRendererTurbo(this, 85, 57, textureX, textureY); // Box 18
		attachmentModel[13] = new ModelRendererTurbo(this, 85, 52, textureX, textureY); // Box 19
		attachmentModel[14] = new ModelRendererTurbo(this, 139, 39, textureX, textureY); // Box 1
		attachmentModel[15] = new ModelRendererTurbo(this, 145, 21, textureX, textureY); // Box 2
		attachmentModel[16] = new ModelRendererTurbo(this, 139, 33, textureX, textureY); // Box 3
		attachmentModel[17] = new ModelRendererTurbo(this, 145, 15, textureX, textureY); // Box 4
		attachmentModel[18] = new ModelRendererTurbo(this, 121, 59, textureX, textureY); // Box 5
		attachmentModel[19] = new ModelRendererTurbo(this, 121, 45, textureX, textureY); // Box 6
		attachmentModel[20] = new ModelRendererTurbo(this, 85, 45, textureX, textureY); // Box 7
		attachmentModel[21] = new ModelRendererTurbo(this, 98, 45, textureX, textureY); // Box 8
		attachmentModel[22] = new ModelRendererTurbo(this, 98, 56, textureX, textureY); // Box 9
		attachmentModel[23] = new ModelRendererTurbo(this, 128, 3, textureX, textureY); // Box 10
		attachmentModel[24] = new ModelRendererTurbo(this, 128, 9, textureX, textureY); // Box 11
		attachmentModel[25] = new ModelRendererTurbo(this, 110, 36, textureX, textureY); // Box 12
		attachmentModel[26] = new ModelRendererTurbo(this, 145, 3, textureX, textureY); // Box 13
		attachmentModel[27] = new ModelRendererTurbo(this, 145, 9, textureX, textureY); // Box 14
		attachmentModel[28] = new ModelRendererTurbo(this, 162, 4, textureX, textureY); // Box 15
		attachmentModel[29] = new ModelRendererTurbo(this, 85, 69, textureX, textureY); // Box 16
		attachmentModel[30] = new ModelRendererTurbo(this, 111, 15, textureX, textureY); // Box 17
		attachmentModel[31] = new ModelRendererTurbo(this, 111, 21, textureX, textureY); // Box 18
		attachmentModel[32] = new ModelRendererTurbo(this, 128, 21, textureX, textureY); // Box 19
		attachmentModel[33] = new ModelRendererTurbo(this, 110, 27, textureX, textureY); // Box 20
		attachmentModel[34] = new ModelRendererTurbo(this, 128, 15, textureX, textureY); // Box 21
		attachmentModel[35] = new ModelRendererTurbo(this, 180, 18, textureX, textureY); // Box 28
		attachmentModel[36] = new ModelRendererTurbo(this, 197, 18, textureX, textureY); // Box 31
		attachmentModel[37] = new ModelRendererTurbo(this, 197, 18, textureX, textureY); // Box 34
		attachmentModel[38] = new ModelRendererTurbo(this, 180, 18, textureX, textureY); // Box 35
		attachmentModel[39] = new ModelRendererTurbo(this, 180, 21, textureX, textureY); // Box 36
		attachmentModel[40] = new ModelRendererTurbo(this, 180, 21, textureX, textureY); // Box 37
		attachmentModel[41] = new ModelRendererTurbo(this, 180, 17, textureX, textureY); // Box 38
		attachmentModel[42] = new ModelRendererTurbo(this, 180, 17, textureX, textureY); // Box 39
		attachmentModel[43] = new ModelRendererTurbo(this, 180, 18, textureX, textureY); // Box 40
		attachmentModel[44] = new ModelRendererTurbo(this, 197, 18, textureX, textureY); // Box 41
		attachmentModel[45] = new ModelRendererTurbo(this, 197, 18, textureX, textureY); // Box 42
		attachmentModel[46] = new ModelRendererTurbo(this, 180, 18, textureX, textureY); // Box 43
		attachmentModel[47] = new ModelRendererTurbo(this, 107, 71, textureX, textureY); // Box 44
		attachmentModel[48] = new ModelRendererTurbo(this, 107, 71, textureX, textureY); // Box 45
		attachmentModel[49] = new ModelRendererTurbo(this, 107, 68, textureX, textureY); // Box 46

		attachmentModel[0].addBox(0F, 0F, 0F, 20, 3, 9, 0F); // mounter
		attachmentModel[0].setRotationPoint(-10F, -3F, -4.5F);

		attachmentModel[1].addBox(0F, 0F, 0F, 14, 1, 7, 0F); // mounterBase2
		attachmentModel[1].setRotationPoint(-7F, -4F, -3.5F);

		attachmentModel[2].addShapeBox(0F, 0F, 0F, 5, 1, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope4
		attachmentModel[2].setRotationPoint(-17F, -15F, -3.5F);

		attachmentModel[3].addShapeBox(0F, 0F, 0F, 5, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F); // scope3
		attachmentModel[3].setRotationPoint(-17F, -14F, -3.5F);

		attachmentModel[4].addShapeBox(0F, 0F, 0F, 5, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F); // scope3
		attachmentModel[4].setRotationPoint(-17F, -10F, 4.5F);

		attachmentModel[5].addShapeBox(0F, 0F, 0F, 5, 4, 1, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope3
		attachmentModel[5].setRotationPoint(-17F, -14F, 4.5F);

		attachmentModel[6].addShapeBox(0F, 0F, 0F, 5, 4, 1, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope3
		attachmentModel[6].setRotationPoint(-17F, -10F, -3.5F);

		attachmentModel[7].addShapeBox(0F, 0F, 0F, 1, 2, 4, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // scope1
		attachmentModel[7].setRotationPoint(-6F, -18F, -2F);

		attachmentModel[8].addBox(0F, 0F, 0F, 2, 2, 4, 0F); // scope2
		attachmentModel[8].setRotationPoint(-5F, -18F, -2F);

		attachmentModel[9].addShapeBox(0F, 0F, 0F, 5, 1, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 13
		attachmentModel[9].setRotationPoint(-17F, -6F, -3.5F);

		attachmentModel[10].addShapeBox(0F, 0F, 0F, 17, 2, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 16
		attachmentModel[10].setRotationPoint(-14F, -17F, -8.5F);

		attachmentModel[11].addShapeBox(0F, 0F, 0F, 17, 2, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 17
		attachmentModel[11].setRotationPoint(-14F, -15F, -8.5F);

		attachmentModel[12].addShapeBox(0F, 0F, 0F, 4, 2, 2, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 18
		attachmentModel[12].setRotationPoint(-6F, -12F, -7.5F);

		attachmentModel[13].addShapeBox(0F, 0F, 0F, 4, 2, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 19
		attachmentModel[13].setRotationPoint(-6F, -10F, -7.5F);

		attachmentModel[14].addShapeBox(0F, 0F, 0F, 16, 4, 1, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 1
		attachmentModel[14].setRotationPoint(-12F, -14F, 4.5F);

		attachmentModel[15].addShapeBox(0F, 0F, 0F, 16, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 2
		attachmentModel[15].setRotationPoint(-12F, -10F, 4.5F);

		attachmentModel[16].addShapeBox(0F, 0F, 0F, 16, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 3
		attachmentModel[16].setRotationPoint(-12F, -14F, -5.5F);

		attachmentModel[17].addShapeBox(0F, 0F, 0F, 16, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F); // Box 4
		attachmentModel[17].setRotationPoint(-12F, -10F, -5.5F);

		attachmentModel[18].addShapeBox(0F, 0F, 0F, 16, 2, 11, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 5
		attachmentModel[18].setRotationPoint(-12F, -16F, -5.5F);

		attachmentModel[19].addShapeBox(0F, 0F, 0F, 16, 2, 11, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 6
		attachmentModel[19].setRotationPoint(-12F, -6F, -5.5F);

		attachmentModel[20].addShapeBox(0F, 0F, 0F, 1, 2, 4, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 7
		attachmentModel[20].setRotationPoint(-3F, -18F, -2F);

		attachmentModel[21].addBox(0F, 0F, 0F, 8, 7, 3, 0F); // Box 8
		attachmentModel[21].setRotationPoint(-12F, -13.5F, 5.5F);

		attachmentModel[22].addShapeBox(0F, 0F, 0F, 8, 7, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F); // Box 9
		attachmentModel[22].setRotationPoint(-4F, -13.5F, 5.5F);

		attachmentModel[23].addShapeBox(0F, 0F, 0F, 7, 4, 1, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 10
		attachmentModel[23].setRotationPoint(4F, -14F, 4.5F);

		attachmentModel[24].addShapeBox(0F, 0F, 0F, 7, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 11
		attachmentModel[24].setRotationPoint(4F, -14F, -3.5F);

		attachmentModel[25].addShapeBox(0F, 0F, 0F, 7, 1, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 12
		attachmentModel[25].setRotationPoint(4F, -15F, -3.5F);

		attachmentModel[26].addShapeBox(0F, 0F, 0F, 7, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 13
		attachmentModel[26].setRotationPoint(4F, -10F, 4.5F);

		attachmentModel[27].addShapeBox(0F, 0F, 0F, 7, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F); // Box 14
		attachmentModel[27].setRotationPoint(4F, -10F, -5.5F);

		attachmentModel[28].addBox(0F, 0F, 0F, 7, 2, 8, 0F); // Box 15
		attachmentModel[28].setRotationPoint(4F, -6F, -4F);

		attachmentModel[29].addShapeBox(0F, 0F, 0F, 7, 1, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 16
		attachmentModel[29].setRotationPoint(11F, -6F, -3.5F);

		attachmentModel[30].addShapeBox(0F, 0F, 0F, 7, 4, 1, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 17
		attachmentModel[30].setRotationPoint(11F, -10F, -3.5F);

		attachmentModel[31].addShapeBox(0F, 0F, 0F, 7, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 18
		attachmentModel[31].setRotationPoint(11F, -10F, 4.5F);

		attachmentModel[32].addShapeBox(0F, 0F, 0F, 7, 4, 1, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 19
		attachmentModel[32].setRotationPoint(11F, -14F, 4.5F);

		attachmentModel[33].addShapeBox(0F, 0F, 0F, 7, 1, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 20
		attachmentModel[33].setRotationPoint(11F, -15F, -3.5F);

		attachmentModel[34].addShapeBox(0F, 0F, 0F, 7, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 21
		attachmentModel[34].setRotationPoint(11F, -14F, -3.5F);

		attachmentModel[35].addShapeBox(0F, 0F, 0F, 1, 1, 7, 0F, 0F, 0F, -1F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, -1F, -0.25F, -0.25F, -1F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.25F, -0.25F, -1F); // Box 28
		attachmentModel[35].setRotationPoint(-16.5F, -16.5F, -3.5F);

		attachmentModel[36].addShapeBox(0F, 0F, 0F, 4, 1, 7, 0F, 0F, 0F, 0F, 0F, 2F, 2F, 0F, 2F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 2F, 0F, -2F, 2F, 0F, 0F, 0F); // Box 31
		attachmentModel[36].setRotationPoint(-15.5F, -17F, -3.5F);

		attachmentModel[37].addShapeBox(0F, 0F, 0F, 4, 1, 7, 0F, 0F, 0F, 2F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 2F); // Box 34
		attachmentModel[37].setRotationPoint(-11.5F, -19F, -3.5F);

		attachmentModel[38].addShapeBox(0F, 0F, 0F, 1, 1, 7, 0F, 0F, 0F, 0F, -0.25F, 0.25F, -1F, -0.25F, 0.25F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -1F, 0F, -0.5F, -1F, 0F, 0F, 0F); // Box 35
		attachmentModel[38].setRotationPoint(-7.5F, -21F, -3.5F);

		attachmentModel[39].addShapeBox(0F, 0F, 0F, 2, 2, 1, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 36
		attachmentModel[39].setRotationPoint(-17F, -17F, -2F);

		attachmentModel[40].addShapeBox(0F, 0F, 0F, 2, 2, 1, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 37
		attachmentModel[40].setRotationPoint(-17F, -17F, 1F);

		attachmentModel[41].addShapeBox(0F, 0F, 0F, 2, 2, 1, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 38
		attachmentModel[41].setRotationPoint(16F, -17F, -2F);

		attachmentModel[42].addShapeBox(0F, 0F, 0F, 2, 2, 1, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 39
		attachmentModel[42].setRotationPoint(16F, -17F, 1F);

		attachmentModel[43].addShapeBox(0F, 0F, 0F, 1, 1, 7, 0F, 0F, 0.5F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0.5F, 0F, 0F, -0.5F, 0F, -0.25F, -0.25F, -1F, -0.25F, -0.25F, -1F, 0F, -0.5F, 0F); // Box 40
		attachmentModel[43].setRotationPoint(16.5F, -16.5F, -3.5F);

		attachmentModel[44].addShapeBox(0F, 0F, 0F, 4, 1, 7, 0F, 0F, 2F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 2F, 0F, -2F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 2F); // Box 41
		attachmentModel[44].setRotationPoint(12.5F, -17F, -3.5F);

		attachmentModel[45].addShapeBox(0F, 0F, 0F, 4, 1, 7, 0F, 0F, 2F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, -2F, 0F); // Box 42
		attachmentModel[45].setRotationPoint(8.5F, -19F, -3.5F);

		attachmentModel[46].addShapeBox(0F, 0F, 0F, 1, 1, 7, 0F, -0.25F, 0.25F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, -0.25F, 0.25F, -1F, 0F, -0.5F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -1F); // Box 43
		attachmentModel[46].setRotationPoint(7.5F, -21F, -3.5F);

		attachmentModel[47].addBox(0F, 0F, 0F, 1, 2, 2, 0F); // Box 44
		attachmentModel[47].setRotationPoint(-12.5F, -13F, 6F);

		attachmentModel[48].addBox(0F, 0F, 0F, 1, 2, 2, 0F); // Box 45
		attachmentModel[48].setRotationPoint(-12.5F, -9F, 6F);

		attachmentModel[49].addBox(0F, 0F, 0F, 1, 1, 1, 0F); // Box 46
		attachmentModel[49].setRotationPoint(11.5F, -10.5F, -0.5F);

		renderOffset = 0F;

		flipAll();
	}
}