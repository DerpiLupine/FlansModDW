package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.ModelAttachment;
import com.flansmod.client.tmt.ModelRendererTurbo;

import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelSMCSolidStock extends ModelAttachment
{
	int textureX = 512;
	int textureY = 256;

	public ModelSMCSolidStock()
	{
		attachmentModel = new ModelRendererTurbo[12];
		attachmentModel[0] = new ModelRendererTurbo(this, 59, 34, textureX, textureY); // Box 54
		attachmentModel[1] = new ModelRendererTurbo(this, 105, 59, textureX, textureY); // Box 55
		attachmentModel[2] = new ModelRendererTurbo(this, 105, 41, textureX, textureY); // Box 67
		attachmentModel[3] = new ModelRendererTurbo(this, 1, 31, textureX, textureY); // Box 68
		attachmentModel[4] = new ModelRendererTurbo(this, 28, 60, textureX, textureY); // Box 69
		attachmentModel[5] = new ModelRendererTurbo(this, 28, 47, textureX, textureY); // Box 70
		attachmentModel[6] = new ModelRendererTurbo(this, 59, 34, textureX, textureY); // Box 71
		attachmentModel[7] = new ModelRendererTurbo(this, 30, 32, textureX, textureY); // Box 72
		attachmentModel[8] = new ModelRendererTurbo(this, 28, 47, textureX, textureY); // Box 74
		attachmentModel[9] = new ModelRendererTurbo(this, 84, 34, textureX, textureY); // Box 75
		attachmentModel[10] = new ModelRendererTurbo(this, 1, 51, textureX, textureY); // Box 76
		attachmentModel[11] = new ModelRendererTurbo(this, 84, 34, textureX, textureY); // Box 77

		attachmentModel[0].addShapeBox(-4F, -7F, -4F, 4, 2, 8, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 54
		attachmentModel[0].setRotationPoint(0F, 0F, 0F);

		attachmentModel[1].addBox(-4F, -5F, -4F, 4, 10, 8, 0F); // Box 55
		attachmentModel[1].setRotationPoint(0F, 0F, 0F);

		attachmentModel[2].addShapeBox(-8F, -2F, -4F, 4, 7, 8, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F); // Box 67
		attachmentModel[2].setRotationPoint(0F, 0F, 0F);

		attachmentModel[3].addShapeBox(-8F, -7F, -4F, 4, 5, 8, 0F, 0F, -3F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, -3F, -2F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F); // Box 68
		attachmentModel[3].setRotationPoint(0F, 0F, 0F);

		attachmentModel[4].addShapeBox(-36F, -2F, -4F, 28, 9, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 9F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 9F, 0F); // Box 69
		attachmentModel[4].setRotationPoint(0F, 0F, 0F);

		attachmentModel[5].addShapeBox(-36F, -4F, -4F, 28, 2, 8, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 70
		attachmentModel[5].setRotationPoint(0F, 0F, 0F);

		attachmentModel[6].addShapeBox(-4F, 5F, -4F, 4, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 71
		attachmentModel[6].setRotationPoint(0F, 0F, 0F);

		attachmentModel[7].addShapeBox(-8F, 5F, -4F, 4, 4, 8, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, -2F, 0F, -2F, -2F, 0F, -2F, -2F, 0F, 0F, -2F); // Box 72
		attachmentModel[7].setRotationPoint(0F, 0F, 0F);

		attachmentModel[8].addShapeBox(-36F, 16F, -4F, 28, 2, 8, 0F, 0F, 0F, 0F, 0F, 9F, 0F, 0F, 9F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, -9F, -2F, 0F, -9F, -2F, 0F, 0F, -2F); // Box 74
		attachmentModel[8].setRotationPoint(0F, 0F, 0F);

		attachmentModel[9].addShapeBox(-41F, -4F, -4F, 5, 2, 8, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 75
		attachmentModel[9].setRotationPoint(0F, 0F, 0F);

		attachmentModel[10].addBox(-41F, -2F, -4F, 5, 18, 8, 0F); // Box 76
		attachmentModel[10].setRotationPoint(0F, 0F, 0F);

		attachmentModel[11].addShapeBox(-41F, 16F, -4F, 5, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 77
		attachmentModel[11].setRotationPoint(0F, 0F, 0F);

		renderOffset = 0F;

		flipAll();
	}
}