package com.flansmod.client.model.dwfirearms; //Path where the model is located

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelAGMPredator extends ModelGun //Same as Filename
{
	int textureX = 512;
	int textureY = 256;

	public ModelAGMPredator() //Same as Filename
	{
		gunModel = new ModelRendererTurbo[75];
		gunModel[0] = new ModelRendererTurbo(this, 1, 28, textureX, textureY); // Box 34
		gunModel[1] = new ModelRendererTurbo(this, 1, 41, textureX, textureY); // Box 35
		gunModel[2] = new ModelRendererTurbo(this, 1, 59, textureX, textureY); // Box 36
		gunModel[3] = new ModelRendererTurbo(this, 1, 72, textureX, textureY); // Box 38
		gunModel[4] = new ModelRendererTurbo(this, 38, 28, textureX, textureY); // Box 39
		gunModel[5] = new ModelRendererTurbo(this, 38, 41, textureX, textureY); // Box 40
		gunModel[6] = new ModelRendererTurbo(this, 38, 59, textureX, textureY); // Box 41
		gunModel[7] = new ModelRendererTurbo(this, 36, 72, textureX, textureY); // Box 42
		gunModel[8] = new ModelRendererTurbo(this, 59, 32, textureX, textureY); // Box 0
		gunModel[9] = new ModelRendererTurbo(this, 268, 27, textureX, textureY); // Box 1
		gunModel[10] = new ModelRendererTurbo(this, 171, 78, textureX, textureY); // Box 2
		gunModel[11] = new ModelRendererTurbo(this, 220, 22, textureX, textureY); // Box 3
		gunModel[12] = new ModelRendererTurbo(this, 144, 74, textureX, textureY); // Box 4
		gunModel[13] = new ModelRendererTurbo(this, 234, 43, textureX, textureY); // Box 5
		gunModel[14] = new ModelRendererTurbo(this, 144, 38, textureX, textureY); // Box 7
		gunModel[15] = new ModelRendererTurbo(this, 144, 1, textureX, textureY); // Box 8
		gunModel[16] = new ModelRendererTurbo(this, 199, 22, textureX, textureY); // Box 9
		gunModel[17] = new ModelRendererTurbo(this, 199, 22, textureX, textureY); // Box 10
		gunModel[18] = new ModelRendererTurbo(this, 199, 22, textureX, textureY); // Box 11
		gunModel[19] = new ModelRendererTurbo(this, 199, 22, textureX, textureY); // Box 12
		gunModel[20] = new ModelRendererTurbo(this, 244, 59, textureX, textureY); // Box 13
		gunModel[21] = new ModelRendererTurbo(this, 144, 22, textureX, textureY); // Box 15
		gunModel[22] = new ModelRendererTurbo(this, 144, 60, textureX, textureY); // Box 16
		gunModel[23] = new ModelRendererTurbo(this, 253, 29, textureX, textureY); // Box 17
		gunModel[24] = new ModelRendererTurbo(this, 22, 17, textureX, textureY); // Box 18
		gunModel[25] = new ModelRendererTurbo(this, 22, 13, textureX, textureY); // Box 19
		gunModel[26] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 21
		gunModel[27] = new ModelRendererTurbo(this, 1, 11, textureX, textureY); // Box 22
		gunModel[28] = new ModelRendererTurbo(this, 118, 1, textureX, textureY); // Box 23
		gunModel[29] = new ModelRendererTurbo(this, 118, 1, textureX, textureY); // Box 24
		gunModel[30] = new ModelRendererTurbo(this, 118, 1, textureX, textureY); // Box 25
		gunModel[31] = new ModelRendererTurbo(this, 118, 1, textureX, textureY); // Box 26
		gunModel[32] = new ModelRendererTurbo(this, 118, 1, textureX, textureY); // Box 27
		gunModel[33] = new ModelRendererTurbo(this, 118, 1, textureX, textureY); // Box 28
		gunModel[34] = new ModelRendererTurbo(this, 118, 1, textureX, textureY); // Box 30
		gunModel[35] = new ModelRendererTurbo(this, 118, 1, textureX, textureY); // Box 31
		gunModel[36] = new ModelRendererTurbo(this, 97, 32, textureX, textureY); // Box 32
		gunModel[37] = new ModelRendererTurbo(this, 88, 32, textureX, textureY); // Box 33
		gunModel[38] = new ModelRendererTurbo(this, 279, 52, textureX, textureY); // Box 38
		gunModel[39] = new ModelRendererTurbo(this, 144, 11, textureX, textureY); // Box 39
		gunModel[40] = new ModelRendererTurbo(this, 118, 1, textureX, textureY); // Box 40
		gunModel[41] = new ModelRendererTurbo(this, 144, 93, textureX, textureY); // Box 41
		gunModel[42] = new ModelRendererTurbo(this, 144, 113, textureX, textureY); // Box 42
		gunModel[43] = new ModelRendererTurbo(this, 144, 113, textureX, textureY); // Box 43
		gunModel[44] = new ModelRendererTurbo(this, 193, 62, textureX, textureY); // Box 44
		gunModel[45] = new ModelRendererTurbo(this, 205, 49, textureX, textureY); // Box 45
		gunModel[46] = new ModelRendererTurbo(this, 54, 21, textureX, textureY); // Box 46
		gunModel[47] = new ModelRendererTurbo(this, 118, 1, textureX, textureY); // Box 47
		gunModel[48] = new ModelRendererTurbo(this, 118, 1, textureX, textureY); // Box 48
		gunModel[49] = new ModelRendererTurbo(this, 118, 1, textureX, textureY); // Box 49
		gunModel[50] = new ModelRendererTurbo(this, 117, 22, textureX, textureY); // Box 50
		gunModel[51] = new ModelRendererTurbo(this, 61, 11, textureX, textureY); // Box 51
		gunModel[52] = new ModelRendererTurbo(this, 118, 11, textureX, textureY); // Box 52
		gunModel[53] = new ModelRendererTurbo(this, 118, 11, textureX, textureY); // Box 53
		gunModel[54] = new ModelRendererTurbo(this, 118, 11, textureX, textureY); // Box 54
		gunModel[55] = new ModelRendererTurbo(this, 118, 11, textureX, textureY); // Box 55
		gunModel[56] = new ModelRendererTurbo(this, 273, 11, textureX, textureY); // Box 56
		gunModel[57] = new ModelRendererTurbo(this, 273, 2, textureX, textureY); // Box 57
		gunModel[58] = new ModelRendererTurbo(this, 273, 2, textureX, textureY); // Box 58
		gunModel[59] = new ModelRendererTurbo(this, 210, 39, textureX, textureY); // Box 62
		gunModel[60] = new ModelRendererTurbo(this, 210, 39, textureX, textureY); // Box 63
		gunModel[61] = new ModelRendererTurbo(this, 191, 38, textureX, textureY); // Box 64
		gunModel[62] = new ModelRendererTurbo(this, 210, 39, textureX, textureY); // Box 65
		gunModel[63] = new ModelRendererTurbo(this, 191, 38, textureX, textureY); // Box 66
		gunModel[64] = new ModelRendererTurbo(this, 210, 39, textureX, textureY); // Box 67
		gunModel[65] = new ModelRendererTurbo(this, 144, 49, textureX, textureY); // Box 78
		gunModel[66] = new ModelRendererTurbo(this, 304, 56, textureX, textureY); // Box 0
		gunModel[67] = new ModelRendererTurbo(this, 304, 65, textureX, textureY); // Box 1
		gunModel[68] = new ModelRendererTurbo(this, 304, 56, textureX, textureY); // Box 2
		gunModel[69] = new ModelRendererTurbo(this, 210, 39, textureX, textureY); // Box 3
		gunModel[70] = new ModelRendererTurbo(this, 191, 38, textureX, textureY); // Box 4
		gunModel[71] = new ModelRendererTurbo(this, 210, 39, textureX, textureY); // Box 5
		gunModel[72] = new ModelRendererTurbo(this, 210, 39, textureX, textureY); // Box 6
		gunModel[73] = new ModelRendererTurbo(this, 191, 38, textureX, textureY); // Box 7
		gunModel[74] = new ModelRendererTurbo(this, 210, 39, textureX, textureY); // Box 8

		gunModel[0].addShapeBox(0F, 0F, 0F, 10, 4, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 34
		gunModel[0].setRotationPoint(-8F, -5.5F, -4F);

		gunModel[1].addShapeBox(0F, 0F, 0F, 10, 9, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 5F, 0F, 0F, -5F, 0F, 0F, -5F, 0F, 0F, 5F, 0F, 0F); // Box 35
		gunModel[1].setRotationPoint(-8F, -1.5F, -4F);

		gunModel[2].addShapeBox(0F, 0F, 0F, 10, 4, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, -3F, 0F); // Box 36
		gunModel[2].setRotationPoint(-13F, 7.5F, -4F);

		gunModel[3].addShapeBox(0F, 0F, 0F, 9, 2, 8, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F); // Box 38
		gunModel[3].setRotationPoint(-13F, 11.5F, -4F);

		gunModel[4].addShapeBox(0F, 0F, 0F, 2, 4, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F); // Box 39
		gunModel[4].setRotationPoint(2F, -5.5F, -4F);

		gunModel[5].addShapeBox(0F, 0F, 0F, 2, 9, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 5F, 0F, 0F, -5F, 0F, -1.5F, -5F, 0F, -1.5F, 5F, 0F, 0F); // Box 40
		gunModel[5].setRotationPoint(2F, -1.5F, -4F);

		gunModel[6].addShapeBox(0F, 0F, 0F, 2, 4, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, -1.5F, -1F, 0F, -1.5F, 1F, 0F, 0F); // Box 41
		gunModel[6].setRotationPoint(-3F, 7.5F, -4F);

		gunModel[7].addShapeBox(0F, 0F, 0F, 2, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F); // Box 42
		gunModel[7].setRotationPoint(-4F, 11.5F, -4F);

		gunModel[8].addShapeBox(0F, 0F, 0F, 10, 2, 8, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 0
		gunModel[8].setRotationPoint(-8F, -7.5F, -4F);

		gunModel[9].addShapeBox(0F, 0F, 0F, 3, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F); // Box 1
		gunModel[9].setRotationPoint(-16F, -9.5F, -4F);

		gunModel[10].addBox(0F, 0F, 0F, 12, 6, 8, 0F); // Box 2
		gunModel[10].setRotationPoint(2F, -11.5F, -4F);

		gunModel[11].addShapeBox(0F, 0F, 0F, 7, 6, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F); // Box 3
		gunModel[11].setRotationPoint(-16F, -17.5F, -4.5F);

		gunModel[12].addBox(0F, 0F, 0F, 4, 7, 8, 0F); // Box 4
		gunModel[12].setRotationPoint(14F, -8.5F, -4F);

		gunModel[13].addShapeBox(0F, 0F, 0F, 5, 7, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F); // Box 5
		gunModel[13].setRotationPoint(31F, -11.5F, -4F);

		gunModel[14].addBox(0F, 0F, 0F, 15, 2, 8, 0F); // Box 7
		gunModel[14].setRotationPoint(-13F, -9.5F, -4F);

		gunModel[15].addShapeBox(0F, 0F, 0F, 56, 1, 8, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 8
		gunModel[15].setRotationPoint(-16F, -20.5F, -4F);

		gunModel[16].addShapeBox(0F, 0F, 0F, 1, 6, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F); // Box 9
		gunModel[16].setRotationPoint(-8F, -17.5F, -4.5F);

		gunModel[17].addShapeBox(0F, 0F, 0F, 1, 6, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F); // Box 10
		gunModel[17].setRotationPoint(-6F, -17.5F, -4.5F);

		gunModel[18].addShapeBox(0F, 0F, 0F, 1, 6, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F); // Box 11
		gunModel[18].setRotationPoint(-4F, -17.5F, -4.5F);

		gunModel[19].addShapeBox(0F, 0F, 0F, 1, 6, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F); // Box 12
		gunModel[19].setRotationPoint(-2F, -17.5F, -4.5F);

		gunModel[20].addShapeBox(0F, 0F, 0F, 9, 6, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F); // Box 13
		gunModel[20].setRotationPoint(-9F, -17.5F, -4F);

		gunModel[21].addShapeBox(0F, 0F, 0F, 18, 6, 9, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 15
		gunModel[21].setRotationPoint(1F, -17.5F, -4.5F);

		gunModel[22].addBox(0F, 0F, 0F, 17, 6, 7, 0F); // Box 16
		gunModel[22].setRotationPoint(19F, -17.5F, -2.5F);

		gunModel[23].addBox(0F, 0F, 0F, 5, 6, 2, 0F); // Box 17
		gunModel[23].setRotationPoint(31F, -17.5F, -4.5F);

		gunModel[24].addBox(0F, 0F, 0F, 12, 1, 2, 0F); // Box 18
		gunModel[24].setRotationPoint(19F, -17.5F, -4.5F);

		gunModel[25].addBox(0F, 0F, 0F, 12, 1, 2, 0F); // Box 19
		gunModel[25].setRotationPoint(19F, -12.5F, -4.5F);

		gunModel[26].addBox(0F, 0F, 0F, 51, 2, 7, 0F); // Box 21
		gunModel[26].setRotationPoint(-12F, -22.5F, -3.5F);

		gunModel[27].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 22
		gunModel[27].setRotationPoint(-15F, -22.5F, -3.5F);

		gunModel[28].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 23
		gunModel[28].setRotationPoint(-12F, -24.5F, -3.5F);

		gunModel[29].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 24
		gunModel[29].setRotationPoint(-6F, -24.5F, -3.5F);

		gunModel[30].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 25
		gunModel[30].setRotationPoint(0F, -24.5F, -3.5F);

		gunModel[31].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 26
		gunModel[31].setRotationPoint(18F, -24.5F, -3.5F);

		gunModel[32].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 27
		gunModel[32].setRotationPoint(12F, -24.5F, -3.5F);

		gunModel[33].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 28
		gunModel[33].setRotationPoint(6F, -24.5F, -3.5F);

		gunModel[34].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 30
		gunModel[34].setRotationPoint(30F, -24.5F, -3.5F);

		gunModel[35].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 31
		gunModel[35].setRotationPoint(24F, -24.5F, -3.5F);

		gunModel[36].addShapeBox(0F, 0F, 0F, 3, 6, 1, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 32
		gunModel[36].setRotationPoint(16F, -17.5F, -5.5F);

		gunModel[37].addShapeBox(0F, 0F, 0F, 3, 6, 1, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 33
		gunModel[37].setRotationPoint(31F, -17.5F, -5.5F);

		gunModel[38].addBox(0F, 0F, 0F, 4, 13, 8, 0F); // Box 38
		gunModel[38].setRotationPoint(36F, -19.5F, -4F);

		gunModel[39].addBox(0F, 0F, 0F, 52, 2, 8, 0F); // Box 39
		gunModel[39].setRotationPoint(-16F, -19.5F, -4F);

		gunModel[40].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 40
		gunModel[40].setRotationPoint(36F, -24.5F, -3.5F);

		gunModel[41].addBox(0F, 0F, 0F, 30, 10, 9, 0F); // Box 41
		gunModel[41].setRotationPoint(40F, -19F, -4.5F);

		gunModel[42].addShapeBox(0F, 0F, 0F, 30, 2, 9, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 42
		gunModel[42].setRotationPoint(40F, -21F, -4.5F);

		gunModel[43].addShapeBox(0F, 0F, 0F, 30, 2, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 43
		gunModel[43].setRotationPoint(40F, -9F, -4.5F);

		gunModel[44].addBox(0F, 0F, 0F, 17, 3, 8, 0F); // Box 44
		gunModel[44].setRotationPoint(14F, -11.5F, -4F);

		gunModel[45].addShapeBox(0F, 0F, 0F, 5, 3, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F); // Box 45
		gunModel[45].setRotationPoint(40F, -9F, -4.5F);

		gunModel[46].addBox(0F, 0F, 0F, 24, 3, 7, 0F); // Box 46
		gunModel[46].setRotationPoint(42F, -22.5F, -3.5F);

		gunModel[47].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 47
		gunModel[47].setRotationPoint(63F, -24.5F, -3.5F);

		gunModel[48].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 48
		gunModel[48].setRotationPoint(57F, -24.5F, -3.5F);

		gunModel[49].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 49
		gunModel[49].setRotationPoint(51F, -24.5F, -3.5F);

		gunModel[50].addShapeBox(0F, 0F, 0F, 6, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 50
		gunModel[50].setRotationPoint(42F, -24.5F, -3.5F);

		gunModel[51].addBox(0F, 0F, 0F, 21, 2, 7, 0F); // Box 51
		gunModel[51].setRotationPoint(45F, -8.5F, -3.5F);

		gunModel[52].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 52
		gunModel[52].setRotationPoint(63F, -6.5F, -3.5F);

		gunModel[53].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 53
		gunModel[53].setRotationPoint(57F, -6.5F, -3.5F);

		gunModel[54].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 54
		gunModel[54].setRotationPoint(51F, -6.5F, -3.5F);

		gunModel[55].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 55
		gunModel[55].setRotationPoint(45F, -6.5F, -3.5F);

		gunModel[56].addBox(0F, 0F, 0F, 20, 2, 6, 0F); // Box 56
		gunModel[56].setRotationPoint(70F, -12F, -3F);

		gunModel[57].addShapeBox(0F, 0F, 0F, 20, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 57
		gunModel[57].setRotationPoint(70F, -14F, -3F);

		gunModel[58].addShapeBox(0F, 0F, 0F, 20, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 58
		gunModel[58].setRotationPoint(70F, -10F, -3F);

		gunModel[59].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 62
		gunModel[59].setRotationPoint(70F, -14.5F, -3.5F);

		gunModel[60].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 63
		gunModel[60].setRotationPoint(70F, -9.5F, -3.5F);

		gunModel[61].addBox(0F, 0F, 0F, 2, 3, 7, 0F); // Box 64
		gunModel[61].setRotationPoint(70F, -12.5F, -3.5F);

		gunModel[62].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 65
		gunModel[62].setRotationPoint(73F, -14.5F, -3.5F);

		gunModel[63].addBox(0F, 0F, 0F, 2, 3, 7, 0F); // Box 66
		gunModel[63].setRotationPoint(73F, -12.5F, -3.5F);

		gunModel[64].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 67
		gunModel[64].setRotationPoint(73F, -9.5F, -3.5F);

		gunModel[65].addBox(0F, 0F, 0F, 18, 2, 8, 0F); // Box 78
		gunModel[65].setRotationPoint(-16F, -11.5F, -4F);

		gunModel[66].addShapeBox(0F, 0F, 0F, 15, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 0
		gunModel[66].setRotationPoint(70F, -21F, -3F);

		gunModel[67].addBox(0F, 0F, 0F, 15, 2, 6, 0F); // Box 1
		gunModel[67].setRotationPoint(70F, -19F, -3F);

		gunModel[68].addShapeBox(0F, 0F, 0F, 15, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 2
		gunModel[68].setRotationPoint(70F, -17F, -3F);

		gunModel[69].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 3
		gunModel[69].setRotationPoint(70F, -21.5F, -3.5F);

		gunModel[70].addBox(0F, 0F, 0F, 2, 3, 7, 0F); // Box 4
		gunModel[70].setRotationPoint(70F, -19.5F, -3.5F);

		gunModel[71].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 5
		gunModel[71].setRotationPoint(70F, -16.5F, -3.5F);

		gunModel[72].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 6
		gunModel[72].setRotationPoint(82F, -16.5F, -3.5F);

		gunModel[73].addBox(0F, 0F, 0F, 2, 3, 7, 0F); // Box 7
		gunModel[73].setRotationPoint(82F, -19.5F, -3.5F);

		gunModel[74].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 8
		gunModel[74].setRotationPoint(82F, -21.5F, -3.5F);


		defaultBarrelModel = new ModelRendererTurbo[9];
		defaultBarrelModel[0] = new ModelRendererTurbo(this, 326, 1, textureX, textureY); // Box 68
		defaultBarrelModel[1] = new ModelRendererTurbo(this, 326, 21, textureX, textureY); // Box 69
		defaultBarrelModel[2] = new ModelRendererTurbo(this, 326, 11, textureX, textureY); // Box 70
		defaultBarrelModel[3] = new ModelRendererTurbo(this, 345, 26, textureX, textureY); // Box 71
		defaultBarrelModel[4] = new ModelRendererTurbo(this, 354, 26, textureX, textureY); // Box 72
		defaultBarrelModel[5] = new ModelRendererTurbo(this, 354, 26, textureX, textureY); // Box 73
		defaultBarrelModel[6] = new ModelRendererTurbo(this, 345, 26, textureX, textureY); // Box 74
		defaultBarrelModel[7] = new ModelRendererTurbo(this, 345, 26, textureX, textureY); // Box 75
		defaultBarrelModel[8] = new ModelRendererTurbo(this, 354, 26, textureX, textureY); // Box 76

		defaultBarrelModel[0].addShapeBox(0F, 0F, 0F, 14, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 68
		defaultBarrelModel[0].setRotationPoint(90F, -14.5F, -3.5F);

		defaultBarrelModel[1].addBox(0F, 0F, 0F, 2, 3, 7, 0F); // Box 69
		defaultBarrelModel[1].setRotationPoint(90F, -12.5F, -3.5F);

		defaultBarrelModel[2].addShapeBox(0F, 0F, 0F, 14, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 70
		defaultBarrelModel[2].setRotationPoint(90F, -9.5F, -3.5F);

		defaultBarrelModel[3].addBox(0F, 0F, 0F, 2, 3, 2, 0F); // Box 71
		defaultBarrelModel[3].setRotationPoint(94F, -12.5F, -3.5F);

		defaultBarrelModel[4].addBox(0F, 0F, 0F, 2, 3, 2, 0F); // Box 72
		defaultBarrelModel[4].setRotationPoint(94F, -12.5F, 1.5F);

		defaultBarrelModel[5].addBox(0F, 0F, 0F, 2, 3, 2, 0F); // Box 73
		defaultBarrelModel[5].setRotationPoint(102F, -12.5F, 1.5F);

		defaultBarrelModel[6].addBox(0F, 0F, 0F, 2, 3, 2, 0F); // Box 74
		defaultBarrelModel[6].setRotationPoint(102F, -12.5F, -3.5F);

		defaultBarrelModel[7].addBox(0F, 0F, 0F, 2, 3, 2, 0F); // Box 75
		defaultBarrelModel[7].setRotationPoint(98F, -12.5F, -3.5F);

		defaultBarrelModel[8].addBox(0F, 0F, 0F, 2, 3, 2, 0F); // Box 76
		defaultBarrelModel[8].setRotationPoint(98F, -12.5F, 1.5F);


		defaultScopeModel = new ModelRendererTurbo[21];
		defaultScopeModel[0] = new ModelRendererTurbo(this, 109, 44, textureX, textureY); // Box 98
		defaultScopeModel[1] = new ModelRendererTurbo(this, 109, 32, textureX, textureY); // Box 99
		defaultScopeModel[2] = new ModelRendererTurbo(this, 144, 125, textureX, textureY); // Box 100
		defaultScopeModel[3] = new ModelRendererTurbo(this, 144, 136, textureX, textureY); // Box 101
		defaultScopeModel[4] = new ModelRendererTurbo(this, 130, 83, textureX, textureY); // Box 102
		defaultScopeModel[5] = new ModelRendererTurbo(this, 130, 77, textureX, textureY); // Box 103
		defaultScopeModel[6] = new ModelRendererTurbo(this, 130, 92, textureX, textureY); // Box 104
		defaultScopeModel[7] = new ModelRendererTurbo(this, 130, 89, textureX, textureY); // Box 105
		defaultScopeModel[8] = new ModelRendererTurbo(this, 109, 93, textureX, textureY); // Box 106
		defaultScopeModel[9] = new ModelRendererTurbo(this, 130, 73, textureX, textureY); // Box 107
		defaultScopeModel[10] = new ModelRendererTurbo(this, 109, 55, textureX, textureY); // Box 108
		defaultScopeModel[11] = new ModelRendererTurbo(this, 130, 55, textureX, textureY); // Box 110
		defaultScopeModel[12] = new ModelRendererTurbo(this, 130, 70, textureX, textureY); // Box 111
		defaultScopeModel[13] = new ModelRendererTurbo(this, 130, 67, textureX, textureY); // Box 112
		defaultScopeModel[14] = new ModelRendererTurbo(this, 130, 61, textureX, textureY); // Box 113
		defaultScopeModel[15] = new ModelRendererTurbo(this, 124, 85, textureX, textureY); // Box 115
		defaultScopeModel[16] = new ModelRendererTurbo(this, 116, 81, textureX, textureY); // Box 116
		defaultScopeModel[17] = new ModelRendererTurbo(this, 109, 81, textureX, textureY); // Box 117
		defaultScopeModel[18] = new ModelRendererTurbo(this, 119, 85, textureX, textureY); // Box 118
		defaultScopeModel[19] = new ModelRendererTurbo(this, 109, 85, textureX, textureY); // Box 119
		defaultScopeModel[20] = new ModelRendererTurbo(this, 114, 85, textureX, textureY); // Box 120

		defaultScopeModel[0].addShapeBox(0F, 0F, 0F, 9, 2, 8, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 98
		defaultScopeModel[0].setRotationPoint(-9F, -25.5F, -4F);

		defaultScopeModel[1].addBox(0F, 0F, 0F, 9, 3, 8, 0F); // Box 99
		defaultScopeModel[1].setRotationPoint(-9F, -23.5F, -4F);

		defaultScopeModel[2].addShapeBox(0F, 0F, 0F, 5, 2, 8, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 100
		defaultScopeModel[2].setRotationPoint(59F, -25.5F, -4F);

		defaultScopeModel[3].addBox(0F, 0F, 0F, 5, 3, 8, 0F); // Box 101
		defaultScopeModel[3].setRotationPoint(59F, -23.5F, -4F);

		defaultScopeModel[4].addBox(0F, 0F, 0F, 5, 4, 1, 0F); // Box 102
		defaultScopeModel[4].setRotationPoint(59F, -32.5F, -3F);

		defaultScopeModel[5].addBox(0F, 0F, 0F, 5, 4, 1, 0F); // Box 103
		defaultScopeModel[5].setRotationPoint(59F, -32.5F, 2F);

		defaultScopeModel[6].addShapeBox(0F, 0F, 0F, 5, 1, 1, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 104
		defaultScopeModel[6].setRotationPoint(59F, -33.5F, -3F);

		defaultScopeModel[7].addShapeBox(0F, 0F, 0F, 5, 1, 1, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 105
		defaultScopeModel[7].setRotationPoint(59F, -33.5F, 2F);

		defaultScopeModel[8].addBox(0F, 0F, 0F, 5, 3, 6, 0F); // Box 106
		defaultScopeModel[8].setRotationPoint(59F, -28.5F, -3F);

		defaultScopeModel[9].addBox(0F, 0F, 0F, 3, 2, 1, 0F); // Box 107
		defaultScopeModel[9].setRotationPoint(60F, -30.5F, -0.5F);

		defaultScopeModel[10].addShapeBox(0F, 0F, 0F, 4, 3, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F); // Box 108
		defaultScopeModel[10].setRotationPoint(-8F, -28.5F, -3F);

		defaultScopeModel[11].addBox(0F, 0F, 0F, 4, 4, 1, 0F); // Box 110
		defaultScopeModel[11].setRotationPoint(-8F, -32.5F, 2F);

		defaultScopeModel[12].addShapeBox(0F, 0F, 0F, 4, 1, 1, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 111
		defaultScopeModel[12].setRotationPoint(-8F, -33.5F, 2F);

		defaultScopeModel[13].addShapeBox(0F, 0F, 0F, 4, 1, 1, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 112
		defaultScopeModel[13].setRotationPoint(-8F, -33.5F, -3F);

		defaultScopeModel[14].addBox(0F, 0F, 0F, 4, 4, 1, 0F); // Box 113
		defaultScopeModel[14].setRotationPoint(-8F, -32.5F, -3F);

		defaultScopeModel[15].addShapeBox(0F, 0F, 0F, 1, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F, 1F); // Box 115
		defaultScopeModel[15].setRotationPoint(-7F, -30.5F, -2F);

		defaultScopeModel[16].addBox(0F, 0F, 0F, 1, 1, 2, 0F); // Box 116
		defaultScopeModel[16].setRotationPoint(-7F, -32.5F, -1F);

		defaultScopeModel[17].addBox(0F, 0F, 0F, 1, 1, 2, 0F); // Box 117
		defaultScopeModel[17].setRotationPoint(-7F, -29.5F, -1F);

		defaultScopeModel[18].addShapeBox(0F, 0F, 0F, 1, 2, 1, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 118
		defaultScopeModel[18].setRotationPoint(-7F, -32.5F, -2F);

		defaultScopeModel[19].addShapeBox(0F, 0F, 0F, 1, 2, 1, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 119
		defaultScopeModel[19].setRotationPoint(-7F, -32.5F, 1F);

		defaultScopeModel[20].addShapeBox(0F, 0F, 0F, 1, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 120
		defaultScopeModel[20].setRotationPoint(-7F, -30.5F, 1F);


		defaultStockModel = new ModelRendererTurbo[10];
		defaultStockModel[0] = new ModelRendererTurbo(this, 1, 137, textureX, textureY); // Box 82
		defaultStockModel[1] = new ModelRendererTurbo(this, 1, 92, textureX, textureY); // Box 83
		defaultStockModel[2] = new ModelRendererTurbo(this, 1, 83, textureX, textureY); // Box 84
		defaultStockModel[3] = new ModelRendererTurbo(this, 39, 129, textureX, textureY); // Box 85
		defaultStockModel[4] = new ModelRendererTurbo(this, 39, 138, textureX, textureY); // Box 86
		defaultStockModel[5] = new ModelRendererTurbo(this, 39, 116, textureX, textureY); // Box 87
		defaultStockModel[6] = new ModelRendererTurbo(this, 1, 116, textureX, textureY); // Box 88
		defaultStockModel[7] = new ModelRendererTurbo(this, 19, 137, textureX, textureY); // Box 89
		defaultStockModel[8] = new ModelRendererTurbo(this, 26, 115, textureX, textureY); // Box 90
		defaultStockModel[9] = new ModelRendererTurbo(this, 1, 101, textureX, textureY); // Box 95

		defaultStockModel[0].addShapeBox(0F, 0F, 0F, 5, 7, 7, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F); // Box 82
		defaultStockModel[0].setRotationPoint(-21F, -16.5F, -3.5F);

		defaultStockModel[1].addShapeBox(0F, 0F, 0F, 25, 1, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 10F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 10F, 0F); // Box 83
		defaultStockModel[1].setRotationPoint(-46F, -9.5F, -3.5F);

		defaultStockModel[2].addShapeBox(0F, 0F, 0F, 25, 1, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 84
		defaultStockModel[2].setRotationPoint(-46F, -17.5F, -3.5F);

		defaultStockModel[3].addShapeBox(0F, 0F, 0F, 5, 1, 7, 0F, 0F, -2F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, -2F, -1F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F); // Box 85
		defaultStockModel[3].setRotationPoint(-21F, -19.5F, -3.5F);

		defaultStockModel[4].addShapeBox(0F, 0F, 0F, 6, 1, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 86
		defaultStockModel[4].setRotationPoint(-53F, -17.5F, -3.5F);

		defaultStockModel[5].addShapeBox(0F, 0F, 0F, 6, 5, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F); // Box 87
		defaultStockModel[5].setRotationPoint(-53F, -16.5F, -3.5F);

		defaultStockModel[6].addBox(0F, 0F, 0F, 5, 13, 7, 0F); // Box 88
		defaultStockModel[6].setRotationPoint(-52F, -11.5F, -3.5F);

		defaultStockModel[7].addShapeBox(0F, 0F, 0F, 1, 1, 5, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 89
		defaultStockModel[7].setRotationPoint(-47F, -16.5F, -2.5F);

		defaultStockModel[8].addBox(0F, 0F, 0F, 1, 16, 5, 0F); // Box 90
		defaultStockModel[8].setRotationPoint(-47F, -15.5F, -2.5F);

		defaultStockModel[9].addBox(0F, 0F, 0F, 25, 7, 7, 0F); // Box 95
		defaultStockModel[9].setRotationPoint(-46F, -16.5F, -3.5F);


		ammoModel = new ModelRendererTurbo[13];
		ammoModel[0] = new ModelRendererTurbo(this, 66, 145, textureX, textureY); // Box 6
		ammoModel[1] = new ModelRendererTurbo(this, 66, 43, textureX, textureY); // Box 79
		ammoModel[2] = new ModelRendererTurbo(this, 66, 93, textureX, textureY); // Box 81
		ammoModel[3] = new ModelRendererTurbo(this, 66, 50, textureX, textureY); // Box 83
		ammoModel[4] = new ModelRendererTurbo(this, 66, 113, textureX, textureY); // Box 84
		ammoModel[5] = new ModelRendererTurbo(this, 66, 73, textureX, textureY); // Box 88
		ammoModel[6] = new ModelRendererTurbo(this, 66, 128, textureX, textureY); // Box 89
		ammoModel[7] = new ModelRendererTurbo(this, 66, 50, textureX, textureY); // Box 90
		ammoModel[8] = new ModelRendererTurbo(this, 66, 93, textureX, textureY); // Box 91
		ammoModel[9] = new ModelRendererTurbo(this, 66, 113, textureX, textureY); // Box 92
		ammoModel[10] = new ModelRendererTurbo(this, 66, 73, textureX, textureY); // Box 93
		ammoModel[11] = new ModelRendererTurbo(this, 66, 128, textureX, textureY); // Box 94
		ammoModel[12] = new ModelRendererTurbo(this, 107, 145, textureX, textureY); // Box 96

		ammoModel[0].addBox(0F, 0F, -3.5F, 13, 20, 7, 0F); // Box 6
		ammoModel[0].setRotationPoint(18F, -9.5F, 0F);
		ammoModel[0].rotateAngleZ = 0.06981317F;

		ammoModel[1].addShapeBox(1F, -1F, -2.5F, 11, 1, 5, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 79
		ammoModel[1].setRotationPoint(18F, -9.5F, 0F);
		ammoModel[1].rotateAngleZ = 0.06981317F;

		ammoModel[2].addShapeBox(0F, 6F, -19.5F, 13, 3, 16, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 81
		ammoModel[2].setRotationPoint(18F, -9.5F, 0F);
		ammoModel[2].rotateAngleZ = 0.06981317F;

		ammoModel[3].addBox(0F, 9F, -19.5F, 13, 6, 16, 0F); // Box 83
		ammoModel[3].setRotationPoint(18F, -9.5F, 0F);
		ammoModel[3].rotateAngleZ = 0.06981317F;

		ammoModel[4].addShapeBox(0F, 4F, -17.5F, 13, 2, 12, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 84
		ammoModel[4].setRotationPoint(18F, -9.5F, 0F);
		ammoModel[4].rotateAngleZ = 0.06981317F;

		ammoModel[5].addShapeBox(0F, 15F, -19.5F, 13, 3, 16, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 88
		ammoModel[5].setRotationPoint(18F, -9.5F, 0F);
		ammoModel[5].rotateAngleZ = 0.06981317F;

		ammoModel[6].addShapeBox(0F, 18F, -17.5F, 13, 2, 14, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 89
		ammoModel[6].setRotationPoint(18F, -9.5F, 0F);
		ammoModel[6].rotateAngleZ = 0.06981317F;

		ammoModel[7].addBox(0F, 9F, 3.5F, 13, 6, 16, 0F); // Box 90
		ammoModel[7].setRotationPoint(18F, -9.5F, 0F);
		ammoModel[7].rotateAngleZ = 0.06981317F;

		ammoModel[8].addShapeBox(0F, 6F, 3.5F, 13, 3, 16, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 91
		ammoModel[8].setRotationPoint(18F, -9.5F, 0F);
		ammoModel[8].rotateAngleZ = 0.06981317F;

		ammoModel[9].addShapeBox(0F, 4F, 5.5F, 13, 2, 12, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 92
		ammoModel[9].setRotationPoint(18F, -9.5F, 0F);
		ammoModel[9].rotateAngleZ = 0.06981317F;

		ammoModel[10].addShapeBox(0F, 15F, 3.5F, 13, 3, 16, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 93
		ammoModel[10].setRotationPoint(18F, -9.5F, 0F);
		ammoModel[10].rotateAngleZ = 0.06981317F;

		ammoModel[11].addShapeBox(0F, 18F, 3.5F, 13, 2, 14, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 94
		ammoModel[11].setRotationPoint(18F, -9.5F, 0F);
		ammoModel[11].rotateAngleZ = 0.06981317F;

		ammoModel[12].addShapeBox(13F, 14F, -3.5F, 1, 6, 7, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 96
		ammoModel[12].setRotationPoint(18F, -9.5F, 0F);
		ammoModel[12].rotateAngleZ = 0.06981317F;


		slideModel = new ModelRendererTurbo[3];
		slideModel[0] = new ModelRendererTurbo(this, 1, 21, textureX, textureY); // Box 20
		slideModel[1] = new ModelRendererTurbo(this, 61, 53, textureX, textureY); // Box 37
		slideModel[2] = new ModelRendererTurbo(this, 61, 53, textureX, textureY); // Box 97

		slideModel[0].addShapeBox(0F, 0F, 0F, 12, 4, 1, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 20
		slideModel[0].setRotationPoint(19F, -16.5F, -4F);

		slideModel[1].addShapeBox(0F, 0F, 0F, 2, 4, 8, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F); // Box 37
		slideModel[1].setRotationPoint(28F, -16.5F, -11F);

		slideModel[2].addShapeBox(0F, 0F, 0F, 2, 4, 8, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F); // Box 97
		slideModel[2].setRotationPoint(30F, -16.5F, -11F);

		barrelAttachPoint = new Vector3f(90F /16F, 11F /16F, 0F /16F);
		stockAttachPoint = new Vector3f(-16F /16F, 15F /16F, 0F /16F);
		scopeAttachPoint = new Vector3f(23F /16F, 22F /16F, 0F /16F);
		gripAttachPoint = new Vector3f(55 /16F, 8F /16F, 0F /16F);


		gunSlideDistance = 0.8F;
		animationType = EnumAnimationType.BOTTOM_CLIP;

		translateAll(0F, 0F, 0F);


		flipAll();
	}
}