package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.ModelAttachment;
import com.flansmod.client.tmt.ModelRendererTurbo;

import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelABTIronPipeStock extends ModelAttachment
{
	int textureX = 512;
	int textureY = 256;

	public ModelABTIronPipeStock()
	{
		attachmentModel = new ModelRendererTurbo[14];
		attachmentModel[0] = new ModelRendererTurbo(this, 26, 1, textureX, textureY); // Box 0
		attachmentModel[1] = new ModelRendererTurbo(this, 26, 10, textureX, textureY); // Box 1
		attachmentModel[2] = new ModelRendererTurbo(this, 26, 1, textureX, textureY); // Box 2
		attachmentModel[3] = new ModelRendererTurbo(this, 30, 26, textureX, textureY); // Box 3
		attachmentModel[4] = new ModelRendererTurbo(this, 30, 19, textureX, textureY); // Box 4
		attachmentModel[5] = new ModelRendererTurbo(this, 30, 26, textureX, textureY); // Box 5
		attachmentModel[6] = new ModelRendererTurbo(this, 75, 19, textureX, textureY); // Box 6
		attachmentModel[7] = new ModelRendererTurbo(this, 75, 26, textureX, textureY); // Box 7
		attachmentModel[8] = new ModelRendererTurbo(this, 75, 26, textureX, textureY); // Box 8
		attachmentModel[9] = new ModelRendererTurbo(this, 1, 33, textureX, textureY); // Box 9
		attachmentModel[10] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 10
		attachmentModel[11] = new ModelRendererTurbo(this, 1, 23, textureX, textureY); // Box 11
		attachmentModel[12] = new ModelRendererTurbo(this, 26, 33, textureX, textureY); // Box 12
		attachmentModel[13] = new ModelRendererTurbo(this, 26, 33, textureX, textureY); // Box 13

		attachmentModel[0].addBox(-30F, -3.5F, -3F, 30, 2, 6, 0F); // Box 0
		attachmentModel[0].setRotationPoint(0F, 0F, 0F);

		attachmentModel[1].addShapeBox(-30F, -1.5F, -3F, 30, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 1
		attachmentModel[1].setRotationPoint(0F, 0F, 0F);

		attachmentModel[2].addShapeBox(-30F, -5.5F, -3F, 30, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 2
		attachmentModel[2].setRotationPoint(0F, 0F, 0F);

		attachmentModel[3].addShapeBox(-18F, 4F, -2F, 18, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 3
		attachmentModel[3].setRotationPoint(0F, 0F, 0F);

		attachmentModel[4].addBox(-18F, 2F, -2F, 18, 2, 4, 0F); // Box 4
		attachmentModel[4].setRotationPoint(0F, 0F, 0F);

		attachmentModel[5].addShapeBox(-18F, 1F, -2F, 18, 1, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 5
		attachmentModel[5].setRotationPoint(0F, 0F, 0F);

		attachmentModel[6].addShapeBox(-30F, 9F, -2F, 12, 2, 4, 0F, 0F, 0F, 0F, 0F, 7F, 0F, 0F, 7F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -7F, 0F, 0F, -7F, 0F, 0F, 0F, 0F); // Box 6
		attachmentModel[6].setRotationPoint(0F, 0F, 0F);

		attachmentModel[7].addShapeBox(-30F, 11F, -2F, 12, 1, 4, 0F, 0F, 0F, 0F, 0F, 7F, 0F, 0F, 7F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -7F, -1F, 0F, -7F, -1F, 0F, 0F, -1F); // Box 7
		attachmentModel[7].setRotationPoint(0F, 0F, 0F);

		attachmentModel[8].addShapeBox(-30F, 8F, -2F, 12, 1, 4, 0F, 0F, 0F, -1F, 0F, 7F, -1F, 0F, 7F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, -7F, 0F, 0F, -7F, 0F, 0F, 0F, 0F); // Box 8
		attachmentModel[8].setRotationPoint(0F, 0F, 0F);

		attachmentModel[9].addShapeBox(-35F, -6F, -3.5F, 5, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 9
		attachmentModel[9].setRotationPoint(0F, 0F, 0F);

		attachmentModel[10].addBox(-35F, -4F, -3.5F, 5, 14, 7, 0F); // Box 10
		attachmentModel[10].setRotationPoint(0F, 0F, 0F);

		attachmentModel[11].addShapeBox(-35F, 10F, -3.5F, 7, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 11
		attachmentModel[11].setRotationPoint(0F, 0F, 0F);

		attachmentModel[12].addShapeBox(-30F, 6F, -3.5F, 2, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 12
		attachmentModel[12].setRotationPoint(0F, 0F, 0F);

		attachmentModel[13].addBox(-30F, 8F, -3.5F, 2, 2, 7, 0F); // Box 13
		attachmentModel[13].setRotationPoint(0F, 0F, 0F);

		renderOffset = 0F;

		flipAll();
	}
}