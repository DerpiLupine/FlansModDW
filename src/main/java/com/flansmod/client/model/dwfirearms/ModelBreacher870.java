package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelBreacher870 extends ModelGun
{
	int textureX = 512;
	int textureY = 256;

	public ModelBreacher870()
	{
		gunModel = new ModelRendererTurbo[41];
		gunModel[0] = new ModelRendererTurbo(this, 80, 2, textureX, textureY); // barrel1
		gunModel[1] = new ModelRendererTurbo(this, 80, 2, textureX, textureY); // barrel1-2
		gunModel[2] = new ModelRendererTurbo(this, 80, 2, textureX, textureY); // barrel1-3
		gunModel[3] = new ModelRendererTurbo(this, 80, 2, textureX, textureY); // barrel1-4
		gunModel[4] = new ModelRendererTurbo(this, 80, 158, textureX, textureY); // barrel2
		gunModel[5] = new ModelRendererTurbo(this, 80, 158, textureX, textureY); // barrel2-2
		gunModel[6] = new ModelRendererTurbo(this, 80, 49, textureX, textureY); // body1
		gunModel[7] = new ModelRendererTurbo(this, 80, 71, textureX, textureY); // body2
		gunModel[8] = new ModelRendererTurbo(this, 80, 84, textureX, textureY); // body3
		gunModel[9] = new ModelRendererTurbo(this, 65, 5, textureX, textureY); // ironSight1
		gunModel[10] = new ModelRendererTurbo(this, 65, 5, textureX, textureY); // ironSight1-2
		gunModel[11] = new ModelRendererTurbo(this, 41, 5, textureX, textureY); // ironSight3
		gunModel[12] = new ModelRendererTurbo(this, 103, 39, textureX, textureY); // muzzleRing1
		gunModel[13] = new ModelRendererTurbo(this, 103, 39, textureX, textureY); // muzzleRing1-2
		gunModel[14] = new ModelRendererTurbo(this, 80, 38, textureX, textureY); // muzzleRing2
		gunModel[15] = new ModelRendererTurbo(this, 1, 51, textureX, textureY); // railBase
		gunModel[16] = new ModelRendererTurbo(this, 1, 41, textureX, textureY); // railBlock
		gunModel[17] = new ModelRendererTurbo(this, 1, 41, textureX, textureY); // railBlock
		gunModel[18] = new ModelRendererTurbo(this, 1, 41, textureX, textureY); // railBlock
		gunModel[19] = new ModelRendererTurbo(this, 1, 41, textureX, textureY); // railBlock
		gunModel[20] = new ModelRendererTurbo(this, 1, 41, textureX, textureY); // railBlock
		gunModel[21] = new ModelRendererTurbo(this, 44, 2, textureX, textureY); // scopeBase
		gunModel[22] = new ModelRendererTurbo(this, 97, 149, textureX, textureY); // shellCase1
		gunModel[23] = new ModelRendererTurbo(this, 97, 149, textureX, textureY); // shellCase1-2
		gunModel[24] = new ModelRendererTurbo(this, 97, 149, textureX, textureY); // shellCase1-3
		gunModel[25] = new ModelRendererTurbo(this, 97, 149, textureX, textureY); // shellCase1-4
		gunModel[26] = new ModelRendererTurbo(this, 97, 149, textureX, textureY); // shellCase1-5
		gunModel[27] = new ModelRendererTurbo(this, 97, 149, textureX, textureY); // shellCase1-6
		gunModel[28] = new ModelRendererTurbo(this, 80, 140, textureX, textureY); // shellDeco1
		gunModel[29] = new ModelRendererTurbo(this, 80, 140, textureX, textureY); // shellDeco1-2
		gunModel[30] = new ModelRendererTurbo(this, 80, 140, textureX, textureY); // shellDeco1-3
		gunModel[31] = new ModelRendererTurbo(this, 80, 140, textureX, textureY); // shellDeco1-4
		gunModel[32] = new ModelRendererTurbo(this, 80, 140, textureX, textureY); // shellDeco1-5
		gunModel[33] = new ModelRendererTurbo(this, 80, 140, textureX, textureY); // shellDeco1-6
		gunModel[34] = new ModelRendererTurbo(this, 80, 130, textureX, textureY); // shellRack
		gunModel[35] = new ModelRendererTurbo(this, 97, 149, textureX, textureY); // Box 54
		gunModel[36] = new ModelRendererTurbo(this, 97, 149, textureX, textureY); // Box 55
		gunModel[37] = new ModelRendererTurbo(this, 80, 140, textureX, textureY); // Box 56
		gunModel[38] = new ModelRendererTurbo(this, 80, 140, textureX, textureY); // Box 57
		gunModel[39] = new ModelRendererTurbo(this, 97, 149, textureX, textureY); // Box 58
		gunModel[40] = new ModelRendererTurbo(this, 80, 140, textureX, textureY); // Box 59

		gunModel[0].addShapeBox(0F, 0F, 0F, 60, 2, 6, 0F,0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // barrel1
		gunModel[0].setRotationPoint(20F, -24.5F, -3F);

		gunModel[1].addShapeBox(0F, 0F, 0F, 60, 2, 6, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // barrel1-2
		gunModel[1].setRotationPoint(20F, -20.5F, -3F);

		gunModel[2].addShapeBox(0F, 0F, 0F, 60, 2, 6, 0F,0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // barrel1-3
		gunModel[2].setRotationPoint(20F, -18.5F, -3F);

		gunModel[3].addShapeBox(0F, 0F, 0F, 60, 2, 6, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // barrel1-4
		gunModel[3].setRotationPoint(20F, -14.5F, -3F);

		gunModel[4].addBox(0F, 0F, 0F, 60, 2, 6, 0F); // barrel2
		gunModel[4].setRotationPoint(20F, -22.5F, -3F);

		gunModel[5].addBox(0F, 0F, 0F, 60, 2, 6, 0F); // barrel2-2
		gunModel[5].setRotationPoint(20F, -16.5F, -3F);

		gunModel[6].addBox(0F, 0F, 0F, 30, 11, 10, 0F); // body1
		gunModel[6].setRotationPoint(-10F, -23F, -5F);

		gunModel[7].addShapeBox(0F, 0F, 0F, 30, 2, 10, 0F,-3F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, -3F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body2
		gunModel[7].setRotationPoint(-10F, -25F, -5F);

		gunModel[8].addShapeBox(0F, 0F, 0F, 30, 2, 10, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, -3F, 0F, -2F); // body3
		gunModel[8].setRotationPoint(-10F, -12F, -5F);

		gunModel[9].addBox(0F, 0F, 0F, 3, 1, 3, 0F); // ironSight1
		gunModel[9].setRotationPoint(-6F, -29F, 1F);

		gunModel[10].addBox(0F, 0F, 0F, 3, 1, 3, 0F); // ironSight1-2
		gunModel[10].setRotationPoint(-6F, -29F, -4F);

		gunModel[11].addBox(0F, 0F, 0F, 4, 3, 1, 0F); // ironSight3
		gunModel[11].setRotationPoint(75F, -27F, -0.5F);

		gunModel[12].addShapeBox(0F, 0F, 0F, 4, 2, 7, 0F,0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // muzzleRing1
		gunModel[12].setRotationPoint(20F, -25F, -3.5F);

		gunModel[13].addShapeBox(0F, 0F, 0F, 4, 2, 7, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // muzzleRing1-2
		gunModel[13].setRotationPoint(20F, -20F, -3.5F);

		gunModel[14].addBox(0F, 0F, 0F, 4, 3, 7, 0F); // muzzleRing2
		gunModel[14].setRotationPoint(20F, -23F, -3.5F);

		gunModel[15].addBox(0F, 0F, 0F, 27, 2, 7, 0F); // railBase
		gunModel[15].setRotationPoint(0F, -26F, -3.5F);

		gunModel[16].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // railBlock
		gunModel[16].setRotationPoint(6F, -28F, -3.5F);

		gunModel[17].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // railBlock
		gunModel[17].setRotationPoint(0F, -28F, -3.5F);

		gunModel[18].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // railBlock
		gunModel[18].setRotationPoint(12F, -28F, -3.5F);

		gunModel[19].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // railBlock
		gunModel[19].setRotationPoint(18F, -28F, -3.5F);

		gunModel[20].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // railBlock
		gunModel[20].setRotationPoint(24F, -28F, -3.5F);

		gunModel[21].addBox(0F, 0F, 0F, 6, 4, 8, 0F); // scopeBase
		gunModel[21].setRotationPoint(-6F, -28F, -4F);

		gunModel[22].addShapeBox(0F, 0F, 0F, 2, 2, 6, 0F,0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F); // shellCase1
		gunModel[22].setRotationPoint(-3F, -13F, -11F);

		gunModel[23].addBox(0F, 0F, 0F, 2, 2, 6, 0F); // shellCase1-2
		gunModel[23].setRotationPoint(-1F, -13F, -11F);

		gunModel[24].addShapeBox(0F, 0F, 0F, 2, 2, 6, 0F,0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F); // shellCase1-3
		gunModel[24].setRotationPoint(1F, -13F, -11F);

		gunModel[25].addShapeBox(0F, 0F, 0F, 2, 2, 6, 0F,0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F); // shellCase1-4
		gunModel[25].setRotationPoint(3F, -13F, -11F);

		gunModel[26].addBox(0F, 0F, 0F, 2, 2, 6, 0F); // shellCase1-5
		gunModel[26].setRotationPoint(5F, -13F, -11F);

		gunModel[27].addShapeBox(0F, 0F, 0F, 2, 2, 6, 0F,0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F); // shellCase1-6
		gunModel[27].setRotationPoint(7F, -13F, -11F);

		gunModel[28].addShapeBox(0F, 0F, 0F, 2, 11, 6, 0F,0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F); // shellDeco1
		gunModel[28].setRotationPoint(-3F, -24F, -11F);

		gunModel[29].addBox(0F, 0F, 0F, 2, 11, 6, 0F); // shellDeco1-2
		gunModel[29].setRotationPoint(-1F, -24F, -11F);

		gunModel[30].addShapeBox(0F, 0F, 0F, 2, 11, 6, 0F,0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F); // shellDeco1-3
		gunModel[30].setRotationPoint(1F, -24F, -11F);

		gunModel[31].addShapeBox(0F, 0F, 0F, 2, 11, 6, 0F,0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F); // shellDeco1-4
		gunModel[31].setRotationPoint(3F, -24F, -11F);

		gunModel[32].addBox(0F, 0F, 0F, 2, 11, 6, 0F); // shellDeco1-5
		gunModel[32].setRotationPoint(5F, -24F, -11F);

		gunModel[33].addShapeBox(0F, 0F, 0F, 2, 11, 6, 0F,0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F); // shellDeco1-6
		gunModel[33].setRotationPoint(7F, -24F, -11F);

		gunModel[34].addBox(0F, 0F, 0F, 20, 6, 3, 0F); // shellRack
		gunModel[34].setRotationPoint(-4F, -21F, -8F);

		gunModel[35].addShapeBox(0F, 0F, 0F, 2, 2, 6, 0F,0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F); // Box 54
		gunModel[35].setRotationPoint(13F, -13F, -11F);

		gunModel[36].addShapeBox(0F, 0F, 0F, 2, 2, 6, 0F,0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F); // Box 55
		gunModel[36].setRotationPoint(9F, -13F, -11F);

		gunModel[37].addShapeBox(0F, 0F, 0F, 2, 11, 6, 0F,0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F); // Box 56
		gunModel[37].setRotationPoint(9F, -24F, -11F);

		gunModel[38].addBox(0F, 0F, 0F, 2, 11, 6, 0F); // Box 57
		gunModel[38].setRotationPoint(11F, -24F, -11F);

		gunModel[39].addBox(0F, 0F, 0F, 2, 2, 6, 0F); // Box 58
		gunModel[39].setRotationPoint(11F, -13F, -11F);

		gunModel[40].addShapeBox(0F, 0F, 0F, 2, 11, 6, 0F,0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F); // Box 59
		gunModel[40].setRotationPoint(13F, -24F, -11F);


		defaultStockModel = new ModelRendererTurbo[7];
		defaultStockModel[0] = new ModelRendererTurbo(this, 34, 61, textureX, textureY); // stock1
		defaultStockModel[1] = new ModelRendererTurbo(this, 1, 61, textureX, textureY); // stock2
		defaultStockModel[2] = new ModelRendererTurbo(this, 1, 102, textureX, textureY); // stock3
		defaultStockModel[3] = new ModelRendererTurbo(this, 24, 140, textureX, textureY); // stock4
		defaultStockModel[4] = new ModelRendererTurbo(this, 24, 128, textureX, textureY); // stock5
		defaultStockModel[5] = new ModelRendererTurbo(this, 1, 89, textureX, textureY); // stock6
		defaultStockModel[6] = new ModelRendererTurbo(this, 1, 128, textureX, textureY); // stock7

		defaultStockModel[0].addShapeBox(0F, 0F, 0F, 6, 13, 9, 0F,0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F); // stock1
		defaultStockModel[0].setRotationPoint(-16F, -21F, -4.5F);

		defaultStockModel[1].addShapeBox(0F, 0F, 0F, 7, 18, 9, 0F,0F, -8F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -8F, 0F, 0F, 0F, 0F, 0F, -8F, 0F, 0F, -8F, 0F, 0F, 0F, 0F); // stock2
		defaultStockModel[1].setRotationPoint(-23F, -18F, -4.5F);

		defaultStockModel[2].addShapeBox(0F, 0F, 0F, 28, 16, 9, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -6F, 0F, 0F, -6F, 0F, 0F, 0F, 0F); // stock3
		defaultStockModel[2].setRotationPoint(-51F, -10F, -4.5F);

		defaultStockModel[3].addShapeBox(0F, 0F, 0F, 6, 2, 9, 0F,0F, 0F, -2F, 0F, 3F, -2F, 0F, 3F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F); // stock4
		defaultStockModel[3].setRotationPoint(-16F, -20F, -4.5F);

		defaultStockModel[4].addShapeBox(0F, 0F, 0F, 7, 2, 9, 0F,0F, 0F, -2F, 0F, 8F, -2F, 0F, 8F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, -8F, 0F, 0F, -8F, 0F, 0F, 0F, 0F); // stock5
		defaultStockModel[4].setRotationPoint(-23F, -12F, -4.5F);

		defaultStockModel[5].addShapeBox(0F, 0F, 0F, 30, 3, 9, 0F,0F, 0F, -2F, -3F, 0F, -2F, -3F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // stock6
		defaultStockModel[5].setRotationPoint(-53F, -13F, -4.5F);

		defaultStockModel[6].addBox(0F, 0F, 0F, 2, 16, 9, 0F); // stock7
		defaultStockModel[6].setRotationPoint(-53F, -10F, -4.5F);


		defaultGripModel = new ModelRendererTurbo[6];
		defaultGripModel[0] = new ModelRendererTurbo(this, 26, 15, textureX, textureY); // grip1
		defaultGripModel[1] = new ModelRendererTurbo(this, 26, 15, textureX, textureY); // grip1-2
		defaultGripModel[2] = new ModelRendererTurbo(this, 1, 15, textureX, textureY); // grip2
		defaultGripModel[3] = new ModelRendererTurbo(this, 1, 2, textureX, textureY); // gripBase
		defaultGripModel[4] = new ModelRendererTurbo(this, 80, 11, textureX, textureY); // handGuard1
		defaultGripModel[5] = new ModelRendererTurbo(this, 80, 25, textureX, textureY); // handGuard2

		defaultGripModel[0].addShapeBox(0F, 0F, 0F, 2, 15, 8, 0F,0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F); // grip1
		defaultGripModel[0].setRotationPoint(44F, -9.5F, -4F);

		defaultGripModel[1].addShapeBox(0F, 0F, 0F, 2, 15, 8, 0F,0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F); // grip1-2
		defaultGripModel[1].setRotationPoint(50F, -9.5F, -4F);

		defaultGripModel[2].addBox(0F, 0F, 0F, 4, 15, 8, 0F); // grip2
		defaultGripModel[2].setRotationPoint(46F, -9.5F, -4F);

		defaultGripModel[3].addBox(0F, 0F, 0F, 12, 3, 9, 0F); // gripBase
		defaultGripModel[3].setRotationPoint(42F, -12.5F, -4.5F);

		defaultGripModel[4].addBox(0F, 0F, 0F, 40, 4, 9, 0F); // handGuard1
		defaultGripModel[4].setRotationPoint(20F, -17.5F, -4.5F);

		defaultGripModel[5].addShapeBox(0F, 0F, 0F, 40, 3, 9, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // handGuard2
		defaultGripModel[5].setRotationPoint(20F, -13.5F, -4.5F);


		ammoModel = new ModelRendererTurbo[6];
		ammoModel[0] = new ModelRendererTurbo(this, 115, 121, textureX, textureY); // casing1
		ammoModel[1] = new ModelRendererTurbo(this, 115, 121, textureX, textureY); // casing1-2
		ammoModel[2] = new ModelRendererTurbo(this, 115, 121, textureX, textureY); // casing1-3
		ammoModel[3] = new ModelRendererTurbo(this, 80, 121, textureX, textureY); // shell1
		ammoModel[4] = new ModelRendererTurbo(this, 80, 121, textureX, textureY); // shell1-2
		ammoModel[5] = new ModelRendererTurbo(this, 80, 121, textureX, textureY); // shell1-3

		ammoModel[0].addShapeBox(0F, 0F, 0F, 2, 2, 6, 0F,0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // casing1
		ammoModel[0].setRotationPoint(6F, -18F, -3F);

		ammoModel[1].addBox(0F, 0F, 0F, 2, 2, 6, 0F); // casing1-2
		ammoModel[1].setRotationPoint(6F, -16F, -3F);

		ammoModel[2].addShapeBox(0F, 0F, 0F, 2, 2, 6, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // casing1-3
		ammoModel[2].setRotationPoint(6F, -14F, -3F);

		ammoModel[3].addShapeBox(0F, 0F, 0F, 11, 2, 6, 0F,0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // shell1
		ammoModel[3].setRotationPoint(8F, -18F, -3F);

		ammoModel[4].addBox(0F, 0F, 0F, 11, 2, 6, 0F); // shell1-2
		ammoModel[4].setRotationPoint(8F, -16F, -3F);

		ammoModel[5].addShapeBox(0F, 0F, 0F, 11, 2, 6, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // shell1-3
		ammoModel[5].setRotationPoint(8F, -14F, -3F);



		barrelAttachPoint = new Vector3f(0F /16F, 0F /16F, 0F /16F);
		stockAttachPoint = new Vector3f(0F /16F, 0F /16F, 0F /16F);
		scopeAttachPoint = new Vector3f(0F /16F, 0F /16F, 0F /16F);
		gripAttachPoint = new Vector3f(0 /16F, 0F /16F, 0F /16F);

		gunSlideDistance = 0F;
		animationType = EnumAnimationType.SHOTGUN;

		numBulletsInReloadAnimation = 2;
		tiltGunTime = 0.25F;
		unloadClipTime = 0.0F;
		loadClipTime = 0.5F;
		untiltGunTime = 0.25F;

		translateAll(0F, 0F, 0F);


		flipAll();
	}
}