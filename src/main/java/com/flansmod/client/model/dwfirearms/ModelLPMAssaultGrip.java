package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.ModelAttachment;
import com.flansmod.client.tmt.ModelRendererTurbo;

import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelLPMAssaultGrip extends ModelAttachment 
{
	int textureX = 512;
	int textureY = 256;

	public ModelLPMAssaultGrip()
	{
		attachmentModel = new ModelRendererTurbo[14];
		attachmentModel[0] = new ModelRendererTurbo(this, 240, 25, textureX, textureY); // Box 10
		attachmentModel[1] = new ModelRendererTurbo(this, 240, 25, textureX, textureY); // Box 11
		attachmentModel[2] = new ModelRendererTurbo(this, 240, 25, textureX, textureY); // Box 12
		attachmentModel[3] = new ModelRendererTurbo(this, 240, 25, textureX, textureY); // Box 14
		attachmentModel[4] = new ModelRendererTurbo(this, 240, 25, textureX, textureY); // Box 15
		attachmentModel[5] = new ModelRendererTurbo(this, 240, 25, textureX, textureY); // Box 16
		attachmentModel[6] = new ModelRendererTurbo(this, 240, 1, textureX, textureY); // grip1
		attachmentModel[7] = new ModelRendererTurbo(this, 240, 1, textureX, textureY); // grip1
		attachmentModel[8] = new ModelRendererTurbo(this, 261, 1, textureX, textureY); // grip2
		attachmentModel[9] = new ModelRendererTurbo(this, 284, 14, textureX, textureY); // grip3
		attachmentModel[10] = new ModelRendererTurbo(this, 284, 14, textureX, textureY); // grip3
		attachmentModel[11] = new ModelRendererTurbo(this, 305, 14, textureX, textureY); // grip4
		attachmentModel[12] = new ModelRendererTurbo(this, 284, 1, textureX, textureY); // gripBase
		attachmentModel[13] = new ModelRendererTurbo(this, 263, 25, textureX, textureY); // gripBase2

		attachmentModel[0].addShapeBox(1F, 6F, -4F, 3, 1, 8, 0F, 0F, 0F, 0F, -1F, 0F, -2F, -1F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F); // Box 10
		attachmentModel[0].setRotationPoint(0F, 0F, 0F);

		attachmentModel[1].addShapeBox(1F, 7F, -4F, 3, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F); // Box 11
		attachmentModel[1].setRotationPoint(0F, 0F, 0F);

		attachmentModel[2].addShapeBox(1F, 8F, -4F, 3, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -2F, -1F, 0F, -2F, 0F, 0F, 0F); // Box 12
		attachmentModel[2].setRotationPoint(0F, 0F, 0F);

		attachmentModel[3].addShapeBox(1F, 16F, -4F, 3, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F); // Box 14
		attachmentModel[3].setRotationPoint(0F, 0F, 0F);

		attachmentModel[4].addShapeBox(1F, 15F, -4F, 3, 1, 8, 0F, 0F, 0F, 0F, -1F, 0F, -2F, -1F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F); // Box 15
		attachmentModel[4].setRotationPoint(0F, 0F, 0F);

		attachmentModel[5].addShapeBox(1F, 17F, -4F, 3, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -2F, -1F, 0F, -2F, 0F, 0F, 0F); // Box 16
		attachmentModel[5].setRotationPoint(0F, 0F, 0F);

		attachmentModel[6].addShapeBox(-4F, 3F, -4F, 2, 15, 8, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F); // grip1
		attachmentModel[6].setRotationPoint(0F, 0F, 0F);

		attachmentModel[7].addShapeBox(1F, 3F, -4F, 2, 15, 8, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F); // grip1
		attachmentModel[7].setRotationPoint(0F, 0F, 0F);

		attachmentModel[8].addBox(-2F, 3F, -4F, 3, 15, 8, 0F); // grip2
		attachmentModel[8].setRotationPoint(0F, 0F, 0F);

		attachmentModel[9].addShapeBox(-4F, 18F, -4F, 2, 2, 8, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, -1.5F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, -1.5F, 0F, -2F); // grip3
		attachmentModel[9].setRotationPoint(0F, 0F, 0F);

		attachmentModel[10].addShapeBox(1F, 18F, -4F, 2, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, -2F, -1.5F, 0F, -2F, -1.5F, 0F, -2F, 0F, 0F, -2F); // grip3
		attachmentModel[10].setRotationPoint(0F, 0F, 0F);

		attachmentModel[11].addShapeBox(-2F, 18F, -4F, 3, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // grip4
		attachmentModel[11].setRotationPoint(0F, 0F, 0F);

		attachmentModel[12].addBox(-7F, 0F, -4F, 13, 3, 8, 0F); // gripBase
		attachmentModel[12].setRotationPoint(0F, 0F, 0F);

		attachmentModel[13].addShapeBox(6F, 1F, -4F, 2, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // gripBase2
		attachmentModel[13].setRotationPoint(0F, 0F, 0F);
		
		renderOffset = 0F;

		flipAll();
	}
}