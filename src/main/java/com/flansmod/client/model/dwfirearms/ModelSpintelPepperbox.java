package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelSpintelPepperbox extends ModelGun
{
	int textureX = 512;
	int textureY = 256;

	public ModelSpintelPepperbox()
	{
		gunModel = new ModelRendererTurbo[63];
		gunModel[0] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Import barrel
		gunModel[1] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Import barrel
		gunModel[2] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Import barrel
		gunModel[3] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Import barrel
		gunModel[4] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Import barrel
		gunModel[5] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Import barrel
		gunModel[6] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Import barrel
		gunModel[7] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Import barrel
		gunModel[8] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Import barrel
		gunModel[9] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Import barrel
		gunModel[10] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Import barrel
		gunModel[11] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Import barrel
		gunModel[12] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Import barrel
		gunModel[13] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Import barrel
		gunModel[14] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Import barrel
		gunModel[15] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Import barrel
		gunModel[16] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Import barrel
		gunModel[17] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Import barrel
		gunModel[18] = new ModelRendererTurbo(this, 96, 78, textureX, textureY); // Import body1
		gunModel[19] = new ModelRendererTurbo(this, 96, 89, textureX, textureY); // Import body2
		gunModel[20] = new ModelRendererTurbo(this, 96, 62, textureX, textureY); // Import body3
		gunModel[21] = new ModelRendererTurbo(this, 96, 100, textureX, textureY); // Import body4
		gunModel[22] = new ModelRendererTurbo(this, 139, 75, textureX, textureY); // Import body5
		gunModel[23] = new ModelRendererTurbo(this, 96, 35, textureX, textureY); // Import body6
		gunModel[24] = new ModelRendererTurbo(this, 139, 85, textureX, textureY); // Import Box38
		gunModel[25] = new ModelRendererTurbo(this, 19, 28, textureX, textureY); // Import canister1
		gunModel[26] = new ModelRendererTurbo(this, 19, 28, textureX, textureY); // Import canister1
		gunModel[27] = new ModelRendererTurbo(this, 1, 24, textureX, textureY); // Import canister2
		gunModel[28] = new ModelRendererTurbo(this, 1, 24, textureX, textureY); // Import canister2
		gunModel[29] = new ModelRendererTurbo(this, 1, 24, textureX, textureY); // Import canister2
		gunModel[30] = new ModelRendererTurbo(this, 1, 24, textureX, textureY); // Import canister2
		gunModel[31] = new ModelRendererTurbo(this, 10, 24, textureX, textureY); // Import canister3
		gunModel[32] = new ModelRendererTurbo(this, 10, 24, textureX, textureY); // Import canister3
		gunModel[33] = new ModelRendererTurbo(this, 1, 75, textureX, textureY); // Import grip1
		gunModel[34] = new ModelRendererTurbo(this, 1, 33, textureX, textureY); // Import grip2
		gunModel[35] = new ModelRendererTurbo(this, 1, 61, textureX, textureY); // Import grip3
		gunModel[36] = new ModelRendererTurbo(this, 1, 50, textureX, textureY); // Import grip4
		gunModel[37] = new ModelRendererTurbo(this, 96, 28, textureX, textureY); // Import pepperboxBarrel1
		gunModel[38] = new ModelRendererTurbo(this, 96, 28, textureX, textureY); // Import pepperboxBarrel1
		gunModel[39] = new ModelRendererTurbo(this, 96, 16, textureX, textureY); // Import pepperboxBarrel2
		gunModel[40] = new ModelRendererTurbo(this, 96, 16, textureX, textureY); // Import pepperboxBarrel2
		gunModel[41] = new ModelRendererTurbo(this, 96, 1, textureX, textureY); // Import pepperboxBarrel3
		gunModel[42] = new ModelRendererTurbo(this, 43, 9, textureX, textureY); // Import rail1
		gunModel[43] = new ModelRendererTurbo(this, 43, 1, textureX, textureY); // Import rail2
		gunModel[44] = new ModelRendererTurbo(this, 43, 15, textureX, textureY); // Import rail3
		gunModel[45] = new ModelRendererTurbo(this, 43, 15, textureX, textureY); // Import rail3-2
		gunModel[46] = new ModelRendererTurbo(this, 26, 2, textureX, textureY); // Import railBase
		gunModel[47] = new ModelRendererTurbo(this, 150, 54, textureX, textureY); // Import ring1
		gunModel[48] = new ModelRendererTurbo(this, 150, 54, textureX, textureY); // Import ring1
		gunModel[49] = new ModelRendererTurbo(this, 150, 54, textureX, textureY); // Import ring1
		gunModel[50] = new ModelRendererTurbo(this, 150, 54, textureX, textureY); // Import ring1
		gunModel[51] = new ModelRendererTurbo(this, 150, 54, textureX, textureY); // Import ring1
		gunModel[52] = new ModelRendererTurbo(this, 150, 54, textureX, textureY); // Import ring1
		gunModel[53] = new ModelRendererTurbo(this, 125, 49, textureX, textureY); // Import ring2
		gunModel[54] = new ModelRendererTurbo(this, 125, 49, textureX, textureY); // Import ring2
		gunModel[55] = new ModelRendererTurbo(this, 125, 49, textureX, textureY); // Import ring2
		gunModel[56] = new ModelRendererTurbo(this, 125, 49, textureX, textureY); // Import ring2
		gunModel[57] = new ModelRendererTurbo(this, 125, 49, textureX, textureY); // Import ring2
		gunModel[58] = new ModelRendererTurbo(this, 125, 49, textureX, textureY); // Import ring2
		gunModel[59] = new ModelRendererTurbo(this, 96, 45, textureX, textureY); // Import ring3
		gunModel[60] = new ModelRendererTurbo(this, 96, 45, textureX, textureY); // Import ring3
		gunModel[61] = new ModelRendererTurbo(this, 96, 45, textureX, textureY); // Import ring3
		gunModel[62] = new ModelRendererTurbo(this, 1, 6, textureX, textureY); // Import sightBase

		gunModel[0].addShapeBox(0F, 0F, 0F, 1, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import barrel
		gunModel[0].setRotationPoint(56.2F, -23F, -3.5F);

		gunModel[1].addShapeBox(0F, 0F, 0F, 1, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Import barrel
		gunModel[1].setRotationPoint(56.2F, -18F, 2F);

		gunModel[2].addBox(0F, 0F, 0F, 1, 1, 3, 0F); // Import barrel
		gunModel[2].setRotationPoint(56.2F, -16F, 0.5F);

		gunModel[3].addBox(0F, 0F, 0F, 1, 1, 3, 0F); // Import barrel
		gunModel[3].setRotationPoint(56.2F, -22F, -3.5F);

		gunModel[4].addShapeBox(0F, 0F, 0F, 1, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Import barrel
		gunModel[4].setRotationPoint(56.2F, -15F, 0.5F);

		gunModel[5].addBox(0F, 0F, 0F, 1, 1, 3, 0F); // Import barrel
		gunModel[5].setRotationPoint(56.2F, -16F, -3.5F);

		gunModel[6].addShapeBox(0F, 0F, 0F, 1, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Import barrel
		gunModel[6].setRotationPoint(56.2F, -15F, -3.5F);

		gunModel[7].addShapeBox(0F, 0F, 0F, 1, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import barrel
		gunModel[7].setRotationPoint(56.2F, -17F, -3.5F);

		gunModel[8].addShapeBox(0F, 0F, 0F, 1, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import barrel
		gunModel[8].setRotationPoint(56.2F, -17F, 0.5F);

		gunModel[9].addBox(0F, 0F, 0F, 1, 1, 3, 0F); // Import barrel
		gunModel[9].setRotationPoint(56.2F, -19F, 2F);

		gunModel[10].addShapeBox(0F, 0F, 0F, 1, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Import barrel
		gunModel[10].setRotationPoint(56.2F, -21F, -3.5F);

		gunModel[11].addShapeBox(0F, 0F, 0F, 1, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import barrel
		gunModel[11].setRotationPoint(56.2F, -23F, 0.5F);

		gunModel[12].addShapeBox(0F, 0F, 0F, 1, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import barrel
		gunModel[12].setRotationPoint(56.2F, -20F, 2F);

		gunModel[13].addShapeBox(0F, 0F, 0F, 1, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import barrel
		gunModel[13].setRotationPoint(56.2F, -20F, -5F);

		gunModel[14].addBox(0F, 0F, 0F, 1, 1, 3, 0F); // Import barrel
		gunModel[14].setRotationPoint(56.2F, -19F, -5F);

		gunModel[15].addBox(0F, 0F, 0F, 1, 1, 3, 0F); // Import barrel
		gunModel[15].setRotationPoint(56.2F, -22F, 0.5F);

		gunModel[16].addShapeBox(0F, 0F, 0F, 1, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Import barrel
		gunModel[16].setRotationPoint(56.2F, -21F, 0.5F);

		gunModel[17].addShapeBox(0F, 0F, 0F, 1, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Import barrel
		gunModel[17].setRotationPoint(56.2F, -18F, -5F);

		gunModel[18].addShapeBox(0F, 0F, 0F, 11, 2, 8, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import body1
		gunModel[18].setRotationPoint(-4F, -13F, -4F);

		gunModel[19].addShapeBox(0F, 0F, 0F, 12, 2, 8, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import body2
		gunModel[19].setRotationPoint(-6F, -15F, -4F);

		gunModel[20].addBox(0F, 0F, 0F, 13, 7, 8, 0F); // Import body3
		gunModel[20].setRotationPoint(-7F, -22F, -4F);

		gunModel[21].addShapeBox(0F, 0F, 0F, 11, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F); // Import body4
		gunModel[21].setRotationPoint(-5F, -24F, -4F);

		gunModel[22].addShapeBox(0F, 0F, 0F, 3, 3, 6, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F); // Import body5
		gunModel[22].setRotationPoint(7F, -14F, -3F);

		gunModel[23].addShapeBox(0F, 0F, 0F, 40, 3, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Import body6
		gunModel[23].setRotationPoint(10F, -14F, -3F);

		gunModel[24].addShapeBox(0F, 0F, 0F, 1, 8, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F); // Import Box38
		gunModel[24].setRotationPoint(-8F, -25F, -1.5F);

		gunModel[25].addBox(0F, 0F, 0F, 1, 3, 1, 0F); // Import canister1
		gunModel[25].setRotationPoint(-2F, -20.5F, -6F);
		gunModel[25].rotateAngleX = 1.04719755F;

		gunModel[26].addBox(0F, 0F, 0F, 1, 3, 1, 0F); // Import canister1
		gunModel[26].setRotationPoint(2F, -20.5F, -6F);
		gunModel[26].rotateAngleX = 1.04719755F;

		gunModel[27].addShapeBox(0F, 0F, 0F, 1, 5, 3, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Import canister2
		gunModel[27].setRotationPoint(3F, -22F, -10.5F);
		gunModel[27].rotateAngleX = 1.04719755F;

		gunModel[28].addShapeBox(0F, 0F, 0F, 1, 5, 3, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Import canister2
		gunModel[28].setRotationPoint(1F, -22F, -10.5F);
		gunModel[28].rotateAngleX = 1.04719755F;

		gunModel[29].addShapeBox(0F, 0F, 0F, 1, 5, 3, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Import canister2
		gunModel[29].setRotationPoint(-1F, -22F, -10.5F);
		gunModel[29].rotateAngleX = 1.04719755F;

		gunModel[30].addShapeBox(0F, 0F, 0F, 1, 5, 3, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Import canister2
		gunModel[30].setRotationPoint(-3F, -22F, -10.5F);
		gunModel[30].rotateAngleX = 1.04719755F;

		gunModel[31].addBox(0F, 0F, 0F, 1, 5, 3, 0F); // Import canister3
		gunModel[31].setRotationPoint(2F, -22F, -10.5F);
		gunModel[31].rotateAngleX = 1.04719755F;

		gunModel[32].addBox(0F, 0F, 0F, 1, 5, 3, 0F); // Import canister3
		gunModel[32].setRotationPoint(-2F, -22F, -10.5F);
		gunModel[32].rotateAngleX = 1.04719755F;

		gunModel[33].addShapeBox(0F, 0F, 0F, 11, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Import grip1
		gunModel[33].setRotationPoint(-10F, 4F, -4F);

		gunModel[34].addBox(0F, 0F, 0F, 11, 8, 8, 0F); // Import grip2
		gunModel[34].setRotationPoint(-10F, -4F, -4F);

		gunModel[35].addShapeBox(0F, 0F, 0F, 10, 5, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 3F, 0F, 0F); // Import grip3
		gunModel[35].setRotationPoint(-7F, -9F, -4F);

		gunModel[36].addShapeBox(0F, 0F, 0F, 12, 2, 8, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F); // Import grip4
		gunModel[36].setRotationPoint(-7F, -11F, -4F);

		gunModel[37].addShapeBox(0F, 0F, 0F, 39, 2, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2.5F, 0F, 0F, 2.5F, 0F, 0F, 2.5F, 0F, 0F, 2.5F); // Import pepperboxBarrel1
		gunModel[37].setRotationPoint(18F, -24F, -2F);

		gunModel[38].addShapeBox(0F, 0F, 0F, 39, 2, 4, 0F, 0F, 0F, 2.5F, 0F, 0F, 2.5F, 0F, 0F, 2.5F, 0F, 0F, 2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import pepperboxBarrel1
		gunModel[38].setRotationPoint(18F, -15F, -2F);

		gunModel[39].addShapeBox(0F, 0F, 0F, 39, 2, 9, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import pepperboxBarrel2
		gunModel[39].setRotationPoint(18F, -17F, -4.5F);

		gunModel[40].addShapeBox(0F, 0F, 0F, 39, 2, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F); // Import pepperboxBarrel2
		gunModel[40].setRotationPoint(18F, -22F, -4.5F);

		gunModel[41].addBox(0F, 0F, 0F, 39, 3, 11, 0F); // Import pepperboxBarrel3
		gunModel[41].setRotationPoint(18F, -20F, -5.5F);

		gunModel[42].addBox(0F, 0F, 0F, 18, 1, 4, 0F); // Import rail1
		gunModel[42].setRotationPoint(6F, -26F, -2F);

		gunModel[43].addBox(0F, 0F, 0F, 18, 1, 6, 0F); // Import rail2
		gunModel[43].setRotationPoint(6F, -27F, -3F);

		gunModel[44].addShapeBox(0F, 0F, 0F, 18, 1, 1, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import rail3
		gunModel[44].setRotationPoint(6F, -28F, 2F);

		gunModel[45].addShapeBox(0F, 0F, 0F, 18, 1, 1, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import rail3-2
		gunModel[45].setRotationPoint(6F, -28F, -3F);

		gunModel[46].addShapeBox(0F, 0F, 0F, 4, 2, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F); // Import railBase
		gunModel[46].setRotationPoint(2F, -26F, -2F);

		gunModel[47].addShapeBox(0F, 0F, 0F, 2, 2, 5, 0F, 0F, 0F, 2.5F, 0F, 0F, 2.5F, 0F, 0F, 2.5F, 0F, 0F, 2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import ring1
		gunModel[47].setRotationPoint(19F, -14.5F, -2.5F);

		gunModel[48].addShapeBox(0F, 0F, 0F, 2, 2, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2.5F, 0F, 0F, 2.5F, 0F, 0F, 2.5F, 0F, 0F, 2.5F); // Import ring1
		gunModel[48].setRotationPoint(19F, -24.5F, -2.5F);

		gunModel[49].addShapeBox(0F, 0F, 0F, 2, 2, 5, 0F, 0F, 0F, 2.5F, 0F, 0F, 2.5F, 0F, 0F, 2.5F, 0F, 0F, 2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import ring1
		gunModel[49].setRotationPoint(6F, -14.5F, -2.5F);

		gunModel[50].addShapeBox(0F, 0F, 0F, 2, 2, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2.5F, 0F, 0F, 2.5F, 0F, 0F, 2.5F, 0F, 0F, 2.5F); // Import ring1
		gunModel[50].setRotationPoint(6F, -24.5F, -2.5F);

		gunModel[51].addShapeBox(0F, 0F, 0F, 2, 2, 5, 0F, 0F, 0F, 2.5F, 0F, 0F, 2.5F, 0F, 0F, 2.5F, 0F, 0F, 2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import ring1
		gunModel[51].setRotationPoint(47.8F, -14.5F, -2.5F);

		gunModel[52].addShapeBox(0F, 0F, 0F, 2, 2, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2.5F, 0F, 0F, 2.5F, 0F, 0F, 2.5F, 0F, 0F, 2.5F); // Import ring1
		gunModel[52].setRotationPoint(47.8F, -24.5F, -2.5F);

		gunModel[53].addShapeBox(0F, 0F, 0F, 2, 2, 10, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import ring2
		gunModel[53].setRotationPoint(6F, -16.5F, -5F);

		gunModel[54].addShapeBox(0F, 0F, 0F, 2, 2, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F); // Import ring2
		gunModel[54].setRotationPoint(6F, -22.5F, -5F);

		gunModel[55].addShapeBox(0F, 0F, 0F, 2, 2, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F); // Import ring2
		gunModel[55].setRotationPoint(47.8F, -22.5F, -5F);

		gunModel[56].addShapeBox(0F, 0F, 0F, 2, 2, 10, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import ring2
		gunModel[56].setRotationPoint(47.8F, -16.5F, -5F);

		gunModel[57].addShapeBox(0F, 0F, 0F, 2, 2, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F); // Import ring2
		gunModel[57].setRotationPoint(19F, -22.5F, -5F);

		gunModel[58].addShapeBox(0F, 0F, 0F, 2, 2, 10, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import ring2
		gunModel[58].setRotationPoint(19F, -16.5F, -5F);

		gunModel[59].addBox(0F, 0F, 0F, 2, 4, 12, 0F); // Import ring3
		gunModel[59].setRotationPoint(6F, -20.5F, -6F);

		gunModel[60].addBox(0F, 0F, 0F, 2, 4, 12, 0F); // Import ring3
		gunModel[60].setRotationPoint(19F, -20.5F, -6F);

		gunModel[61].addBox(0F, 0F, 0F, 2, 4, 12, 0F); // Import ring3
		gunModel[61].setRotationPoint(47.8F, -20.5F, -6F);

		gunModel[62].addBox(0F, 0F, 0F, 4, 2, 6, 0F); // Import sightBase
		gunModel[62].setRotationPoint(-4F, -25.5F, -3F);


		defaultScopeModel = new ModelRendererTurbo[6];
		defaultScopeModel[0] = new ModelRendererTurbo(this, 10, 2, textureX, textureY); // Import ironSight
		defaultScopeModel[1] = new ModelRendererTurbo(this, 1, 15, textureX, textureY); // Import sight1
		defaultScopeModel[2] = new ModelRendererTurbo(this, 1, 15, textureX, textureY); // Import sight1
		defaultScopeModel[3] = new ModelRendererTurbo(this, 18, 17, textureX, textureY); // Import sight3
		defaultScopeModel[4] = new ModelRendererTurbo(this, 18, 17, textureX, textureY); // Import sight3
		defaultScopeModel[5] = new ModelRendererTurbo(this, 22, 9, textureX, textureY); // Import sightBase2

		defaultScopeModel[0].addBox(0F, 0F, 0F, 1, 2, 1, 0F); // Import ironSight
		defaultScopeModel[0].setRotationPoint(-2.5F, -29.5F, -0.5F);

		defaultScopeModel[1].addShapeBox(0F, 0F, 0F, 2, 1, 6, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import sight1
		defaultScopeModel[1].setRotationPoint(-3F, -32.5F, -3F);

		defaultScopeModel[2].addShapeBox(0F, 0F, 0F, 2, 1, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Import sight1
		defaultScopeModel[2].setRotationPoint(-3F, -27.5F, -3F);

		defaultScopeModel[3].addBox(0F, 0F, 0F, 2, 4, 1, 0F); // Import sight3
		defaultScopeModel[3].setRotationPoint(-3F, -31.5F, 2F);

		defaultScopeModel[4].addBox(0F, 0F, 0F, 2, 4, 1, 0F); // Import sight3
		defaultScopeModel[4].setRotationPoint(-3F, -31.5F, -3F);

		defaultScopeModel[5].addShapeBox(0F, 0F, 0F, 4, 1, 6, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import sightBase2
		defaultScopeModel[5].setRotationPoint(-4F, -26.5F, -3F);


		ammoModel = new ModelRendererTurbo[5];
		ammoModel[0] = new ModelRendererTurbo(this, 42, 83, textureX, textureY); // Import body7
		ammoModel[1] = new ModelRendererTurbo(this, 42, 77, textureX, textureY); // Import Box36
		ammoModel[2] = new ModelRendererTurbo(this, 42, 67, textureX, textureY); // Import Box37
		ammoModel[3] = new ModelRendererTurbo(this, 42, 67, textureX, textureY); // Import Box39
		ammoModel[4] = new ModelRendererTurbo(this, 42, 77, textureX, textureY); // Import Box40

		ammoModel[0].addBox(0F, 0F, 0F, 6, 3, 9, 0F); // Import body7
		ammoModel[0].setRotationPoint(7F, -20F, -4.5F);

		ammoModel[1].addShapeBox(0F, 0F, 0F, 6, 1, 4, 0F, 0F, 0F, 1.5F, 0F, 0F, 1.5F, 0F, 0F, 1.5F, 0F, 0F, 1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import Box36
		ammoModel[1].setRotationPoint(7F, -15F, -2F);

		ammoModel[2].addShapeBox(0F, 0F, 0F, 6, 2, 7, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import Box37
		ammoModel[2].setRotationPoint(7F, -17F, -3.5F);

		ammoModel[3].addShapeBox(0F, 0F, 0F, 6, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F); // Import Box39
		ammoModel[3].setRotationPoint(7F, -22F, -3.5F);

		ammoModel[4].addShapeBox(0F, 0F, 0F, 6, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1.5F, 0F, 0F, 1.5F, 0F, 0F, 1.5F, 0F, 0F, 1.5F); // Import Box40
		ammoModel[4].setRotationPoint(7F, -23F, -2F);


		revolverBarrelModel = new ModelRendererTurbo[5];
		revolverBarrelModel[0] = new ModelRendererTurbo(this, 42, 33, textureX, textureY); // Import pepperboxCylinder2
		revolverBarrelModel[1] = new ModelRendererTurbo(this, 42, 48, textureX, textureY); // Import pepperboxCylinder2
		revolverBarrelModel[2] = new ModelRendererTurbo(this, 42, 60, textureX, textureY); // Import pepperboxCylinder2
		revolverBarrelModel[3] = new ModelRendererTurbo(this, 42, 60, textureX, textureY); // Import pepperboxCylinder2
		revolverBarrelModel[4] = new ModelRendererTurbo(this, 42, 48, textureX, textureY); // Import pepperboxCylinder2

		revolverBarrelModel[0].addBox(0F, 0F, 0F, 10, 3, 11, 0F); // Import pepperboxCylinder2
		revolverBarrelModel[0].setRotationPoint(8F, -20F, -5.5F);

		revolverBarrelModel[1].addShapeBox(0F, 0F, 0F, 10, 2, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F); // Import pepperboxCylinder2
		revolverBarrelModel[1].setRotationPoint(8F, -22F, -4.5F);

		revolverBarrelModel[2].addShapeBox(0F, 0F, 0F, 10, 2, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2.5F, 0F, 0F, 2.5F, 0F, 0F, 2.5F, 0F, 0F, 2.5F); // Import pepperboxCylinder2
		revolverBarrelModel[2].setRotationPoint(8F, -24F, -2F);

		revolverBarrelModel[3].addShapeBox(0F, 0F, 0F, 10, 2, 4, 0F, 0F, 0F, 2.5F, 0F, 0F, 2.5F, 0F, 0F, 2.5F, 0F, 0F, 2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import pepperboxCylinder2
		revolverBarrelModel[3].setRotationPoint(8F, -15F, -2F);

		revolverBarrelModel[4].addShapeBox(0F, 0F, 0F, 10, 2, 9, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import pepperboxCylinder2
		revolverBarrelModel[4].setRotationPoint(8F, -17F, -4.5F);

		scopeAttachPoint = new Vector3f(16F /16F, 26F /16F, 0F /16F);

		gunSlideDistance = 0F;
		animationType = EnumAnimationType.REVOLVER;

		revolverFlipAngle = -25F;

		translateAll(0F, 0F, 0F);


		flipAll();
	}
}