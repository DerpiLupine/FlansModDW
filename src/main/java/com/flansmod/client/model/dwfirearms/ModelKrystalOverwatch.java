package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelKrystalOverwatch extends ModelGun
{
	int textureX = 512;
	int textureY = 256;

	public ModelKrystalOverwatch()
	{
		gunModel = new ModelRendererTurbo[47];
		gunModel[0] = new ModelRendererTurbo(this, 56, 61, textureX, textureY); // barrelFront
		gunModel[1] = new ModelRendererTurbo(this, 56, 61, textureX, textureY); // barrelFront2
		gunModel[2] = new ModelRendererTurbo(this, 104, 61, textureX, textureY); // barrelFront3
		gunModel[3] = new ModelRendererTurbo(this, 104, 61, textureX, textureY); // barrelFront3-2
		gunModel[4] = new ModelRendererTurbo(this, 81, 61, textureX, textureY); // barrelFront4
		gunModel[5] = new ModelRendererTurbo(this, 56, 61, textureX, textureY); // barrelFront5
		gunModel[6] = new ModelRendererTurbo(this, 102, 73, textureX, textureY); // barrelFront6
		gunModel[7] = new ModelRendererTurbo(this, 56, 28, textureX, textureY); // barrelRail
		gunModel[8] = new ModelRendererTurbo(this, 112, 46, textureX, textureY); // body1
		gunModel[9] = new ModelRendererTurbo(this, 56, 82, textureX, textureY); // body2
		gunModel[10] = new ModelRendererTurbo(this, 56, 71, textureX, textureY); // body3
		gunModel[11] = new ModelRendererTurbo(this, 83, 98, textureX, textureY); // body4
		gunModel[12] = new ModelRendererTurbo(this, 83, 98, textureX, textureY); // body4-2
		gunModel[13] = new ModelRendererTurbo(this, 83, 91, textureX, textureY); // body5
		gunModel[14] = new ModelRendererTurbo(this, 83, 91, textureX, textureY); // body5-2
		gunModel[15] = new ModelRendererTurbo(this, 56, 45, textureX, textureY); // body6
		gunModel[16] = new ModelRendererTurbo(this, 56, 53, textureX, textureY); // body7
		gunModel[17] = new ModelRendererTurbo(this, 83, 81, textureX, textureY); // body8
		gunModel[18] = new ModelRendererTurbo(this, 79, 71, textureX, textureY); // body9
		gunModel[19] = new ModelRendererTurbo(this, 107, 94, textureX, textureY); // brassPart1
		gunModel[20] = new ModelRendererTurbo(this, 107, 94, textureX, textureY); // brassPart2
		gunModel[21] = new ModelRendererTurbo(this, 56, 32, textureX, textureY); // brassPipe1
		gunModel[22] = new ModelRendererTurbo(this, 56, 32, textureX, textureY); // brassPipe1-2
		gunModel[23] = new ModelRendererTurbo(this, 56, 32, textureX, textureY); // brassPipe1-3
		gunModel[24] = new ModelRendererTurbo(this, 56, 32, textureX, textureY); // brassPipe1-4
		gunModel[25] = new ModelRendererTurbo(this, 1, 88, textureX, textureY); // canister1
		gunModel[26] = new ModelRendererTurbo(this, 1, 61, textureX, textureY); // gasCanister2
		gunModel[27] = new ModelRendererTurbo(this, 1, 70, textureX, textureY); // gasCanister3
		gunModel[28] = new ModelRendererTurbo(this, 1, 79, textureX, textureY); // gasCanister4
		gunModel[29] = new ModelRendererTurbo(this, 56, 36, textureX, textureY); // crane1
		gunModel[30] = new ModelRendererTurbo(this, 56, 36, textureX, textureY); // crane1-2
		gunModel[31] = new ModelRendererTurbo(this, 56, 36, textureX, textureY); // crane1-3
		gunModel[32] = new ModelRendererTurbo(this, 73, 71, textureX, textureY); // hammer1
		gunModel[33] = new ModelRendererTurbo(this, 92, 98, textureX, textureY); // hammer2
		gunModel[34] = new ModelRendererTurbo(this, 36, 36, textureX, textureY); // ironSight1
		gunModel[35] = new ModelRendererTurbo(this, 36, 36, textureX, textureY); // ironSight1-2
		gunModel[36] = new ModelRendererTurbo(this, 36, 32, textureX, textureY); // ironSight2
		gunModel[37] = new ModelRendererTurbo(this, 56, 19, textureX, textureY); // mainBarrelBottom
		gunModel[38] = new ModelRendererTurbo(this, 56, 10, textureX, textureY); // mainBarrelMiddle
		gunModel[39] = new ModelRendererTurbo(this, 56, 1, textureX, textureY); // mainBarrelTop
		gunModel[40] = new ModelRendererTurbo(this, 32, 61, textureX, textureY); // meter1
		gunModel[41] = new ModelRendererTurbo(this, 32, 71, textureX, textureY); // meter1-2
		gunModel[42] = new ModelRendererTurbo(this, 32, 81, textureX, textureY); // meter2
		gunModel[43] = new ModelRendererTurbo(this, 1, 97, textureX, textureY); // pipe1
		gunModel[44] = new ModelRendererTurbo(this, 1, 97, textureX, textureY); // pipe1-2
		gunModel[45] = new ModelRendererTurbo(this, 23, 88, textureX, textureY); // pipe2
		gunModel[46] = new ModelRendererTurbo(this, 12, 88, textureX, textureY); // pipe3

		gunModel[0].addShapeBox(0F, 0F, 0F, 5, 2, 7, 0F,0F, 0F, -0.5F,0F, 0F, -0.5F,0F, 0F, -0.5F,0F, 0F, -0.5F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F); // barrelFront
		gunModel[0].setRotationPoint(1F, -16F, -3.5F);

		gunModel[1].addBox(0F, 0F, 0F, 5, 2, 7, 0F); // barrelFront2
		gunModel[1].setRotationPoint(1F, -14F, -3.5F);

		gunModel[2].addShapeBox(0F, 0F, 0F, 4, 1, 7, 0F,0F, 0.5F, 0F,0F, 0.5F, 0F,0F, 0.5F, 0F,0F, 0.5F, 0F,0F, 0.5F, -2F,0F, 0.5F, -2F,0F, 0.5F, -2F,0F, 0.5F, -2F); // barrelFront3
		gunModel[2].setRotationPoint(2F, -11.5F, -3.5F);

		gunModel[3].addShapeBox(0F, 0F, 0F, 4, 1, 7, 0F,0F, 0.5F, -2F,0F, 0.5F, -2F,0F, 0.5F, -2F,0F, 0.5F, -2F,0F, 0.5F, 0F,0F, 0.5F, 0F,0F, 0.5F, 0F,0F, 0.5F, 0F); // barrelFront3-2
		gunModel[3].setRotationPoint(2F, -9.5F, -3.5F);

		gunModel[4].addBox(0F, 0F, 0F, 4, 2, 7, 0F); // barrelFront4
		gunModel[4].setRotationPoint(2F, -8F, -3.5F);

		gunModel[5].addShapeBox(0F, 0F, 0F, 5, 2, 7, 0F,0F, 0.5F, 0F,0F, 0.5F, 0F,0F, 0.5F, 0F,0F, 0.5F, 0F,0F, 0F, -2F,0F, 0F, -2F,0F, 0F, -2F,0F, 0F, -2F); // barrelFront5
		gunModel[5].setRotationPoint(1F, -5.5F, -3.5F);

		gunModel[6].addShapeBox(0F, 0F, 0F, 5, 1, 6, 0F,0F, 0.5F, -2F,0F, 0.5F, -2F,0F, 0.5F, -2F,0F, 0.5F, -2F,0F, 0.5F, 0F,0F, 0.5F, 0F,0F, 0.5F, 0F,0F, 0.5F, 0F); // barrelFront6
		gunModel[6].setRotationPoint(1F, -4.5F, -3F);

		gunModel[7].addBox(0F, 0F, 0F, 38, 1, 2, 0F); // barrelRail
		gunModel[7].setRotationPoint(6F, -17F, -1F);

		gunModel[8].addBox(0F, 0F, 0F, 1, 6, 8, 0F); // body1
		gunModel[8].setRotationPoint(1F, -12F, -4F);

		gunModel[9].addBox(0F, 0F, 0F, 7, 15, 6, 0F); // body2
		gunModel[9].setRotationPoint(-18F, -15F, -3F);

		gunModel[10].addBox(0F, 0F, 0F, 5, 4, 6, 0F); // body3
		gunModel[10].setRotationPoint(-23F, -8F, -3F);

		gunModel[11].addShapeBox(0F, 0F, 0F, 2, 3, 2, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,3F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,3F, 0F, 0F); // body4
		gunModel[11].setRotationPoint(-20F, -11F, 1F);

		gunModel[12].addShapeBox(0F, 0F, 0F, 2, 3, 2, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,3F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,3F, 0F, 0F); // body4-2
		gunModel[12].setRotationPoint(-20F, -11F, -3F);

		gunModel[13].addShapeBox(0F, 0F, 0F, 1, 4, 2, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,1F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,1F, 0F, 0F); // body5
		gunModel[13].setRotationPoint(-19F, -15F, 1F);

		gunModel[14].addShapeBox(0F, 0F, 0F, 1, 4, 2, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,1F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,1F, 0F, 0F); // body5-2
		gunModel[14].setRotationPoint(-19F, -15F, -3F);

		gunModel[15].addShapeBox(0F, 0F, 0F, 25, 1, 6, 0F,0F, 0F, -1F,0F, 0F, -1F,0F, 0F, -1F,0F, 0F, -1F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F); // body6
		gunModel[15].setRotationPoint(-19F, -17F, -3F);

		gunModel[16].addBox(0F, 0F, 0F, 20, 1, 6, 0F); // body7
		gunModel[16].setRotationPoint(-19F, -16F, -3F);

		gunModel[17].addBox(0F, 0F, 0F, 12, 3, 6, 0F); // body8
		gunModel[17].setRotationPoint(-11F, -3F, -3F);

		gunModel[18].addShapeBox(0F, 0F, 0F, 5, 3, 6, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,-2F, 0F, 0F,-2F, 0F, 0F,0F, 0F, 0F); // body9
		gunModel[18].setRotationPoint(1F, -3F, -3F);

		gunModel[19].addBox(0F, 0F, 0F, 3, 2, 7, 0F); // brassPart1
		gunModel[19].setRotationPoint(11F, -13F, -3.5F);

		gunModel[20].addBox(0F, 0F, 0F, 3, 2, 7, 0F); // brassPart2
		gunModel[20].setRotationPoint(28F, -13F, -3.5F);

		gunModel[21].addShapeBox(0F, 0F, 0F, 36, 1, 2, 0F,0F, 0F, -0.5F,0F, 0F, -0.5F,0F, 0F, -0.5F,0F, 0F, -0.5F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F); // brassPipe1
		gunModel[21].setRotationPoint(2F, -11F, -4F);

		gunModel[22].addShapeBox(0F, 0F, 0F, 36, 1, 2, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, -0.5F,0F, 0F, -0.5F,0F, 0F, -0.5F,0F, 0F, -0.5F); // brassPipe1-2
		gunModel[22].setRotationPoint(2F, -10F, -4F);

		gunModel[23].addShapeBox(0F, 0F, 0F, 36, 1, 2, 0F,0F, 0F, -0.5F,0F, 0F, -0.5F,0F, 0F, -0.5F,0F, 0F, -0.5F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F); // brassPipe1-3
		gunModel[23].setRotationPoint(2F, -11F, 2F);

		gunModel[24].addShapeBox(0F, 0F, 0F, 36, 1, 2, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, -0.5F,0F, 0F, -0.5F,0F, 0F, -0.5F,0F, 0F, -0.5F); // brassPipe1-4
		gunModel[24].setRotationPoint(2F, -10F, 2F);

		gunModel[25].addBox(0F, 0F, 0F, 3, 6, 2, 0F); // canister1
		gunModel[25].setRotationPoint(-18F, -17F, -4.5F);

		gunModel[26].addShapeBox(0F, 0F, 0F, 9, 2, 6, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, -2F,0F, 0F, -2F,0F, 0F, -2F,0F, 0F, -2F); // gasCanister2
		gunModel[26].setRotationPoint(-21F, -13F, -9F);

		gunModel[27].addBox(0F, 0F, 0F, 9, 2, 6, 0F); // gasCanister3
		gunModel[27].setRotationPoint(-21F, -15F, -9F);

		gunModel[28].addShapeBox(0F, 0F, 0F, 9, 2, 6, 0F,0F, 0F, -2F,0F, 0F, -2F,0F, 0F, -2F,0F, 0F, -2F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F); // gasCanister4
		gunModel[28].setRotationPoint(-21F, -17F, -9F);

		gunModel[29].addShapeBox(0F, 0F, 0F, 32, 2, 6, 0F,0F, 0F, -2F,0F, 0F, -2F,0F, 0F, -2F,0F, 0F, -2F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F); // crane1
		gunModel[29].setRotationPoint(6F, -10F, -3F);

		gunModel[30].addBox(0F, 0F, 0F, 32, 2, 6, 0F); // crane1-2
		gunModel[30].setRotationPoint(6F, -8F, -3F);

		gunModel[31].addShapeBox(0F, 0F, 0F, 32, 2, 6, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, -2F,0F, 0F, -2F,0F, 0F, -2F,0F, 0F, -2F); // crane1-3
		gunModel[31].setRotationPoint(6F, -6F, -3F);

		gunModel[32].addBox(0F, 0F, 0F, 2, 3, 2, 0F); // hammer1
		gunModel[32].setRotationPoint(-20F, -11F, -1F);

		gunModel[33].addShapeBox(0F, 0F, 0F, 5, 3, 2, 0F,0F, 1F, 0F,0F, -1F, 0F,0F, -1F, 0F,0F, 1F, 0F,0F, -3F, 0F,0F, 1F, 0F,0F, 1F, 0F,0F, -3F, 0F); // hammer2
		gunModel[33].setRotationPoint(-23F, -12F, -1F);

		gunModel[34].addBox(0F, 0F, 0F, 4, 2, 2, 0F); // ironSight1
		gunModel[34].setRotationPoint(-18.9F, -18F, 0.5F);

		gunModel[35].addBox(0F, 0F, 0F, 4, 2, 2, 0F); // ironSight1-2
		gunModel[35].setRotationPoint(-18.9F, -18F, -2.5F);

		gunModel[36].addShapeBox(0F, 0F, 0F, 4, 2, 1, 0F,0F, 0F, 0F,-1F, 0F, 0F,-1F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F); // ironSight2
		gunModel[36].setRotationPoint(39F, -19F, -0.5F);

		gunModel[37].addShapeBox(0F, 0F, 0F, 38, 2, 6, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, -2F,0F, 0F, -2F,0F, 0F, -2F,0F, 0F, -2F); // mainBarrelBottom
		gunModel[37].setRotationPoint(6F, -12F, -3F);

		gunModel[38].addBox(0F, 0F, 0F, 38, 2, 6, 0F); // mainBarrelMiddle
		gunModel[38].setRotationPoint(6F, -14F, -3F);

		gunModel[39].addShapeBox(0F, 0F, 0F, 38, 2, 6, 0F,0F, 0F, -2F,0F, 0F, -2F,0F, 0F, -2F,0F, 0F, -2F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F); // mainBarrelTop
		gunModel[39].setRotationPoint(6F, -16F, -3F);

		gunModel[40].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, -2F,0F, 0F, -2F,0F, 0F, -2F,0F, 0F, -2F); // meter1
		gunModel[40].setRotationPoint(-16F, -17F, -14F);

		gunModel[41].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F,0F, 0F, -2F,0F, 0F, -2F,0F, 0F, -2F,0F, 0F, -2F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F); // meter1-2
		gunModel[41].setRotationPoint(-16F, -22F, -14F);

		gunModel[42].addBox(0F, 0F, 0F, 2, 3, 7, 0F); // meter2
		gunModel[42].setRotationPoint(-16F, -20F, -14F);

		gunModel[43].addShapeBox(0F, 0F, 0F, 15, 1, 2, 0F,0F, 0F, -0.5F,0F, 0F, -0.5F,0F, 0F, -0.5F,0F, 0F, -0.5F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F); // pipe1
		gunModel[43].setRotationPoint(-12.5F, -15F, -7F);

		gunModel[44].addShapeBox(0F, 0F, 0F, 15, 1, 2, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, -0.5F,0F, 0F, -0.5F,0F, 0F, -0.5F,0F, 0F, -0.5F); // pipe1-2
		gunModel[44].setRotationPoint(-12.5F, -14F, -7F);

		gunModel[45].addBox(0F, 0F, 0F, 2, 2, 4, 0F); // pipe2
		gunModel[45].setRotationPoint(2.5F, -15F, -7F);

		gunModel[46].addBox(0F, 0F, 0F, 4, 4, 1, 0F); // pipe3
		gunModel[46].setRotationPoint(1.5F, -16F, -4F);


		defaultScopeModel = new ModelRendererTurbo[57];
		defaultScopeModel[0] = new ModelRendererTurbo(this, 150, 23, textureX, textureY); // Box 119
		defaultScopeModel[1] = new ModelRendererTurbo(this, 150, 35, textureX, textureY); // Box 120
		defaultScopeModel[2] = new ModelRendererTurbo(this, 150, 29, textureX, textureY); // Box 121
		defaultScopeModel[3] = new ModelRendererTurbo(this, 150, 35, textureX, textureY); // Box 122
		defaultScopeModel[4] = new ModelRendererTurbo(this, 150, 23, textureX, textureY); // Box 123
		defaultScopeModel[5] = new ModelRendererTurbo(this, 150, 29, textureX, textureY); // Box 124
		defaultScopeModel[6] = new ModelRendererTurbo(this, 150, 35, textureX, textureY); // Box 125
		defaultScopeModel[7] = new ModelRendererTurbo(this, 150, 35, textureX, textureY); // Box 126
		defaultScopeModel[8] = new ModelRendererTurbo(this, 175, 23, textureX, textureY); // scopeEnd1-2
		defaultScopeModel[9] = new ModelRendererTurbo(this, 175, 35, textureX, textureY); // scopeEnd3-3
		defaultScopeModel[10] = new ModelRendererTurbo(this, 175, 29, textureX, textureY); // scopeEnd2-2
		defaultScopeModel[11] = new ModelRendererTurbo(this, 175, 35, textureX, textureY); // scopeEnd3-2
		defaultScopeModel[12] = new ModelRendererTurbo(this, 175, 23, textureX, textureY); // scopeEnd1
		defaultScopeModel[13] = new ModelRendererTurbo(this, 175, 35, textureX, textureY); // scopeEnd3
		defaultScopeModel[14] = new ModelRendererTurbo(this, 175, 29, textureX, textureY); // scopeEnd2
		defaultScopeModel[15] = new ModelRendererTurbo(this, 175, 35, textureX, textureY); // scopeEnd3-4
		defaultScopeModel[16] = new ModelRendererTurbo(this, 175, 14, textureX, textureY); // scopeBase2
		defaultScopeModel[17] = new ModelRendererTurbo(this, 175, 5, textureX, textureY); // scopeBase3
		defaultScopeModel[18] = new ModelRendererTurbo(this, 150, 51, textureX, textureY); // knob1
		defaultScopeModel[19] = new ModelRendererTurbo(this, 150, 51, textureX, textureY); // knob2
		defaultScopeModel[20] = new ModelRendererTurbo(this, 150, 51, textureX, textureY); // knob3
		defaultScopeModel[21] = new ModelRendererTurbo(this, 175, 14, textureX, textureY); // scopeBase2-2
		defaultScopeModel[22] = new ModelRendererTurbo(this, 150, 1, textureX, textureY); // scopeBase1
		defaultScopeModel[23] = new ModelRendererTurbo(this, 175, 5, textureX, textureY); // scopeBase3-2
		defaultScopeModel[24] = new ModelRendererTurbo(this, 175, 23, textureX, textureY); // scopeFront1
		defaultScopeModel[25] = new ModelRendererTurbo(this, 175, 23, textureX, textureY); // scopeFront1-2
		defaultScopeModel[26] = new ModelRendererTurbo(this, 175, 29, textureX, textureY); // scopeFront2
		defaultScopeModel[27] = new ModelRendererTurbo(this, 175, 29, textureX, textureY); // scopeFront2-2
		defaultScopeModel[28] = new ModelRendererTurbo(this, 175, 35, textureX, textureY); // scopeFront3
		defaultScopeModel[29] = new ModelRendererTurbo(this, 175, 35, textureX, textureY); // scopeFront3-2
		defaultScopeModel[30] = new ModelRendererTurbo(this, 175, 35, textureX, textureY); // scopeFront3-3
		defaultScopeModel[31] = new ModelRendererTurbo(this, 175, 35, textureX, textureY); // scopeFront3-4
		defaultScopeModel[32] = new ModelRendererTurbo(this, 150, 5, textureX, textureY); // scopeMain1
		defaultScopeModel[33] = new ModelRendererTurbo(this, 150, 5, textureX, textureY); // scopeMain1-2
		defaultScopeModel[34] = new ModelRendererTurbo(this, 150, 11, textureX, textureY); // scopeMain2
		defaultScopeModel[35] = new ModelRendererTurbo(this, 150, 11, textureX, textureY); // scopeMain2-2
		defaultScopeModel[36] = new ModelRendererTurbo(this, 150, 17, textureX, textureY); // scopeMain3
		defaultScopeModel[37] = new ModelRendererTurbo(this, 150, 17, textureX, textureY); // scopeMain3-2
		defaultScopeModel[38] = new ModelRendererTurbo(this, 150, 17, textureX, textureY); // scopeMain3-3
		defaultScopeModel[39] = new ModelRendererTurbo(this, 150, 17, textureX, textureY); // scopeMain3-4
		defaultScopeModel[40] = new ModelRendererTurbo(this, 150, 23, textureX, textureY); // scopeMain4
		defaultScopeModel[41] = new ModelRendererTurbo(this, 150, 23, textureX, textureY); // scopeMain4-2
		defaultScopeModel[42] = new ModelRendererTurbo(this, 150, 29, textureX, textureY); // scopeMain5
		defaultScopeModel[43] = new ModelRendererTurbo(this, 150, 29, textureX, textureY); // scopeMain5-2
		defaultScopeModel[44] = new ModelRendererTurbo(this, 150, 35, textureX, textureY); // scopeMain6
		defaultScopeModel[45] = new ModelRendererTurbo(this, 150, 35, textureX, textureY); // scopeMain6-2
		defaultScopeModel[46] = new ModelRendererTurbo(this, 150, 35, textureX, textureY); // scopeMain6-3
		defaultScopeModel[47] = new ModelRendererTurbo(this, 150, 35, textureX, textureY); // scopeMain6-4
		defaultScopeModel[48] = new ModelRendererTurbo(this, 194, 5, textureX, textureY); // scopeSight1
		defaultScopeModel[49] = new ModelRendererTurbo(this, 194, 5, textureX, textureY); // scopeSight1-2
		defaultScopeModel[50] = new ModelRendererTurbo(this, 203, 5, textureX, textureY); // scopeSight2-1
		defaultScopeModel[51] = new ModelRendererTurbo(this, 150, 46, textureX, textureY); // knob4
		defaultScopeModel[52] = new ModelRendererTurbo(this, 150, 46, textureX, textureY); // Box 146
		defaultScopeModel[53] = new ModelRendererTurbo(this, 150, 46, textureX, textureY); // Box 147
		defaultScopeModel[54] = new ModelRendererTurbo(this, 150, 41, textureX, textureY); // Box 148
		defaultScopeModel[55] = new ModelRendererTurbo(this, 150, 41, textureX, textureY); // Box 149
		defaultScopeModel[56] = new ModelRendererTurbo(this, 150, 41, textureX, textureY); // Box 150

		defaultScopeModel[0].addShapeBox(0F, 0F, 0F, 8, 1, 4, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 1F,0F, 0F, 1F,0F, 0F, 1F,0F, 0F, 1F); // Box 119
		defaultScopeModel[0].setRotationPoint(-10F, -31F, -2F);

		defaultScopeModel[1].addShapeBox(0F, 0F, 0F, 8, 4, 1, 0F,0F, 1F, 0F,0F, 1F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 1F, 0F,0F, 1F, 0F,0F, 0F, 0F,0F, 0F, 0F); // Box 120
		defaultScopeModel[1].setRotationPoint(-10F, -30.3F, 1.3F);
		defaultScopeModel[1].rotateAngleX = 0.78539816F;

		defaultScopeModel[2].addShapeBox(0F, 0F, 0F, 8, 4, 1, 0F,0F, 1F, 0F,0F, 1F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 1F, 0F,0F, 1F, 0F,0F, 0F, 0F,0F, 0F, 0F); // Box 121
		defaultScopeModel[2].setRotationPoint(-10F, -28F, 4F);

		defaultScopeModel[3].addShapeBox(0F, 0F, 0F, 8, 4, 1, 0F,0F, 1F, 0F,0F, 1F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 1F, 0F,0F, 1F, 0F,0F, 0F, 0F,0F, 0F, 0F); // Box 122
		defaultScopeModel[3].setRotationPoint(-10F, -24.7F, 4.3F);
		defaultScopeModel[3].rotateAngleX = -0.78539816F;

		defaultScopeModel[4].addShapeBox(0F, 0F, 0F, 8, 1, 4, 0F,0F, 0F, 1F,0F, 0F, 1F,0F, 0F, 1F,0F, 0F, 1F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F); // Box 123
		defaultScopeModel[4].setRotationPoint(-10F, -22F, -2F);

		defaultScopeModel[5].addShapeBox(0F, 0F, 0F, 8, 4, 1, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 1F, 0F,0F, 1F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 1F, 0F,0F, 1F, 0F); // Box 124
		defaultScopeModel[5].setRotationPoint(-10F, -28F, -5F);

		defaultScopeModel[6].addShapeBox(0F, 0F, 0F, 8, 4, 1, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 1F, 0F,0F, 1F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 1F, 0F,0F, 1F, 0F); // Box 125
		defaultScopeModel[6].setRotationPoint(-10F, -31F, -2.2F);
		defaultScopeModel[6].rotateAngleX = -0.78539816F;

		defaultScopeModel[7].addShapeBox(0F, 0F, 0F, 8, 4, 1, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 1F, 0F,0F, 1F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 1F, 0F,0F, 1F, 0F); // Box 126
		defaultScopeModel[7].setRotationPoint(-10F, -23.8F, -4.7F);
		defaultScopeModel[7].rotateAngleX = 0.78539816F;

		defaultScopeModel[8].addShapeBox(0F, 0F, 0F, 11, 1, 4, 0F,0F, 0F, 1F,0F, 0F, 1F,0F, 0F, 1F,0F, 0F, 1F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F); // scopeEnd1-2
		defaultScopeModel[8].setRotationPoint(-21F, -21.2F, -2F);

		defaultScopeModel[9].addShapeBox(0F, 0F, 0F, 11, 4, 1, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 1F, 0F,0F, 1F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 1F, 0F,0F, 1F, 0F); // scopeEnd3-3
		defaultScopeModel[9].setRotationPoint(-21F, -24F, -5.7F);
		defaultScopeModel[9].rotateAngleX = 0.78539816F;

		defaultScopeModel[10].addShapeBox(0F, 0F, 0F, 11, 4, 1, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 1F, 0F,0F, 1F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 1F, 0F,0F, 1F, 0F); // scopeEnd2-2
		defaultScopeModel[10].setRotationPoint(-21F, -28F, -5.8F);

		defaultScopeModel[11].addShapeBox(0F, 0F, 0F, 11, 4, 1, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 1F, 0F,0F, 1F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 1F, 0F,0F, 1F, 0F); // scopeEnd3-2
		defaultScopeModel[11].setRotationPoint(-21F, -31F, -3F);
		defaultScopeModel[11].rotateAngleX = -0.78539816F;

		defaultScopeModel[12].addShapeBox(0F, 0F, 0F, 11, 1, 4, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 1F,0F, 0F, 1F,0F, 0F, 1F,0F, 0F, 1F); // scopeEnd1
		defaultScopeModel[12].setRotationPoint(-21F, -32F, -2F);

		defaultScopeModel[13].addShapeBox(0F, 0F, 0F, 11, 4, 1, 0F,0F, 1F, 0F,0F, 1F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 1F, 0F,0F, 1F, 0F,0F, 0F, 0F,0F, 0F, 0F); // scopeEnd3
		defaultScopeModel[13].setRotationPoint(-21F, -30.3F, 2.3F);
		defaultScopeModel[13].rotateAngleX = 0.78539816F;

		defaultScopeModel[14].addShapeBox(0F, 0F, 0F, 11, 4, 1, 0F,0F, 1F, 0F,0F, 1F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 1F, 0F,0F, 1F, 0F,0F, 0F, 0F,0F, 0F, 0F); // scopeEnd2
		defaultScopeModel[14].setRotationPoint(-21F, -28F, 4.85F);

		defaultScopeModel[15].addShapeBox(0F, 0F, 0F, 11, 4, 1, 0F,0F, 1F, 0F,0F, 1F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 1F, 0F,0F, 1F, 0F,0F, 0F, 0F,0F, 0F, 0F); // scopeEnd3-4
		defaultScopeModel[15].setRotationPoint(-21F, -23.7F, 4.15F);
		defaultScopeModel[15].rotateAngleX = -0.78539816F;

		defaultScopeModel[16].addBox(0F, 0F, 0F, 7, 4, 4, 0F); // scopeBase2
		defaultScopeModel[16].setRotationPoint(-9.5F, -21F, -2F);

		defaultScopeModel[17].addBox(0F, 0F, 0F, 6, 2, 6, 0F); // scopeBase3
		defaultScopeModel[17].setRotationPoint(-9F, -19F, -3F);

		defaultScopeModel[18].addShapeBox(0F, 0F, 0F, 2, 2, 6, 0F,0F, 0F, -2F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, -2F,0F, 0F, -2F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, -2F); // knob1
		defaultScopeModel[18].setRotationPoint(-1F, -33.1F, -3F);

		defaultScopeModel[19].addBox(0F, 0F, 0F, 2, 2, 6, 0F); // knob2
		defaultScopeModel[19].setRotationPoint(1F, -33.1F, -3F);

		defaultScopeModel[20].addShapeBox(0F, 0F, 0F, 2, 2, 6, 0F,0F, 0F, 0F,0F, 0F, -2F,0F, 0F, -2F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, -2F,0F, 0F, -2F,0F, 0F, 0F); // knob3
		defaultScopeModel[20].setRotationPoint(3F, -33.1F, -3F);

		defaultScopeModel[21].addBox(0F, 0F, 0F, 7, 4, 4, 0F); // scopeBase2-2
		defaultScopeModel[21].setRotationPoint(6.5F, -21F, -2F);

		defaultScopeModel[22].addBox(0F, 0F, 0F, 26, 1, 2, 0F); // scopeBase1
		defaultScopeModel[22].setRotationPoint(-11F, -18F, -1F);

		defaultScopeModel[23].addBox(0F, 0F, 0F, 6, 2, 6, 0F); // scopeBase3-2
		defaultScopeModel[23].setRotationPoint(7F, -19F, -3F);

		defaultScopeModel[24].addShapeBox(0F, 0F, 0F, 11, 1, 4, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 1F,0F, 0F, 1F,0F, 0F, 1F,0F, 0F, 1F); // scopeFront1
		defaultScopeModel[24].setRotationPoint(14F, -32F, -2F);

		defaultScopeModel[25].addShapeBox(0F, 0F, 0F, 11, 1, 4, 0F,0F, 0F, 1F,0F, 0F, 1F,0F, 0F, 1F,0F, 0F, 1F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F); // scopeFront1-2
		defaultScopeModel[25].setRotationPoint(14F, -21.2F, -2F);

		defaultScopeModel[26].addShapeBox(0F, 0F, 0F, 11, 4, 1, 0F,0F, 1F, 0F,0F, 1F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 1F, 0F,0F, 1F, 0F,0F, 0F, 0F,0F, 0F, 0F); // scopeFront2
		defaultScopeModel[26].setRotationPoint(14F, -28F, 4.85F);

		defaultScopeModel[27].addShapeBox(0F, 0F, 0F, 11, 4, 1, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 1F, 0F,0F, 1F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 1F, 0F,0F, 1F, 0F); // scopeFront2-2
		defaultScopeModel[27].setRotationPoint(14F, -28F, -5.8F);

		defaultScopeModel[28].addShapeBox(0F, 0F, 0F, 11, 4, 1, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 1F, 0F,0F, 1F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 1F, 0F,0F, 1F, 0F); // scopeFront3
		defaultScopeModel[28].setRotationPoint(14F, -31F, -3F);
		defaultScopeModel[28].rotateAngleX = -0.78539816F;

		defaultScopeModel[29].addShapeBox(0F, 0F, 0F, 11, 4, 1, 0F,0F, 1F, 0F,0F, 1F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 1F, 0F,0F, 1F, 0F,0F, 0F, 0F,0F, 0F, 0F); // scopeFront3-2
		defaultScopeModel[29].setRotationPoint(14F, -30.3F, 2.3F);
		defaultScopeModel[29].rotateAngleX = 0.78539816F;

		defaultScopeModel[30].addShapeBox(0F, 0F, 0F, 11, 4, 1, 0F,0F, 1F, 0F,0F, 1F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 1F, 0F,0F, 1F, 0F,0F, 0F, 0F,0F, 0F, 0F); // scopeFront3-3
		defaultScopeModel[30].setRotationPoint(14F, -23.7F, 4.15F);
		defaultScopeModel[30].rotateAngleX = -0.78539816F;

		defaultScopeModel[31].addShapeBox(0F, 0F, 0F, 11, 4, 1, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 1F, 0F,0F, 1F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 1F, 0F,0F, 1F, 0F); // scopeFront3-4
		defaultScopeModel[31].setRotationPoint(14F, -24F, -5.7F);
		defaultScopeModel[31].rotateAngleX = 0.78539816F;

		defaultScopeModel[32].addShapeBox(0F, 0F, 0F, 8, 1, 4, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 1F,0F, 0F, 1F,0F, 0F, 1F,0F, 0F, 1F); // scopeMain1
		defaultScopeModel[32].setRotationPoint(-2F, -32F, -2F);

		defaultScopeModel[33].addShapeBox(0F, 0F, 0F, 8, 1, 4, 0F,0F, 0F, 1F,0F, 0F, 1F,0F, 0F, 1F,0F, 0F, 1F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F); // scopeMain1-2
		defaultScopeModel[33].setRotationPoint(-2F, -21.2F, -2F);

		defaultScopeModel[34].addShapeBox(0F, 0F, 0F, 8, 4, 1, 0F,0F, 1F, 0F,0F, 1F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 1F, 0F,0F, 1F, 0F,0F, 0F, 0F,0F, 0F, 0F); // scopeMain2
		defaultScopeModel[34].setRotationPoint(-2F, -28F, 4.85F);

		defaultScopeModel[35].addShapeBox(0F, 0F, 0F, 8, 4, 1, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 1F, 0F,0F, 1F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 1F, 0F,0F, 1F, 0F); // scopeMain2-2
		defaultScopeModel[35].setRotationPoint(-2F, -28F, -5.8F);

		defaultScopeModel[36].addShapeBox(0F, 0F, 0F, 8, 4, 1, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 1F, 0F,0F, 1F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 1F, 0F,0F, 1F, 0F); // scopeMain3
		defaultScopeModel[36].setRotationPoint(-2F, -31F, -3F);
		defaultScopeModel[36].rotateAngleX = -0.78539816F;

		defaultScopeModel[37].addShapeBox(0F, 0F, 0F, 8, 4, 1, 0F,0F, 1F, 0F,0F, 1F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 1F, 0F,0F, 1F, 0F,0F, 0F, 0F,0F, 0F, 0F); // scopeMain3-2
		defaultScopeModel[37].setRotationPoint(-2F, -30.3F, 2.3F);
		defaultScopeModel[37].rotateAngleX = 0.78539816F;

		defaultScopeModel[38].addShapeBox(0F, 0F, 0F, 8, 4, 1, 0F,0F, 1F, 0F,0F, 1F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 1F, 0F,0F, 1F, 0F,0F, 0F, 0F,0F, 0F, 0F); // scopeMain3-3
		defaultScopeModel[38].setRotationPoint(-2F, -23.7F, 4.15F);
		defaultScopeModel[38].rotateAngleX = -0.78539816F;

		defaultScopeModel[39].addShapeBox(0F, 0F, 0F, 8, 4, 1, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 1F, 0F,0F, 1F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 1F, 0F,0F, 1F, 0F); // scopeMain3-4
		defaultScopeModel[39].setRotationPoint(-2F, -24F, -5.7F);
		defaultScopeModel[39].rotateAngleX = 0.78539816F;

		defaultScopeModel[40].addShapeBox(0F, 0F, 0F, 8, 1, 4, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 1F,0F, 0F, 1F,0F, 0F, 1F,0F, 0F, 1F); // scopeMain4
		defaultScopeModel[40].setRotationPoint(6F, -31F, -2F);

		defaultScopeModel[41].addShapeBox(0F, 0F, 0F, 8, 1, 4, 0F,0F, 0F, 1F,0F, 0F, 1F,0F, 0F, 1F,0F, 0F, 1F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F); // scopeMain4-2
		defaultScopeModel[41].setRotationPoint(6F, -22F, -2F);

		defaultScopeModel[42].addShapeBox(0F, 0F, 0F, 8, 4, 1, 0F,0F, 1F, 0F,0F, 1F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 1F, 0F,0F, 1F, 0F,0F, 0F, 0F,0F, 0F, 0F); // scopeMain5
		defaultScopeModel[42].setRotationPoint(6F, -28F, 4F);

		defaultScopeModel[43].addShapeBox(0F, 0F, 0F, 8, 4, 1, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 1F, 0F,0F, 1F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 1F, 0F,0F, 1F, 0F); // scopeMain5-2
		defaultScopeModel[43].setRotationPoint(6F, -28F, -5F);

		defaultScopeModel[44].addShapeBox(0F, 0F, 0F, 8, 4, 1, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 1F, 0F,0F, 1F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 1F, 0F,0F, 1F, 0F); // scopeMain6
		defaultScopeModel[44].setRotationPoint(6F, -31F, -2.2F);
		defaultScopeModel[44].rotateAngleX = -0.78539816F;

		defaultScopeModel[45].addShapeBox(0F, 0F, 0F, 8, 4, 1, 0F,0F, 1F, 0F,0F, 1F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 1F, 0F,0F, 1F, 0F,0F, 0F, 0F,0F, 0F, 0F); // scopeMain6-2
		defaultScopeModel[45].setRotationPoint(6F, -30.3F, 1.3F);
		defaultScopeModel[45].rotateAngleX = 0.78539816F;

		defaultScopeModel[46].addShapeBox(0F, 0F, 0F, 8, 4, 1, 0F,0F, 1F, 0F,0F, 1F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 1F, 0F,0F, 1F, 0F,0F, 0F, 0F,0F, 0F, 0F); // scopeMain6-3
		defaultScopeModel[46].setRotationPoint(6F, -24.7F, 4.3F);
		defaultScopeModel[46].rotateAngleX = -0.78539816F;

		defaultScopeModel[47].addShapeBox(0F, 0F, 0F, 8, 4, 1, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 1F, 0F,0F, 1F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 1F, 0F,0F, 1F, 0F); // scopeMain6-4
		defaultScopeModel[47].setRotationPoint(6F, -23.8F, -4.7F);
		defaultScopeModel[47].rotateAngleX = 0.78539816F;

		defaultScopeModel[48].addBox(0F, 0F, 0F, 1, 1, 3, 0F); // scopeSight1
		defaultScopeModel[48].setRotationPoint(-9F, -26.5F, 1F);

		defaultScopeModel[49].addBox(0F, 0F, 0F, 1, 1, 3, 0F); // scopeSight1-2
		defaultScopeModel[49].setRotationPoint(-9F, -26.5F, -4F);

		defaultScopeModel[50].addBox(0F, 0F, 0F, 1, 3, 1, 0F); // scopeSight2-1
		defaultScopeModel[50].setRotationPoint(-9F, -25F, -0.5F);

		defaultScopeModel[51].addShapeBox(0F, 0F, 0F, 6, 2, 2, 0F,-2F, 0F, 0F,-2F, 0F, 0F,-2F, 0F, 0F,-2F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F); // knob4
		defaultScopeModel[51].setRotationPoint(-1F, -29F, -7.1F);

		defaultScopeModel[52].addBox(0F, 0F, 0F, 6, 2, 2, 0F); // Box 146
		defaultScopeModel[52].setRotationPoint(-1F, -27F, -7.1F);

		defaultScopeModel[53].addShapeBox(0F, 0F, 0F, 6, 2, 2, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,-2F, 0F, 0F,-2F, 0F, 0F,-2F, 0F, 0F,-2F, 0F, 0F); // Box 147
		defaultScopeModel[53].setRotationPoint(-1F, -25F, -7.1F);

		defaultScopeModel[54].addShapeBox(0F, 0F, 0F, 6, 2, 2, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,-2F, 0F, 0F,-2F, 0F, 0F,-2F, 0F, 0F,-2F, 0F, 0F); // Box 148
		defaultScopeModel[54].setRotationPoint(-1F, -25F, 5.1F);

		defaultScopeModel[55].addShapeBox(0F, 0F, 0F, 6, 2, 2, 0F,-2F, 0F, 0F,-2F, 0F, 0F,-2F, 0F, 0F,-2F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F); // Box 149
		defaultScopeModel[55].setRotationPoint(-1F, -29F, 5.1F);

		defaultScopeModel[56].addBox(0F, 0F, 0F, 6, 2, 2, 0F); // Box 150
		defaultScopeModel[56].setRotationPoint(-1F, -27F, 5.1F);


		defaultGripModel = new ModelRendererTurbo[8];
		defaultGripModel[0] = new ModelRendererTurbo(this, 33, 100, textureX, textureY); // grip1
		defaultGripModel[1] = new ModelRendererTurbo(this, 1, 146, textureX, textureY); // grip2
		defaultGripModel[2] = new ModelRendererTurbo(this, 1, 125, textureX, textureY); // grip3
		defaultGripModel[3] = new ModelRendererTurbo(this, 1, 136, textureX, textureY); // grip4
		defaultGripModel[4] = new ModelRendererTurbo(this, 1, 104, textureX, textureY); // grip5
		defaultGripModel[5] = new ModelRendererTurbo(this, 1, 181, textureX, textureY); // gripPlate1
		defaultGripModel[6] = new ModelRendererTurbo(this, 1, 193, textureX, textureY); // gripPlate2
		defaultGripModel[7] = new ModelRendererTurbo(this, 1, 160, textureX, textureY); // gripPlate3

		defaultGripModel[0].addBox(0F, 0F, 0F, 4, 3, 7, 0F); // grip1
		defaultGripModel[0].setRotationPoint(-23.5F, -7F, -3.5F);

		defaultGripModel[1].addShapeBox(0F, 0F, 0F, 10, 4, 7, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,4F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,4F, 0F, 0F); // grip2
		defaultGripModel[1].setRotationPoint(-23.5F, -4F, -3.5F);

		defaultGripModel[2].addShapeBox(0F, 0F, 0F, 14, 3, 7, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,2F, 0F, 0F,-4F, 0F, 0F,-4F, 0F, 0F,2F, 0F, 0F); // grip3
		defaultGripModel[2].setRotationPoint(-27.5F, 0F, -3.5F);

		defaultGripModel[3].addShapeBox(0F, 0F, 0F, 13, 2, 7, 0F,-1F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,-1F, 0F, 0F,0F, 0F, 0F,-1F, 0F, 0F,-1F, 0F, 0F,0F, 0F, 0F); // grip4
		defaultGripModel[3].setRotationPoint(-30.5F, 3F, -3.5F);

		defaultGripModel[4].addShapeBox(0F, 0F, 0F, 12, 13, 7, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,2F, 0F, 0F,-2F, 0F, 0F,-2F, 0F, 0F,2F, 0F, 0F); // grip5
		defaultGripModel[4].setRotationPoint(-30.5F, 5F, -3.5F);

		defaultGripModel[5].addShapeBox(0F, 0F, 0F, 13, 3, 8, 0F,0F, 0F, 0F,-13F, 0F, 0F,-13F, 0F, 0F,0F, 0F, 0F,2F, 0F, 0F,-4F, 0F, 0F,-4F, 0F, 0F,2F, 0F, 0F); // gripPlate1
		defaultGripModel[5].setRotationPoint(-27F, 0F, -4F);

		defaultGripModel[6].addShapeBox(0F, 0F, 0F, 12, 2, 8, 0F,-1F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,-1F, 0F, 0F,0F, 0F, 0F,-1F, 0F, 0F,-1F, 0F, 0F,0F, 0F, 0F); // gripPlate2
		defaultGripModel[6].setRotationPoint(-30F, 3F, -4F);

		defaultGripModel[7].addShapeBox(0F, 0F, 0F, 11, 12, 8, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,2F, 0F, 0F,-2F, 0F, 0F,-2F, 0F, 0F,2F, 0F, 0F); // gripPlate3
		defaultGripModel[7].setRotationPoint(-30F, 5F, -4F);


		ammoModel = new ModelRendererTurbo[18];
		ammoModel[0] = new ModelRendererTurbo(this, 1, 46, textureX, textureY); // bullet1
		ammoModel[1] = new ModelRendererTurbo(this, 1, 46, textureX, textureY); // bullet1-2
		ammoModel[2] = new ModelRendererTurbo(this, 1, 46, textureX, textureY); // bullet1-3
		ammoModel[3] = new ModelRendererTurbo(this, 26, 46, textureX, textureY); // bullet2
		ammoModel[4] = new ModelRendererTurbo(this, 26, 46, textureX, textureY); // bullet2-2
		ammoModel[5] = new ModelRendererTurbo(this, 26, 46, textureX, textureY); // bullet2-3
		ammoModel[6] = new ModelRendererTurbo(this, 1, 51, textureX, textureY); // bullet3
		ammoModel[7] = new ModelRendererTurbo(this, 1, 51, textureX, textureY); // bullet3-2
		ammoModel[8] = new ModelRendererTurbo(this, 1, 51, textureX, textureY); // bullet3-3
		ammoModel[9] = new ModelRendererTurbo(this, 26, 51, textureX, textureY); // bullet4
		ammoModel[10] = new ModelRendererTurbo(this, 26, 51, textureX, textureY); // bullet4-2
		ammoModel[11] = new ModelRendererTurbo(this, 26, 51, textureX, textureY); // bullet4-3
		ammoModel[12] = new ModelRendererTurbo(this, 1, 56, textureX, textureY); // bullet5
		ammoModel[13] = new ModelRendererTurbo(this, 1, 56, textureX, textureY); // bullet5-2
		ammoModel[14] = new ModelRendererTurbo(this, 1, 56, textureX, textureY); // bullet5-3
		ammoModel[15] = new ModelRendererTurbo(this, 26, 56, textureX, textureY); // bullet6
		ammoModel[16] = new ModelRendererTurbo(this, 26, 56, textureX, textureY); // bullet6-2
		ammoModel[17] = new ModelRendererTurbo(this, 26, 56, textureX, textureY); // bullet6-3

		ammoModel[0].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F,0F, 0F, -1F,0F, 0F, -1F,0F, 0F, -1F,0F, 0F, -1F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F); // bullet1
		ammoModel[0].setRotationPoint(-10.7F, -14.5F, -1.5F);

		ammoModel[1].addBox(0F, 0F, 0F, 9, 1, 3, 0F); // bullet1-2
		ammoModel[1].setRotationPoint(-10.7F, -13.5F, -1.5F);

		ammoModel[2].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, -1F,0F, 0F, -1F,0F, 0F, -1F,0F, 0F, -1F); // bullet1-3
		ammoModel[2].setRotationPoint(-10.7F, -12.5F, -1.5F);

		ammoModel[3].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F,0F, 0F, -1F,0F, 0F, -1F,0F, 0F, -1F,0F, 0F, -1F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F); // bullet2
		ammoModel[3].setRotationPoint(-10.7F, -12.5F, -5F);

		ammoModel[4].addBox(0F, 0F, 0F, 9, 1, 3, 0F); // bullet2-2
		ammoModel[4].setRotationPoint(-10.7F, -11.5F, -5F);

		ammoModel[5].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, -1F,0F, 0F, -1F,0F, 0F, -1F,0F, 0F, -1F); // bullet2-3
		ammoModel[5].setRotationPoint(-10.7F, -10.5F, -5F);

		ammoModel[6].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F,0F, 0F, -1F,0F, 0F, -1F,0F, 0F, -1F,0F, 0F, -1F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F); // bullet3
		ammoModel[6].setRotationPoint(-10.7F, -8.5F, -5F);

		ammoModel[7].addBox(0F, 0F, 0F, 9, 1, 3, 0F); // bullet3-2
		ammoModel[7].setRotationPoint(-10.7F, -7.5F, -5F);

		ammoModel[8].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, -1F,0F, 0F, -1F,0F, 0F, -1F,0F, 0F, -1F); // bullet3-3
		ammoModel[8].setRotationPoint(-10.7F, -6.5F, -5F);

		ammoModel[9].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F,0F, 0F, -1F,0F, 0F, -1F,0F, 0F, -1F,0F, 0F, -1F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F); // bullet4
		ammoModel[9].setRotationPoint(-10.7F, -6.5F, -1.5F);

		ammoModel[10].addBox(0F, 0F, 0F, 9, 1, 3, 0F); // bullet4-2
		ammoModel[10].setRotationPoint(-10.7F, -5.5F, -1.5F);

		ammoModel[11].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, -1F,0F, 0F, -1F,0F, 0F, -1F,0F, 0F, -1F); // bullet4-3
		ammoModel[11].setRotationPoint(-10.7F, -4.5F, -1.5F);

		ammoModel[12].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F,0F, 0F, -1F,0F, 0F, -1F,0F, 0F, -1F,0F, 0F, -1F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F); // bullet5
		ammoModel[12].setRotationPoint(-10.7F, -8.5F, 2F);

		ammoModel[13].addBox(0F, 0F, 0F, 9, 1, 3, 0F); // bullet5-2
		ammoModel[13].setRotationPoint(-10.7F, -7.5F, 2F);

		ammoModel[14].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, -1F,0F, 0F, -1F,0F, 0F, -1F,0F, 0F, -1F); // bullet5-3
		ammoModel[14].setRotationPoint(-10.7F, -6.5F, 2F);

		ammoModel[15].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F,0F, 0F, -1F,0F, 0F, -1F,0F, 0F, -1F,0F, 0F, -1F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F); // bullet6
		ammoModel[15].setRotationPoint(-10.7F, -12.5F, 2F);

		ammoModel[16].addBox(0F, 0F, 0F, 9, 1, 3, 0F); // bullet6-2
		ammoModel[16].setRotationPoint(-10.7F, -11.5F, 2F);

		ammoModel[17].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, -1F,0F, 0F, -1F,0F, 0F, -1F,0F, 0F, -1F); // bullet6-3
		ammoModel[17].setRotationPoint(-10.7F, -10.5F, 2F);


		revolverBarrelModel = new ModelRendererTurbo[5];
		revolverBarrelModel[0] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // canister1
		revolverBarrelModel[1] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // canister1-2
		revolverBarrelModel[2] = new ModelRendererTurbo(this, 1, 14, textureX, textureY); // canister2
		revolverBarrelModel[3] = new ModelRendererTurbo(this, 1, 14, textureX, textureY); // canister2-2
		revolverBarrelModel[4] = new ModelRendererTurbo(this, 1, 29, textureX, textureY); // canister3

		revolverBarrelModel[0].addShapeBox(0F, 0F, 0F, 11, 2, 10, 0F,0F, 0F, -3F,0F, 0F, -3F,0F, 0F, -3F,0F, 0F, -3F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F); // canister1
		revolverBarrelModel[0].setRotationPoint(-10.5F, -15F, -5F);

		revolverBarrelModel[1].addShapeBox(0F, 0F, 0F, 11, 2, 10, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, -3F,0F, 0F, -3F,0F, 0F, -3F,0F, 0F, -3F); // canister1-2
		revolverBarrelModel[1].setRotationPoint(-10.5F, -5F, -5F);

		revolverBarrelModel[2].addShapeBox(0F, 0F, 0F, 11, 2, 12, 0F,0F, 0F, -1F,0F, 0F, -1F,0F, 0F, -1F,0F, 0F, -1F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F); // canister2
		revolverBarrelModel[2].setRotationPoint(-10.5F, -13F, -6F);

		revolverBarrelModel[3].addShapeBox(0F, 0F, 0F, 11, 2, 12, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, -1F,0F, 0F, -1F,0F, 0F, -1F,0F, 0F, -1F); // canister2-2
		revolverBarrelModel[3].setRotationPoint(-10.5F, -7F, -6F);

		revolverBarrelModel[4].addBox(0F, 0F, 0F, 11, 4, 12, 0F); // canister3
		revolverBarrelModel[4].setRotationPoint(-10.5F, -11F, -6F);


		barrelAttachPoint = new Vector3f(0F /16F, 0F /16F, 0F /16F);
		stockAttachPoint = new Vector3f(0F /16F, 0F /16F, 0F /16F);
		scopeAttachPoint = new Vector3f(0F /16F, 0F /16F, 0F /16F);
		gripAttachPoint = new Vector3f(0 /16F, 0F /16F, 0F /16F);

		gunSlideDistance = 0F;
		animationType = EnumAnimationType.REVOLVER;

		revolverFlipAngle = -20F;


		translateAll(16F, -6F, 0F);


		flipAll();
	}
}