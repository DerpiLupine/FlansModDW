package com.flansmod.client.model.dwfirearms; //Path where the model is located

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelKAT3 extends ModelGun //Same as Filename
{
	int textureX = 512;
	int textureY = 256;

	public ModelKAT3() //Same as Filename
	{
		gunModel = new ModelRendererTurbo[116];
		gunModel[0] = new ModelRendererTurbo(this, 100, 15, textureX, textureY); // Box 1
		gunModel[1] = new ModelRendererTurbo(this, 135, 60, textureX, textureY); // Box 3
		gunModel[2] = new ModelRendererTurbo(this, 100, 60, textureX, textureY); // Box 5
		gunModel[3] = new ModelRendererTurbo(this, 148, 41, textureX, textureY); // Box 7
		gunModel[4] = new ModelRendererTurbo(this, 100, 94, textureX, textureY); // Box 8
		gunModel[5] = new ModelRendererTurbo(this, 125, 43, textureX, textureY); // Box 9
		gunModel[6] = new ModelRendererTurbo(this, 1, 56, textureX, textureY); // Box 10
		gunModel[7] = new ModelRendererTurbo(this, 40, 56, textureX, textureY); // Box 11
		gunModel[8] = new ModelRendererTurbo(this, 1, 67, textureX, textureY); // Box 14
		gunModel[9] = new ModelRendererTurbo(this, 30, 67, textureX, textureY); // Box 15
		gunModel[10] = new ModelRendererTurbo(this, 1, 44, textureX, textureY); // Box 16
		gunModel[11] = new ModelRendererTurbo(this, 100, 76, textureX, textureY); // Box 18
		gunModel[12] = new ModelRendererTurbo(this, 163, 102, textureX, textureY); // Box 19
		gunModel[13] = new ModelRendererTurbo(this, 177, 40, textureX, textureY); // Box 20
		gunModel[14] = new ModelRendererTurbo(this, 100, 84, textureX, textureY); // Box 22
		gunModel[15] = new ModelRendererTurbo(this, 100, 27, textureX, textureY); // Box 23
		gunModel[16] = new ModelRendererTurbo(this, 158, 61, textureX, textureY); // Box 33
		gunModel[17] = new ModelRendererTurbo(this, 177, 40, textureX, textureY); // Box 34
		gunModel[18] = new ModelRendererTurbo(this, 145, 102, textureX, textureY); // Box 38
		gunModel[19] = new ModelRendererTurbo(this, 150, 107, textureX, textureY); // Box 39
		gunModel[20] = new ModelRendererTurbo(this, 150, 102, textureX, textureY); // Box 40
		gunModel[21] = new ModelRendererTurbo(this, 145, 102, textureX, textureY); // Box 41
		gunModel[22] = new ModelRendererTurbo(this, 150, 102, textureX, textureY); // Box 42
		gunModel[23] = new ModelRendererTurbo(this, 150, 107, textureX, textureY); // Box 43
		gunModel[24] = new ModelRendererTurbo(this, 145, 102, textureX, textureY); // Box 44
		gunModel[25] = new ModelRendererTurbo(this, 150, 102, textureX, textureY); // Box 45
		gunModel[26] = new ModelRendererTurbo(this, 150, 107, textureX, textureY); // Box 46
		gunModel[27] = new ModelRendererTurbo(this, 150, 99, textureX, textureY); // Box 47
		gunModel[28] = new ModelRendererTurbo(this, 150, 99, textureX, textureY); // Box 48
		gunModel[29] = new ModelRendererTurbo(this, 150, 99, textureX, textureY); // Box 49
		gunModel[30] = new ModelRendererTurbo(this, 145, 94, textureX, textureY); // Box 50
		gunModel[31] = new ModelRendererTurbo(this, 150, 94, textureX, textureY); // Box 51
		gunModel[32] = new ModelRendererTurbo(this, 150, 94, textureX, textureY); // Box 52
		gunModel[33] = new ModelRendererTurbo(this, 145, 94, textureX, textureY); // Box 53
		gunModel[34] = new ModelRendererTurbo(this, 150, 94, textureX, textureY); // Box 54
		gunModel[35] = new ModelRendererTurbo(this, 145, 94, textureX, textureY); // Box 55
		gunModel[36] = new ModelRendererTurbo(this, 163, 94, textureX, textureY); // Box 56
		gunModel[37] = new ModelRendererTurbo(this, 100, 2, textureX, textureY); // Box 57
		gunModel[38] = new ModelRendererTurbo(this, 100, 8, textureX, textureY); // Box 58
		gunModel[39] = new ModelRendererTurbo(this, 100, 2, textureX, textureY); // Box 60
		gunModel[40] = new ModelRendererTurbo(this, 199, 2, textureX, textureY); // Box 61
		gunModel[41] = new ModelRendererTurbo(this, 206, 10, textureX, textureY); // Box 62
		gunModel[42] = new ModelRendererTurbo(this, 199, 14, textureX, textureY); // Box 63
		gunModel[43] = new ModelRendererTurbo(this, 230, 2, textureX, textureY); // Box 64
		gunModel[44] = new ModelRendererTurbo(this, 230, 10, textureX, textureY); // Box 65
		gunModel[45] = new ModelRendererTurbo(this, 230, 2, textureX, textureY); // Box 66
		gunModel[46] = new ModelRendererTurbo(this, 1, 34, textureX, textureY); // Box 67
		gunModel[47] = new ModelRendererTurbo(this, 22, 1, textureX, textureY); // Box 68
		gunModel[48] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 69
		gunModel[49] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 70
		gunModel[50] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 71
		gunModel[51] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 72
		gunModel[52] = new ModelRendererTurbo(this, 100, 110, textureX, textureY); // Box 76
		gunModel[53] = new ModelRendererTurbo(this, 141, 111, textureX, textureY); // Box 77
		gunModel[54] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 81
		gunModel[55] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 82
		gunModel[56] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 83
		gunModel[57] = new ModelRendererTurbo(this, 99, 156, textureX, textureY); // stock1
		gunModel[58] = new ModelRendererTurbo(this, 99, 122, textureX, textureY); // stock3
		gunModel[59] = new ModelRendererTurbo(this, 153, 122, textureX, textureY); // stockPipe1
		gunModel[60] = new ModelRendererTurbo(this, 153, 132, textureX, textureY); // stockPipe2
		gunModel[61] = new ModelRendererTurbo(this, 153, 145, textureX, textureY); // Box 84
		gunModel[62] = new ModelRendererTurbo(this, 100, 41, textureX, textureY); // Box 89
		gunModel[63] = new ModelRendererTurbo(this, 1, 95, textureX, textureY); // Box 90
		gunModel[64] = new ModelRendererTurbo(this, 72, 1, textureX, textureY); // Box 91
		gunModel[65] = new ModelRendererTurbo(this, 72, 1, textureX, textureY); // Box 92
		gunModel[66] = new ModelRendererTurbo(this, 75, 56, textureX, textureY); // Box 0
		gunModel[67] = new ModelRendererTurbo(this, 75, 56, textureX, textureY); // Box 1
		gunModel[68] = new ModelRendererTurbo(this, 47, 69, textureX, textureY); // Box 2
		gunModel[69] = new ModelRendererTurbo(this, 64, 73, textureX, textureY); // Box 3
		gunModel[70] = new ModelRendererTurbo(this, 1, 84, textureX, textureY); // Box 4
		gunModel[71] = new ModelRendererTurbo(this, 64, 73, textureX, textureY); // Box 5
		gunModel[72] = new ModelRendererTurbo(this, 230, 2, textureX, textureY); // Box 7
		gunModel[73] = new ModelRendererTurbo(this, 230, 10, textureX, textureY); // Box 8
		gunModel[74] = new ModelRendererTurbo(this, 230, 2, textureX, textureY); // Box 9
		gunModel[75] = new ModelRendererTurbo(this, 177, 34, textureX, textureY); // Box 10
		gunModel[76] = new ModelRendererTurbo(this, 177, 27, textureX, textureY); // Box 11
		gunModel[77] = new ModelRendererTurbo(this, 177, 34, textureX, textureY); // Box 12
		gunModel[78] = new ModelRendererTurbo(this, 150, 116, textureX, textureY); // Box 13
		gunModel[79] = new ModelRendererTurbo(this, 150, 116, textureX, textureY); // Box 14
		gunModel[80] = new ModelRendererTurbo(this, 141, 116, textureX, textureY); // Box 15
		gunModel[81] = new ModelRendererTurbo(this, 141, 116, textureX, textureY); // Box 16
		gunModel[82] = new ModelRendererTurbo(this, 205, 113, textureX, textureY); // Box 17
		gunModel[83] = new ModelRendererTurbo(this, 191, 116, textureX, textureY); // Box 18
		gunModel[84] = new ModelRendererTurbo(this, 191, 116, textureX, textureY); // Box 19
		gunModel[85] = new ModelRendererTurbo(this, 99, 171, textureX, textureY); // Box 20
		gunModel[86] = new ModelRendererTurbo(this, 172, 157, textureX, textureY); // Box 21
		gunModel[87] = new ModelRendererTurbo(this, 236, 149, textureX, textureY); // Box 22
		gunModel[88] = new ModelRendererTurbo(this, 99, 134, textureX, textureY); // Box 23
		gunModel[89] = new ModelRendererTurbo(this, 130, 133, textureX, textureY); // Box 24
		gunModel[90] = new ModelRendererTurbo(this, 236, 162, textureX, textureY); // Box 25
		gunModel[91] = new ModelRendererTurbo(this, 126, 122, textureX, textureY); // Box 26
		gunModel[92] = new ModelRendererTurbo(this, 144, 167, textureX, textureY); // Box 0
		gunModel[93] = new ModelRendererTurbo(this, 167, 171, textureX, textureY); // Box 1
		gunModel[94] = new ModelRendererTurbo(this, 213, 155, textureX, textureY); // Box 32
		gunModel[95] = new ModelRendererTurbo(this, 190, 168, textureX, textureY); // Box 33
		gunModel[96] = new ModelRendererTurbo(this, 230, 122, textureX, textureY); // Box 34
		gunModel[97] = new ModelRendererTurbo(this, 230, 135, textureX, textureY); // Box 35
		gunModel[98] = new ModelRendererTurbo(this, 199, 10, textureX, textureY); // Box 36
		gunModel[99] = new ModelRendererTurbo(this, 199, 10, textureX, textureY); // Box 37
		gunModel[100] = new ModelRendererTurbo(this, 206, 10, textureX, textureY); // Box 38
		gunModel[101] = new ModelRendererTurbo(this, 199, 10, textureX, textureY); // Box 39
		gunModel[102] = new ModelRendererTurbo(this, 206, 10, textureX, textureY); // Box 40
		gunModel[103] = new ModelRendererTurbo(this, 205, 113, textureX, textureY); // Box 49
		gunModel[104] = new ModelRendererTurbo(this, 205, 113, textureX, textureY); // Box 50
		gunModel[105] = new ModelRendererTurbo(this, 22, 18, textureX, textureY); // Box 51
		gunModel[106] = new ModelRendererTurbo(this, 22, 18, textureX, textureY); // Box 52
		gunModel[107] = new ModelRendererTurbo(this, 22, 18, textureX, textureY); // Box 53
		gunModel[108] = new ModelRendererTurbo(this, 22, 18, textureX, textureY); // Box 54
		gunModel[109] = new ModelRendererTurbo(this, 43, 19, textureX, textureY); // Box 55
		gunModel[110] = new ModelRendererTurbo(this, 43, 19, textureX, textureY); // Box 56
		gunModel[111] = new ModelRendererTurbo(this, 1, 34, textureX, textureY); // Box 124
		gunModel[112] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 125
		gunModel[113] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 126
		gunModel[114] = new ModelRendererTurbo(this, 22, 18, textureX, textureY); // Box 127
		gunModel[115] = new ModelRendererTurbo(this, 22, 18, textureX, textureY); // Box 128

		gunModel[0].addShapeBox(0F, 0F, 0F, 40, 3, 8, 0F, -3F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, -3F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 1
		gunModel[0].setRotationPoint(-10F, -21F, -4F);

		gunModel[1].addShapeBox(0F, 0F, 0F, 3, 7, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F); // Box 3
		gunModel[1].setRotationPoint(-6F, -18F, -4F);

		gunModel[2].addBox(0F, 0F, 0F, 9, 7, 8, 0F); // Box 5
		gunModel[2].setRotationPoint(-3F, -18F, -4F);

		gunModel[3].addBox(0F, 0F, 0F, 3, 10, 8, 0F); // Box 7
		gunModel[3].setRotationPoint(6F, -18F, -4F);

		gunModel[4].addShapeBox(0F, 0F, 0F, 12, 5, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F); // Box 8
		gunModel[4].setRotationPoint(9F, -13F, -4F);

		gunModel[5].addBox(0F, 0F, 0F, 3, 8, 8, 0F); // Box 9
		gunModel[5].setRotationPoint(21F, -18F, -4F);

		gunModel[6].addBox(0F, 0F, 0F, 12, 3, 7, 0F); // Box 10
		gunModel[6].setRotationPoint(-6F, -11F, -3.5F);

		gunModel[7].addShapeBox(0F, 0F, 0F, 10, 3, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F); // Box 11
		gunModel[7].setRotationPoint(-5F, -8F, -3.5F);

		gunModel[8].addShapeBox(0F, 0F, 0F, 7, 9, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, -2F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 4F, -2F, 0F); // Box 14
		gunModel[8].setRotationPoint(-2F, -5F, -3.5F);

		gunModel[9].addShapeBox(0F, 0F, 0F, 1, 9, 7, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 4F, 0F, 0F, -4F, 0F, -1F, -4F, 0F, -1F, 4F, 0F, 0F); // Box 15
		gunModel[9].setRotationPoint(5F, -5F, -3.5F);

		gunModel[10].addBox(0F, 0F, 0F, 5, 4, 7, 0F); // Box 16
		gunModel[10].setRotationPoint(-8F, -23F, -3.5F);

		gunModel[11].addBox(0F, 0F, 0F, 18, 2, 5, 0F); // Box 18
		gunModel[11].setRotationPoint(12F, -22.5F, -2.5F);

		gunModel[12].addShapeBox(0F, 0F, 0F, 10, 6, 1, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 19
		gunModel[12].setRotationPoint(24F, -19.5F, -5F);

		gunModel[13].addBox(0F, 0F, 0F, 28, 1, 1, 0F); // Box 20
		gunModel[13].setRotationPoint(24F, -13.5F, -5F);

		gunModel[14].addShapeBox(0F, 0F, 0F, 28, 1, 8, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 22
		gunModel[14].setRotationPoint(24F, -20.5F, -4F);

		gunModel[15].addShapeBox(0F, 0F, 0F, 28, 1, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 23
		gunModel[15].setRotationPoint(24F, -12.5F, -5F);

		gunModel[16].addBox(0F, 0F, 0F, 6, 6, 8, 0F); // Box 33
		gunModel[16].setRotationPoint(27F, -18F, -4F);

		gunModel[17].addBox(0F, 0F, 0F, 28, 1, 1, 0F); // Box 34
		gunModel[17].setRotationPoint(24F, -13.5F, 4F);

		gunModel[18].addShapeBox(0F, 0F, 0F, 1, 6, 1, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 38
		gunModel[18].setRotationPoint(39F, -19.5F, -5F);

		gunModel[19].addShapeBox(0F, 0F, 0F, 5, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.15F, 0F, 0F, 0.15F, 0F, 0F, -0.15F, 0F, 0F, -0.15F); // Box 39
		gunModel[19].setRotationPoint(34F, -19.5F, -4F);

		gunModel[20].addShapeBox(0F, 0F, 0F, 5, 3, 1, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 40
		gunModel[20].setRotationPoint(34F, -16.5F, -5F);

		gunModel[21].addShapeBox(0F, 0F, 0F, 1, 6, 1, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 41
		gunModel[21].setRotationPoint(45F, -19.5F, -5F);

		gunModel[22].addShapeBox(0F, 0F, 0F, 5, 3, 1, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 42
		gunModel[22].setRotationPoint(40F, -16.5F, -5F);

		gunModel[23].addShapeBox(0F, 0F, 0F, 5, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.15F, 0F, 0F, 0.15F, 0F, 0F, -0.15F, 0F, 0F, -0.15F); // Box 43
		gunModel[23].setRotationPoint(40F, -19.5F, -4F);

		gunModel[24].addShapeBox(0F, 0F, 0F, 1, 6, 1, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 44
		gunModel[24].setRotationPoint(51F, -19.5F, -5F);

		gunModel[25].addShapeBox(0F, 0F, 0F, 5, 3, 1, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 45
		gunModel[25].setRotationPoint(46F, -16.5F, -5F);

		gunModel[26].addShapeBox(0F, 0F, 0F, 5, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.15F, 0F, 0F, 0.15F, 0F, 0F, -0.15F, 0F, 0F, -0.15F); // Box 46
		gunModel[26].setRotationPoint(46F, -19.5F, -4F);

		gunModel[27].addShapeBox(0F, 0F, 0F, 5, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.15F, 0F, 0F, -0.15F, 0F, 0F, 0.15F, 0F, 0F, 0.15F); // Box 47
		gunModel[27].setRotationPoint(46F, -19.5F, 3F);

		gunModel[28].addShapeBox(0F, 0F, 0F, 5, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.15F, 0F, 0F, -0.15F, 0F, 0F, 0.15F, 0F, 0F, 0.15F); // Box 48
		gunModel[28].setRotationPoint(40F, -19.5F, 3F);

		gunModel[29].addShapeBox(0F, 0F, 0F, 5, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.15F, 0F, 0F, -0.15F, 0F, 0F, 0.15F, 0F, 0F, 0.15F); // Box 49
		gunModel[29].setRotationPoint(34F, -19.5F, 3F);

		gunModel[30].addShapeBox(0F, 0F, 0F, 1, 6, 1, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 50
		gunModel[30].setRotationPoint(39F, -19.5F, 4F);

		gunModel[31].addShapeBox(0F, 0F, 0F, 5, 3, 1, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 51
		gunModel[31].setRotationPoint(34F, -16.5F, 4F);

		gunModel[32].addShapeBox(0F, 0F, 0F, 5, 3, 1, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 52
		gunModel[32].setRotationPoint(40F, -16.5F, 4F);

		gunModel[33].addShapeBox(0F, 0F, 0F, 1, 6, 1, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 53
		gunModel[33].setRotationPoint(45F, -19.5F, 4F);

		gunModel[34].addShapeBox(0F, 0F, 0F, 5, 3, 1, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 54
		gunModel[34].setRotationPoint(46F, -16.5F, 4F);

		gunModel[35].addShapeBox(0F, 0F, 0F, 1, 6, 1, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 55
		gunModel[35].setRotationPoint(51F, -19.5F, 4F);

		gunModel[36].addShapeBox(0F, 0F, 0F, 10, 6, 1, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 56
		gunModel[36].setRotationPoint(24F, -19.5F, 4F);

		gunModel[37].addShapeBox(0F, 0F, 0F, 35, 1, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 57
		gunModel[37].setRotationPoint(33F, -17F, -2F);

		gunModel[38].addBox(0F, 0F, 0F, 35, 2, 4, 0F); // Box 58
		gunModel[38].setRotationPoint(33F, -16F, -2F);

		gunModel[39].addShapeBox(0F, 0F, 0F, 35, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 60
		gunModel[39].setRotationPoint(33F, -14F, -2F);

		gunModel[40].addShapeBox(0F, 0F, 0F, 10, 2, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F); // Box 61
		gunModel[40].setRotationPoint(68F, -14F, -2.5F);

		gunModel[41].addBox(0F, 0F, 0F, 2, 2, 1, 0F); // Box 62
		gunModel[41].setRotationPoint(68F, -16F, -2.5F);

		gunModel[42].addShapeBox(0F, 0F, 0F, 10, 2, 5, 0F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 63
		gunModel[42].setRotationPoint(68F, -18F, -2.5F);

		gunModel[43].addShapeBox(0F, 0F, 0F, 3, 2, 5, 0F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 64
		gunModel[43].setRotationPoint(53F, -18F, -2.5F);

		gunModel[44].addBox(0F, 0F, 0F, 3, 2, 5, 0F); // Box 65
		gunModel[44].setRotationPoint(53F, -16F, -2.5F);

		gunModel[45].addShapeBox(0F, 0F, 0F, 3, 2, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F); // Box 66
		gunModel[45].setRotationPoint(53F, -14F, -2.5F);

		gunModel[46].addBox(0F, 0F, 0F, 15, 2, 7, 0F); // Box 67
		gunModel[46].setRotationPoint(-3F, -23F, -3.5F);

		gunModel[47].addBox(0F, 0F, 0F, 21, 2, 7, 0F); // Box 68
		gunModel[47].setRotationPoint(24F, -12F, -3.5F);

		gunModel[48].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 69
		gunModel[48].setRotationPoint(24F, -10F, -3.5F);

		gunModel[49].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 70
		gunModel[49].setRotationPoint(30F, -10F, -3.5F);

		gunModel[50].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 71
		gunModel[50].setRotationPoint(42F, -10F, -3.5F);

		gunModel[51].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 72
		gunModel[51].setRotationPoint(36F, -10F, -3.5F);

		gunModel[52].addBox(0F, 0F, 0F, 12, 5, 6, 0F); // Box 76
		gunModel[52].setRotationPoint(9F, -18F, -2F);

		gunModel[53].addBox(0F, 0F, 0F, 12, 2, 2, 0F); // Box 77
		gunModel[53].setRotationPoint(9F, -18F, -4F);

		gunModel[54].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 81
		gunModel[54].setRotationPoint(-3F, -25F, -3.5F);

		gunModel[55].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 82
		gunModel[55].setRotationPoint(3F, -25F, -3.5F);

		gunModel[56].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 83
		gunModel[56].setRotationPoint(9F, -25F, -3.5F);

		gunModel[57].addBox(0F, 0F, 0F, 30, 4, 6, 0F); // stock1
		gunModel[57].setRotationPoint(-40F, -12F, -3F);

		gunModel[58].addShapeBox(0F, 0F, 0F, 5, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F); // stock3
		gunModel[58].setRotationPoint(-45F, -11F, -4F);

		gunModel[59].addShapeBox(0F, 0F, 0F, 30, 1, 8, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // stockPipe1
		gunModel[59].setRotationPoint(-40F, -18F, -4F);

		gunModel[60].addBox(0F, 0F, 0F, 30, 4, 8, 0F); // stockPipe2
		gunModel[60].setRotationPoint(-40F, -17F, -4F);

		gunModel[61].addShapeBox(0F, 0F, 0F, 30, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 84
		gunModel[61].setRotationPoint(-40F, -13F, -4F);

		gunModel[62].addBox(0F, 0F, 0F, 4, 10, 8, 0F); // Box 89
		gunModel[62].setRotationPoint(-10F, -18F, -4F);

		gunModel[63].addShapeBox(0F, 0F, 0F, 30, 30, 6, 0F, 0F, 0F, 0F, -25F, 0F, 0F, -25F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, -25F, 0F, -25F, -25F, 0F, -25F, -25F, 0.4F, 0F, -25F, 0.4F); // Box 90
		gunModel[63].setRotationPoint(-21.5F, -9.5F, -3.2F);

		gunModel[64].addShapeBox(0F, 0F, 0F, 4, 2, 1, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 91
		gunModel[64].setRotationPoint(-2.5F, -10.5F, -4F);

		gunModel[65].addShapeBox(0F, 0F, 0F, 4, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 92
		gunModel[65].setRotationPoint(-2.5F, -8.5F, -4F);

		gunModel[66].addShapeBox(0F, 0F, 0F, 1, 3, 7, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 0
		gunModel[66].setRotationPoint(5F, -8F, -3.5F);

		gunModel[67].addShapeBox(0F, 0F, 0F, 1, 3, 7, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, -3F, 0F, -1F, 3F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, -1F); // Box 1
		gunModel[67].setRotationPoint(-6F, -8F, -3.5F);

		gunModel[68].addShapeBox(0F, 0F, 0F, 1, 7, 7, 0F, -4F, 0F, -1F, 4F, 0F, 0F, 4F, 0F, 0F, -4F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 2
		gunModel[68].setRotationPoint(-7F, -5F, -3.5F);

		gunModel[69].addShapeBox(0F, 0F, 0F, 1, 3, 7, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, -1F, -1F, 0F, -1F, 1F, 0F, 0F); // Box 3
		gunModel[69].setRotationPoint(1F, 4F, -3.5F);

		gunModel[70].addShapeBox(0F, 0F, 0F, 7, 3, 7, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 1F, -2F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, -2F, 0F); // Box 4
		gunModel[70].setRotationPoint(-6F, 4F, -3.5F);

		gunModel[71].addShapeBox(0F, 0F, 0F, 1, 3, 7, 0F, -1F, 0F, -1F, 1F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, -1F, 0F, -0.5F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -1F); // Box 5
		gunModel[71].setRotationPoint(-8F, 2F, -3.5F);

		gunModel[72].addShapeBox(0F, 0F, 0F, 3, 2, 5, 0F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 7
		gunModel[72].setRotationPoint(53F, -23F, -2.5F);

		gunModel[73].addBox(0F, 0F, 0F, 3, 2, 5, 0F); // Box 8
		gunModel[73].setRotationPoint(53F, -21F, -2.5F);

		gunModel[74].addShapeBox(0F, 0F, 0F, 3, 2, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F); // Box 9
		gunModel[74].setRotationPoint(53F, -19F, -2.5F);

		gunModel[75].addShapeBox(0F, 0F, 0F, 28, 1, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 10
		gunModel[75].setRotationPoint(30F, -22F, -2F);

		gunModel[76].addBox(0F, 0F, 0F, 28, 2, 4, 0F); // Box 11
		gunModel[76].setRotationPoint(30F, -21F, -2F);

		gunModel[77].addShapeBox(0F, 0F, 0F, 28, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 12
		gunModel[77].setRotationPoint(30F, -19F, -2F);

		gunModel[78].addShapeBox(0F, 0F, 0F, 19, 4, 1, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 13
		gunModel[78].setRotationPoint(-10F, -17F, -5F);

		gunModel[79].addShapeBox(0F, 0F, 0F, 19, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F); // Box 14
		gunModel[79].setRotationPoint(-10F, -17F, 4F);

		gunModel[80].addShapeBox(0F, 0F, 0F, 3, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F); // Box 15
		gunModel[80].setRotationPoint(21F, -17F, 4F);

		gunModel[81].addShapeBox(0F, 0F, 0F, 3, 4, 1, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 16
		gunModel[81].setRotationPoint(21F, -17F, -5F);

		gunModel[82].addShapeBox(0F, 0F, 0F, 12, 1, 1, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 17
		gunModel[82].setRotationPoint(9F, -17F, -5F);

		gunModel[83].addShapeBox(0F, 0F, 0F, 30, 4, 1, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 18
		gunModel[83].setRotationPoint(-40F, -17F, -5F);

		gunModel[84].addShapeBox(0F, 0F, 0F, 30, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F); // Box 19
		gunModel[84].setRotationPoint(-40F, -17F, 4F);

		gunModel[85].addShapeBox(0F, 0F, 0F, 16, 1, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 20
		gunModel[85].setRotationPoint(-31F, -8F, -3F);

		gunModel[86].addBox(0F, 0F, 0F, 14, 3, 6, 0F); // Box 21
		gunModel[86].setRotationPoint(-30F, -7F, -3F);

		gunModel[87].addShapeBox(0F, 0F, 0F, 5, 2, 10, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 22
		gunModel[87].setRotationPoint(-45F, -19F, -5F);

		gunModel[88].addShapeBox(0F, 0F, 0F, 5, 2, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 23
		gunModel[88].setRotationPoint(-45F, -13F, -5F);

		gunModel[89].addBox(0F, 0F, 0F, 3, 10, 8, 0F); // Box 24
		gunModel[89].setRotationPoint(-43F, -8F, -4F);

		gunModel[90].addBox(0F, 0F, 0F, 5, 4, 10, 0F); // Box 25
		gunModel[90].setRotationPoint(-45F, -17F, -5F);

		gunModel[91].addBox(0F, 0F, 0F, 3, 2, 8, 0F); // Box 26
		gunModel[91].setRotationPoint(-43F, 2F, -4F);

		gunModel[92].addBox(0F, 0F, 0F, 5, 5, 6, 0F); // Box 0
		gunModel[92].setRotationPoint(-40F, -8F, -3F);

		gunModel[93].addShapeBox(0F, 0F, 0F, 5, 1, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F); // Box 1
		gunModel[93].setRotationPoint(-40F, -3F, -3F);

		gunModel[94].addBox(0F, 0F, 0F, 5, 5, 6, 0F); // Box 32
		gunModel[94].setRotationPoint(-21F, -4F, -3F);

		gunModel[95].addShapeBox(0F, 0F, 0F, 8, 4, 6, 0F, 0F, 0F, 0F, 1F, -5F, -0.5F, 1F, -5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3.5F, -0.5F, 0F, 3.5F, -0.5F, 0F, 0F, 0F); // Box 33
		gunModel[95].setRotationPoint(-16F, -3F, -3F);

		gunModel[96].addShapeBox(0F, 0F, 0F, 16, 2, 10, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 34
		gunModel[96].setRotationPoint(-31F, -19F, -5F);

		gunModel[97].addBox(0F, 0F, 0F, 16, 3, 10, 0F); // Box 35
		gunModel[97].setRotationPoint(-31F, -17F, -5F);

		gunModel[98].addBox(0F, 0F, 0F, 2, 2, 1, 0F); // Box 36
		gunModel[98].setRotationPoint(68F, -16F, 1.5F);

		gunModel[99].addBox(0F, 0F, 0F, 2, 2, 1, 0F); // Box 37
		gunModel[99].setRotationPoint(72F, -16F, 1.5F);

		gunModel[100].addBox(0F, 0F, 0F, 2, 2, 1, 0F); // Box 38
		gunModel[100].setRotationPoint(72F, -16F, -2.5F);

		gunModel[101].addBox(0F, 0F, 0F, 2, 2, 1, 0F); // Box 39
		gunModel[101].setRotationPoint(76F, -16F, 1.5F);

		gunModel[102].addBox(0F, 0F, 0F, 2, 2, 1, 0F); // Box 40
		gunModel[102].setRotationPoint(76F, -16F, -2.5F);

		gunModel[103].addShapeBox(0F, 0F, 0F, 12, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 49
		gunModel[103].setRotationPoint(9F, -17F, 4F);

		gunModel[104].addShapeBox(0F, 0F, 0F, 12, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F); // Box 50
		gunModel[104].setRotationPoint(9F, -14F, 4F);

		gunModel[105].addShapeBox(0F, 0F, 0F, 2, 1, 8, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 51
		gunModel[105].setRotationPoint(-4F, -22F, -4F);

		gunModel[106].addShapeBox(0F, 0F, 0F, 2, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 52
		gunModel[106].setRotationPoint(-4F, -21F, -4F);

		gunModel[107].addShapeBox(0F, 0F, 0F, 2, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 53
		gunModel[107].setRotationPoint(11F, -21F, -4F);

		gunModel[108].addShapeBox(0F, 0F, 0F, 2, 1, 8, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 54
		gunModel[108].setRotationPoint(11F, -22F, -4F);

		gunModel[109].addShapeBox(0F, 0F, 0F, 2, 1, 7, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 55
		gunModel[109].setRotationPoint(-38.5F, -5F, -3.5F);

		gunModel[110].addShapeBox(0F, 0F, 0F, 2, 1, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 56
		gunModel[110].setRotationPoint(-38.5F, -4F, -3.5F);

		gunModel[111].addBox(0F, 0F, 0F, 15, 2, 7, 0F); // Box 124
		gunModel[111].setRotationPoint(12F, -23F, -3.5F);

		gunModel[112].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 125
		gunModel[112].setRotationPoint(21F, -25F, -3.5F);

		gunModel[113].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 126
		gunModel[113].setRotationPoint(15F, -25F, -3.5F);

		gunModel[114].addShapeBox(0F, 0F, 0F, 2, 1, 8, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 127
		gunModel[114].setRotationPoint(24F, -22F, -4F);

		gunModel[115].addShapeBox(0F, 0F, 0F, 2, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 128
		gunModel[115].setRotationPoint(24F, -21F, -4F);


		defaultScopeModel = new ModelRendererTurbo[4];
		defaultScopeModel[0] = new ModelRendererTurbo(this, 1, 11, textureX, textureY); // Box 96
		defaultScopeModel[1] = new ModelRendererTurbo(this, 1, 18, textureX, textureY); // Box 41
		defaultScopeModel[2] = new ModelRendererTurbo(this, 10, 11, textureX, textureY); // Box 47
		defaultScopeModel[3] = new ModelRendererTurbo(this, 10, 11, textureX, textureY); // Box 48

		defaultScopeModel[0].addShapeBox(0F, 0F, 0F, 3, 5, 1, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 96
		defaultScopeModel[0].setRotationPoint(53F, -27.5F, -0.5F);

		defaultScopeModel[1].addBox(0F, 0F, 0F, 4, 3, 6, 0F); // Box 41
		defaultScopeModel[1].setRotationPoint(-7F, -26F, -3F);

		defaultScopeModel[2].addShapeBox(0F, 0F, 0F, 4, 2, 2, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 47
		defaultScopeModel[2].setRotationPoint(-7F, -28F, -3F);

		defaultScopeModel[3].addShapeBox(0F, 0F, 0F, 4, 2, 2, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 48
		defaultScopeModel[3].setRotationPoint(-7F, -28F, 1F);


		ammoModel = new ModelRendererTurbo[6];
		ammoModel[0] = new ModelRendererTurbo(this, 30, 89, textureX, textureY); // Box 86
		ammoModel[1] = new ModelRendererTurbo(this, 57, 89, textureX, textureY); // Box 87
		ammoModel[2] = new ModelRendererTurbo(this, 1, 138, textureX, textureY); // Box 27
		ammoModel[3] = new ModelRendererTurbo(this, 24, 138, textureX, textureY); // Box 2
		ammoModel[4] = new ModelRendererTurbo(this, 1, 132, textureX, textureY); // Box 3
		ammoModel[5] = new ModelRendererTurbo(this, 45, 139, textureX, textureY); // Box 26

		ammoModel[0].addShapeBox(0F, 0F, 0F, 9, 1, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 86
		ammoModel[0].setRotationPoint(-29F, -8F, -2F);

		ammoModel[1].addShapeBox(0F, 0F, 0F, 3, 1, 4, 0F, 0F, 0F, -1F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 87
		ammoModel[1].setRotationPoint(-20F, -8F, -2F);

		ammoModel[2].addBox(0F, 0F, 0F, 6, 12, 5, 0F); // Box 27
		ammoModel[2].setRotationPoint(-29.5F, -7F, -2.5F);

		ammoModel[3].addBox(0F, 0F, 0F, 5, 12, 5, 0F); // Box 2
		ammoModel[3].setRotationPoint(-21.5F, -7F, -2.5F);

		ammoModel[4].addBox(0F, 0F, 0F, 13, 1, 4, 0F); // Box 3
		ammoModel[4].setRotationPoint(-29.5F, 5F, -2F);

		ammoModel[5].addBox(0F, 0F, 0F, 2, 12, 4, 0F); // Box 26
		ammoModel[5].setRotationPoint(-23.5F, -7F, -2F);


		slideModel = new ModelRendererTurbo[1];
		slideModel[0] = new ModelRendererTurbo(this, 174, 111, textureX, textureY); // Box 79

		slideModel[0].addShapeBox(0F, 0F, 0F, 12, 3, 1, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 79
		slideModel[0].setRotationPoint(9F, -16F, -4F);


		pumpModel = new ModelRendererTurbo[2];
		pumpModel[0] = new ModelRendererTurbo(this, 22, 18, textureX, textureY); // Box 122
		pumpModel[1] = new ModelRendererTurbo(this, 22, 18, textureX, textureY); // Box 123

		pumpModel[0].addShapeBox(0F, 0F, 0F, 2, 1, 8, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 122
		pumpModel[0].setRotationPoint(19F, -16F, 0F);

		pumpModel[1].addShapeBox(0F, 0F, 0F, 2, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 123
		pumpModel[1].setRotationPoint(19F, -15F, 0F);


		barrelAttachPoint = new Vector3f(68F /16F, 15F /16F, 0F /16F);
		scopeAttachPoint = new Vector3f(11F /16F, 22.5F /16F, 0F /16F);
		gripAttachPoint = new Vector3f(35 /16F, 10.5F /16F, 0F /16F);

		gunSlideDistance = 0.5F;
		pumpTime = 9;
		pumpHandleDistance = 0.7F;
		pumpDelayAfterReload = 76;

		animationType = EnumAnimationType.CUSTOM;

		/* ----Start of Reload Block---- */
		/* Reload Name: Bullpup */
		rotateGunVertical = 70F;
		tiltGun = 10F;
		translateGun = new Vector3f(0.5F, -0.2F, 0F);

		rotateClipVertical = -90F;
		translateClip = new Vector3f(-2F, -2F, 0F);
		/* ----End of Reload Block---- */


		translateAll(0F, 0F, 0F);


		flipAll();
	}
}