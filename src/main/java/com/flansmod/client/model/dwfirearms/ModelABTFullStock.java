package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.ModelAttachment;
import com.flansmod.client.tmt.ModelRendererTurbo;

import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelABTFullStock extends ModelAttachment
{
	int textureX = 512;
	int textureY = 256;

	public ModelABTFullStock()
	{
		attachmentModel = new ModelRendererTurbo[7];
		attachmentModel[0] = new ModelRendererTurbo(this, 93, 159, textureX, textureY); // stockBolt
		attachmentModel[1] = new ModelRendererTurbo(this, 64, 154, textureX, textureY); // stockPart1
		attachmentModel[2] = new ModelRendererTurbo(this, 93, 139, textureX, textureY); // stockPart2
		attachmentModel[3] = new ModelRendererTurbo(this, 37, 157, textureX, textureY); // stockPart3
		attachmentModel[4] = new ModelRendererTurbo(this, 1, 120, textureX, textureY); // stockPart4
		attachmentModel[5] = new ModelRendererTurbo(this, 22, 150, textureX, textureY); // stockPart5
		attachmentModel[6] = new ModelRendererTurbo(this, 1, 146, textureX, textureY); // stockPart6

		attachmentModel[0].addBox(-1F, -4F, -4.5F, 2, 9, 9, 0F); // stockBolt
		attachmentModel[0].setRotationPoint(0F, 0F, 0F);

		attachmentModel[1].addShapeBox(-6F, -7F, -4F, 6, 15, 8, 0F, 0F, -6F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -6F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F); // stockPart1
		attachmentModel[1].setRotationPoint(0F, 0F, 0F);

		attachmentModel[2].addShapeBox(-8F, -1F, -4F, 2, 11, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F); // stockPart2
		attachmentModel[2].setRotationPoint(0F, 0F, 0F);

		attachmentModel[3].addShapeBox(-13F, -1F, -4F, 5, 12, 8, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F); // stockPart3
		attachmentModel[3].setRotationPoint(0F, 0F, 0F);

		attachmentModel[4].addShapeBox(-37F, -4F, -4F, 24, 17, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 6F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 6F, 0F); // stockPart4
		attachmentModel[4].setRotationPoint(0F, 0F, 0F);

		attachmentModel[5].addBox(-38F, -3F, -3F, 1, 21, 6, 0F); // stockPart5
		attachmentModel[5].setRotationPoint(0F, 0F, 0F);

		attachmentModel[6].addBox(-40F, -4F, -4F, 2, 23, 8, 0F); // stockPart6
		attachmentModel[6].setRotationPoint(0F, 0F, 0F);


		renderOffset = 0F;

		flipAll();
	}
}