package com.flansmod.client.model.dwfirearms; //Path where the model is located

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelPAGL extends ModelGun //Same as Filename
{
	int textureX = 512;
	int textureY = 256;

	public ModelPAGL() //Same as Filename
	{
		gunModel = new ModelRendererTurbo[58];
		gunModel[0] = new ModelRendererTurbo(this, 72, 32, textureX, textureY); // body1
		gunModel[1] = new ModelRendererTurbo(this, 72, 53, textureX, textureY); // body10
		gunModel[2] = new ModelRendererTurbo(this, 99, 143, textureX, textureY); // body11
		gunModel[3] = new ModelRendererTurbo(this, 99, 149, textureX, textureY); // body12
		gunModel[4] = new ModelRendererTurbo(this, 72, 88, textureX, textureY); // body2
		gunModel[5] = new ModelRendererTurbo(this, 72, 103, textureX, textureY); // body3
		gunModel[6] = new ModelRendererTurbo(this, 72, 118, textureX, textureY); // body4
		gunModel[7] = new ModelRendererTurbo(this, 72, 129, textureX, textureY); // body5
		gunModel[8] = new ModelRendererTurbo(this, 72, 141, textureX, textureY); // body6
		gunModel[9] = new ModelRendererTurbo(this, 112, 146, textureX, textureY); // body7
		gunModel[10] = new ModelRendererTurbo(this, 72, 75, textureX, textureY); // body8
		gunModel[11] = new ModelRendererTurbo(this, 72, 64, textureX, textureY); // body9
		gunModel[12] = new ModelRendererTurbo(this, 1, 47, textureX, textureY); // grip1
		gunModel[13] = new ModelRendererTurbo(this, 1, 26, textureX, textureY); // grip2
		gunModel[14] = new ModelRendererTurbo(this, 1, 59, textureX, textureY); // grip3
		gunModel[15] = new ModelRendererTurbo(this, 72, 20, textureX, textureY); // mainBarrelBottom
		gunModel[16] = new ModelRendererTurbo(this, 72, 11, textureX, textureY); // mainBarrelMiddle
		gunModel[17] = new ModelRendererTurbo(this, 72, 2, textureX, textureY); // mainBarrelTop
		gunModel[18] = new ModelRendererTurbo(this, 101, 19, textureX, textureY); // ring1
		gunModel[19] = new ModelRendererTurbo(this, 101, 19, textureX, textureY); // ring1-2
		gunModel[20] = new ModelRendererTurbo(this, 101, 8, textureX, textureY); // ring2
		gunModel[21] = new ModelRendererTurbo(this, 1, 59, textureX, textureY); // grip4
		gunModel[22] = new ModelRendererTurbo(this, 26, 11, textureX, textureY); // Box 71
		gunModel[23] = new ModelRendererTurbo(this, 26, 11, textureX, textureY); // Box 72
		gunModel[24] = new ModelRendererTurbo(this, 26, 11, textureX, textureY); // Box 73
		gunModel[25] = new ModelRendererTurbo(this, 26, 11, textureX, textureY); // Box 74
		gunModel[26] = new ModelRendererTurbo(this, 1, 11, textureX, textureY); // clipPart
		gunModel[27] = new ModelRendererTurbo(this, 1, 71, textureX, textureY); // Box 0
		gunModel[28] = new ModelRendererTurbo(this, 1, 59, textureX, textureY); // Box 5
		gunModel[29] = new ModelRendererTurbo(this, 41, 83, textureX, textureY); // Box 6
		gunModel[30] = new ModelRendererTurbo(this, 41, 83, textureX, textureY); // Box 9
		gunModel[31] = new ModelRendererTurbo(this, 1, 83, textureX, textureY); // Box 10
		gunModel[32] = new ModelRendererTurbo(this, 24, 84, textureX, textureY); // Box 11
		gunModel[33] = new ModelRendererTurbo(this, 24, 84, textureX, textureY); // Box 12
		gunModel[34] = new ModelRendererTurbo(this, 1, 107, textureX, textureY); // Box 13
		gunModel[35] = new ModelRendererTurbo(this, 24, 84, textureX, textureY); // Box 0
		gunModel[36] = new ModelRendererTurbo(this, 1, 83, textureX, textureY); // Box 1
		gunModel[37] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 2
		gunModel[38] = new ModelRendererTurbo(this, 133, 128, textureX, textureY); // Box 3
		gunModel[39] = new ModelRendererTurbo(this, 133, 94, textureX, textureY); // Box 4
		gunModel[40] = new ModelRendererTurbo(this, 133, 142, textureX, textureY); // Box 5
		gunModel[41] = new ModelRendererTurbo(this, 133, 118, textureX, textureY); // Box 6
		gunModel[42] = new ModelRendererTurbo(this, 156, 131, textureX, textureY); // Box 7
		gunModel[43] = new ModelRendererTurbo(this, 176, 66, textureX, textureY); // Box 8
		gunModel[44] = new ModelRendererTurbo(this, 213, 67, textureX, textureY); // Box 9
		gunModel[45] = new ModelRendererTurbo(this, 250, 72, textureX, textureY); // Box 10
		gunModel[46] = new ModelRendererTurbo(this, 213, 67, textureX, textureY); // Box 11
		gunModel[47] = new ModelRendererTurbo(this, 250, 72, textureX, textureY); // Box 12
		gunModel[48] = new ModelRendererTurbo(this, 176, 47, textureX, textureY); // Box 13
		gunModel[49] = new ModelRendererTurbo(this, 209, 48, textureX, textureY); // Box 14
		gunModel[50] = new ModelRendererTurbo(this, 242, 53, textureX, textureY); // Box 15
		gunModel[51] = new ModelRendererTurbo(this, 242, 53, textureX, textureY); // Box 16
		gunModel[52] = new ModelRendererTurbo(this, 209, 48, textureX, textureY); // Box 17
		gunModel[53] = new ModelRendererTurbo(this, 176, 35, textureX, textureY); // Box 23
		gunModel[54] = new ModelRendererTurbo(this, 176, 18, textureX, textureY); // Box 24
		gunModel[55] = new ModelRendererTurbo(this, 176, 1, textureX, textureY); // Box 25
		gunModel[56] = new ModelRendererTurbo(this, 176, 18, textureX, textureY); // Box 26
		gunModel[57] = new ModelRendererTurbo(this, 176, 35, textureX, textureY); // Box 27

		gunModel[0].addBox(0F, 0F, 0F, 25, 10, 10, 0F); // body1
		gunModel[0].setRotationPoint(-10F, -20F, -5F);

		gunModel[1].addShapeBox(0F, 0F, 0F, 40, 2, 8, 0F, -1F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, -1F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body10
		gunModel[1].setRotationPoint(-8F, -26F, -4F);

		gunModel[2].addShapeBox(0F, 0F, 0F, 3, 2, 3, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body11
		gunModel[2].setRotationPoint(-9F, -25F, -1.5F);

		gunModel[3].addBox(0F, 0F, 0F, 12, 3, 1, 0F); // body12
		gunModel[3].setRotationPoint(14F, -19F, 4.5F);

		gunModel[4].addBox(0F, 0F, 0F, 20, 5, 9, 0F); // body2
		gunModel[4].setRotationPoint(15F, -15F, -4.5F);

		gunModel[5].addBox(0F, 0F, 0F, 20, 4, 10, 0F); // body3
		gunModel[5].setRotationPoint(15F, -19F, -5F);

		gunModel[6].addBox(0F, 0F, 0F, 20, 1, 9, 0F); // body4
		gunModel[6].setRotationPoint(15F, -20F, -4F);

		gunModel[7].addShapeBox(0F, 0F, 0F, 14, 2, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body5
		gunModel[7].setRotationPoint(15F, -22F, -4F);

		gunModel[8].addShapeBox(0F, 0F, 0F, 3, 2, 10, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body6
		gunModel[8].setRotationPoint(29F, -22F, -5F);

		gunModel[9].addBox(0F, 0F, 0F, 6, 1, 1, 0F); // body7
		gunModel[9].setRotationPoint(29F, -20F, -5F);

		gunModel[10].addShapeBox(0F, 0F, 0F, 25, 2, 10, 0F, -1F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, -1F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body8
		gunModel[10].setRotationPoint(-10F, -22F, -5F);

		gunModel[11].addShapeBox(0F, 0F, 0F, 41, 2, 8, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body9
		gunModel[11].setRotationPoint(-9F, -24F, -4F);

		gunModel[12].addShapeBox(0F, 0F, 0F, 12, 2, 9, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F); // grip1
		gunModel[12].setRotationPoint(-9F, -9F, -4.5F);

		gunModel[13].addShapeBox(0F, 0F, 0F, 11, 11, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, -2F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 4F, -2F, 0F); // grip2
		gunModel[13].setRotationPoint(-8F, -5F, -4.5F);

		gunModel[14].addShapeBox(0F, 0F, 0F, 11, 2, 9, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, -2F, 0F); // grip3
		gunModel[14].setRotationPoint(-12F, 6F, -4.5F);

		gunModel[15].addShapeBox(0F, 0F, 0F, 8, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // mainBarrelBottom
		gunModel[15].setRotationPoint(57F, -13.5F, -3F);

		gunModel[16].addBox(0F, 0F, 0F, 8, 2, 6, 0F); // mainBarrelMiddle
		gunModel[16].setRotationPoint(57F, -15.5F, -3F);

		gunModel[17].addShapeBox(0F, 0F, 0F, 8, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // mainBarrelTop
		gunModel[17].setRotationPoint(57F, -17.5F, -3F);

		gunModel[18].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // ring1
		gunModel[18].setRotationPoint(65F, -18F, -3.5F);

		gunModel[19].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // ring1-2
		gunModel[19].setRotationPoint(65F, -13F, -3.5F);

		gunModel[20].addBox(0F, 0F, 0F, 2, 3, 7, 0F); // ring2
		gunModel[20].setRotationPoint(65F, -16F, -3.5F);

		gunModel[21].addShapeBox(0F, 0F, 0F, 11, 2, 9, 0F, 0F, 2F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, -2F, 0F); // grip4
		gunModel[21].setRotationPoint(-12F, 8F, -4.5F);

		gunModel[22].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 71
		gunModel[22].setRotationPoint(40F, -8F, -3.5F);

		gunModel[23].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 72
		gunModel[23].setRotationPoint(46F, -8F, -3.5F);

		gunModel[24].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 73
		gunModel[24].setRotationPoint(52F, -8F, -3.5F);

		gunModel[25].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 74
		gunModel[25].setRotationPoint(58F, -8F, -3.5F);

		gunModel[26].addBox(0F, 0F, 0F, 3, 5, 9, 0F); // clipPart
		gunModel[26].setRotationPoint(12F, -10F, -4.5F);

		gunModel[27].addBox(0F, 0F, 0F, 12, 3, 8, 0F); // Box 0
		gunModel[27].setRotationPoint(-8F, -10F, -4F);

		gunModel[28].addBox(0F, 0F, 0F, 11, 2, 9, 0F); // Box 5
		gunModel[28].setRotationPoint(-8F, -7F, -4.5F);

		gunModel[29].addBox(0F, 0F, 0F, 3, 15, 8, 0F); // Box 6
		gunModel[29].setRotationPoint(15F, -10F, -4F);
		gunModel[29].rotateAngleZ = 0.12217305F;

		gunModel[30].addBox(4F, 0F, 0F, 3, 15, 8, 0F); // Box 9
		gunModel[30].setRotationPoint(15F, -10F, -4F);
		gunModel[30].rotateAngleZ = 0.12217305F;

		gunModel[31].addBox(8F, 0F, 0F, 3, 15, 8, 0F); // Box 10
		gunModel[31].setRotationPoint(15F, -10F, -4F);
		gunModel[31].rotateAngleZ = 0.12217305F;

		gunModel[32].addBox(3F, 0F, 0.5F, 1, 15, 7, 0F); // Box 11
		gunModel[32].setRotationPoint(15F, -10F, -4F);
		gunModel[32].rotateAngleZ = 0.12217305F;

		gunModel[33].addBox(7F, 0F, 0.5F, 1, 15, 7, 0F); // Box 12
		gunModel[33].setRotationPoint(15F, -10F, -4F);
		gunModel[33].rotateAngleZ = 0.12217305F;

		gunModel[34].addBox(0F, 15F, 0F, 15, 2, 8, 0F); // Box 13
		gunModel[34].setRotationPoint(15F, -10F, -4F);
		gunModel[34].rotateAngleZ = 0.12217305F;

		gunModel[35].addBox(11F, 0F, 0.5F, 1, 15, 7, 0F); // Box 0
		gunModel[35].setRotationPoint(15F, -10F, -4F);
		gunModel[35].rotateAngleZ = 0.12217305F;

		gunModel[36].addBox(12F, 0F, 0F, 3, 15, 8, 0F); // Box 1
		gunModel[36].setRotationPoint(15F, -10F, -4F);
		gunModel[36].rotateAngleZ = 0.12217305F;

		gunModel[37].addBox(0F, 0F, 0F, 21, 2, 7, 0F); // Box 2
		gunModel[37].setRotationPoint(40F, -10F, -3.5F);

		gunModel[38].addBox(0F, 0F, 0F, 3, 5, 8, 0F); // Box 3
		gunModel[38].setRotationPoint(32F, -25F, -4F);

		gunModel[39].addShapeBox(0F, 0F, 0F, 22, 15, 8, 0F, 0F, 0F, 0F, 0F, -8F, 0F, 0F, -8F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 4
		gunModel[39].setRotationPoint(35F, -25F, -4F);

		gunModel[40].addShapeBox(0F, 0F, 0F, 3, 1, 8, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 5
		gunModel[40].setRotationPoint(32F, -26F, -4F);

		gunModel[41].addShapeBox(0F, 0F, 0F, 22, 1, 8, 0F, 0F, 0F, -1F, 0F, -8F, -1F, 0F, -8F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 8F, 0F, 0F, 8F, 0F, 0F, 0F, 0F); // Box 6
		gunModel[41].setRotationPoint(35F, -26F, -4F);

		gunModel[42].addBox(0F, 0F, 0F, 5, 2, 8, 0F); // Box 7
		gunModel[42].setRotationPoint(57F, -12F, -4F);

		gunModel[43].addBox(0F, 0F, 0F, 4, 4, 14, 0F); // Box 8
		gunModel[43].setRotationPoint(67F, -16.5F, -7.5F);

		gunModel[44].addShapeBox(0F, 0F, 0F, 4, 3, 14, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 9
		gunModel[44].setRotationPoint(67F, -19.5F, -7.5F);

		gunModel[45].addShapeBox(0F, 0F, 0F, 4, 2, 10, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 10
		gunModel[45].setRotationPoint(67F, -21.5F, -5.5F);

		gunModel[46].addShapeBox(0F, 0F, 0F, 4, 3, 14, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 11
		gunModel[46].setRotationPoint(67F, -12.5F, -7.5F);

		gunModel[47].addShapeBox(0F, 0F, 0F, 4, 2, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 12
		gunModel[47].setRotationPoint(67F, -9.5F, -5.5F);

		gunModel[48].addBox(0F, 0F, 0F, 2, 4, 14, 0F); // Box 13
		gunModel[48].setRotationPoint(87F, -16.5F, -7.5F);

		gunModel[49].addShapeBox(0F, 0F, 0F, 2, 3, 14, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 14
		gunModel[49].setRotationPoint(87F, -19.5F, -7.5F);

		gunModel[50].addShapeBox(0F, 0F, 0F, 2, 2, 10, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 15
		gunModel[50].setRotationPoint(87F, -21.5F, -5.5F);

		gunModel[51].addShapeBox(0F, 0F, 0F, 2, 2, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 16
		gunModel[51].setRotationPoint(87F, -9.5F, -5.5F);

		gunModel[52].addShapeBox(0F, 0F, 0F, 2, 3, 14, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 17
		gunModel[52].setRotationPoint(87F, -12.5F, -7.5F);

		gunModel[53].addShapeBox(0F, 0F, 0F, 16, 2, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 23
		gunModel[53].setRotationPoint(71F, -10F, -5F);

		gunModel[54].addShapeBox(0F, 0F, 0F, 16, 3, 13, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 24
		gunModel[54].setRotationPoint(71F, -13F, -7F);

		gunModel[55].addBox(0F, 0F, 0F, 16, 3, 13, 0F); // Box 25
		gunModel[55].setRotationPoint(71F, -16F, -7F);

		gunModel[56].addShapeBox(0F, 0F, 0F, 16, 3, 13, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 26
		gunModel[56].setRotationPoint(71F, -19F, -7F);

		gunModel[57].addShapeBox(0F, 0F, 0F, 16, 2, 9, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 27
		gunModel[57].setRotationPoint(71F, -21F, -5F);


		ammoModel = new ModelRendererTurbo[6];
		ammoModel[0] = new ModelRendererTurbo(this, 1, 133, textureX, textureY); // Box 28
		ammoModel[1] = new ModelRendererTurbo(this, 1, 148, textureX, textureY); // Box 29
		ammoModel[2] = new ModelRendererTurbo(this, 1, 148, textureX, textureY); // Box 30
		ammoModel[3] = new ModelRendererTurbo(this, 1, 118, textureX, textureY); // Box 32
		ammoModel[4] = new ModelRendererTurbo(this, 28, 119, textureX, textureY); // Box 33
		ammoModel[5] = new ModelRendererTurbo(this, 28, 119, textureX, textureY); // Box 34

		ammoModel[0].addBox(0F, 0F, 0F, 13, 4, 10, 0F); // Box 28
		ammoModel[0].setRotationPoint(72F, -16.5F, -5.5F);

		ammoModel[1].addShapeBox(0F, 0F, 0F, 13, 3, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F); // Box 29
		ammoModel[1].setRotationPoint(72F, -12.5F, -5.5F);

		ammoModel[2].addShapeBox(0F, 0F, 0F, 13, 3, 10, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 30
		ammoModel[2].setRotationPoint(72F, -19.5F, -5.5F);

		ammoModel[3].addShapeBox(0F, 0F, 0F, 3, 4, 10, 0F, 0F, 0F, 0F, 0F, -1F, -2F, 0F, -1F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, -2F, 0F, -1F, -2F, 0F, 0F, 0F); // Box 32
		ammoModel[3].setRotationPoint(85F, -16.5F, -5.5F);

		ammoModel[4].addShapeBox(0F, 0F, 0F, 3, 3, 10, 0F, 0F, 0F, -2.5F, 0F, -2F, -3.5F, 0F, -2F, -3.5F, 0F, 0F, -2.5F, 0F, 0F, 0F, 0F, 1F, -2F, 0F, 1F, -2F, 0F, 0F, 0F); // Box 33
		ammoModel[4].setRotationPoint(85F, -19.5F, -5.5F);

		ammoModel[5].addShapeBox(0F, 0F, 0F, 3, 3, 10, 0F, 0F, 0F, 0F, 0F, 1F, -2F, 0F, 1F, -2F, 0F, 0F, 0F, 0F, 0F, -2.5F, 0F, -2F, -3.5F, 0F, -2F, -3.5F, 0F, 0F, -2.5F); // Box 34
		ammoModel[5].setRotationPoint(85F, -12.5F, -5.5F);


		pumpModel = new ModelRendererTurbo[2];
		pumpModel[0] = new ModelRendererTurbo(this, 58, 1, textureX, textureY); // bolt1
		pumpModel[1] = new ModelRendererTurbo(this, 58, 8, textureX, textureY); // bolt2

		pumpModel[0].addBox(0F, 0F, 0F, 1, 3, 3, 0F); // bolt1
		pumpModel[0].setRotationPoint(28F, -22F, -7F);

		pumpModel[1].addBox(0F, 0F, 0F, 3, 3, 1, 0F); // bolt2
		pumpModel[1].setRotationPoint(25F, -22F, -4.5F);

		stockAttachPoint = new Vector3f(-10F /16F, 15F /16F, 0F /16F);
		gripAttachPoint = new Vector3f(51 /16F, 9F /16F, 0F /16F);
		

		gunSlideDistance = 0F;
		animationType = EnumAnimationType.END_LOADED;

		pumpDelayAfterReload = 65;
		pumpTime = 8;
		pumpHandleDistance = 0.8F;

		translateAll(0F, 0F, 0F);


		flipAll();
	}
}