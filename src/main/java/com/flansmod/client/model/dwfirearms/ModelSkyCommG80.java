package com.flansmod.client.model.dwfirearms; //Path where the model is located

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelSkyCommG80 extends ModelGun //Same as Filename
{
	int textureX = 512;
	int textureY = 256;

	public ModelSkyCommG80() //Same as Filename
	{
		gunModel = new ModelRendererTurbo[111];
		gunModel[0] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // railPart3
		gunModel[1] = new ModelRendererTurbo(this, 193, 41, textureX, textureY); // Box 15
		gunModel[2] = new ModelRendererTurbo(this, 193, 41, textureX, textureY); // Box 16
		gunModel[3] = new ModelRendererTurbo(this, 193, 41, textureX, textureY); // Box 17
		gunModel[4] = new ModelRendererTurbo(this, 167, 17, textureX, textureY); // Box 6
		gunModel[5] = new ModelRendererTurbo(this, 1, 126, textureX, textureY); // Box 8
		gunModel[6] = new ModelRendererTurbo(this, 112, 40, textureX, textureY); // Box 9
		gunModel[7] = new ModelRendererTurbo(this, 112, 16, textureX, textureY); // Box 10
		gunModel[8] = new ModelRendererTurbo(this, 18, 94, textureX, textureY); // Box 17
		gunModel[9] = new ModelRendererTurbo(this, 18, 100, textureX, textureY); // Box 18
		gunModel[10] = new ModelRendererTurbo(this, 1, 82, textureX, textureY); // Box 20
		gunModel[11] = new ModelRendererTurbo(this, 18, 82, textureX, textureY); // Box 21
		gunModel[12] = new ModelRendererTurbo(this, 18, 88, textureX, textureY); // Box 22
		gunModel[13] = new ModelRendererTurbo(this, 1, 96, textureX, textureY); // Box 24
		gunModel[14] = new ModelRendererTurbo(this, 26, 112, textureX, textureY); // Box 25
		gunModel[15] = new ModelRendererTurbo(this, 1, 112, textureX, textureY); // Box 26
		gunModel[16] = new ModelRendererTurbo(this, 112, 65, textureX, textureY); // Box 28
		gunModel[17] = new ModelRendererTurbo(this, 186, 130, textureX, textureY); // Box 30
		gunModel[18] = new ModelRendererTurbo(this, 112, 112, textureX, textureY); // Box 1
		gunModel[19] = new ModelRendererTurbo(this, 1, 15, textureX, textureY); // Box 3
		gunModel[20] = new ModelRendererTurbo(this, 1, 15, textureX, textureY); // Box 4
		gunModel[21] = new ModelRendererTurbo(this, 1, 15, textureX, textureY); // Box 6
		gunModel[22] = new ModelRendererTurbo(this, 1, 15, textureX, textureY); // Box 7
		gunModel[23] = new ModelRendererTurbo(this, 1, 15, textureX, textureY); // Box 8
		gunModel[24] = new ModelRendererTurbo(this, 112, 141, textureX, textureY); // Box 27
		gunModel[25] = new ModelRendererTurbo(this, 111, 159, textureX, textureY); // Box 28
		gunModel[26] = new ModelRendererTurbo(this, 67, 126, textureX, textureY); // Box 49
		gunModel[27] = new ModelRendererTurbo(this, 67, 112, textureX, textureY); // Box 50
		gunModel[28] = new ModelRendererTurbo(this, 26, 106, textureX, textureY); // Box 64
		gunModel[29] = new ModelRendererTurbo(this, 165, 55, textureX, textureY); // Box 67
		gunModel[30] = new ModelRendererTurbo(this, 172, 46, textureX, textureY); // Box 68
		gunModel[31] = new ModelRendererTurbo(this, 165, 45, textureX, textureY); // Box 69
		gunModel[32] = new ModelRendererTurbo(this, 181, 51, textureX, textureY); // Box 72
		gunModel[33] = new ModelRendererTurbo(this, 181, 51, textureX, textureY); // Box 73
		gunModel[34] = new ModelRendererTurbo(this, 256, 68, textureX, textureY); // Box 74
		gunModel[35] = new ModelRendererTurbo(this, 256, 90, textureX, textureY); // Box 78
		gunModel[36] = new ModelRendererTurbo(this, 181, 48, textureX, textureY); // Box 79
		gunModel[37] = new ModelRendererTurbo(this, 181, 48, textureX, textureY); // Box 80
		gunModel[38] = new ModelRendererTurbo(this, 181, 45, textureX, textureY); // Box 81
		gunModel[39] = new ModelRendererTurbo(this, 188, 52, textureX, textureY); // Box 82
		gunModel[40] = new ModelRendererTurbo(this, 57, 71, textureX, textureY); // Box 83
		gunModel[41] = new ModelRendererTurbo(this, 57, 71, textureX, textureY); // Box 84
		gunModel[42] = new ModelRendererTurbo(this, 57, 71, textureX, textureY); // Box 85
		gunModel[43] = new ModelRendererTurbo(this, 57, 71, textureX, textureY); // Box 86
		gunModel[44] = new ModelRendererTurbo(this, 256, 90, textureX, textureY); // Box 134
		gunModel[45] = new ModelRendererTurbo(this, 165, 45, textureX, textureY); // Box 135
		gunModel[46] = new ModelRendererTurbo(this, 295, 95, textureX, textureY); // Box 136
		gunModel[47] = new ModelRendererTurbo(this, 308, 98, textureX, textureY); // Box 137
		gunModel[48] = new ModelRendererTurbo(this, 308, 101, textureX, textureY); // Box 138
		gunModel[49] = new ModelRendererTurbo(this, 112, 84, textureX, textureY); // Box 0
		gunModel[50] = new ModelRendererTurbo(this, 122, 159, textureX, textureY); // Box 1
		gunModel[51] = new ModelRendererTurbo(this, 112, 1, textureX, textureY); // Box 4
		gunModel[52] = new ModelRendererTurbo(this, 112, 9, textureX, textureY); // Box 4
		gunModel[53] = new ModelRendererTurbo(this, 112, 9, textureX, textureY); // Box 4
		gunModel[54] = new ModelRendererTurbo(this, 112, 100, textureX, textureY); // Box 6
		gunModel[55] = new ModelRendererTurbo(this, 219, 9, textureX, textureY); // Box 7
		gunModel[56] = new ModelRendererTurbo(this, 219, 9, textureX, textureY); // Box 8
		gunModel[57] = new ModelRendererTurbo(this, 221, 1, textureX, textureY); // Box 9
		gunModel[58] = new ModelRendererTurbo(this, 198, 9, textureX, textureY); // Box 10
		gunModel[59] = new ModelRendererTurbo(this, 198, 9, textureX, textureY); // Box 11
		gunModel[60] = new ModelRendererTurbo(this, 198, 1, textureX, textureY); // Box 12
		gunModel[61] = new ModelRendererTurbo(this, 199, 100, textureX, textureY); // Box 13
		gunModel[62] = new ModelRendererTurbo(this, 197, 56, textureX, textureY); // Box 14
		gunModel[63] = new ModelRendererTurbo(this, 199, 100, textureX, textureY); // Box 15
		gunModel[64] = new ModelRendererTurbo(this, 112, 132, textureX, textureY); // Box 17
		gunModel[65] = new ModelRendererTurbo(this, 112, 124, textureX, textureY); // Box 18
		gunModel[66] = new ModelRendererTurbo(this, 165, 132, textureX, textureY); // Box 19
		gunModel[67] = new ModelRendererTurbo(this, 165, 124, textureX, textureY); // Box 20
		gunModel[68] = new ModelRendererTurbo(this, 187, 1, textureX, textureY); // Box 21
		gunModel[69] = new ModelRendererTurbo(this, 187, 1, textureX, textureY); // Box 22
		gunModel[70] = new ModelRendererTurbo(this, 187, 1, textureX, textureY); // Box 23
		gunModel[71] = new ModelRendererTurbo(this, 232, 1, textureX, textureY); // Box 24
		gunModel[72] = new ModelRendererTurbo(this, 232, 1, textureX, textureY); // Box 25
		gunModel[73] = new ModelRendererTurbo(this, 232, 1, textureX, textureY); // Box 26
		gunModel[74] = new ModelRendererTurbo(this, 198, 16, textureX, textureY); // Box 27
		gunModel[75] = new ModelRendererTurbo(this, 198, 16, textureX, textureY); // Box 28
		gunModel[76] = new ModelRendererTurbo(this, 221, 16, textureX, textureY); // Box 29
		gunModel[77] = new ModelRendererTurbo(this, 71, 17, textureX, textureY); // Box 31
		gunModel[78] = new ModelRendererTurbo(this, 28, 16, textureX, textureY); // Box 32
		gunModel[79] = new ModelRendererTurbo(this, 71, 28, textureX, textureY); // Box 33
		gunModel[80] = new ModelRendererTurbo(this, 1, 28, textureX, textureY); // Box 34
		gunModel[81] = new ModelRendererTurbo(this, 1, 40, textureX, textureY); // Box 35
		gunModel[82] = new ModelRendererTurbo(this, 1, 54, textureX, textureY); // Box 36
		gunModel[83] = new ModelRendererTurbo(this, 1, 70, textureX, textureY); // Box 38
		gunModel[84] = new ModelRendererTurbo(this, 36, 28, textureX, textureY); // Box 39
		gunModel[85] = new ModelRendererTurbo(this, 36, 40, textureX, textureY); // Box 40
		gunModel[86] = new ModelRendererTurbo(this, 36, 54, textureX, textureY); // Box 41
		gunModel[87] = new ModelRendererTurbo(this, 36, 70, textureX, textureY); // Box 42
		gunModel[88] = new ModelRendererTurbo(this, 40, 125, textureX, textureY); // Box 43
		gunModel[89] = new ModelRendererTurbo(this, 199, 85, textureX, textureY); // Box 46
		gunModel[90] = new ModelRendererTurbo(this, 191, 109, textureX, textureY); // Box 47
		gunModel[91] = new ModelRendererTurbo(this, 199, 71, textureX, textureY); // Box 48
		gunModel[92] = new ModelRendererTurbo(this, 218, 50, textureX, textureY); // Box 49
		gunModel[93] = new ModelRendererTurbo(this, 218, 34, textureX, textureY); // Box 50
		gunModel[94] = new ModelRendererTurbo(this, 256, 70, textureX, textureY); // Box 51
		gunModel[95] = new ModelRendererTurbo(this, 57, 40, textureX, textureY); // Box 55
		gunModel[96] = new ModelRendererTurbo(this, 57, 55, textureX, textureY); // Box 56
		gunModel[97] = new ModelRendererTurbo(this, 74, 52, textureX, textureY); // Box 57
		gunModel[98] = new ModelRendererTurbo(this, 218, 56, textureX, textureY); // Box 58
		gunModel[99] = new ModelRendererTurbo(this, 230, 65, textureX, textureY); // Box 59
		gunModel[100] = new ModelRendererTurbo(this, 237, 65, textureX, textureY); // Box 60
		gunModel[101] = new ModelRendererTurbo(this, 197, 65, textureX, textureY); // Box 61
		gunModel[102] = new ModelRendererTurbo(this, 57, 71, textureX, textureY); // Box 62
		gunModel[103] = new ModelRendererTurbo(this, 57, 71, textureX, textureY); // Box 63
		gunModel[104] = new ModelRendererTurbo(this, 219, 27, textureX, textureY); // Box 84
		gunModel[105] = new ModelRendererTurbo(this, 206, 41, textureX, textureY); // Box 85
		gunModel[106] = new ModelRendererTurbo(this, 216, 50, textureX, textureY); // Box 86
		gunModel[107] = new ModelRendererTurbo(this, 206, 29, textureX, textureY); // Box 88
		gunModel[108] = new ModelRendererTurbo(this, 206, 44, textureX, textureY); // Box 89
		gunModel[109] = new ModelRendererTurbo(this, 206, 47, textureX, textureY); // Box 90
		gunModel[110] = new ModelRendererTurbo(this, 193, 50, textureX, textureY); // Box 91

		gunModel[0].addBox(0F, 0F, 0F, 35, 3, 7, 0F); // railPart3
		gunModel[0].setRotationPoint(-5F, -22F, -3.5F);

		gunModel[1].addShapeBox(0F, 0F, 0F, 4, 6, 2, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 15
		gunModel[1].setRotationPoint(53F, -12.5F, -11F);

		gunModel[2].addBox(0F, 0F, 0F, 4, 6, 2, 0F); // Box 16
		gunModel[2].setRotationPoint(53F, -12.5F, -9F);

		gunModel[3].addShapeBox(0F, 0F, 0F, 4, 6, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F); // Box 17
		gunModel[3].setRotationPoint(53F, -12.5F, -7F);

		gunModel[4].addBox(0F, 0F, 0F, 10, 13, 9, 0F); // Box 6
		gunModel[4].setRotationPoint(-12F, -19F, -4.5F);

		gunModel[5].addShapeBox(0F, 0F, 0F, 7, 3, 9, 0F, -3F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, -3F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 8
		gunModel[5].setRotationPoint(-12F, -22F, -4.5F);

		gunModel[6].addBox(0F, 0F, 0F, 17, 15, 9, 0F); // Box 9
		gunModel[6].setRotationPoint(-2F, -19F, -4.5F);

		gunModel[7].addBox(0F, 0F, 0F, 18, 14, 9, 0F); // Box 10
		gunModel[7].setRotationPoint(15F, -19F, -4.5F);

		gunModel[8].addBox(0F, 0F, 0F, 18, 4, 1, 0F); // Box 17
		gunModel[8].setRotationPoint(15F, -5F, 3.5F);

		gunModel[9].addBox(0F, 0F, 0F, 18, 4, 1, 0F); // Box 18
		gunModel[9].setRotationPoint(15F, -5F, -4.5F);

		gunModel[10].addBox(0F, 0F, 0F, 1, 5, 7, 0F); // Box 20
		gunModel[10].setRotationPoint(32F, -5F, -3.5F);

		gunModel[11].addShapeBox(0F, 0F, 0F, 17, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F); // Box 21
		gunModel[11].setRotationPoint(16F, -1F, 3.5F);

		gunModel[12].addShapeBox(0F, 0F, 0F, 17, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F); // Box 22
		gunModel[12].setRotationPoint(16F, -1F, -4.5F);

		gunModel[13].addBox(0F, 0F, 0F, 2, 5, 10, 0F); // Box 24
		gunModel[13].setRotationPoint(14.5F, -3F, -5F);

		gunModel[14].addBox(0F, 0F, 0F, 1, 4, 9, 0F); // Box 25
		gunModel[14].setRotationPoint(15F, -1F, -4.5F);

		gunModel[15].addShapeBox(0F, 0F, 0F, 3, 4, 9, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F); // Box 26
		gunModel[15].setRotationPoint(30F, -23F, -4.5F);

		gunModel[16].addBox(0F, 0F, 0F, 31, 6, 12, 0F); // Box 28
		gunModel[16].setRotationPoint(33F, -18.5F, -6F);

		gunModel[17].addShapeBox(0F, 0F, 0F, 5, 3, 7, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F); // Box 30
		gunModel[17].setRotationPoint(33F, -5.5F, -3.5F);

		gunModel[18].addShapeBox(0F, 0F, 0F, 30, 2, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 1
		gunModel[18].setRotationPoint(33F, -7.5F, -4.5F);

		gunModel[19].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 3
		gunModel[19].setRotationPoint(9F, -24F, -3.5F);

		gunModel[20].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 4
		gunModel[20].setRotationPoint(3F, -24F, -3.5F);

		gunModel[21].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 6
		gunModel[21].setRotationPoint(27F, -24F, -3.5F);

		gunModel[22].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 7
		gunModel[22].setRotationPoint(21F, -24F, -3.5F);

		gunModel[23].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 8
		gunModel[23].setRotationPoint(15F, -24F, -3.5F);

		gunModel[24].addBox(0F, 0F, 0F, 25, 2, 5, 0F); // Box 27
		gunModel[24].setRotationPoint(33F, -23F, -2.5F);

		gunModel[25].addShapeBox(0F, 0F, 0F, 5, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F); // Box 28
		gunModel[25].setRotationPoint(58F, -23.5F, -3F);

		gunModel[26].addShapeBox(0F, 0F, 0F, 6, 3, 9, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 49
		gunModel[26].setRotationPoint(-4F, -25F, -4.5F);

		gunModel[27].addBox(0F, 0F, 0F, 6, 2, 9, 0F); // Box 50
		gunModel[27].setRotationPoint(-4F, -22F, -4.5F);

		gunModel[28].addBox(0F, 0F, 0F, 8, 4, 1, 0F); // Box 64
		gunModel[28].setRotationPoint(24F, -5F, 4F);

		gunModel[29].addBox(0F, 0F, 0F, 8, 7, 2, 0F); // Box 67
		gunModel[29].setRotationPoint(12F, -18.5F, 4.5F);

		gunModel[30].addBox(0F, 0F, 0F, 2, 6, 2, 0F); // Box 68
		gunModel[30].setRotationPoint(13.5F, -18F, 6.5F);

		gunModel[31].addShapeBox(-2F, -4F, 0F, 2, 8, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F); // Box 69
		gunModel[31].setRotationPoint(14.5F, -15F, 8.5F);
		gunModel[31].rotateAngleZ = -0.43633231F;

		gunModel[32].addShapeBox(-0.5F, 0.5F, 0.5F, 1, 1, 2, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 72
		gunModel[32].setRotationPoint(13.5F, -18F, 6F);

		gunModel[33].addShapeBox(-0.5F, 1.5F, 0.5F, 1, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 73
		gunModel[33].setRotationPoint(13.5F, -18F, 6F);

		gunModel[34].addShapeBox(-1F, -4F, 1F, 1, 24, 36, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -24F, 0F, 0F, -24F, 0F, -16F, 0F, 0F, -16F, 0F, 0F, -16F, -24F, 0F, -16F, -24F); // Box 74
		gunModel[34].setRotationPoint(14.5F, -15F, 8.5F);
		gunModel[34].rotateAngleZ = -0.43633231F;

		gunModel[35].addBox(-2F, -5F, 1F, 2, 1, 12, 0F); // Box 78
		gunModel[35].setRotationPoint(14.5F, -15F, 8.5F);
		gunModel[35].rotateAngleZ = -0.43633231F;

		gunModel[36].addShapeBox(0F, 0F, 0F, 3, 1, 1, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 79
		gunModel[36].setRotationPoint(-1F, -8F, -5F);

		gunModel[37].addShapeBox(0F, 0F, 0F, 3, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 80
		gunModel[37].setRotationPoint(-1F, -6F, -5F);

		gunModel[38].addBox(0F, 0F, 0F, 3, 1, 1, 0F); // Box 81
		gunModel[38].setRotationPoint(-1F, -7F, -5F);

		gunModel[39].addBox(0F, 0F, 0F, 1, 1, 1, 0F); // Box 82
		gunModel[39].setRotationPoint(2F, -7F, -5F);

		gunModel[40].addShapeBox(0F, 0F, 0F, 2, 1, 10, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 83
		gunModel[40].setRotationPoint(34F, -8.5F, -5F);

		gunModel[41].addShapeBox(0F, 0F, 0F, 2, 1, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 84
		gunModel[41].setRotationPoint(34F, -7.5F, -5F);

		gunModel[42].addShapeBox(0F, 0F, 0F, 2, 1, 10, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 85
		gunModel[42].setRotationPoint(38F, -8.5F, -5F);

		gunModel[43].addShapeBox(0F, 0F, 0F, 2, 1, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 86
		gunModel[43].setRotationPoint(38F, -7.5F, -5F);

		gunModel[44].addBox(-2F, 4F, 1F, 2, 1, 12, 0F); // Box 134
		gunModel[44].setRotationPoint(14.5F, -15F, 8.5F);
		gunModel[44].rotateAngleZ = -0.43633231F;

		gunModel[45].addShapeBox(-2F, -4F, 13F, 2, 8, 1, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 135
		gunModel[45].setRotationPoint(14.5F, -15F, 8.5F);
		gunModel[45].rotateAngleZ = -0.43633231F;

		gunModel[46].addShapeBox(-2F, 1F, 1F, 2, 4, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 136
		gunModel[46].setRotationPoint(14.5F, -15F, 8.5F);
		gunModel[46].rotateAngleZ = -0.43633231F;

		gunModel[47].addBox(-2.3F, 3F, 2.5F, 1, 1, 1, 0F); // Box 137
		gunModel[47].setRotationPoint(14.5F, -15F, 8.5F);
		gunModel[47].rotateAngleZ = -0.43633231F;

		gunModel[48].addBox(-2.3F, 3F, 1F, 1, 1, 1, 0F); // Box 138
		gunModel[48].setRotationPoint(14.5F, -15F, 8.5F);
		gunModel[48].rotateAngleZ = -0.43633231F;

		gunModel[49].addShapeBox(0F, 0F, 0F, 31, 3, 12, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 0
		gunModel[49].setRotationPoint(33F, -21.5F, -6F);

		gunModel[50].addShapeBox(0F, 0F, 0F, 31, 3, 12, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 1
		gunModel[50].setRotationPoint(33F, -12.5F, -6F);

		gunModel[51].addBox(0F, 0F, 0F, 35, 5, 2, 0F); // Box 4
		gunModel[51].setRotationPoint(64F, -15F, -1F);

		gunModel[52].addShapeBox(0F, 0F, 0F, 35, 5, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0.5F, 0F, -1.5F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0.5F, 0F, -1.5F, 0.5F); // Box 4
		gunModel[52].setRotationPoint(64F, -15F, 1F);

		gunModel[53].addShapeBox(0F, 0F, 0F, 35, 5, 1, 0F, 0F, -1.5F, 0.5F, 0F, -1.5F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0.5F, 0F, -1.5F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 4
		gunModel[53].setRotationPoint(64F, -15F, -2F);

		gunModel[54].addBox(0F, 0F, 0F, 30, 2, 9, 0F); // Box 6
		gunModel[54].setRotationPoint(33F, -9.5F, -4.5F);

		gunModel[55].addShapeBox(0F, 0F, 0F, 3, 5, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0.5F, 0F, -1.5F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0.5F, 0F, -1.5F, 0.5F); // Box 7
		gunModel[55].setRotationPoint(64F, -21.5F, 1F);

		gunModel[56].addShapeBox(0F, 0F, 0F, 3, 5, 1, 0F, 0F, -1.5F, 0.5F, 0F, -1.5F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0.5F, 0F, -1.5F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 8
		gunModel[56].setRotationPoint(64F, -21.5F, -2F);

		gunModel[57].addBox(0F, 0F, 0F, 3, 5, 2, 0F); // Box 9
		gunModel[57].setRotationPoint(64F, -21.5F, -1F);

		gunModel[58].addShapeBox(0F, 0F, 0F, 9, 5, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0.5F, 0F, -1.5F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0.5F, 0F, -1.5F, 0.5F); // Box 10
		gunModel[58].setRotationPoint(49F, -12F, -7F);

		gunModel[59].addShapeBox(0F, 0F, 0F, 9, 5, 1, 0F, 0F, -1.5F, 0.5F, 0F, -1.5F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0.5F, 0F, -1.5F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 11
		gunModel[59].setRotationPoint(49F, -12F, -10F);

		gunModel[60].addBox(0F, 0F, 0F, 9, 5, 2, 0F); // Box 12
		gunModel[60].setRotationPoint(49F, -12F, -9F);

		gunModel[61].addShapeBox(0F, 0F, 0F, 8, 6, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F); // Box 13
		gunModel[61].setRotationPoint(58F, -12.5F, -7F);

		gunModel[62].addBox(0F, 0F, 0F, 8, 6, 2, 0F); // Box 14
		gunModel[62].setRotationPoint(58F, -12.5F, -9F);

		gunModel[63].addShapeBox(0F, 0F, 0F, 8, 6, 2, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 15
		gunModel[63].setRotationPoint(58F, -12.5F, -11F);

		gunModel[64].addShapeBox(0F, 0F, 0F, 20, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F); // Box 17
		gunModel[64].setRotationPoint(38F, -4.5F, -3F);

		gunModel[65].addBox(0F, 0F, 0F, 20, 1, 6, 0F); // Box 18
		gunModel[65].setRotationPoint(38F, -5.5F, -3F);

		gunModel[66].addShapeBox(0F, 0F, 0F, 4, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, -3F, 0F, -1.5F, -3F, 0F, -1.5F, 0F, 0F, -1.5F); // Box 19
		gunModel[66].setRotationPoint(58F, -4.5F, -3F);

		gunModel[67].addBox(0F, 0F, 0F, 4, 1, 6, 0F); // Box 20
		gunModel[67].setRotationPoint(58F, -5.5F, -3F);

		gunModel[68].addShapeBox(0F, 0F, 0F, 3, 6, 2, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 21
		gunModel[68].setRotationPoint(64F, -15.5F, -3F);

		gunModel[69].addShapeBox(0F, 0F, 0F, 3, 6, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F); // Box 22
		gunModel[69].setRotationPoint(64F, -15.5F, 1F);

		gunModel[70].addBox(0F, 0F, 0F, 3, 6, 2, 0F); // Box 23
		gunModel[70].setRotationPoint(64F, -15.5F, -1F);

		gunModel[71].addShapeBox(0F, 0F, 0F, 2, 6, 2, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 24
		gunModel[71].setRotationPoint(69F, -15.5F, -3F);

		gunModel[72].addShapeBox(0F, 0F, 0F, 2, 6, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F); // Box 25
		gunModel[72].setRotationPoint(69F, -15.5F, 1F);

		gunModel[73].addBox(0F, 0F, 0F, 2, 6, 2, 0F); // Box 26
		gunModel[73].setRotationPoint(69F, -15.5F, -1F);

		gunModel[74].addShapeBox(0F, 0F, 0F, 9, 7, 2, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 27
		gunModel[74].setRotationPoint(71F, -16F, -3.5F);

		gunModel[75].addShapeBox(0F, 0F, 0F, 9, 7, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F); // Box 28
		gunModel[75].setRotationPoint(71F, -16F, 1.5F);

		gunModel[76].addBox(0F, 0F, 0F, 9, 7, 3, 0F); // Box 29
		gunModel[76].setRotationPoint(71F, -16F, -1.5F);

		gunModel[77].addBox(0F, 0F, 0F, 10, 2, 8, 0F); // Box 31
		gunModel[77].setRotationPoint(-12F, -6F, -4F);

		gunModel[78].addShapeBox(0F, 0F, 0F, 13, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F); // Box 32
		gunModel[78].setRotationPoint(-12F, -4F, -4F);

		gunModel[79].addBox(0F, 0F, 0F, 6, 3, 8, 0F); // Box 33
		gunModel[79].setRotationPoint(1F, -4F, -4F);

		gunModel[80].addShapeBox(0F, 0F, 0F, 9, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 34
		gunModel[80].setRotationPoint(-8F, -1F, -4F);

		gunModel[81].addShapeBox(0F, 0F, 0F, 9, 5, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F); // Box 35
		gunModel[81].setRotationPoint(-8F, 2F, -4F);

		gunModel[82].addShapeBox(0F, 0F, 0F, 9, 7, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, -3F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 3F, -3F, 0F); // Box 36
		gunModel[82].setRotationPoint(-10F, 7F, -4F);

		gunModel[83].addShapeBox(0F, 0F, 0F, 9, 2, 8, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F); // Box 38
		gunModel[83].setRotationPoint(-13F, 14F, -4F);

		gunModel[84].addShapeBox(0F, 0F, 0F, 2, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F); // Box 39
		gunModel[84].setRotationPoint(1F, -1F, -4F);

		gunModel[85].addShapeBox(0F, 0F, 0F, 2, 5, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, -1.5F, -2F, 0F, -1.5F, 2F, 0F, 0F); // Box 40
		gunModel[85].setRotationPoint(1F, 2F, -4F);

		gunModel[86].addShapeBox(0F, 0F, 0F, 2, 7, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, -1.5F, -3F, 0F, -1.5F, 3F, 0F, 0F); // Box 41
		gunModel[86].setRotationPoint(-1F, 7F, -4F);

		gunModel[87].addShapeBox(0F, 0F, 0F, 2, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F); // Box 42
		gunModel[87].setRotationPoint(-4F, 14F, -4F);

		gunModel[88].addBox(0F, 0F, 0F, 3, 3, 10, 0F); // Box 43
		gunModel[88].setRotationPoint(13.5F, -6F, -5F);

		gunModel[89].addShapeBox(0F, 0F, 0F, 18, 2, 12, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 46
		gunModel[89].setRotationPoint(15F, -19F, -6F);

		gunModel[90].addShapeBox(0F, 0F, 0F, 18, 2, 12, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 47
		gunModel[90].setRotationPoint(15F, -14F, -6F);

		gunModel[91].addBox(0F, 0F, 0F, 18, 3, 10, 0F); // Box 48
		gunModel[91].setRotationPoint(15F, -17F, -4F);

		gunModel[92].addShapeBox(0F, 0F, 0F, 2, 2, 12, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 49
		gunModel[92].setRotationPoint(13F, -19F, -6F);

		gunModel[93].addBox(0F, 0F, 0F, 2, 3, 12, 0F); // Box 50
		gunModel[93].setRotationPoint(13F, -17F, -6F);

		gunModel[94].addShapeBox(0F, 0F, 0F, 2, 2, 12, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 51
		gunModel[94].setRotationPoint(13F, -14F, -6F);

		gunModel[95].addShapeBox(0F, 0F, 0F, 2, 2, 12, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 55
		gunModel[95].setRotationPoint(10F, -19F, -6F);

		gunModel[96].addBox(0F, 0F, 0F, 2, 3, 12, 0F); // Box 56
		gunModel[96].setRotationPoint(10F, -17F, -6F);

		gunModel[97].addShapeBox(0F, 0F, 0F, 2, 2, 12, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 57
		gunModel[97].setRotationPoint(10F, -14F, -6F);

		gunModel[98].addBox(0F, 0F, 0F, 3, 3, 2, 0F); // Box 58
		gunModel[98].setRotationPoint(30F, -17F, -6F);

		gunModel[99].addShapeBox(0F, 0F, 0F, 2, 3, 1, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 59
		gunModel[99].setRotationPoint(15F, -17F, -6F);

		gunModel[100].addShapeBox(0F, 0F, 0F, 2, 3, 1, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 60
		gunModel[100].setRotationPoint(28F, -17F, -6F);

		gunModel[101].addBox(0F, 0F, 0F, 15, 3, 1, 0F); // Box 61
		gunModel[101].setRotationPoint(15F, -17F, -5F);

		gunModel[102].addShapeBox(0F, 0F, 0F, 2, 1, 10, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 62
		gunModel[102].setRotationPoint(60F, -8.5F, -5F);

		gunModel[103].addShapeBox(0F, 0F, 0F, 2, 1, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 63
		gunModel[103].setRotationPoint(60F, -7.5F, -5F);

		gunModel[104].addBox(0F, 0F, 0F, 12, 1, 1, 0F); // Box 84
		gunModel[104].setRotationPoint(32F, -6F, -5.5F);

		gunModel[105].addShapeBox(0F, 0F, 0F, 4, 1, 1, 0F, 0F, 0F, -3F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 0F, 3F); // Box 85
		gunModel[105].setRotationPoint(44F, -6F, -8.5F);

		gunModel[106].addBox(0F, 0F, 0F, 1, 2, 2, 0F); // Box 86
		gunModel[106].setRotationPoint(48F, -10.5F, -9F);

		gunModel[107].addShapeBox(0F, 0F, 0F, 3, 5, 5, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 88
		gunModel[107].setRotationPoint(53.5F, -15.5F, -9F);

		gunModel[108].addShapeBox(0F, 0F, 0F, 3, 1, 1, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F); // Box 89
		gunModel[108].setRotationPoint(29F, -6F, -5.5F);

		gunModel[109].addBox(0F, 0F, 0F, 2, 1, 1, 0F); // Box 90
		gunModel[109].setRotationPoint(27F, -8F, -5.5F);

		gunModel[110].addShapeBox(0F, 0F, 0F, 9, 2, 2, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 91
		gunModel[110].setRotationPoint(18F, -8.5F, -6F);


		defaultScopeModel = new ModelRendererTurbo[13];
		defaultScopeModel[0] = new ModelRendererTurbo(this, 135, 150, textureX, textureY); // Box 42
		defaultScopeModel[1] = new ModelRendererTurbo(this, 135, 150, textureX, textureY); // Box 43
		defaultScopeModel[2] = new ModelRendererTurbo(this, 112, 149, textureX, textureY); // Box 44
		defaultScopeModel[3] = new ModelRendererTurbo(this, 154, 153, textureX, textureY); // Box 45
		defaultScopeModel[4] = new ModelRendererTurbo(this, 154, 153, textureX, textureY); // Box 46
		defaultScopeModel[5] = new ModelRendererTurbo(this, 147, 153, textureX, textureY); // Box 48
		defaultScopeModel[6] = new ModelRendererTurbo(this, 74, 139, textureX, textureY); // Box 51
		defaultScopeModel[7] = new ModelRendererTurbo(this, 74, 139, textureX, textureY); // Box 52
		defaultScopeModel[8] = new ModelRendererTurbo(this, 82, 148, textureX, textureY); // Box 53
		defaultScopeModel[9] = new ModelRendererTurbo(this, 82, 158, textureX, textureY); // Box 60
		defaultScopeModel[10] = new ModelRendererTurbo(this, 82, 158, textureX, textureY); // Box 61
		defaultScopeModel[11] = new ModelRendererTurbo(this, 82, 158, textureX, textureY); // Box 62
		defaultScopeModel[12] = new ModelRendererTurbo(this, 82, 158, textureX, textureY); // Box 63

		defaultScopeModel[0].addShapeBox(0F, 0F, 0F, 2, 1, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 42
		defaultScopeModel[0].setRotationPoint(59F, -24F, -3.5F);

		defaultScopeModel[1].addShapeBox(0F, 0F, 0F, 2, 1, 7, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 43
		defaultScopeModel[1].setRotationPoint(59F, -25F, -3.5F);

		defaultScopeModel[2].addShapeBox(0F, 0F, 0F, 5, 3, 6, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 44
		defaultScopeModel[2].setRotationPoint(58F, -26.5F, -3F);

		defaultScopeModel[3].addShapeBox(0F, 0F, 0F, 4, 4, 1, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 45
		defaultScopeModel[3].setRotationPoint(58F, -30.5F, -3F);

		defaultScopeModel[4].addShapeBox(0F, 0F, 0F, 4, 4, 1, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 46
		defaultScopeModel[4].setRotationPoint(58F, -30.5F, 2F);

		defaultScopeModel[5].addShapeBox(0F, 0F, 0F, 2, 2, 1, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 48
		defaultScopeModel[5].setRotationPoint(60F, -28.5F, -0.5F);

		defaultScopeModel[6].addShapeBox(0F, 0F, 0F, 6, 3, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 51
		defaultScopeModel[6].setRotationPoint(-4F, -30F, -3.5F);

		defaultScopeModel[7].addShapeBox(0F, 0F, 0F, 6, 3, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 52
		defaultScopeModel[7].setRotationPoint(-4F, -30F, 0.5F);

		defaultScopeModel[8].addBox(0F, 0F, 0F, 6, 2, 7, 0F); // Box 53
		defaultScopeModel[8].setRotationPoint(-4F, -27F, -3.5F);

		defaultScopeModel[9].addShapeBox(0F, 0F, 0F, 7, 1, 2, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 60
		defaultScopeModel[9].setRotationPoint(-4.5F, -26.5F, -3F);

		defaultScopeModel[10].addShapeBox(0F, 0F, 0F, 7, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 61
		defaultScopeModel[10].setRotationPoint(-4.5F, -25.5F, -3F);

		defaultScopeModel[11].addShapeBox(0F, 0F, 0F, 7, 1, 2, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 62
		defaultScopeModel[11].setRotationPoint(-4.5F, -26.5F, 1F);

		defaultScopeModel[12].addShapeBox(0F, 0F, 0F, 7, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 63
		defaultScopeModel[12].setRotationPoint(-4.5F, -25.5F, 1F);


		defaultStockModel = new ModelRendererTurbo[18];
		defaultStockModel[0] = new ModelRendererTurbo(this, 48, 143, textureX, textureY); // Box 64
		defaultStockModel[1] = new ModelRendererTurbo(this, 1, 139, textureX, textureY); // Box 65
		defaultStockModel[2] = new ModelRendererTurbo(this, 1, 171, textureX, textureY); // Box 66
		defaultStockModel[3] = new ModelRendererTurbo(this, 1, 159, textureX, textureY); // Box 67
		defaultStockModel[4] = new ModelRendererTurbo(this, 65, 148, textureX, textureY); // Box 68
		defaultStockModel[5] = new ModelRendererTurbo(this, 65, 148, textureX, textureY); // Box 69
		defaultStockModel[6] = new ModelRendererTurbo(this, 83, 168, textureX, textureY); // Box 70
		defaultStockModel[7] = new ModelRendererTurbo(this, 95, 187, textureX, textureY); // Box 71
		defaultStockModel[8] = new ModelRendererTurbo(this, 1, 206, textureX, textureY); // Box 72
		defaultStockModel[9] = new ModelRendererTurbo(this, 1, 195, textureX, textureY); // Box 74
		defaultStockModel[10] = new ModelRendererTurbo(this, 1, 183, textureX, textureY); // Box 75
		defaultStockModel[11] = new ModelRendererTurbo(this, 48, 187, textureX, textureY); // Box 76
		defaultStockModel[12] = new ModelRendererTurbo(this, 102, 217, textureX, textureY); // Box 78
		defaultStockModel[13] = new ModelRendererTurbo(this, 73, 221, textureX, textureY); // Box 79
		defaultStockModel[14] = new ModelRendererTurbo(this, 48, 211, textureX, textureY); // Box 80
		defaultStockModel[15] = new ModelRendererTurbo(this, 73, 209, textureX, textureY); // Box 81
		defaultStockModel[16] = new ModelRendererTurbo(this, 48, 199, textureX, textureY); // Box 82
		defaultStockModel[17] = new ModelRendererTurbo(this, 48, 175, textureX, textureY); // Box 83

		defaultStockModel[0].addBox(0F, 0F, 0F, 1, 7, 7, 0F); // Box 64
		defaultStockModel[0].setRotationPoint(-13F, -16F, -3.5F);

		defaultStockModel[1].addBox(0F, 0F, 0F, 14, 6, 9, 0F); // Box 65
		defaultStockModel[1].setRotationPoint(-27F, -14F, -4.5F);

		defaultStockModel[2].addShapeBox(0F, 0F, 0F, 14, 2, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 66
		defaultStockModel[2].setRotationPoint(-27F, -8F, -4.5F);

		defaultStockModel[3].addShapeBox(0F, 0F, 0F, 20, 2, 9, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 67
		defaultStockModel[3].setRotationPoint(-47F, -19F, -4.5F);

		defaultStockModel[4].addShapeBox(0F, 0F, 0F, 1, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 68
		defaultStockModel[4].setRotationPoint(-13F, -18F, -3.5F);

		defaultStockModel[5].addShapeBox(0F, 0F, 0F, 1, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 69
		defaultStockModel[5].setRotationPoint(-13F, -9F, -3.5F);

		defaultStockModel[6].addShapeBox(0F, 0F, 0F, 6, 9, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 10F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 10F, 0F); // Box 70
		defaultStockModel[6].setRotationPoint(-33F, -17F, -4.5F);

		defaultStockModel[7].addShapeBox(0F, 0F, 0F, 6, 2, 9, 0F, 0F, -10F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -10F, 0F, 0F, 10F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 10F, -2F); // Box 71
		defaultStockModel[7].setRotationPoint(-33F, -8F, -4.5F);

		defaultStockModel[8].addBox(0F, 0F, 0F, 14, 19, 9, 0F); // Box 72
		defaultStockModel[8].setRotationPoint(-47F, -17F, -4.5F);

		defaultStockModel[9].addShapeBox(0F, 0F, 0F, 14, 2, 8, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 74
		defaultStockModel[9].setRotationPoint(-27F, -18.5F, -4F);

		defaultStockModel[10].addBox(0F, 0F, 0F, 14, 3, 8, 0F); // Box 75
		defaultStockModel[10].setRotationPoint(-27F, -16.5F, -4F);

		defaultStockModel[11].addShapeBox(0F, 0F, 0F, 14, 2, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 76
		defaultStockModel[11].setRotationPoint(-47F, 2F, -4.5F);

		defaultStockModel[12].addBox(0F, 0F, 0F, 4, 7, 10, 0F); // Box 78
		defaultStockModel[12].setRotationPoint(-49F, -18.5F, -5F);

		defaultStockModel[13].addShapeBox(0F, 0F, 0F, 4, 3, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F); // Box 79
		defaultStockModel[13].setRotationPoint(-49F, -11.5F, -5F);

		defaultStockModel[14].addBox(0F, 0F, 0F, 2, 13, 10, 0F); // Box 80
		defaultStockModel[14].setRotationPoint(-49F, -8.5F, -5F);

		defaultStockModel[15].addShapeBox(0F, 0F, 0F, 4, 1, 10, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 81
		defaultStockModel[15].setRotationPoint(-49F, -19.5F, -5F);

		defaultStockModel[16].addShapeBox(0F, 0F, 0F, 2, 1, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 82
		defaultStockModel[16].setRotationPoint(-49F, 4.5F, -5F);

		defaultStockModel[17].addBox(0F, 0F, 0F, 10, 4, 7, 0F); // Box 83
		defaultStockModel[17].setRotationPoint(-47F, 2F, -3.5F);


		ammoModel = new ModelRendererTurbo[16];
		ammoModel[0] = new ModelRendererTurbo(this, 256, 16, textureX, textureY); // Box 29
		ammoModel[1] = new ModelRendererTurbo(this, 256, 2, textureX, textureY); // Box 30
		ammoModel[2] = new ModelRendererTurbo(this, 289, 3, textureX, textureY); // Box 31
		ammoModel[3] = new ModelRendererTurbo(this, 289, 22, textureX, textureY); // Box 32
		ammoModel[4] = new ModelRendererTurbo(this, 256, 45, textureX, textureY); // Box 33
		ammoModel[5] = new ModelRendererTurbo(this, 273, 59, textureX, textureY); // Box 34
		ammoModel[6] = new ModelRendererTurbo(this, 273, 51, textureX, textureY); // Box 35
		ammoModel[7] = new ModelRendererTurbo(this, 273, 59, textureX, textureY); // Box 36
		ammoModel[8] = new ModelRendererTurbo(this, 290, 45, textureX, textureY); // Box 40
		ammoModel[9] = new ModelRendererTurbo(this, 273, 45, textureX, textureY); // Box 41
		ammoModel[10] = new ModelRendererTurbo(this, 165, 149, textureX, textureY); // Box 54
		ammoModel[11] = new ModelRendererTurbo(this, 165, 149, textureX, textureY); // Box 55
		ammoModel[12] = new ModelRendererTurbo(this, 165, 149, textureX, textureY); // Box 56
		ammoModel[13] = new ModelRendererTurbo(this, 165, 149, textureX, textureY); // Box 57
		ammoModel[14] = new ModelRendererTurbo(this, 165, 149, textureX, textureY); // Box 58
		ammoModel[15] = new ModelRendererTurbo(this, 165, 149, textureX, textureY); // Box 59

		ammoModel[0].addBox(0F, 0F, -3.5F, 8, 20, 7, 0F); // Box 29
		ammoModel[0].setRotationPoint(15.5F, -2F, 0F);
		ammoModel[0].rotateAngleZ = 0.17453293F;

		ammoModel[1].addBox(8F, 15F, -3.5F, 8, 5, 7, 0F); // Box 30
		ammoModel[1].setRotationPoint(15.5F, -2F, 0F);
		ammoModel[1].rotateAngleZ = 0.17453293F;

		ammoModel[2].addBox(-0.5F, 20F, -4F, 17, 3, 8, 0F); // Box 31
		ammoModel[2].setRotationPoint(15.5F, -2F, 0F);
		ammoModel[2].rotateAngleZ = 0.17453293F;

		ammoModel[3].addBox(8F, 0F, -3F, 7, 15, 6, 0F); // Box 32
		ammoModel[3].setRotationPoint(15.5F, -2F, 0F);
		ammoModel[3].rotateAngleZ = 0.17453293F;

		ammoModel[4].addShapeBox(15F, 0F, -3F, 1, 15, 6, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 33
		ammoModel[4].setRotationPoint(15.5F, -2F, 0F);
		ammoModel[4].rotateAngleZ = 0.17453293F;

		ammoModel[5].addBox(1.5F, 23F, -2.5F, 2, 3, 5, 0F); // Box 34
		ammoModel[5].setRotationPoint(15.5F, -2F, 0F);
		ammoModel[5].rotateAngleZ = 0.17453293F;

		ammoModel[6].addShapeBox(1.5F, 26F, -2.5F, 13, 2, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 35
		ammoModel[6].setRotationPoint(15.5F, -2F, 0F);
		ammoModel[6].rotateAngleZ = 0.17453293F;

		ammoModel[7].addBox(12.5F, 23F, -2.5F, 2, 3, 5, 0F); // Box 36
		ammoModel[7].setRotationPoint(15.5F, -2F, 0F);
		ammoModel[7].rotateAngleZ = 0.17453293F;

		ammoModel[8].addShapeBox(1F, -1F, -2F, 10, 1, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 40
		ammoModel[8].setRotationPoint(15.5F, -2F, 0F);
		ammoModel[8].rotateAngleZ = 0.17453293F;

		ammoModel[9].addShapeBox(11F, -1F, -2F, 4, 1, 4, 0F, 0F, 0F, -1F, -1F, 0F, -1.5F, -1F, 0F, -1.5F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 41
		ammoModel[9].setRotationPoint(15.5F, -2F, 0F);
		ammoModel[9].rotateAngleZ = 0.17453293F;

		ammoModel[10].addShapeBox(1F, 1F, -4F, 2, 1, 8, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 54
		ammoModel[10].setRotationPoint(15.5F, -2F, 0F);
		ammoModel[10].rotateAngleZ = 0.17453293F;

		ammoModel[11].addShapeBox(1F, 2F, -4F, 2, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 55
		ammoModel[11].setRotationPoint(15.5F, -2F, 0F);
		ammoModel[11].rotateAngleZ = 0.17453293F;

		ammoModel[12].addShapeBox(1F, 17F, -4F, 2, 1, 8, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 56
		ammoModel[12].setRotationPoint(15.5F, -2F, 0F);
		ammoModel[12].rotateAngleZ = 0.17453293F;

		ammoModel[13].addShapeBox(1F, 18F, -4F, 2, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 57
		ammoModel[13].setRotationPoint(15.5F, -2F, 0F);
		ammoModel[13].rotateAngleZ = 0.17453293F;

		ammoModel[14].addShapeBox(1F, 9.5F, -4F, 2, 1, 8, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 58
		ammoModel[14].setRotationPoint(15.5F, -2F, 0F);
		ammoModel[14].rotateAngleZ = 0.17453293F;

		ammoModel[15].addShapeBox(1F, 10.5F, -4F, 2, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 59
		ammoModel[15].setRotationPoint(15.5F, -2F, 0F);
		ammoModel[15].rotateAngleZ = 0.17453293F;


		slideModel = new ModelRendererTurbo[1];
		slideModel[0] = new ModelRendererTurbo(this, 196, 70, textureX, textureY); // Box 92

		slideModel[0].addShapeBox(0F, 0F, 0F, 4, 3, 2, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 92
		slideModel[0].setRotationPoint(25F, -17F, -7F);


		barrelAttachPoint = new Vector3f(100F /16F, 16F /16F, 0F /16F);
		stockAttachPoint = new Vector3f(-12F /16F, 12F /16F, 0F /16F);
		scopeAttachPoint = new Vector3f(16F /16F, 20F /16F, 0F /16F);

		gunSlideDistance = 0.5F;
		animationType = EnumAnimationType.BOTTOM_CLIP;

		translateAll(0F, 0F, 0F);


		flipAll();
	}
}