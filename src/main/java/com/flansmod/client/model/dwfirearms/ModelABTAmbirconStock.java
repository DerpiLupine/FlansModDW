package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.ModelAttachment;
import com.flansmod.client.tmt.ModelRendererTurbo;

import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelABTAmbirconStock extends ModelAttachment
{
	int textureX = 512;
	int textureY = 256;

	public ModelABTAmbirconStock()
	{
		attachmentModel = new ModelRendererTurbo[10];
		attachmentModel[0] = new ModelRendererTurbo(this, 1, 59, textureX, textureY); // stock1
		attachmentModel[1] = new ModelRendererTurbo(this, 49, 85, textureX, textureY); // stock10
		attachmentModel[2] = new ModelRendererTurbo(this, 24, 84, textureX, textureY); // stock2
		attachmentModel[3] = new ModelRendererTurbo(this, 1, 43, textureX, textureY); // stock3
		attachmentModel[4] = new ModelRendererTurbo(this, 1, 71, textureX, textureY); // stock5
		attachmentModel[5] = new ModelRendererTurbo(this, 39, 106, textureX, textureY); // stock6
		attachmentModel[6] = new ModelRendererTurbo(this, 24, 97, textureX, textureY); // stock7
		attachmentModel[7] = new ModelRendererTurbo(this, 1, 84, textureX, textureY); // stock8
		attachmentModel[8] = new ModelRendererTurbo(this, 68, 107, textureX, textureY); // stock9
		attachmentModel[9] = new ModelRendererTurbo(this, 56, 105, textureX, textureY); // Box 0

		attachmentModel[0].addBox(-29F, -4F, -3.5F, 29, 4, 7, 0F); // stock1
		attachmentModel[0].setRotationPoint(0F, 0F, 0F);

		attachmentModel[1].addShapeBox(-32F, 13F, -4.5F, 2, 2, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // stock10
		attachmentModel[1].setRotationPoint(0F, 0F, 0F);

		attachmentModel[2].addBox(-1F, 2F, -5F, 2, 2, 10, 0F); // stock2
		attachmentModel[2].setRotationPoint(0F, 0F, 0F);

		attachmentModel[3].addShapeBox(-29F, 0F, -4.5F, 29, 6, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 5F, 0F); // stock3
		attachmentModel[3].setRotationPoint(0F, 0F, 0F);

		attachmentModel[4].addShapeBox(-29F, -7F, -4.5F, 29, 3, 9, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // stock5
		attachmentModel[4].setRotationPoint(0F, 0F, 0F);

		attachmentModel[5].addBox(-26F, 6F, -5F, 3, 3, 10, 0F); // stock6
		attachmentModel[5].setRotationPoint(0F, 0F, 0F);

		attachmentModel[6].addBox(-30F, -5F, -3F, 1, 16, 6, 0F); // stock7
		attachmentModel[6].setRotationPoint(0F, 0F, 0F);

		attachmentModel[7].addBox(-32F, -4F, -4.5F, 2, 17, 9, 0F); // stock8
		attachmentModel[7].setRotationPoint(0F, 0F, 0F);

		attachmentModel[8].addShapeBox(-32F, -7F, -4.5F, 2, 3, 9, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // stock9
		attachmentModel[8].setRotationPoint(0F, 0F, 0F);

		attachmentModel[9].addBox(-1F, -3F, -4F, 2, 2, 8, 0F); // Box 0
		attachmentModel[9].setRotationPoint(0F, 0F, 0F);

		renderOffset = 0F;

		flipAll();
	}
}