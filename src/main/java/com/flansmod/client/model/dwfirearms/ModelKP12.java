package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelKP12 extends ModelGun
{
	int textureX = 512;
	int textureY = 256;

	public ModelKP12()
	{
		gunModel = new ModelRendererTurbo[41];
		gunModel[0] = new ModelRendererTurbo(this, 80, 2, textureX, textureY); // barrel1
		gunModel[1] = new ModelRendererTurbo(this, 80, 2, textureX, textureY); // barrel1-2
		gunModel[2] = new ModelRendererTurbo(this, 80, 2, textureX, textureY); // barrel1-3
		gunModel[3] = new ModelRendererTurbo(this, 80, 32, textureX, textureY); // body1
		gunModel[4] = new ModelRendererTurbo(this, 80, 83, textureX, textureY); // body3
		gunModel[5] = new ModelRendererTurbo(this, 80, 71, textureX, textureY); // body4
		gunModel[6] = new ModelRendererTurbo(this, 80, 56, textureX, textureY); // body5
		gunModel[7] = new ModelRendererTurbo(this, 127, 26, textureX, textureY); // Connector
		gunModel[8] = new ModelRendererTurbo(this, 104, 21, textureX, textureY); // muzzleRing1
		gunModel[9] = new ModelRendererTurbo(this, 104, 21, textureX, textureY); // muzzleRing1-2
		gunModel[10] = new ModelRendererTurbo(this, 104, 21, textureX, textureY); // muzzleRing1-3
		gunModel[11] = new ModelRendererTurbo(this, 104, 21, textureX, textureY); // muzzleRing1-4
		gunModel[12] = new ModelRendererTurbo(this, 80, 20, textureX, textureY); // muzzleRing2
		gunModel[13] = new ModelRendererTurbo(this, 80, 20, textureX, textureY); // muzzleRing2-2
		gunModel[14] = new ModelRendererTurbo(this, 80, 11, textureX, textureY); // pumpBarrelBottom
		gunModel[15] = new ModelRendererTurbo(this, 80, 11, textureX, textureY); // pumpBarrelMiddle
		gunModel[16] = new ModelRendererTurbo(this, 80, 11, textureX, textureY); // pumpBarrelTop
		gunModel[17] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // scopeBase
		gunModel[18] = new ModelRendererTurbo(this, 141, 59, textureX, textureY); // Box 47
		gunModel[19] = new ModelRendererTurbo(this, 131, 83, textureX, textureY); // Box 48
		gunModel[20] = new ModelRendererTurbo(this, 140, 84, textureX, textureY); // Box 49
		gunModel[21] = new ModelRendererTurbo(this, 80, 103, textureX, textureY); // Box 50
		gunModel[22] = new ModelRendererTurbo(this, 132, 111, textureX, textureY); // Box 51
		gunModel[23] = new ModelRendererTurbo(this, 132, 111, textureX, textureY); // Box 52
		gunModel[24] = new ModelRendererTurbo(this, 132, 111, textureX, textureY); // Box 53
		gunModel[25] = new ModelRendererTurbo(this, 132, 111, textureX, textureY); // Box 54
		gunModel[26] = new ModelRendererTurbo(this, 132, 111, textureX, textureY); // Box 55
		gunModel[27] = new ModelRendererTurbo(this, 132, 111, textureX, textureY); // Box 56
		gunModel[28] = new ModelRendererTurbo(this, 80, 111, textureX, textureY); // Box 57
		gunModel[29] = new ModelRendererTurbo(this, 113, 103, textureX, textureY); // Box 58
		gunModel[30] = new ModelRendererTurbo(this, 149, 120, textureX, textureY); // Box 59
		gunModel[31] = new ModelRendererTurbo(this, 149, 111, textureX, textureY); // Box 60
		gunModel[32] = new ModelRendererTurbo(this, 149, 120, textureX, textureY); // Box 61
		gunModel[33] = new ModelRendererTurbo(this, 149, 120, textureX, textureY); // Box 62
		gunModel[34] = new ModelRendererTurbo(this, 149, 111, textureX, textureY); // Box 63
		gunModel[35] = new ModelRendererTurbo(this, 149, 120, textureX, textureY); // Box 64
		gunModel[36] = new ModelRendererTurbo(this, 80, 129, textureX, textureY); // Box 1
		gunModel[37] = new ModelRendererTurbo(this, 104, 21, textureX, textureY); // Box 2
		gunModel[38] = new ModelRendererTurbo(this, 80, 20, textureX, textureY); // Box 3
		gunModel[39] = new ModelRendererTurbo(this, 104, 21, textureX, textureY); // Box 4
		gunModel[40] = new ModelRendererTurbo(this, 131, 103, textureX, textureY); // Box 6

		gunModel[0].addShapeBox(0F, 0F, 0F, 55, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // barrel1
		gunModel[0].setRotationPoint(25F, -24.5F, -3F);

		gunModel[1].addBox(0F, 0F, 0F, 55, 2, 6, 0F); // barrel1-2
		gunModel[1].setRotationPoint(25F, -22.5F, -3F);

		gunModel[2].addShapeBox(0F, 0F, 0F, 55, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // barrel1-3
		gunModel[2].setRotationPoint(25F, -20.5F, -3F);

		gunModel[3].addBox(0F, 0F, 0F, 20, 11, 10, 0F); // body1
		gunModel[3].setRotationPoint(-10F, -23F, -5F);

		gunModel[4].addBox(0F, 0F, 0F, 15, 7, 10, 0F); // body3
		gunModel[4].setRotationPoint(10F, -19F, -5F);

		gunModel[5].addBox(0F, 0F, 0F, 15, 4, 7, 0F); // body4
		gunModel[5].setRotationPoint(10F, -23F, -2F);

		gunModel[6].addShapeBox(0F, 0F, 0F, 20, 2, 10, 0F, -3F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, -3F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body5
		gunModel[6].setRotationPoint(-10F, -25F, -5F);

		gunModel[7].addBox(0F, 0F, 0F, 4, 1, 3, 0F); // Connector
		gunModel[7].setRotationPoint(65F, -18F, -1.5F);

		gunModel[8].addShapeBox(0F, 0F, 0F, 4, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // muzzleRing1
		gunModel[8].setRotationPoint(65F, -25F, -3.5F);

		gunModel[9].addShapeBox(0F, 0F, 0F, 4, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // muzzleRing1-2
		gunModel[9].setRotationPoint(65F, -20F, -3.5F);

		gunModel[10].addShapeBox(0F, 0F, 0F, 4, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // muzzleRing1-3
		gunModel[10].setRotationPoint(65F, -17.5F, -3.5F);

		gunModel[11].addShapeBox(0F, 0F, 0F, 4, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // muzzleRing1-4
		gunModel[11].setRotationPoint(65F, -12.5F, -3.5F);

		gunModel[12].addBox(0F, 0F, 0F, 4, 3, 7, 0F); // muzzleRing2
		gunModel[12].setRotationPoint(65F, -23F, -3.5F);

		gunModel[13].addBox(0F, 0F, 0F, 4, 3, 7, 0F); // muzzleRing2-2
		gunModel[13].setRotationPoint(65F, -15.5F, -3.5F);

		gunModel[14].addShapeBox(0F, 0F, 0F, 40, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // pumpBarrelBottom
		gunModel[14].setRotationPoint(25F, -13F, -3F);

		gunModel[15].addBox(0F, 0F, 0F, 40, 2, 6, 0F); // pumpBarrelMiddle
		gunModel[15].setRotationPoint(25F, -15F, -3F);

		gunModel[16].addShapeBox(0F, 0F, 0F, 40, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // pumpBarrelTop
		gunModel[16].setRotationPoint(25F, -17F, -3F);

		gunModel[17].addBox(0F, 0F, 0F, 8, 2, 8, 0F); // scopeBase
		gunModel[17].setRotationPoint(-6F, -26F, -4F);

		gunModel[18].addShapeBox(0F, 0F, 0F, 15, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 47
		gunModel[18].setRotationPoint(10F, -25F, -2F);

		gunModel[19].addShapeBox(0F, 0F, 0F, 2, 6, 2, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 48
		gunModel[19].setRotationPoint(23F, -25F, -5F);

		gunModel[20].addBox(0F, 0F, 0F, 2, 6, 1, 0F); // Box 49
		gunModel[20].setRotationPoint(23F, -25F, -3F);

		gunModel[21].addBox(0F, 0F, 0F, 15, 6, 1, 0F); // Box 50
		gunModel[21].setRotationPoint(0F, -22F, 5F);

		gunModel[22].addShapeBox(0F, 0F, 0F, 2, 11, 6, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F); // Box 51
		gunModel[22].setRotationPoint(1F, -26F, 5F);

		gunModel[23].addBox(0F, 0F, 0F, 2, 11, 6, 0F); // Box 52
		gunModel[23].setRotationPoint(3F, -26F, 5F);

		gunModel[24].addShapeBox(0F, 0F, 0F, 2, 11, 6, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F); // Box 53
		gunModel[24].setRotationPoint(5F, -26F, 5F);

		gunModel[25].addShapeBox(0F, 0F, 0F, 2, 11, 6, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F); // Box 54
		gunModel[25].setRotationPoint(12F, -26F, 5F);

		gunModel[26].addBox(0F, 0F, 0F, 2, 11, 6, 0F); // Box 55
		gunModel[26].setRotationPoint(10F, -26F, 5F);

		gunModel[27].addShapeBox(0F, 0F, 0F, 2, 11, 6, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F); // Box 56
		gunModel[27].setRotationPoint(8F, -26F, 5F);

		gunModel[28].addBox(0F, 0F, 0F, 13, 6, 1, 0F); // Box 57
		gunModel[28].setRotationPoint(1F, -22F, 6F);

		gunModel[29].addBox(0F, 0F, 0F, 3, 6, 3, 0F); // Box 58
		gunModel[29].setRotationPoint(6F, -22F, 7F);

		gunModel[30].addShapeBox(0F, 0F, 0F, 2, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F); // Box 59
		gunModel[30].setRotationPoint(12F, -15F, 5F);

		gunModel[31].addBox(0F, 0F, 0F, 2, 2, 6, 0F); // Box 60
		gunModel[31].setRotationPoint(10F, -15F, 5F);

		gunModel[32].addShapeBox(0F, 0F, 0F, 2, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F); // Box 61
		gunModel[32].setRotationPoint(8F, -15F, 5F);

		gunModel[33].addShapeBox(0F, 0F, 0F, 2, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F); // Box 62
		gunModel[33].setRotationPoint(5F, -15F, 5F);

		gunModel[34].addBox(0F, 0F, 0F, 2, 2, 6, 0F); // Box 63
		gunModel[34].setRotationPoint(3F, -15F, 5F);

		gunModel[35].addShapeBox(0F, 0F, 0F, 2, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F); // Box 64
		gunModel[35].setRotationPoint(1F, -15F, 5F);

		gunModel[36].addShapeBox(0F, 0F, 0F, 35, 2, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 1
		gunModel[36].setRotationPoint(-10F, -12F, -5F);

		gunModel[37].addShapeBox(0F, 0F, 0F, 4, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 2
		gunModel[37].setRotationPoint(75.5F, -25F, -3.5F);

		gunModel[38].addBox(0F, 0F, 0F, 4, 3, 7, 0F); // Box 3
		gunModel[38].setRotationPoint(75.5F, -23F, -3.5F);

		gunModel[39].addShapeBox(0F, 0F, 0F, 4, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 4
		gunModel[39].setRotationPoint(75.5F, -20F, -3.5F);

		gunModel[40].addBox(0F, 0F, 0F, 13, 1, 4, 0F); // Box 6
		gunModel[40].setRotationPoint(7F, -25.5F, -1.5F);


		defaultScopeModel = new ModelRendererTurbo[4];
		defaultScopeModel[0] = new ModelRendererTurbo(this, 1, 12, textureX, textureY); // ironSight1
		defaultScopeModel[1] = new ModelRendererTurbo(this, 1, 12, textureX, textureY); // ironSight1-2
		defaultScopeModel[2] = new ModelRendererTurbo(this, 1, 17, textureX, textureY); // ironSight3
		defaultScopeModel[3] = new ModelRendererTurbo(this, 26, 18, textureX, textureY); // Box 5

		defaultScopeModel[0].addBox(0F, 0F, 0F, 5, 1, 3, 0F); // ironSight1
		defaultScopeModel[0].setRotationPoint(-6F, -27F, 1F);

		defaultScopeModel[1].addBox(0F, 0F, 0F, 5, 1, 3, 0F); // ironSight1-2
		defaultScopeModel[1].setRotationPoint(-6F, -27F, -4F);

		defaultScopeModel[2].addBox(0F, 0F, 0F, 4, 3, 1, 0F); // ironSight3
		defaultScopeModel[2].setRotationPoint(75.5F, -28F, -0.5F);

		defaultScopeModel[3].addBox(0F, 0F, 0F, 8, 2, 1, 0F); // Box 5
		defaultScopeModel[3].setRotationPoint(67.5F, -26F, -0.5F);


		defaultStockModel = new ModelRendererTurbo[12];
		defaultStockModel[0] = new ModelRendererTurbo(this, 34, 67, textureX, textureY); // stock1
		defaultStockModel[1] = new ModelRendererTurbo(this, 1, 61, textureX, textureY); // stock2
		defaultStockModel[2] = new ModelRendererTurbo(this, 1, 102, textureX, textureY); // stock3
		defaultStockModel[3] = new ModelRendererTurbo(this, 24, 130, textureX, textureY); // stock4
		defaultStockModel[4] = new ModelRendererTurbo(this, 24, 118, textureX, textureY); // stock5
		defaultStockModel[5] = new ModelRendererTurbo(this, 1, 89, textureX, textureY); // stock6
		defaultStockModel[6] = new ModelRendererTurbo(this, 1, 118, textureX, textureY); // stock7
		defaultStockModel[7] = new ModelRendererTurbo(this, 24, 130, textureX, textureY); // Box 65
		defaultStockModel[8] = new ModelRendererTurbo(this, 24, 118, textureX, textureY); // Box 66
		defaultStockModel[9] = new ModelRendererTurbo(this, 1, 102, textureX, textureY); // Box 68
		defaultStockModel[10] = new ModelRendererTurbo(this, 54, 39, textureX, textureY); // Box 69
		defaultStockModel[11] = new ModelRendererTurbo(this, 54, 52, textureX, textureY); // Box 0

		defaultStockModel[0].addShapeBox(0F, 0F, 0F, 6, 11, 9, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F); // stock1
		defaultStockModel[0].setRotationPoint(-16F, -21F, -4.5F);

		defaultStockModel[1].addShapeBox(0F, 0F, 0F, 7, 16, 9, 0F, 0F, -8F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -8F, 0F, 0F, 0F, 0F, 0F, -8F, 0F, 0F, -8F, 0F, 0F, 0F, 0F); // stock2
		defaultStockModel[1].setRotationPoint(-23F, -18F, -4.5F);

		defaultStockModel[2].addShapeBox(0F, 0F, 0F, 28, 6, 9, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 8F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 8F, 0F); // stock3
		defaultStockModel[2].setRotationPoint(-51F, -8F, -4.5F);

		defaultStockModel[3].addShapeBox(0F, 0F, 0F, 6, 2, 9, 0F, 0F, 0F, -2F, 0F, 3F, -2F, 0F, 3F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F); // stock4
		defaultStockModel[3].setRotationPoint(-16F, -20F, -4.5F);

		defaultStockModel[4].addShapeBox(0F, 0F, 0F, 7, 2, 9, 0F, 0F, 0F, -2F, 0F, 8F, -2F, 0F, 8F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, -8F, 0F, 0F, -8F, 0F, 0F, 0F, 0F); // stock5
		defaultStockModel[4].setRotationPoint(-23F, -12F, -4.5F);

		defaultStockModel[5].addShapeBox(0F, 0F, 0F, 28, 3, 9, 0F, 0F, -2F, -2F, -3F, 0F, -2F, -3F, 0F, -2F, 0F, -2F, -2F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F); // stock6
		defaultStockModel[5].setRotationPoint(-51F, -13F, -4.5F);

		defaultStockModel[6].addBox(0F, 0F, 0F, 2, 14, 9, 0F); // stock7
		defaultStockModel[6].setRotationPoint(-53F, -8F, -4.5F);

		defaultStockModel[7].addShapeBox(0F, 0F, 0F, 6, 2, 9, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 3F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 3F, -2F); // Box 65
		defaultStockModel[7].setRotationPoint(-16F, -13F, -4.5F);

		defaultStockModel[8].addShapeBox(0F, 0F, 0F, 7, 2, 9, 0F, 0F, -8F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -8F, 0F, 0F, 8F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 8F, -2F); // Box 66
		defaultStockModel[8].setRotationPoint(-23F, -10F, -4.5F);

		defaultStockModel[9].addShapeBox(0F, 0F, 0F, 28, 2, 9, 0F, 0F, 0F, 0F, 0F, 8F, 0F, 0F, 8F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, -8F, -2F, 0F, -8F, -2F, 0F, 0F, -2F); // Box 68
		defaultStockModel[9].setRotationPoint(-51F, 6F, -4.5F);

		defaultStockModel[10].addShapeBox(0F, 0F, 0F, 2, 3, 9, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 69
		defaultStockModel[10].setRotationPoint(-53F, -11F, -4.5F);

		defaultStockModel[11].addShapeBox(0F, 0F, 0F, 2, 2, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 0
		defaultStockModel[11].setRotationPoint(-53F, 6F, -4.5F);


		ammoModel = new ModelRendererTurbo[6];
		ammoModel[0] = new ModelRendererTurbo(this, 115, 120, textureX, textureY); // casing1
		ammoModel[1] = new ModelRendererTurbo(this, 115, 120, textureX, textureY); // casing1-2
		ammoModel[2] = new ModelRendererTurbo(this, 115, 120, textureX, textureY); // casing1-3
		ammoModel[3] = new ModelRendererTurbo(this, 80, 120, textureX, textureY); // shell1
		ammoModel[4] = new ModelRendererTurbo(this, 80, 120, textureX, textureY); // shell1-2
		ammoModel[5] = new ModelRendererTurbo(this, 80, 120, textureX, textureY); // shell1-3

		ammoModel[0].addShapeBox(0F, 0F, 0F, 2, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // casing1
		ammoModel[0].setRotationPoint(10F, -17F, -3F);

		ammoModel[1].addBox(0F, 0F, 0F, 2, 2, 6, 0F); // casing1-2
		ammoModel[1].setRotationPoint(10F, -15F, -3F);

		ammoModel[2].addShapeBox(0F, 0F, 0F, 2, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // casing1-3
		ammoModel[2].setRotationPoint(10F, -13F, -3F);

		ammoModel[3].addShapeBox(0F, 0F, 0F, 11, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // shell1
		ammoModel[3].setRotationPoint(12F, -17F, -3F);

		ammoModel[4].addBox(0F, 0F, 0F, 11, 2, 6, 0F); // shell1-2
		ammoModel[4].setRotationPoint(12F, -15F, -3F);

		ammoModel[5].addShapeBox(0F, 0F, 0F, 11, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // shell1-3
		ammoModel[5].setRotationPoint(12F, -13F, -3F);


		pumpModel = new ModelRendererTurbo[12];
		pumpModel[0] = new ModelRendererTurbo(this, 131, 92, textureX, textureY); // body2
		pumpModel[1] = new ModelRendererTurbo(this, 15, 15, textureX, textureY); // bolt1
		pumpModel[2] = new ModelRendererTurbo(this, 1, 22, textureX, textureY); // pump1
		pumpModel[3] = new ModelRendererTurbo(this, 1, 39, textureX, textureY); // pump2
		pumpModel[4] = new ModelRendererTurbo(this, 1, 50, textureX, textureY); // pump3
		pumpModel[5] = new ModelRendererTurbo(this, 1, 50, textureX, textureY); // pump3-2
		pumpModel[6] = new ModelRendererTurbo(this, 125, 69, textureX, textureY); // Box 41
		pumpModel[7] = new ModelRendererTurbo(this, 141, 32, textureX, textureY); // Box 42
		pumpModel[8] = new ModelRendererTurbo(this, 141, 44, textureX, textureY); // Box 43
		pumpModel[9] = new ModelRendererTurbo(this, 54, 22, textureX, textureY); // Box 44
		pumpModel[10] = new ModelRendererTurbo(this, 54, 11, textureX, textureY); // Box 45
		pumpModel[11] = new ModelRendererTurbo(this, 131, 97, textureX, textureY); // Box 46

		pumpModel[0].addBox(0F, 0F, 0F, 13, 2, 2, 0F); // body2
		pumpModel[0].setRotationPoint(10F, -21F, -4F);

		pumpModel[1].addBox(0F, 0F, 0F, 2, 3, 3, 0F); // bolt1
		pumpModel[1].setRotationPoint(21F, -22F, -7F);

		pumpModel[2].addBox(0F, 0F, 0F, 18, 8, 8, 0F); // pump1
		pumpModel[2].setRotationPoint(46F, -19.5F, -4F);

		pumpModel[3].addShapeBox(0F, 0F, 0F, 18, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // pump2
		pumpModel[3].setRotationPoint(46F, -11.5F, -4F);

		pumpModel[4].addBox(0F, 0F, 0F, 16, 1, 9, 0F); // pump3
		pumpModel[4].setRotationPoint(47F, -17F, -4.5F);

		pumpModel[5].addBox(0F, 0F, 0F, 16, 1, 9, 0F); // pump3-2
		pumpModel[5].setRotationPoint(47F, -14F, -4.5F);

		pumpModel[6].addShapeBox(0F, 0F, 0F, 18, 2, 11, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 41
		pumpModel[6].setRotationPoint(26F, -9.5F, -5.5F);

		pumpModel[7].addBox(0F, 0F, 0F, 18, 10, 1, 0F); // Box 42
		pumpModel[7].setRotationPoint(26F, -19.5F, -5.5F);

		pumpModel[8].addBox(0F, 0F, 0F, 18, 10, 1, 0F); // Box 43
		pumpModel[8].setRotationPoint(26F, -19.5F, 4.5F);

		pumpModel[9].addShapeBox(0F, 0F, 0F, 2, 8, 8, 0F, 0F, 0F, 1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1.5F, 0F, 2F, 1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 1.5F); // Box 44
		pumpModel[9].setRotationPoint(44F, -19.5F, -4F);

		pumpModel[10].addShapeBox(0F, 0F, 0F, 2, 2, 8, 0F, 0F, 0F, 1.5F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 1.5F, 0F, 0F, 0.5F, 0F, -2F, -1F, 0F, -2F, -1F, 0F, 0F, 0.5F); // Box 45
		pumpModel[10].setRotationPoint(44F, -9.5F, -4F);

		pumpModel[11].addShapeBox(0F, 0F, 0F, 13, 3, 2, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 46
		pumpModel[11].setRotationPoint(10F, -24F, -4F);



		barrelAttachPoint = new Vector3f(80F /16F, 21.5F /16F, 0F /16F);
		stockAttachPoint = new Vector3f(-10F /16F, 17F /16F, 0F /16F);
		scopeAttachPoint = new Vector3f(0F /16F, 0F /16F, 0F /16F);
		gripAttachPoint = new Vector3f(0 /16F, 0F /16F, 0F /16F);

		gunSlideDistance = 0F;
		animationType = EnumAnimationType.SHOTGUN;

		numBulletsInReloadAnimation = 5;
		tiltGunTime = 0.159F;
		unloadClipTime = 0.0F;
		loadClipTime = 0.708F;
		untiltGunTime = 0.133F;

		pumpDelay = 10;
		pumpTime = 7;
		pumpDelayAfterReload = 110;
		pumpHandleDistance = 1.2F;


		translateAll(0F, 0F, 0F);


		flipAll();
	}
}