package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelPUP227 extends ModelGun
{
	int textureX = 512;
	int textureY = 256;

	public ModelPUP227()
	{
		gunModel = new ModelRendererTurbo[13];
		gunModel[0] = new ModelRendererTurbo(this, 64, 94, textureX, textureY); // ammoConnector
		gunModel[1] = new ModelRendererTurbo(this, 64, 83, textureX, textureY); // ammoConnector2
		gunModel[2] = new ModelRendererTurbo(this, 64, 1, textureX, textureY); // barrel1
		gunModel[3] = new ModelRendererTurbo(this, 64, 1, textureX, textureY); // barrel1-2
		gunModel[4] = new ModelRendererTurbo(this, 64, 10, textureX, textureY); // barrel2
		gunModel[5] = new ModelRendererTurbo(this, 113, 14, textureX, textureY); // barrelLower1
		gunModel[6] = new ModelRendererTurbo(this, 113, 14, textureX, textureY); // barrelLower1-2
		gunModel[7] = new ModelRendererTurbo(this, 113, 14, textureX, textureY); // barrelLower1-3
		gunModel[8] = new ModelRendererTurbo(this, 64, 19, textureX, textureY); // body1
		gunModel[9] = new ModelRendererTurbo(this, 111, 31, textureX, textureY); // body2
		gunModel[10] = new ModelRendererTurbo(this, 77, 109, textureX, textureY); // hammer
		gunModel[11] = new ModelRendererTurbo(this, 112, 95, textureX, textureY); // stopper1
		gunModel[12] = new ModelRendererTurbo(this, 112, 80, textureX, textureY); // stopper2

		gunModel[0].addBox(0F, 0F, 0F, 12, 1, 7, 0F); // ammoConnector
		gunModel[0].setRotationPoint(10F, -14F, -3.5F);

		gunModel[1].addBox(0F, 0F, 0F, 12, 2, 8, 0F); // ammoConnector2
		gunModel[1].setRotationPoint(10F, -13F, -4F);

		gunModel[2].addShapeBox(0F, 0F, 0F, 18, 2, 6, 0F,0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // barrel1
		gunModel[2].setRotationPoint(20F, -24.5F, -3F);

		gunModel[3].addShapeBox(0F, 0F, 0F, 18, 2, 6, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // barrel1-2
		gunModel[3].setRotationPoint(20F, -20.5F, -3F);

		gunModel[4].addBox(0F, 0F, 0F, 18, 2, 6, 0F); // barrel2
		gunModel[4].setRotationPoint(20F, -22.5F, -3F);

		gunModel[5].addShapeBox(0F, 0F, 0F, 4, 1, 3, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // barrelLower1
		gunModel[5].setRotationPoint(28.5F, -18F, -1.5F);

		gunModel[6].addBox(0F, 0F, 0F, 4, 1, 3, 0F); // barrelLower1-2
		gunModel[6].setRotationPoint(28.5F, -17F, -1.5F);

		gunModel[7].addShapeBox(0F, 0F, 0F, 4, 1, 3, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // barrelLower1-3
		gunModel[7].setRotationPoint(28.5F, -16F, -1.5F);

		gunModel[8].addBox(0F, 0F, 0F, 38, 3, 8, 0F); // body1
		gunModel[8].setRotationPoint(-10F, -17F, -4F);

		gunModel[9].addBox(0F, 0F, 0F, 14, 2, 1, 0F); // body2
		gunModel[9].setRotationPoint(9F, -16.5F, 3.5F);

		gunModel[10].addShapeBox(0F, 0F, 0F, 4, 2, 2, 0F,0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F); // hammer
		gunModel[10].setRotationPoint(-11F, -23F, -1F);

		gunModel[11].addShapeBox(0F, 0F, 0F, 9, 1, 7, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // stopper1
		gunModel[11].setRotationPoint(10F, -25F, -3.5F);

		gunModel[12].addBox(0F, 0F, 0F, 9, 7, 7, 0F); // stopper2
		gunModel[12].setRotationPoint(10F, -24F, -3.5F);


		defaultGripModel = new ModelRendererTurbo[4];
		defaultGripModel[0] = new ModelRendererTurbo(this, 1, 45, textureX, textureY); // grip1
		defaultGripModel[1] = new ModelRendererTurbo(this, 1, 17, textureX, textureY); // grip2
		defaultGripModel[2] = new ModelRendererTurbo(this, 1, 72, textureX, textureY); // grip3
		defaultGripModel[3] = new ModelRendererTurbo(this, 1, 6, textureX, textureY); // gripConnector

		defaultGripModel[0].addShapeBox(0F, 0F, 0F, 12, 18, 8, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, -2F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 3F, -2F, 0F); // grip1
		defaultGripModel[0].setRotationPoint(-8F, -12F, -4F);

		defaultGripModel[1].addShapeBox(0F, 0F, 0F, 11, 18, 9, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, -2F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 3F, -2F, 0F); // grip2
		defaultGripModel[1].setRotationPoint(-7.4F, -12.5F, -4.5F);

		defaultGripModel[2].addBox(0F, 0F, 0F, 13, 2, 9, 0F); // grip3
		defaultGripModel[2].setRotationPoint(-11F, 4F, -4.5F);
		defaultGripModel[2].rotateAngleZ = -0.16580628F;

		defaultGripModel[3].addShapeBox(0F, 0F, 0F, 13, 2, 8, 0F,2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // gripConnector
		defaultGripModel[3].setRotationPoint(-8F, -14F, -4F);


		ammoModel = new ModelRendererTurbo[4];
		ammoModel[0] = new ModelRendererTurbo(this, 1, 84, textureX, textureY); // ammoClip
		ammoModel[1] = new ModelRendererTurbo(this, 42, 66, textureX, textureY); // bullet
		ammoModel[2] = new ModelRendererTurbo(this, 42, 60, textureX, textureY); // bulletTip
		ammoModel[3] = new ModelRendererTurbo(this, 1, 109, textureX, textureY); // clip2

		ammoModel[0].addShapeBox(0F, 0F, 0F, 9, 18, 6, 0F,0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 3F, -2F, 0F, 3F, -2F, 0F, -3F, 0F, 0F); // ammoClip
		ammoModel[0].setRotationPoint(11F, -11F, -3F);

		ammoModel[1].addShapeBox(0F, 0F, 0F, 5, 1, 4, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // bullet
		ammoModel[1].setRotationPoint(12F, -12F, -2F);
		ammoModel[1].rotateAngleZ = 0.17453293F;

		ammoModel[2].addShapeBox(0F, 0F, 0F, 2, 1, 4, 0F,0F, 0F, -1F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F); // bulletTip
		ammoModel[2].setRotationPoint(16.8F, -12.9F, -2F);
		ammoModel[2].rotateAngleZ = 0.17453293F;

		ammoModel[3].addBox(0F, 0F, 0F, 11, 2, 7, 0F); // clip2
		ammoModel[3].setRotationPoint(13F, 6.5F, -3.5F);
		ammoModel[3].rotateAngleZ = 0.16580628F;


		slideModel = new ModelRendererTurbo[13];
		slideModel[0] = new ModelRendererTurbo(this, 64, 73, textureX, textureY); // slide1
		slideModel[1] = new ModelRendererTurbo(this, 1, 2, textureX, textureY); // ironSightFront
		slideModel[2] = new ModelRendererTurbo(this, 14, 2, textureX, textureY); // ironSight
		slideModel[3] = new ModelRendererTurbo(this, 1, 2, textureX, textureY); // ironSight2
		slideModel[4] = new ModelRendererTurbo(this, 64, 57, textureX, textureY); // slide2
		slideModel[5] = new ModelRendererTurbo(this, 103, 41, textureX, textureY); // slide7
		slideModel[6] = new ModelRendererTurbo(this, 64, 31, textureX, textureY); // slide8
		slideModel[7] = new ModelRendererTurbo(this, 64, 103, textureX, textureY); // slideEnd1
		slideModel[8] = new ModelRendererTurbo(this, 64, 103, textureX, textureY); // slideEnd1-2
		slideModel[9] = new ModelRendererTurbo(this, 103, 41, textureX, textureY); // Box 30
		slideModel[10] = new ModelRendererTurbo(this, 103, 41, textureX, textureY); // Box 31
		slideModel[11] = new ModelRendererTurbo(this, 64, 41, textureX, textureY); // Box 32
		slideModel[12] = new ModelRendererTurbo(this, 122, 42, textureX, textureY); // Box 33

		slideModel[0].addShapeBox(0F, 0F, 0F, 10, 1, 8, 0F,0F, 0.5F, -1F, 0F, 0.5F, -1F, 0F, 0.5F, -1F, 0F, 0.5F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // slide1
		slideModel[0].setRotationPoint(19F, -25F, -4F);

		slideModel[1].addBox(0F, 0F, 0F, 4, 1, 2, 0F); // ironSightFront
		slideModel[1].setRotationPoint(24F, -26.5F, -2.5F);

		slideModel[2].addShapeBox(0F, 0F, 0F, 4, 1, 2, 0F,0F, 0.5F, -0.5F, 0F, 0.5F, -0.5F, 0F, 0.5F, -0.5F, 0F, 0.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // ironSight
		slideModel[2].setRotationPoint(-7F, -26F, -1F);

		slideModel[3].addBox(0F, 0F, 0F, 4, 1, 2, 0F); // ironSight2
		slideModel[3].setRotationPoint(24F, -26.5F, 0.5F);

		slideModel[4].addBox(0F, 0F, 0F, 10, 7, 8, 0F); // slide2
		slideModel[4].setRotationPoint(19F, -24F, -4F);

		slideModel[5].addBox(0F, 0F, 0F, 1, 7, 8, 0F); // slide7
		slideModel[5].setRotationPoint(-7F, -24F, -4F);

		slideModel[6].addShapeBox(0F, 0F, 0F, 19, 1, 8, 0F,-1F, 0.5F, -1F, 0F, 0.5F, -1F, 0F, 0.5F, -1F, -1F, 0.5F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // slide8
		slideModel[6].setRotationPoint(-9F, -25F, -4F);

		slideModel[7].addShapeBox(0F, 0F, 0F, 3, 7, 3, 0F,-1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // slideEnd1
		slideModel[7].setRotationPoint(-10F, -24F, 1F);

		slideModel[8].addShapeBox(0F, 0F, 0F, 3, 7, 3, 0F,-1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // slideEnd1-2
		slideModel[8].setRotationPoint(-10F, -24F, -4F);

		slideModel[9].addBox(0F, 0F, 0F, 1, 7, 8, 0F); // Box 30
		slideModel[9].setRotationPoint(-5F, -24F, -4F);

		slideModel[10].addBox(0F, 0F, 0F, 1, 7, 8, 0F); // Box 31
		slideModel[10].setRotationPoint(-3F, -24F, -4F);

		slideModel[11].addBox(0F, 0F, 0F, 11, 7, 8, 0F); // Box 32
		slideModel[11].setRotationPoint(-1F, -24F, -4F);

		slideModel[12].addBox(0F, 0F, 0F, 5, 7, 7, 0F); // Box 33
		slideModel[12].setRotationPoint(-6F, -24F, -3.5F);



		barrelAttachPoint = new Vector3f(0F /16F, 0F /16F, 0F /16F);
		stockAttachPoint = new Vector3f(0F /16F, 0F /16F, 0F /16F);
		scopeAttachPoint = new Vector3f(0F /16F, 0F /16F, 0F /16F);
		gripAttachPoint = new Vector3f(0 /16F, 0F /16F, 0F /16F);

		gunSlideDistance = 0.8F;
		animationType = EnumAnimationType.CUSTOM;

		/* ----Start of Reload Block---- */
		/* Reload Name: Alternate Pistol Clip */
		rotateGunVertical = 10F;
		translateGun = new Vector3f(0.15F, 0.25F, 0F);

		rotateClipVertical = 5F;
		translateClip = new Vector3f(0F, -3F, 0F);
		/* ----End of Reload Block---- */


		translateAll(2F, 0F, 0F);


		flipAll();
	}
}