package com.flansmod.client.model.dwfirearms; //Path where the model is located

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelKPSS12 extends ModelGun //Same as Filename
{
	int textureX = 512;
	int textureY = 256;

	public ModelKPSS12() //Same as Filename
	{
		gunModel = new ModelRendererTurbo[65];
		gunModel[0] = new ModelRendererTurbo(this, 80, 2, textureX, textureY); // barrel1
		gunModel[1] = new ModelRendererTurbo(this, 80, 2, textureX, textureY); // barrel1-2
		gunModel[2] = new ModelRendererTurbo(this, 80, 2, textureX, textureY); // barrel1-3
		gunModel[3] = new ModelRendererTurbo(this, 80, 66, textureX, textureY); // body1
		gunModel[4] = new ModelRendererTurbo(this, 80, 89, textureX, textureY); // pump1
		gunModel[5] = new ModelRendererTurbo(this, 80, 107, textureX, textureY); // pump2
		gunModel[6] = new ModelRendererTurbo(this, 80, 11, textureX, textureY); // pumpBarrelBottom
		gunModel[7] = new ModelRendererTurbo(this, 80, 11, textureX, textureY); // pumpBarrelMiddle
		gunModel[8] = new ModelRendererTurbo(this, 80, 11, textureX, textureY); // pumpBarrelTop
		gunModel[9] = new ModelRendererTurbo(this, 1, 68, textureX, textureY); // stock1
		gunModel[10] = new ModelRendererTurbo(this, 1, 49, textureX, textureY); // Box 1
		gunModel[11] = new ModelRendererTurbo(this, 37, 52, textureX, textureY); // Box 2
		gunModel[12] = new ModelRendererTurbo(this, 59, 56, textureX, textureY); // Box 3
		gunModel[13] = new ModelRendererTurbo(this, 50, 56, textureX, textureY); // Box 4
		gunModel[14] = new ModelRendererTurbo(this, 24, 52, textureX, textureY); // Box 5
		gunModel[15] = new ModelRendererTurbo(this, 111, 54, textureX, textureY); // Box 6
		gunModel[16] = new ModelRendererTurbo(this, 32, 88, textureX, textureY); // Box 7
		gunModel[17] = new ModelRendererTurbo(this, 103, 66, textureX, textureY); // Box 8
		gunModel[18] = new ModelRendererTurbo(this, 80, 54, textureX, textureY); // Box 9
		gunModel[19] = new ModelRendererTurbo(this, 157, 45, textureX, textureY); // Box 10
		gunModel[20] = new ModelRendererTurbo(this, 80, 43, textureX, textureY); // Box 11
		gunModel[21] = new ModelRendererTurbo(this, 111, 21, textureX, textureY); // Box 12
		gunModel[22] = new ModelRendererTurbo(this, 111, 43, textureX, textureY); // Box 15
		gunModel[23] = new ModelRendererTurbo(this, 128, 43, textureX, textureY); // Box 16
		gunModel[24] = new ModelRendererTurbo(this, 159, 66, textureX, textureY); // Box 17
		gunModel[25] = new ModelRendererTurbo(this, 130, 66, textureX, textureY); // Box 18
		gunModel[26] = new ModelRendererTurbo(this, 80, 31, textureX, textureY); // Box 19
		gunModel[27] = new ModelRendererTurbo(this, 155, 31, textureX, textureY); // Box 20
		gunModel[28] = new ModelRendererTurbo(this, 32, 75, textureX, textureY); // Box 21
		gunModel[29] = new ModelRendererTurbo(this, 134, 53, textureX, textureY); // Box 22
		gunModel[30] = new ModelRendererTurbo(this, 134, 53, textureX, textureY); // Box 25
		gunModel[31] = new ModelRendererTurbo(this, 42, 114, textureX, textureY); // Box 26
		gunModel[32] = new ModelRendererTurbo(this, 1, 91, textureX, textureY); // Box 27
		gunModel[33] = new ModelRendererTurbo(this, 1, 114, textureX, textureY); // Box 28
		gunModel[34] = new ModelRendererTurbo(this, 24, 127, textureX, textureY); // Box 0
		gunModel[35] = new ModelRendererTurbo(this, 1, 127, textureX, textureY); // Box 1
		gunModel[36] = new ModelRendererTurbo(this, 42, 101, textureX, textureY); // Box 2
		gunModel[37] = new ModelRendererTurbo(this, 42, 101, textureX, textureY); // Box 3
		gunModel[38] = new ModelRendererTurbo(this, 32, 88, textureX, textureY); // Box 4
		gunModel[39] = new ModelRendererTurbo(this, 1, 91, textureX, textureY); // Box 5
		gunModel[40] = new ModelRendererTurbo(this, 1, 143, textureX, textureY); // Box 6
		gunModel[41] = new ModelRendererTurbo(this, 1, 143, textureX, textureY); // Box 7
		gunModel[42] = new ModelRendererTurbo(this, 134, 53, textureX, textureY); // Box 8
		gunModel[43] = new ModelRendererTurbo(this, 134, 53, textureX, textureY); // Box 9
		gunModel[44] = new ModelRendererTurbo(this, 24, 143, textureX, textureY); // Box 10
		gunModel[45] = new ModelRendererTurbo(this, 47, 131, textureX, textureY); // Box 11
		gunModel[46] = new ModelRendererTurbo(this, 47, 131, textureX, textureY); // Box 12
		gunModel[47] = new ModelRendererTurbo(this, 80, 121, textureX, textureY); // Box 13
		gunModel[48] = new ModelRendererTurbo(this, 181, 94, textureX, textureY); // Box 14
		gunModel[49] = new ModelRendererTurbo(this, 181, 107, textureX, textureY); // Box 15
		gunModel[50] = new ModelRendererTurbo(this, 181, 120, textureX, textureY); // Box 16
		gunModel[51] = new ModelRendererTurbo(this, 1, 36, textureX, textureY); // Box 17
		gunModel[52] = new ModelRendererTurbo(this, 1, 36, textureX, textureY); // Box 18
		gunModel[53] = new ModelRendererTurbo(this, 28, 36, textureX, textureY); // Box 19
		gunModel[54] = new ModelRendererTurbo(this, 28, 36, textureX, textureY); // Box 21
		gunModel[55] = new ModelRendererTurbo(this, 182, 80, textureX, textureY); // Box 108
		gunModel[56] = new ModelRendererTurbo(this, 182, 70, textureX, textureY); // Box 109
		gunModel[57] = new ModelRendererTurbo(this, 182, 70, textureX, textureY); // Box 110
		gunModel[58] = new ModelRendererTurbo(this, 182, 70, textureX, textureY); // Box 111
		gunModel[59] = new ModelRendererTurbo(this, 182, 70, textureX, textureY); // Box 112
		gunModel[60] = new ModelRendererTurbo(this, 182, 70, textureX, textureY); // Box 113
		gunModel[61] = new ModelRendererTurbo(this, 1, 36, textureX, textureY); // Box 113
		gunModel[62] = new ModelRendererTurbo(this, 1, 36, textureX, textureY); // Box 114
		gunModel[63] = new ModelRendererTurbo(this, 1, 36, textureX, textureY); // Box 115
		gunModel[64] = new ModelRendererTurbo(this, 1, 36, textureX, textureY); // Box 116

		gunModel[0].addShapeBox(0F, 0F, 0F, 18, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // barrel1
		gunModel[0].setRotationPoint(70F, -20.5F, -3F);

		gunModel[1].addBox(0F, 0F, 0F, 18, 2, 6, 0F); // barrel1-2
		gunModel[1].setRotationPoint(70F, -18.5F, -3F);

		gunModel[2].addShapeBox(0F, 0F, 0F, 18, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // barrel1-3
		gunModel[2].setRotationPoint(70F, -16.5F, -3F);

		gunModel[3].addShapeBox(0F, 0F, 0F, 1, 12, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 10F, 0F, 0F, 10F, 0F, 0F, 0F, 0F, 0F); // body1
		gunModel[3].setRotationPoint(-8F, -19.5F, -5F);

		gunModel[4].addBox(0F, 0F, 0F, 40, 7, 10, 0F); // pump1
		gunModel[4].setRotationPoint(30F, -14.5F, -5F);

		gunModel[5].addShapeBox(0F, 0F, 0F, 40, 2, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // pump2
		gunModel[5].setRotationPoint(30F, -7.5F, -5F);

		gunModel[6].addShapeBox(0F, 0F, 0F, 30, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // pumpBarrelBottom
		gunModel[6].setRotationPoint(70F, -8.5F, -3F);

		gunModel[7].addBox(0F, 0F, 0F, 30, 2, 6, 0F); // pumpBarrelMiddle
		gunModel[7].setRotationPoint(70F, -10.5F, -3F);

		gunModel[8].addShapeBox(0F, 0F, 0F, 30, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // pumpBarrelTop
		gunModel[8].setRotationPoint(70F, -12.5F, -3F);

		gunModel[9].addShapeBox(0F, 0F, 0F, 5, 14, 8, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F); // stock1
		gunModel[9].setRotationPoint(-13F, -20.5F, -4F);

		gunModel[10].addShapeBox(0F, 0F, 0F, 3, 10, 8, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F); // Box 1
		gunModel[10].setRotationPoint(-16F, -18.5F, -4F);

		gunModel[11].addShapeBox(0F, 0F, 0F, 5, 14, 1, 0F, 0F, -3F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -3F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -2F, 0F); // Box 2
		gunModel[11].setRotationPoint(-13F, -20.5F, -5F);

		gunModel[12].addShapeBox(0F, 0F, 0F, 3, 10, 1, 0F, -1F, -3F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, -1F, -3F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -3F, 0F); // Box 3
		gunModel[12].setRotationPoint(-16F, -18.5F, -5F);

		gunModel[13].addShapeBox(0F, 0F, 0F, 3, 10, 1, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -1F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -1F, -3F, 0F); // Box 4
		gunModel[13].setRotationPoint(-16F, -18.5F, 4F);

		gunModel[14].addShapeBox(0F, 0F, 0F, 5, 14, 1, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -3F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -3F, 0F); // Box 5
		gunModel[14].setRotationPoint(-13F, -20.5F, 4F);

		gunModel[15].addShapeBox(0F, 0F, 0F, 1, 1, 10, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 6
		gunModel[15].setRotationPoint(-8F, -20.5F, -5F);

		gunModel[16].addShapeBox(0F, 0F, 0F, 11, 2, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F); // Box 7
		gunModel[16].setRotationPoint(-8F, -7.5F, -5F);

		gunModel[17].addBox(0F, 0F, 0F, 3, 12, 10, 0F); // Box 8
		gunModel[17].setRotationPoint(27F, -19.5F, -5F);

		gunModel[18].addShapeBox(0F, 0F, 0F, 3, 1, 10, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 9
		gunModel[18].setRotationPoint(27F, -20.5F, -5F);

		gunModel[19].addBox(0F, 0F, 0F, 19, 12, 8, 0F); // Box 10
		gunModel[19].setRotationPoint(8F, -19.5F, -3F);

		gunModel[20].addBox(0F, 0F, 0F, 13, 8, 2, 0F); // Box 11
		gunModel[20].setRotationPoint(14F, -15.5F, -5F);

		gunModel[21].addShapeBox(0F, 0F, 0F, 13, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 12
		gunModel[21].setRotationPoint(14F, -20.5F, -3F);

		gunModel[22].addBox(0F, 0F, 0F, 6, 8, 2, 0F); // Box 15
		gunModel[22].setRotationPoint(8F, -15.5F, -5F);

		gunModel[23].addBox(0F, 0F, 0F, 6, 3, 2, 0F); // Box 16
		gunModel[23].setRotationPoint(8F, -19.5F, -5F);

		gunModel[24].addShapeBox(0F, 0F, 0F, 1, 12, 10, 0F, 10F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 10F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 17
		gunModel[24].setRotationPoint(3F, -19.5F, -5F);

		gunModel[25].addBox(0F, 0F, 0F, 4, 12, 10, 0F); // Box 18
		gunModel[25].setRotationPoint(4F, -19.5F, -5F);

		gunModel[26].addShapeBox(0F, 0F, 0F, 27, 1, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 19
		gunModel[26].setRotationPoint(3F, -7.5F, -5F);

		gunModel[27].addShapeBox(0F, 0F, 0F, 21, 1, 10, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 20
		gunModel[27].setRotationPoint(-7F, -20.5F, -5F);

		gunModel[28].addBox(0F, 0F, 0F, 10, 2, 10, 0F); // Box 21
		gunModel[28].setRotationPoint(-7F, -5.5F, -5F);

		gunModel[29].addShapeBox(0F, 0F, 0F, 1, 2, 10, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, -1F, 0F, -1F, 1F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, -1F); // Box 22
		gunModel[29].setRotationPoint(-9F, -7.5F, -5F);

		gunModel[30].addShapeBox(0F, 0F, 0F, 1, 2, 10, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 25
		gunModel[30].setRotationPoint(-8F, -5.5F, -5F);

		gunModel[31].addShapeBox(0F, 0F, 0F, 1, 4, 10, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 26
		gunModel[31].setRotationPoint(3F, -7.5F, -5F);

		gunModel[32].addShapeBox(0F, 0F, 0F, 10, 5, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 4F, 0F, 0F); // Box 27
		gunModel[32].setRotationPoint(-7F, -3.5F, -5F);

		gunModel[33].addBox(0F, 0F, 0F, 10, 2, 10, 0F); // Box 28
		gunModel[33].setRotationPoint(-13F, 7.5F, -5F);

		gunModel[34].addShapeBox(0F, 0F, 0F, 1, 5, 10, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 4F, 0F, 0F, -4F, 0F, -1F, -4F, 0F, -1F, 4F, 0F, 0F); // Box 0
		gunModel[34].setRotationPoint(3F, -3.5F, -5F);

		gunModel[35].addShapeBox(0F, 0F, 0F, 1, 5, 10, 0F, -4F, 0F, -1F, 4F, 0F, 0F, 4F, 0F, 0F, -4F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 1
		gunModel[35].setRotationPoint(-12F, -3.5F, -5F);

		gunModel[36].addShapeBox(0F, 0F, 0F, 1, 2, 10, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 2
		gunModel[36].setRotationPoint(-3F, 7.5F, -5F);

		gunModel[37].addShapeBox(0F, 0F, 0F, 1, 2, 10, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 3
		gunModel[37].setRotationPoint(-14F, 7.5F, -5F);

		gunModel[38].addShapeBox(0F, 0F, 0F, 11, 2, 10, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F); // Box 4
		gunModel[38].setRotationPoint(-13F, 9.5F, -5F);

		gunModel[39].addShapeBox(0F, 0F, 0F, 10, 6, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F); // Box 5
		gunModel[39].setRotationPoint(-11F, 1.5F, -5F);

		gunModel[40].addShapeBox(0F, 0F, 0F, 1, 6, 10, 0F, -2F, 0F, -1F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 6
		gunModel[40].setRotationPoint(-14F, 1.5F, -5F);

		gunModel[41].addShapeBox(0F, 0F, 0F, 1, 6, 10, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, -1F, -2F, 0F, -1F, 2F, 0F, 0F); // Box 7
		gunModel[41].setRotationPoint(-1F, 1.5F, -5F);

		gunModel[42].addShapeBox(0F, 0F, 0F, 1, 2, 10, 0F, 1F, 0F, 0F, -1F, 0F, -1F, -1F, 0F, -1F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 8
		gunModel[42].setRotationPoint(-2F, 9.5F, -5F);

		gunModel[43].addShapeBox(0F, 0F, 0F, 1, 2, 10, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, -1F, 0F, -1F, 1F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, -1F); // Box 9
		gunModel[43].setRotationPoint(-14F, 9.5F, -5F);

		gunModel[44].addShapeBox(0F, 0F, 0F, 7, 1, 10, 0F, 2F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 10
		gunModel[44].setRotationPoint(-10F, 11.5F, -5F);

		gunModel[45].addShapeBox(0F, 0F, 0F, 1, 1, 10, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, -2F, 0F, -2F, 2F, 0F, -1F, 2F, 0F, -1F, -2F, 0F, -2F); // Box 11
		gunModel[45].setRotationPoint(-13F, 11.5F, -5F);

		gunModel[46].addShapeBox(0F, 0F, 0F, 1, 1, 10, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 1F, 0F, -1F, -1F, 0F, -2F, -1F, 0F, -2F, 1F, 0F, -1F); // Box 12
		gunModel[46].setRotationPoint(-2F, 11.5F, -5F);

		gunModel[47].addBox(0F, 0F, 0F, 40, 2, 10, 0F); // Box 13
		gunModel[47].setRotationPoint(30F, -19.5F, -5F);

		gunModel[48].addBox(0F, 0F, 0F, 37, 3, 9, 0F); // Box 14
		gunModel[48].setRotationPoint(33F, -17.5F, -4.5F);

		gunModel[49].addShapeBox(0F, 0F, 0F, 40, 2, 10, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 15
		gunModel[49].setRotationPoint(30F, -21.5F, -5F);

		gunModel[50].addBox(0F, 0F, 0F, 3, 3, 10, 0F); // Box 16
		gunModel[50].setRotationPoint(30F, -17.5F, -5F);

		gunModel[51].addShapeBox(0F, 0F, 0F, 2, 1, 11, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 17
		gunModel[51].setRotationPoint(-14F, -14.5F, -5.5F);

		gunModel[52].addShapeBox(0F, 0F, 0F, 2, 1, 11, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 18
		gunModel[52].setRotationPoint(-14F, -13.5F, -5.5F);

		gunModel[53].addShapeBox(0F, 0F, 0F, 2, 1, 13, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 19
		gunModel[53].setRotationPoint(-8F, -18.5F, -6.5F);

		gunModel[54].addShapeBox(0F, 0F, 0F, 2, 1, 13, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 21
		gunModel[54].setRotationPoint(-8F, -17.5F, -6.5F);

		gunModel[55].addBox(0F, 0F, 0F, 27, 1, 7, 0F); // Box 108
		gunModel[55].setRotationPoint(38F, -22.5F, -3.5F);

		gunModel[56].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 109
		gunModel[56].setRotationPoint(38F, -24.5F, -3.5F);

		gunModel[57].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 110
		gunModel[57].setRotationPoint(44F, -24.5F, -3.5F);

		gunModel[58].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 111
		gunModel[58].setRotationPoint(56F, -24.5F, -3.5F);

		gunModel[59].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 112
		gunModel[59].setRotationPoint(50F, -24.5F, -3.5F);

		gunModel[60].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 113
		gunModel[60].setRotationPoint(62F, -24.5F, -3.5F);

		gunModel[61].addShapeBox(0F, 0F, 0F, 2, 1, 11, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 113
		gunModel[61].setRotationPoint(-3.5F, -4.5F, -5.5F);

		gunModel[62].addShapeBox(0F, 0F, 0F, 2, 1, 11, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 114
		gunModel[62].setRotationPoint(-3.5F, -3.5F, -5.5F);

		gunModel[63].addShapeBox(0F, 0F, 0F, 2, 1, 11, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 115
		gunModel[63].setRotationPoint(-9F, 7.5F, -5.5F);

		gunModel[64].addShapeBox(0F, 0F, 0F, 2, 1, 11, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 116
		gunModel[64].setRotationPoint(-9F, 8.5F, -5.5F);


		defaultBarrelModel = new ModelRendererTurbo[3];
		defaultBarrelModel[0] = new ModelRendererTurbo(this, 153, 1, textureX, textureY); // muzzleRing1
		defaultBarrelModel[1] = new ModelRendererTurbo(this, 153, 1, textureX, textureY); // muzzleRing1-2
		defaultBarrelModel[2] = new ModelRendererTurbo(this, 153, 11, textureX, textureY); // muzzleRing2

		defaultBarrelModel[0].addShapeBox(0F, 0F, 0F, 12, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // muzzleRing1
		defaultBarrelModel[0].setRotationPoint(88F, -21F, -3.5F);

		defaultBarrelModel[1].addShapeBox(0F, 0F, 0F, 12, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // muzzleRing1-2
		defaultBarrelModel[1].setRotationPoint(88F, -16F, -3.5F);

		defaultBarrelModel[2].addBox(0F, 0F, 0F, 12, 3, 7, 0F); // muzzleRing2
		defaultBarrelModel[2].setRotationPoint(88F, -19F, -3.5F);


		defaultScopeModel = new ModelRendererTurbo[7];
		defaultScopeModel[0] = new ModelRendererTurbo(this, 1, 10, textureX, textureY); // Box 40
		defaultScopeModel[1] = new ModelRendererTurbo(this, 31, 12, textureX, textureY); // Box 52
		defaultScopeModel[2] = new ModelRendererTurbo(this, 48, 13, textureX, textureY); // Box 53
		defaultScopeModel[3] = new ModelRendererTurbo(this, 48, 13, textureX, textureY); // Box 54
		defaultScopeModel[4] = new ModelRendererTurbo(this, 61, 15, textureX, textureY); // Box 55
		defaultScopeModel[5] = new ModelRendererTurbo(this, 61, 15, textureX, textureY); // Box 56
		defaultScopeModel[6] = new ModelRendererTurbo(this, 16, 13, textureX, textureY); // Box 57

		defaultScopeModel[0].addShapeBox(0F, 0F, 0F, 4, 5, 3, 0F, 0F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 40
		defaultScopeModel[0].setRotationPoint(84F, -27.5F, -1.5F);

		defaultScopeModel[1].addBox(0F, 0F, 0F, 3, 2, 5, 0F); // Box 52
		defaultScopeModel[1].setRotationPoint(32F, -23F, -2.5F);

		defaultScopeModel[2].addBox(0F, 0F, 0F, 1, 1, 5, 0F); // Box 53
		defaultScopeModel[2].setRotationPoint(33F, -24F, -2.5F);

		defaultScopeModel[3].addShapeBox(0F, 0F, 0F, 1, 1, 5, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 54
		defaultScopeModel[3].setRotationPoint(33F, -28F, -2.5F);

		defaultScopeModel[4].addBox(0F, 0F, 0F, 1, 3, 1, 0F); // Box 55
		defaultScopeModel[4].setRotationPoint(33F, -27F, -2.5F);

		defaultScopeModel[5].addBox(0F, 0F, 0F, 1, 3, 1, 0F); // Box 56
		defaultScopeModel[5].setRotationPoint(33F, -27F, 1.5F);

		defaultScopeModel[6].addBox(0F, 0F, 0F, 4, 3, 3, 0F); // Box 57
		defaultScopeModel[6].setRotationPoint(84F, -22.5F, -1.5F);


		defaultStockModel = new ModelRendererTurbo[20];
		defaultStockModel[0] = new ModelRendererTurbo(this, 1, 20, textureX, textureY); // Box 20
		defaultStockModel[1] = new ModelRendererTurbo(this, 1, 28, textureX, textureY); // Box 22
		defaultStockModel[2] = new ModelRendererTurbo(this, 37, 33, textureX, textureY); // Box 23
		defaultStockModel[3] = new ModelRendererTurbo(this, 37, 33, textureX, textureY); // Box 24
		defaultStockModel[4] = new ModelRendererTurbo(this, 30, 30, textureX, textureY); // Box 25
		defaultStockModel[5] = new ModelRendererTurbo(this, 30, 30, textureX, textureY); // Box 26
		defaultStockModel[6] = new ModelRendererTurbo(this, 30, 30, textureX, textureY); // Box 27
		defaultStockModel[7] = new ModelRendererTurbo(this, 10, 28, textureX, textureY); // Box 28
		defaultStockModel[8] = new ModelRendererTurbo(this, 23, 28, textureX, textureY); // Box 29
		defaultStockModel[9] = new ModelRendererTurbo(this, 10, 28, textureX, textureY); // Box 41
		defaultStockModel[10] = new ModelRendererTurbo(this, 30, 30, textureX, textureY); // Box 42
		defaultStockModel[11] = new ModelRendererTurbo(this, 37, 33, textureX, textureY); // Box 43
		defaultStockModel[12] = new ModelRendererTurbo(this, 37, 33, textureX, textureY); // Box 44
		defaultStockModel[13] = new ModelRendererTurbo(this, 30, 30, textureX, textureY); // Box 45
		defaultStockModel[14] = new ModelRendererTurbo(this, 30, 30, textureX, textureY); // Box 46
		defaultStockModel[15] = new ModelRendererTurbo(this, 1, 28, textureX, textureY); // Box 47
		defaultStockModel[16] = new ModelRendererTurbo(this, 1, 20, textureX, textureY); // Box 48
		defaultStockModel[17] = new ModelRendererTurbo(this, 23, 28, textureX, textureY); // Box 49
		defaultStockModel[18] = new ModelRendererTurbo(this, 1, 161, textureX, textureY); // Box 50
		defaultStockModel[19] = new ModelRendererTurbo(this, 37, 21, textureX, textureY); // Box 51

		defaultStockModel[0].addShapeBox(0F, 0F, 0F, 20, 6, 1, 0F, 0F, 0F, 0F, 0F, 7F, 0F, 0F, 7F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, 0F, -7F, 0F, 0F, -7F, 0F, -0.5F, 0F, 0F); // Box 20
		defaultStockModel[0].setRotationPoint(-8F, -20F, -6F);

		defaultStockModel[1].addBox(0F, 0F, 0F, 3, 6, 1, 0F); // Box 22
		defaultStockModel[1].setRotationPoint(12F, -27F, -6F);

		defaultStockModel[2].addBox(0F, 0F, 0F, 18, 1, 1, 0F); // Box 23
		defaultStockModel[2].setRotationPoint(15F, -27F, -6F);

		defaultStockModel[3].addBox(0F, 0F, 0F, 18, 1, 1, 0F); // Box 24
		defaultStockModel[3].setRotationPoint(15F, -22F, -6F);

		defaultStockModel[4].addBox(0F, 0F, 0F, 2, 4, 1, 0F); // Box 25
		defaultStockModel[4].setRotationPoint(19F, -26F, -6F);

		defaultStockModel[5].addBox(0F, 0F, 0F, 2, 4, 1, 0F); // Box 26
		defaultStockModel[5].setRotationPoint(25F, -26F, -6F);

		defaultStockModel[6].addBox(0F, 0F, 0F, 2, 4, 1, 0F); // Box 27
		defaultStockModel[6].setRotationPoint(31F, -26F, -6F);

		defaultStockModel[7].addBox(0F, 0F, 0F, 5, 6, 1, 0F); // Box 28
		defaultStockModel[7].setRotationPoint(33F, -27F, -6F);

		defaultStockModel[8].addShapeBox(0F, 0F, 0F, 2, 6, 1, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, -0.5F, -0.5F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, -0.5F, -0.5F, 0F); // Box 29
		defaultStockModel[8].setRotationPoint(-10F, -20F, -6F);

		defaultStockModel[9].addBox(0F, 0F, 0F, 5, 6, 1, 0F); // Box 41
		defaultStockModel[9].setRotationPoint(33F, -27F, 5F);

		defaultStockModel[10].addBox(0F, 0F, 0F, 2, 4, 1, 0F); // Box 42
		defaultStockModel[10].setRotationPoint(31F, -26F, 5F);

		defaultStockModel[11].addBox(0F, 0F, 0F, 18, 1, 1, 0F); // Box 43
		defaultStockModel[11].setRotationPoint(15F, -22F, 5F);

		defaultStockModel[12].addBox(0F, 0F, 0F, 18, 1, 1, 0F); // Box 44
		defaultStockModel[12].setRotationPoint(15F, -27F, 5F);

		defaultStockModel[13].addBox(0F, 0F, 0F, 2, 4, 1, 0F); // Box 45
		defaultStockModel[13].setRotationPoint(25F, -26F, 5F);

		defaultStockModel[14].addBox(0F, 0F, 0F, 2, 4, 1, 0F); // Box 46
		defaultStockModel[14].setRotationPoint(19F, -26F, 5F);

		defaultStockModel[15].addBox(0F, 0F, 0F, 3, 6, 1, 0F); // Box 47
		defaultStockModel[15].setRotationPoint(12F, -27F, 5F);

		defaultStockModel[16].addShapeBox(0F, 0F, 0F, 20, 6, 1, 0F, 0F, 0F, 0F, 0F, 7F, 0F, 0F, 7F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, 0F, -7F, 0F, 0F, -7F, 0F, -0.5F, 0F, 0F); // Box 48
		defaultStockModel[16].setRotationPoint(-8F, -20F, 5F);

		defaultStockModel[17].addShapeBox(0F, 0F, 0F, 2, 6, 1, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, -0.5F, -0.5F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, -0.5F, -0.5F, 0F); // Box 49
		defaultStockModel[17].setRotationPoint(-10F, -20F, 5F);

		defaultStockModel[18].addBox(0F, 0F, 0F, 19, 1, 10, 0F); // Box 50
		defaultStockModel[18].setRotationPoint(12F, -22F, -5F);

		defaultStockModel[19].addBox(0F, 0F, 0F, 2, 1, 10, 0F); // Box 51
		defaultStockModel[19].setRotationPoint(36F, -22F, -5F);


		ammoModel = new ModelRendererTurbo[6];
		ammoModel[0] = new ModelRendererTurbo(this, 36, 1, textureX, textureY); // casing1
		ammoModel[1] = new ModelRendererTurbo(this, 36, 1, textureX, textureY); // casing1-2
		ammoModel[2] = new ModelRendererTurbo(this, 36, 1, textureX, textureY); // casing1-3
		ammoModel[3] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // shell1
		ammoModel[4] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // shell1-2
		ammoModel[5] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // shell1-3

		ammoModel[0].addShapeBox(0F, 0F, 0F, 2, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // casing1
		ammoModel[0].setRotationPoint(14F, -13.5F, -3F);

		ammoModel[1].addBox(0F, 0F, 0F, 2, 2, 6, 0F); // casing1-2
		ammoModel[1].setRotationPoint(14F, -11.5F, -3F);

		ammoModel[2].addShapeBox(0F, 0F, 0F, 2, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // casing1-3
		ammoModel[2].setRotationPoint(14F, -9.5F, -3F);

		ammoModel[3].addShapeBox(0F, 0F, 0F, 11, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // shell1
		ammoModel[3].setRotationPoint(16F, -13.5F, -3F);

		ammoModel[4].addBox(0F, 0F, 0F, 11, 2, 6, 0F); // shell1-2
		ammoModel[4].setRotationPoint(16F, -11.5F, -3F);

		ammoModel[5].addShapeBox(0F, 0F, 0F, 11, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // shell1-3
		ammoModel[5].setRotationPoint(16F, -9.5F, -3F);


		pumpModel = new ModelRendererTurbo[16];
		pumpModel[0] = new ModelRendererTurbo(this, 80, 24, textureX, textureY); // Box 13
		pumpModel[1] = new ModelRendererTurbo(this, 80, 20, textureX, textureY); // Box 14
		pumpModel[2] = new ModelRendererTurbo(this, 194, 151, textureX, textureY); // Box 58
		pumpModel[3] = new ModelRendererTurbo(this, 217, 134, textureX, textureY); // Box 59
		pumpModel[4] = new ModelRendererTurbo(this, 153, 134, textureX, textureY); // Box 60
		pumpModel[5] = new ModelRendererTurbo(this, 184, 134, textureX, textureY); // Box 61
		pumpModel[6] = new ModelRendererTurbo(this, 153, 134, textureX, textureY); // Box 62
		pumpModel[7] = new ModelRendererTurbo(this, 81, 134, textureX, textureY); // Box 63
		pumpModel[8] = new ModelRendererTurbo(this, 128, 134, textureX, textureY); // Box 64
		pumpModel[9] = new ModelRendererTurbo(this, 128, 134, textureX, textureY); // Box 65
		pumpModel[10] = new ModelRendererTurbo(this, 144, 149, textureX, textureY); // Box 66
		pumpModel[11] = new ModelRendererTurbo(this, 111, 149, textureX, textureY); // Box 67
		pumpModel[12] = new ModelRendererTurbo(this, 225, 151, textureX, textureY); // Box 68
		pumpModel[13] = new ModelRendererTurbo(this, 80, 149, textureX, textureY); // Box 69
		pumpModel[14] = new ModelRendererTurbo(this, 80, 149, textureX, textureY); // Box 70
		pumpModel[15] = new ModelRendererTurbo(this, 169, 151, textureX, textureY); // Box 71

		pumpModel[0].addBox(0F, 0F, 0F, 13, 4, 2, 0F); // Box 13
		pumpModel[0].setRotationPoint(14F, -19F, -4.5F);

		pumpModel[1].addShapeBox(0F, 0F, 0F, 13, 1, 2, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 14
		pumpModel[1].setRotationPoint(14F, -20F, -4.5F);

		pumpModel[2].addShapeBox(0F, 0F, 0F, 4, 10, 11, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 58
		pumpModel[2].setRotationPoint(67F, -17.5F, -5.5F);

		pumpModel[3].addShapeBox(0F, 0F, 0F, 4, 3, 11, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 59
		pumpModel[3].setRotationPoint(67F, -7.5F, -5.5F);

		pumpModel[4].addShapeBox(0F, 0F, 0F, 4, 3, 11, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 2F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 2F, -2F); // Box 60
		pumpModel[4].setRotationPoint(63F, -7.5F, -5.5F);

		pumpModel[5].addShapeBox(0F, 0F, 0F, 5, 3, 11, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 61
		pumpModel[5].setRotationPoint(58F, -5.5F, -5.5F);

		pumpModel[6].addShapeBox(0F, 0F, 0F, 4, 3, 11, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 2F, -2F, 0F, 2F, -2F, 0F, 0F, -2F); // Box 62
		pumpModel[6].setRotationPoint(54F, -7.5F, -5.5F);

		pumpModel[7].addShapeBox(0F, 0F, 0F, 12, 3, 11, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 63
		pumpModel[7].setRotationPoint(42F, -7.5F, -5.5F);

		pumpModel[8].addShapeBox(0F, 0F, 0F, 1, 3, 11, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 2F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 2F, -2F); // Box 64
		pumpModel[8].setRotationPoint(41F, -7.5F, -5.5F);

		pumpModel[9].addShapeBox(0F, 0F, 0F, 1, 3, 11, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 65
		pumpModel[9].setRotationPoint(40F, -5.5F, -5.5F);

		pumpModel[10].addBox(0F, 0F, 0F, 1, 12, 11, 0F); // Box 66
		pumpModel[10].setRotationPoint(40F, -17.5F, -5.5F);

		pumpModel[11].addBox(0F, 0F, 0F, 5, 12, 11, 0F); // Box 67
		pumpModel[11].setRotationPoint(58F, -17.5F, -5.5F);

		pumpModel[12].addBox(0F, 0F, 0F, 12, 10, 11, 0F); // Box 68
		pumpModel[12].setRotationPoint(42F, -17.5F, -5.5F);

		pumpModel[13].addShapeBox(0F, 0F, 0F, 4, 12, 11, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F); // Box 69
		pumpModel[13].setRotationPoint(54F, -17.5F, -5.5F);

		pumpModel[14].addShapeBox(0F, 0F, 0F, 4, 12, 11, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F); // Box 70
		pumpModel[14].setRotationPoint(63F, -17.5F, -5.5F);

		pumpModel[15].addShapeBox(0F, 0F, 0F, 1, 10, 11, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F); // Box 71
		pumpModel[15].setRotationPoint(41F, -17.5F, -5.5F);

		barrelAttachPoint = new Vector3f(88F /16F, 17.5F /16F, 0F /16F);
		stockAttachPoint = new Vector3f(-13F /16F, 13.5F /16F, 0F /16F);
		scopeAttachPoint = new Vector3f(53F /16F, 22F /16F, 0F /16F);

		animationType = EnumAnimationType.SHOTGUN;

		numBulletsInReloadAnimation = 6;
		tiltGunTime = 0.200F;
		unloadClipTime = 0.0F;
		loadClipTime = 0.600F;
		untiltGunTime = 0.200F;

		pumpDelay = 10;
		pumpTime = 7;
		pumpDelayAfterReload = 110;
		pumpHandleDistance = 1F;

		translateAll(0F, 0F, 0F);


		flipAll();
	}
}