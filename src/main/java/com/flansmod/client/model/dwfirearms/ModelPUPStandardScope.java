package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.ModelAttachment;
import com.flansmod.client.tmt.ModelRendererTurbo;

import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelPUPStandardScope extends ModelAttachment
{
	int textureX = 512;
	int textureY = 256;

	public ModelPUPStandardScope()
	{
		attachmentModel = new ModelRendererTurbo[43];
		attachmentModel[0] = new ModelRendererTurbo(this, 1, 26, textureX, textureY); // mounter
		attachmentModel[1] = new ModelRendererTurbo(this, 1, 39, textureX, textureY); // mounterBase3
		attachmentModel[2] = new ModelRendererTurbo(this, 1, 47, textureX, textureY); // scope4
		attachmentModel[3] = new ModelRendererTurbo(this, 1, 56, textureX, textureY); // scope3
		attachmentModel[4] = new ModelRendererTurbo(this, 1, 56, textureX, textureY); // scope3
		attachmentModel[5] = new ModelRendererTurbo(this, 1, 56, textureX, textureY); // scope3
		attachmentModel[6] = new ModelRendererTurbo(this, 1, 56, textureX, textureY); // scope3
		attachmentModel[7] = new ModelRendererTurbo(this, 91, 116, textureX, textureY); // scope1
		attachmentModel[8] = new ModelRendererTurbo(this, 91, 116, textureX, textureY); // scope1
		attachmentModel[9] = new ModelRendererTurbo(this, 70, 116, textureX, textureY); // scope2
		attachmentModel[10] = new ModelRendererTurbo(this, 1, 47, textureX, textureY); // Box 13
		attachmentModel[11] = new ModelRendererTurbo(this, 40, 42, textureX, textureY); // Box 18
		attachmentModel[12] = new ModelRendererTurbo(this, 40, 42, textureX, textureY); // Box 19
		attachmentModel[13] = new ModelRendererTurbo(this, 1, 96, textureX, textureY); // Box 22
		attachmentModel[14] = new ModelRendererTurbo(this, 1, 80, textureX, textureY); // Box 30
		attachmentModel[15] = new ModelRendererTurbo(this, 1, 62, textureX, textureY); // Box 31
		attachmentModel[16] = new ModelRendererTurbo(this, 1, 80, textureX, textureY); // Box 32
		attachmentModel[17] = new ModelRendererTurbo(this, 1, 96, textureX, textureY); // Box 33
		attachmentModel[18] = new ModelRendererTurbo(this, 68, 96, textureX, textureY); // Box 35
		attachmentModel[19] = new ModelRendererTurbo(this, 68, 80, textureX, textureY); // Box 36
		attachmentModel[20] = new ModelRendererTurbo(this, 68, 62, textureX, textureY); // Box 37
		attachmentModel[21] = new ModelRendererTurbo(this, 68, 80, textureX, textureY); // Box 38
		attachmentModel[22] = new ModelRendererTurbo(this, 68, 96, textureX, textureY); // Box 39
		attachmentModel[23] = new ModelRendererTurbo(this, 68, 62, textureX, textureY); // Box 40
		attachmentModel[24] = new ModelRendererTurbo(this, 68, 80, textureX, textureY); // Box 41
		attachmentModel[25] = new ModelRendererTurbo(this, 68, 96, textureX, textureY); // Box 42
		attachmentModel[26] = new ModelRendererTurbo(this, 68, 80, textureX, textureY); // Box 43
		attachmentModel[27] = new ModelRendererTurbo(this, 68, 96, textureX, textureY); // Box 44
		attachmentModel[28] = new ModelRendererTurbo(this, 103, 113, textureX, textureY); // scope4
		attachmentModel[29] = new ModelRendererTurbo(this, 84, 113, textureX, textureY); // scope3
		attachmentModel[30] = new ModelRendererTurbo(this, 84, 113, textureX, textureY); // scope3
		attachmentModel[31] = new ModelRendererTurbo(this, 26, 112, textureX, textureY); // glass2
		attachmentModel[32] = new ModelRendererTurbo(this, 1, 110, textureX, textureY); // glass3
		attachmentModel[33] = new ModelRendererTurbo(this, 26, 112, textureX, textureY); // glass2
		attachmentModel[34] = new ModelRendererTurbo(this, 51, 116, textureX, textureY); // glass1
		attachmentModel[35] = new ModelRendererTurbo(this, 51, 116, textureX, textureY); // glass1
		attachmentModel[36] = new ModelRendererTurbo(this, 26, 112, textureX, textureY); // glass2
		attachmentModel[37] = new ModelRendererTurbo(this, 1, 110, textureX, textureY); // glass3
		attachmentModel[38] = new ModelRendererTurbo(this, 26, 112, textureX, textureY); // glass2
		attachmentModel[39] = new ModelRendererTurbo(this, 51, 116, textureX, textureY); // glass1
		attachmentModel[40] = new ModelRendererTurbo(this, 40, 42, textureX, textureY); // Box 59
		attachmentModel[41] = new ModelRendererTurbo(this, 40, 42, textureX, textureY); // Box 60
		attachmentModel[42] = new ModelRendererTurbo(this, 51, 116, textureX, textureY); // glass1

		attachmentModel[0].addBox(0F, 0F, 0F, 20, 3, 9, 0F); // mounter
		attachmentModel[0].setRotationPoint(0F, -27F, -4.5F);

		attachmentModel[1].addBox(0F, 0F, 0F, 14, 2, 5, 0F); // mounterBase3
		attachmentModel[1].setRotationPoint(3F, -29F, -2.5F);

		attachmentModel[2].addShapeBox(0F, 0F, 0F, 32, 1, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope4
		attachmentModel[2].setRotationPoint(-4F, -39F, -3.5F);

		attachmentModel[3].addShapeBox(0F, 0F, 0F, 32, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F); // scope3
		attachmentModel[3].setRotationPoint(-4F, -38F, -3.5F);

		attachmentModel[4].addShapeBox(0F, 0F, 0F, 32, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F); // scope3
		attachmentModel[4].setRotationPoint(-4F, -34F, 4.5F);

		attachmentModel[5].addShapeBox(0F, 0F, 0F, 32, 4, 1, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope3
		attachmentModel[5].setRotationPoint(-4F, -38F, 4.5F);

		attachmentModel[6].addShapeBox(0F, 0F, 0F, 32, 4, 1, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope3
		attachmentModel[6].setRotationPoint(-4F, -34F, -3.5F);

		attachmentModel[7].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F); // scope1
		attachmentModel[7].setRotationPoint(7F, -41F, -3.5F);

		attachmentModel[8].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F); // scope1
		attachmentModel[8].setRotationPoint(12F, -41F, -3.5F);

		attachmentModel[9].addBox(0F, 0F, 0F, 3, 2, 7, 0F); // scope2
		attachmentModel[9].setRotationPoint(9F, -41F, -3.5F);

		attachmentModel[10].addShapeBox(0F, 0F, 0F, 32, 1, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 13
		attachmentModel[10].setRotationPoint(-4F, -30F, -3.5F);

		attachmentModel[11].addShapeBox(0F, 0F, 0F, 4, 2, 2, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 18
		attachmentModel[11].setRotationPoint(11F, -36F, -8F);

		attachmentModel[12].addShapeBox(0F, 0F, 0F, 4, 2, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 19
		attachmentModel[12].setRotationPoint(11F, -34F, -8F);

		attachmentModel[13].addShapeBox(0F, 0F, 0F, 20, 2, 11, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 22
		attachmentModel[13].setRotationPoint(28F, -40F, -5.5F);

		attachmentModel[14].addShapeBox(0F, 0F, 0F, 20, 2, 13, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 30
		attachmentModel[14].setRotationPoint(28F, -38F, -6.5F);

		attachmentModel[15].addBox(0F, 0F, 0F, 20, 4, 13, 0F); // Box 31
		attachmentModel[15].setRotationPoint(28F, -36F, -6.5F);

		attachmentModel[16].addShapeBox(0F, 0F, 0F, 20, 2, 13, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 32
		attachmentModel[16].setRotationPoint(28F, -32F, -6.5F);

		attachmentModel[17].addShapeBox(0F, 0F, 0F, 20, 2, 11, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 33
		attachmentModel[17].setRotationPoint(28F, -30F, -5.5F);

		attachmentModel[18].addShapeBox(0F, 0F, 0F, 11, 2, 11, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 35
		attachmentModel[18].setRotationPoint(5F, -30F, -5.5F);

		attachmentModel[19].addShapeBox(0F, 0F, 0F, 11, 2, 13, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 36
		attachmentModel[19].setRotationPoint(5F, -32F, -6.5F);

		attachmentModel[20].addBox(0F, 0F, 0F, 11, 4, 13, 0F); // Box 37
		attachmentModel[20].setRotationPoint(5F, -36F, -6.5F);

		attachmentModel[21].addShapeBox(0F, 0F, 0F, 11, 2, 13, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 38
		attachmentModel[21].setRotationPoint(5F, -38F, -6.5F);

		attachmentModel[22].addShapeBox(0F, 0F, 0F, 11, 2, 11, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 39
		attachmentModel[22].setRotationPoint(5F, -40F, -5.5F);

		attachmentModel[23].addBox(0F, 0F, 0F, 11, 4, 13, 0F); // Box 40
		attachmentModel[23].setRotationPoint(-15F, -36F, -6.5F);

		attachmentModel[24].addShapeBox(0F, 0F, 0F, 11, 2, 13, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 41
		attachmentModel[24].setRotationPoint(-15F, -32F, -6.5F);

		attachmentModel[25].addShapeBox(0F, 0F, 0F, 11, 2, 11, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 42
		attachmentModel[25].setRotationPoint(-15F, -30F, -5.5F);

		attachmentModel[26].addShapeBox(0F, 0F, 0F, 11, 2, 13, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 43
		attachmentModel[26].setRotationPoint(-15F, -38F, -6.5F);

		attachmentModel[27].addShapeBox(0F, 0F, 0F, 11, 2, 11, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 44
		attachmentModel[27].setRotationPoint(-15F, -40F, -5.5F);

		attachmentModel[28].addBox(0F, 0F, 0F, 3, 7, 2, 0F); // scope4
		attachmentModel[28].setRotationPoint(9F, -37.5F, 5.5F);

		attachmentModel[29].addShapeBox(0F, 0F, 0F, 2, 7, 2, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F); // scope3
		attachmentModel[29].setRotationPoint(7F, -37.5F, 5.5F);

		attachmentModel[30].addShapeBox(0F, 0F, 0F, 2, 7, 2, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F); // scope3
		attachmentModel[30].setRotationPoint(12F, -37.5F, 5.5F);

		attachmentModel[31].addShapeBox(0F, 0F, 0F, 1, 2, 11, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F); // glass2
		attachmentModel[31].setRotationPoint(-15.2F, -32F, -5.5F);

		attachmentModel[32].addBox(0F, 0F, 0F, 1, 4, 11, 0F); // glass3
		attachmentModel[32].setRotationPoint(-15.2F, -36F, -5.5F);

		attachmentModel[33].addShapeBox(0F, 0F, 0F, 1, 2, 11, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // glass2
		attachmentModel[33].setRotationPoint(-15.2F, -38F, -5.5F);

		attachmentModel[34].addShapeBox(0F, 0F, 0F, 1, 1, 8, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // glass1
		attachmentModel[34].setRotationPoint(-15.2F, -39F, -4F);

		attachmentModel[35].addShapeBox(0F, 0F, 0F, 1, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // glass1
		attachmentModel[35].setRotationPoint(47.2F, -30F, -4F);

		attachmentModel[36].addShapeBox(0F, 0F, 0F, 1, 2, 11, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F); // glass2
		attachmentModel[36].setRotationPoint(47.2F, -32F, -5.5F);

		attachmentModel[37].addBox(0F, 0F, 0F, 1, 4, 11, 0F); // glass3
		attachmentModel[37].setRotationPoint(47.2F, -36F, -5.5F);

		attachmentModel[38].addShapeBox(0F, 0F, 0F, 1, 2, 11, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // glass2
		attachmentModel[38].setRotationPoint(47.2F, -38F, -5.5F);

		attachmentModel[39].addShapeBox(0F, 0F, 0F, 1, 1, 8, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // glass1
		attachmentModel[39].setRotationPoint(47.2F, -39F, -4F);

		attachmentModel[40].addShapeBox(0F, 0F, 0F, 4, 2, 2, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 59
		attachmentModel[40].setRotationPoint(6F, -36F, -8F);

		attachmentModel[41].addShapeBox(0F, 0F, 0F, 4, 2, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 60
		attachmentModel[41].setRotationPoint(6F, -34F, -8F);

		attachmentModel[42].addShapeBox(0F, 0F, 0F, 1, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // glass1
		attachmentModel[42].setRotationPoint(-15.2F, -30F, -4F);

		renderOffset = 0F;

		flipAll();
	}
}