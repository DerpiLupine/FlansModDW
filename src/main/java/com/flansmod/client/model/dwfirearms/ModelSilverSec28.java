package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelSilverSec28 extends ModelGun
{
	int textureX = 512;
	int textureY = 256;

	public ModelSilverSec28()
	{
		gunModel = new ModelRendererTurbo[12];
		gunModel[0] = new ModelRendererTurbo(this, 64, 1, textureX, textureY); // barrel1
		gunModel[1] = new ModelRendererTurbo(this, 64, 1, textureX, textureY); // barrel1-2
		gunModel[2] = new ModelRendererTurbo(this, 64, 10, textureX, textureY); // barrel2
		gunModel[3] = new ModelRendererTurbo(this, 117, 14, textureX, textureY); // barrelLower1
		gunModel[4] = new ModelRendererTurbo(this, 117, 14, textureX, textureY); // barrelLower1-2
		gunModel[5] = new ModelRendererTurbo(this, 117, 14, textureX, textureY); // barrelLower1-3
		gunModel[6] = new ModelRendererTurbo(this, 64, 19, textureX, textureY); // body1
		gunModel[7] = new ModelRendererTurbo(this, 120, 32, textureX, textureY); // body2
		gunModel[8] = new ModelRendererTurbo(this, 1, 17, textureX, textureY); // grip1
		gunModel[9] = new ModelRendererTurbo(this, 40, 33, textureX, textureY); // grip2
		gunModel[10] = new ModelRendererTurbo(this, 1, 6, textureX, textureY); // gripConnector
		gunModel[11] = new ModelRendererTurbo(this, 96, 90, textureX, textureY); // hammer

		gunModel[0].addShapeBox(0F, 0F, 0F, 20, 2, 6, 0F,0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // barrel1
		gunModel[0].setRotationPoint(9F, -24.5F, -3F);

		gunModel[1].addShapeBox(0F, 0F, 0F, 20, 2, 6, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // barrel1-2
		gunModel[1].setRotationPoint(9F, -20.5F, -3F);

		gunModel[2].addBox(0F, 0F, 0F, 20, 2, 6, 0F); // barrel2
		gunModel[2].setRotationPoint(9F, -22.5F, -3F);

		gunModel[3].addShapeBox(0F, 0F, 0F, 2, 1, 3, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // barrelLower1
		gunModel[3].setRotationPoint(26.5F, -18F, -1.5F);

		gunModel[4].addBox(0F, 0F, 0F, 2, 1, 3, 0F); // barrelLower1-2
		gunModel[4].setRotationPoint(26.5F, -17F, -1.5F);

		gunModel[5].addShapeBox(0F, 0F, 0F, 2, 1, 3, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // barrelLower1-3
		gunModel[5].setRotationPoint(26.5F, -16F, -1.5F);

		gunModel[6].addBox(0F, 0F, 0F, 38, 4, 8, 0F); // body1
		gunModel[6].setRotationPoint(-11F, -18F, -4F);

		gunModel[7].addBox(0F, 0F, 0F, 14, 2, 1, 0F); // body2
		gunModel[7].setRotationPoint(8F, -17.5F, 3.5F);

		gunModel[8].addShapeBox(0F, 0F, 0F, 12, 18, 7, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, -2F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 3F, -2F, 0F); // grip1
		gunModel[8].setRotationPoint(-8F, -12F, -3.5F);

		gunModel[9].addBox(0F, 0F, 0F, 2, 2, 7, 0F); // grip2
		gunModel[9].setRotationPoint(-11F, 4F, -3.5F);
		gunModel[9].rotateAngleZ = -0.17453293F;

		gunModel[10].addShapeBox(0F, 0F, 0F, 13, 2, 8, 0F,2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // gripConnector
		gunModel[10].setRotationPoint(-8F, -14F, -4F);

		gunModel[11].addShapeBox(0F, 0F, 0F, 4, 2, 2, 0F,0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F); // hammer
		gunModel[11].setRotationPoint(-12F, -23F, -1F);


		ammoModel = new ModelRendererTurbo[4];
		ammoModel[0] = new ModelRendererTurbo(this, 1, 54, textureX, textureY); // ammoClip
		ammoModel[1] = new ModelRendererTurbo(this, 36, 71, textureX, textureY); // bullet
		ammoModel[2] = new ModelRendererTurbo(this, 36, 65, textureX, textureY); // bulletTip
		ammoModel[3] = new ModelRendererTurbo(this, 1, 43, textureX, textureY); // clip2

		ammoModel[0].addShapeBox(0F, 0F, 0F, 11, 16, 6, 0F,0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 3F, -2F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 3F, -2F, 0F); // ammoClip
		ammoModel[0].setRotationPoint(-8F, -8F, -3F);

		ammoModel[1].addShapeBox(0F, 0F, 0F, 6, 1, 4, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // bullet
		ammoModel[1].setRotationPoint(-7F, -10.5F, -2F);
		ammoModel[1].rotateAngleZ = -0.17453293F;

		ammoModel[2].addShapeBox(0F, 0F, 0F, 3, 1, 4, 0F,0F, 0F, -1F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F); // bulletTip
		ammoModel[2].setRotationPoint(-1F, -9.5F, -2F);
		ammoModel[2].rotateAngleZ = -0.17453293F;

		ammoModel[3].addBox(0F, 0F, 0F, 11, 3, 7, 0F); // clip2
		ammoModel[3].setRotationPoint(-9F, 4.5F, -3.5F);
		ammoModel[3].rotateAngleZ = -0.16580628F;


		slideModel = new ModelRendererTurbo[18];
		slideModel[0] = new ModelRendererTurbo(this, 64, 81, textureX, textureY); // bodyRail1
		slideModel[1] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 24
		slideModel[2] = new ModelRendererTurbo(this, 64, 81, textureX, textureY); // Box 3
		slideModel[3] = new ModelRendererTurbo(this, 64, 81, textureX, textureY); // Box 4
		slideModel[4] = new ModelRendererTurbo(this, 64, 81, textureX, textureY); // Box 5
		slideModel[5] = new ModelRendererTurbo(this, 64, 81, textureX, textureY); // Box 6
		slideModel[6] = new ModelRendererTurbo(this, 14, 2, textureX, textureY); // ironSight
		slideModel[7] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // ironSight2
		slideModel[8] = new ModelRendererTurbo(this, 64, 66, textureX, textureY); // slide1
		slideModel[9] = new ModelRendererTurbo(this, 99, 69, textureX, textureY); // slide2
		slideModel[10] = new ModelRendererTurbo(this, 126, 66, textureX, textureY); // slide3
		slideModel[11] = new ModelRendererTurbo(this, 64, 32, textureX, textureY); // slide4
		slideModel[12] = new ModelRendererTurbo(this, 107, 57, textureX, textureY); // slide5
		slideModel[13] = new ModelRendererTurbo(this, 105, 46, textureX, textureY); // slide6
		slideModel[14] = new ModelRendererTurbo(this, 64, 41, textureX, textureY); // slide7
		slideModel[15] = new ModelRendererTurbo(this, 64, 56, textureX, textureY); // slide8
		slideModel[16] = new ModelRendererTurbo(this, 83, 85, textureX, textureY); // slideEnd1
		slideModel[17] = new ModelRendererTurbo(this, 83, 85, textureX, textureY); // slideEnd1-2

		slideModel[0].addShapeBox(0F, 0F, 0F, 1, 5, 8, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.2F, 0F, 0F, 0.2F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0.2F, 1F, 0F, 0.2F); // bodyRail1
		slideModel[0].setRotationPoint(-6F, -23F, -4.1F);

		slideModel[1].addBox(0F, 0F, 0F, 4, 2, 2, 0F); // Box 24
		slideModel[1].setRotationPoint(-8F, -27.5F, -2.5F);

		slideModel[2].addShapeBox(0F, 0F, 0F, 1, 5, 8, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.2F, 0F, 0F, 0.2F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0.2F, 1F, 0F, 0.2F); // Box 3
		slideModel[2].setRotationPoint(-4F, -23F, -4.1F);

		slideModel[3].addShapeBox(0F, 0F, 0F, 1, 5, 8, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.2F, 0F, 0F, 0.2F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0.2F, 1F, 0F, 0.2F); // Box 4
		slideModel[3].setRotationPoint(-2F, -23F, -4.1F);

		slideModel[4].addShapeBox(0F, 0F, 0F, 1, 5, 8, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.2F, 0F, 0F, 0.2F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0.2F, 1F, 0F, 0.2F); // Box 5
		slideModel[4].setRotationPoint(0F, -23F, -4.1F);

		slideModel[5].addShapeBox(0F, 0F, 0F, 1, 5, 8, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.2F, 0F, 0F, 0.2F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0.2F, 1F, 0F, 0.2F); // Box 6
		slideModel[5].setRotationPoint(2F, -23F, -4.1F);

		slideModel[6].addShapeBox(0F, 0F, 0F, 4, 1, 2, 0F,0F, 0.5F, -0.5F, 0F, 0.5F, -0.5F, 0F, 0.5F, -0.5F, 0F, 0.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // ironSight
		slideModel[6].setRotationPoint(23F, -26F, -1F);

		slideModel[7].addBox(0F, 0F, 0F, 4, 2, 2, 0F); // ironSight2
		slideModel[7].setRotationPoint(-8F, -27.5F, 0.5F);

		slideModel[8].addBox(0F, 0F, 0F, 10, 7, 7, 0F); // slide1
		slideModel[8].setRotationPoint(18F, -24F, -3.5F);

		slideModel[9].addBox(0F, 0F, 0F, 9, 7, 4, 0F); // slide2
		slideModel[9].setRotationPoint(9F, -24F, -0.5F);

		slideModel[10].addBox(0F, 0F, 0F, 5, 7, 7, 0F); // slide3
		slideModel[10].setRotationPoint(4F, -24F, -3.5F);

		slideModel[11].addShapeBox(0F, 0F, 0F, 24, 1, 7, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // slide4
		slideModel[11].setRotationPoint(4F, -25F, -3.5F);

		slideModel[12].addBox(0F, 0F, 0F, 9, 1, 7, 0F); // slide5
		slideModel[12].setRotationPoint(9F, -24F, -3.5F);

		slideModel[13].addBox(0F, 0F, 0F, 9, 2, 7, 0F); // slide6
		slideModel[13].setRotationPoint(9F, -20F, -3.5F);

		slideModel[14].addBox(0F, 0F, 0F, 12, 6, 8, 0F); // slide7
		slideModel[14].setRotationPoint(-8F, -24F, -4F);

		slideModel[15].addShapeBox(0F, 0F, 0F, 13, 1, 8, 0F,-1F, 0.5F, -1F, 0F, 0.5F, -1F, 0F, 0.5F, -1F, -1F, 0.5F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // slide8
		slideModel[15].setRotationPoint(-9F, -25F, -4F);

		slideModel[16].addShapeBox(0F, 0F, 0F, 3, 6, 3, 0F,-2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // slideEnd1
		slideModel[16].setRotationPoint(-11F, -24F, 1F);

		slideModel[17].addShapeBox(0F, 0F, 0F, 3, 6, 3, 0F,-2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // slideEnd1-2
		slideModel[17].setRotationPoint(-11F, -24F, -4F);



		barrelAttachPoint = new Vector3f(0F /16F, 0F /16F, 0F /16F);
		stockAttachPoint = new Vector3f(0F /16F, 0F /16F, 0F /16F);
		scopeAttachPoint = new Vector3f(0F /16F, 0F /16F, 0F /16F);
		gripAttachPoint = new Vector3f(0 /16F, 0F /16F, 0F /16F);

		gunSlideDistance = 0.8F;
		animationType = EnumAnimationType.CUSTOM;

		/* ----Start of Reload Block---- */
		/* Reload Name: Alternate Pistol Clip */
		rotateGunVertical = 10F;
		translateGun = new Vector3f(0.15F, 0.25F, 0F);

		rotateClipVertical = 5F;
		translateClip = new Vector3f(0F, -3F, 0F);
		/* ----End of Reload Block---- */


		translateAll(2F, 0F, 0F);


		flipAll();
	}
}