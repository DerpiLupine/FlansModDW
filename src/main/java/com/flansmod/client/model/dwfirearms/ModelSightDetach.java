package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.ModelAttachment;
import com.flansmod.client.tmt.ModelRendererTurbo;

import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelSightDetach extends ModelAttachment
{
	int textureX = 512;
	int textureY = 256;

	public ModelSightDetach()
	{
		attachmentModel = new ModelRendererTurbo[1];
		attachmentModel[0] = new ModelRendererTurbo(this, 0, 0, textureX, textureY); // Box 0

		attachmentModel[0].addBox(0F, 0F, 0F, 0, 0, 0, 0F); // Box 0
		attachmentModel[0].setRotationPoint(-2F, -14F, -0.5F);

		renderOffset = 0F;

		flipAll();
	}
}