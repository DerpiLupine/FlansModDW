package com.flansmod.client.model.dwfirearms;

import net.minecraft.client.model.ModelBase;
import net.minecraft.entity.Entity;

import com.flansmod.client.tmt.ModelRendererTurbo;

public class ModelMRE extends ModelBase 
{
	public ModelRendererTurbo[] grenadeModel;

	int textureX = 512;
	int textureY = 256;

	public ModelMRE()
	{
		grenadeModel = new ModelRendererTurbo[1];
		grenadeModel[0] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // lever1

		grenadeModel[0].addShapeBox(-3.5F, -4.5F, -2.7F, 50, 80, 10, 0F, 0F, 0F, 0F, -45F, 0F, 0F, -45F, 0F, -9F, 0F, 0F, -9F, 0F, -72F, 0F, -45F, -72F, 0F, -45F, -72F, -9F, 0F, -72F, -9F); // lever1
		grenadeModel[0].rotateAngleX = 0.17453293F;
		grenadeModel[0].rotateAngleY = -1.57079633F;
		grenadeModel[0].rotateAngleZ = -3.05F;
	}

	@Override
	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
	{
		for(ModelRendererTurbo mineModelBit : grenadeModel)
			mineModelBit.render(f5);
	}
}