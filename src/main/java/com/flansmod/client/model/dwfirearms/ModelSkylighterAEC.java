package com.flansmod.client.model.dwfirearms; //Path where the model is located

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelSkylighterAEC extends ModelGun //Same as Filename
{
	int textureX = 512;
	int textureY = 256;

	public ModelSkylighterAEC() //Same as Filename
	{
		gunModel = new ModelRendererTurbo[120];
		gunModel[0] = new ModelRendererTurbo(this, 1, 55, textureX, textureY); // body11
		gunModel[1] = new ModelRendererTurbo(this, 112, 90, textureX, textureY); // body3
		gunModel[2] = new ModelRendererTurbo(this, 112, 105, textureX, textureY); // body4
		gunModel[3] = new ModelRendererTurbo(this, 177, 78, textureX, textureY); // body5
		gunModel[4] = new ModelRendererTurbo(this, 267, 78, textureX, textureY); // body6
		gunModel[5] = new ModelRendererTurbo(this, 112, 78, textureX, textureY); // body7
		gunModel[6] = new ModelRendererTurbo(this, 1, 67, textureX, textureY); // frontGrip1
		gunModel[7] = new ModelRendererTurbo(this, 1, 79, textureX, textureY); // frontGrip2
		gunModel[8] = new ModelRendererTurbo(this, 28, 44, textureX, textureY); // grip2
		gunModel[9] = new ModelRendererTurbo(this, 22, 1, textureX, textureY); // railPart0
		gunModel[10] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // railPart1
		gunModel[11] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // railPart2
		gunModel[12] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // railPart3
		gunModel[13] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // railPart4
		gunModel[14] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // railPart5
		gunModel[15] = new ModelRendererTurbo(this, 80, 154, textureX, textureY); // stockPart3
		gunModel[16] = new ModelRendererTurbo(this, 87, 56, textureX, textureY); // Box 7
		gunModel[17] = new ModelRendererTurbo(this, 78, 67, textureX, textureY); // Box 0
		gunModel[18] = new ModelRendererTurbo(this, 112, 1, textureX, textureY); // Box 3
		gunModel[19] = new ModelRendererTurbo(this, 97, 43, textureX, textureY); // Box 11
		gunModel[20] = new ModelRendererTurbo(this, 80, 175, textureX, textureY); // Box 3
		gunModel[21] = new ModelRendererTurbo(this, 38, 34, textureX, textureY); // Box 2
		gunModel[22] = new ModelRendererTurbo(this, 38, 38, textureX, textureY); // Box 3
		gunModel[23] = new ModelRendererTurbo(this, 31, 36, textureX, textureY); // Box 5
		gunModel[24] = new ModelRendererTurbo(this, 204, 89, textureX, textureY); // Box 15
		gunModel[25] = new ModelRendererTurbo(this, 1, 100, textureX, textureY); // Box 4
		gunModel[26] = new ModelRendererTurbo(this, 1, 125, textureX, textureY); // Box 5
		gunModel[27] = new ModelRendererTurbo(this, 221, 141, textureX, textureY); // Box 20
		gunModel[28] = new ModelRendererTurbo(this, 221, 124, textureX, textureY); // Box 21
		gunModel[29] = new ModelRendererTurbo(this, 221, 107, textureX, textureY); // Box 22
		gunModel[30] = new ModelRendererTurbo(this, 266, 107, textureX, textureY); // Box 23
		gunModel[31] = new ModelRendererTurbo(this, 266, 124, textureX, textureY); // Box 24
		gunModel[32] = new ModelRendererTurbo(this, 266, 141, textureX, textureY); // Box 25
		gunModel[33] = new ModelRendererTurbo(this, 338, 124, textureX, textureY); // Box 27
		gunModel[34] = new ModelRendererTurbo(this, 338, 107, textureX, textureY); // Box 28
		gunModel[35] = new ModelRendererTurbo(this, 338, 141, textureX, textureY); // Box 29
		gunModel[36] = new ModelRendererTurbo(this, 1, 89, textureX, textureY); // Box 40
		gunModel[37] = new ModelRendererTurbo(this, 307, 107, textureX, textureY); // Box 44
		gunModel[38] = new ModelRendererTurbo(this, 307, 124, textureX, textureY); // Box 45
		gunModel[39] = new ModelRendererTurbo(this, 307, 141, textureX, textureY); // Box 46
		gunModel[40] = new ModelRendererTurbo(this, 1, 116, textureX, textureY); // Box 4
		gunModel[41] = new ModelRendererTurbo(this, 248, 4, textureX, textureY); // Box 5
		gunModel[42] = new ModelRendererTurbo(this, 112, 14, textureX, textureY); // Box 6
		gunModel[43] = new ModelRendererTurbo(this, 142, 31, textureX, textureY); // Box 7
		gunModel[44] = new ModelRendererTurbo(this, 135, 39, textureX, textureY); // Box 8
		gunModel[45] = new ModelRendererTurbo(this, 135, 31, textureX, textureY); // Box 9
		gunModel[46] = new ModelRendererTurbo(this, 112, 37, textureX, textureY); // Box 15
		gunModel[47] = new ModelRendererTurbo(this, 112, 31, textureX, textureY); // Box 16
		gunModel[48] = new ModelRendererTurbo(this, 68, 79, textureX, textureY); // Box 0
		gunModel[49] = new ModelRendererTurbo(this, 68, 92, textureX, textureY); // Box 1
		gunModel[50] = new ModelRendererTurbo(this, 68, 92, textureX, textureY); // Box 2
		gunModel[51] = new ModelRendererTurbo(this, 205, 5, textureX, textureY); // Box 3
		gunModel[52] = new ModelRendererTurbo(this, 205, 5, textureX, textureY); // Box 4
		gunModel[53] = new ModelRendererTurbo(this, 205, 5, textureX, textureY); // Box 5
		gunModel[54] = new ModelRendererTurbo(this, 1, 108, textureX, textureY); // Box 6
		gunModel[55] = new ModelRendererTurbo(this, 213, 63, textureX, textureY); // Box 111
		gunModel[56] = new ModelRendererTurbo(this, 214, 38, textureX, textureY); // Box 11
		gunModel[57] = new ModelRendererTurbo(this, 248, 20, textureX, textureY); // Box 12
		gunModel[58] = new ModelRendererTurbo(this, 213, 22, textureX, textureY); // Box 111
		gunModel[59] = new ModelRendererTurbo(this, 176, 57, textureX, textureY); // Box 0
		gunModel[60] = new ModelRendererTurbo(this, 177, 38, textureX, textureY); // Box 1
		gunModel[61] = new ModelRendererTurbo(this, 112, 60, textureX, textureY); // Box 2
		gunModel[62] = new ModelRendererTurbo(this, 112, 43, textureX, textureY); // Box 3
		gunModel[63] = new ModelRendererTurbo(this, 1, 133, textureX, textureY); // Box 6
		gunModel[64] = new ModelRendererTurbo(this, 1, 192, textureX, textureY); // Box 7
		gunModel[65] = new ModelRendererTurbo(this, 236, 78, textureX, textureY); // Box 8
		gunModel[66] = new ModelRendererTurbo(this, 1, 150, textureX, textureY); // Box 10
		gunModel[67] = new ModelRendererTurbo(this, 177, 91, textureX, textureY); // Box 11
		gunModel[68] = new ModelRendererTurbo(this, 1, 169, textureX, textureY); // Box 12
		gunModel[69] = new ModelRendererTurbo(this, 247, 95, textureX, textureY); // Box 13
		gunModel[70] = new ModelRendererTurbo(this, 80, 189, textureX, textureY); // Box 14
		gunModel[71] = new ModelRendererTurbo(this, 184, 20, textureX, textureY); // Box 16
		gunModel[72] = new ModelRendererTurbo(this, 1, 180, textureX, textureY); // Box 17
		gunModel[73] = new ModelRendererTurbo(this, 249, 42, textureX, textureY); // Box 94
		gunModel[74] = new ModelRendererTurbo(this, 22, 26, textureX, textureY); // Box 111
		gunModel[75] = new ModelRendererTurbo(this, 22, 21, textureX, textureY); // Box 111
		gunModel[76] = new ModelRendererTurbo(this, 1, 44, textureX, textureY); // Box 114
		gunModel[77] = new ModelRendererTurbo(this, 264, 62, textureX, textureY); // Box 111
		gunModel[78] = new ModelRendererTurbo(this, 22, 21, textureX, textureY); // Box 116
		gunModel[79] = new ModelRendererTurbo(this, 1, 31, textureX, textureY); // Box 117
		gunModel[80] = new ModelRendererTurbo(this, 1, 31, textureX, textureY); // Box 119
		gunModel[81] = new ModelRendererTurbo(this, 1, 21, textureX, textureY); // Box 120
		gunModel[82] = new ModelRendererTurbo(this, 1, 26, textureX, textureY); // Box 121
		gunModel[83] = new ModelRendererTurbo(this, 1, 21, textureX, textureY); // Box 122
		gunModel[84] = new ModelRendererTurbo(this, 38, 191, textureX, textureY); // Box 123
		gunModel[85] = new ModelRendererTurbo(this, 80, 133, textureX, textureY); // Box 124
		gunModel[86] = new ModelRendererTurbo(this, 1, 11, textureX, textureY); // Box 125
		gunModel[87] = new ModelRendererTurbo(this, 40, 11, textureX, textureY); // Box 126
		gunModel[88] = new ModelRendererTurbo(this, 62, 56, textureX, textureY); // Box 0
		gunModel[89] = new ModelRendererTurbo(this, 43, 21, textureX, textureY); // Box 1
		gunModel[90] = new ModelRendererTurbo(this, 43, 25, textureX, textureY); // Box 2
		gunModel[91] = new ModelRendererTurbo(this, 60, 21, textureX, textureY); // Box 3
		gunModel[92] = new ModelRendererTurbo(this, 176, 124, textureX, textureY); // Box 7
		gunModel[93] = new ModelRendererTurbo(this, 176, 141, textureX, textureY); // Box 8
		gunModel[94] = new ModelRendererTurbo(this, 176, 107, textureX, textureY); // Box 9
		gunModel[95] = new ModelRendererTurbo(this, 112, 144, textureX, textureY); // Box 10
		gunModel[96] = new ModelRendererTurbo(this, 112, 129, textureX, textureY); // Box 11
		gunModel[97] = new ModelRendererTurbo(this, 112, 118, textureX, textureY); // Box 13
		gunModel[98] = new ModelRendererTurbo(this, 113, 172, textureX, textureY); // Box 14
		gunModel[99] = new ModelRendererTurbo(this, 113, 161, textureX, textureY); // Box 15
		gunModel[100] = new ModelRendererTurbo(this, 72, 31, textureX, textureY); // Box 18
		gunModel[101] = new ModelRendererTurbo(this, 72, 31, textureX, textureY); // Box 19
		gunModel[102] = new ModelRendererTurbo(this, 72, 31, textureX, textureY); // Box 20
		gunModel[103] = new ModelRendererTurbo(this, 72, 31, textureX, textureY); // Box 21
		gunModel[104] = new ModelRendererTurbo(this, 72, 37, textureX, textureY); // Box 22
		gunModel[105] = new ModelRendererTurbo(this, 20, 108, textureX, textureY); // Box 26
		gunModel[106] = new ModelRendererTurbo(this, 44, 102, textureX, textureY); // Box 27
		gunModel[107] = new ModelRendererTurbo(this, 44, 95, textureX, textureY); // Box 28
		gunModel[108] = new ModelRendererTurbo(this, 44, 102, textureX, textureY); // Box 30
		gunModel[109] = new ModelRendererTurbo(this, 28, 12, textureX, textureY); // Box 31
		gunModel[110] = new ModelRendererTurbo(this, 28, 12, textureX, textureY); // Box 32
		gunModel[111] = new ModelRendererTurbo(this, 77, 14, textureX, textureY); // Box 33
		gunModel[112] = new ModelRendererTurbo(this, 87, 7, textureX, textureY); // Box 34
		gunModel[113] = new ModelRendererTurbo(this, 69, 22, textureX, textureY); // Box 35
		gunModel[114] = new ModelRendererTurbo(this, 62, 14, textureX, textureY); // Box 42
		gunModel[115] = new ModelRendererTurbo(this, 62, 14, textureX, textureY); // Box 43
		gunModel[116] = new ModelRendererTurbo(this, 304, 42, textureX, textureY); // Box 44
		gunModel[117] = new ModelRendererTurbo(this, 304, 42, textureX, textureY); // Box 45
		gunModel[118] = new ModelRendererTurbo(this, 38, 191, textureX, textureY); // Box 46
		gunModel[119] = new ModelRendererTurbo(this, 38, 195, textureX, textureY); // Box 47

		gunModel[0].addBox(0F, 0F, 0F, 23, 4, 7, 0F); // body11
		gunModel[0].setRotationPoint(16F, -3F, -3.5F);

		gunModel[1].addShapeBox(0F, 0F, 0F, 23, 5, 9, 0F, 7F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 7F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body3
		gunModel[1].setRotationPoint(16F, -9F, -4.5F);

		gunModel[2].addBox(0F, 0F, 0F, 27, 5, 7, 0F); // body4
		gunModel[2].setRotationPoint(-11F, -8F, -3.5F);

		gunModel[3].addShapeBox(0F, 0F, 0F, 20, 1, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // body5
		gunModel[3].setRotationPoint(-11F, -9F, -4.5F);

		gunModel[4].addShapeBox(0F, 0F, 0F, 7, 1, 9, 0F, 0F, 0F, 0F, 0F, -5F, 0F, 0F, -5F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 5F, -1F, 0F, 5F, -1F, 0F, 0F, -1F); // body6
		gunModel[4].setRotationPoint(9F, -9F, -4.5F);

		gunModel[5].addShapeBox(0F, 0F, 0F, 23, 1, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // body7
		gunModel[5].setRotationPoint(16F, -4F, -4.5F);

		gunModel[6].addBox(0F, 0F, 0F, 31, 4, 7, 0F); // frontGrip1
		gunModel[6].setRotationPoint(39F, -7F, -3.5F);

		gunModel[7].addShapeBox(0F, 0F, 0F, 26, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // frontGrip2
		gunModel[7].setRotationPoint(44F, -3F, -3.5F);

		gunModel[8].addShapeBox(0F, 0F, 0F, 4, 3, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F); // grip2
		gunModel[8].setRotationPoint(-5F, -3F, -3.5F);

		gunModel[9].addBox(0F, 0F, 0F, 27, 2, 7, 0F); // railPart0
		gunModel[9].setRotationPoint(7F, -21F, -3.5F);

		gunModel[10].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // railPart1
		gunModel[10].setRotationPoint(7F, -23F, -3.5F);

		gunModel[11].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // railPart2
		gunModel[11].setRotationPoint(13F, -23F, -3.5F);

		gunModel[12].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // railPart3
		gunModel[12].setRotationPoint(19F, -23F, -3.5F);

		gunModel[13].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // railPart4
		gunModel[13].setRotationPoint(25F, -23F, -3.5F);

		gunModel[14].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // railPart5
		gunModel[14].setRotationPoint(31F, -23F, -3.5F);

		gunModel[15].addBox(0F, 0F, 0F, 5, 9, 11, 0F); // stockPart3
		gunModel[15].setRotationPoint(-51F, -12F, -5.5F);

		gunModel[16].addBox(0F, 0F, 0F, 5, 3, 7, 0F); // Box 7
		gunModel[16].setRotationPoint(16F, 1F, -3.5F);

		gunModel[17].addShapeBox(0F, 0F, 0F, 7, 4, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F); // Box 0
		gunModel[17].setRotationPoint(39F, -3F, -3.5F);

		gunModel[18].addShapeBox(0F, 0F, 0F, 37, 3, 9, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 3
		gunModel[18].setRotationPoint(2F, -20F, -4.5F);

		gunModel[19].addShapeBox(0F, 0F, 0F, 1, 6, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F); // Box 11
		gunModel[19].setRotationPoint(35F, 1F, -3F);

		gunModel[20].addShapeBox(0F, 0F, 0F, 5, 2, 11, 0F, -2F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, -2F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 3
		gunModel[20].setRotationPoint(-51F, -14F, -5.5F);

		gunModel[21].addShapeBox(0F, 0F, 0F, 9, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F); // Box 2
		gunModel[21].setRotationPoint(18F, -6F, -6F);

		gunModel[22].addBox(0F, 0F, 0F, 9, 3, 2, 0F); // Box 3
		gunModel[22].setRotationPoint(18F, -9F, -6F);

		gunModel[23].addBox(0F, 0F, 0F, 2, 2, 1, 0F); // Box 5
		gunModel[23].setRotationPoint(24F, -8F, -6.5F);

		gunModel[24].addBox(0F, 0F, 0F, 12, 8, 9, 0F); // Box 15
		gunModel[24].setRotationPoint(27F, -17F, -4.5F);

		gunModel[25].addShapeBox(0F, 0F, 0F, 15, 1, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 4
		gunModel[25].setRotationPoint(70F, -3F, -3F);

		gunModel[26].addBox(0F, 0F, 0F, 46, 1, 6, 0F); // Box 5
		gunModel[26].setRotationPoint(39F, -18F, -3F);

		gunModel[27].addShapeBox(0F, 0F, 0F, 10, 4, 12, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, -4F); // Box 20
		gunModel[27].setRotationPoint(39F, -10F, -6F);

		gunModel[28].addBox(0F, 0F, 0F, 10, 4, 12, 0F); // Box 21
		gunModel[28].setRotationPoint(39F, -14F, -6F);

		gunModel[29].addShapeBox(0F, 0F, 0F, 10, 4, 12, 0F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 22
		gunModel[29].setRotationPoint(39F, -18F, -6F);

		gunModel[30].addShapeBox(0F, 0F, 0F, 8, 4, 12, 0F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 23
		gunModel[30].setRotationPoint(30.5F, -18F, -6F);

		gunModel[31].addBox(0F, 0F, 0F, 8, 4, 12, 0F); // Box 24
		gunModel[31].setRotationPoint(30.5F, -14F, -6F);

		gunModel[32].addShapeBox(0F, 0F, 0F, 8, 4, 12, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, -4F); // Box 25
		gunModel[32].setRotationPoint(30.5F, -10F, -6F);

		gunModel[33].addShapeBox(0F, 0F, 0F, 3, 4, 12, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F); // Box 27
		gunModel[33].setRotationPoint(78F, -14F, -6F);

		gunModel[34].addShapeBox(0F, 0F, 0F, 3, 4, 12, 0F, 0F, 0F, -4F, 0F, -2F, -4F, 0F, -2F, -4F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F); // Box 28
		gunModel[34].setRotationPoint(78F, -18F, -6F);

		gunModel[35].addShapeBox(0F, 0F, 0F, 3, 4, 12, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, -2F, -4F, 0F, -2F, -4F, 0F, 0F, -4F); // Box 29
		gunModel[35].setRotationPoint(78F, -10F, -6F);

		gunModel[36].addBox(0F, 0F, 0F, 15, 4, 6, 0F); // Box 40
		gunModel[36].setRotationPoint(70F, -7F, -3F);

		gunModel[37].addShapeBox(0F, 0F, 0F, 3, 4, 12, 0F, 0F, -2F, -4F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, -2F, -4F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F); // Box 44
		gunModel[37].setRotationPoint(27.5F, -18F, -6F);

		gunModel[38].addShapeBox(0F, 0F, 0F, 3, 4, 12, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F); // Box 45
		gunModel[38].setRotationPoint(27.5F, -14F, -6F);

		gunModel[39].addShapeBox(0F, 0F, 0F, 3, 4, 12, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, -2F, -4F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, -2F, -4F); // Box 46
		gunModel[39].setRotationPoint(27.5F, -10F, -6F);

		gunModel[40].addShapeBox(0F, 0F, 0F, 46, 2, 6, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 4
		gunModel[40].setRotationPoint(39F, -20F, -3F);

		gunModel[41].addShapeBox(0F, 0F, 0F, 10, 3, 12, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 5
		gunModel[41].setRotationPoint(17F, -17F, -6F);

		gunModel[42].addBox(0F, 0F, 0F, 25, 5, 11, 0F); // Box 6
		gunModel[42].setRotationPoint(2F, -14F, -5F);

		gunModel[43].addBox(0F, 0F, 0F, 2, 5, 1, 0F); // Box 7
		gunModel[43].setRotationPoint(2F, -14F, -6F);

		gunModel[44].addBox(0F, 0F, 0F, 21, 2, 1, 0F); // Box 8
		gunModel[44].setRotationPoint(4F, -11F, -6F);

		gunModel[45].addBox(0F, 0F, 0F, 2, 5, 1, 0F); // Box 9
		gunModel[45].setRotationPoint(25F, -14F, -6F);

		gunModel[46].addBox(0F, 0F, 0F, 10, 4, 1, 0F); // Box 15
		gunModel[46].setRotationPoint(17F, -9F, 4F);

		gunModel[47].addShapeBox(0F, 0F, 0F, 10, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F); // Box 16
		gunModel[47].setRotationPoint(17F, -9F, 5F);

		gunModel[48].addBox(0F, 0F, 0F, 3, 4, 8, 0F); // Box 0
		gunModel[48].setRotationPoint(82F, -14F, -4F);

		gunModel[49].addShapeBox(0F, 0F, 0F, 3, 2, 8, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 1
		gunModel[49].setRotationPoint(82F, -16F, -4F);

		gunModel[50].addShapeBox(0F, 0F, 0F, 3, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 2
		gunModel[50].setRotationPoint(82F, -10F, -4F);

		gunModel[51].addBox(0F, 0F, 0F, 6, 2, 6, 0F); // Box 3
		gunModel[51].setRotationPoint(81F, -13F, -3F);

		gunModel[52].addShapeBox(0F, 0F, 0F, 6, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 4
		gunModel[52].setRotationPoint(81F, -15F, -3F);

		gunModel[53].addShapeBox(0F, 0F, 0F, 6, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 5
		gunModel[53].setRotationPoint(81F, -11F, -3F);

		gunModel[54].addShapeBox(0F, 0F, 0F, 3, 1, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 6
		gunModel[54].setRotationPoint(82F, -17F, -3F);

		gunModel[55].addBox(0F, 0F, 0F, 13, 2, 12, 0F); // Box 111
		gunModel[55].setRotationPoint(-11F, -14F, -6F);

		gunModel[56].addShapeBox(0F, 0F, 0F, 5, 6, 12, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 11
		gunModel[56].setRotationPoint(-11F, -20F, -6F);

		gunModel[57].addShapeBox(0F, 0F, 0F, 8, 5, 12, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 12
		gunModel[57].setRotationPoint(-6F, -19F, -6F);

		gunModel[58].addShapeBox(0F, 0F, 0F, 5, 3, 12, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F); // Box 111
		gunModel[58].setRotationPoint(-3F, -12F, -6F);

		gunModel[59].addShapeBox(0F, 0F, 0F, 6, 8, 12, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F); // Box 0
		gunModel[59].setRotationPoint(-17F, -14F, -6F);

		gunModel[60].addShapeBox(0F, 0F, 0F, 6, 6, 12, 0F, 0F, -5F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, -5F, -3F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F); // Box 1
		gunModel[60].setRotationPoint(-17F, -20F, -6F);

		gunModel[61].addBox(0F, 0F, 0F, 18, 5, 12, 0F); // Box 2
		gunModel[61].setRotationPoint(-35F, -11F, -6F);

		gunModel[62].addShapeBox(0F, 0F, 0F, 20, 4, 12, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 3
		gunModel[62].setRotationPoint(-37F, -15F, -6F);

		gunModel[63].addBox(0F, 0F, 0F, 29, 7, 9, 0F); // Box 6
		gunModel[63].setRotationPoint(-46F, -11F, -4.5F);

		gunModel[64].addShapeBox(0F, 0F, 0F, 9, 2, 9, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 7
		gunModel[64].setRotationPoint(-46F, -13F, -4.5F);

		gunModel[65].addShapeBox(0F, 0F, 0F, 6, 1, 9, 0F, 0F, -5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -5F, 0F, 0F, 5F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 5F, -1F); // Box 8
		gunModel[65].setRotationPoint(-17F, -9F, -4.5F);

		gunModel[66].addShapeBox(0F, 0F, 0F, 29, 11, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -10F, 0F, 0F, -10F, 0F, 0F, 0F, 0F); // Box 10
		gunModel[66].setRotationPoint(-46F, -4F, -3.5F);

		gunModel[67].addBox(0F, 0F, 0F, 6, 8, 7, 0F); // Box 11
		gunModel[67].setRotationPoint(-17F, -8F, -3.5F);

		gunModel[68].addShapeBox(0F, 0F, 0F, 29, 3, 7, 0F, 0F, -10F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -10F, 0F, 0F, 10F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 10F, 0F); // Box 12
		gunModel[68].setRotationPoint(-46F, -3F, -3.5F);

		gunModel[69].addShapeBox(0F, 0F, 0F, 6, 2, 9, 0F, 0F, -5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -5F, 0F, 0F, 5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 5F, 0F); // Box 13
		gunModel[69].setRotationPoint(-17F, -11F, -4.5F);

		gunModel[70].addShapeBox(0F, 0F, 0F, 5, 2, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 14
		gunModel[70].setRotationPoint(-51F, 9F, -4.5F);

		gunModel[71].addShapeBox(0F, 0F, 0F, 2, 5, 12, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F); // Box 16
		gunModel[71].setRotationPoint(-37F, -11F, -6F);

		gunModel[72].addShapeBox(0F, 0F, 0F, 29, 1, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 17
		gunModel[72].setRotationPoint(-46F, -4F, -4.5F);

		gunModel[73].addShapeBox(0F, 0F, 0F, 15, 3, 11, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 94
		gunModel[73].setRotationPoint(2F, -17F, -6F);

		gunModel[74].addShapeBox(0F, 0F, 0F, 9, 3, 1, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 111
		gunModel[74].setRotationPoint(-9F, -9F, -5.5F);

		gunModel[75].addShapeBox(0F, 0F, 0F, 9, 3, 1, 0F, -3F, -1F, 0F, -3F, -1F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 111
		gunModel[75].setRotationPoint(-9F, -12F, -5.5F);

		gunModel[76].addBox(0F, 0F, 0F, 6, 3, 7, 0F); // Box 114
		gunModel[76].setRotationPoint(-11F, -3F, -3.5F);

		gunModel[77].addShapeBox(0F, 0F, 0F, 5, 3, 12, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F); // Box 111
		gunModel[77].setRotationPoint(-11F, -12F, -6F);

		gunModel[78].addShapeBox(0F, 0F, 0F, 9, 3, 1, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, -1F, 0F, -3F, -1F, 0F, -3F, 0F, 0F, -3F, 0F, 0F); // Box 116
		gunModel[78].setRotationPoint(-9F, -6F, -5.5F);

		gunModel[79].addShapeBox(0F, 0F, 0F, 9, 3, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F); // Box 117
		gunModel[79].setRotationPoint(-9F, -6F, -4.5F);

		gunModel[80].addBox(0F, 0F, 0F, 9, 3, 9, 0F); // Box 119
		gunModel[80].setRotationPoint(-9F, -9F, -4.5F);

		gunModel[81].addShapeBox(0F, 0F, 0F, 9, 3, 1, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, -1F, 0F, -3F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 120
		gunModel[81].setRotationPoint(-9F, -12F, 4.5F);

		gunModel[82].addShapeBox(0F, 0F, 0F, 9, 3, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 121
		gunModel[82].setRotationPoint(-9F, -9F, 4.5F);

		gunModel[83].addShapeBox(0F, 0F, 0F, 9, 3, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, -1F, 0F, -3F, -1F, 0F); // Box 122
		gunModel[83].setRotationPoint(-9F, -6F, 4.5F);

		gunModel[84].addShapeBox(0F, 0F, 0F, 5, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 123
		gunModel[84].setRotationPoint(-51F, -3F, -5.5F);

		gunModel[85].addBox(0F, 0F, 0F, 5, 11, 9, 0F); // Box 124
		gunModel[85].setRotationPoint(-51F, -2F, -4.5F);

		gunModel[86].addBox(0F, 0F, 0F, 7, 3, 6, 0F); // Box 125
		gunModel[86].setRotationPoint(-1F, -22F, -3F);

		gunModel[87].addShapeBox(0F, 0F, 0F, 7, 1, 6, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 126
		gunModel[87].setRotationPoint(-1F, -23F, -3F);

		gunModel[88].addShapeBox(0F, 0F, 0F, 5, 3, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F); // Box 0
		gunModel[88].setRotationPoint(21F, 1F, -3.5F);

		gunModel[89].addBox(0F, 0F, 0F, 7, 2, 1, 0F); // Box 1
		gunModel[89].setRotationPoint(10F, -8.5F, 4.5F);

		gunModel[90].addBox(0F, 0F, 0F, 7, 2, 1, 0F); // Box 2
		gunModel[90].setRotationPoint(0F, -8.5F, 3.5F);

		gunModel[91].addShapeBox(0F, 0F, 0F, 3, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F, 0F); // Box 3
		gunModel[91].setRotationPoint(7F, -8.5F, 3.5F);

		gunModel[92].addBox(0F, 0F, 0F, 10, 4, 12, 0F); // Box 7
		gunModel[92].setRotationPoint(68F, -14F, -6F);

		gunModel[93].addShapeBox(0F, 0F, 0F, 10, 4, 12, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, -4F); // Box 8
		gunModel[93].setRotationPoint(68F, -10F, -6F);

		gunModel[94].addShapeBox(0F, 0F, 0F, 10, 4, 12, 0F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 9
		gunModel[94].setRotationPoint(68F, -18F, -6F);

		gunModel[95].addBox(0F, 0F, 0F, 19, 4, 12, 0F); // Box 10
		gunModel[95].setRotationPoint(49F, -14F, -6F);

		gunModel[96].addShapeBox(0F, 0F, 0F, 19, 2, 12, 0F, 0F, -0.5F, -2F, 0F, -0.5F, -2F, 0F, -0.5F, -2F, 0F, -0.5F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 11
		gunModel[96].setRotationPoint(49F, -16F, -6F);

		gunModel[97].addShapeBox(0F, 0F, 0F, 19, 2, 8, 0F, 0F, -0.5F, -1F, 0F, -0.5F, -1F, 0F, -0.5F, -1F, 0F, -0.5F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 13
		gunModel[97].setRotationPoint(49F, -17.5F, -4F);

		gunModel[98].addShapeBox(0F, 0F, 0F, 19, 2, 12, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -2F, 0F, -0.5F, -2F, 0F, -0.5F, -2F, 0F, -0.5F, -2F); // Box 14
		gunModel[98].setRotationPoint(49F, -10F, -6F);

		gunModel[99].addShapeBox(0F, 0F, 0F, 19, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -1F, 0F, -0.5F, -1F, 0F, -0.5F, -1F, 0F, -0.5F, -1F); // Box 15
		gunModel[99].setRotationPoint(49F, -8.5F, -4F);

		gunModel[100].addShapeBox(0F, 0F, 0F, 2, 3, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 18
		gunModel[100].setRotationPoint(4F, -17F, 4F);

		gunModel[101].addShapeBox(0F, 0F, 0F, 2, 3, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 19
		gunModel[101].setRotationPoint(7F, -17F, 4F);

		gunModel[102].addShapeBox(0F, 0F, 0F, 2, 3, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 20
		gunModel[102].setRotationPoint(13F, -17F, 4F);

		gunModel[103].addShapeBox(0F, 0F, 0F, 2, 3, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 21
		gunModel[103].setRotationPoint(10F, -17F, 4F);

		gunModel[104].addBox(0F, 0F, 0F, 5, 1, 5, 0F); // Box 22
		gunModel[104].setRotationPoint(-6F, -19.5F, -2.5F);

		gunModel[105].addShapeBox(0F, 0F, 0F, 3, 1, 6, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 26
		gunModel[105].setRotationPoint(82F, -8F, -3F);

		gunModel[106].addShapeBox(0F, 0F, 0F, 1, 1, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 27
		gunModel[106].setRotationPoint(84.5F, -7F, -2F);

		gunModel[107].addBox(0F, 0F, 0F, 1, 2, 4, 0F); // Box 28
		gunModel[107].setRotationPoint(84.5F, -6F, -2F);

		gunModel[108].addShapeBox(0F, 0F, 0F, 1, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 30
		gunModel[108].setRotationPoint(84.5F, -4F, -2F);

		gunModel[109].addShapeBox(0F, 0F, 0F, 2, 1, 7, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 31
		gunModel[109].setRotationPoint(2.5F, -22F, -3.5F);

		gunModel[110].addShapeBox(0F, 0F, 0F, 2, 1, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 32
		gunModel[110].setRotationPoint(2.5F, -21F, -3.5F);

		gunModel[111].addBox(0F, 0F, 0F, 8, 2, 4, 0F); // Box 33
		gunModel[111].setRotationPoint(75F, -22F, -2F);

		gunModel[112].addShapeBox(0F, 0F, 0F, 5, 2, 4, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 34
		gunModel[112].setRotationPoint(75F, -24F, -2F);

		gunModel[113].addBox(0F, 0F, 0F, 3, 2, 4, 0F); // Box 35
		gunModel[113].setRotationPoint(80F, -24F, -2F);

		gunModel[114].addShapeBox(0F, 0F, 0F, 2, 1, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 42
		gunModel[114].setRotationPoint(80.5F, -22F, -2.5F);

		gunModel[115].addShapeBox(0F, 0F, 0F, 2, 1, 5, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 43
		gunModel[115].setRotationPoint(80.5F, -23F, -2.5F);

		gunModel[116].addShapeBox(0F, 0F, 0F, 24, 12, 1, 0F, 0F, 0F, 0F, -12F, 0F, 0F, -12F, 0F, 0F, 0F, 0F, 0F, 0F, -6F, 0F, -12F, -6F, 0F, -12F, -6F, 0F, 0F, -6F, 0F); // Box 44
		gunModel[116].setRotationPoint(-45.5F, -3F, -3.8F);

		gunModel[117].addShapeBox(0F, 0F, 0F, 24, 12, 1, 0F, 0F, 0F, 0F, -12F, 0F, 0F, -12F, 0F, 0F, 0F, 0F, 0F, 0F, -6F, 0F, -12F, -6F, 0F, -12F, -6F, 0F, 0F, -6F, 0F); // Box 45
		gunModel[117].setRotationPoint(-45.5F, -3F, 2.8F);

		gunModel[118].addShapeBox(0F, 0F, 0F, 5, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 46
		gunModel[118].setRotationPoint(-51F, -3F, 3.5F);

		gunModel[119].addBox(0F, 0F, 0F, 5, 1, 7, 0F); // Box 47
		gunModel[119].setRotationPoint(-51F, -3F, -3.5F);


		defaultScopeModel = new ModelRendererTurbo[9];
		defaultScopeModel[0] = new ModelRendererTurbo(this, 84, 1, textureX, textureY); // Box 36
		defaultScopeModel[1] = new ModelRendererTurbo(this, 84, 1, textureX, textureY); // Box 36
		defaultScopeModel[2] = new ModelRendererTurbo(this, 95, 2, textureX, textureY); // Box 36
		defaultScopeModel[3] = new ModelRendererTurbo(this, 95, 2, textureX, textureY); // Box 36
		defaultScopeModel[4] = new ModelRendererTurbo(this, 72, 12, textureX, textureY); // Box 36
		defaultScopeModel[5] = new ModelRendererTurbo(this, 72, 12, textureX, textureY); // Box 36
		defaultScopeModel[6] = new ModelRendererTurbo(this, 84, 24, textureX, textureY); // Box 36
		defaultScopeModel[7] = new ModelRendererTurbo(this, 84, 21, textureX, textureY); // Box 36
		defaultScopeModel[8] = new ModelRendererTurbo(this, 84, 21, textureX, textureY); // Box 36

		defaultScopeModel[0].addBox(0F, 0F, 0F, 3, 2, 2, 0F); // Box 36
		defaultScopeModel[0].setRotationPoint(2F, -24F, -3F);

		defaultScopeModel[1].addBox(0F, 0F, 0F, 3, 2, 2, 0F); // Box 36
		defaultScopeModel[1].setRotationPoint(2F, -24F, 1F);

		defaultScopeModel[2].addBox(0F, 0F, 0F, 3, 2, 1, 0F); // Box 36
		defaultScopeModel[2].setRotationPoint(2F, -26F, 1.5F);

		defaultScopeModel[3].addBox(0F, 0F, 0F, 3, 2, 1, 0F); // Box 36
		defaultScopeModel[3].setRotationPoint(2F, -26F, -2.5F);

		defaultScopeModel[4].addBox(0F, 0F, 0F, 3, 4, 1, 0F); // Box 36
		defaultScopeModel[4].setRotationPoint(80F, -28F, -2F);

		defaultScopeModel[5].addBox(0F, 0F, 0F, 3, 4, 1, 0F); // Box 36
		defaultScopeModel[5].setRotationPoint(80F, -28F, 1F);

		defaultScopeModel[6].addBox(0F, 0F, 0F, 2, 2, 2, 0F); // Box 36
		defaultScopeModel[6].setRotationPoint(80.5F, -26F, -1F);

		defaultScopeModel[7].addShapeBox(0F, 0F, 0F, 3, 1, 1, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 36
		defaultScopeModel[7].setRotationPoint(80F, -29F, 1F);

		defaultScopeModel[8].addShapeBox(0F, 0F, 0F, 3, 1, 1, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 36
		defaultScopeModel[8].setRotationPoint(80F, -29F, -2F);


		ammoModel = new ModelRendererTurbo[13];
		ammoModel[0] = new ModelRendererTurbo(this, 304, 56, textureX, textureY); // Box 127
		ammoModel[1] = new ModelRendererTurbo(this, 373, 57, textureX, textureY); // Box 127
		ammoModel[2] = new ModelRendererTurbo(this, 335, 56, textureX, textureY); // Box 127
		ammoModel[3] = new ModelRendererTurbo(this, 339, 97, textureX, textureY); // Box 127
		ammoModel[4] = new ModelRendererTurbo(this, 370, 80, textureX, textureY); // Box 127
		ammoModel[5] = new ModelRendererTurbo(this, 304, 97, textureX, textureY); // Box 127
		ammoModel[6] = new ModelRendererTurbo(this, 354, 58, textureX, textureY); // Box 127
		ammoModel[7] = new ModelRendererTurbo(this, 351, 78, textureX, textureY); // Box 127
		ammoModel[8] = new ModelRendererTurbo(this, 356, 99, textureX, textureY); // Box 127
		ammoModel[9] = new ModelRendererTurbo(this, 304, 86, textureX, textureY); // Box 127
		ammoModel[10] = new ModelRendererTurbo(this, 304, 78, textureX, textureY); // Box 127
		ammoModel[11] = new ModelRendererTurbo(this, 351, 94, textureX, textureY); // Box 127
		ammoModel[12] = new ModelRendererTurbo(this, 351, 90, textureX, textureY); // Box 127

		ammoModel[0].addBox(0F, 0F, 0F, 9, 15, 6, 0F); // Box 127
		ammoModel[0].setRotationPoint(21.5F, -6F, -3F);

		ammoModel[1].addBox(0F, 0F, 0F, 1, 15, 5, 0F); // Box 127
		ammoModel[1].setRotationPoint(20.5F, -6F, -2.5F);

		ammoModel[2].addBox(0F, 0F, 0F, 3, 15, 6, 0F); // Box 127
		ammoModel[2].setRotationPoint(17.5F, -6F, -3F);

		ammoModel[3].addBox(0F, 0F, 0F, 3, 4, 5, 0F); // Box 127
		ammoModel[3].setRotationPoint(19F, -10F, -2.5F);

		ammoModel[4].addShapeBox(0F, 0F, 0F, 1, 4, 5, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 127
		ammoModel[4].setRotationPoint(18F, -10F, -2.5F);

		ammoModel[5].addShapeBox(0F, 0F, 0F, 12, 4, 5, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 127
		ammoModel[5].setRotationPoint(22F, -10F, -2.5F);

		ammoModel[6].addBox(0F, 0F, 0F, 4, 14, 5, 0F); // Box 127
		ammoModel[6].setRotationPoint(30.5F, -6F, -2.5F);

		ammoModel[7].addShapeBox(0F, 0F, 0F, 3, 5, 6, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 127
		ammoModel[7].setRotationPoint(30.5F, 4F, -3F);

		ammoModel[8].addBox(0F, 0F, 0F, 1, 1, 6, 0F); // Box 127
		ammoModel[8].setRotationPoint(33.5F, 8F, -3F);

		ammoModel[9].addBox(0F, 0F, 0F, 17, 4, 6, 0F); // Box 127
		ammoModel[9].setRotationPoint(17.5F, 9F, -3F);

		ammoModel[10].addShapeBox(0F, 0F, 0F, 17, 1, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F); // Box 127
		ammoModel[10].setRotationPoint(17.5F, 13F, -3F);

		ammoModel[11].addShapeBox(0F, 0F, 0F, 4, 2, 1, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F); // Box 127
		ammoModel[11].setRotationPoint(26F, -10F, -2.5F);

		ammoModel[12].addShapeBox(0F, 0F, 0F, 4, 2, 1, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F); // Box 127
		ammoModel[12].setRotationPoint(26F, -10F, 1.5F);


		pumpModel = new ModelRendererTurbo[3];
		pumpModel[0] = new ModelRendererTurbo(this, 61, 39, textureX, textureY); // Box 4
		pumpModel[1] = new ModelRendererTurbo(this, 61, 36, textureX, textureY); // Box 6
		pumpModel[2] = new ModelRendererTurbo(this, 61, 33, textureX, textureY); // Box 7

		pumpModel[0].addBox(0F, 0F, 0F, 4, 3, 1, 0F); // Box 4
		pumpModel[0].setRotationPoint(21F, -14F, -6.5F);

		pumpModel[1].addShapeBox(0F, 0F, 0F, 4, 1, 1, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 6
		pumpModel[1].setRotationPoint(21F, -14F, -7.5F);

		pumpModel[2].addShapeBox(0F, 0F, 0F, 4, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 7
		pumpModel[2].setRotationPoint(21F, -12F, -7.5F);

		stockAttachPoint = new Vector3f(-11F /16F, 9F /16F, 0F /16F);
		scopeAttachPoint = new Vector3f(30F /16F, 20F /16F, 0F /16F);

		pumpTime = 8;
		pumpHandleDistance = 0.7F;
		pumpDelayAfterReload = 82;

		animationType = EnumAnimationType.BOTTOM_CLIP;

		translateAll(0F, 0F, 0F);


		flipAll();
	}
}