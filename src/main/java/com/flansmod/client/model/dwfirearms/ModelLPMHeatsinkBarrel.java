package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.ModelAttachment;
import com.flansmod.client.tmt.ModelRendererTurbo;

import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelLPMHeatsinkBarrel extends ModelAttachment 
{
	int textureX = 512;
	int textureY = 256;

	public ModelLPMHeatsinkBarrel()
	{	
		attachmentModel = new ModelRendererTurbo[3];
		attachmentModel[0] = new ModelRendererTurbo(this, 1, 155, textureX, textureY); // Box 0
		attachmentModel[1] = new ModelRendererTurbo(this, 1, 141, textureX, textureY); // Box 1
		attachmentModel[2] = new ModelRendererTurbo(this, 1, 141, textureX, textureY); // Box 2

		attachmentModel[0].addBox(0F, -1.5F, -4.5F, 35, 3, 9, 0F); // Box 0
		attachmentModel[0].setRotationPoint(0F, 0F, 0F);

		attachmentModel[1].addShapeBox(0F, -4.5F, -4.5F, 35, 3, 9, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 1
		attachmentModel[1].setRotationPoint(0F, 0F, 0F);

		attachmentModel[2].addShapeBox(0F, 1.5F, -4.5F, 35, 3, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 2
		attachmentModel[2].setRotationPoint(0F, 0F, 0F);

		renderOffset = 0F;

		flipAll();
	}
}