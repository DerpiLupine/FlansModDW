package com.flansmod.client.model.dwfirearms; //Path where the model is located

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelAirmareDefender extends ModelGun //Same as Filename
{
	int textureX = 512;
	int textureY = 256;

	public ModelAirmareDefender() //Same as Filename
	{
		gunModel = new ModelRendererTurbo[124];
		gunModel[0] = new ModelRendererTurbo(this, 59, 78, textureX, textureY); // Box 1
		gunModel[1] = new ModelRendererTurbo(this, 140, 82, textureX, textureY); // Box 2
		gunModel[2] = new ModelRendererTurbo(this, 211, 186, textureX, textureY); // Box 4
		gunModel[3] = new ModelRendererTurbo(this, 140, 183, textureX, textureY); // Box 5
		gunModel[4] = new ModelRendererTurbo(this, 65, 17, textureX, textureY); // Box 6
		gunModel[5] = new ModelRendererTurbo(this, 140, 68, textureX, textureY); // Box 8
		gunModel[6] = new ModelRendererTurbo(this, 140, 126, textureX, textureY); // Box 9
		gunModel[7] = new ModelRendererTurbo(this, 140, 145, textureX, textureY); // Box 10
		gunModel[8] = new ModelRendererTurbo(this, 140, 200, textureX, textureY); // Box 11
		gunModel[9] = new ModelRendererTurbo(this, 225, 133, textureX, textureY); // Box 12
		gunModel[10] = new ModelRendererTurbo(this, 225, 146, textureX, textureY); // Box 13
		gunModel[11] = new ModelRendererTurbo(this, 140, 19, textureX, textureY); // Box 14
		gunModel[12] = new ModelRendererTurbo(this, 140, 29, textureX, textureY); // Box 15
		gunModel[13] = new ModelRendererTurbo(this, 212, 27, textureX, textureY); // Box 16
		gunModel[14] = new ModelRendererTurbo(this, 212, 27, textureX, textureY); // Box 17
		gunModel[15] = new ModelRendererTurbo(this, 167, 170, textureX, textureY); // Box 21
		gunModel[16] = new ModelRendererTurbo(this, 259, 6, textureX, textureY); // Box 22
		gunModel[17] = new ModelRendererTurbo(this, 259, 19, textureX, textureY); // Box 23
		gunModel[18] = new ModelRendererTurbo(this, 235, 29, textureX, textureY); // Box 24
		gunModel[19] = new ModelRendererTurbo(this, 235, 29, textureX, textureY); // Box 25
		gunModel[20] = new ModelRendererTurbo(this, 140, 19, textureX, textureY); // Box 26
		gunModel[21] = new ModelRendererTurbo(this, 185, 27, textureX, textureY); // Box 27
		gunModel[22] = new ModelRendererTurbo(this, 185, 27, textureX, textureY); // Box 28
		gunModel[23] = new ModelRendererTurbo(this, 185, 27, textureX, textureY); // Box 29
		gunModel[24] = new ModelRendererTurbo(this, 206, 27, textureX, textureY); // Box 54
		gunModel[25] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 55
		gunModel[26] = new ModelRendererTurbo(this, 140, 53, textureX, textureY); // Box 56
		gunModel[27] = new ModelRendererTurbo(this, 140, 40, textureX, textureY); // Box 0
		gunModel[28] = new ModelRendererTurbo(this, 197, 113, textureX, textureY); // Box 0
		gunModel[29] = new ModelRendererTurbo(this, 140, 161, textureX, textureY); // Box 3
		gunModel[30] = new ModelRendererTurbo(this, 201, 161, textureX, textureY); // Box 4
		gunModel[31] = new ModelRendererTurbo(this, 218, 164, textureX, textureY); // Box 5
		gunModel[32] = new ModelRendererTurbo(this, 219, 68, textureX, textureY); // Box 10
		gunModel[33] = new ModelRendererTurbo(this, 219, 82, textureX, textureY); // Box 11
		gunModel[34] = new ModelRendererTurbo(this, 169, 103, textureX, textureY); // Box 13
		gunModel[35] = new ModelRendererTurbo(this, 169, 100, textureX, textureY); // Box 14
		gunModel[36] = new ModelRendererTurbo(this, 1, 16, textureX, textureY); // Box 20
		gunModel[37] = new ModelRendererTurbo(this, 30, 22, textureX, textureY); // Box 21
		gunModel[38] = new ModelRendererTurbo(this, 62, 1, textureX, textureY); // Box 22
		gunModel[39] = new ModelRendererTurbo(this, 105, 9, textureX, textureY); // Box 23
		gunModel[40] = new ModelRendererTurbo(this, 105, 9, textureX, textureY); // Box 24
		gunModel[41] = new ModelRendererTurbo(this, 140, 170, textureX, textureY); // Box 26
		gunModel[42] = new ModelRendererTurbo(this, 96, 8, textureX, textureY); // Box 28
		gunModel[43] = new ModelRendererTurbo(this, 43, 4, textureX, textureY); // Box 30
		gunModel[44] = new ModelRendererTurbo(this, 28, 3, textureX, textureY); // Box 31
		gunModel[45] = new ModelRendererTurbo(this, 276, 69, textureX, textureY); // Box 35
		gunModel[46] = new ModelRendererTurbo(this, 215, 100, textureX, textureY); // Box 36
		gunModel[47] = new ModelRendererTurbo(this, 215, 100, textureX, textureY); // Box 37
		gunModel[48] = new ModelRendererTurbo(this, 109, 15, textureX, textureY); // Box 38
		gunModel[49] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 39
		gunModel[50] = new ModelRendererTurbo(this, 240, 103, textureX, textureY); // Box 82
		gunModel[51] = new ModelRendererTurbo(this, 64, 163, textureX, textureY); // Box 98
		gunModel[52] = new ModelRendererTurbo(this, 87, 168, textureX, textureY); // Box 99
		gunModel[53] = new ModelRendererTurbo(this, 257, 103, textureX, textureY); // Box 145
		gunModel[54] = new ModelRendererTurbo(this, 248, 88, textureX, textureY); // Box 146
		gunModel[55] = new ModelRendererTurbo(this, 248, 74, textureX, textureY); // Box 147
		gunModel[56] = new ModelRendererTurbo(this, 248, 74, textureX, textureY); // Box 148
		gunModel[57] = new ModelRendererTurbo(this, 248, 74, textureX, textureY); // Box 149
		gunModel[58] = new ModelRendererTurbo(this, 252, 78, textureX, textureY); // Box 153
		gunModel[59] = new ModelRendererTurbo(this, 263, 78, textureX, textureY); // Box 154
		gunModel[60] = new ModelRendererTurbo(this, 252, 78, textureX, textureY); // Box 161
		gunModel[61] = new ModelRendererTurbo(this, 263, 74, textureX, textureY); // Box 162
		gunModel[62] = new ModelRendererTurbo(this, 252, 78, textureX, textureY); // Box 163
		gunModel[63] = new ModelRendererTurbo(this, 263, 71, textureX, textureY); // Box 164
		gunModel[64] = new ModelRendererTurbo(this, 263, 78, textureX, textureY); // Box 165
		gunModel[65] = new ModelRendererTurbo(this, 263, 74, textureX, textureY); // Box 166
		gunModel[66] = new ModelRendererTurbo(this, 252, 78, textureX, textureY); // Box 167
		gunModel[67] = new ModelRendererTurbo(this, 252, 78, textureX, textureY); // Box 168
		gunModel[68] = new ModelRendererTurbo(this, 263, 71, textureX, textureY); // Box 169
		gunModel[69] = new ModelRendererTurbo(this, 252, 78, textureX, textureY); // Box 170
		gunModel[70] = new ModelRendererTurbo(this, 248, 74, textureX, textureY); // Box 171
		gunModel[71] = new ModelRendererTurbo(this, 248, 74, textureX, textureY); // Box 172
		gunModel[72] = new ModelRendererTurbo(this, 248, 74, textureX, textureY); // Box 173
		gunModel[73] = new ModelRendererTurbo(this, 248, 74, textureX, textureY); // Box 174
		gunModel[74] = new ModelRendererTurbo(this, 252, 78, textureX, textureY); // Box 175
		gunModel[75] = new ModelRendererTurbo(this, 263, 78, textureX, textureY); // Box 176
		gunModel[76] = new ModelRendererTurbo(this, 263, 74, textureX, textureY); // Box 177
		gunModel[77] = new ModelRendererTurbo(this, 252, 78, textureX, textureY); // Box 178
		gunModel[78] = new ModelRendererTurbo(this, 248, 74, textureX, textureY); // Box 179
		gunModel[79] = new ModelRendererTurbo(this, 248, 74, textureX, textureY); // Box 180
		gunModel[80] = new ModelRendererTurbo(this, 252, 78, textureX, textureY); // Box 181
		gunModel[81] = new ModelRendererTurbo(this, 263, 71, textureX, textureY); // Box 182
		gunModel[82] = new ModelRendererTurbo(this, 263, 78, textureX, textureY); // Box 183
		gunModel[83] = new ModelRendererTurbo(this, 252, 78, textureX, textureY); // Box 184
		gunModel[84] = new ModelRendererTurbo(this, 263, 74, textureX, textureY); // Box 185
		gunModel[85] = new ModelRendererTurbo(this, 252, 78, textureX, textureY); // Box 186
		gunModel[86] = new ModelRendererTurbo(this, 263, 71, textureX, textureY); // Box 187
		gunModel[87] = new ModelRendererTurbo(this, 252, 78, textureX, textureY); // Box 188
		gunModel[88] = new ModelRendererTurbo(this, 248, 74, textureX, textureY); // Box 189
		gunModel[89] = new ModelRendererTurbo(this, 248, 74, textureX, textureY); // Box 190
		gunModel[90] = new ModelRendererTurbo(this, 248, 74, textureX, textureY); // Box 191
		gunModel[91] = new ModelRendererTurbo(this, 1, 131, textureX, textureY); // Box 32
		gunModel[92] = new ModelRendererTurbo(this, 42, 131, textureX, textureY); // Box 33
		gunModel[93] = new ModelRendererTurbo(this, 1, 78, textureX, textureY); // Box 34
		gunModel[94] = new ModelRendererTurbo(this, 1, 90, textureX, textureY); // Box 35
		gunModel[95] = new ModelRendererTurbo(this, 1, 104, textureX, textureY); // Box 36
		gunModel[96] = new ModelRendererTurbo(this, 1, 120, textureX, textureY); // Box 38
		gunModel[97] = new ModelRendererTurbo(this, 38, 78, textureX, textureY); // Box 39
		gunModel[98] = new ModelRendererTurbo(this, 38, 90, textureX, textureY); // Box 40
		gunModel[99] = new ModelRendererTurbo(this, 38, 104, textureX, textureY); // Box 41
		gunModel[100] = new ModelRendererTurbo(this, 38, 120, textureX, textureY); // Box 42
		gunModel[101] = new ModelRendererTurbo(this, 140, 11, textureX, textureY); // Box 1
		gunModel[102] = new ModelRendererTurbo(this, 140, 3, textureX, textureY); // Box 2
		gunModel[103] = new ModelRendererTurbo(this, 140, 3, textureX, textureY); // Box 3
		gunModel[104] = new ModelRendererTurbo(this, 222, 2, textureX, textureY); // Box 4
		gunModel[105] = new ModelRendererTurbo(this, 222, 10, textureX, textureY); // Box 5
		gunModel[106] = new ModelRendererTurbo(this, 222, 2, textureX, textureY); // Box 6
		gunModel[107] = new ModelRendererTurbo(this, 222, 2, textureX, textureY); // Box 7
		gunModel[108] = new ModelRendererTurbo(this, 222, 10, textureX, textureY); // Box 8
		gunModel[109] = new ModelRendererTurbo(this, 222, 2, textureX, textureY); // Box 9
		gunModel[110] = new ModelRendererTurbo(this, 140, 3, textureX, textureY); // Box 10
		gunModel[111] = new ModelRendererTurbo(this, 140, 11, textureX, textureY); // Box 11
		gunModel[112] = new ModelRendererTurbo(this, 140, 3, textureX, textureY); // Box 12
		gunModel[113] = new ModelRendererTurbo(this, 189, 2, textureX, textureY); // Box 13
		gunModel[114] = new ModelRendererTurbo(this, 189, 10, textureX, textureY); // Box 14
		gunModel[115] = new ModelRendererTurbo(this, 189, 2, textureX, textureY); // Box 15
		gunModel[116] = new ModelRendererTurbo(this, 189, 2, textureX, textureY); // Box 16
		gunModel[117] = new ModelRendererTurbo(this, 189, 10, textureX, textureY); // Box 17
		gunModel[118] = new ModelRendererTurbo(this, 189, 2, textureX, textureY); // Box 18
		gunModel[119] = new ModelRendererTurbo(this, 96, 8, textureX, textureY); // Box 19
		gunModel[120] = new ModelRendererTurbo(this, 96, 8, textureX, textureY); // Box 20
		gunModel[121] = new ModelRendererTurbo(this, 96, 8, textureX, textureY); // Box 21
		gunModel[122] = new ModelRendererTurbo(this, 76, 4, textureX, textureY); // Box 22
		gunModel[123] = new ModelRendererTurbo(this, 76, 4, textureX, textureY); // Box 23

		gunModel[0].addBox(0F, 0F, 0F, 7, 3, 8, 0F); // Box 1
		gunModel[0].setRotationPoint(-9F, -12F, -4F);

		gunModel[1].addShapeBox(0F, 0F, 0F, 28, 4, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F); // Box 2
		gunModel[1].setRotationPoint(-9F, -23F, -4.5F);

		gunModel[2].addBox(0F, 0F, 0F, 18, 3, 8, 0F); // Box 4
		gunModel[2].setRotationPoint(-2F, -12F, -4F);

		gunModel[3].addBox(0F, 0F, 0F, 25, 4, 8, 0F); // Box 5
		gunModel[3].setRotationPoint(16F, -12F, -4F);

		gunModel[4].addBox(0F, 0F, 0F, 17, 2, 9, 0F); // Box 6
		gunModel[4].setRotationPoint(19F, -8F, -4.5F);

		gunModel[5].addShapeBox(0F, 0F, 0F, 28, 2, 9, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 8
		gunModel[5].setRotationPoint(-9F, -25F, -4.5F);

		gunModel[6].addBox(0F, 0F, 0F, 31, 7, 10, 0F); // Box 9
		gunModel[6].setRotationPoint(41F, -17F, -5F);

		gunModel[7].addShapeBox(0F, 0F, 0F, 31, 3, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 10
		gunModel[7].setRotationPoint(41F, -10F, -5F);

		gunModel[8].addBox(0F, 0F, 0F, 30, 1, 9, 0F); // Box 11
		gunModel[8].setRotationPoint(41F, -18F, -4.5F);

		gunModel[9].addShapeBox(0F, 0F, 0F, 21, 3, 9, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 12
		gunModel[9].setRotationPoint(41F, -24F, -4.5F);

		gunModel[10].addBox(0F, 0F, 0F, 21, 3, 9, 0F); // Box 13
		gunModel[10].setRotationPoint(41F, -21F, -4.5F);

		gunModel[11].addShapeBox(0F, 0F, 0F, 15, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 14
		gunModel[11].setRotationPoint(62F, -23F, -3.5F);

		gunModel[12].addBox(0F, 0F, 0F, 15, 3, 7, 0F); // Box 15
		gunModel[12].setRotationPoint(62F, -21F, -3.5F);

		gunModel[13].addBox(0F, 0F, 0F, 2, 3, 9, 0F); // Box 16
		gunModel[13].setRotationPoint(69F, -21F, -4.5F);

		gunModel[14].addShapeBox(0F, 0F, 0F, 2, 3, 9, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 17
		gunModel[14].setRotationPoint(69F, -24F, -4.5F);

		gunModel[15].addBox(0F, 0F, 0F, 5, 2, 8, 0F); // Box 21
		gunModel[15].setRotationPoint(36F, -8F, -4F);

		gunModel[16].addShapeBox(0F, 0F, 0F, 15, 2, 10, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 22
		gunModel[16].setRotationPoint(42F, -25F, -5F);

		gunModel[17].addBox(0F, 0F, 0F, 15, 2, 10, 0F); // Box 23
		gunModel[17].setRotationPoint(42F, -23F, -5F);

		gunModel[18].addShapeBox(0F, 0F, 0F, 1, 2, 8, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 24
		gunModel[18].setRotationPoint(41F, -24.5F, -4F);

		gunModel[19].addBox(0F, 0F, 0F, 1, 2, 8, 0F); // Box 25
		gunModel[19].setRotationPoint(41F, -22.5F, -4F);

		gunModel[20].addShapeBox(0F, 0F, 0F, 15, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 26
		gunModel[20].setRotationPoint(62F, -18F, -3.5F);

		gunModel[21].addShapeBox(0F, 0F, 0F, 4, 3, 9, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 27
		gunModel[21].setRotationPoint(77F, -21F, -4.5F);

		gunModel[22].addShapeBox(0F, 0F, 0F, 4, 3, 9, 0F, 0F, 0F, -3F, -1F, 0F, -3F, -1F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F); // Box 28
		gunModel[22].setRotationPoint(77F, -24F, -4.5F);

		gunModel[23].addShapeBox(0F, 0F, 0F, 4, 3, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 29
		gunModel[23].setRotationPoint(77F, -18F, -4.5F);

		gunModel[24].addShapeBox(0F, 0F, 0F, 3, 4, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 2F); // Box 54
		gunModel[24].setRotationPoint(77.5F, -17F, -2F);

		gunModel[25].addBox(0F, 0F, 0F, 4, 2, 9, 0F); // Box 55
		gunModel[25].setRotationPoint(77F, -24.5F, -4.5F);

		gunModel[26].addBox(0F, 0F, 0F, 50, 6, 6, 0F); // Box 56
		gunModel[26].setRotationPoint(-9F, -18F, -2F);

		gunModel[27].addShapeBox(0F, 0F, 0F, 50, 1, 9, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 0
		gunModel[27].setRotationPoint(-9F, -19F, -4.5F);

		gunModel[28].addShapeBox(0F, 0F, 0F, 17, 1, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F); // Box 0
		gunModel[28].setRotationPoint(19F, -20F, -4.5F);

		gunModel[29].addBox(0F, 0F, 0F, 28, 6, 2, 0F); // Box 3
		gunModel[29].setRotationPoint(-9F, -18F, -4F);

		gunModel[30].addBox(0F, 0F, 0F, 6, 6, 2, 0F); // Box 4
		gunModel[30].setRotationPoint(35F, -18F, -4F);

		gunModel[31].addBox(0F, 0F, 0F, 16, 3, 2, 0F); // Box 5
		gunModel[31].setRotationPoint(19F, -15F, -4F);

		gunModel[32].addShapeBox(0F, 0F, 0F, 5, 2, 9, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 10
		gunModel[32].setRotationPoint(36F, -25F, -4.5F);

		gunModel[33].addShapeBox(0F, 0F, 0F, 5, 4, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F); // Box 11
		gunModel[33].setRotationPoint(36F, -23F, -4.5F);

		gunModel[34].addBox(0F, 0F, 0F, 17, 2, 1, 0F); // Box 13
		gunModel[34].setRotationPoint(19F, -22F, -3.5F);

		gunModel[35].addShapeBox(0F, 0F, 0F, 17, 1, 1, 0F, 0F, -0.5F, -0.25F, 0F, -0.5F, -0.25F, 0F, -0.5F, -0.25F, 0F, -0.5F, -0.25F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 14
		gunModel[35].setRotationPoint(19F, -23F, -3.5F);

		gunModel[36].addBox(0F, 0F, 0F, 3, 3, 9, 0F); // Box 20
		gunModel[36].setRotationPoint(5F, -14F, -4.5F);

		gunModel[37].addBox(0F, 0F, 0F, 6, 1, 1, 0F); // Box 21
		gunModel[37].setRotationPoint(9F, -12F, 3.5F);

		gunModel[38].addBox(0F, 0F, 0F, 3, 3, 8, 0F); // Box 22
		gunModel[38].setRotationPoint(0F, -26F, -4F);

		gunModel[39].addBox(0F, 0F, 0F, 10, 2, 1, 0F); // Box 23
		gunModel[39].setRotationPoint(-1F, -26.5F, -3F);

		gunModel[40].addBox(0F, 0F, 0F, 10, 2, 1, 0F); // Box 24
		gunModel[40].setRotationPoint(-1F, -26.5F, 2F);

		gunModel[41].addBox(0F, 0F, 0F, 3, 2, 8, 0F); // Box 26
		gunModel[41].setRotationPoint(16F, -8F, -4F);

		gunModel[42].addShapeBox(0F, 0F, 0F, 2, 2, 2, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 28
		gunModel[42].setRotationPoint(0.5F, -35F, 2F);

		gunModel[43].addShapeBox(0F, 0F, 0F, 2, 1, 6, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 30
		gunModel[43].setRotationPoint(0.5F, -36F, -3F);

		gunModel[44].addBox(0F, 0F, 0F, 2, 5, 8, 0F); // Box 31
		gunModel[44].setRotationPoint(0.5F, -31F, -4F);

		gunModel[45].addShapeBox(0F, 0F, 0F, 1, 24, 18, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -9F, 0F, 0F, -9F, 0F, -12F, 0F, 0F, -12F, 0F, 0F, -12F, -9F, 0F, -12F, -9F); // Box 35
		gunModel[45].setRotationPoint(9F, -36.75F, -4.25F);

		gunModel[46].addShapeBox(-1F, -1F, -6F, 2, 1, 10, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 36
		gunModel[46].setRotationPoint(79F, -24F, 1F);

		gunModel[47].addShapeBox(-1F, 0F, -6F, 2, 1, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 37
		gunModel[47].setRotationPoint(79F, -24F, 1F);

		gunModel[48].addBox(0F, 0F, 0F, 2, 1, 9, 0F); // Box 38
		gunModel[48].setRotationPoint(78F, -25.5F, -4.5F);

		gunModel[49].addBox(0F, 0F, 0F, 2, 5, 1, 0F); // Box 39
		gunModel[49].setRotationPoint(78F, -30.5F, -0.5F);

		gunModel[50].addBox(0F, 0F, 0F, 2, 2, 6, 0F); // Box 82
		gunModel[50].setRotationPoint(34F, -22F, -2.5F);

		gunModel[51].addShapeBox(0F, 0F, 0F, 8, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 98
		gunModel[51].setRotationPoint(22F, -19.5F, -1F);

		gunModel[52].addShapeBox(0F, 0F, 0F, 3, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -0.75F, -1F, 0F, -0.75F, -1F, 0F, 0F, -1F); // Box 99
		gunModel[52].setRotationPoint(30F, -19.5F, -1F);

		gunModel[53].addBox(0F, 0F, 0F, 3, 2, 6, 0F); // Box 145
		gunModel[53].setRotationPoint(19F, -22F, -2.5F);

		gunModel[54].addBox(0F, 0F, 0F, 3, 3, 4, 0F); // Box 146
		gunModel[54].setRotationPoint(33F, -23F, -1.5F);

		gunModel[55].addShapeBox(0F, 0F, 0F, 6, 2, 1, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 147
		gunModel[55].setRotationPoint(37F, -25F, -6F);

		gunModel[56].addBox(0F, 0F, 0F, 6, 2, 1, 0F); // Box 148
		gunModel[56].setRotationPoint(37F, -23F, -6F);

		gunModel[57].addShapeBox(0F, 0F, 0F, 6, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F); // Box 149
		gunModel[57].setRotationPoint(37F, -21F, -6F);

		gunModel[58].addShapeBox(0F, 0F, 0F, 4, 2, 1, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F); // Box 153
		gunModel[58].setRotationPoint(41.5F, -25F, -6F);

		gunModel[59].addShapeBox(0F, 0F, 0F, 8, 2, 1, 0F, 0F, 0.5F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0.5F, 0F, 0F, -0.5F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, -0.5F, 0F); // Box 154
		gunModel[59].setRotationPoint(45.5F, -25F, -6F);

		gunModel[60].addShapeBox(0F, 0F, 0F, 4, 2, 1, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F); // Box 161
		gunModel[60].setRotationPoint(41F, -23F, -6F);

		gunModel[61].addShapeBox(0F, 0F, 0F, 7, 2, 1, 0F, 0F, 0.5F, 0F, 0F, 2.5F, 0F, 0F, 2.5F, 0F, 0F, 0.5F, 0F, 0F, -0.5F, 0F, 0F, -3.5F, 0F, 0F, -3.5F, 0F, 0F, -0.5F, 0F); // Box 162
		gunModel[61].setRotationPoint(45F, -23F, -6F);

		gunModel[62].addShapeBox(0F, 0F, 0F, 4, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F); // Box 163
		gunModel[62].setRotationPoint(40.5F, -21.5F, -6F);

		gunModel[63].addShapeBox(0F, 0F, 0F, 5, 1, 1, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F); // Box 164
		gunModel[63].setRotationPoint(44.5F, -21.5F, -6F);

		gunModel[64].addShapeBox(0F, 0F, 0F, 8, 2, 1, 0F, 0F, 0.5F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0.5F, 0F, 0F, -0.5F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, -0.5F, 0F); // Box 165
		gunModel[64].setRotationPoint(45.5F, -25F, 5F);

		gunModel[65].addShapeBox(0F, 0F, 0F, 7, 2, 1, 0F, 0F, 0.5F, 0F, 0F, 2.5F, 0F, 0F, 2.5F, 0F, 0F, 0.5F, 0F, 0F, -0.5F, 0F, 0F, -3.5F, 0F, 0F, -3.5F, 0F, 0F, -0.5F, 0F); // Box 166
		gunModel[65].setRotationPoint(45F, -23F, 5F);

		gunModel[66].addShapeBox(0F, 0F, 0F, 4, 2, 1, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F); // Box 167
		gunModel[66].setRotationPoint(41.5F, -25F, 5F);

		gunModel[67].addShapeBox(0F, 0F, 0F, 4, 2, 1, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F); // Box 168
		gunModel[67].setRotationPoint(41F, -23F, 5F);

		gunModel[68].addShapeBox(0F, 0F, 0F, 5, 1, 1, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F); // Box 169
		gunModel[68].setRotationPoint(44.5F, -21.5F, 5F);

		gunModel[69].addShapeBox(0F, 0F, 0F, 4, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F); // Box 170
		gunModel[69].setRotationPoint(40.5F, -21.5F, 5F);

		gunModel[70].addShapeBox(0F, 0F, 0F, 6, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F); // Box 171
		gunModel[70].setRotationPoint(37F, -21F, 5F);

		gunModel[71].addBox(0F, 0F, 0F, 6, 2, 1, 0F); // Box 172
		gunModel[71].setRotationPoint(37F, -23F, 5F);

		gunModel[72].addShapeBox(0F, 0F, 0F, 6, 2, 1, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 173
		gunModel[72].setRotationPoint(37F, -25F, 5F);

		gunModel[73].addShapeBox(0F, 0F, 0F, 6, 2, 1, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 174
		gunModel[73].setRotationPoint(-8F, -14F, -4.5F);

		gunModel[74].addShapeBox(0F, 0F, 0F, 4, 2, 1, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F); // Box 175
		gunModel[74].setRotationPoint(-10.5F, -10F, -4.5F);

		gunModel[75].addShapeBox(0F, 0F, 0F, 8, 2, 1, 0F, 0F, -4F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -4F, 0F, 0F, 3F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 3F, 0F); // Box 176
		gunModel[75].setRotationPoint(-18.5F, -10F, -4.5F);

		gunModel[76].addShapeBox(0F, 0F, 0F, 7, 2, 1, 0F, 0F, -3.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -3.5F, 0F, 0F, 2.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 2.5F, 0F); // Box 177
		gunModel[76].setRotationPoint(-17F, -12F, -4.5F);

		gunModel[77].addShapeBox(0F, 0F, 0F, 4, 2, 1, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F); // Box 178
		gunModel[77].setRotationPoint(-10F, -12F, -4.5F);

		gunModel[78].addBox(0F, 0F, 0F, 6, 2, 1, 0F); // Box 179
		gunModel[78].setRotationPoint(-8F, -12F, -4.5F);

		gunModel[79].addShapeBox(0F, 0F, 0F, 6, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F); // Box 180
		gunModel[79].setRotationPoint(-8F, -10F, -4.5F);

		gunModel[80].addShapeBox(0F, 0F, 0F, 4, 2, 1, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 181
		gunModel[80].setRotationPoint(-9.5F, -13.5F, -4.5F);

		gunModel[81].addShapeBox(0F, 0F, 0F, 5, 1, 1, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F); // Box 182
		gunModel[81].setRotationPoint(-14.5F, -12.5F, -4.5F);

		gunModel[82].addShapeBox(0F, 0F, 0F, 8, 2, 1, 0F, 0F, -4F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -4F, 0F, 0F, 3F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 3F, 0F); // Box 183
		gunModel[82].setRotationPoint(-18.5F, -10F, 3.5F);

		gunModel[83].addShapeBox(0F, 0F, 0F, 4, 2, 1, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F); // Box 184
		gunModel[83].setRotationPoint(-10.5F, -10F, 3.5F);

		gunModel[84].addShapeBox(0F, 0F, 0F, 7, 2, 1, 0F, 0F, -3.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -3.5F, 0F, 0F, 2.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 2.5F, 0F); // Box 185
		gunModel[84].setRotationPoint(-17F, -12F, 3.5F);

		gunModel[85].addShapeBox(0F, 0F, 0F, 4, 2, 1, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F); // Box 186
		gunModel[85].setRotationPoint(-10F, -12F, 3.5F);

		gunModel[86].addShapeBox(0F, 0F, 0F, 5, 1, 1, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F); // Box 187
		gunModel[86].setRotationPoint(-14.5F, -12.5F, 3.5F);

		gunModel[87].addShapeBox(0F, 0F, 0F, 4, 2, 1, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 188
		gunModel[87].setRotationPoint(-9.5F, -13.5F, 3.5F);

		gunModel[88].addShapeBox(0F, 0F, 0F, 6, 2, 1, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 189
		gunModel[88].setRotationPoint(-8F, -14F, 3.5F);

		gunModel[89].addBox(0F, 0F, 0F, 6, 2, 1, 0F); // Box 190
		gunModel[89].setRotationPoint(-8F, -12F, 3.5F);

		gunModel[90].addShapeBox(0F, 0F, 0F, 6, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F); // Box 191
		gunModel[90].setRotationPoint(-8F, -10F, 3.5F);

		gunModel[91].addShapeBox(0F, 0F, 0F, 12, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F); // Box 32
		gunModel[91].setRotationPoint(-9F, -9F, -4F);

		gunModel[92].addBox(0F, 0F, 0F, 6, 3, 8, 0F); // Box 33
		gunModel[92].setRotationPoint(3F, -9F, -4F);

		gunModel[93].addShapeBox(0F, 0F, 0F, 10, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 34
		gunModel[93].setRotationPoint(-6F, -6F, -4F);

		gunModel[94].addShapeBox(0F, 0F, 0F, 10, 5, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F); // Box 35
		gunModel[94].setRotationPoint(-6F, -3F, -4F);

		gunModel[95].addShapeBox(0F, 0F, 0F, 10, 7, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, -3F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 3F, -3F, 0F); // Box 36
		gunModel[95].setRotationPoint(-8F, 2F, -4F);

		gunModel[96].addShapeBox(0F, 0F, 0F, 10, 2, 8, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F); // Box 38
		gunModel[96].setRotationPoint(-11F, 9F, -4F);

		gunModel[97].addShapeBox(0F, 0F, 0F, 2, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F); // Box 39
		gunModel[97].setRotationPoint(4F, -6F, -4F);

		gunModel[98].addShapeBox(0F, 0F, 0F, 2, 5, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, -1.5F, -2F, 0F, -1.5F, 2F, 0F, 0F); // Box 40
		gunModel[98].setRotationPoint(4F, -3F, -4F);

		gunModel[99].addShapeBox(0F, 0F, 0F, 2, 7, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, -1.5F, -3F, 0F, -1.5F, 3F, 0F, 0F); // Box 41
		gunModel[99].setRotationPoint(2F, 2F, -4F);

		gunModel[100].addShapeBox(0F, 0F, 0F, 2, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F); // Box 42
		gunModel[100].setRotationPoint(-1F, 9F, -4F);

		gunModel[101].addBox(0F, 0F, 0F, 20, 2, 4, 0F); // Box 1
		gunModel[101].setRotationPoint(72F, -13F, 1F);

		gunModel[102].addShapeBox(0F, 0F, 0F, 20, 1, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 2
		gunModel[102].setRotationPoint(72F, -14F, 1F);

		gunModel[103].addShapeBox(0F, 0F, 0F, 20, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 3
		gunModel[103].setRotationPoint(72F, -11F, 1F);

		gunModel[104].addShapeBox(0F, 0F, 0F, 3, 1, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F); // Box 4
		gunModel[104].setRotationPoint(77.5F, -11F, 0.5F);

		gunModel[105].addBox(0F, 0F, 0F, 3, 2, 5, 0F); // Box 5
		gunModel[105].setRotationPoint(77.5F, -13F, 0.5F);

		gunModel[106].addShapeBox(0F, 0F, 0F, 3, 1, 5, 0F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 6
		gunModel[106].setRotationPoint(77.5F, -14F, 0.5F);

		gunModel[107].addShapeBox(0F, 0F, 0F, 3, 1, 5, 0F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 7
		gunModel[107].setRotationPoint(77.5F, -14F, -5.5F);

		gunModel[108].addBox(0F, 0F, 0F, 3, 2, 5, 0F); // Box 8
		gunModel[108].setRotationPoint(77.5F, -13F, -5.5F);

		gunModel[109].addShapeBox(0F, 0F, 0F, 3, 1, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F); // Box 9
		gunModel[109].setRotationPoint(77.5F, -11F, -5.5F);

		gunModel[110].addShapeBox(0F, 0F, 0F, 20, 1, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 10
		gunModel[110].setRotationPoint(72F, -14F, -5F);

		gunModel[111].addBox(0F, 0F, 0F, 20, 2, 4, 0F); // Box 11
		gunModel[111].setRotationPoint(72F, -13F, -5F);

		gunModel[112].addShapeBox(0F, 0F, 0F, 20, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 12
		gunModel[112].setRotationPoint(72F, -11F, -5F);

		gunModel[113].addShapeBox(0F, 0F, 0F, 11, 1, 5, 0F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 13
		gunModel[113].setRotationPoint(91.5F, -14F, 0.5F);

		gunModel[114].addBox(0F, 0F, 0F, 11, 2, 5, 0F); // Box 14
		gunModel[114].setRotationPoint(91.5F, -13F, 0.5F);

		gunModel[115].addShapeBox(0F, 0F, 0F, 11, 1, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F); // Box 15
		gunModel[115].setRotationPoint(91.5F, -11F, 0.5F);

		gunModel[116].addShapeBox(0F, 0F, 0F, 11, 1, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F); // Box 16
		gunModel[116].setRotationPoint(91.5F, -11F, -5.5F);

		gunModel[117].addBox(0F, 0F, 0F, 11, 2, 5, 0F); // Box 17
		gunModel[117].setRotationPoint(91.5F, -13F, -5.5F);

		gunModel[118].addShapeBox(0F, 0F, 0F, 11, 1, 5, 0F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 18
		gunModel[118].setRotationPoint(91.5F, -14F, -5.5F);

		gunModel[119].addShapeBox(0F, 0F, 0F, 2, 2, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 19
		gunModel[119].setRotationPoint(0.5F, -33F, 2F);

		gunModel[120].addShapeBox(0F, 0F, 0F, 2, 2, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F); // Box 20
		gunModel[120].setRotationPoint(0.5F, -33F, -4F);

		gunModel[121].addShapeBox(0F, 0F, 0F, 2, 2, 2, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 21
		gunModel[121].setRotationPoint(0.5F, -35F, -4F);

		gunModel[122].addShapeBox(0F, 0F, 0F, 2, 1, 9, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 22
		gunModel[122].setRotationPoint(0.5F, -27F, -4.5F);

		gunModel[123].addShapeBox(0F, 0F, 0F, 2, 1, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 23
		gunModel[123].setRotationPoint(0.5F, -26F, -4.5F);


		defaultStockModel = new ModelRendererTurbo[14];
		defaultStockModel[0] = new ModelRendererTurbo(this, 122, 59, textureX, textureY); // Box 41
		defaultStockModel[1] = new ModelRendererTurbo(this, 122, 59, textureX, textureY); // Box 42
		defaultStockModel[2] = new ModelRendererTurbo(this, 59, 34, textureX, textureY); // Box 54
		defaultStockModel[3] = new ModelRendererTurbo(this, 105, 59, textureX, textureY); // Box 55
		defaultStockModel[4] = new ModelRendererTurbo(this, 105, 41, textureX, textureY); // Box 67
		defaultStockModel[5] = new ModelRendererTurbo(this, 1, 31, textureX, textureY); // Box 68
		defaultStockModel[6] = new ModelRendererTurbo(this, 28, 60, textureX, textureY); // Box 69
		defaultStockModel[7] = new ModelRendererTurbo(this, 28, 47, textureX, textureY); // Box 70
		defaultStockModel[8] = new ModelRendererTurbo(this, 59, 34, textureX, textureY); // Box 71
		defaultStockModel[9] = new ModelRendererTurbo(this, 30, 32, textureX, textureY); // Box 72
		defaultStockModel[10] = new ModelRendererTurbo(this, 28, 47, textureX, textureY); // Box 74
		defaultStockModel[11] = new ModelRendererTurbo(this, 84, 34, textureX, textureY); // Box 75
		defaultStockModel[12] = new ModelRendererTurbo(this, 1, 51, textureX, textureY); // Box 76
		defaultStockModel[13] = new ModelRendererTurbo(this, 84, 34, textureX, textureY); // Box 77

		defaultStockModel[0].addShapeBox(0F, 0F, 0F, 3, 6, 1, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 41
		defaultStockModel[0].setRotationPoint(-10F, -19F, -5F);

		defaultStockModel[1].addShapeBox(0F, 0F, 0F, 3, 6, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 42
		defaultStockModel[1].setRotationPoint(-10F, -19F, 4F);

		defaultStockModel[2].addShapeBox(0F, 0F, 0F, 4, 2, 8, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 54
		defaultStockModel[2].setRotationPoint(-13F, -23F, -4F);

		defaultStockModel[3].addBox(0F, 0F, 0F, 4, 10, 8, 0F); // Box 55
		defaultStockModel[3].setRotationPoint(-13F, -21F, -4F);

		defaultStockModel[4].addShapeBox(0F, 0F, 0F, 4, 7, 8, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F); // Box 67
		defaultStockModel[4].setRotationPoint(-17F, -18F, -4F);

		defaultStockModel[5].addShapeBox(0F, 0F, 0F, 4, 5, 8, 0F, 0F, -3F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, -3F, -2F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F); // Box 68
		defaultStockModel[5].setRotationPoint(-17F, -23F, -4F);

		defaultStockModel[6].addShapeBox(0F, 0F, 0F, 28, 9, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 9F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 9F, 0F); // Box 69
		defaultStockModel[6].setRotationPoint(-45F, -18F, -4F);

		defaultStockModel[7].addShapeBox(0F, 0F, 0F, 28, 2, 8, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 70
		defaultStockModel[7].setRotationPoint(-45F, -20F, -4F);

		defaultStockModel[8].addShapeBox(0F, 0F, 0F, 4, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 71
		defaultStockModel[8].setRotationPoint(-13F, -11F, -4F);

		defaultStockModel[9].addShapeBox(0F, 0F, 0F, 4, 4, 8, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, -2F, 0F, -2F, -2F, 0F, -2F, -2F, 0F, 0F, -2F); // Box 72
		defaultStockModel[9].setRotationPoint(-17F, -11F, -4F);

		defaultStockModel[10].addShapeBox(0F, 0F, 0F, 28, 2, 8, 0F, 0F, 0F, 0F, 0F, 9F, 0F, 0F, 9F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, -9F, -2F, 0F, -9F, -2F, 0F, 0F, -2F); // Box 74
		defaultStockModel[10].setRotationPoint(-45F, 0F, -4F);

		defaultStockModel[11].addShapeBox(0F, 0F, 0F, 5, 2, 8, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 75
		defaultStockModel[11].setRotationPoint(-50F, -20F, -4F);

		defaultStockModel[12].addBox(0F, 0F, 0F, 5, 18, 8, 0F); // Box 76
		defaultStockModel[12].setRotationPoint(-50F, -18F, -4F);

		defaultStockModel[13].addShapeBox(0F, 0F, 0F, 5, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 77
		defaultStockModel[13].setRotationPoint(-50F, 0F, -4F);


		ammoModel = new ModelRendererTurbo[48];
		ammoModel[0] = new ModelRendererTurbo(this, 1, 168, textureX, textureY); // Box 96
		ammoModel[1] = new ModelRendererTurbo(this, 1, 201, textureX, textureY); // Box 96
		ammoModel[2] = new ModelRendererTurbo(this, 1, 168, textureX, textureY); // Box 96
		ammoModel[3] = new ModelRendererTurbo(this, 64, 163, textureX, textureY); // Box 96
		ammoModel[4] = new ModelRendererTurbo(this, 64, 163, textureX, textureY); // Box 96
		ammoModel[5] = new ModelRendererTurbo(this, 64, 168, textureX, textureY); // Box 96
		ammoModel[6] = new ModelRendererTurbo(this, 87, 168, textureX, textureY); // Box 96
		ammoModel[7] = new ModelRendererTurbo(this, 87, 168, textureX, textureY); // Box 96
		ammoModel[8] = new ModelRendererTurbo(this, 87, 168, textureX, textureY); // Box 96
		ammoModel[9] = new ModelRendererTurbo(this, 64, 163, textureX, textureY); // Box 96
		ammoModel[10] = new ModelRendererTurbo(this, 64, 168, textureX, textureY); // Box 96
		ammoModel[11] = new ModelRendererTurbo(this, 64, 163, textureX, textureY); // Box 96
		ammoModel[12] = new ModelRendererTurbo(this, 87, 168, textureX, textureY); // Box 96
		ammoModel[13] = new ModelRendererTurbo(this, 87, 168, textureX, textureY); // Box 96
		ammoModel[14] = new ModelRendererTurbo(this, 87, 168, textureX, textureY); // Box 96
		ammoModel[15] = new ModelRendererTurbo(this, 64, 163, textureX, textureY); // Box 96
		ammoModel[16] = new ModelRendererTurbo(this, 64, 168, textureX, textureY); // Box 96
		ammoModel[17] = new ModelRendererTurbo(this, 87, 168, textureX, textureY); // Box 96
		ammoModel[18] = new ModelRendererTurbo(this, 87, 168, textureX, textureY); // Box 96
		ammoModel[19] = new ModelRendererTurbo(this, 64, 163, textureX, textureY); // Box 96
		ammoModel[20] = new ModelRendererTurbo(this, 64, 168, textureX, textureY); // Box 96
		ammoModel[21] = new ModelRendererTurbo(this, 64, 163, textureX, textureY); // Box 96
		ammoModel[22] = new ModelRendererTurbo(this, 64, 163, textureX, textureY); // Box 96
		ammoModel[23] = new ModelRendererTurbo(this, 64, 168, textureX, textureY); // Box 96
		ammoModel[24] = new ModelRendererTurbo(this, 64, 163, textureX, textureY); // Box 96
		ammoModel[25] = new ModelRendererTurbo(this, 64, 163, textureX, textureY); // Box 96
		ammoModel[26] = new ModelRendererTurbo(this, 64, 168, textureX, textureY); // Box 96
		ammoModel[27] = new ModelRendererTurbo(this, 64, 163, textureX, textureY); // Box 96
		ammoModel[28] = new ModelRendererTurbo(this, 87, 168, textureX, textureY); // Box 96
		ammoModel[29] = new ModelRendererTurbo(this, 87, 168, textureX, textureY); // Box 96
		ammoModel[30] = new ModelRendererTurbo(this, 87, 168, textureX, textureY); // Box 96
		ammoModel[31] = new ModelRendererTurbo(this, 87, 168, textureX, textureY); // Box 96
		ammoModel[32] = new ModelRendererTurbo(this, 87, 168, textureX, textureY); // Box 96
		ammoModel[33] = new ModelRendererTurbo(this, 87, 168, textureX, textureY); // Box 96
		ammoModel[34] = new ModelRendererTurbo(this, 87, 168, textureX, textureY); // Box 96
		ammoModel[35] = new ModelRendererTurbo(this, 87, 168, textureX, textureY); // Box 96
		ammoModel[36] = new ModelRendererTurbo(this, 87, 168, textureX, textureY); // Box 96
		ammoModel[37] = new ModelRendererTurbo(this, 87, 168, textureX, textureY); // Box 96
		ammoModel[38] = new ModelRendererTurbo(this, 87, 168, textureX, textureY); // Box 96
		ammoModel[39] = new ModelRendererTurbo(this, 87, 168, textureX, textureY); // Box 96
		ammoModel[40] = new ModelRendererTurbo(this, 64, 163, textureX, textureY); // Box 96
		ammoModel[41] = new ModelRendererTurbo(this, 64, 168, textureX, textureY); // Box 96
		ammoModel[42] = new ModelRendererTurbo(this, 64, 163, textureX, textureY); // Box 96
		ammoModel[43] = new ModelRendererTurbo(this, 87, 168, textureX, textureY); // Box 96
		ammoModel[44] = new ModelRendererTurbo(this, 87, 168, textureX, textureY); // Box 96
		ammoModel[45] = new ModelRendererTurbo(this, 64, 168, textureX, textureY); // Box 96
		ammoModel[46] = new ModelRendererTurbo(this, 64, 163, textureX, textureY); // Box 96
		ammoModel[47] = new ModelRendererTurbo(this, 64, 173, textureX, textureY); // Box 96

		ammoModel[0].addShapeBox(0F, 0F, 0F, 16, 2, 30, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 96
		ammoModel[0].setRotationPoint(19.5F, 11F, -10F);

		ammoModel[1].addBox(0F, 0F, 0F, 16, 16, 30, 0F); // Box 96
		ammoModel[1].setRotationPoint(19.5F, -5F, -10F);

		ammoModel[2].addShapeBox(0F, 0F, 0F, 16, 2, 30, 0F, -1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 96
		ammoModel[2].setRotationPoint(19.5F, -7F, -10F);

		ammoModel[3].addShapeBox(0F, 0F, 0F, 8, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 96
		ammoModel[3].setRotationPoint(22F, -23F, 5F);

		ammoModel[4].addShapeBox(0F, 0F, 0F, 8, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 96
		ammoModel[4].setRotationPoint(22F, -21F, 5F);

		ammoModel[5].addBox(0F, 0F, 0F, 8, 1, 3, 0F); // Box 96
		ammoModel[5].setRotationPoint(22F, -22F, 5F);

		ammoModel[6].addShapeBox(0F, 0F, 0F, 3, 1, 3, 0F, 0F, 0F, -1F, 0F, -0.75F, -1F, 0F, -0.75F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F); // Box 96
		ammoModel[6].setRotationPoint(30F, -23F, 5F);

		ammoModel[7].addShapeBox(0F, 0F, 0F, 3, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F); // Box 96
		ammoModel[7].setRotationPoint(30F, -22F, 5F);

		ammoModel[8].addShapeBox(0F, 0F, 0F, 3, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -0.75F, -1F, 0F, -0.75F, -1F, 0F, 0F, -1F); // Box 96
		ammoModel[8].setRotationPoint(30F, -21F, 5F);

		ammoModel[9].addShapeBox(0F, 0F, 0F, 8, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 96
		ammoModel[9].setRotationPoint(22F, -22.5F, 2F);

		ammoModel[10].addBox(0F, 0F, 0F, 8, 1, 3, 0F); // Box 96
		ammoModel[10].setRotationPoint(22F, -21.5F, 2F);

		ammoModel[11].addShapeBox(0F, 0F, 0F, 8, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 96
		ammoModel[11].setRotationPoint(22F, -20.5F, 2F);

		ammoModel[12].addShapeBox(0F, 0F, 0F, 3, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -0.75F, -1F, 0F, -0.75F, -1F, 0F, 0F, -1F); // Box 96
		ammoModel[12].setRotationPoint(30F, -20.5F, 2F);

		ammoModel[13].addShapeBox(0F, 0F, 0F, 3, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F); // Box 96
		ammoModel[13].setRotationPoint(30F, -21.5F, 2F);

		ammoModel[14].addShapeBox(0F, 0F, 0F, 3, 1, 3, 0F, 0F, 0F, -1F, 0F, -0.75F, -1F, 0F, -0.75F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F); // Box 96
		ammoModel[14].setRotationPoint(30F, -22.5F, 2F);

		ammoModel[15].addShapeBox(0F, 0F, 0F, 8, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 96
		ammoModel[15].setRotationPoint(22F, -21.5F, -1F);

		ammoModel[16].addBox(0F, 0F, 0F, 8, 1, 3, 0F); // Box 96
		ammoModel[16].setRotationPoint(22F, -20.5F, -1F);

		ammoModel[17].addShapeBox(0F, 0F, 0F, 3, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F); // Box 96
		ammoModel[17].setRotationPoint(30F, -20.5F, -1F);

		ammoModel[18].addShapeBox(0F, 0F, 0F, 3, 1, 3, 0F, 0F, 0F, -1F, 0F, -0.75F, -1F, 0F, -0.75F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F); // Box 96
		ammoModel[18].setRotationPoint(30F, -21.5F, -1F);

		ammoModel[19].addShapeBox(0F, 0F, 0F, 8, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 96
		ammoModel[19].setRotationPoint(22F, -18.5F, 6.5F);

		ammoModel[20].addBox(0F, 0F, 0F, 8, 1, 3, 0F); // Box 96
		ammoModel[20].setRotationPoint(22F, -19.5F, 6.5F);

		ammoModel[21].addShapeBox(0F, 0F, 0F, 8, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 96
		ammoModel[21].setRotationPoint(22F, -20.5F, 6.5F);

		ammoModel[22].addShapeBox(0F, 0F, 0F, 8, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 96
		ammoModel[22].setRotationPoint(22F, -15.5F, 6.5F);

		ammoModel[23].addBox(0F, 0F, 0F, 8, 1, 3, 0F); // Box 96
		ammoModel[23].setRotationPoint(22F, -16.5F, 6.5F);

		ammoModel[24].addShapeBox(0F, 0F, 0F, 8, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 96
		ammoModel[24].setRotationPoint(22F, -17.5F, 6.5F);

		ammoModel[25].addShapeBox(0F, 0F, 0F, 8, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 96
		ammoModel[25].setRotationPoint(22F, -13F, 8F);

		ammoModel[26].addBox(0F, 0F, 0F, 8, 1, 3, 0F); // Box 96
		ammoModel[26].setRotationPoint(22F, -14F, 8F);

		ammoModel[27].addShapeBox(0F, 0F, 0F, 8, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 96
		ammoModel[27].setRotationPoint(22F, -15F, 8F);

		ammoModel[28].addShapeBox(0F, 0F, 0F, 3, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -0.75F, -1F, 0F, -0.75F, -1F, 0F, 0F, -1F); // Box 96
		ammoModel[28].setRotationPoint(30F, -18.5F, 6.5F);

		ammoModel[29].addShapeBox(0F, 0F, 0F, 3, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F); // Box 96
		ammoModel[29].setRotationPoint(30F, -19.5F, 6.5F);

		ammoModel[30].addShapeBox(0F, 0F, 0F, 3, 1, 3, 0F, 0F, 0F, -1F, 0F, -0.75F, -1F, 0F, -0.75F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F); // Box 96
		ammoModel[30].setRotationPoint(30F, -20.5F, 6.5F);

		ammoModel[31].addShapeBox(0F, 0F, 0F, 3, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -0.75F, -1F, 0F, -0.75F, -1F, 0F, 0F, -1F); // Box 96
		ammoModel[31].setRotationPoint(30F, -15.5F, 6.5F);

		ammoModel[32].addShapeBox(0F, 0F, 0F, 3, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F); // Box 96
		ammoModel[32].setRotationPoint(30F, -16.5F, 6.5F);

		ammoModel[33].addShapeBox(0F, 0F, 0F, 3, 1, 3, 0F, 0F, 0F, -1F, 0F, -0.75F, -1F, 0F, -0.75F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F); // Box 96
		ammoModel[33].setRotationPoint(30F, -17.5F, 6.5F);

		ammoModel[34].addShapeBox(0F, 0F, 0F, 3, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -0.75F, -1F, 0F, -0.75F, -1F, 0F, 0F, -1F); // Box 96
		ammoModel[34].setRotationPoint(30F, -13F, 8F);

		ammoModel[35].addShapeBox(0F, 0F, 0F, 3, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F); // Box 96
		ammoModel[35].setRotationPoint(30F, -14F, 8F);

		ammoModel[36].addShapeBox(0F, 0F, 0F, 3, 1, 3, 0F, 0F, 0F, -1F, 0F, -0.75F, -1F, 0F, -0.75F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F); // Box 96
		ammoModel[36].setRotationPoint(30F, -15F, 8F);

		ammoModel[37].addShapeBox(0F, 0F, 0F, 3, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -0.75F, -1F, 0F, -0.75F, -1F, 0F, 0F, -1F); // Box 96
		ammoModel[37].setRotationPoint(30F, -11F, 10F);

		ammoModel[38].addShapeBox(0F, 0F, 0F, 3, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F); // Box 96
		ammoModel[38].setRotationPoint(30F, -12F, 10F);

		ammoModel[39].addShapeBox(0F, 0F, 0F, 3, 1, 3, 0F, 0F, 0F, -1F, 0F, -0.75F, -1F, 0F, -0.75F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F); // Box 96
		ammoModel[39].setRotationPoint(30F, -13F, 10F);

		ammoModel[40].addShapeBox(0F, 0F, 0F, 8, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 96
		ammoModel[40].setRotationPoint(22F, -11F, 10F);

		ammoModel[41].addBox(0F, 0F, 0F, 8, 1, 3, 0F); // Box 96
		ammoModel[41].setRotationPoint(22F, -12F, 10F);

		ammoModel[42].addShapeBox(0F, 0F, 0F, 8, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 96
		ammoModel[42].setRotationPoint(22F, -13F, 10F);

		ammoModel[43].addShapeBox(0F, 0F, 0F, 3, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F); // Box 96
		ammoModel[43].setRotationPoint(30F, -10F, 12F);

		ammoModel[44].addShapeBox(0F, 0F, 0F, 3, 1, 3, 0F, 0F, 0F, -1F, 0F, -0.75F, -1F, 0F, -0.75F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F); // Box 96
		ammoModel[44].setRotationPoint(30F, -11F, 12F);

		ammoModel[45].addBox(0F, 0F, 0F, 8, 1, 3, 0F); // Box 96
		ammoModel[45].setRotationPoint(22F, -10F, 12F);

		ammoModel[46].addShapeBox(0F, 0F, 0F, 8, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 96
		ammoModel[46].setRotationPoint(22F, -11F, 12F);

		ammoModel[47].addBox(0F, 0F, 0F, 12, 2, 4, 0F); // Box 96
		ammoModel[47].setRotationPoint(21.5F, -9F, 11.5F);


		slideModel = new ModelRendererTurbo[3];
		slideModel[0] = new ModelRendererTurbo(this, 30, 25, textureX, textureY); // Box 6
		slideModel[1] = new ModelRendererTurbo(this, 45, 17, textureX, textureY); // Box 7
		slideModel[2] = new ModelRendererTurbo(this, 45, 17, textureX, textureY); // Box 8

		slideModel[0].addShapeBox(0F, 0F, 0F, 16, 4, 1, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 6
		slideModel[0].setRotationPoint(19F, -18.5F, -3.5F);

		slideModel[1].addShapeBox(0F, 0F, 0F, 2, 1, 6, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 7
		slideModel[1].setRotationPoint(32F, -17.5F, -9.5F);

		slideModel[2].addShapeBox(0F, 0F, 0F, 2, 1, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 8
		slideModel[2].setRotationPoint(32F, -16.5F, -9.5F);


		breakActionModel = new ModelRendererTurbo[6];
		breakActionModel[0] = new ModelRendererTurbo(this, 140, 112, textureX, textureY); // Box 1
		breakActionModel[1] = new ModelRendererTurbo(this, 169, 107, textureX, textureY); // Box 2
		breakActionModel[2] = new ModelRendererTurbo(this, 206, 107, textureX, textureY); // Box 12
		breakActionModel[3] = new ModelRendererTurbo(this, 140, 98, textureX, textureY); // Box 17
		breakActionModel[4] = new ModelRendererTurbo(this, 140, 98, textureX, textureY); // Box 19
		breakActionModel[5] = new ModelRendererTurbo(this, 206, 107, textureX, textureY); // Box 81

		breakActionModel[0].addShapeBox(-17F, -5F, -6.5F, 17, 2, 9, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 1
		breakActionModel[0].setRotationPoint(36F, -20F, 2F);

		breakActionModel[1].addBox(-17F, -3F, -6.5F, 17, 3, 1, 0F); // Box 2
		breakActionModel[1].setRotationPoint(36F, -20F, 2F);

		breakActionModel[2].addBox(-17F, -3F, 3.5F, 3, 3, 1, 0F); // Box 12
		breakActionModel[2].setRotationPoint(36F, -20F, 0F);

		breakActionModel[3].addShapeBox(-1F, -1F, -6F, 2, 1, 10, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 17
		breakActionModel[3].setRotationPoint(36F, -20F, 1F);

		breakActionModel[4].addShapeBox(-1F, 0F, -6F, 2, 1, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 19
		breakActionModel[4].setRotationPoint(36F, -20F, 1F);

		breakActionModel[5].addBox(-3F, -3F, 3.5F, 3, 3, 1, 0F); // Box 81
		breakActionModel[5].setRotationPoint(36F, -20F, 0F);

		stockAttachPoint = new Vector3f(-9F /16F, 16F /16F, 0F /16F);

		gunSlideDistance = 0.8F;
		animationType = EnumAnimationType.CUSTOM;

		/* ----Start of Reload Block---- */
		/* Reload Name: Amended Side Clip */
		rotateClipHorizontal = 120F;
		rotateClipVertical = 60F;
		translateClip = new Vector3f(0.5F, 0F, 0F);
		/* ----End of Reload Block---- */

		barrelBreakPoint = new Vector3f(2F, 1.25F, 0F);
		breakAngle = 45F;

		translateAll(0F, 0F, 0F);


		flipAll();
	}
}