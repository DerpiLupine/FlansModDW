package com.flansmod.client.model.dwfirearms; //Path where the model is located

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelMFPK9 extends ModelGun //Same as Filename
{
	int textureX = 512;
	int textureY = 256;

	public ModelMFPK9() //Same as Filename
	{
		gunModel = new ModelRendererTurbo[32];
		gunModel[0] = new ModelRendererTurbo(this, 80, 49, textureX, textureY); // body1
		gunModel[1] = new ModelRendererTurbo(this, 116, 98, textureX, textureY); // body3
		gunModel[2] = new ModelRendererTurbo(this, 30, 34, textureX, textureY); // body2
		gunModel[3] = new ModelRendererTurbo(this, 1, 46, textureX, textureY); // grip1
		gunModel[4] = new ModelRendererTurbo(this, 80, 19, textureX, textureY); // mainBarrelBottom
		gunModel[5] = new ModelRendererTurbo(this, 80, 10, textureX, textureY); // mainBarrelMiddle
		gunModel[6] = new ModelRendererTurbo(this, 80, 1, textureX, textureY); // mainBarrelTop
		gunModel[7] = new ModelRendererTurbo(this, 80, 32, textureX, textureY); // pin1
		gunModel[8] = new ModelRendererTurbo(this, 80, 32, textureX, textureY); // pin1-2
		gunModel[9] = new ModelRendererTurbo(this, 80, 32, textureX, textureY); // pin1-3
		gunModel[10] = new ModelRendererTurbo(this, 45, 122, textureX, textureY); // grip2
		gunModel[11] = new ModelRendererTurbo(this, 1, 33, textureX, textureY); // Box 37
		gunModel[12] = new ModelRendererTurbo(this, 150, 9, textureX, textureY); // Box 38
		gunModel[13] = new ModelRendererTurbo(this, 150, 2, textureX, textureY); // Box 39
		gunModel[14] = new ModelRendererTurbo(this, 116, 98, textureX, textureY); // Box 57
		gunModel[15] = new ModelRendererTurbo(this, 95, 97, textureX, textureY); // Box 58
		gunModel[16] = new ModelRendererTurbo(this, 157, 98, textureX, textureY); // Box 59
		gunModel[17] = new ModelRendererTurbo(this, 116, 108, textureX, textureY); // Box 60
		gunModel[18] = new ModelRendererTurbo(this, 149, 108, textureX, textureY); // Box 61
		gunModel[19] = new ModelRendererTurbo(this, 1, 60, textureX, textureY); // Box 62
		gunModel[20] = new ModelRendererTurbo(this, 1, 60, textureX, textureY); // Box 63
		gunModel[21] = new ModelRendererTurbo(this, 66, 122, textureX, textureY); // Box 64
		gunModel[22] = new ModelRendererTurbo(this, 57, 33, textureX, textureY); // Box 65
		gunModel[23] = new ModelRendererTurbo(this, 33, 51, textureX, textureY); // Box 66
		gunModel[24] = new ModelRendererTurbo(this, 57, 33, textureX, textureY); // Box 67
		gunModel[25] = new ModelRendererTurbo(this, 57, 48, textureX, textureY); // Box 68
		gunModel[26] = new ModelRendererTurbo(this, 170, 34, textureX, textureY); // Box 69
		gunModel[27] = new ModelRendererTurbo(this, 170, 34, textureX, textureY); // Box 71
		gunModel[28] = new ModelRendererTurbo(this, 154, 33, textureX, textureY); // Box 72
		gunModel[29] = new ModelRendererTurbo(this, 165, 33, textureX, textureY); // Box 73
		gunModel[30] = new ModelRendererTurbo(this, 125, 18, textureX, textureY); // Box 75
		gunModel[31] = new ModelRendererTurbo(this, 125, 18, textureX, textureY); // Box 76

		gunModel[0].addBox(0F, 0F, 0F, 43, 6, 9, 0F); // body1
		gunModel[0].setRotationPoint(-19F, -17F, -4.5F);

		gunModel[1].addBox(0F, 0F, 0F, 12, 1, 8, 0F); // body3
		gunModel[1].setRotationPoint(24F, -17F, -4F);

		gunModel[2].addShapeBox(0F, 0F, 0F, 5, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F); // body2
		gunModel[2].setRotationPoint(-24F, -15F, -4F);

		gunModel[3].addShapeBox(0F, 0F, 0F, 11, 4, 9, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // grip1
		gunModel[3].setRotationPoint(-8F, -11F, -4.5F);

		gunModel[4].addShapeBox(0F, 0F, 0F, 16, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // mainBarrelBottom
		gunModel[4].setRotationPoint(28F, -22F, -3F);

		gunModel[5].addBox(0F, 0F, 0F, 16, 2, 6, 0F); // mainBarrelMiddle
		gunModel[5].setRotationPoint(28F, -24F, -3F);

		gunModel[6].addShapeBox(0F, 0F, 0F, 16, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // mainBarrelTop
		gunModel[6].setRotationPoint(28F, -26F, -3F);

		gunModel[7].addShapeBox(0F, 0F, 0F, 11, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // pin1
		gunModel[7].setRotationPoint(26F, -18F, -1.5F);

		gunModel[8].addBox(0F, 0F, 0F, 11, 1, 3, 0F); // pin1-2
		gunModel[8].setRotationPoint(26F, -17F, -1.5F);

		gunModel[9].addShapeBox(0F, 0F, 0F, 11, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // pin1-3
		gunModel[9].setRotationPoint(26F, -16F, -1.5F);

		gunModel[10].addBox(0F, 0F, 0F, 2, 30, 8, 0F); // grip2
		gunModel[10].setRotationPoint(2.5F, -11F, -4F);

		gunModel[11].addBox(0F, 0F, 0F, 5, 2, 9, 0F); // Box 37
		gunModel[11].setRotationPoint(-24F, -17F, -4.5F);

		gunModel[12].addBox(0F, 0F, 0F, 9, 3, 5, 0F); // Box 38
		gunModel[12].setRotationPoint(2F, -25F, -3.25F);

		gunModel[13].addShapeBox(0F, 0F, 0F, 7, 1, 5, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 39
		gunModel[13].setRotationPoint(4F, -26F, -3.25F);

		gunModel[14].addBox(0F, 0F, 0F, 12, 1, 8, 0F); // Box 57
		gunModel[14].setRotationPoint(24F, -14F, -4F);

		gunModel[15].addBox(0F, 0F, 0F, 2, 2, 8, 0F); // Box 58
		gunModel[15].setRotationPoint(24F, -16F, -4F);

		gunModel[16].addBox(0F, 0F, 0F, 10, 2, 7, 0F); // Box 59
		gunModel[16].setRotationPoint(26F, -16F, -3.5F);

		gunModel[17].addShapeBox(0F, 0F, 0F, 8, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -1F, 0F, -0.5F, -1F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 60
		gunModel[17].setRotationPoint(24F, -13F, -4F);

		gunModel[18].addShapeBox(0F, 0F, 0F, 4, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -1F, 0F, -0.5F); // Box 61
		gunModel[18].setRotationPoint(32F, -13F, -4F);

		gunModel[19].addBox(0F, 0F, 0F, 11, 5, 9, 0F); // Box 62
		gunModel[19].setRotationPoint(-8F, -7F, -4.5F);

		gunModel[20].addShapeBox(0F, 0F, 0F, 11, 5, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F); // Box 63
		gunModel[20].setRotationPoint(-8F, -2F, -4.5F);

		gunModel[21].addBox(0F, 0F, 0F, 13, 16, 9, 0F); // Box 64
		gunModel[21].setRotationPoint(-10F, 3F, -4.5F);

		gunModel[22].addShapeBox(0F, 0F, 0F, 1, 5, 9, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 65
		gunModel[22].setRotationPoint(-9F, -7F, -4.5F);

		gunModel[23].addShapeBox(0F, 0F, 0F, 1, 4, 9, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, -2F, 0F, -1F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, -1F); // Box 66
		gunModel[23].setRotationPoint(-11F, -11F, -4.5F);

		gunModel[24].addShapeBox(0F, 0F, 0F, 1, 5, 9, 0F, -2F, 0F, -1F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 67
		gunModel[24].setRotationPoint(-11F, -2F, -4.5F);

		gunModel[25].addShapeBox(0F, 0F, 0F, 1, 16, 9, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 68
		gunModel[25].setRotationPoint(-11F, 3F, -4.5F);

		gunModel[26].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 69
		gunModel[26].setRotationPoint(-9F, -15F, 4.5F);

		gunModel[27].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 71
		gunModel[27].setRotationPoint(-9F, -14F, 4.5F);

		gunModel[28].addBox(0F, 0F, 0F, 4, 2, 1, 0F); // Box 72
		gunModel[28].setRotationPoint(-12F, -15F, 4F);

		gunModel[29].addShapeBox(0F, 0F, 0F, 1, 2, 1, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F); // Box 73
		gunModel[29].setRotationPoint(-13F, -15F, 4F);

		gunModel[30].addShapeBox(0F, 0F, 0F, 4, 2, 10, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 75
		gunModel[30].setRotationPoint(8F, -16F, -5F);

		gunModel[31].addShapeBox(0F, 0F, 0F, 4, 2, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 76
		gunModel[31].setRotationPoint(8F, -14F, -5F);


		ammoModel = new ModelRendererTurbo[5];
		ammoModel[0] = new ModelRendererTurbo(this, 1, 122, textureX, textureY); // bullet
		ammoModel[1] = new ModelRendererTurbo(this, 20, 122, textureX, textureY); // bulletTip
		ammoModel[2] = new ModelRendererTurbo(this, 1, 127, textureX, textureY); // clip1
		ammoModel[3] = new ModelRendererTurbo(this, 30, 128, textureX, textureY); // clip2
		ammoModel[4] = new ModelRendererTurbo(this, 1, 158, textureX, textureY); // clip3

		ammoModel[0].addShapeBox(0F, 0F, 0F, 6, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // bullet
		ammoModel[0].setRotationPoint(-5.5F, -5F, -1.5F);

		ammoModel[1].addShapeBox(0F, 0F, 0F, 2, 1, 3, 0F, 0F, 0F, -1F, 0F, -0.5F, -1F, 0F, -0.5F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F); // bulletTip
		ammoModel[1].setRotationPoint(0.5F, -5F, -1.5F);

		ammoModel[2].addBox(0F, 0F, 0F, 8, 24, 6, 0F); // clip1
		ammoModel[2].setRotationPoint(-6.5F, -4F, -3F);

		ammoModel[3].addBox(0F, 0F, 0F, 2, 24, 5, 0F); // clip2
		ammoModel[3].setRotationPoint(1.5F, -4F, -2.5F);

		ammoModel[4].addBox(0F, 0F, 0F, 11, 2, 7, 0F); // clip3
		ammoModel[4].setRotationPoint(-7F, 20F, -3.5F);


		slideModel = new ModelRendererTurbo[49];
		slideModel[0] = new ModelRendererTurbo(this, 1, 9, textureX, textureY); // ironSight3-2
		slideModel[1] = new ModelRendererTurbo(this, 1, 9, textureX, textureY); // ironSight3
		slideModel[2] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // ironSight2
		slideModel[3] = new ModelRendererTurbo(this, 14, 10, textureX, textureY); // ironSight1
		slideModel[4] = new ModelRendererTurbo(this, 1, 107, textureX, textureY); // slide1
		slideModel[5] = new ModelRendererTurbo(this, 1, 19, textureX, textureY); // slide2
		slideModel[6] = new ModelRendererTurbo(this, 130, 89, textureX, textureY); // slide4-2
		slideModel[7] = new ModelRendererTurbo(this, 47, 112, textureX, textureY); // slide5
		slideModel[8] = new ModelRendererTurbo(this, 1, 82, textureX, textureY); // slide6
		slideModel[9] = new ModelRendererTurbo(this, 1, 92, textureX, textureY); // Box 0
		slideModel[10] = new ModelRendererTurbo(this, 20, 93, textureX, textureY); // Box 1
		slideModel[11] = new ModelRendererTurbo(this, 24, 112, textureX, textureY); // Box 2
		slideModel[12] = new ModelRendererTurbo(this, 80, 37, textureX, textureY); // Box 3
		slideModel[13] = new ModelRendererTurbo(this, 80, 37, textureX, textureY); // Box 4
		slideModel[14] = new ModelRendererTurbo(this, 80, 65, textureX, textureY); // Box 5
		slideModel[15] = new ModelRendererTurbo(this, 54, 97, textureX, textureY); // Box 9
		slideModel[16] = new ModelRendererTurbo(this, 37, 98, textureX, textureY); // Box 10
		slideModel[17] = new ModelRendererTurbo(this, 54, 97, textureX, textureY); // Box 13
		slideModel[18] = new ModelRendererTurbo(this, 1, 92, textureX, textureY); // Box 14
		slideModel[19] = new ModelRendererTurbo(this, 54, 97, textureX, textureY); // Box 17
		slideModel[20] = new ModelRendererTurbo(this, 1, 92, textureX, textureY); // Box 18
		slideModel[21] = new ModelRendererTurbo(this, 54, 97, textureX, textureY); // Box 21
		slideModel[22] = new ModelRendererTurbo(this, 1, 92, textureX, textureY); // Box 22
		slideModel[23] = new ModelRendererTurbo(this, 54, 97, textureX, textureY); // Box 23
		slideModel[24] = new ModelRendererTurbo(this, 1, 92, textureX, textureY); // Box 24
		slideModel[25] = new ModelRendererTurbo(this, 54, 97, textureX, textureY); // Box 27
		slideModel[26] = new ModelRendererTurbo(this, 1, 92, textureX, textureY); // Box 28
		slideModel[27] = new ModelRendererTurbo(this, 80, 76, textureX, textureY); // Box 31
		slideModel[28] = new ModelRendererTurbo(this, 1, 92, textureX, textureY); // Box 34
		slideModel[29] = new ModelRendererTurbo(this, 54, 97, textureX, textureY); // Box 35
		slideModel[30] = new ModelRendererTurbo(this, 80, 86, textureX, textureY); // Box 36
		slideModel[31] = new ModelRendererTurbo(this, 93, 88, textureX, textureY); // Box 40
		slideModel[32] = new ModelRendererTurbo(this, 125, 9, textureX, textureY); // Box 41
		slideModel[33] = new ModelRendererTurbo(this, 125, 2, textureX, textureY); // Box 42
		slideModel[34] = new ModelRendererTurbo(this, 80, 97, textureX, textureY); // Box 43
		slideModel[35] = new ModelRendererTurbo(this, 20, 93, textureX, textureY); // Box 44
		slideModel[36] = new ModelRendererTurbo(this, 37, 98, textureX, textureY); // Box 45
		slideModel[37] = new ModelRendererTurbo(this, 20, 93, textureX, textureY); // Box 46
		slideModel[38] = new ModelRendererTurbo(this, 37, 98, textureX, textureY); // Box 47
		slideModel[39] = new ModelRendererTurbo(this, 37, 98, textureX, textureY); // Box 48
		slideModel[40] = new ModelRendererTurbo(this, 20, 93, textureX, textureY); // Box 49
		slideModel[41] = new ModelRendererTurbo(this, 20, 93, textureX, textureY); // Box 50
		slideModel[42] = new ModelRendererTurbo(this, 37, 98, textureX, textureY); // Box 51
		slideModel[43] = new ModelRendererTurbo(this, 37, 98, textureX, textureY); // Box 52
		slideModel[44] = new ModelRendererTurbo(this, 20, 93, textureX, textureY); // Box 53
		slideModel[45] = new ModelRendererTurbo(this, 37, 98, textureX, textureY); // Box 54
		slideModel[46] = new ModelRendererTurbo(this, 20, 93, textureX, textureY); // Box 55
		slideModel[47] = new ModelRendererTurbo(this, 109, 31, textureX, textureY); // Box 56
		slideModel[48] = new ModelRendererTurbo(this, 1, 168, textureX, textureY); // Box 0

		slideModel[0].addBox(0F, 0F, 0F, 4, 2, 2, 0F); // ironSight3-2
		slideModel[0].setRotationPoint(-22F, -31F, -3F);

		slideModel[1].addBox(0F, 0F, 0F, 4, 2, 2, 0F); // ironSight3
		slideModel[1].setRotationPoint(-22F, -31F, 1F);

		slideModel[2].addBox(0F, 0F, 0F, 6, 1, 6, 0F); // ironSight2
		slideModel[2].setRotationPoint(-23F, -29F, -3F);

		slideModel[3].addShapeBox(0F, 0F, 0F, 4, 2, 2, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // ironSight1
		slideModel[3].setRotationPoint(31F, -29F, -1F);

		slideModel[4].addBox(0F, 0F, 0F, 3, 6, 8, 0F); // slide1
		slideModel[4].setRotationPoint(-24F, -27F, -4F);

		slideModel[5].addBox(0F, 0F, 0F, 25, 5, 8, 0F); // slide2
		slideModel[5].setRotationPoint(11F, -26F, -4F);

		slideModel[6].addShapeBox(0F, 0F, 0F, 16, 6, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // slide4-2
		slideModel[6].setRotationPoint(-5F, -27F, 3F);

		slideModel[7].addShapeBox(0F, 0F, 0F, 18, 1, 8, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F); // slide5
		slideModel[7].setRotationPoint(-24F, -28F, -4F);

		slideModel[8].addShapeBox(0F, 0F, 0F, 25, 1, 8, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // slide6
		slideModel[8].setRotationPoint(11F, -27F, -4F);

		slideModel[9].addBox(0F, 0F, 0F, 1, 6, 8, 0F); // Box 0
		slideModel[9].setRotationPoint(-20F, -27F, -4F);

		slideModel[10].addShapeBox(0F, 0F, 0F, 1, 6, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // Box 1
		slideModel[10].setRotationPoint(-21F, -27F, -3.75F);

		slideModel[11].addShapeBox(0F, 0F, 0F, 3, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F); // Box 2
		slideModel[11].setRotationPoint(-24F, -21F, -4F);

		slideModel[12].addShapeBox(0F, 0F, 0F, 60, 1, 10, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 3
		slideModel[12].setRotationPoint(-24F, -18F, -5F);

		slideModel[13].addBox(0F, 0F, 0F, 60, 1, 10, 0F); // Box 4
		slideModel[13].setRotationPoint(-24F, -20F, -5F);

		slideModel[14].addBox(0F, 0F, 0F, 60, 1, 9, 0F); // Box 5
		slideModel[14].setRotationPoint(-24F, -19F, -4.5F);

		slideModel[15].addShapeBox(0F, 0F, 0F, 1, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F); // Box 9
		slideModel[15].setRotationPoint(-20F, -21F, -4F);

		slideModel[16].addShapeBox(0F, 0F, 0F, 1, 1, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1.5F, 0F, 0F, 1.5F); // Box 10
		slideModel[16].setRotationPoint(-21F, -21F, -3.75F);

		slideModel[17].addShapeBox(0F, 0F, 0F, 1, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F); // Box 13
		slideModel[17].setRotationPoint(-18F, -21F, -4F);

		slideModel[18].addBox(0F, 0F, 0F, 1, 6, 8, 0F); // Box 14
		slideModel[18].setRotationPoint(-18F, -27F, -4F);

		slideModel[19].addShapeBox(0F, 0F, 0F, 1, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F); // Box 17
		slideModel[19].setRotationPoint(-16F, -21F, -4F);

		slideModel[20].addBox(0F, 0F, 0F, 1, 6, 8, 0F); // Box 18
		slideModel[20].setRotationPoint(-16F, -27F, -4F);

		slideModel[21].addShapeBox(0F, 0F, 0F, 1, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F); // Box 21
		slideModel[21].setRotationPoint(-10F, -21F, -4F);

		slideModel[22].addBox(0F, 0F, 0F, 1, 6, 8, 0F); // Box 22
		slideModel[22].setRotationPoint(-10F, -27F, -4F);

		slideModel[23].addShapeBox(0F, 0F, 0F, 1, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F); // Box 23
		slideModel[23].setRotationPoint(-12F, -21F, -4F);

		slideModel[24].addBox(0F, 0F, 0F, 1, 6, 8, 0F); // Box 24
		slideModel[24].setRotationPoint(-12F, -27F, -4F);

		slideModel[25].addShapeBox(0F, 0F, 0F, 1, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F); // Box 27
		slideModel[25].setRotationPoint(-14F, -21F, -4F);

		slideModel[26].addBox(0F, 0F, 0F, 1, 6, 8, 0F); // Box 28
		slideModel[26].setRotationPoint(-14F, -27F, -4F);

		slideModel[27].addShapeBox(0F, 0F, 0F, 43, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F); // Box 31
		slideModel[27].setRotationPoint(-7F, -21F, -4F);

		slideModel[28].addBox(0F, 0F, 0F, 1, 6, 8, 0F); // Box 34
		slideModel[28].setRotationPoint(-8F, -27F, -4F);

		slideModel[29].addShapeBox(0F, 0F, 0F, 1, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F); // Box 35
		slideModel[29].setRotationPoint(-8F, -21F, -4F);

		slideModel[30].addBox(0F, 0F, 0F, 2, 6, 4, 0F); // Box 36
		slideModel[30].setRotationPoint(-7F, -27F, 0F);

		slideModel[31].addBox(0F, 0F, 0F, 16, 6, 2, 0F); // Box 40
		slideModel[31].setRotationPoint(-5F, -27F, 1F);

		slideModel[32].addBox(0F, 0F, 0F, 7, 3, 5, 0F); // Box 41
		slideModel[32].setRotationPoint(-5F, -25F, -4F);

		slideModel[33].addShapeBox(0F, 0F, 0F, 7, 1, 5, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 42
		slideModel[33].setRotationPoint(-5F, -26F, -4F);

		slideModel[34].addShapeBox(0F, 0F, 0F, 2, 5, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F); // Box 43
		slideModel[34].setRotationPoint(-7F, -27F, -4F);

		slideModel[35].addShapeBox(0F, 0F, 0F, 1, 6, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // Box 44
		slideModel[35].setRotationPoint(-19F, -27F, -3.75F);

		slideModel[36].addShapeBox(0F, 0F, 0F, 1, 1, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1.5F, 0F, 0F, 1.5F); // Box 45
		slideModel[36].setRotationPoint(-19F, -21F, -3.75F);

		slideModel[37].addShapeBox(0F, 0F, 0F, 1, 6, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // Box 46
		slideModel[37].setRotationPoint(-15F, -27F, -3.75F);

		slideModel[38].addShapeBox(0F, 0F, 0F, 1, 1, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1.5F, 0F, 0F, 1.5F); // Box 47
		slideModel[38].setRotationPoint(-15F, -21F, -3.75F);

		slideModel[39].addShapeBox(0F, 0F, 0F, 1, 1, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1.5F, 0F, 0F, 1.5F); // Box 48
		slideModel[39].setRotationPoint(-17F, -21F, -3.75F);

		slideModel[40].addShapeBox(0F, 0F, 0F, 1, 6, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // Box 49
		slideModel[40].setRotationPoint(-17F, -27F, -3.75F);

		slideModel[41].addShapeBox(0F, 0F, 0F, 1, 6, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // Box 50
		slideModel[41].setRotationPoint(-9F, -27F, -3.75F);

		slideModel[42].addShapeBox(0F, 0F, 0F, 1, 1, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1.5F, 0F, 0F, 1.5F); // Box 51
		slideModel[42].setRotationPoint(-9F, -21F, -3.75F);

		slideModel[43].addShapeBox(0F, 0F, 0F, 1, 1, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1.5F, 0F, 0F, 1.5F); // Box 52
		slideModel[43].setRotationPoint(-11F, -21F, -3.75F);

		slideModel[44].addShapeBox(0F, 0F, 0F, 1, 6, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // Box 53
		slideModel[44].setRotationPoint(-11F, -27F, -3.75F);

		slideModel[45].addShapeBox(0F, 0F, 0F, 1, 1, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1.5F, 0F, 0F, 1.5F); // Box 54
		slideModel[45].setRotationPoint(-13F, -21F, -3.75F);

		slideModel[46].addShapeBox(0F, 0F, 0F, 1, 6, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // Box 55
		slideModel[46].setRotationPoint(-13F, -27F, -3.75F);

		slideModel[47].addBox(0F, 0F, 0F, 18, 1, 4, 0F); // Box 56
		slideModel[47].setRotationPoint(-7F, -22F, -4F);

		slideModel[48].addShapeBox(0F, 0F, 0F, 60, 8, 1, 0F, 0F, 0F, 0F, -30F, 0F, 0F, -30F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, -30F, -4F, 0F, -30F, -4F, 0F, 0F, -4F, 0F); // Box 0
		slideModel[48].setRotationPoint(0F, -25.5F, 3.25F);

		barrelAttachPoint = new Vector3f(44F /16F, 23F /16F, 0F /16F);
		gripAttachPoint = new Vector3f(31 /16F, 15F /16F, 0F /16F);

		gunSlideDistance = 0.5F;
		animationType = EnumAnimationType.PISTOL_CLIP;

		translateAll(0F, 0F, 0F);


		flipAll();
	}
}