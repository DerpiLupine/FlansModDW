package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.ModelAttachment;
import com.flansmod.client.tmt.ModelRendererTurbo;

import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelABTHeatsinkFrontrail extends ModelAttachment
{
	int textureX = 512;
	int textureY = 256;

	public ModelABTHeatsinkFrontrail()
	{
		attachmentModel = new ModelRendererTurbo[12];
		attachmentModel[0] = new ModelRendererTurbo(this, 1, 14, textureX, textureY); // topRail2
		attachmentModel[1] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // topRail
		attachmentModel[2] = new ModelRendererTurbo(this, 78, 57, textureX, textureY); // Box 2
		attachmentModel[3] = new ModelRendererTurbo(this, 78, 44, textureX, textureY); // Box 3
		attachmentModel[4] = new ModelRendererTurbo(this, 1, 44, textureX, textureY); // bodyRail3
		attachmentModel[5] = new ModelRendererTurbo(this, 1, 27, textureX, textureY); // bodyRail1
		attachmentModel[6] = new ModelRendererTurbo(this, 78, 31, textureX, textureY); // bodyRail2
		attachmentModel[7] = new ModelRendererTurbo(this, 78, 18, textureX, textureY); // bodyRail3
		attachmentModel[8] = new ModelRendererTurbo(this, 78, 31, textureX, textureY); // bodyRail2
		attachmentModel[9] = new ModelRendererTurbo(this, 78, 31, textureX, textureY); // bodyRail2
		attachmentModel[10] = new ModelRendererTurbo(this, 1, 57, textureX, textureY); // woodenGrip1
		attachmentModel[11] = new ModelRendererTurbo(this, 1, 72, textureX, textureY); // woodenGrip2

		attachmentModel[0].addBox(0F, -5F, -5F, 28, 2, 10, 0F); // topRail2
		attachmentModel[0].setRotationPoint(0F, 0F, 0F);

		attachmentModel[1].addShapeBox(0F, -7F, -5F, 26, 2, 10, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // topRail
		attachmentModel[1].setRotationPoint(0F, 0F, 0F);

		attachmentModel[2].addBox(26F, -6F, -5F, 2, 1, 10, 0F); // Box 2
		attachmentModel[2].setRotationPoint(0F, 0F, 0F);

		attachmentModel[3].addShapeBox(26F, -8F, -5F, 2, 2, 10, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 3
		attachmentModel[3].setRotationPoint(0F, 0F, 0F);

		attachmentModel[4].addShapeBox(0F, 5F, -5F, 28, 2, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // bodyRail3
		attachmentModel[4].setRotationPoint(0F, 0F, 0F);

		attachmentModel[5].addBox(0F, -1F, -5F, 28, 6, 10, 0F); // bodyRail1
		attachmentModel[5].setRotationPoint(0F, 0F, 0F);

		attachmentModel[6].addBox(0F, -3F, -5F, 4, 2, 10, 0F); // bodyRail2
		attachmentModel[6].setRotationPoint(0F, 0F, 0F);

		attachmentModel[7].addBox(24F, -3F, -5F, 4, 2, 10, 0F); // bodyRail3
		attachmentModel[7].setRotationPoint(0F, 0F, 0F);

		attachmentModel[8].addBox(8F, -3F, -5F, 4, 2, 10, 0F); // bodyRail2
		attachmentModel[8].setRotationPoint(0F, 0F, 0F);

		attachmentModel[9].addBox(16F, -3F, -5F, 4, 2, 10, 0F); // bodyRail2
		attachmentModel[9].setRotationPoint(0F, 0F, 0F);

		attachmentModel[10].addShapeBox(0.5F, 2.5F, -5.5F, 27, 3, 11, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // woodenGrip1
		attachmentModel[10].setRotationPoint(0F, 0F, 0F);

		attachmentModel[11].addShapeBox(0.5F, 5.5F, -5.5F, 27, 2, 11, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // woodenGrip2
		attachmentModel[11].setRotationPoint(0F, 0F, 0F);

		renderOffset = 0F;

		//Based off the Ambircon SteamRifle positioning.

		flipAll();
	}
}

