package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.ModelAttachment;
import com.flansmod.client.tmt.ModelRendererTurbo;

import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelLPMSilverspeakScope extends ModelAttachment 
{
	int textureX = 512;
	int textureY = 256;

	public ModelLPMSilverspeakScope()
	{	
		attachmentModel = new ModelRendererTurbo[51];
		attachmentModel[0] = new ModelRendererTurbo(this, 105, 78, textureX, textureY); // mounter
		attachmentModel[1] = new ModelRendererTurbo(this, 105, 91, textureX, textureY); // mounterBase2
		attachmentModel[2] = new ModelRendererTurbo(this, 139, 136, textureX, textureY); // scope4
		attachmentModel[3] = new ModelRendererTurbo(this, 180, 117, textureX, textureY); // scope1
		attachmentModel[4] = new ModelRendererTurbo(this, 167, 117, textureX, textureY); // scope2
		attachmentModel[5] = new ModelRendererTurbo(this, 139, 136, textureX, textureY); // Box 13
		attachmentModel[6] = new ModelRendererTurbo(this, 191, 118, textureX, textureY); // Box 18
		attachmentModel[7] = new ModelRendererTurbo(this, 191, 118, textureX, textureY); // Box 19
		attachmentModel[8] = new ModelRendererTurbo(this, 180, 117, textureX, textureY); // Box 7
		attachmentModel[9] = new ModelRendererTurbo(this, 104, 136, textureX, textureY); // Box 16
		attachmentModel[10] = new ModelRendererTurbo(this, 104, 124, textureX, textureY); // Box 19
		attachmentModel[11] = new ModelRendererTurbo(this, 104, 136, textureX, textureY); // Box 20
		attachmentModel[12] = new ModelRendererTurbo(this, 180, 109, textureX, textureY); // Box 38
		attachmentModel[13] = new ModelRendererTurbo(this, 180, 109, textureX, textureY); // Box 39
		attachmentModel[14] = new ModelRendererTurbo(this, 187, 106, textureX, textureY); // Box 40
		attachmentModel[15] = new ModelRendererTurbo(this, 164, 106, textureX, textureY); // Box 41
		attachmentModel[16] = new ModelRendererTurbo(this, 164, 106, textureX, textureY); // Box 42
		attachmentModel[17] = new ModelRendererTurbo(this, 187, 106, textureX, textureY); // Box 43
		attachmentModel[18] = new ModelRendererTurbo(this, 209, 78, textureX, textureY); // Box 0
		attachmentModel[19] = new ModelRendererTurbo(this, 209, 78, textureX, textureY); // Box 1
		attachmentModel[20] = new ModelRendererTurbo(this, 209, 78, textureX, textureY); // Box 2
		attachmentModel[21] = new ModelRendererTurbo(this, 164, 77, textureX, textureY); // Box 3
		attachmentModel[22] = new ModelRendererTurbo(this, 164, 91, textureX, textureY); // Box 4
		attachmentModel[23] = new ModelRendererTurbo(this, 164, 77, textureX, textureY); // Box 5
		attachmentModel[24] = new ModelRendererTurbo(this, 104, 124, textureX, textureY); // Box 6
		attachmentModel[25] = new ModelRendererTurbo(this, 139, 124, textureX, textureY); // Box 7
		attachmentModel[26] = new ModelRendererTurbo(this, 139, 124, textureX, textureY); // Box 8
		attachmentModel[27] = new ModelRendererTurbo(this, 209, 93, textureX, textureY); // Box 9
		attachmentModel[28] = new ModelRendererTurbo(this, 209, 93, textureX, textureY); // Box 10
		attachmentModel[29] = new ModelRendererTurbo(this, 209, 93, textureX, textureY); // Box 11
		attachmentModel[30] = new ModelRendererTurbo(this, 105, 114, textureX, textureY); // Box 12
		attachmentModel[31] = new ModelRendererTurbo(this, 105, 103, textureX, textureY); // Box 13
		attachmentModel[32] = new ModelRendererTurbo(this, 105, 114, textureX, textureY); // Box 15
		attachmentModel[33] = new ModelRendererTurbo(this, 138, 108, textureX, textureY); // Box 16
		attachmentModel[34] = new ModelRendererTurbo(this, 167, 132, textureX, textureY); // Box 17
		attachmentModel[35] = new ModelRendererTurbo(this, 167, 125, textureX, textureY); // Box 18
		attachmentModel[36] = new ModelRendererTurbo(this, 167, 125, textureX, textureY); // Box 19
		attachmentModel[37] = new ModelRendererTurbo(this, 198, 124, textureX, textureY); // Box 20
		attachmentModel[38] = new ModelRendererTurbo(this, 198, 131, textureX, textureY); // Box 21
		attachmentModel[39] = new ModelRendererTurbo(this, 198, 124, textureX, textureY); // Box 23
		attachmentModel[40] = new ModelRendererTurbo(this, 217, 124, textureX, textureY); // Box 24
		attachmentModel[41] = new ModelRendererTurbo(this, 217, 131, textureX, textureY); // Box 25
		attachmentModel[42] = new ModelRendererTurbo(this, 217, 124, textureX, textureY); // Box 26
		attachmentModel[43] = new ModelRendererTurbo(this, 204, 108, textureX, textureY); // Box 27
		attachmentModel[44] = new ModelRendererTurbo(this, 204, 108, textureX, textureY); // Box 28
		attachmentModel[45] = new ModelRendererTurbo(this, 234, 107, textureX, textureY); // Box 29
		attachmentModel[46] = new ModelRendererTurbo(this, 221, 109, textureX, textureY); // Box 30
		attachmentModel[47] = new ModelRendererTurbo(this, 221, 118, textureX, textureY); // Box 32
		attachmentModel[48] = new ModelRendererTurbo(this, 234, 118, textureX, textureY); // Box 33
		attachmentModel[49] = new ModelRendererTurbo(this, 206, 117, textureX, textureY); // Box 34
		attachmentModel[50] = new ModelRendererTurbo(this, 197, 107, textureX, textureY); // Box 35

		attachmentModel[0].addBox(0F, 0F, 0F, 20, 3, 9, 0F); // mounter
		attachmentModel[0].setRotationPoint(-10F, -3F, -4.5F);

		attachmentModel[1].addBox(0F, 0F, 0F, 16, 2, 9, 0F); // mounterBase2
		attachmentModel[1].setRotationPoint(-7F, -5F, -4.5F);

		attachmentModel[2].addShapeBox(0F, 0F, 0F, 5, 1, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope4
		attachmentModel[2].setRotationPoint(-24F, -15F, -3.5F);

		attachmentModel[3].addShapeBox(0F, 0F, 0F, 1, 2, 4, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // scope1
		attachmentModel[3].setRotationPoint(-1F, -17F, -2F);

		attachmentModel[4].addBox(0F, 0F, 0F, 2, 2, 4, 0F); // scope2
		attachmentModel[4].setRotationPoint(0F, -17F, -2F);

		attachmentModel[5].addShapeBox(0F, 0F, 0F, 5, 1, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 13
		attachmentModel[5].setRotationPoint(-24F, -6F, -3.5F);

		attachmentModel[6].addShapeBox(0F, 0F, 0F, 4, 2, 3, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 18
		attachmentModel[6].setRotationPoint(-1F, -12F, -8F);

		attachmentModel[7].addShapeBox(0F, 0F, 0F, 4, 2, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 19
		attachmentModel[7].setRotationPoint(-1F, -10F, -8F);

		attachmentModel[8].addShapeBox(0F, 0F, 0F, 1, 2, 4, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 7
		attachmentModel[8].setRotationPoint(2F, -17F, -2F);

		attachmentModel[9].addShapeBox(0F, 0F, 0F, 10, 1, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 16
		attachmentModel[9].setRotationPoint(17F, -6F, -3.5F);

		attachmentModel[10].addShapeBox(0F, 0F, 0F, 10, 4, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 2F); // Box 19
		attachmentModel[10].setRotationPoint(17F, -14F, -3.5F);

		attachmentModel[11].addShapeBox(0F, 0F, 0F, 10, 1, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 20
		attachmentModel[11].setRotationPoint(17F, -15F, -3.5F);

		attachmentModel[12].addShapeBox(0F, 0F, 0F, 2, 2, 1, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 38
		attachmentModel[12].setRotationPoint(25F, -17F, -2F);

		attachmentModel[13].addShapeBox(0F, 0F, 0F, 2, 2, 1, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 39
		attachmentModel[13].setRotationPoint(25F, -17F, 1F);

		attachmentModel[14].addShapeBox(0F, 0F, 0F, 1, 1, 7, 0F, 0F, 0.5F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0.5F, 0F, 0F, -0.5F, 0F, -0.25F, -0.25F, -1F, -0.25F, -0.25F, -1F, 0F, -0.5F, 0F); // Box 40
		attachmentModel[14].setRotationPoint(25.5F, -16.5F, -3.5F);

		attachmentModel[15].addShapeBox(0F, 0F, 0F, 4, 1, 7, 0F, 0F, 2F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 2F, 0F, -2F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 2F); // Box 41
		attachmentModel[15].setRotationPoint(21.5F, -17F, -3.5F);

		attachmentModel[16].addShapeBox(0F, 0F, 0F, 4, 1, 7, 0F, 0F, 2F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, -2F, 0F); // Box 42
		attachmentModel[16].setRotationPoint(17.5F, -19F, -3.5F);

		attachmentModel[17].addShapeBox(0F, 0F, 0F, 1, 1, 7, 0F, -0.25F, 0.25F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, -0.25F, 0.25F, -1F, 0F, -0.5F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -1F); // Box 43
		attachmentModel[17].setRotationPoint(16.5F, -21F, -3.5F);

		attachmentModel[18].addShapeBox(0F, 0F, 0F, 10, 3, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 0
		attachmentModel[18].setRotationPoint(7F, -8.5F, -4.5F);

		attachmentModel[19].addBox(0F, 0F, 0F, 10, 3, 9, 0F); // Box 1
		attachmentModel[19].setRotationPoint(7F, -11.5F, -4.5F);

		attachmentModel[20].addShapeBox(0F, 0F, 0F, 10, 3, 9, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 2
		attachmentModel[20].setRotationPoint(7F, -14.5F, -4.5F);

		attachmentModel[21].addShapeBox(0F, 0F, 0F, 12, 3, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 3
		attachmentModel[21].setRotationPoint(-5F, -8F, -5F);

		attachmentModel[22].addBox(0F, 0F, 0F, 12, 4, 10, 0F); // Box 4
		attachmentModel[22].setRotationPoint(-5F, -12F, -5F);

		attachmentModel[23].addShapeBox(0F, 0F, 0F, 12, 3, 10, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 5
		attachmentModel[23].setRotationPoint(-5F, -15F, -5F);

		attachmentModel[24].addShapeBox(0F, 0F, 0F, 10, 4, 7, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 6
		attachmentModel[24].setRotationPoint(17F, -10F, -3.5F);

		attachmentModel[25].addShapeBox(0F, 0F, 0F, 5, 4, 7, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 7
		attachmentModel[25].setRotationPoint(-24F, -10F, -3.5F);

		attachmentModel[26].addShapeBox(0F, 0F, 0F, 5, 4, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 2F); // Box 8
		attachmentModel[26].setRotationPoint(-24F, -14F, -3.5F);

		attachmentModel[27].addShapeBox(0F, 0F, 0F, 5, 3, 9, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 9
		attachmentModel[27].setRotationPoint(-19F, -14.5F, -4.5F);

		attachmentModel[28].addBox(0F, 0F, 0F, 5, 3, 9, 0F); // Box 10
		attachmentModel[28].setRotationPoint(-19F, -11.5F, -4.5F);

		attachmentModel[29].addShapeBox(0F, 0F, 0F, 5, 3, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 11
		attachmentModel[29].setRotationPoint(-19F, -8.5F, -4.5F);

		attachmentModel[30].addShapeBox(0F, 0F, 0F, 9, 2, 7, 0F, 0F, 1F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 1F, -2F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F); // Box 12
		attachmentModel[30].setRotationPoint(-14F, -13.5F, -3.5F);

		attachmentModel[31].addShapeBox(0F, 0F, 0F, 9, 3, 7, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F); // Box 13
		attachmentModel[31].setRotationPoint(-14F, -11.5F, -3.5F);

		attachmentModel[32].addShapeBox(0F, 0F, 0F, 9, 2, 7, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 1F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 1F, -2F); // Box 15
		attachmentModel[32].setRotationPoint(-14F, -8.5F, -3.5F);

		attachmentModel[33].addBox(0F, 0F, 0F, 4, 5, 10, 0F); // Box 16
		attachmentModel[33].setRotationPoint(-1F, -8F, -5F);

		attachmentModel[34].addBox(0F, 0F, 0F, 11, 2, 4, 0F); // Box 17
		attachmentModel[34].setRotationPoint(-12.5F, -2F, 5.5F);

		attachmentModel[35].addShapeBox(0F, 0F, 0F, 11, 1, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 18
		attachmentModel[35].setRotationPoint(-12.5F, -3F, 5.5F);

		attachmentModel[36].addShapeBox(0F, 0F, 0F, 11, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 19
		attachmentModel[36].setRotationPoint(-12.5F, 0F, 5.5F);

		attachmentModel[37].addShapeBox(0F, 0F, 0F, 4, 1, 5, 0F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 20
		attachmentModel[37].setRotationPoint(-10.5F, -3F, 5F);

		attachmentModel[38].addBox(0F, 0F, 0F, 4, 2, 5, 0F); // Box 21
		attachmentModel[38].setRotationPoint(-10.5F, -2F, 5F);

		attachmentModel[39].addShapeBox(0F, 0F, 0F, 4, 1, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F); // Box 23
		attachmentModel[39].setRotationPoint(-10.5F, 0F, 5F);

		attachmentModel[40].addShapeBox(0F, 0F, 0F, 3, 1, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F); // Box 24
		attachmentModel[40].setRotationPoint(-15.5F, 0F, 5F);

		attachmentModel[41].addBox(0F, 0F, 0F, 3, 2, 5, 0F); // Box 25
		attachmentModel[41].setRotationPoint(-15.5F, -2F, 5F);

		attachmentModel[42].addShapeBox(0F, 0F, 0F, 3, 1, 5, 0F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 26
		attachmentModel[42].setRotationPoint(-15.5F, -3F, 5F);

		attachmentModel[43].addShapeBox(0F, 0F, 0F, 4, 2, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F); // Box 27
		attachmentModel[43].setRotationPoint(-6F, -5F, 4.5F);

		attachmentModel[44].addShapeBox(0F, 0F, 0F, 4, 2, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F); // Box 28
		attachmentModel[44].setRotationPoint(6F, -5F, 4.5F);

		attachmentModel[45].addShapeBox(0F, 0F, 0F, 4, 4, 2, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 29
		attachmentModel[45].setRotationPoint(6F, -1F, 6.5F);

		attachmentModel[46].addBox(0F, 0F, 0F, 4, 2, 2, 0F); // Box 30
		attachmentModel[46].setRotationPoint(-6F, 1F, 6.5F);

		attachmentModel[47].addShapeBox(0F, 0F, 0F, 4, 3, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F); // Box 32
		attachmentModel[47].setRotationPoint(-6F, 3F, 6.5F);

		attachmentModel[48].addShapeBox(0F, 0F, 0F, 4, 3, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F); // Box 33
		attachmentModel[48].setRotationPoint(6F, 3F, 6.5F);

		attachmentModel[49].addBox(0F, 0F, 0F, 4, 3, 3, 0F); // Box 34
		attachmentModel[49].setRotationPoint(-2F, 3F, 6F);

		attachmentModel[50].addBox(0F, 0F, 0F, 4, 3, 1, 0F); // Box 35
		attachmentModel[50].setRotationPoint(2F, 3F, 7F);

		renderOffset = 0F;

		flipAll();
	}
}