package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelLK103 extends ModelGun
{
	int textureX = 512;
	int textureY = 256;

	public ModelLK103()
	{
		gunModel = new ModelRendererTurbo[27];
		gunModel[0] = new ModelRendererTurbo(this, 1, 36, textureX, textureY); // Box 1
		gunModel[1] = new ModelRendererTurbo(this, 1, 49, textureX, textureY); // Box 2
		gunModel[2] = new ModelRendererTurbo(this, 1, 64, textureX, textureY); // Box 3
		gunModel[3] = new ModelRendererTurbo(this, 1, 49, textureX, textureY); // Box 6
		gunModel[4] = new ModelRendererTurbo(this, 1, 36, textureX, textureY); // Box 7
		gunModel[5] = new ModelRendererTurbo(this, 62, 97, textureX, textureY); // Box 8
		gunModel[6] = new ModelRendererTurbo(this, 62, 83, textureX, textureY); // Box 9
		gunModel[7] = new ModelRendererTurbo(this, 62, 83, textureX, textureY); // Box 10
		gunModel[8] = new ModelRendererTurbo(this, 144, 36, textureX, textureY); // Box 11
		gunModel[9] = new ModelRendererTurbo(this, 144, 49, textureX, textureY); // Box 12
		gunModel[10] = new ModelRendererTurbo(this, 144, 64, textureX, textureY); // Box 13
		gunModel[11] = new ModelRendererTurbo(this, 144, 49, textureX, textureY); // Box 14
		gunModel[12] = new ModelRendererTurbo(this, 144, 36, textureX, textureY); // Box 15
		gunModel[13] = new ModelRendererTurbo(this, 1, 83, textureX, textureY); // Box 16
		gunModel[14] = new ModelRendererTurbo(this, 1, 97, textureX, textureY); // Box 17
		gunModel[15] = new ModelRendererTurbo(this, 1, 83, textureX, textureY); // Box 18
		gunModel[16] = new ModelRendererTurbo(this, 88, 36, textureX, textureY); // Box 19
		gunModel[17] = new ModelRendererTurbo(this, 88, 49, textureX, textureY); // Box 20
		gunModel[18] = new ModelRendererTurbo(this, 88, 64, textureX, textureY); // Box 21
		gunModel[19] = new ModelRendererTurbo(this, 88, 49, textureX, textureY); // Box 22
		gunModel[20] = new ModelRendererTurbo(this, 88, 36, textureX, textureY); // Box 23
		gunModel[21] = new ModelRendererTurbo(this, 73, 10, textureX, textureY); // Box 24
		gunModel[22] = new ModelRendererTurbo(this, 73, 1, textureX, textureY); // Box 25
		gunModel[23] = new ModelRendererTurbo(this, 48, 1, textureX, textureY); // Box 26
		gunModel[24] = new ModelRendererTurbo(this, 48, 16, textureX, textureY); // Box 27
		gunModel[25] = new ModelRendererTurbo(this, 98, 10, textureX, textureY); // Box 28
		gunModel[26] = new ModelRendererTurbo(this, 98, 23, textureX, textureY); // Box 29

		gunModel[0].addShapeBox(0F, 0F, 0F, 30, 2, 10, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 1
		gunModel[0].setRotationPoint(-24F, -20F, -5F);

		gunModel[1].addShapeBox(0F, 0F, 0F, 30, 2, 12, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 2
		gunModel[1].setRotationPoint(-24F, -18F, -6F);

		gunModel[2].addBox(0F, 0F, 0F, 30, 4, 12, 0F); // Box 3
		gunModel[2].setRotationPoint(-24F, -16F, -6F);

		gunModel[3].addShapeBox(0F, 0F, 0F, 30, 2, 12, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 6
		gunModel[3].setRotationPoint(-24F, -12F, -6F);

		gunModel[4].addShapeBox(0F, 0F, 0F, 30, 2, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 7
		gunModel[4].setRotationPoint(-24F, -10F, -5F);

		gunModel[5].addBox(0F, 0F, 0F, 40, 4, 10, 0F); // Box 8
		gunModel[5].setRotationPoint(6F, -16F, -5F);

		gunModel[6].addShapeBox(0F, 0F, 0F, 40, 3, 10, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 9
		gunModel[6].setRotationPoint(6F, -19F, -5F);

		gunModel[7].addShapeBox(0F, 0F, 0F, 40, 3, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 10
		gunModel[7].setRotationPoint(6F, -12F, -5F);

		gunModel[8].addShapeBox(0F, 0F, 0F, 2, 2, 10, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 11
		gunModel[8].setRotationPoint(43.9F, -20F, -5F);

		gunModel[9].addShapeBox(0F, 0F, 0F, 2, 2, 12, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 12
		gunModel[9].setRotationPoint(43.9F, -18F, -6F);

		gunModel[10].addBox(0F, 0F, 0F, 2, 4, 12, 0F); // Box 13
		gunModel[10].setRotationPoint(43.9F, -16F, -6F);

		gunModel[11].addShapeBox(0F, 0F, 0F, 2, 2, 12, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 14
		gunModel[11].setRotationPoint(43.9F, -12F, -6F);

		gunModel[12].addShapeBox(0F, 0F, 0F, 2, 2, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 15
		gunModel[12].setRotationPoint(43.9F, -10F, -5F);

		gunModel[13].addShapeBox(0F, 0F, 0F, 20, 3, 10, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 16
		gunModel[13].setRotationPoint(-44F, -19F, -5F);

		gunModel[14].addBox(0F, 0F, 0F, 20, 4, 10, 0F); // Box 17
		gunModel[14].setRotationPoint(-44F, -16F, -5F);

		gunModel[15].addShapeBox(0F, 0F, 0F, 20, 3, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 18
		gunModel[15].setRotationPoint(-44F, -12F, -5F);

		gunModel[16].addShapeBox(0F, 0F, 0F, 15, 2, 10, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 19
		gunModel[16].setRotationPoint(-59F, -20F, -5F);

		gunModel[17].addShapeBox(0F, 0F, 0F, 15, 2, 12, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 20
		gunModel[17].setRotationPoint(-59F, -18F, -6F);

		gunModel[18].addBox(0F, 0F, 0F, 15, 4, 12, 0F); // Box 21
		gunModel[18].setRotationPoint(-59F, -16F, -6F);

		gunModel[19].addShapeBox(0F, 0F, 0F, 15, 2, 12, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 22
		gunModel[19].setRotationPoint(-59F, -12F, -6F);

		gunModel[20].addShapeBox(0F, 0F, 0F, 15, 2, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 23
		gunModel[20].setRotationPoint(-59F, -10F, -5F);

		gunModel[21].addBox(0F, 0F, 0F, 8, 18, 4, 0F); // Box 24
		gunModel[21].setRotationPoint(-8F, -8F, -2F);

		gunModel[22].addBox(0F, 0F, 0F, 20, 4, 4, 0F); // Box 25
		gunModel[22].setRotationPoint(0F, -9F, -2F);

		gunModel[23].addBox(0F, 0F, 0F, 8, 10, 4, 0F); // Box 26
		gunModel[23].setRotationPoint(10F, -5F, -2F);

		gunModel[24].addBox(0F, 0F, 0F, 7, 10, 4, 0F); // Box 27
		gunModel[24].setRotationPoint(10.5F, 5F, -2F);

		gunModel[25].addBox(0F, 0F, 0F, 20, 6, 6, 0F); // Box 28
		gunModel[25].setRotationPoint(-44F, -10F, -3F);

		gunModel[26].addBox(0F, 0F, 0F, 12, 5, 5, 0F); // Box 29
		gunModel[26].setRotationPoint(-8F, -14F, 1.5F);


		defaultScopeModel = new ModelRendererTurbo[5];
		defaultScopeModel[0] = new ModelRendererTurbo(this, 18, 13, textureX, textureY); // Box 0
		defaultScopeModel[1] = new ModelRendererTurbo(this, 18, 19, textureX, textureY); // Box 30
		defaultScopeModel[2] = new ModelRendererTurbo(this, 18, 19, textureX, textureY); // Box 31
		defaultScopeModel[3] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 32
		defaultScopeModel[4] = new ModelRendererTurbo(this, 1, 13, textureX, textureY); // Box 33

		defaultScopeModel[0].addBox(0F, 0F, 0F, 10, 2, 3, 0F); // Box 0
		defaultScopeModel[0].setRotationPoint(-5F, -22F, -1.5F);

		defaultScopeModel[1].addBox(0F, 0F, 0F, 5, 5, 1, 0F); // Box 30
		defaultScopeModel[1].setRotationPoint(0F, -25F, -2F);

		defaultScopeModel[2].addBox(0F, 0F, 0F, 5, 5, 1, 0F); // Box 31
		defaultScopeModel[2].setRotationPoint(0F, -25F, 1F);

		defaultScopeModel[3].addBox(0F, 0F, 0F, 8, 5, 6, 0F); // Box 32
		defaultScopeModel[3].setRotationPoint(33F, -22F, -3F);

		defaultScopeModel[4].addBox(0F, 0F, 0F, 2, 8, 6, 0F); // Box 33
		defaultScopeModel[4].setRotationPoint(39F, -30F, -3F);


		ammoModel = new ModelRendererTurbo[10];
		ammoModel[0] = new ModelRendererTurbo(this, 38, 112, textureX, textureY); // rocketBase2
		ammoModel[1] = new ModelRendererTurbo(this, 38, 126, textureX, textureY); // rocketBase1
		ammoModel[2] = new ModelRendererTurbo(this, 38, 112, textureX, textureY); // rocketBase2-2
		ammoModel[3] = new ModelRendererTurbo(this, 1, 126, textureX, textureY); // rocketTip2
		ammoModel[4] = new ModelRendererTurbo(this, 1, 112, textureX, textureY); // rocketTip1
		ammoModel[5] = new ModelRendererTurbo(this, 1, 112, textureX, textureY); // rocketTip1-2
		ammoModel[6] = new ModelRendererTurbo(this, 79, 112, textureX, textureY); // Box 39
		ammoModel[7] = new ModelRendererTurbo(this, 79, 126, textureX, textureY); // Box 40
		ammoModel[8] = new ModelRendererTurbo(this, 79, 112, textureX, textureY); // Box 41
		ammoModel[9] = new ModelRendererTurbo(this, 1, 141, textureX, textureY); // Box 42

		ammoModel[0].addShapeBox(0F, 0F, 0F, 10, 3, 10, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // rocketBase2
		ammoModel[0].setRotationPoint(48F, -19F, -5F);

		ammoModel[1].addBox(0F, 0F, 0F, 10, 4, 10, 0F); // rocketBase1
		ammoModel[1].setRotationPoint(48F, -16F, -5F);

		ammoModel[2].addShapeBox(0F, 0F, 0F, 10, 3, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // rocketBase2-2
		ammoModel[2].setRotationPoint(48F, -12F, -5F);

		ammoModel[3].addShapeBox(0F, 0F, 0F, 8, 4, 10, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F); // rocketTip2
		ammoModel[3].setRotationPoint(58F, -16F, -5F);

		ammoModel[4].addShapeBox(0F, 0F, 0F, 8, 3, 10, 0F, 0F, 0F, -3F, 0F, -3F, -3F, 0F, -3F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F); // rocketTip1
		ammoModel[4].setRotationPoint(58F, -19F, -5F);

		ammoModel[5].addShapeBox(0F, 0F, 0F, 8, 3, 10, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, -3F, -3F, 0F, -3F, -3F, 0F, 0F, -3F); // rocketTip1-2
		ammoModel[5].setRotationPoint(58F, -12F, -5F);

		ammoModel[6].addShapeBox(0F, 0F, 0F, 3, 3, 10, 0F, 0F, 0F, -3F, 0F, -3F, -3F, 0F, -3F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F); // Box 39
		ammoModel[6].setRotationPoint(46F, -19F, -5F);

		ammoModel[7].addShapeBox(0F, 0F, 0F, 3, 4, 10, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F); // Box 40
		ammoModel[7].setRotationPoint(46F, -16F, -5F);

		ammoModel[8].addShapeBox(0F, 0F, 0F, 3, 3, 10, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, -3F, -3F, 0F, -3F, -3F, 0F, 0F, -3F); // Box 41
		ammoModel[8].setRotationPoint(46F, -12F, -5F);

		ammoModel[9].addBox(0F, 0F, 0F, 10, 4, 4, 0F); // Box 42
		ammoModel[9].setRotationPoint(36F, -16F, -2F);

		scopeAttachPoint = new Vector3f(20F /16F, 17F /16F, 0F /16F);

		gunSlideDistance = 0F;
		animationType = EnumAnimationType.END_LOADED;

		translateAll(0F, 0F, 0F);

		flipAll();
	}
}