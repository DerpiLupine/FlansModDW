package com.flansmod.client.model.dwfirearms;

import net.minecraft.client.model.ModelBase;
import net.minecraft.entity.Entity;

import com.flansmod.client.tmt.ModelRendererTurbo;
import org.lwjgl.opengl.GL11;

public class ModelFragGrenadeSMC extends ModelBase 
{
	public ModelRendererTurbo[] grenadeModel;

	int textureX = 512;
	int textureY = 256;

	public ModelFragGrenadeSMC()
	{
		grenadeModel = new ModelRendererTurbo[45];
		grenadeModel[0] = new ModelRendererTurbo(this, 30, 76, textureX, textureY); // lidTop3
		grenadeModel[1] = new ModelRendererTurbo(this, 72, 81, textureX, textureY); // cap2
		grenadeModel[2] = new ModelRendererTurbo(this, 72, 81, textureX, textureY); // cap1
		grenadeModel[3] = new ModelRendererTurbo(this, 72, 81, textureX, textureY); // cap3
		grenadeModel[4] = new ModelRendererTurbo(this, 89, 81, textureX, textureY); // topPart1
		grenadeModel[5] = new ModelRendererTurbo(this, 82, 75, textureX, textureY); // topPart2
		grenadeModel[6] = new ModelRendererTurbo(this, 73, 77, textureX, textureY); // leverPart1
		grenadeModel[7] = new ModelRendererTurbo(this, 59, 73, textureX, textureY); // leverPart2
		grenadeModel[8] = new ModelRendererTurbo(this, 66, 74, textureX, textureY); // leverPart3
		grenadeModel[9] = new ModelRendererTurbo(this, 97, 73, textureX, textureY); // pinHolder
		grenadeModel[10] = new ModelRendererTurbo(this, 66, 15, textureX, textureY); // ring1
		grenadeModel[11] = new ModelRendererTurbo(this, 29, 90, textureX, textureY); // Box 1
		grenadeModel[12] = new ModelRendererTurbo(this, 29, 104, textureX, textureY); // Box 2
		grenadeModel[13] = new ModelRendererTurbo(this, 29, 90, textureX, textureY); // Box 4
		grenadeModel[14] = new ModelRendererTurbo(this, 29, 104, textureX, textureY); // Box 5
		grenadeModel[15] = new ModelRendererTurbo(this, 1, 129, textureX, textureY); // Box 6
		grenadeModel[16] = new ModelRendererTurbo(this, 30, 129, textureX, textureY); // Box 9
		grenadeModel[17] = new ModelRendererTurbo(this, 55, 131, textureX, textureY); // Box 10
		grenadeModel[18] = new ModelRendererTurbo(this, 30, 129, textureX, textureY); // Box 11
		grenadeModel[19] = new ModelRendererTurbo(this, 55, 131, textureX, textureY); // Box 12
		grenadeModel[20] = new ModelRendererTurbo(this, 59, 82, textureX, textureY); // Box 13
		grenadeModel[21] = new ModelRendererTurbo(this, 73, 73, textureX, textureY); // Box 14
		grenadeModel[22] = new ModelRendererTurbo(this, 66, 15, textureX, textureY); // Box 16
		grenadeModel[23] = new ModelRendererTurbo(this, 71, 11, textureX, textureY); // Box 18
		grenadeModel[24] = new ModelRendererTurbo(this, 71, 11, textureX, textureY); // Box 19
		grenadeModel[25] = new ModelRendererTurbo(this, 99, 115, textureX, textureY); // Box 20
		grenadeModel[26] = new ModelRendererTurbo(this, 74, 113, textureX, textureY); // Box 21
		grenadeModel[27] = new ModelRendererTurbo(this, 74, 127, textureX, textureY); // Box 22
		grenadeModel[28] = new ModelRendererTurbo(this, 74, 113, textureX, textureY); // Box 23
		grenadeModel[29] = new ModelRendererTurbo(this, 99, 115, textureX, textureY); // Box 24
		grenadeModel[30] = new ModelRendererTurbo(this, 58, 90, textureX, textureY); // Box 28
		grenadeModel[31] = new ModelRendererTurbo(this, 58, 90, textureX, textureY); // Box 29
		grenadeModel[32] = new ModelRendererTurbo(this, 58, 90, textureX, textureY); // Box 30
		grenadeModel[33] = new ModelRendererTurbo(this, 84, 90, textureX, textureY); // Box 45
		grenadeModel[34] = new ModelRendererTurbo(this, 1, 104, textureX, textureY); // Box 0
		grenadeModel[35] = new ModelRendererTurbo(this, 1, 90, textureX, textureY); // Box 1
		grenadeModel[36] = new ModelRendererTurbo(this, 1, 76, textureX, textureY); // Box 2
		grenadeModel[37] = new ModelRendererTurbo(this, 1, 104, textureX, textureY); // Box 3
		grenadeModel[38] = new ModelRendererTurbo(this, 1, 90, textureX, textureY); // Box 4
		grenadeModel[39] = new ModelRendererTurbo(this, 55, 118, textureX, textureY); // Box 5
		grenadeModel[40] = new ModelRendererTurbo(this, 30, 116, textureX, textureY); // Box 6
		grenadeModel[41] = new ModelRendererTurbo(this, 1, 116, textureX, textureY); // Box 7
		grenadeModel[42] = new ModelRendererTurbo(this, 30, 116, textureX, textureY); // Box 8
		grenadeModel[43] = new ModelRendererTurbo(this, 55, 118, textureX, textureY); // Box 9
		grenadeModel[44] = new ModelRendererTurbo(this, 93, 75, textureX, textureY); // Box 10

		grenadeModel[0].addBox(-1F, -10F, -4F, 4, 3, 10, 0F); // lidTop3
		grenadeModel[0].setRotationPoint(0F, 0F, 0F);

		grenadeModel[1].addBox(0F, -13F, -2F, 2, 2, 6, 0F); // cap2
		grenadeModel[1].setRotationPoint(0F, 0F, 0F);

		grenadeModel[2].addShapeBox(-2F, -13F, -2F, 2, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F); // cap1
		grenadeModel[2].setRotationPoint(0F, 0F, 0F);

		grenadeModel[3].addShapeBox(2F, -13F, -2F, 2, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F); // cap3
		grenadeModel[3].setRotationPoint(0F, 0F, 0F);

		grenadeModel[4].addBox(-0.5F, -15F, -3F, 3, 2, 6, 0F); // topPart1
		grenadeModel[4].setRotationPoint(0F, 0F, 0F);

		grenadeModel[5].addBox(-0.5F, -16F, -3F, 3, 1, 4, 0F); // topPart2
		grenadeModel[5].setRotationPoint(0F, 0F, 0F);

		grenadeModel[6].addShapeBox(0F, -14.5F, -5F, 2, 1, 2, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // leverPart1
		grenadeModel[6].setRotationPoint(0F, 0F, 0F);

		grenadeModel[7].addShapeBox(0F, -11.5F, -6F, 2, 6, 1, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // leverPart2
		grenadeModel[7].setRotationPoint(0F, 0F, 0F);

		grenadeModel[8].addShapeBox(0F, -6.5F, -6F, 2, 5, 1, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // leverPart3
		grenadeModel[8].setRotationPoint(0F, 0F, 0F);

		grenadeModel[9].addBox(0F, -16F, 1F, 2, 2, 5, 0F); // pinHolder
		grenadeModel[9].setRotationPoint(0F, 0F, 0F);

		grenadeModel[10].addShapeBox(-1.5F, -14.3F, -6.75F, 5, 1, 1, 0F, -1F, -0.5F, 0F, -1F, -0.5F, 0F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, -0.5F, -0.25F, 0F, -0.5F, -0.25F); // ring1
		grenadeModel[10].setRotationPoint(0F, 0F, 0F);

		grenadeModel[11].addShapeBox(-3F, -10F, -4F, 2, 3, 10, 0F, -0.5F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, -1F, -0.5F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, -1F); // Box 1
		grenadeModel[11].setRotationPoint(0F, 0F, 0F);

		grenadeModel[12].addShapeBox(-4F, -10F, -3F, 1, 3, 8, 0F, 0F, 0F, -2F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, -2F); // Box 2
		grenadeModel[12].setRotationPoint(0F, 0F, 0F);

		grenadeModel[13].addShapeBox(3F, -10F, -4F, 2, 3, 10, 0F, 0F, 0F, 0F, -0.5F, 0F, -1F, -0.5F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, -1F, -0.5F, 0F, -1F, 0F, 0F, 0F); // Box 4
		grenadeModel[13].setRotationPoint(0F, 0F, 0F);

		grenadeModel[14].addShapeBox(5F, -10F, -3F, 1, 3, 8, 0F, 0.5F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0.5F, 0F, 0F); // Box 5
		grenadeModel[14].setRotationPoint(0F, 0F, 0F);

		grenadeModel[15].addShapeBox(-1F, -11F, -4F, 4, 1, 10, 0F, -0.5F, 0F, -1.5F, -0.5F, 0F, -1.5F, -0.5F, 0F, -1.5F, -0.5F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 6
		grenadeModel[15].setRotationPoint(0F, 0F, 0F);

		grenadeModel[16].addShapeBox(3F, -11F, -4F, 2, 1, 10, 0F, 0.5F, 0F, -1.5F, -1.5F, 0F, -2.5F, -1.5F, 0F, -2.5F, 0.5F, 0F, -1.5F, 0F, 0F, 0F, -0.5F, 0F, -1F, -0.5F, 0F, -1F, 0F, 0F, 0F); // Box 9
		grenadeModel[16].setRotationPoint(0F, 0F, 0F);

		grenadeModel[17].addShapeBox(5F, -11F, -3F, 1, 1, 8, 0F, 1.5F, 0F, -1.5F, -1.5F, 0F, -2.5F, -1.5F, 0F, -2.5F, 1.5F, 0F, -1.5F, 0.5F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0.5F, 0F, 0F); // Box 10
		grenadeModel[17].setRotationPoint(0F, 0F, 0F);

		grenadeModel[18].addShapeBox(-3F, -11F, -4F, 2, 1, 10, 0F, -1.5F, 0F, -2.5F, 0.5F, 0F, -1.5F, 0.5F, 0F, -1.5F, -1.5F, 0F, -2.5F, -0.5F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, -1F); // Box 11
		grenadeModel[18].setRotationPoint(0F, 0F, 0F);

		grenadeModel[19].addShapeBox(-4F, -11F, -3F, 1, 1, 8, 0F, -1.5F, 0F, -2.5F, 1.5F, 0F, -1.5F, 1.5F, 0F, -1.5F, -1.5F, 0F, -2.5F, 0F, 0F, -2F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, -2F); // Box 12
		grenadeModel[19].setRotationPoint(0F, 0F, 0F);

		grenadeModel[20].addBox(0F, -16.5F, -3.5F, 2, 3, 4, 0F); // Box 13
		grenadeModel[20].setRotationPoint(0F, 0F, 0F);

		grenadeModel[21].addShapeBox(-0.5F, -15F, 3F, 3, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 14
		grenadeModel[21].setRotationPoint(0F, 0F, 0F);

		grenadeModel[22].addShapeBox(-1.5F, -16.05F, -4F, 5, 1, 1, 0F, 0F, -0.5F, -0.25F, 0F, -0.5F, -0.25F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, -1F, -0.5F, 0F, -1F, -0.5F, 0F); // Box 16
		grenadeModel[22].setRotationPoint(0F, 0F, 0F);

		grenadeModel[23].addShapeBox(-1.5F, -14.3F, -5.75F, 1, 1, 2, 0F, 0F, 0F, 0.25F, -0.25F, 0F, 0.25F, -0.25F, 1.25F, 0F, 0F, 1.25F, 0F, 0F, -0.5F, 0.25F, -0.25F, -0.5F, 0.25F, -0.25F, -1.75F, 0F, 0F, -1.75F, 0F); // Box 18
		grenadeModel[23].setRotationPoint(0F, 0F, 0F);

		grenadeModel[24].addShapeBox(2.5F, -14.3F, -5.75F, 1, 1, 2, 0F, -0.25F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 1.25F, 0F, -0.25F, 1.25F, 0F, -0.25F, -0.5F, 0.25F, 0F, -0.5F, 0.25F, 0F, -1.75F, 0F, -0.25F, -1.75F, 0F); // Box 19
		grenadeModel[24].setRotationPoint(0F, 0F, 0F);

		grenadeModel[25].addShapeBox(-4F, 6F, -3F, 1, 3, 8, 0F, 0F, 0F, -2F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, -2F); // Box 20
		grenadeModel[25].setRotationPoint(0F, 0F, 0F);

		grenadeModel[26].addShapeBox(-3F, 6F, -4F, 2, 3, 10, 0F, -0.5F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, -1F, -0.5F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, -1F); // Box 21
		grenadeModel[26].setRotationPoint(0F, 0F, 0F);

		grenadeModel[27].addBox(-1F, 6F, -4F, 4, 3, 10, 0F); // Box 22
		grenadeModel[27].setRotationPoint(0F, 0F, 0F);

		grenadeModel[28].addShapeBox(3F, 6F, -4F, 2, 3, 10, 0F, 0F, 0F, 0F, -0.5F, 0F, -1F, -0.5F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, -1F, -0.5F, 0F, -1F, 0F, 0F, 0F); // Box 23
		grenadeModel[28].setRotationPoint(0F, 0F, 0F);

		grenadeModel[29].addShapeBox(5F, 6F, -3F, 1, 3, 8, 0F, 0.5F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0.5F, 0F, 0F); // Box 24
		grenadeModel[29].setRotationPoint(0F, 0F, 0F);

		grenadeModel[30].addBox(-0.5F, -7F, -3.5F, 3, 13, 9, 0F); // Box 28
		grenadeModel[30].setRotationPoint(0F, 0F, 0F);

		grenadeModel[31].addShapeBox(-3.5F, -7F, -3.5F, 3, 13, 9, 0F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F); // Box 29
		grenadeModel[31].setRotationPoint(0F, 0F, 0F);

		grenadeModel[32].addShapeBox(2.5F, -7F, -3.5F, 3, 13, 9, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F); // Box 30
		grenadeModel[32].setRotationPoint(0F, 0F, 0F);

		grenadeModel[33].addShapeBox(-1F, -15.5F, -2.5F, 4, 6, 9, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, -6F, -0.25F, 0F, -6F, -0.25F, -4F, 0F, -0.25F, -4F, 0F, -0.25F, -4F, -6F, -0.25F, -4F, -6F); // Box 45
		grenadeModel[33].setRotationPoint(0F, 0F, 0F);

		grenadeModel[34].addShapeBox(5F, -6.5F, -3F, 1, 3, 8, 0F, 0.5F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0.5F, 0F, 0F); // Box 0
		grenadeModel[34].setRotationPoint(0F, 0F, 0F);

		grenadeModel[35].addShapeBox(3F, -6.5F, -4F, 2, 3, 10, 0F, 0F, 0F, 0F, -0.5F, 0F, -1F, -0.5F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, -1F, -0.5F, 0F, -1F, 0F, 0F, 0F); // Box 1
		grenadeModel[35].setRotationPoint(0F, 0F, 0F);

		grenadeModel[36].addBox(-1F, -6.5F, -4F, 4, 3, 10, 0F); // Box 2
		grenadeModel[36].setRotationPoint(0F, 0F, 0F);

		grenadeModel[37].addShapeBox(-4F, -6.5F, -3F, 1, 3, 8, 0F, 0F, 0F, -2F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, -2F); // Box 3
		grenadeModel[37].setRotationPoint(0F, 0F, 0F);

		grenadeModel[38].addShapeBox(-3F, -6.5F, -4F, 2, 3, 10, 0F, -0.5F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, -1F, -0.5F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, -1F); // Box 4
		grenadeModel[38].setRotationPoint(0F, 0F, 0F);

		grenadeModel[39].addShapeBox(-4F, 9F, -3F, 1, 2, 8, 0F, 0F, 0F, -2F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, -2F, -1.5F, 0F, -2.5F, 1.5F, 0F, -1.5F, 1.5F, 0F, -1.5F, -1.5F, 0F, -2.5F); // Box 5
		grenadeModel[39].setRotationPoint(0F, 0F, 0F);

		grenadeModel[40].addShapeBox(-3F, 9F, -4F, 2, 2, 10, 0F, -0.5F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, -1F, -1.5F, 0F, -2.5F, 0.5F, 0F, -1.5F, 0.5F, 0F, -1.5F, -1.5F, 0F, -2.5F); // Box 6
		grenadeModel[40].setRotationPoint(0F, 0F, 0F);

		grenadeModel[41].addShapeBox(-1F, 9F, -4F, 4, 2, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, -1.5F, -0.5F, 0F, -1.5F, -0.5F, 0F, -1.5F, -0.5F, 0F, -1.5F); // Box 7
		grenadeModel[41].setRotationPoint(0F, 0F, 0F);

		grenadeModel[42].addShapeBox(3F, 9F, -4F, 2, 2, 10, 0F, 0F, 0F, 0F, -0.5F, 0F, -1F, -0.5F, 0F, -1F, 0F, 0F, 0F, 0.5F, 0F, -1.5F, -1.5F, 0F, -2.5F, -1.5F, 0F, -2.5F, 0.5F, 0F, -1.5F); // Box 8
		grenadeModel[42].setRotationPoint(0F, 0F, 0F);

		grenadeModel[43].addShapeBox(5F, 9F, -3F, 1, 2, 8, 0F, 0.5F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0.5F, 0F, 0F, 1.5F, 0F, -1.5F, -1.5F, 0F, -2.5F, -1.5F, 0F, -2.5F, 1.5F, 0F, -1.5F); // Box 9
		grenadeModel[43].setRotationPoint(0F, 0F, 0F);

		grenadeModel[44].addBox(-0.5F, -15.5F, 4.5F, 3, 1, 1, 0F); // Box 10
		grenadeModel[44].setRotationPoint(0F, 0F, 0F);

		for(int i = 0; i < 45; i++)
			grenadeModel[i].rotateAngleZ = 3.14159265F;
	}

	@Override
	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
	{
		for(ModelRendererTurbo mineModelBit : grenadeModel)
		{
			GL11.glPushMatrix();
			GL11.glScalef(0.33F, 0.33F, 0.33F);
			mineModelBit.render(f5);
			GL11.glPopMatrix();
		}
	}
}