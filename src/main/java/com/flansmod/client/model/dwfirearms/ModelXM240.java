package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelXM240 extends ModelGun
{
	int textureX = 512;
	int textureY = 256;

	public ModelXM240()
	{
		gunModel = new ModelRendererTurbo[73];
		gunModel[0] = new ModelRendererTurbo(this, 80, 40, textureX, textureY); // ammoClip
		gunModel[1] = new ModelRendererTurbo(this, 109, 36, textureX, textureY); // ammoClip2
		gunModel[2] = new ModelRendererTurbo(this, 140, 1, textureX, textureY); // barrel1
		gunModel[3] = new ModelRendererTurbo(this, 140, 1, textureX, textureY); // barrel1
		gunModel[4] = new ModelRendererTurbo(this, 140, 11, textureX, textureY); // barrel2
		gunModel[5] = new ModelRendererTurbo(this, 140, 40, textureX, textureY); // body1
		gunModel[6] = new ModelRendererTurbo(this, 140, 107, textureX, textureY); // body10
		gunModel[7] = new ModelRendererTurbo(this, 140, 79, textureX, textureY); // body2
		gunModel[8] = new ModelRendererTurbo(this, 217, 81, textureX, textureY); // body5
		gunModel[9] = new ModelRendererTurbo(this, 217, 93, textureX, textureY); // body7
		gunModel[10] = new ModelRendererTurbo(this, 217, 40, textureX, textureY); // body9
		gunModel[11] = new ModelRendererTurbo(this, 140, 164, textureX, textureY); // receiverBarrel1
		gunModel[12] = new ModelRendererTurbo(this, 229, 135, textureX, textureY); // receiverPart1
		gunModel[13] = new ModelRendererTurbo(this, 140, 164, textureX, textureY); // receiverBarrel1
		gunModel[14] = new ModelRendererTurbo(this, 140, 164, textureX, textureY); // receiverBarrel1
		gunModel[15] = new ModelRendererTurbo(this, 216, 135, textureX, textureY); // receiverPart2
		gunModel[16] = new ModelRendererTurbo(this, 216, 132, textureX, textureY); // receiverTube3
		gunModel[17] = new ModelRendererTurbo(this, 216, 129, textureX, textureY); // receiverTube2
		gunModel[18] = new ModelRendererTurbo(this, 227, 132, textureX, textureY); // receiverTube1
		gunModel[19] = new ModelRendererTurbo(this, 252, 136, textureX, textureY); // receiverPart3
		gunModel[20] = new ModelRendererTurbo(this, 140, 129, textureX, textureY); // forerailPart1
		gunModel[21] = new ModelRendererTurbo(this, 140, 120, textureX, textureY); // forerailPart2
		gunModel[22] = new ModelRendererTurbo(this, 32, 4, textureX, textureY); // grip1
		gunModel[23] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // igniter
		gunModel[24] = new ModelRendererTurbo(this, 64, 3, textureX, textureY); // ironSight
		gunModel[25] = new ModelRendererTurbo(this, 24, 2, textureX, textureY); // ironSight
		gunModel[26] = new ModelRendererTurbo(this, 1, 4, textureX, textureY); // ironSight2
		gunModel[27] = new ModelRendererTurbo(this, 1, 2, textureX, textureY); // ironSightBase
		gunModel[28] = new ModelRendererTurbo(this, 200, 31, textureX, textureY); // lowerBarrel1
		gunModel[29] = new ModelRendererTurbo(this, 229, 34, textureX, textureY); // lowerBarrel2
		gunModel[30] = new ModelRendererTurbo(this, 229, 34, textureX, textureY); // lowerBarrel2
		gunModel[31] = new ModelRendererTurbo(this, 183, 33, textureX, textureY); // lowerBarrel3
		gunModel[32] = new ModelRendererTurbo(this, 140, 22, textureX, textureY); // lowerBarrel4
		gunModel[33] = new ModelRendererTurbo(this, 140, 22, textureX, textureY); // lowerBarrel4
		gunModel[34] = new ModelRendererTurbo(this, 140, 22, textureX, textureY); // lowerBarrel4
		gunModel[35] = new ModelRendererTurbo(this, 235, 1, textureX, textureY); // muzzle1
		gunModel[36] = new ModelRendererTurbo(this, 274, 1, textureX, textureY); // muzzle1
		gunModel[37] = new ModelRendererTurbo(this, 235, 14, textureX, textureY); // muzzle2
		gunModel[38] = new ModelRendererTurbo(this, 101, 12, textureX, textureY); // rail
		gunModel[39] = new ModelRendererTurbo(this, 101, 12, textureX, textureY); // rail
		gunModel[40] = new ModelRendererTurbo(this, 101, 12, textureX, textureY); // rail
		gunModel[41] = new ModelRendererTurbo(this, 101, 12, textureX, textureY); // rail
		gunModel[42] = new ModelRendererTurbo(this, 80, 1, textureX, textureY); // railbase
		gunModel[43] = new ModelRendererTurbo(this, 80, 11, textureX, textureY); // railBolt
		gunModel[44] = new ModelRendererTurbo(this, 80, 11, textureX, textureY); // railBolt
		gunModel[45] = new ModelRendererTurbo(this, 109, 121, textureX, textureY); // stockPipe1
		gunModel[46] = new ModelRendererTurbo(this, 242, 42, textureX, textureY); // Box 0
		gunModel[47] = new ModelRendererTurbo(this, 217, 68, textureX, textureY); // Box 1
		gunModel[48] = new ModelRendererTurbo(this, 267, 44, textureX, textureY); // Box 2
		gunModel[49] = new ModelRendererTurbo(this, 140, 93, textureX, textureY); // Box 3
		gunModel[50] = new ModelRendererTurbo(this, 165, 107, textureX, textureY); // Box 4
		gunModel[51] = new ModelRendererTurbo(this, 109, 126, textureX, textureY); // Box 7
		gunModel[52] = new ModelRendererTurbo(this, 20, 14, textureX, textureY); // Box 56
		gunModel[53] = new ModelRendererTurbo(this, 20, 34, textureX, textureY); // Box 57
		gunModel[54] = new ModelRendererTurbo(this, 20, 24, textureX, textureY); // Box 58
		gunModel[55] = new ModelRendererTurbo(this, 20, 47, textureX, textureY); // Box 59
		gunModel[56] = new ModelRendererTurbo(this, 1, 14, textureX, textureY); // Box 60
		gunModel[57] = new ModelRendererTurbo(this, 1, 24, textureX, textureY); // Box 61
		gunModel[58] = new ModelRendererTurbo(this, 1, 34, textureX, textureY); // Box 62
		gunModel[59] = new ModelRendererTurbo(this, 1, 47, textureX, textureY); // Box 63
		gunModel[60] = new ModelRendererTurbo(this, 262, 60, textureX, textureY); // Box 135
		gunModel[61] = new ModelRendererTurbo(this, 262, 81, textureX, textureY); // Box 139
		gunModel[62] = new ModelRendererTurbo(this, 269, 87, textureX, textureY); // Box 140
		gunModel[63] = new ModelRendererTurbo(this, 262, 87, textureX, textureY); // Box 141
		gunModel[64] = new ModelRendererTurbo(this, 273, 81, textureX, textureY); // Box 142
		gunModel[65] = new ModelRendererTurbo(this, 262, 92, textureX, textureY); // Box 143
		gunModel[66] = new ModelRendererTurbo(this, 273, 92, textureX, textureY); // Box 144
		gunModel[67] = new ModelRendererTurbo(this, 262, 97, textureX, textureY); // Box 145
		gunModel[68] = new ModelRendererTurbo(this, 273, 97, textureX, textureY); // Box 146
		gunModel[69] = new ModelRendererTurbo(this, 285, 74, textureX, textureY); // Box 147
		gunModel[70] = new ModelRendererTurbo(this, 262, 103, textureX, textureY); // Box 148
		gunModel[71] = new ModelRendererTurbo(this, 262, 106, textureX, textureY); // Box 149
		gunModel[72] = new ModelRendererTurbo(this, 273, 106, textureX, textureY); // Box 150

		gunModel[0].addBox(0F, 0F, 0F, 7, 2, 7, 0F); // ammoClip
		gunModel[0].setRotationPoint(17.5F, -10F, -3.5F);
		gunModel[0].rotateAngleZ = -0.19198622F;

		gunModel[1].addShapeBox(0F, 0F, 0F, 2, 8, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // ammoClip2
		gunModel[1].setRotationPoint(23F, -8F, -2.5F);
		gunModel[1].rotateAngleZ = 0.36651914F;

		gunModel[2].addShapeBox(0F, 0F, 0F, 40, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // barrel1
		gunModel[2].setRotationPoint(28F, -21F, -3.5F);

		gunModel[3].addShapeBox(0F, 0F, 0F, 40, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // barrel1
		gunModel[3].setRotationPoint(28F, -16F, -3.5F);

		gunModel[4].addBox(0F, 0F, 0F, 40, 3, 7, 0F); // barrel2
		gunModel[4].setRotationPoint(28F, -19F, -3.5F);

		gunModel[5].addBox(0F, 0F, 0F, 28, 7, 10, 0F); // body1
		gunModel[5].setRotationPoint(-14F, -22F, -5F);

		gunModel[6].addShapeBox(0F, 0F, 0F, 2, 2, 10, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body10
		gunModel[6].setRotationPoint(26F, -24F, -5F);

		gunModel[7].addShapeBox(0F, 0F, 0F, 28, 3, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, -2F, 0F, -2F); // body2
		gunModel[7].setRotationPoint(-14F, -12F, -5F);

		gunModel[8].addShapeBox(0F, 0F, 0F, 12, 1, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F); // body5
		gunModel[8].setRotationPoint(14F, -13F, -5F);

		gunModel[9].addShapeBox(0F, 0F, 0F, 12, 3, 10, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 2F, -2F, 0F, 2F, -2F, 0F, 0F, -2F); // body7
		gunModel[9].setRotationPoint(14F, -12F, -5F);

		gunModel[10].addBox(0F, 0F, 0F, 2, 7, 10, 0F); // body9
		gunModel[10].setRotationPoint(26F, -22F, -5F);

		gunModel[11].addShapeBox(0F, 0F, 0F, 12, 3, 9, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // receiverBarrel1
		gunModel[11].setRotationPoint(14F, -22F, -4.5F);

		gunModel[12].addBox(0F, 0F, 0F, 10, 4, 1, 0F); // receiverPart1
		gunModel[12].setRotationPoint(-8F, -21F, 5F);

		gunModel[13].addShapeBox(0F, 0F, 0F, 12, 3, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F); // receiverBarrel1
		gunModel[13].setRotationPoint(14F, -16F, -4.5F);

		gunModel[14].addBox(0F, 0F, 0F, 12, 3, 9, 0F); // receiverBarrel1
		gunModel[14].setRotationPoint(14F, -19F, -4.5F);

		gunModel[15].addShapeBox(0F, 0F, 0F, 4, 3, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // receiverPart2
		gunModel[15].setRotationPoint(18F, -15F, 5F);

		gunModel[16].addBox(0F, 0F, 0F, 4, 1, 1, 0F); // receiverTube3
		gunModel[16].setRotationPoint(14F, -13.5F, 4.5F);

		gunModel[17].addShapeBox(0F, 0F, 0F, 6, 1, 1, 0F, 0F, 6F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 6F, 0F, 0F, -6F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -6F, 0F); // receiverTube2
		gunModel[17].setRotationPoint(8F, -13.5F, 4.5F);

		gunModel[18].addBox(0F, 0F, 0F, 2, 1, 1, 0F); // receiverTube1
		gunModel[18].setRotationPoint(6F, -19.5F, 4.5F);

		gunModel[19].addShapeBox(0F, 0F, 0F, 4, 3, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F); // receiverPart3
		gunModel[19].setRotationPoint(2F, -20.5F, 5F);

		gunModel[20].addBox(0F, 0F, 0F, 23, 5, 6, 0F); // forerailPart1
		gunModel[20].setRotationPoint(28F, -13.5F, -3F);

		gunModel[21].addBox(0F, 0F, 0F, 23, 1, 7, 0F); // forerailPart2
		gunModel[21].setRotationPoint(28F, -14.5F, -3.5F);

		gunModel[22].addBox(0F, 0F, 0F, 12, 2, 7, 0F); // grip1
		gunModel[22].setRotationPoint(-9F, -10F, -3.5F);

		gunModel[23].addBox(0F, 0F, 0F, 1, 1, 1, 0F); // igniter
		gunModel[23].setRotationPoint(82F, -13.5F, -0.5F);

		gunModel[24].addShapeBox(0F, 0F, 0F, 2, 4, 3, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 1F, 0F, -0.5F, 1F, 0F, 0F, 0F, 0F, 0F); // ironSight
		gunModel[24].setRotationPoint(-12F, -29F, 1F);

		gunModel[25].addShapeBox(0F, 0F, 0F, 2, 4, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, -0.5F, 0F, 0F, -0.5F); // ironSight
		gunModel[25].setRotationPoint(-12F, -29F, -4F);

		gunModel[26].addShapeBox(0F, 0F, 0F, 1, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F); // ironSight2
		gunModel[26].setRotationPoint(26F, -28F, -0.5F);

		gunModel[27].addShapeBox(0F, 0F, 0F, 7, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // ironSightBase
		gunModel[27].setRotationPoint(-13F, -25F, -4F);

		gunModel[28].addBox(0F, 0F, 0F, 10, 4, 4, 0F); // lowerBarrel1
		gunModel[28].setRotationPoint(68F, -13F, -2F);

		gunModel[29].addShapeBox(0F, 0F, 0F, 4, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // lowerBarrel2
		gunModel[29].setRotationPoint(51F, -10F, -2F);

		gunModel[30].addShapeBox(0F, 0F, 0F, 4, 1, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // lowerBarrel2
		gunModel[30].setRotationPoint(51F, -13F, -2F);

		gunModel[31].addBox(0F, 0F, 0F, 4, 2, 4, 0F); // lowerBarrel3
		gunModel[31].setRotationPoint(51F, -12F, -2F);

		gunModel[32].addBox(0F, 0F, 0F, 32, 1, 3, 0F); // lowerBarrel4
		gunModel[32].setRotationPoint(51F, -11.5F, -1.5F);

		gunModel[33].addShapeBox(0F, 0F, 0F, 32, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // lowerBarrel4
		gunModel[33].setRotationPoint(51F, -10.5F, -1.5F);

		gunModel[34].addShapeBox(0F, 0F, 0F, 32, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // lowerBarrel4
		gunModel[34].setRotationPoint(51F, -12.5F, -1.5F);

		gunModel[35].addShapeBox(0F, 0F, 0F, 10, 3, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F); // muzzle1
		gunModel[35].setRotationPoint(68F, -16F, -4.5F);

		gunModel[36].addShapeBox(0F, 0F, 0F, 10, 3, 9, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // muzzle1
		gunModel[36].setRotationPoint(68F, -22F, -4.5F);

		gunModel[37].addBox(0F, 0F, 0F, 10, 3, 9, 0F); // muzzle2
		gunModel[37].setRotationPoint(68F, -19F, -4.5F);

		gunModel[38].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // rail
		gunModel[38].setRotationPoint(35F, -8F, -3.5F);

		gunModel[39].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // rail
		gunModel[39].setRotationPoint(29F, -8F, -3.5F);

		gunModel[40].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // rail
		gunModel[40].setRotationPoint(41F, -8F, -3.5F);

		gunModel[41].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // rail
		gunModel[41].setRotationPoint(47F, -8F, -3.5F);

		gunModel[42].addBox(0F, 0F, 0F, 21, 2, 7, 0F); // railbase
		gunModel[42].setRotationPoint(29F, -10F, -3.5F);

		gunModel[43].addBox(0F, 0F, 0F, 2, 2, 8, 0F); // railBolt
		gunModel[43].setRotationPoint(47F, -11F, -4F);

		gunModel[44].addBox(0F, 0F, 0F, 2, 2, 8, 0F); // railBolt
		gunModel[44].setRotationPoint(30F, -11F, -4F);

		gunModel[45].addShapeBox(0F, 0F, 0F, 8, 3, 1, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // stockPipe1
		gunModel[45].setRotationPoint(14F, -19F, -6F);

		gunModel[46].addBox(0F, 0F, 0F, 2, 5, 10, 0F); // Box 0
		gunModel[46].setRotationPoint(26F, -15F, -5F);

		gunModel[47].addBox(0F, 0F, 0F, 12, 2, 10, 0F); // Box 1
		gunModel[47].setRotationPoint(14F, -15F, -5F);

		gunModel[48].addShapeBox(0F, 0F, 0F, 2, 3, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 2
		gunModel[48].setRotationPoint(26F, -10F, -5F);

		gunModel[49].addBox(0F, 0F, 0F, 28, 3, 10, 0F); // Box 3
		gunModel[49].setRotationPoint(-14F, -15F, -5F);

		gunModel[50].addShapeBox(0F, 0F, 0F, 28, 2, 10, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 4
		gunModel[50].setRotationPoint(-14F, -24F, -5F);

		gunModel[51].addBox(0F, 0F, 0F, 8, 3, 1, 0F); // Box 7
		gunModel[51].setRotationPoint(14F, -19F, -5F);

		gunModel[52].addBox(0F, 0F, 0F, 8, 2, 7, 0F); // Box 56
		gunModel[52].setRotationPoint(-9F, -8F, -3.5F);

		gunModel[53].addShapeBox(0F, 0F, 0F, 8, 5, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 4F, 0F, 0F); // Box 57
		gunModel[53].setRotationPoint(-9F, -6F, -3.5F);

		gunModel[54].addShapeBox(0F, 0F, 0F, 8, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F); // Box 58
		gunModel[54].setRotationPoint(-15F, 4F, -3.5F);

		gunModel[55].addShapeBox(0F, 0F, 0F, 8, 5, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F); // Box 59
		gunModel[55].setRotationPoint(-13F, -1F, -3.5F);

		gunModel[56].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F); // Box 60
		gunModel[56].setRotationPoint(-1F, -8F, -3.5F);

		gunModel[57].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F); // Box 61
		gunModel[57].setRotationPoint(-7F, 4F, -3.5F);

		gunModel[58].addShapeBox(0F, 0F, 0F, 2, 5, 7, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 4F, 0F, 0F, -4F, 0F, -2F, -4F, 0F, -2F, 4F, 0F, 0F); // Box 62
		gunModel[58].setRotationPoint(-1F, -6F, -3.5F);

		gunModel[59].addShapeBox(0F, 0F, 0F, 2, 5, 7, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, -2F, -2F, 0F, -2F, 2F, 0F, 0F); // Box 63
		gunModel[59].setRotationPoint(-5F, -1F, -3.5F);

		gunModel[60].addShapeBox(0F, 0F, 0F, 1, 10, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -5F, 0F, 0F, -5F, 0F, -5F, 0F, 0F, -5F, 0F, 0F, -5F, -5F, 0F, -5F, -5F); // Box 135
		gunModel[60].setRotationPoint(-3F, -27.5F, -10F);

		gunModel[61].addShapeBox(0F, 0F, 0F, 2, 2, 3, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 139
		gunModel[61].setRotationPoint(-3.5F, -28.5F, -11F);

		gunModel[62].addBox(0F, 0F, 0F, 2, 3, 1, 0F); // Box 140
		gunModel[62].setRotationPoint(-3.5F, -26.5F, -11F);

		gunModel[63].addBox(0F, 0F, 0F, 2, 3, 1, 0F); // Box 141
		gunModel[63].setRotationPoint(-3.5F, -26.5F, -5F);

		gunModel[64].addShapeBox(0F, 0F, 0F, 2, 2, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 142
		gunModel[64].setRotationPoint(-3.5F, -28.5F, -7F);

		gunModel[65].addBox(0F, 0F, 0F, 2, 1, 3, 0F); // Box 143
		gunModel[65].setRotationPoint(-3.5F, -28.5F, -9F);

		gunModel[66].addBox(0F, 0F, 0F, 2, 1, 3, 0F); // Box 144
		gunModel[66].setRotationPoint(-3.5F, -22.5F, -9F);

		gunModel[67].addShapeBox(0F, 0F, 0F, 2, 2, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 145
		gunModel[67].setRotationPoint(-3.5F, -23.5F, -11F);

		gunModel[68].addShapeBox(0F, 0F, 0F, 2, 2, 3, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 146
		gunModel[68].setRotationPoint(-3.5F, -23.5F, -7F);

		gunModel[69].addShapeBox(0F, 0F, 0F, 2, 2, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 147
		gunModel[69].setRotationPoint(-3.5F, -21.5F, -9F);

		gunModel[70].addBox(0F, 0F, 0F, 10, 1, 1, 0F); // Box 148
		gunModel[70].setRotationPoint(4F, -18F, -5.5F);

		gunModel[71].addShapeBox(0F, 0F, 0F, 4, 1, 1, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F); // Box 149
		gunModel[71].setRotationPoint(0F, -21F, -5.5F);

		gunModel[72].addBox(0F, 0F, 0F, 2, 1, 1, 0F); // Box 150
		gunModel[72].setRotationPoint(-2F, -21F, -5.5F);


		defaultBarrelModel = new ModelRendererTurbo[3];
		defaultBarrelModel[0] = new ModelRendererTurbo(this, 140, 27, textureX, textureY); // barrel3
		defaultBarrelModel[1] = new ModelRendererTurbo(this, 140, 27, textureX, textureY); // barrel3
		defaultBarrelModel[2] = new ModelRendererTurbo(this, 140, 27, textureX, textureY); // barrel3

		defaultBarrelModel[0].addShapeBox(0F, 0F, 0F, 12, 3, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F); // barrel3
		defaultBarrelModel[0].setRotationPoint(31F, -16F, -4.5F);

		defaultBarrelModel[1].addBox(0F, 0F, 0F, 12, 3, 9, 0F); // barrel3
		defaultBarrelModel[1].setRotationPoint(31F, -19F, -4.5F);

		defaultBarrelModel[2].addShapeBox(0F, 0F, 0F, 12, 3, 9, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // barrel3
		defaultBarrelModel[2].setRotationPoint(31F, -22F, -4.5F);


		defaultStockModel = new ModelRendererTurbo[47];
		defaultStockModel[0] = new ModelRendererTurbo(this, 78, 115, textureX, textureY); // Box 52
		defaultStockModel[1] = new ModelRendererTurbo(this, 109, 131, textureX, textureY); // Box 52
		defaultStockModel[2] = new ModelRendererTurbo(this, 1, 79, textureX, textureY); // Box 52
		defaultStockModel[3] = new ModelRendererTurbo(this, 1, 79, textureX, textureY); // Box 52
		defaultStockModel[4] = new ModelRendererTurbo(this, 1, 92, textureX, textureY); // Box 52
		defaultStockModel[5] = new ModelRendererTurbo(this, 109, 131, textureX, textureY); // Box 52
		defaultStockModel[6] = new ModelRendererTurbo(this, 93, 165, textureX, textureY); // Box 52
		defaultStockModel[7] = new ModelRendererTurbo(this, 93, 165, textureX, textureY); // Box 52
		defaultStockModel[8] = new ModelRendererTurbo(this, 93, 165, textureX, textureY); // Box 52
		defaultStockModel[9] = new ModelRendererTurbo(this, 1, 133, textureX, textureY); // Box 52
		defaultStockModel[10] = new ModelRendererTurbo(this, 1, 105, textureX, textureY); // Box 52
		defaultStockModel[11] = new ModelRendererTurbo(this, 140, 68, textureX, textureY); // Box 52
		defaultStockModel[12] = new ModelRendererTurbo(this, 140, 58, textureX, textureY); // Box 52
		defaultStockModel[13] = new ModelRendererTurbo(this, 140, 58, textureX, textureY); // Box 52
		defaultStockModel[14] = new ModelRendererTurbo(this, 1, 118, textureX, textureY); // Box 52
		defaultStockModel[15] = new ModelRendererTurbo(this, 1, 150, textureX, textureY); // Box 52
		defaultStockModel[16] = new ModelRendererTurbo(this, 1, 165, textureX, textureY); // Box 52
		defaultStockModel[17] = new ModelRendererTurbo(this, 58, 161, textureX, textureY); // Box 52
		defaultStockModel[18] = new ModelRendererTurbo(this, 58, 146, textureX, textureY); // Box 52
		defaultStockModel[19] = new ModelRendererTurbo(this, 93, 148, textureX, textureY); // Box 52
		defaultStockModel[20] = new ModelRendererTurbo(this, 1, 75, textureX, textureY); // Box 52
		defaultStockModel[21] = new ModelRendererTurbo(this, 1, 75, textureX, textureY); // Box 52
		defaultStockModel[22] = new ModelRendererTurbo(this, 1, 75, textureX, textureY); // Box 52
		defaultStockModel[23] = new ModelRendererTurbo(this, 1, 75, textureX, textureY); // Box 52
		defaultStockModel[24] = new ModelRendererTurbo(this, 58, 146, textureX, textureY); // Box 52
		defaultStockModel[25] = new ModelRendererTurbo(this, 93, 148, textureX, textureY); // Box 52
		defaultStockModel[26] = new ModelRendererTurbo(this, 37, 80, textureX, textureY); // Box 52
		defaultStockModel[27] = new ModelRendererTurbo(this, 37, 80, textureX, textureY); // Box 52
		defaultStockModel[28] = new ModelRendererTurbo(this, 37, 80, textureX, textureY); // Box 52
		defaultStockModel[29] = new ModelRendererTurbo(this, 31, 60, textureX, textureY); // Box 52
		defaultStockModel[30] = new ModelRendererTurbo(this, 1, 62, textureX, textureY); // Box 52
		defaultStockModel[31] = new ModelRendererTurbo(this, 1, 62, textureX, textureY); // Box 52
		defaultStockModel[32] = new ModelRendererTurbo(this, 31, 67, textureX, textureY); // Box 52
		defaultStockModel[33] = new ModelRendererTurbo(this, 16, 63, textureX, textureY); // Box 52
		defaultStockModel[34] = new ModelRendererTurbo(this, 1, 178, textureX, textureY); // Box 52
		defaultStockModel[35] = new ModelRendererTurbo(this, 30, 178, textureX, textureY); // Box 52
		defaultStockModel[36] = new ModelRendererTurbo(this, 1, 191, textureX, textureY); // Box 52
		defaultStockModel[37] = new ModelRendererTurbo(this, 14, 198, textureX, textureY); // Box 52
		defaultStockModel[38] = new ModelRendererTurbo(this, 14, 193, textureX, textureY); // Box 52
		defaultStockModel[39] = new ModelRendererTurbo(this, 14, 193, textureX, textureY); // Box 52
		defaultStockModel[40] = new ModelRendererTurbo(this, 27, 192, textureX, textureY); // Box 52
		defaultStockModel[41] = new ModelRendererTurbo(this, 1, 191, textureX, textureY); // Box 52
		defaultStockModel[42] = new ModelRendererTurbo(this, 53, 92, textureX, textureY); // Box 52
		defaultStockModel[43] = new ModelRendererTurbo(this, 53, 96, textureX, textureY); // Box 52
		defaultStockModel[44] = new ModelRendererTurbo(this, 46, 88, textureX, textureY); // Box 52
		defaultStockModel[45] = new ModelRendererTurbo(this, 53, 88, textureX, textureY); // Box 52
		defaultStockModel[46] = new ModelRendererTurbo(this, 46, 95, textureX, textureY); // Box 52

		defaultStockModel[0].addBox(0F, 0F, 0F, 4, 19, 11, 0F); // Box 52
		defaultStockModel[0].setRotationPoint(-52F, -21F, -5.5F);

		defaultStockModel[1].addShapeBox(0F, 0F, 0F, 4, 3, 11, 0F, 0F, -0.5F, -2.5F, 0F, -0.5F, -2.5F, 0F, -0.5F, -2.5F, 0F, -0.5F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 52
		defaultStockModel[1].setRotationPoint(-52F, -24F, -5.5F);

		defaultStockModel[2].addShapeBox(0F, 0F, 0F, 13, 3, 9, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 52
		defaultStockModel[2].setRotationPoint(-48F, -22F, -4.5F);

		defaultStockModel[3].addShapeBox(0F, 0F, 0F, 13, 3, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F); // Box 52
		defaultStockModel[3].setRotationPoint(-48F, -16F, -4.5F);

		defaultStockModel[4].addBox(0F, 0F, 0F, 13, 3, 9, 0F); // Box 52
		defaultStockModel[4].setRotationPoint(-48F, -19F, -4.5F);

		defaultStockModel[5].addShapeBox(0F, 0F, 0F, 4, 3, 11, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -2.5F, 0F, -0.5F, -2.5F, 0F, -0.5F, -2.5F, 0F, -0.5F, -2.5F); // Box 52
		defaultStockModel[5].setRotationPoint(-52F, -2F, -5.5F);

		defaultStockModel[6].addBox(0F, 0F, 0F, 2, 3, 9, 0F); // Box 52
		defaultStockModel[6].setRotationPoint(-16F, -19F, -4.5F);

		defaultStockModel[7].addShapeBox(0F, 0F, 0F, 2, 3, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F); // Box 52
		defaultStockModel[7].setRotationPoint(-16F, -16F, -4.5F);

		defaultStockModel[8].addShapeBox(0F, 0F, 0F, 2, 3, 9, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 52
		defaultStockModel[8].setRotationPoint(-16F, -22F, -4.5F);

		defaultStockModel[9].addBox(0F, 0F, 0F, 18, 4, 12, 0F); // Box 52
		defaultStockModel[9].setRotationPoint(-48F, -9F, -6F);

		defaultStockModel[10].addShapeBox(0F, 0F, 0F, 18, 2, 10, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 52
		defaultStockModel[10].setRotationPoint(-48F, -13F, -5F);

		defaultStockModel[11].addBox(0F, 0F, 0F, 19, 3, 7, 0F); // Box 52
		defaultStockModel[11].setRotationPoint(-35F, -19F, -3.5F);

		defaultStockModel[12].addShapeBox(0F, 0F, 0F, 19, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 52
		defaultStockModel[12].setRotationPoint(-35F, -16F, -3.5F);

		defaultStockModel[13].addShapeBox(0F, 0F, 0F, 19, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 52
		defaultStockModel[13].setRotationPoint(-35F, -21F, -3.5F);

		defaultStockModel[14].addShapeBox(0F, 0F, 0F, 18, 2, 12, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 52
		defaultStockModel[14].setRotationPoint(-48F, -11F, -6F);

		defaultStockModel[15].addShapeBox(0F, 0F, 0F, 18, 2, 12, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 52
		defaultStockModel[15].setRotationPoint(-48F, -5F, -6F);

		defaultStockModel[16].addShapeBox(0F, 0F, 0F, 18, 2, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 52
		defaultStockModel[16].setRotationPoint(-48F, -3F, -5F);

		defaultStockModel[17].addShapeBox(0F, 0F, 0F, 5, 4, 12, 0F, 0F, 0F, 0F, 0F, -1F, -3F, 0F, -1F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, -3F, 0F, -1F, -3F, 0F, 0F, 0F); // Box 52
		defaultStockModel[17].setRotationPoint(-30F, -9F, -6F);

		defaultStockModel[18].addShapeBox(0F, 0F, 0F, 5, 2, 12, 0F, 0F, 0F, 0F, 0F, 1F, -3F, 0F, 1F, -3F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -2F, -4F, 0F, -2F, -4F, 0F, 0F, -1F); // Box 52
		defaultStockModel[18].setRotationPoint(-30F, -5F, -6F);

		defaultStockModel[19].addShapeBox(0F, 0F, 0F, 5, 2, 10, 0F, 0F, 0F, 0F, 0F, 2F, -3F, 0F, 2F, -3F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, -3F, -4F, 0F, -3F, -4F, 0F, 0F, -3F); // Box 52
		defaultStockModel[19].setRotationPoint(-30F, -3F, -5F);

		defaultStockModel[20].addShapeBox(0F, 0F, 0F, 34, 1, 2, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 52
		defaultStockModel[20].setRotationPoint(-48F, -14F, -5F);

		defaultStockModel[21].addShapeBox(0F, 0F, 0F, 34, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 52
		defaultStockModel[21].setRotationPoint(-48F, -13F, -5F);

		defaultStockModel[22].addShapeBox(0F, 0F, 0F, 34, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 52
		defaultStockModel[22].setRotationPoint(-48F, -13F, 3F);

		defaultStockModel[23].addShapeBox(0F, 0F, 0F, 34, 1, 2, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 52
		defaultStockModel[23].setRotationPoint(-48F, -14F, 3F);

		defaultStockModel[24].addShapeBox(0F, 0F, 0F, 5, 2, 12, 0F, 0F, 0F, -1F, 0F, -2F, -4F, 0F, -2F, -4F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 1F, -3F, 0F, 1F, -3F, 0F, 0F, 0F); // Box 52
		defaultStockModel[24].setRotationPoint(-30F, -11F, -6F);

		defaultStockModel[25].addShapeBox(0F, 0F, 0F, 5, 2, 10, 0F, 0F, 0F, -3F, 0F, -3F, -4F, 0F, -3F, -4F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 2F, -3F, 0F, 2F, -3F, 0F, 0F, 0F); // Box 52
		defaultStockModel[25].setRotationPoint(-30F, -13F, -5F);

		defaultStockModel[26].addBox(0F, 0F, 0F, 3, 2, 5, 0F); // Box 52
		defaultStockModel[26].setRotationPoint(-25F, -8F, -2.5F);

		defaultStockModel[27].addShapeBox(0F, 0F, 0F, 3, 2, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F); // Box 52
		defaultStockModel[27].setRotationPoint(-25F, -6F, -2.5F);

		defaultStockModel[28].addShapeBox(0F, 0F, 0F, 3, 2, 5, 0F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 52
		defaultStockModel[28].setRotationPoint(-25F, -10F, -2.5F);

		defaultStockModel[29].addShapeBox(0F, 0F, 0F, 2, 1, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F); // Box 52
		defaultStockModel[29].setRotationPoint(-22F, -9F, -2.5F);

		defaultStockModel[30].addShapeBox(0F, 0F, 0F, 2, 7, 5, 0F, -0.5F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, -1.5F, -0.5F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, -1.5F); // Box 52
		defaultStockModel[30].setRotationPoint(-24F, -15F, -2.5F);

		defaultStockModel[31].addShapeBox(0F, 0F, 0F, 2, 7, 5, 0F, 0F, 0F, 0F, -0.5F, 0F, -1.5F, -0.5F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, -1.5F, -0.5F, 0F, -1.5F, 0F, 0F, 0F); // Box 52
		defaultStockModel[31].setRotationPoint(-20F, -15F, -2.5F);

		defaultStockModel[32].addShapeBox(0F, 0F, 0F, 2, 2, 5, 0F, 0F, 0F, 0F, -0.5F, 0F, -1.5F, -0.5F, 0F, -1.5F, 0F, 0F, 0F, 2F, 0F, 0F, -4F, 1.5F, -1.5F, -4F, 1.5F, -1.5F, 2F, 0F, 0F); // Box 52
		defaultStockModel[32].setRotationPoint(-20F, -8F, -2.5F);

		defaultStockModel[33].addBox(0F, 0F, 0F, 2, 6, 5, 0F); // Box 52
		defaultStockModel[33].setRotationPoint(-22F, -15F, -2.5F);

		defaultStockModel[34].addShapeBox(0F, 0F, 0F, 5, 3, 9, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 52
		defaultStockModel[34].setRotationPoint(-42F, -23F, -4.5F);

		defaultStockModel[35].addShapeBox(0F, 0F, 0F, 5, 2, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 52
		defaultStockModel[35].setRotationPoint(-42F, -2F, -5F);

		defaultStockModel[36].addShapeBox(0F, 0F, 0F, 5, 11, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 52
		defaultStockModel[36].setRotationPoint(-42F, -20F, -4.5F);

		defaultStockModel[37].addBox(0F, 0F, 0F, 5, 4, 1, 0F); // Box 52
		defaultStockModel[37].setRotationPoint(-42F, -9F, -6.5F);

		defaultStockModel[38].addShapeBox(0F, 0F, 0F, 5, 3, 1, 0F, 0F, 0F, 1.5F, 0F, 0F, 1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 52
		defaultStockModel[38].setRotationPoint(-42F, -5F, -5F);

		defaultStockModel[39].addShapeBox(0F, 0F, 0F, 5, 3, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1.5F, 0F, 0F, 1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F); // Box 52
		defaultStockModel[39].setRotationPoint(-42F, -5F, 5.5F);

		defaultStockModel[40].addBox(0F, 0F, 0F, 5, 4, 1, 0F); // Box 52
		defaultStockModel[40].setRotationPoint(-42F, -9F, 5.5F);

		defaultStockModel[41].addShapeBox(0F, 0F, 0F, 5, 11, 1, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 52
		defaultStockModel[41].setRotationPoint(-42F, -20F, 5.5F);

		defaultStockModel[42].addBox(0F, 0F, 0F, 2, 2, 1, 0F); // Box 52
		defaultStockModel[42].setRotationPoint(-22F, -10F, -3.5F);

		defaultStockModel[43].addShapeBox(0F, 0F, 0F, 2, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 52
		defaultStockModel[43].setRotationPoint(-22F, -10F, -4.5F);

		defaultStockModel[44].addBox(0F, 0F, 0F, 2, 5, 1, 0F); // Box 52
		defaultStockModel[44].setRotationPoint(-22F, -17F, -6F);

		defaultStockModel[45].addShapeBox(0F, 0F, 0F, 2, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 1.5F, 0F, 0F, 1.5F); // Box 52
		defaultStockModel[45].setRotationPoint(-22F, -12F, -6F);

		defaultStockModel[46].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 52
		defaultStockModel[46].setRotationPoint(-22F, -18F, -6F);


		defaultGripModel = new ModelRendererTurbo[6];
		defaultGripModel[0] = new ModelRendererTurbo(this, 165, 141, textureX, textureY); // foreGrip5
		defaultGripModel[1] = new ModelRendererTurbo(this, 165, 153, textureX, textureY); // foreGrip5
		defaultGripModel[2] = new ModelRendererTurbo(this, 140, 141, textureX, textureY); // foreGrip5
		defaultGripModel[3] = new ModelRendererTurbo(this, 206, 155, textureX, textureY); // foreGrip5
		defaultGripModel[4] = new ModelRendererTurbo(this, 199, 125, textureX, textureY); // foreGrip5
		defaultGripModel[5] = new ModelRendererTurbo(this, 206, 143, textureX, textureY); // foreGrip5

		defaultGripModel[0].addBox(0F, 0F, 0F, 12, 3, 8, 0F); // foreGrip5
		defaultGripModel[0].setRotationPoint(33.5F, -10.5F, -4F);

		defaultGripModel[1].addShapeBox(0F, 0F, 0F, 12, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // foreGrip5
		defaultGripModel[1].setRotationPoint(33.5F, -7.5F, -4F);

		defaultGripModel[2].addBox(0F, 0F, 0F, 6, 14, 6, 0F); // foreGrip5
		defaultGripModel[2].setRotationPoint(34.5F, -5.5F, -3F);

		defaultGripModel[3].addShapeBox(0F, 0F, 0F, 2, 2, 6, 0F, 0F, 0F, 0F, 2F, 0F, -2F, 2F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F); // foreGrip5
		defaultGripModel[3].setRotationPoint(40.5F, -5.5F, -3F);

		defaultGripModel[4].addShapeBox(0F, 0F, 0F, 2, 9, 6, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, -2F, 1F, 0F, -2F, 0F, 0F, 0F); // foreGrip5
		defaultGripModel[4].setRotationPoint(40.5F, -3.5F, -3F);

		defaultGripModel[5].addShapeBox(0F, 0F, 0F, 3, 3, 6, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F); // foreGrip5
		defaultGripModel[5].setRotationPoint(40.5F, 5.5F, -3F);


		ammoModel = new ModelRendererTurbo[15];
		ammoModel[0] = new ModelRendererTurbo(this, 54, 50, textureX, textureY); // gasCanister1
		ammoModel[1] = new ModelRendererTurbo(this, 108, 50, textureX, textureY); // gasCanister2
		ammoModel[2] = new ModelRendererTurbo(this, 81, 50, textureX, textureY); // gasCanister3
		ammoModel[3] = new ModelRendererTurbo(this, 110, 75, textureX, textureY); // gasCanister4
		ammoModel[4] = new ModelRendererTurbo(this, 110, 75, textureX, textureY); // gasCanister4
		ammoModel[5] = new ModelRendererTurbo(this, 77, 75, textureX, textureY); // gasCanister5
		ammoModel[6] = new ModelRendererTurbo(this, 110, 103, textureX, textureY); // gasCanister6
		ammoModel[7] = new ModelRendererTurbo(this, 110, 103, textureX, textureY); // gasCanister6
		ammoModel[8] = new ModelRendererTurbo(this, 78, 103, textureX, textureY); // gasCanister7
		ammoModel[9] = new ModelRendererTurbo(this, 110, 90, textureX, textureY); // gasCanister8
		ammoModel[10] = new ModelRendererTurbo(this, 110, 90, textureX, textureY); // gasCanister8
		ammoModel[11] = new ModelRendererTurbo(this, 77, 90, textureX, textureY); // gasCanister9
		ammoModel[12] = new ModelRendererTurbo(this, 63, 39, textureX, textureY); // gasNozzle1
		ammoModel[13] = new ModelRendererTurbo(this, 63, 39, textureX, textureY); // gasNozzle1
		ammoModel[14] = new ModelRendererTurbo(this, 63, 39, textureX, textureY); // gasNozzle1

		ammoModel[0].addShapeBox(-5F, 6F, 0F, 3, 14, 10, 0F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F); // gasCanister1
		ammoModel[0].setRotationPoint(21F, -9F, -5F);
		ammoModel[0].rotateAngleZ = -0.2443461F;

		ammoModel[1].addBox(-2F, 6F, 0F, 4, 14, 10, 0F); // gasCanister2
		ammoModel[1].setRotationPoint(21F, -9F, -5F);
		ammoModel[1].rotateAngleZ = -0.2443461F;

		ammoModel[2].addShapeBox(2F, 6F, 0F, 3, 14, 10, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F); // gasCanister3
		ammoModel[2].setRotationPoint(21F, -9F, -5F);
		ammoModel[2].rotateAngleZ = -0.2443461F;

		ammoModel[3].addShapeBox(-5.5F, 6F, 0F, 3, 3, 11, 0F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F); // gasCanister4
		ammoModel[3].setRotationPoint(21F, -9F, -5.5F);
		ammoModel[3].rotateAngleZ = -0.2443461F;

		ammoModel[4].addShapeBox(2.5F, 6F, 0F, 3, 3, 11, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F); // gasCanister4
		ammoModel[4].setRotationPoint(21F, -9F, -5.5F);
		ammoModel[4].rotateAngleZ = -0.2443461F;

		ammoModel[5].addBox(-2.5F, 6F, 0F, 5, 3, 11, 0F); // gasCanister5
		ammoModel[5].setRotationPoint(21F, -9F, -5.5F);
		ammoModel[5].rotateAngleZ = -0.2443461F;

		ammoModel[6].addShapeBox(-5F, 20F, 0F, 3, 1, 10, 0F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, -1F, 0F, -3F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, -1F, 0F, -3F); // gasCanister6
		ammoModel[6].setRotationPoint(21F, -9F, -5F);
		ammoModel[6].rotateAngleZ = -0.2443461F;

		ammoModel[7].addShapeBox(2F, 20F, 0F, 3, 1, 10, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, -1.5F, -1F, 0F, -3F, -1F, 0F, -3F, 0F, 0F, -1.5F); // gasCanister6
		ammoModel[7].setRotationPoint(21F, -9F, -5F);
		ammoModel[7].rotateAngleZ = -0.2443461F;

		ammoModel[8].addShapeBox(-2F, 20F, 0F, 4, 1, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F); // gasCanister7
		ammoModel[8].setRotationPoint(21F, -9F, -5F);
		ammoModel[8].rotateAngleZ = -0.2443461F;

		ammoModel[9].addShapeBox(-5F, 4F, 0F, 3, 2, 10, 0F, -1F, 0F, -3F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, -1F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F); // gasCanister8
		ammoModel[9].setRotationPoint(21F, -9F, -5F);
		ammoModel[9].rotateAngleZ = -0.2443461F;

		ammoModel[10].addShapeBox(2F, 4F, 0F, 3, 2, 10, 0F, 0F, 0F, -1.5F, -1F, 0F, -3F, -1F, 0F, -3F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F); // gasCanister8
		ammoModel[10].setRotationPoint(21F, -9F, -5F);
		ammoModel[10].rotateAngleZ = -0.2443461F;

		ammoModel[11].addShapeBox(-2F, 4F, 0F, 4, 2, 10, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // gasCanister9
		ammoModel[11].setRotationPoint(21F, -9F, -5F);
		ammoModel[11].rotateAngleZ = -0.2443461F;

		ammoModel[12].addShapeBox(-3F, 0F, 0F, 2, 4, 6, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F); // gasNozzle1
		ammoModel[12].setRotationPoint(21F, -9F, -3F);
		ammoModel[12].rotateAngleZ = -0.2443461F;

		ammoModel[13].addBox(-1F, 0F, 0F, 2, 4, 6, 0F); // gasNozzle1
		ammoModel[13].setRotationPoint(21F, -9F, -3F);
		ammoModel[13].rotateAngleZ = -0.2443461F;

		ammoModel[14].addShapeBox(1F, 0F, 0F, 2, 4, 6, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F); // gasNozzle1
		ammoModel[14].setRotationPoint(21F, -9F, -3F);
		ammoModel[14].rotateAngleZ = -0.2443461F;

		barrelAttachPoint = new Vector3f(31F /16F, 17.5F /16F, 0F /16F);
		stockAttachPoint = new Vector3f(-14F /16F, 17F /16F, 0F /16F);
		scopeAttachPoint = new Vector3f(10F /16F, 22F /16F, 0F /16F);
		gripAttachPoint = new Vector3f(39 /16F, 10F /16F, 0F /16F);


		gunSlideDistance = 0F;
		animationType = EnumAnimationType.BOTTOM_CLIP;


		translateAll(0F, 0F, 0F);


		flipAll();
	}
}