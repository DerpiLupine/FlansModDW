package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelMatriarchSnubShort extends ModelGun
{
	int textureX = 512;
	int textureY = 256;

	public ModelMatriarchSnubShort()
	{
		gunModel = new ModelRendererTurbo[36];
		gunModel[0] = new ModelRendererTurbo(this, 56, 61, textureX, textureY); // barrelFront
		gunModel[1] = new ModelRendererTurbo(this, 56, 61, textureX, textureY); // barrelFront2
		gunModel[2] = new ModelRendererTurbo(this, 81, 61, textureX, textureY); // barrelFront3
		gunModel[3] = new ModelRendererTurbo(this, 56, 28, textureX, textureY); // barrelRail
		gunModel[4] = new ModelRendererTurbo(this, 56, 82, textureX, textureY); // body2
		gunModel[5] = new ModelRendererTurbo(this, 56, 71, textureX, textureY); // body3
		gunModel[6] = new ModelRendererTurbo(this, 83, 97, textureX, textureY); // body4
		gunModel[7] = new ModelRendererTurbo(this, 83, 97, textureX, textureY); // body4-2
		gunModel[8] = new ModelRendererTurbo(this, 83, 90, textureX, textureY); // body5
		gunModel[9] = new ModelRendererTurbo(this, 83, 90, textureX, textureY); // body5-2
		gunModel[10] = new ModelRendererTurbo(this, 56, 45, textureX, textureY); // body6
		gunModel[11] = new ModelRendererTurbo(this, 56, 53, textureX, textureY); // body7
		gunModel[12] = new ModelRendererTurbo(this, 79, 71, textureX, textureY); // body9
		gunModel[13] = new ModelRendererTurbo(this, 125, 67, textureX, textureY); // cover1-2
		gunModel[14] = new ModelRendererTurbo(this, 125, 67, textureX, textureY); // cover1
		gunModel[15] = new ModelRendererTurbo(this, 56, 103, textureX, textureY); // grip1
		gunModel[16] = new ModelRendererTurbo(this, 1, 102, textureX, textureY); // grip2
		gunModel[17] = new ModelRendererTurbo(this, 1, 82, textureX, textureY); // grip3
		gunModel[18] = new ModelRendererTurbo(this, 1, 92, textureX, textureY); // grip4
		gunModel[19] = new ModelRendererTurbo(this, 1, 61, textureX, textureY); // grip5
		gunModel[20] = new ModelRendererTurbo(this, 92, 91, textureX, textureY); // hammer1
		gunModel[21] = new ModelRendererTurbo(this, 92, 97, textureX, textureY); // hammer2
		gunModel[22] = new ModelRendererTurbo(this, 47, 28, textureX, textureY); // ironSight1
		gunModel[23] = new ModelRendererTurbo(this, 47, 28, textureX, textureY); // ironSight1-2
		gunModel[24] = new ModelRendererTurbo(this, 56, 19, textureX, textureY); // mainBarrelBottom
		gunModel[25] = new ModelRendererTurbo(this, 56, 10, textureX, textureY); // mainBarrelMiddle
		gunModel[26] = new ModelRendererTurbo(this, 56, 1, textureX, textureY); // mainBarrelTop
		gunModel[27] = new ModelRendererTurbo(this, 36, 29, textureX, textureY); // Box 3
		gunModel[28] = new ModelRendererTurbo(this, 36, 36, textureX, textureY); // Box 4
		gunModel[29] = new ModelRendererTurbo(this, 104, 62, textureX, textureY); // Box 0
		gunModel[30] = new ModelRendererTurbo(this, 81, 61, textureX, textureY); // Box 1
		gunModel[31] = new ModelRendererTurbo(this, 56, 61, textureX, textureY); // Box 2
		gunModel[32] = new ModelRendererTurbo(this, 81, 61, textureX, textureY); // Box 3
		gunModel[33] = new ModelRendererTurbo(this, 99, 1, textureX, textureY); // Box 4
		gunModel[34] = new ModelRendererTurbo(this, 99, 10, textureX, textureY); // Box 5
		gunModel[35] = new ModelRendererTurbo(this, 99, 19, textureX, textureY); // Box 6

		gunModel[0].addShapeBox(0F, 0F, 0F, 5, 2, 7, 0F,0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // barrelFront
		gunModel[0].setRotationPoint(0F, -16F, -3.5F);

		gunModel[1].addBox(0F, 0F, 0F, 5, 2, 7, 0F); // barrelFront2
		gunModel[1].setRotationPoint(0F, -14F, -3.5F);

		gunModel[2].addShapeBox(0F, 0F, 0F, 5, 1, 7, 0F,0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F); // barrelFront3
		gunModel[2].setRotationPoint(0F, -11.5F, -3.5F);

		gunModel[3].addBox(0F, 0F, 0F, 15, 1, 2, 0F); // barrelRail
		gunModel[3].setRotationPoint(5F, -17F, -1F);

		gunModel[4].addBox(0F, 0F, 0F, 7, 14, 6, 0F); // body2
		gunModel[4].setRotationPoint(-18F, -15F, -3F);

		gunModel[5].addBox(0F, 0F, 0F, 5, 4, 6, 0F); // body3
		gunModel[5].setRotationPoint(-23F, -8F, -3F);

		gunModel[6].addShapeBox(0F, 0F, 0F, 2, 3, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F); // body4
		gunModel[6].setRotationPoint(-20F, -11F, 1F);

		gunModel[7].addShapeBox(0F, 0F, 0F, 2, 3, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F); // body4-2
		gunModel[7].setRotationPoint(-20F, -11F, -3F);

		gunModel[8].addShapeBox(0F, 0F, 0F, 1, 4, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F); // body5
		gunModel[8].setRotationPoint(-19F, -15F, 1F);

		gunModel[9].addShapeBox(0F, 0F, 0F, 1, 4, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F); // body5-2
		gunModel[9].setRotationPoint(-19F, -15F, -3F);

		gunModel[10].addShapeBox(0F, 0F, 0F, 24, 1, 6, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body6
		gunModel[10].setRotationPoint(-19F, -17F, -3F);

		gunModel[11].addBox(0F, 0F, 0F, 19, 1, 6, 0F); // body7
		gunModel[11].setRotationPoint(-19F, -16F, -3F);

		gunModel[12].addShapeBox(0F, 0F, 0F, 16, 2, 6, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F); // body9
		gunModel[12].setRotationPoint(-11F, -3F, -3F);

		gunModel[13].addShapeBox(0F, 0F, 0F, 3, 10, 2, 0F,0F, -2F, -2F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, -2F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, -2F, 0F); // cover1-2
		gunModel[13].setRotationPoint(-14F, -14F, -5F);

		gunModel[14].addShapeBox(0F, 0F, 0F, 3, 10, 2, 0F,0F, -2F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, -2F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, -2F); // cover1
		gunModel[14].setRotationPoint(-14F, -14F, 3F);

		gunModel[15].addBox(0F, 0F, 0F, 6, 3, 7, 0F); // grip1
		gunModel[15].setRotationPoint(-23.5F, -7F, -3.5F);

		gunModel[16].addShapeBox(0F, 0F, 0F, 10, 3, 7, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F); // grip2
		gunModel[16].setRotationPoint(-23.5F, -4F, -3.5F);

		gunModel[17].addShapeBox(0F, 0F, 0F, 14, 2, 7, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 2F, 0F, 0F); // grip3
		gunModel[17].setRotationPoint(-27.5F, -1F, -3.5F);

		gunModel[18].addShapeBox(0F, 0F, 0F, 13, 2, 7, 0F,-1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F); // grip4
		gunModel[18].setRotationPoint(-30.5F, 1F, -3.5F);

		gunModel[19].addShapeBox(0F, 0F, 0F, 12, 13, 7, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F); // grip5
		gunModel[19].setRotationPoint(-30.5F, 3F, -3.5F);

		gunModel[20].addBox(0F, 0F, 0F, 2, 3, 2, 0F); // hammer1
		gunModel[20].setRotationPoint(-20F, -11F, -1F);

		gunModel[21].addShapeBox(0F, 0F, 0F, 5, 3, 2, 0F,0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F, -3F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, -3F, 0F); // hammer2
		gunModel[21].setRotationPoint(-23F, -12F, -1F);

		gunModel[22].addBox(0F, 0F, 0F, 2, 3, 2, 0F); // ironSight1
		gunModel[22].setRotationPoint(-17.9F, -19F, 0.5F);

		gunModel[23].addBox(0F, 0F, 0F, 2, 3, 2, 0F); // ironSight1-2
		gunModel[23].setRotationPoint(-17.9F, -19F, -2.5F);

		gunModel[24].addShapeBox(0F, 0F, 0F, 15, 2, 6, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // mainBarrelBottom
		gunModel[24].setRotationPoint(5F, -12F, -3F);

		gunModel[25].addBox(0F, 0F, 0F, 15, 2, 6, 0F); // mainBarrelMiddle
		gunModel[25].setRotationPoint(5F, -14F, -3F);

		gunModel[26].addShapeBox(0F, 0F, 0F, 15, 2, 6, 0F,0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // mainBarrelTop
		gunModel[26].setRotationPoint(5F, -16F, -3F);

		gunModel[27].addShapeBox(0F, 0F, 0F, 4, 3, 1, 0F,-1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 3
		gunModel[27].setRotationPoint(16F, -20F, -0.5F);

		gunModel[28].addBox(0F, 0F, 0F, 5, 1, 3, 0F); // Box 4
		gunModel[28].setRotationPoint(-17.9F, -17.5F, -1.5F);

		gunModel[29].addShapeBox(0F, 0F, 0F, 5, 1, 6, 0F,0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F); // Box 0
		gunModel[29].setRotationPoint(0F, -4.5F, -3F);

		gunModel[30].addShapeBox(0F, 0F, 0F, 5, 1, 7, 0F,0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F); // Box 1
		gunModel[30].setRotationPoint(0F, -5.5F, -3.5F);

		gunModel[31].addBox(0F, 0F, 0F, 5, 2, 7, 0F); // Box 2
		gunModel[31].setRotationPoint(0F, -8F, -3.5F);

		gunModel[32].addShapeBox(0F, 0F, 0F, 5, 1, 7, 0F,0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F); // Box 3
		gunModel[32].setRotationPoint(0F, -9.5F, -3.5F);

		gunModel[33].addShapeBox(0F, 0F, 0F, 15, 2, 6, 0F,0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 4
		gunModel[33].setRotationPoint(5F, -10F, -3F);

		gunModel[34].addBox(0F, 0F, 0F, 15, 2, 6, 0F); // Box 5
		gunModel[34].setRotationPoint(5F, -8F, -3F);

		gunModel[35].addShapeBox(0F, 0F, 0F, 15, 2, 6, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 6
		gunModel[35].setRotationPoint(5F, -6F, -3F);


		ammoModel = new ModelRendererTurbo[18];
		ammoModel[0] = new ModelRendererTurbo(this, 1, 46, textureX, textureY); // bullet1
		ammoModel[1] = new ModelRendererTurbo(this, 1, 46, textureX, textureY); // bullet1-2
		ammoModel[2] = new ModelRendererTurbo(this, 1, 46, textureX, textureY); // bullet1-3
		ammoModel[3] = new ModelRendererTurbo(this, 26, 46, textureX, textureY); // bullet2
		ammoModel[4] = new ModelRendererTurbo(this, 26, 46, textureX, textureY); // bullet2-2
		ammoModel[5] = new ModelRendererTurbo(this, 26, 46, textureX, textureY); // bullet2-3
		ammoModel[6] = new ModelRendererTurbo(this, 1, 51, textureX, textureY); // bullet3
		ammoModel[7] = new ModelRendererTurbo(this, 1, 51, textureX, textureY); // bullet3-2
		ammoModel[8] = new ModelRendererTurbo(this, 1, 51, textureX, textureY); // bullet3-3
		ammoModel[9] = new ModelRendererTurbo(this, 26, 51, textureX, textureY); // bullet4
		ammoModel[10] = new ModelRendererTurbo(this, 26, 51, textureX, textureY); // bullet4-2
		ammoModel[11] = new ModelRendererTurbo(this, 26, 51, textureX, textureY); // bullet4-3
		ammoModel[12] = new ModelRendererTurbo(this, 1, 56, textureX, textureY); // bullet5
		ammoModel[13] = new ModelRendererTurbo(this, 1, 56, textureX, textureY); // bullet5-2
		ammoModel[14] = new ModelRendererTurbo(this, 1, 56, textureX, textureY); // bullet5-3
		ammoModel[15] = new ModelRendererTurbo(this, 26, 56, textureX, textureY); // bullet6
		ammoModel[16] = new ModelRendererTurbo(this, 26, 56, textureX, textureY); // bullet6-2
		ammoModel[17] = new ModelRendererTurbo(this, 26, 56, textureX, textureY); // bullet6-3

		ammoModel[0].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // bullet1
		ammoModel[0].setRotationPoint(-10.7F, -14.5F, -1.5F);

		ammoModel[1].addBox(0F, 0F, 0F, 9, 1, 3, 0F); // bullet1-2
		ammoModel[1].setRotationPoint(-10.7F, -13.5F, -1.5F);

		ammoModel[2].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // bullet1-3
		ammoModel[2].setRotationPoint(-10.7F, -12.5F, -1.5F);

		ammoModel[3].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // bullet2
		ammoModel[3].setRotationPoint(-10.7F, -12.5F, -5F);

		ammoModel[4].addBox(0F, 0F, 0F, 9, 1, 3, 0F); // bullet2-2
		ammoModel[4].setRotationPoint(-10.7F, -11.5F, -5F);

		ammoModel[5].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // bullet2-3
		ammoModel[5].setRotationPoint(-10.7F, -10.5F, -5F);

		ammoModel[6].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // bullet3
		ammoModel[6].setRotationPoint(-10.7F, -8.5F, -5F);

		ammoModel[7].addBox(0F, 0F, 0F, 9, 1, 3, 0F); // bullet3-2
		ammoModel[7].setRotationPoint(-10.7F, -7.5F, -5F);

		ammoModel[8].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // bullet3-3
		ammoModel[8].setRotationPoint(-10.7F, -6.5F, -5F);

		ammoModel[9].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // bullet4
		ammoModel[9].setRotationPoint(-10.7F, -6.5F, -1.5F);

		ammoModel[10].addBox(0F, 0F, 0F, 9, 1, 3, 0F); // bullet4-2
		ammoModel[10].setRotationPoint(-10.7F, -5.5F, -1.5F);

		ammoModel[11].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // bullet4-3
		ammoModel[11].setRotationPoint(-10.7F, -4.5F, -1.5F);

		ammoModel[12].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // bullet5
		ammoModel[12].setRotationPoint(-10.7F, -8.5F, 2F);

		ammoModel[13].addBox(0F, 0F, 0F, 9, 1, 3, 0F); // bullet5-2
		ammoModel[13].setRotationPoint(-10.7F, -7.5F, 2F);

		ammoModel[14].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // bullet5-3
		ammoModel[14].setRotationPoint(-10.7F, -6.5F, 2F);

		ammoModel[15].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // bullet6
		ammoModel[15].setRotationPoint(-10.7F, -12.5F, 2F);

		ammoModel[16].addBox(0F, 0F, 0F, 9, 1, 3, 0F); // bullet6-2
		ammoModel[16].setRotationPoint(-10.7F, -11.5F, 2F);

		ammoModel[17].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // bullet6-3
		ammoModel[17].setRotationPoint(-10.7F, -10.5F, 2F);


		revolverBarrelModel = new ModelRendererTurbo[5];
		revolverBarrelModel[0] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // canister1
		revolverBarrelModel[1] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // canister1
		revolverBarrelModel[2] = new ModelRendererTurbo(this, 1, 14, textureX, textureY); // canister1
		revolverBarrelModel[3] = new ModelRendererTurbo(this, 1, 14, textureX, textureY); // canister1
		revolverBarrelModel[4] = new ModelRendererTurbo(this, 1, 29, textureX, textureY); // canister1

		revolverBarrelModel[0].addShapeBox(0F, 0F, 0F, 10, 2, 10, 0F,0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // canister1
		revolverBarrelModel[0].setRotationPoint(-10.5F, -15F, -5F);

		revolverBarrelModel[1].addShapeBox(0F, 0F, 0F, 10, 2, 10, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // canister1
		revolverBarrelModel[1].setRotationPoint(-10.5F, -5F, -5F);

		revolverBarrelModel[2].addShapeBox(0F, 0F, 0F, 10, 2, 12, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // canister1
		revolverBarrelModel[2].setRotationPoint(-10.5F, -13F, -6F);

		revolverBarrelModel[3].addShapeBox(0F, 0F, 0F, 10, 2, 12, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // canister1
		revolverBarrelModel[3].setRotationPoint(-10.5F, -7F, -6F);

		revolverBarrelModel[4].addBox(0F, 0F, 0F, 10, 4, 12, 0F); // canister1
		revolverBarrelModel[4].setRotationPoint(-10.5F, -11F, -6F);



		barrelAttachPoint = new Vector3f(0F /16F, 0F /16F, 0F /16F);
		stockAttachPoint = new Vector3f(0F /16F, 0F /16F, 0F /16F);
		scopeAttachPoint = new Vector3f(0F /16F, 0F /16F, 0F /16F);
		gripAttachPoint = new Vector3f(0 /16F, 0F /16F, 0F /16F);

		gunSlideDistance = 0F;
		animationType = EnumAnimationType.REVOLVER;

		revolverFlipAngle = -20F;


		translateAll(16F, -10F, 0F);


		flipAll();
	}
}