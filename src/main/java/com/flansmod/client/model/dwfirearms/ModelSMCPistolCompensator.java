package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.ModelAttachment;
import com.flansmod.client.tmt.ModelRendererTurbo;

import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelSMCPistolCompensator extends ModelAttachment
{
	int textureX = 512;
	int textureY = 256;

	public ModelSMCPistolCompensator()
	{
		attachmentModel = new ModelRendererTurbo[7];
		attachmentModel[0] = new ModelRendererTurbo(this, 96, 19, textureX, textureY); // Box 0
		attachmentModel[1] = new ModelRendererTurbo(this, 96, 1, textureX, textureY); // Box 1
		attachmentModel[2] = new ModelRendererTurbo(this, 95, 32, textureX, textureY); // Box 2
		attachmentModel[3] = new ModelRendererTurbo(this, 147, 19, textureX, textureY); // Box 3
		attachmentModel[4] = new ModelRendererTurbo(this, 147, 1, textureX, textureY); // Box 4
		attachmentModel[5] = new ModelRendererTurbo(this, 147, 10, textureX, textureY); // Box 5
		attachmentModel[6] = new ModelRendererTurbo(this, 95, 46, textureX, textureY); // Box 6

		attachmentModel[0].addShapeBox(0F, -5F, -5F, 15, 2, 10, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 0
		attachmentModel[0].setRotationPoint(0F, 0F, 0F);

		attachmentModel[1].addBox(0F, -3F, -5F, 15, 7, 10, 0F); // Box 1
		attachmentModel[1].setRotationPoint(0F, 0F, 0F);

		attachmentModel[2].addShapeBox(0F, 4F, -5F, 15, 3, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 2
		attachmentModel[2].setRotationPoint(0F, 0F, 0F);

		attachmentModel[3].addShapeBox(15F, -3F, 1F, 1, 6, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F); // Box 3
		attachmentModel[3].setRotationPoint(0F, 0F, 0F);

		attachmentModel[4].addBox(15F, -3F, -1F, 1, 6, 2, 0F); // Box 4
		attachmentModel[4].setRotationPoint(0F, 0F, 0F);

		attachmentModel[5].addShapeBox(15F, -3F, -3F, 1, 6, 2, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 5
		attachmentModel[5].setRotationPoint(0F, 0F, 0F);

		attachmentModel[6].addShapeBox(0F, 7F, -3F, 15, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 6
		attachmentModel[6].setRotationPoint(0F, 0F, 0F);

		renderOffset = 0F;

		flipAll();
	}
}