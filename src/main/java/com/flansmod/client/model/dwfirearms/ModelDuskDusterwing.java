package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelDuskDusterwing extends ModelGun
{
	int textureX = 512;
	int textureY = 256;

	public ModelDuskDusterwing()
	{
		gunModel = new ModelRendererTurbo[104];
		gunModel[0] = new ModelRendererTurbo(this, 1, 71, textureX, textureY); // Box 33
		gunModel[1] = new ModelRendererTurbo(this, 1, 28, textureX, textureY); // Box 34
		gunModel[2] = new ModelRendererTurbo(this, 1, 40, textureX, textureY); // Box 35
		gunModel[3] = new ModelRendererTurbo(this, 1, 54, textureX, textureY); // Box 36
		gunModel[4] = new ModelRendererTurbo(this, 57, 54, textureX, textureY); // Box 38
		gunModel[5] = new ModelRendererTurbo(this, 36, 28, textureX, textureY); // Box 39
		gunModel[6] = new ModelRendererTurbo(this, 36, 40, textureX, textureY); // Box 40
		gunModel[7] = new ModelRendererTurbo(this, 36, 54, textureX, textureY); // Box 41
		gunModel[8] = new ModelRendererTurbo(this, 24, 83, textureX, textureY); // Box 42
		gunModel[9] = new ModelRendererTurbo(this, 57, 28, textureX, textureY); // Box 0
		gunModel[10] = new ModelRendererTurbo(this, 57, 40, textureX, textureY); // Box 1
		gunModel[11] = new ModelRendererTurbo(this, 57, 40, textureX, textureY); // Box 3
		gunModel[12] = new ModelRendererTurbo(this, 91, 81, textureX, textureY); // Box 5
		gunModel[13] = new ModelRendererTurbo(this, 91, 1, textureX, textureY); // Box 6
		gunModel[14] = new ModelRendererTurbo(this, 91, 41, textureX, textureY); // Box 7
		gunModel[15] = new ModelRendererTurbo(this, 118, 41, textureX, textureY); // Box 9
		gunModel[16] = new ModelRendererTurbo(this, 57, 28, textureX, textureY); // Box 10
		gunModel[17] = new ModelRendererTurbo(this, 1, 83, textureX, textureY); // Box 11
		gunModel[18] = new ModelRendererTurbo(this, 120, 28, textureX, textureY); // Box 12
		gunModel[19] = new ModelRendererTurbo(this, 91, 28, textureX, textureY); // Box 13
		gunModel[20] = new ModelRendererTurbo(this, 91, 17, textureX, textureY); // Box 14
		gunModel[21] = new ModelRendererTurbo(this, 145, 28, textureX, textureY); // Box 15
		gunModel[22] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 16
		gunModel[23] = new ModelRendererTurbo(this, 152, 71, textureX, textureY); // Box 17
		gunModel[24] = new ModelRendererTurbo(this, 142, 81, textureX, textureY); // Box 18
		gunModel[25] = new ModelRendererTurbo(this, 178, 29, textureX, textureY); // Box 19
		gunModel[26] = new ModelRendererTurbo(this, 62, 16, textureX, textureY); // Box 20
		gunModel[27] = new ModelRendererTurbo(this, 157, 45, textureX, textureY); // Box 21
		gunModel[28] = new ModelRendererTurbo(this, 45, 85, textureX, textureY); // Box 22
		gunModel[29] = new ModelRendererTurbo(this, 45, 76, textureX, textureY); // Box 23
		gunModel[30] = new ModelRendererTurbo(this, 91, 55, textureX, textureY); // Box 24
		gunModel[31] = new ModelRendererTurbo(this, 91, 71, textureX, textureY); // Box 25
		gunModel[32] = new ModelRendererTurbo(this, 187, 8, textureX, textureY); // Box 26
		gunModel[33] = new ModelRendererTurbo(this, 187, 1, textureX, textureY); // Box 27
		gunModel[34] = new ModelRendererTurbo(this, 187, 1, textureX, textureY); // Box 28
		gunModel[35] = new ModelRendererTurbo(this, 172, 1, textureX, textureY); // Box 33
		gunModel[36] = new ModelRendererTurbo(this, 172, 8, textureX, textureY); // Box 34
		gunModel[37] = new ModelRendererTurbo(this, 172, 1, textureX, textureY); // Box 35
		gunModel[38] = new ModelRendererTurbo(this, 62, 16, textureX, textureY); // Box 40
		gunModel[39] = new ModelRendererTurbo(this, 201, 29, textureX, textureY); // Box 41
		gunModel[40] = new ModelRendererTurbo(this, 62, 16, textureX, textureY); // Box 42
		gunModel[41] = new ModelRendererTurbo(this, 45, 17, textureX, textureY); // Box 43
		gunModel[42] = new ModelRendererTurbo(this, 45, 17, textureX, textureY); // Box 44
		gunModel[43] = new ModelRendererTurbo(this, 45, 17, textureX, textureY); // Box 45
		gunModel[44] = new ModelRendererTurbo(this, 70, 1, textureX, textureY); // Box 46
		gunModel[45] = new ModelRendererTurbo(this, 70, 1, textureX, textureY); // Box 47
		gunModel[46] = new ModelRendererTurbo(this, 70, 1, textureX, textureY); // Box 48
		gunModel[47] = new ModelRendererTurbo(this, 70, 1, textureX, textureY); // Box 49
		gunModel[48] = new ModelRendererTurbo(this, 70, 1, textureX, textureY); // Box 52
		gunModel[49] = new ModelRendererTurbo(this, 154, 57, textureX, textureY); // Box 0
		gunModel[50] = new ModelRendererTurbo(this, 22, 19, textureX, textureY); // Box 1
		gunModel[51] = new ModelRendererTurbo(this, 22, 23, textureX, textureY); // Box 2
		gunModel[52] = new ModelRendererTurbo(this, 1, 14, textureX, textureY); // Box 3
		gunModel[53] = new ModelRendererTurbo(this, 197, 64, textureX, textureY); // Box 0
		gunModel[54] = new ModelRendererTurbo(this, 197, 64, textureX, textureY); // Box 1
		gunModel[55] = new ModelRendererTurbo(this, 197, 55, textureX, textureY); // Box 2
		gunModel[56] = new ModelRendererTurbo(this, 197, 55, textureX, textureY); // Box 3
		gunModel[57] = new ModelRendererTurbo(this, 242, 11, textureX, textureY); // Box 4
		gunModel[58] = new ModelRendererTurbo(this, 28, 106, textureX, textureY); // Box 23
		gunModel[59] = new ModelRendererTurbo(this, 1, 132, textureX, textureY); // Box 23
		gunModel[60] = new ModelRendererTurbo(this, 185, 87, textureX, textureY); // Box 24
		gunModel[61] = new ModelRendererTurbo(this, 202, 6, textureX, textureY); // Box 27
		gunModel[62] = new ModelRendererTurbo(this, 114, 185, textureX, textureY); // Box 0
		gunModel[63] = new ModelRendererTurbo(this, 1, 106, textureX, textureY); // Box 1
		gunModel[64] = new ModelRendererTurbo(this, 40, 132, textureX, textureY); // Box 2
		gunModel[65] = new ModelRendererTurbo(this, 1, 152, textureX, textureY); // Box 3
		gunModel[66] = new ModelRendererTurbo(this, 1, 143, textureX, textureY); // Box 4
		gunModel[67] = new ModelRendererTurbo(this, 1, 235, textureX, textureY); // Box 6
		gunModel[68] = new ModelRendererTurbo(this, 1, 118, textureX, textureY); // Box 8
		gunModel[69] = new ModelRendererTurbo(this, 32, 162, textureX, textureY); // Box 9
		gunModel[70] = new ModelRendererTurbo(this, 1, 188, textureX, textureY); // Box 10
		gunModel[71] = new ModelRendererTurbo(this, 28, 217, textureX, textureY); // Box 11
		gunModel[72] = new ModelRendererTurbo(this, 40, 189, textureX, textureY); // Box 12
		gunModel[73] = new ModelRendererTurbo(this, 59, 119, textureX, textureY); // Box 13
		gunModel[74] = new ModelRendererTurbo(this, 91, 168, textureX, textureY); // Box 14
		gunModel[75] = new ModelRendererTurbo(this, 32, 175, textureX, textureY); // Box 15
		gunModel[76] = new ModelRendererTurbo(this, 91, 148, textureX, textureY); // Box 21
		gunModel[77] = new ModelRendererTurbo(this, 57, 28, textureX, textureY); // Box 22
		gunModel[78] = new ModelRendererTurbo(this, 57, 28, textureX, textureY); // Box 23
		gunModel[79] = new ModelRendererTurbo(this, 91, 158, textureX, textureY); // Box 24
		gunModel[80] = new ModelRendererTurbo(this, 91, 185, textureX, textureY); // Box 27
		gunModel[81] = new ModelRendererTurbo(this, 91, 185, textureX, textureY); // Box 28
		gunModel[82] = new ModelRendererTurbo(this, 145, 185, textureX, textureY); // Box 29
		gunModel[83] = new ModelRendererTurbo(this, 91, 202, textureX, textureY); // Box 30
		gunModel[84] = new ModelRendererTurbo(this, 1, 162, textureX, textureY); // Box 31
		gunModel[85] = new ModelRendererTurbo(this, 28, 118, textureX, textureY); // Box 32
		gunModel[86] = new ModelRendererTurbo(this, 1, 210, textureX, textureY); // Box 33
		gunModel[87] = new ModelRendererTurbo(this, 28, 118, textureX, textureY); // Box 34
		gunModel[88] = new ModelRendererTurbo(this, 110, 173, textureX, textureY); // Box 35
		gunModel[89] = new ModelRendererTurbo(this, 91, 185, textureX, textureY); // Box 36
		gunModel[90] = new ModelRendererTurbo(this, 61, 217, textureX, textureY); // Box 37
		gunModel[91] = new ModelRendererTurbo(this, 91, 236, textureX, textureY); // Box 38
		gunModel[92] = new ModelRendererTurbo(this, 91, 218, textureX, textureY); // Box 39
		gunModel[93] = new ModelRendererTurbo(this, 91, 236, textureX, textureY); // Box 40
		gunModel[94] = new ModelRendererTurbo(this, 142, 165, textureX, textureY); // Box 41
		gunModel[95] = new ModelRendererTurbo(this, 155, 174, textureX, textureY); // Box 42
		gunModel[96] = new ModelRendererTurbo(this, 168, 148, textureX, textureY); // Box 43
		gunModel[97] = new ModelRendererTurbo(this, 155, 179, textureX, textureY); // Box 44
		gunModel[98] = new ModelRendererTurbo(this, 155, 165, textureX, textureY); // Box 45
		gunModel[99] = new ModelRendererTurbo(this, 155, 148, textureX, textureY); // Box 46
		gunModel[100] = new ModelRendererTurbo(this, 142, 148, textureX, textureY); // Box 47
		gunModel[101] = new ModelRendererTurbo(this, 168, 157, textureX, textureY); // Box 48
		gunModel[102] = new ModelRendererTurbo(this, 142, 174, textureX, textureY); // Box 49
		gunModel[103] = new ModelRendererTurbo(this, 168, 168, textureX, textureY); // Box 50

		gunModel[0].addBox(0F, 0F, 0F, 12, 3, 8, 0F); // Box 33
		gunModel[0].setRotationPoint(-8F, -10F, -4F);

		gunModel[1].addShapeBox(0F, 0F, 0F, 9, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 34
		gunModel[1].setRotationPoint(-9F, -7F, -4F);

		gunModel[2].addShapeBox(0F, 0F, 0F, 9, 5, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F); // Box 35
		gunModel[2].setRotationPoint(-9F, -4F, -4F);

		gunModel[3].addShapeBox(0F, 0F, 0F, 9, 8, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, -3F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 2F, -3F, 0F); // Box 36
		gunModel[3].setRotationPoint(-11F, 1F, -4F);

		gunModel[4].addShapeBox(0F, 0F, 0F, 8, 3, 8, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F); // Box 38
		gunModel[4].setRotationPoint(-13F, 9F, -4F);

		gunModel[5].addShapeBox(0F, 0F, 0F, 2, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F); // Box 39
		gunModel[5].setRotationPoint(0F, -7F, -4F);

		gunModel[6].addShapeBox(0F, 0F, 0F, 2, 5, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, -1.5F, -2F, 0F, -1.5F, 2F, 0F, 0F); // Box 40
		gunModel[6].setRotationPoint(0F, -4F, -4F);

		gunModel[7].addShapeBox(0F, 0F, 0F, 2, 8, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, -1.5F, -3F, 0F, -1.5F, 3F, 0F, 0F); // Box 41
		gunModel[7].setRotationPoint(-2F, 1F, -4F);

		gunModel[8].addShapeBox(0F, 0F, 0F, 2, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F); // Box 42
		gunModel[8].setRotationPoint(-5F, 9F, -4F);

		gunModel[9].addShapeBox(0F, 0F, 0F, 1, 3, 8, 0F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F); // Box 0
		gunModel[9].setRotationPoint(-10F, -7F, -4F);

		gunModel[10].addShapeBox(0F, 0F, 0F, 1, 5, 8, 0F, -2F, 0F, -1.5F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F); // Box 1
		gunModel[10].setRotationPoint(-12F, -4F, -4F);

		gunModel[11].addShapeBox(0F, 0F, 0F, 1, 5, 8, 0F, -2F, 0F, -1.5F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F); // Box 3
		gunModel[11].setRotationPoint(-14F, 1F, -4F);

		gunModel[12].addShapeBox(0F, 0F, 0F, 17, 4, 8, 0F, 3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 5
		gunModel[12].setRotationPoint(-13F, -14F, -4F);

		gunModel[13].addBox(0F, 0F, 0F, 31, 6, 9, 0F); // Box 6
		gunModel[13].setRotationPoint(-16F, -20F, -4.5F);

		gunModel[14].addShapeBox(0F, 0F, 0F, 4, 4, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F); // Box 7
		gunModel[14].setRotationPoint(1F, -14F, -4.5F);

		gunModel[15].addBox(0F, 0F, 0F, 10, 4, 9, 0F); // Box 9
		gunModel[15].setRotationPoint(5F, -14F, -4.5F);

		gunModel[16].addShapeBox(0F, 0F, 0F, 1, 3, 8, 0F, 2F, 0F, -1.5F, -2F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F); // Box 10
		gunModel[16].setRotationPoint(-10F, -10F, -4F);

		gunModel[17].addShapeBox(0F, 0F, 0F, 3, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F); // Box 11
		gunModel[17].setRotationPoint(-11F, -10F, -4F);

		gunModel[18].addShapeBox(0F, 0F, 0F, 4, 4, 8, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 12
		gunModel[18].setRotationPoint(-16F, -24F, -4F);

		gunModel[19].addBox(0F, 0F, 0F, 6, 4, 8, 0F); // Box 13
		gunModel[19].setRotationPoint(-12F, -24F, -4F);

		gunModel[20].addBox(0F, 0F, 0F, 27, 2, 8, 0F); // Box 14
		gunModel[20].setRotationPoint(-6F, -22F, -4F);

		gunModel[21].addBox(0F, 0F, 0F, 8, 4, 8, 0F); // Box 15
		gunModel[21].setRotationPoint(21F, -24F, -4F);

		gunModel[22].addBox(0F, 0F, 0F, 27, 2, 7, 0F); // Box 16
		gunModel[22].setRotationPoint(-6F, -24F, -3.5F);

		gunModel[23].addBox(0F, 0F, 0F, 14, 1, 8, 0F); // Box 17
		gunModel[23].setRotationPoint(15F, -14F, -4F);

		gunModel[24].addShapeBox(0F, 0F, 0F, 13, 4, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F); // Box 18
		gunModel[24].setRotationPoint(4F, -13F, -4F);

		gunModel[25].addBox(0F, 0F, 0F, 3, 3, 8, 0F); // Box 19
		gunModel[25].setRotationPoint(29F, -24F, -4F);

		gunModel[26].addShapeBox(0F, 0F, 0F, 1, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, 0F); // Box 20
		gunModel[26].setRotationPoint(36F, -24F, -4F);

		gunModel[27].addBox(0F, 0F, 0F, 22, 1, 8, 0F); // Box 21
		gunModel[27].setRotationPoint(29F, -21F, -4F);

		gunModel[28].addShapeBox(0F, 0F, 0F, 12, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2.5F, 0F, 0F); // Box 22
		gunModel[28].setRotationPoint(39F, -23F, -3.5F);

		gunModel[29].addShapeBox(0F, 0F, 0F, 12, 1, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 23
		gunModel[29].setRotationPoint(39F, -24F, -3.5F);

		gunModel[30].addBox(0F, 0F, 0F, 22, 6, 9, 0F); // Box 24
		gunModel[30].setRotationPoint(29F, -20F, -4.5F);

		gunModel[31].addBox(0F, 0F, 0F, 22, 1, 8, 0F); // Box 25
		gunModel[31].setRotationPoint(29F, -14F, -4F);

		gunModel[32].addBox(0F, 0F, 0F, 2, 2, 5, 0F); // Box 26
		gunModel[32].setRotationPoint(51F, -17F, -2.5F);

		gunModel[33].addShapeBox(0F, 0F, 0F, 2, 1, 5, 0F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 27
		gunModel[33].setRotationPoint(51F, -18F, -2.5F);

		gunModel[34].addShapeBox(0F, 0F, 0F, 2, 1, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F); // Box 28
		gunModel[34].setRotationPoint(51F, -15F, -2.5F);

		gunModel[35].addShapeBox(0F, 0F, 0F, 2, 1, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F); // Box 33
		gunModel[35].setRotationPoint(51F, -20F, -2.5F);

		gunModel[36].addBox(0F, 0F, 0F, 2, 2, 5, 0F); // Box 34
		gunModel[36].setRotationPoint(51F, -22F, -2.5F);

		gunModel[37].addShapeBox(0F, 0F, 0F, 2, 1, 5, 0F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 35
		gunModel[37].setRotationPoint(51F, -23F, -2.5F);

		gunModel[38].addShapeBox(0F, 0F, 0F, 1, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, 0F); // Box 40
		gunModel[38].setRotationPoint(34F, -24F, -4F);

		gunModel[39].addShapeBox(0F, 0F, 0F, 1, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F); // Box 41
		gunModel[39].setRotationPoint(32F, -24F, -4F);

		gunModel[40].addShapeBox(0F, 0F, 0F, 1, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, 0F); // Box 42
		gunModel[40].setRotationPoint(38F, -24F, -4F);

		gunModel[41].addShapeBox(0F, 0F, 0F, 1, 3, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, 0F); // Box 43
		gunModel[41].setRotationPoint(33F, -24F, -3.5F);

		gunModel[42].addShapeBox(0F, 0F, 0F, 1, 3, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, 0F); // Box 44
		gunModel[42].setRotationPoint(35F, -24F, -3.5F);

		gunModel[43].addShapeBox(0F, 0F, 0F, 1, 3, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, 0F); // Box 45
		gunModel[43].setRotationPoint(37F, -24F, -3.5F);

		gunModel[44].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 46
		gunModel[44].setRotationPoint(-6F, -26F, -3.5F);

		gunModel[45].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 47
		gunModel[45].setRotationPoint(0F, -26F, -3.5F);

		gunModel[46].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 48
		gunModel[46].setRotationPoint(12F, -26F, -3.5F);

		gunModel[47].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 49
		gunModel[47].setRotationPoint(6F, -26F, -3.5F);

		gunModel[48].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 52
		gunModel[48].setRotationPoint(18F, -26F, -3.5F);

		gunModel[49].addBox(0F, 0F, 0F, 14, 6, 7, 0F); // Box 0
		gunModel[49].setRotationPoint(15F, -20F, -2.5F);

		gunModel[50].addBox(0F, 0F, 0F, 9, 1, 2, 0F); // Box 1
		gunModel[50].setRotationPoint(15F, -20F, -4.5F);

		gunModel[51].addBox(0F, 0F, 0F, 9, 2, 2, 0F); // Box 2
		gunModel[51].setRotationPoint(15F, -16F, -4.5F);

		gunModel[52].addBox(0F, 0F, 0F, 5, 6, 2, 0F); // Box 3
		gunModel[52].setRotationPoint(24F, -20F, -4.5F);

		gunModel[53].addShapeBox(0F, 0F, 0F, 17, 7, 1, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 0
		gunModel[53].setRotationPoint(31F, -20.5F, -6F);

		gunModel[54].addBox(0F, 0F, 0F, 17, 7, 1, 0F); // Box 1
		gunModel[54].setRotationPoint(31F, -20.5F, -5F);

		gunModel[55].addBox(0F, 0F, 0F, 17, 7, 1, 0F); // Box 2
		gunModel[55].setRotationPoint(31F, -20.5F, 4F);

		gunModel[56].addShapeBox(0F, 0F, 0F, 17, 7, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F); // Box 3
		gunModel[56].setRotationPoint(31F, -20.5F, 5F);

		gunModel[57].addShapeBox(0F, 0F, 0F, 4, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 4
		gunModel[57].setRotationPoint(46F, -25F, -1.5F);

		gunModel[58].addBox(0F, 0F, 0F, 10, 2, 9, 0F); // Box 23
		gunModel[58].setRotationPoint(-53F, -22F, -4.5F);

		gunModel[59].addShapeBox(0F, 0F, 0F, 10, 1, 9, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 23
		gunModel[59].setRotationPoint(-53F, -23F, -4.5F);

		gunModel[60].addBox(0F, 0F, 0F, 8, 1, 5, 0F); // Box 24
		gunModel[60].setRotationPoint(19F, -13.5F, -2.5F);

		gunModel[61].addBox(0F, 0F, 0F, 5, 2, 7, 0F); // Box 27
		gunModel[61].setRotationPoint(-12F, -26F, -3.5F);

		gunModel[62].addBox(0F, 0F, 0F, 5, 6, 10, 0F); // Box 0
		gunModel[62].setRotationPoint(-53F, -20F, -5F);

		gunModel[63].addShapeBox(0F, 0F, 0F, 4, 2, 9, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 1
		gunModel[63].setRotationPoint(-43F, -22F, -4.5F);

		gunModel[64].addShapeBox(0F, 0F, 0F, 2, 1, 9, 0F, 0F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 2
		gunModel[64].setRotationPoint(-43F, -23F, -4.5F);

		gunModel[65].addBox(0F, 0F, 0F, 28, 2, 7, 0F); // Box 3
		gunModel[65].setRotationPoint(-42F, -21.5F, -3.5F);

		gunModel[66].addShapeBox(0F, 0F, 0F, 28, 1, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 4
		gunModel[66].setRotationPoint(-42F, -22.5F, -3.5F);

		gunModel[67].addBox(0F, 0F, 0F, 21, 4, 9, 0F); // Box 6
		gunModel[67].setRotationPoint(-53F, -14F, -4.5F);

		gunModel[68].addShapeBox(0F, 0F, 0F, 4, 4, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F); // Box 8
		gunModel[68].setRotationPoint(-32F, -14F, -4.5F);

		gunModel[69].addShapeBox(0F, 0F, 0F, 18, 4, 8, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 9
		gunModel[69].setRotationPoint(-31F, -14F, -4F);

		gunModel[70].addBox(0F, 0F, 0F, 10, 12, 9, 0F); // Box 10
		gunModel[70].setRotationPoint(-53F, -10F, -4.5F);

		gunModel[71].addShapeBox(0F, 0F, 0F, 7, 8, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -6F, 0F, 0F, -6F, 0F, 0F, 0F, 0F, 0F); // Box 11
		gunModel[71].setRotationPoint(-50F, 2F, -4.5F);

		gunModel[72].addBox(0F, 0F, 0F, 12, 12, 8, 0F); // Box 12
		gunModel[72].setRotationPoint(-43F, -10F, -4F);

		gunModel[73].addShapeBox(0F, 0F, 0F, 1, 4, 8, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 13
		gunModel[73].setRotationPoint(-31F, -10F, -4F);

		gunModel[74].addBox(0F, 0F, 0F, 1, 8, 8, 0F); // Box 14
		gunModel[74].setRotationPoint(-31F, -6F, -4F);

		gunModel[75].addShapeBox(0F, 0F, 0F, 17, 3, 8, 0F, 0F, 0F, 0F, 0F, -7F, 0F, 0F, -7F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 7F, 0F, 0F, 7F, 0F, 0F, 0F, 0F); // Box 15
		gunModel[75].setRotationPoint(-30F, -1F, -4F);

		gunModel[76].addShapeBox(0F, 0F, 0F, 17, 1, 8, 0F, 0F, 0F, -1.5F, 0F, -7F, -1.5F, 0F, -7F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 7F, 0F, 0F, 7F, 0F, 0F, 0F, 0F); // Box 21
		gunModel[76].setRotationPoint(-30F, -2F, -4F);

		gunModel[77].addShapeBox(0F, 0F, 0F, 1, 5, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F); // Box 22
		gunModel[77].setRotationPoint(-30F, -6F, -4F);

		gunModel[78].addShapeBox(0F, 0F, 0F, 1, 4, 8, 0F, -3F, 0F, 0F, 3F, 0F, -1.5F, 3F, 0F, -1.5F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F); // Box 23
		gunModel[78].setRotationPoint(-30F, -10F, -4F);

		gunModel[79].addShapeBox(0F, 0F, 0F, 16, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F); // Box 24
		gunModel[79].setRotationPoint(-27F, -10F, -4F);

		gunModel[80].addBox(0F, 0F, 0F, 1, 6, 10, 0F); // Box 27
		gunModel[80].setRotationPoint(-45F, -20F, -5F);

		gunModel[81].addBox(0F, 0F, 0F, 1, 6, 10, 0F); // Box 28
		gunModel[81].setRotationPoint(-43F, -20F, -5F);

		gunModel[82].addBox(0F, 0F, 0F, 25, 6, 10, 0F); // Box 29
		gunModel[82].setRotationPoint(-41F, -20F, -5F);

		gunModel[83].addBox(0F, 0F, 0F, 7, 6, 9, 0F); // Box 30
		gunModel[83].setRotationPoint(-48F, -20F, -4.5F);

		gunModel[84].addBox(0F, 0F, 0F, 4, 14, 11, 0F); // Box 31
		gunModel[84].setRotationPoint(-57F, -21F, -5.5F);

		gunModel[85].addShapeBox(0F, 0F, 0F, 4, 2, 11, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 32
		gunModel[85].setRotationPoint(-57F, -23F, -5.5F);

		gunModel[86].addBox(0F, 0F, 0F, 4, 15, 9, 0F); // Box 33
		gunModel[86].setRotationPoint(-57F, -5F, -4.5F);

		gunModel[87].addShapeBox(0F, 0F, 0F, 4, 2, 11, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 34
		gunModel[87].setRotationPoint(-57F, -7F, -5.5F);

		gunModel[88].addShapeBox(0F, 0F, 0F, 4, 2, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 35
		gunModel[88].setRotationPoint(-57F, 10F, -4.5F);

		gunModel[89].addBox(0F, 0F, 0F, 1, 6, 10, 0F); // Box 36
		gunModel[89].setRotationPoint(-47F, -20F, -5F);

		gunModel[90].addBox(0F, 0F, 0F, 3, 8, 9, 0F); // Box 37
		gunModel[90].setRotationPoint(-53F, 2F, -4.5F);

		gunModel[91].addShapeBox(0F, 0F, 0F, 5, 2, 12, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 38
		gunModel[91].setRotationPoint(-53F, -14F, -6F);

		gunModel[92].addBox(0F, 0F, 0F, 5, 5, 12, 0F); // Box 39
		gunModel[92].setRotationPoint(-53F, -12F, -6F);

		gunModel[93].addShapeBox(0F, 0F, 0F, 5, 2, 12, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 40
		gunModel[93].setRotationPoint(-53F, -7F, -6F);

		gunModel[94].addBox(0F, 0F, 0F, 5, 7, 1, 0F); // Box 41
		gunModel[94].setRotationPoint(-38F, -20F, -5.5F);

		gunModel[95].addShapeBox(0F, 0F, 0F, 5, 3, 1, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 42
		gunModel[95].setRotationPoint(-38F, -23F, -5.5F);

		gunModel[96].addBox(0F, 0F, 0F, 5, 1, 7, 0F); // Box 43
		gunModel[96].setRotationPoint(-38F, -23F, -3.5F);

		gunModel[97].addShapeBox(0F, 0F, 0F, 5, 3, 1, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 44
		gunModel[97].setRotationPoint(-38F, -23F, 4.5F);

		gunModel[98].addBox(0F, 0F, 0F, 5, 7, 1, 0F); // Box 45
		gunModel[98].setRotationPoint(-38F, -20F, 4.5F);

		gunModel[99].addShapeBox(0F, 0F, 0F, 5, 15, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F, 1F); // Box 46
		gunModel[99].setRotationPoint(-38F, -13F, -5.5F);

		gunModel[100].addShapeBox(0F, 0F, 0F, 5, 15, 1, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 47
		gunModel[100].setRotationPoint(-38F, -13F, 3.5F);

		gunModel[101].addShapeBox(0F, 0F, 0F, 5, 1, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 48
		gunModel[101].setRotationPoint(-38F, 2F, -4.5F);

		gunModel[102].addBox(0F, 0F, 0F, 5, 8, 1, 0F); // Box 49
		gunModel[102].setRotationPoint(-38F, -11F, -6.5F);

		gunModel[103].addShapeBox(0F, 0F, 0F, 5, 2, 1, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 50
		gunModel[103].setRotationPoint(-38F, -13F, -6.5F);


		defaultScopeModel = new ModelRendererTurbo[3];
		defaultScopeModel[0] = new ModelRendererTurbo(this, 242, 6, textureX, textureY); // Box 26
		defaultScopeModel[1] = new ModelRendererTurbo(this, 227, 4, textureX, textureY); // Box 26
		defaultScopeModel[2] = new ModelRendererTurbo(this, 227, 10, textureX, textureY); // Box 26

		defaultScopeModel[0].addShapeBox(0F, 0F, 0F, 4, 3, 1, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 26
		defaultScopeModel[0].setRotationPoint(46F, -28F, -0.5F);

		defaultScopeModel[1].addShapeBox(0F, 0F, 0F, 5, 3, 2, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 26
		defaultScopeModel[1].setRotationPoint(-12F, -29F, -3.5F);

		defaultScopeModel[2].addShapeBox(0F, 0F, 0F, 5, 3, 2, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 26
		defaultScopeModel[2].setRotationPoint(-12F, -29F, 1.5F);


		ammoModel = new ModelRendererTurbo[9];
		ammoModel[0] = new ModelRendererTurbo(this, 91, 137, textureX, textureY); // Box 29
		ammoModel[1] = new ModelRendererTurbo(this, 91, 94, textureX, textureY); // Box 30
		ammoModel[2] = new ModelRendererTurbo(this, 91, 109, textureX, textureY); // Box 31
		ammoModel[3] = new ModelRendererTurbo(this, 91, 123, textureX, textureY); // Box 32
		ammoModel[4] = new ModelRendererTurbo(this, 176, 94, textureX, textureY); // Box 36
		ammoModel[5] = new ModelRendererTurbo(this, 176, 109, textureX, textureY); // Box 37
		ammoModel[6] = new ModelRendererTurbo(this, 176, 123, textureX, textureY); // Box 39
		ammoModel[7] = new ModelRendererTurbo(this, 126, 142, textureX, textureY); // Box 91
		ammoModel[8] = new ModelRendererTurbo(this, 147, 142, textureX, textureY); // Box 92

		ammoModel[0].addBox(0F, 0F, 0F, 10, 3, 7, 0F); // Box 29
		ammoModel[0].setRotationPoint(18F, -14F, -3.5F);

		ammoModel[1].addBox(0F, 0F, 0F, 32, 4, 10, 0F); // Box 30
		ammoModel[1].setRotationPoint(17F, -10F, -5F);

		ammoModel[2].addShapeBox(0F, 0F, 0F, 32, 3, 10, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 31
		ammoModel[2].setRotationPoint(17F, -13F, -5F);

		ammoModel[3].addShapeBox(0F, 0F, 0F, 32, 3, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 32
		ammoModel[3].setRotationPoint(17F, -6F, -5F);

		ammoModel[4].addShapeBox(0F, 0F, 0F, 2, 4, 10, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F); // Box 36
		ammoModel[4].setRotationPoint(49F, -10F, -5F);

		ammoModel[5].addShapeBox(0F, 0F, 0F, 2, 3, 10, 0F, 0F, 0F, -3F, 0F, -1.5F, -3.5F, 0F, -1.5F, -3.5F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F); // Box 37
		ammoModel[5].setRotationPoint(49F, -13F, -5F);

		ammoModel[6].addShapeBox(0F, 0F, 0F, 2, 3, 10, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, -1.5F, -3.5F, 0F, -1.5F, -3.5F, 0F, 0F, -3F); // Box 39
		ammoModel[6].setRotationPoint(49F, -6F, -5F);

		ammoModel[7].addShapeBox(0F, 0F, 0F, 6, 1, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 91
		ammoModel[7].setRotationPoint(19F, -15F, -2.5F);

		ammoModel[8].addShapeBox(0F, 0F, 0F, 2, 1, 4, 0F, 0F, 0F, -1F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 92
		ammoModel[8].setRotationPoint(25F, -15F, -2.5F);


		slideModel = new ModelRendererTurbo[1];
		slideModel[0] = new ModelRendererTurbo(this, 1, 23, textureX, textureY); // Box 4

		slideModel[0].addShapeBox(0F, 0F, 0F, 9, 3, 1, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 4
		slideModel[0].setRotationPoint(15F, -19F, -4F);

		barrelAttachPoint = new Vector3f(0F, 0F, 0F);
		stockAttachPoint = new Vector3f(0F, 0F, 0F);
		scopeAttachPoint = new Vector3f(0F, 0F, 0F);
		gripAttachPoint = new Vector3f(0F, 0F, 0F);

		gunSlideDistance = 0.5F;
		animationType = EnumAnimationType.BOTTOM_CLIP;


		translateAll(0F, 0F, 0F);


		flipAll();
	}
}
