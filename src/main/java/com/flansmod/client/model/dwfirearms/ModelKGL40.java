package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelKGL40 extends ModelGun
{
	int textureX = 512;
	int textureY = 256;

	public ModelKGL40()
	{
		gunModel = new ModelRendererTurbo[42];
		gunModel[0] = new ModelRendererTurbo(this, 68, 47, textureX, textureY); // barrel
		gunModel[1] = new ModelRendererTurbo(this, 68, 73, textureX, textureY); // barrel
		gunModel[2] = new ModelRendererTurbo(this, 99, 74, textureX, textureY); // barrel
		gunModel[3] = new ModelRendererTurbo(this, 68, 58, textureX, textureY); // barrel
		gunModel[4] = new ModelRendererTurbo(this, 1, 47, textureX, textureY); // barrel
		gunModel[5] = new ModelRendererTurbo(this, 18, 6, textureX, textureY); // barrel
		gunModel[6] = new ModelRendererTurbo(this, 18, 6, textureX, textureY); // barrel
		gunModel[7] = new ModelRendererTurbo(this, 12, 4, textureX, textureY); // barrel
		gunModel[8] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // barrel
		gunModel[9] = new ModelRendererTurbo(this, 1, 13, textureX, textureY); // barrel
		gunModel[10] = new ModelRendererTurbo(this, 25, 1, textureX, textureY); // barrel
		gunModel[11] = new ModelRendererTurbo(this, 25, 1, textureX, textureY); // barrel
		gunModel[12] = new ModelRendererTurbo(this, 16, 13, textureX, textureY); // barrel
		gunModel[13] = new ModelRendererTurbo(this, 36, 1, textureX, textureY); // barrel
		gunModel[14] = new ModelRendererTurbo(this, 36, 7, textureX, textureY); // barrel
		gunModel[15] = new ModelRendererTurbo(this, 16, 21, textureX, textureY); // barrel
		gunModel[16] = new ModelRendererTurbo(this, 110, 113, textureX, textureY); // barrel
		gunModel[17] = new ModelRendererTurbo(this, 110, 106, textureX, textureY); // barrel
		gunModel[18] = new ModelRendererTurbo(this, 1, 35, textureX, textureY); // barrel
		gunModel[19] = new ModelRendererTurbo(this, 16, 40, textureX, textureY); // barrel
		gunModel[20] = new ModelRendererTurbo(this, 32, 47, textureX, textureY); // Box 0
		gunModel[21] = new ModelRendererTurbo(this, 49, 47, textureX, textureY); // Box 1
		gunModel[22] = new ModelRendererTurbo(this, 184, 74, textureX, textureY); // Box 2
		gunModel[23] = new ModelRendererTurbo(this, 24, 36, textureX, textureY); // Box 4
		gunModel[24] = new ModelRendererTurbo(this, 103, 108, textureX, textureY); // Box 14
		gunModel[25] = new ModelRendererTurbo(this, 96, 108, textureX, textureY); // Box 15
		gunModel[26] = new ModelRendererTurbo(this, 137, 112, textureX, textureY); // Box 16
		gunModel[27] = new ModelRendererTurbo(this, 137, 106, textureX, textureY); // Box 17
		gunModel[28] = new ModelRendererTurbo(this, 68, 93, textureX, textureY); // Box 18
		gunModel[29] = new ModelRendererTurbo(this, 130, 106, textureX, textureY); // Box 19
		gunModel[30] = new ModelRendererTurbo(this, 123, 106, textureX, textureY); // Box 20
		gunModel[31] = new ModelRendererTurbo(this, 130, 112, textureX, textureY); // Box 21
		gunModel[32] = new ModelRendererTurbo(this, 123, 112, textureX, textureY); // Box 22
		gunModel[33] = new ModelRendererTurbo(this, 68, 106, textureX, textureY); // Box 23
		gunModel[34] = new ModelRendererTurbo(this, 68, 98, textureX, textureY); // Box 24
		gunModel[35] = new ModelRendererTurbo(this, 68, 88, textureX, textureY); // Box 25
		gunModel[36] = new ModelRendererTurbo(this, 91, 108, textureX, textureY); // Box 26
		gunModel[37] = new ModelRendererTurbo(this, 1, 121, textureX, textureY); // Box 4
		gunModel[38] = new ModelRendererTurbo(this, 30, 111, textureX, textureY); // Box 5
		gunModel[39] = new ModelRendererTurbo(this, 30, 111, textureX, textureY); // Box 6
		gunModel[40] = new ModelRendererTurbo(this, 30, 111, textureX, textureY); // Box 7
		gunModel[41] = new ModelRendererTurbo(this, 30, 111, textureX, textureY); // Box 8

		gunModel[0].addShapeBox(0F, -1F, 0F, 47, 3, 7, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // barrel
		gunModel[0].setRotationPoint(-9F, -4F, -3.5F);

		gunModel[1].addBox(0F, 0F, 0F, 8, 7, 7, 0F); // barrel
		gunModel[1].setRotationPoint(-12F, -12F, -3.5F);

		gunModel[2].addBox(0F, 0F, 0F, 8, 5, 8, 0F); // barrel
		gunModel[2].setRotationPoint(-12F, -17F, -4F);

		gunModel[3].addBox(0F, 0F, 0F, 42, 3, 10, 0F); // barrel
		gunModel[3].setRotationPoint(-4F, -18F, -5F);

		gunModel[4].addShapeBox(0F, 0F, 0F, 8, 15, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 3F, 0F, 0F); // barrel
		gunModel[4].setRotationPoint(-9F, -2F, -3.5F);

		gunModel[5].addShapeBox(0F, 0F, 0F, 1, 3, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F); // barrel
		gunModel[5].setRotationPoint(25F, -33F, 1F);

		gunModel[6].addShapeBox(0F, 0F, 0F, 1, 3, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F); // barrel
		gunModel[6].setRotationPoint(25F, -33F, -3F);

		gunModel[7].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F); // barrel
		gunModel[7].setRotationPoint(25F, -31F, -0.5F);

		gunModel[8].addBox(0F, 0F, 0F, 2, 4, 6, 0F); // barrel
		gunModel[8].setRotationPoint(24F, -30F, -3F);

		gunModel[9].addBox(0F, 0F, 0F, 1, 15, 6, 0F); // barrel
		gunModel[9].setRotationPoint(8F, -41F, -3F);

		gunModel[10].addBox(0F, 0F, 0F, 3, 9, 2, 0F); // barrel
		gunModel[10].setRotationPoint(20F, -25F, 4F);

		gunModel[11].addBox(0F, 0F, 0F, 3, 9, 2, 0F); // barrel
		gunModel[11].setRotationPoint(8F, -25F, 4F);

		gunModel[12].addBox(0F, 0F, 0F, 18, 1, 6, 0F); // barrel
		gunModel[12].setRotationPoint(8F, -26F, -3F);

		gunModel[13].addShapeBox(0F, 0F, 0F, 13, 4, 1, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // barrel
		gunModel[13].setRotationPoint(8F, -29F, -4F);

		gunModel[14].addBox(0F, 0F, 0F, 5, 4, 1, 0F); // barrel
		gunModel[14].setRotationPoint(21F, -29F, -4F);

		gunModel[15].addBox(0F, 0F, 0F, 18, 3, 3, 0F); // barrel
		gunModel[15].setRotationPoint(8F, -28F, 3F);

		gunModel[16].addBox(0F, 0F, 0F, 4, 5, 1, 0F); // barrel
		gunModel[16].setRotationPoint(-5F, -11F, -6F);

		gunModel[17].addBox(0F, 0F, 0F, 5, 5, 1, 0F); // barrel
		gunModel[17].setRotationPoint(1F, -11F, -6F);

		gunModel[18].addBox(0F, 0F, 0F, 3, 3, 8, 0F); // barrel
		gunModel[18].setRotationPoint(-8F, -11F, -4F);

		gunModel[19].addBox(0F, 0F, 0F, 3, 1, 1, 0F); // barrel
		gunModel[19].setRotationPoint(-10F, -8F, 3F);

		gunModel[20].addShapeBox(0F, 0F, 0F, 1, 15, 7, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, -1F, -3F, 0F, -1F, 3F, 0F, 0F); // Box 0
		gunModel[20].setRotationPoint(-1F, -2F, -3.5F);

		gunModel[21].addShapeBox(0F, 0F, 0F, 1, 15, 7, 0F, -3F, 0F, -1F, 3F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 1
		gunModel[21].setRotationPoint(-13F, -2F, -3.5F);

		gunModel[22].addBox(0F, 0F, 0F, 8, 3, 10, 0F); // Box 2
		gunModel[22].setRotationPoint(-12F, -18F, -5F);

		gunModel[23].addShapeBox(0F, 0F, 0F, 1, 3, 7, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, -2F, 0F, -1F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, -1F); // Box 4
		gunModel[23].setRotationPoint(-12F, -5F, -3.5F);

		gunModel[24].addShapeBox(0F, 0F, 0F, 1, 8, 2, 0F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F); // Box 14
		gunModel[24].setRotationPoint(35.5F, -14F, -7F);

		gunModel[25].addShapeBox(0F, 0F, 0F, 1, 8, 2, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F); // Box 15
		gunModel[25].setRotationPoint(36.5F, -14F, -7F);

		gunModel[26].addShapeBox(0F, 0F, 0F, 3, 3, 2, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 16
		gunModel[26].setRotationPoint(33F, -17F, -7F);

		gunModel[27].addShapeBox(0F, 0F, 0F, 3, 3, 2, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 17
		gunModel[27].setRotationPoint(33F, -6F, -7F);

		gunModel[28].addShapeBox(0F, 0F, 0F, 43, 3, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0.5F, 0F, -1F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // Box 18
		gunModel[28].setRotationPoint(-5F, -6F, -5F);

		gunModel[29].addShapeBox(0F, 0F, 0F, 1, 3, 2, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 19
		gunModel[29].setRotationPoint(37F, -6F, -7F);

		gunModel[30].addShapeBox(0F, 0F, 0F, 1, 3, 2, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 20
		gunModel[30].setRotationPoint(37F, -17F, -7F);

		gunModel[31].addBox(0F, 0F, 0F, 1, 3, 2, 0F); // Box 21
		gunModel[31].setRotationPoint(36F, -17F, -7F);

		gunModel[32].addBox(0F, 0F, 0F, 1, 3, 2, 0F); // Box 22
		gunModel[32].setRotationPoint(36F, -6F, -7F);

		gunModel[33].addBox(0F, 0F, 0F, 10, 11, 1, 0F); // Box 23
		gunModel[33].setRotationPoint(23F, -17F, -6F);

		gunModel[34].addBox(0F, 0F, 0F, 28, 6, 1, 0F); // Box 24
		gunModel[34].setRotationPoint(-5F, -17F, -6F);

		gunModel[35].addShapeBox(0F, 0F, 0F, 38, 3, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 25
		gunModel[35].setRotationPoint(-5F, -6F, -6F);

		gunModel[36].addShapeBox(0F, 0F, 0F, 1, 9, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0.5F, 0F, 1F, 0.5F); // Box 26
		gunModel[36].setRotationPoint(-5F, -15F, -5F);

		gunModel[37].addBox(0F, -1F, 0F, 21, 2, 7, 0F); // Box 4
		gunModel[37].setRotationPoint(17F, -1F, -3.5F);

		gunModel[38].addShapeBox(0F, -1F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 5
		gunModel[38].setRotationPoint(17F, 1F, -3.5F);

		gunModel[39].addShapeBox(0F, -1F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 6
		gunModel[39].setRotationPoint(23F, 1F, -3.5F);

		gunModel[40].addShapeBox(0F, -1F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 7
		gunModel[40].setRotationPoint(35F, 1F, -3.5F);

		gunModel[41].addShapeBox(0F, -1F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 8
		gunModel[41].setRotationPoint(29F, 1F, -3.5F);


		defaultStockModel = new ModelRendererTurbo[9];
		defaultStockModel[0] = new ModelRendererTurbo(this, 159, 72, textureX, textureY); // barrel
		defaultStockModel[1] = new ModelRendererTurbo(this, 30, 79, textureX, textureY); // barrel
		defaultStockModel[2] = new ModelRendererTurbo(this, 1, 79, textureX, textureY); // barrel
		defaultStockModel[3] = new ModelRendererTurbo(this, 1, 92, textureX, textureY); // barrel
		defaultStockModel[4] = new ModelRendererTurbo(this, 1, 70, textureX, textureY); // barrel
		defaultStockModel[5] = new ModelRendererTurbo(this, 68, 2, textureX, textureY); // barrel
		defaultStockModel[6] = new ModelRendererTurbo(this, 132, 72, textureX, textureY); // barrel
		defaultStockModel[7] = new ModelRendererTurbo(this, 174, 76, textureX, textureY); // barrel
		defaultStockModel[8] = new ModelRendererTurbo(this, 174, 79, textureX, textureY); // barrel

		defaultStockModel[0].addShapeBox(0F, 0F, 0F, 2, 5, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F); // barrel
		defaultStockModel[0].setRotationPoint(-17F, -17F, -5F);

		defaultStockModel[1].addShapeBox(0F, 0F, 0F, 6, 4, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // barrel
		defaultStockModel[1].setRotationPoint(-41F, 0F, -4F);

		defaultStockModel[2].addShapeBox(0F, 0F, 0F, 6, 4, 8, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // barrel
		defaultStockModel[2].setRotationPoint(-41F, -24F, -4F);

		defaultStockModel[3].addBox(0F, 0F, 0F, 6, 20, 8, 0F); // barrel
		defaultStockModel[3].setRotationPoint(-41F, -20F, -4F);

		defaultStockModel[4].addBox(0F, 0F, 0F, 23, 2, 6, 0F); // barrel
		defaultStockModel[4].setRotationPoint(-35F, -18F, -3F);

		defaultStockModel[5].addBox(0F, 0F, 0F, 63, 2, 4, 0F); // barrel
		defaultStockModel[5].setRotationPoint(-35F, -20F, -2F);

		defaultStockModel[6].addBox(0F, 0F, 0F, 3, 5, 10, 0F); // barrel
		defaultStockModel[6].setRotationPoint(-15F, -17F, -5F);

		defaultStockModel[7].addShapeBox(0F, 0F, 0F, 5, 1, 1, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // barrel
		defaultStockModel[7].setRotationPoint(-17F, -18F, -5F);

		defaultStockModel[8].addShapeBox(0F, 0F, 0F, 5, 1, 1, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // barrel
		defaultStockModel[8].setRotationPoint(-17F, -18F, 4F);


		ammoModel = new ModelRendererTurbo[6];
		ammoModel[0] = new ModelRendererTurbo(this, 68, 119, textureX, textureY); // barrel
		ammoModel[1] = new ModelRendererTurbo(this, 68, 132, textureX, textureY); // barrel
		ammoModel[2] = new ModelRendererTurbo(this, 68, 132, textureX, textureY); // barrel
		ammoModel[3] = new ModelRendererTurbo(this, 119, 119, textureX, textureY); // barrel
		ammoModel[4] = new ModelRendererTurbo(this, 119, 119, textureX, textureY); // barrel
		ammoModel[5] = new ModelRendererTurbo(this, 119, 119, textureX, textureY); // barrel

		ammoModel[0].addBox(0F, 0F, 0F, 16, 3, 9, 0F); // barrel
		ammoModel[0].setRotationPoint(-3.5F, -11.5F, -4.5F);

		ammoModel[1].addShapeBox(0F, 0F, 0F, 16, 3, 9, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // barrel
		ammoModel[1].setRotationPoint(-3.5F, -14.5F, -4.5F);

		ammoModel[2].addShapeBox(0F, 0F, 0F, 16, 3, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // barrel
		ammoModel[2].setRotationPoint(-3.5F, -8.5F, -4.5F);

		ammoModel[3].addShapeBox(0F, 0F, 0F, 4, 3, 9, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F); // barrel
		ammoModel[3].setRotationPoint(12.5F, -11.5F, -4.5F);

		ammoModel[4].addShapeBox(0F, 0F, 0F, 4, 3, 9, 0F, 0F, 0F, -3F, 0F, -2F, -3F, 0F, -2F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F); // barrel
		ammoModel[4].setRotationPoint(12.5F, -14.5F, -4.5F);

		ammoModel[5].addShapeBox(0F, 0F, 0F, 4, 3, 9, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, -2F, -3F, 0F, -2F, -3F, 0F, 0F, -3F); // barrel
		ammoModel[5].setRotationPoint(12.5F, -8.5F, -4.5F);


		revolverBarrelModel = new ModelRendererTurbo[16];
		revolverBarrelModel[0] = new ModelRendererTurbo(this, 68, 9, textureX, textureY); // barrel
		revolverBarrelModel[1] = new ModelRendererTurbo(this, 68, 33, textureX, textureY); // barrel
		revolverBarrelModel[2] = new ModelRendererTurbo(this, 68, 37, textureX, textureY); // barrel
		revolverBarrelModel[3] = new ModelRendererTurbo(this, 163, 58, textureX, textureY); // barrel
		revolverBarrelModel[4] = new ModelRendererTurbo(this, 157, 24, textureX, textureY); // Box 5
		revolverBarrelModel[5] = new ModelRendererTurbo(this, 157, 37, textureX, textureY); // Box 6
		revolverBarrelModel[6] = new ModelRendererTurbo(this, 157, 9, textureX, textureY); // Box 7
		revolverBarrelModel[7] = new ModelRendererTurbo(this, 198, 24, textureX, textureY); // Box 8
		revolverBarrelModel[8] = new ModelRendererTurbo(this, 194, 37, textureX, textureY); // Box 9
		revolverBarrelModel[9] = new ModelRendererTurbo(this, 157, 13, textureX, textureY); // Box 10
		revolverBarrelModel[10] = new ModelRendererTurbo(this, 157, 9, textureX, textureY); // Box 11
		revolverBarrelModel[11] = new ModelRendererTurbo(this, 157, 24, textureX, textureY); // Box 12
		revolverBarrelModel[12] = new ModelRendererTurbo(this, 68, 15, textureX, textureY); // Box 0
		revolverBarrelModel[13] = new ModelRendererTurbo(this, 68, 29, textureX, textureY); // Box 1
		revolverBarrelModel[14] = new ModelRendererTurbo(this, 68, 25, textureX, textureY); // Box 2
		revolverBarrelModel[15] = new ModelRendererTurbo(this, 68, 21, textureX, textureY); // Box 3

		revolverBarrelModel[0].addBox(0F, 0F, 0F, 34, 4, 1, 0F); // barrel
		revolverBarrelModel[0].setRotationPoint(8F, -12F, -5F);

		revolverBarrelModel[1].addShapeBox(0F, 0F, 0F, 34, 2, 1, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // barrel
		revolverBarrelModel[1].setRotationPoint(8F, -14F, -5F);

		revolverBarrelModel[2].addShapeBox(0F, 0F, 0F, 34, 1, 8, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // barrel
		revolverBarrelModel[2].setRotationPoint(8F, -15F, -4F);

		revolverBarrelModel[3].addShapeBox(0F, 0F, 0F, 34, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // barrel
		revolverBarrelModel[3].setRotationPoint(8F, -6F, -4F);

		revolverBarrelModel[4].addShapeBox(0F, 0F, 0F, 10, 2, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 5
		revolverBarrelModel[4].setRotationPoint(-2F, -8F, -5F);

		revolverBarrelModel[5].addShapeBox(0F, 0F, 0F, 10, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -1F); // Box 6
		revolverBarrelModel[5].setRotationPoint(-2F, -6F, -4F);

		revolverBarrelModel[6].addShapeBox(0F, 0F, 0F, 10, 4, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F); // Box 7
		revolverBarrelModel[6].setRotationPoint(-2F, -12F, -5F);

		revolverBarrelModel[7].addShapeBox(0F, 0F, 0F, 10, 2, 10, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F); // Box 8
		revolverBarrelModel[7].setRotationPoint(-2F, -14F, -5F);

		revolverBarrelModel[8].addShapeBox(0F, 0F, 0F, 10, 1, 8, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F); // Box 9
		revolverBarrelModel[8].setRotationPoint(-2F, -15F, -4F);

		revolverBarrelModel[9].addBox(0F, 0F, 0F, 3, 4, 1, 0F); // Box 10
		revolverBarrelModel[9].setRotationPoint(-5F, -12F, 5F);

		revolverBarrelModel[10].addShapeBox(0F, 0F, 0F, 3, 2, 1, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 11
		revolverBarrelModel[10].setRotationPoint(-5F, -14F, 5F);

		revolverBarrelModel[11].addShapeBox(0F, 0F, 0F, 3, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 12
		revolverBarrelModel[11].setRotationPoint(-5F, -8F, 5F);

		revolverBarrelModel[12].addBox(0F, 0F, 0F, 34, 4, 1, 0F); // Box 0
		revolverBarrelModel[12].setRotationPoint(8F, -12F, 4F);

		revolverBarrelModel[13].addShapeBox(0F, 0F, 0F, 34, 2, 1, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 1
		revolverBarrelModel[13].setRotationPoint(8F, -14F, 4F);

		revolverBarrelModel[14].addShapeBox(0F, 0F, 0F, 34, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 2F, 0F, 0F, 2F); // Box 2
		revolverBarrelModel[14].setRotationPoint(8F, -8F, -5F);

		revolverBarrelModel[15].addShapeBox(0F, 0F, 0F, 34, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 3
		revolverBarrelModel[15].setRotationPoint(8F, -8F, 4F);

		barrelAttachPoint = new Vector3f(0F /16F, 0F /16F, 0F /16F);
		stockAttachPoint = new Vector3f(0F /16F, 0F /16F, 0F /16F);
		scopeAttachPoint = new Vector3f(0F /16F, 0F /16F, 0F /16F);
		gripAttachPoint = new Vector3f(27 /16F, 1F /16F, 0F /16F);

		gunSlideDistance = 0F;
		animationType = EnumAnimationType.REVOLVER;

		revolverFlipAngle = -30F;

		translateAll(0F, 0F, 0F);


		flipAll();
	}
}