package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelFlakwaterM550 extends ModelGun
{
	int textureX = 512;
	int textureY = 256;

	public ModelFlakwaterM550()
	{
		gunModel = new ModelRendererTurbo[28];
		gunModel[0] = new ModelRendererTurbo(this, 80, 51, textureX, textureY); // body1
		gunModel[1] = new ModelRendererTurbo(this, 80, 38, textureX, textureY); // body2
		gunModel[2] = new ModelRendererTurbo(this, 80, 75, textureX, textureY); // body3
		gunModel[3] = new ModelRendererTurbo(this, 120, 75, textureX, textureY); // body4
		gunModel[4] = new ModelRendererTurbo(this, 113, 75, textureX, textureY); // body5
		gunModel[5] = new ModelRendererTurbo(this, 80, 25, textureX, textureY); // underbarrel
		gunModel[6] = new ModelRendererTurbo(this, 1, 57, textureX, textureY); // gripConnector
		gunModel[7] = new ModelRendererTurbo(this, 1, 70, textureX, textureY); // gripConnector2
		gunModel[8] = new ModelRendererTurbo(this, 147, 52, textureX, textureY); // body6
		gunModel[9] = new ModelRendererTurbo(this, 80, 1, textureX, textureY); // barrel1
		gunModel[10] = new ModelRendererTurbo(this, 80, 1, textureX, textureY); // barrel1-2
		gunModel[11] = new ModelRendererTurbo(this, 80, 12, textureX, textureY); // barrel2
		gunModel[12] = new ModelRendererTurbo(this, 1, 23, textureX, textureY); // rail
		gunModel[13] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // ironSightBase1
		gunModel[14] = new ModelRendererTurbo(this, 52, 33, textureX, textureY); // railBlock
		gunModel[15] = new ModelRendererTurbo(this, 52, 33, textureX, textureY); // Box 23
		gunModel[16] = new ModelRendererTurbo(this, 52, 33, textureX, textureY); // Box 24
		gunModel[17] = new ModelRendererTurbo(this, 52, 33, textureX, textureY); // Box 25
		gunModel[18] = new ModelRendererTurbo(this, 52, 33, textureX, textureY); // Box 26
		gunModel[19] = new ModelRendererTurbo(this, 191, 25, textureX, textureY); // Box 27
		gunModel[20] = new ModelRendererTurbo(this, 191, 38, textureX, textureY); // Box 28
		gunModel[21] = new ModelRendererTurbo(this, 191, 25, textureX, textureY); // Box 29
		gunModel[22] = new ModelRendererTurbo(this, 80, 84, textureX, textureY); // bodyRail1
		gunModel[23] = new ModelRendererTurbo(this, 80, 84, textureX, textureY); // Box 1
		gunModel[24] = new ModelRendererTurbo(this, 80, 84, textureX, textureY); // Box 2
		gunModel[25] = new ModelRendererTurbo(this, 80, 84, textureX, textureY); // Box 3
		gunModel[26] = new ModelRendererTurbo(this, 80, 84, textureX, textureY); // Box 4
		gunModel[27] = new ModelRendererTurbo(this, 80, 84, textureX, textureY); // Box 5

		gunModel[0].addShapeBox(0F, 0F, 0F, 23, 13, 10, 0F,-2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body1
		gunModel[0].setRotationPoint(-10F, -23F, -5F);

		gunModel[1].addShapeBox(0F, 0F, 0F, 36, 2, 10, 0F,0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body2
		gunModel[1].setRotationPoint(-8F, -25F, -5F);

		gunModel[2].addBox(0F, 0F, 0F, 15, 7, 1, 0F); // body3
		gunModel[2].setRotationPoint(13F, -17F, -5F);

		gunModel[3].addBox(0F, 0F, 0F, 15, 1, 1, 0F); // body4
		gunModel[3].setRotationPoint(13F, -23F, -5F);

		gunModel[4].addBox(0F, 0F, 0F, 2, 5, 1, 0F); // body5
		gunModel[4].setRotationPoint(26F, -22F, -5F);

		gunModel[5].addBox(0F, 0F, 0F, 49, 6, 6, 0F); // underbarrel
		gunModel[5].setRotationPoint(28F, -16F, -3F);

		gunModel[6].addShapeBox(0F, 0F, 0F, 12, 2, 10, 0F,2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // gripConnector
		gunModel[6].setRotationPoint(-8F, -10F, -5F);

		gunModel[7].addBox(0F, 0F, 0F, 7, 3, 10, 0F); // gripConnector2
		gunModel[7].setRotationPoint(-3F, -8F, -5F);

		gunModel[8].addBox(0F, 0F, 0F, 15, 13, 9, 0F); // body6
		gunModel[8].setRotationPoint(13F, -23F, -4F);

		gunModel[9].addShapeBox(0F, 0F, 0F, 55, 2, 8, 0F,0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // barrel1
		gunModel[9].setRotationPoint(28F, -24F, -4F);

		gunModel[10].addShapeBox(0F, 0F, 0F, 55, 2, 8, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // barrel1-2
		gunModel[10].setRotationPoint(28F, -18F, -4F);

		gunModel[11].addBox(0F, 0F, 0F, 55, 4, 8, 0F); // barrel2
		gunModel[11].setRotationPoint(28F, -22F, -4F);

		gunModel[12].addBox(0F, 0F, 0F, 27, 2, 7, 0F); // rail
		gunModel[12].setRotationPoint(1.1F, -26F, -3.5F);

		gunModel[13].addBox(0F, 0F, 0F, 7, 4, 9, 0F); // ironSightBase1
		gunModel[13].setRotationPoint(-6F, -28F, -4.5F);

		gunModel[14].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // railBlock
		gunModel[14].setRotationPoint(1F, -28F, -3.5F);

		gunModel[15].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 23
		gunModel[15].setRotationPoint(7F, -28F, -3.5F);

		gunModel[16].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 24
		gunModel[16].setRotationPoint(19F, -28F, -3.5F);

		gunModel[17].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 25
		gunModel[17].setRotationPoint(13F, -28F, -3.5F);

		gunModel[18].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 26
		gunModel[18].setRotationPoint(25F, -28F, -3.5F);

		gunModel[19].addBox(0F, 0F, 0F, 2, 6, 6, 0F); // Box 27
		gunModel[19].setRotationPoint(81F, -16F, -3F);

		gunModel[20].addBox(0F, 0F, 0F, 4, 5, 5, 0F); // Box 28
		gunModel[20].setRotationPoint(77F, -15.5F, -2.5F);

		gunModel[21].addBox(0F, 0F, 0F, 2, 6, 6, 0F); // Box 29
		gunModel[21].setRotationPoint(78F, -16F, -3F);

		gunModel[22].addShapeBox(0F, 0F, 0F, 1, 6, 11, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F); // bodyRail1
		gunModel[22].setRotationPoint(-6F, -23F, -5.5F);

		gunModel[23].addShapeBox(0F, 0F, 0F, 1, 6, 11, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F); // Box 1
		gunModel[23].setRotationPoint(-4F, -23F, -5.5F);

		gunModel[24].addShapeBox(0F, 0F, 0F, 1, 6, 11, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F); // Box 2
		gunModel[24].setRotationPoint(-2F, -23F, -5.5F);

		gunModel[25].addShapeBox(0F, 0F, 0F, 1, 6, 11, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F); // Box 3
		gunModel[25].setRotationPoint(0F, -23F, -5.5F);

		gunModel[26].addShapeBox(0F, 0F, 0F, 1, 6, 11, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F); // Box 4
		gunModel[26].setRotationPoint(2F, -23F, -5.5F);

		gunModel[27].addShapeBox(0F, 0F, 0F, 1, 6, 11, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F); // Box 5
		gunModel[27].setRotationPoint(4F, -23F, -5.5F);


		defaultScopeModel = new ModelRendererTurbo[8];
		defaultScopeModel[0] = new ModelRendererTurbo(this, 65, 8, textureX, textureY); // ironSight3
		defaultScopeModel[1] = new ModelRendererTurbo(this, 34, 4, textureX, textureY); // ironSightBase2
		defaultScopeModel[2] = new ModelRendererTurbo(this, 25, 1, textureX, textureY); // ironSight1-2
		defaultScopeModel[3] = new ModelRendererTurbo(this, 25, 1, textureX, textureY); // ironSight1
		defaultScopeModel[4] = new ModelRendererTurbo(this, 1, 5, textureX, textureY); // ironBolt1
		defaultScopeModel[5] = new ModelRendererTurbo(this, 1, 5, textureX, textureY); // ironBolt1-2
		defaultScopeModel[6] = new ModelRendererTurbo(this, 65, 14, textureX, textureY); // Box 19
		defaultScopeModel[7] = new ModelRendererTurbo(this, 65, 1, textureX, textureY); // Box 21

		defaultScopeModel[0].addShapeBox(0F, 0F, 0F, 4, 4, 1, 0F,0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // ironSight3
		defaultScopeModel[0].setRotationPoint(78F, -32F, -0.5F);

		defaultScopeModel[1].addBox(0F, 0F, 0F, 7, 2, 8, 0F); // ironSightBase2
		defaultScopeModel[1].setRotationPoint(-6F, -30F, -4F);

		defaultScopeModel[2].addBox(0F, 0F, 0F, 5, 6, 2, 0F); // ironSight1-2
		defaultScopeModel[2].setRotationPoint(-4F, -36F, -4F);

		defaultScopeModel[3].addBox(0F, 0F, 0F, 5, 6, 2, 0F); // ironSight1
		defaultScopeModel[3].setRotationPoint(-4F, -36F, 2F);

		defaultScopeModel[4].addBox(0F, 0F, 0F, 3, 3, 1, 0F); // ironBolt1
		defaultScopeModel[4].setRotationPoint(-3F, -32F, 4F);

		defaultScopeModel[5].addBox(0F, 0F, 0F, 3, 3, 1, 0F); // ironBolt1-2
		defaultScopeModel[5].setRotationPoint(-3F, -32F, -5F);

		defaultScopeModel[6].addShapeBox(0F, 0F, 0F, 4, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F); // Box 19
		defaultScopeModel[6].setRotationPoint(78F, -28F, -0.5F);

		defaultScopeModel[7].addShapeBox(0F, 0F, 0F, 4, 3, 3, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // Box 21
		defaultScopeModel[7].setRotationPoint(78F, -27F, -1.5F);


		defaultGripModel = new ModelRendererTurbo[2];
		defaultGripModel[0] = new ModelRendererTurbo(this, 1, 84, textureX, textureY); // grip1
		defaultGripModel[1] = new ModelRendererTurbo(this, 1, 110, textureX, textureY); // grip2

		defaultGripModel[0].addShapeBox(0F, 0F, 0F, 12, 16, 9, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, -2F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 3F, -2F, 0F); // grip1
		defaultGripModel[0].setRotationPoint(-8F, -8F, -4.5F);

		defaultGripModel[1].addBox(0F, 0F, 0F, 13, 2, 9, 0F); // grip2
		defaultGripModel[1].setRotationPoint(-11F, 6F, -4.5F);
		defaultGripModel[1].rotateAngleZ = -0.17453293F;


		ammoModel = new ModelRendererTurbo[6];
		ammoModel[0] = new ModelRendererTurbo(this, 80, 104, textureX, textureY); // Box 7
		ammoModel[1] = new ModelRendererTurbo(this, 80, 104, textureX, textureY); // Box 8
		ammoModel[2] = new ModelRendererTurbo(this, 80, 104, textureX, textureY); // Box 9
		ammoModel[3] = new ModelRendererTurbo(this, 115, 104, textureX, textureY); // Box 10
		ammoModel[4] = new ModelRendererTurbo(this, 115, 104, textureX, textureY); // Box 11
		ammoModel[5] = new ModelRendererTurbo(this, 115, 104, textureX, textureY); // Box 12

		ammoModel[0].addShapeBox(0F, 0F, 0F, 11, 2, 6, 0F,0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 7
		ammoModel[0].setRotationPoint(16F, -17F, -3F);

		ammoModel[1].addBox(0F, 0F, 0F, 11, 2, 6, 0F); // Box 8
		ammoModel[1].setRotationPoint(16F, -15F, -3F);

		ammoModel[2].addShapeBox(0F, 0F, 0F, 11, 2, 6, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 9
		ammoModel[2].setRotationPoint(16F, -13F, -3F);

		ammoModel[3].addShapeBox(0F, 0F, 0F, 2, 2, 6, 0F,0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 10
		ammoModel[3].setRotationPoint(14F, -17F, -3F);

		ammoModel[4].addBox(0F, 0F, 0F, 2, 2, 6, 0F); // Box 11
		ammoModel[4].setRotationPoint(14F, -15F, -3F);

		ammoModel[5].addShapeBox(0F, 0F, 0F, 2, 2, 6, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 12
		ammoModel[5].setRotationPoint(14F, -13F, -3F);


		pumpModel = new ModelRendererTurbo[3];
		pumpModel[0] = new ModelRendererTurbo(this, 1, 33, textureX, textureY); // pump1
		pumpModel[1] = new ModelRendererTurbo(this, 1, 16, textureX, textureY); // boltSlider
		pumpModel[2] = new ModelRendererTurbo(this, 1, 48, textureX, textureY); // pump2

		pumpModel[0].addBox(0F, 0F, 0F, 18, 7, 7, 0F); // pump1
		pumpModel[0].setRotationPoint(53F, -16.5F, -3.5F);

		pumpModel[1].addShapeBox(0F, 0F, 0F, 13, 5, 1, 0F,0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // boltSlider
		pumpModel[1].setRotationPoint(13F, -22F, -4.75F);

		pumpModel[2].addShapeBox(0F, 0F, 0F, 18, 1, 7, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F); // pump2
		pumpModel[2].setRotationPoint(53F, -9.5F, -3.5F);



		barrelAttachPoint = new Vector3f(0F /16F, 0F /16F, 0F /16F);
		stockAttachPoint = new Vector3f(0F /16F, 0F /16F, 0F /16F);
		scopeAttachPoint = new Vector3f(0F /16F, 0F /16F, 0F /16F);
		gripAttachPoint = new Vector3f(0 /16F, 0F /16F, 0F /16F);

		gunSlideDistance = 0F;
		animationType = EnumAnimationType.SHOTGUN;

		numBulletsInReloadAnimation = 5;
		tiltGunTime = 0.159F;
		unloadClipTime = 0.0F;
		loadClipTime = 0.708F;
		untiltGunTime = 0.133F;

		pumpDelay = 10;
		pumpTime = 7;
		pumpDelayAfterReload = 110;
		pumpHandleDistance = 1.2F;

		translateAll(0F, 0F, 0F);


		flipAll();
	}
}