package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelSteelMonochromaticBluss extends ModelGun
{
	int textureX = 512;
	int textureY = 256;

	public ModelSteelMonochromaticBluss()
	{
		gunModel = new ModelRendererTurbo[38];
		gunModel[0] = new ModelRendererTurbo(this, 64, 45, textureX, textureY); // body1
		gunModel[1] = new ModelRendererTurbo(this, 136, 45, textureX, textureY); // body2
		gunModel[2] = new ModelRendererTurbo(this, 136, 45, textureX, textureY); // body2-2
		gunModel[3] = new ModelRendererTurbo(this, 111, 45, textureX, textureY); // body3
		gunModel[4] = new ModelRendererTurbo(this, 64, 64, textureX, textureY); // body4
		gunModel[5] = new ModelRendererTurbo(this, 64, 64, textureX, textureY); // body4-2
		gunModel[6] = new ModelRendererTurbo(this, 64, 24, textureX, textureY); // bottomBarrel
		gunModel[7] = new ModelRendererTurbo(this, 64, 35, textureX, textureY); // brassRail1
		gunModel[8] = new ModelRendererTurbo(this, 64, 35, textureX, textureY); // brassRail1-2
		gunModel[9] = new ModelRendererTurbo(this, 64, 35, textureX, textureY); // brassRail1-3
		gunModel[10] = new ModelRendererTurbo(this, 64, 35, textureX, textureY); // brassRail1-4
		gunModel[11] = new ModelRendererTurbo(this, 171, 26, textureX, textureY); // breach1
		gunModel[12] = new ModelRendererTurbo(this, 171, 26, textureX, textureY); // breach1-2
		gunModel[13] = new ModelRendererTurbo(this, 171, 26, textureX, textureY); // breach1-3
		gunModel[14] = new ModelRendererTurbo(this, 1, 25, textureX, textureY); // canister1
		gunModel[15] = new ModelRendererTurbo(this, 1, 25, textureX, textureY); // canister2
		gunModel[16] = new ModelRendererTurbo(this, 64, 39, textureX, textureY); // foreRail1
		gunModel[17] = new ModelRendererTurbo(this, 133, 39, textureX, textureY); // foreRail2
		gunModel[18] = new ModelRendererTurbo(this, 133, 39, textureX, textureY); // foreRail2-2
		gunModel[19] = new ModelRendererTurbo(this, 133, 39, textureX, textureY); // foreRail2-3
		gunModel[20] = new ModelRendererTurbo(this, 1, 62, textureX, textureY); // handGuard1
		gunModel[21] = new ModelRendererTurbo(this, 1, 72, textureX, textureY); // handGuard2
		gunModel[22] = new ModelRendererTurbo(this, 1, 80, textureX, textureY); // handGuard3
		gunModel[23] = new ModelRendererTurbo(this, 24, 80, textureX, textureY); // handGuard4
		gunModel[24] = new ModelRendererTurbo(this, 1, 54, textureX, textureY); // handGuard5
		gunModel[25] = new ModelRendererTurbo(this, 1, 46, textureX, textureY); // handGuard6
		gunModel[26] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // ironSight
		gunModel[27] = new ModelRendererTurbo(this, 1, 22, textureX, textureY); // loader2
		gunModel[28] = new ModelRendererTurbo(this, 1, 22, textureX, textureY); // loader2-2
		gunModel[29] = new ModelRendererTurbo(this, 64, 12, textureX, textureY); // middleBarrel
		gunModel[30] = new ModelRendererTurbo(this, 171, 1, textureX, textureY); // muzzle1
		gunModel[31] = new ModelRendererTurbo(this, 171, 1, textureX, textureY); // muzzle1-2
		gunModel[32] = new ModelRendererTurbo(this, 171, 12, textureX, textureY); // muzzle2
		gunModel[33] = new ModelRendererTurbo(this, 142, 91, textureX, textureY); // stockPart7
		gunModel[34] = new ModelRendererTurbo(this, 64, 1, textureX, textureY); // topBarrel
		gunModel[35] = new ModelRendererTurbo(this, 1, 13, textureX, textureY); // topBase
		gunModel[36] = new ModelRendererTurbo(this, 40, 13, textureX, textureY); // topBase2
		gunModel[37] = new ModelRendererTurbo(this, 40, 13, textureX, textureY); // topBase2-2

		gunModel[0].addBox(0F, 0F, 0F, 15, 10, 8, 0F); // body1
		gunModel[0].setRotationPoint(-12F, -16F, -4F);

		gunModel[1].addShapeBox(0F, 0F, 0F, 15, 2, 8, 0F,0F, 0F, -2F,0F, 0F, -2F,0F, 0F, -2F,0F, 0F, -2F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F); // body2
		gunModel[1].setRotationPoint(-12F, -18F, -4F);

		gunModel[2].addShapeBox(0F, 0F, 0F, 15, 2, 8, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, -2F,0F, 0F, -2F,0F, 0F, -2F,0F, 0F, -2F); // body2-2
		gunModel[2].setRotationPoint(-12F, -6F, -4F);

		gunModel[3].addShapeBox(0F, 0F, 0F, 4, 10, 8, 0F,0F, -4F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, -4F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F); // body3
		gunModel[3].setRotationPoint(-16F, -16F, -4F);

		gunModel[4].addShapeBox(0F, 0F, 0F, 4, 2, 8, 0F,0F, -4F, -2F,0F, 0F, -2F,0F, 0F, -2F,0F, -4F, -2F,0F, 4F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 4F, 0F); // body4
		gunModel[4].setRotationPoint(-16F, -18F, -4F);

		gunModel[5].addShapeBox(0F, 0F, 0F, 4, 2, 8, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, -2F,0F, 0F, -2F,0F, 0F, -2F,0F, 0F, -2F); // body4-2
		gunModel[5].setRotationPoint(-16F, -6F, -4F);

		gunModel[6].addShapeBox(0F, 0F, 0F, 45, 2, 8, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, -2F,0F, 0F, -2F,0F, 0F, -2F,0F, 0F, -2F); // bottomBarrel
		gunModel[6].setRotationPoint(3F, -11F, -4F);

		gunModel[7].addShapeBox(0F, 0F, 0F, 48, 1, 2, 0F,0F, 0F, -0.5F,0F, 0F, -0.5F,0F, 0F, -0.5F,0F, 0F, -0.5F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F); // brassRail1
		gunModel[7].setRotationPoint(2.9F, -9.5F, 2F);

		gunModel[8].addShapeBox(0F, 0F, 0F, 48, 1, 2, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, -0.5F,0F, 0F, -0.5F,0F, 0F, -0.5F,0F, 0F, -0.5F); // brassRail1-2
		gunModel[8].setRotationPoint(2.9F, -8.5F, 2F);

		gunModel[9].addShapeBox(0F, 0F, 0F, 48, 1, 2, 0F,0F, 0F, -0.5F,0F, 0F, -0.5F,0F, 0F, -0.5F,0F, 0F, -0.5F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F); // brassRail1-3
		gunModel[9].setRotationPoint(2.9F, -9.5F, -4F);

		gunModel[10].addShapeBox(0F, 0F, 0F, 48, 1, 2, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, -0.5F,0F, 0F, -0.5F,0F, 0F, -0.5F,0F, 0F, -0.5F); // brassRail1-4
		gunModel[10].setRotationPoint(2.9F, -8.5F, -4F);

		gunModel[11].addShapeBox(0F, 0F, 0F, 1, 2, 6, 0F,0F, 0F, -2F,0F, 0F, -2F,0F, 0F, -2F,0F, 0F, -2F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F); // breach1
		gunModel[11].setRotationPoint(48F, -15.5F, -3F);

		gunModel[12].addBox(0F, 0F, 0F, 1, 2, 6, 0F); // breach1-2
		gunModel[12].setRotationPoint(48F, -13.5F, -3F);

		gunModel[13].addShapeBox(0F, 0F, 0F, 1, 2, 6, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, -2F,0F, 0F, -2F,0F, 0F, -2F,0F, 0F, -2F); // breach1-3
		gunModel[13].setRotationPoint(48F, -11.5F, -3F);

		gunModel[14].addShapeBox(0F, 0F, 0F, 11, 6, 1, 0F,0F, -1F, 0F,0F, -1F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, -1F, 0F,0F, -1F, 0F,0F, 0F, 0F,0F, 0F, 0F); // canister1
		gunModel[14].setRotationPoint(-10F, -14F, -5F);

		gunModel[15].addShapeBox(0F, 0F, 0F, 11, 6, 1, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, -1F, 0F,0F, -1F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, -1F, 0F,0F, -1F, 0F); // canister2
		gunModel[15].setRotationPoint(-10F, -14F, 4F);

		gunModel[16].addBox(0F, 0F, 0F, 30, 1, 4, 0F); // foreRail1
		gunModel[16].setRotationPoint(3F, -18F, -2F);

		gunModel[17].addBox(0F, 0F, 0F, 5, 1, 4, 0F); // foreRail2
		gunModel[17].setRotationPoint(8F, -17F, -2F);

		gunModel[18].addBox(0F, 0F, 0F, 5, 1, 4, 0F); // foreRail2-2
		gunModel[18].setRotationPoint(18F, -17F, -2F);

		gunModel[19].addBox(0F, 0F, 0F, 5, 1, 4, 0F); // foreRail2-3
		gunModel[19].setRotationPoint(28F, -17F, -2F);

		gunModel[20].addBox(0F, 0F, 0F, 16, 4, 5, 0F); // handGuard1
		gunModel[20].setRotationPoint(3F, -10F, -2.5F);

		gunModel[21].addShapeBox(0F, 0F, 0F, 16, 2, 5, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, -1.5F,0F, 0F, -1.5F,0F, 0F, -1.5F,0F, 0F, -1.5F); // handGuard2
		gunModel[21].setRotationPoint(3F, -6F, -2.5F);

		gunModel[22].addShapeBox(0F, 0F, 0F, 6, 4, 5, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, -2F, 0F,0F, -2F, 0F,0F, 0F, 0F); // handGuard3
		gunModel[22].setRotationPoint(19F, -10F, -2.5F);

		gunModel[23].addShapeBox(0F, 0F, 0F, 6, 2, 5, 0F,0F, 0F, 0F,0F, 2F, 0F,0F, 2F, 0F,0F, 0F, 0F,0F, 0F, -1.5F,0F, -2F, -1.5F,0F, -2F, -1.5F,0F, 0F, -1.5F); // handGuard4
		gunModel[23].setRotationPoint(19F, -6F, -2.5F);

		gunModel[24].addBox(0F, 0F, 0F, 22, 2, 5, 0F); // handGuard5
		gunModel[24].setRotationPoint(25F, -10F, -2.5F);

		gunModel[25].addShapeBox(0F, 0F, 0F, 22, 2, 5, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, -1.5F,0F, 0F, -1.5F,0F, 0F, -1.5F,0F, 0F, -1.5F); // handGuard6
		gunModel[25].setRotationPoint(25F, -8F, -2.5F);

		gunModel[26].addBox(0F, 0F, 0F, 4, 3, 1, 0F); // ironSight
		gunModel[26].setRotationPoint(29F, -21F, -0.5F);

		gunModel[27].addShapeBox(0F, 0F, 0F, 11, 1, 1, 0F,0F, 0F, -0.2F,0F, 0F, -0.2F,0F, 0F, -0.2F,0F, 0F, -0.2F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F); // loader2
		gunModel[27].setRotationPoint(-10F, -18.8F, -2F);

		gunModel[28].addShapeBox(0F, 0F, 0F, 11, 1, 1, 0F,0F, 0F, -0.2F,0F, 0F, -0.2F,0F, 0F, -0.2F,0F, 0F, -0.2F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F); // loader2-2
		gunModel[28].setRotationPoint(-10F, -18.8F, 1F);

		gunModel[29].addBox(0F, 0F, 0F, 45, 3, 8, 0F); // middleBarrel
		gunModel[29].setRotationPoint(3F, -14F, -4F);

		gunModel[30].addShapeBox(0F, 0F, 0F, 3, 2, 8, 0F,0F, 0F, -2F,0F, 0F, -2F,0F, 0F, -2F,0F, 0F, -2F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F); // muzzle1
		gunModel[30].setRotationPoint(48.5F, -16F, -4F);

		gunModel[31].addShapeBox(0F, 0F, 0F, 3, 2, 8, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, -2F,0F, 0F, -2F,0F, 0F, -2F,0F, 0F, -2F); // muzzle1-2
		gunModel[31].setRotationPoint(48.5F, -11F, -4F);

		gunModel[32].addBox(0F, 0F, 0F, 3, 3, 8, 0F); // muzzle2
		gunModel[32].setRotationPoint(48.5F, -14F, -4F);

		gunModel[33].addShapeBox(0F, 0F, 0F, 2, 2, 8, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, -2F,0F, 0F, -2F,0F, 0F, -2F,0F, 0F, -2F); // stockPart7
		gunModel[33].setRotationPoint(-48F, 6F, -4F);

		gunModel[34].addShapeBox(0F, 0F, 0F, 45, 2, 8, 0F,0F, 0F, -2F,0F, 0F, -2F,0F, 0F, -2F,0F, 0F, -2F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F); // topBarrel
		gunModel[34].setRotationPoint(3F, -16F, -4F);

		gunModel[35].addBox(0F, 0F, 0F, 13, 2, 6, 0F); // topBase
		gunModel[35].setRotationPoint(-11F, -18.5F, -3F);

		gunModel[36].addShapeBox(0F, 0F, 0F, 1, 1, 6, 0F,0F, 0F, -0.5F,0F, 0F, -0.5F,0F, 0F, -0.5F,0F, 0F, -0.5F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F); // topBase2
		gunModel[36].setRotationPoint(1F, -19.5F, -3F);

		gunModel[37].addShapeBox(0F, 0F, 0F, 1, 1, 6, 0F,0F, 0F, -0.5F,0F, 0F, -0.5F,0F, 0F, -0.5F,0F, 0F, -0.5F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F); // topBase2-2
		gunModel[37].setRotationPoint(-11F, -19.5F, -3F);


		defaultStockModel = new ModelRendererTurbo[7];
		defaultStockModel[0] = new ModelRendererTurbo(this, 85, 118, textureX, textureY); // stockPart1
		defaultStockModel[1] = new ModelRendererTurbo(this, 121, 103, textureX, textureY); // stockPart2
		defaultStockModel[2] = new ModelRendererTurbo(this, 64, 80, textureX, textureY); // stockPart3
		defaultStockModel[3] = new ModelRendererTurbo(this, 64, 103, textureX, textureY); // stockPart4
		defaultStockModel[4] = new ModelRendererTurbo(this, 64, 91, textureX, textureY); // stockPart5
		defaultStockModel[5] = new ModelRendererTurbo(this, 64, 118, textureX, textureY); // stockPart6
		defaultStockModel[6] = new ModelRendererTurbo(this, 121, 91, textureX, textureY); // stockPart8

		defaultStockModel[0].addShapeBox(0F, 0F, 0F, 10, 2, 8, 0F,0F, -4F, -2F,0F, 0F, -2F,0F, 0F, -2F,0F, -4F, -2F,0F, 4F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 4F, 0F); // stockPart1
		defaultStockModel[0].setRotationPoint(-26F, -14F, -4F);

		defaultStockModel[1].addShapeBox(0F, 0F, 0F, 10, 6, 8, 0F,0F, -4F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, -4F, 0F,0F, 4F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 4F, 0F); // stockPart2
		defaultStockModel[1].setRotationPoint(-26F, -12F, -4F);

		defaultStockModel[2].addShapeBox(0F, 0F, 0F, 30, 2, 8, 0F,0F, -12F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, -12F, 0F,0F, 12F, -2F,0F, 0F, -2F,0F, 0F, -2F,0F, 12F, -2F); // stockPart3
		defaultStockModel[2].setRotationPoint(-46F, -6F, -4F);

		defaultStockModel[3].addShapeBox(0F, 0F, 0F, 20, 6, 8, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 8F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 8F, 0F); // stockPart4
		defaultStockModel[3].setRotationPoint(-46F, -8F, -4F);

		defaultStockModel[4].addShapeBox(0F, 0F, 0F, 20, 3, 8, 0F,0F, 0F, -2F,-2F, 0F, -2F,-2F, 0F, -2F,0F, 0F, -2F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F); // stockPart5
		defaultStockModel[4].setRotationPoint(-46F, -11F, -4F);

		defaultStockModel[5].addBox(0F, 0F, 0F, 2, 14, 8, 0F); // stockPart6
		defaultStockModel[5].setRotationPoint(-48F, -8F, -4F);

		defaultStockModel[6].addShapeBox(0F, 0F, 0F, 2, 3, 8, 0F,0F, 0F, -2F,0F, 0F, -2F,0F, 0F, -2F,0F, 0F, -2F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F); // stockPart8
		defaultStockModel[6].setRotationPoint(-48F, -11F, -4F);


		ammoModel = new ModelRendererTurbo[3];
		ammoModel[0] = new ModelRendererTurbo(this, 1, 33, textureX, textureY); // steelBullet1
		ammoModel[1] = new ModelRendererTurbo(this, 1, 33, textureX, textureY); // steelBullet1-2
		ammoModel[2] = new ModelRendererTurbo(this, 1, 39, textureX, textureY); // steelBullet2

		ammoModel[0].addShapeBox(0F, 0F, 0F, 4, 1, 4, 0F,-1F, 0F, -1F,-1F, 0F, -1F,-1F, 0F, -1F,-1F, 0F, -1F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F); // steelBullet1
		ammoModel[0].setRotationPoint(42.5F, -14.5F, -2F);

		ammoModel[1].addShapeBox(0F, 0F, 0F, 4, 1, 4, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,-1F, 0F, -1F,-1F, 0F, -1F,-1F, 0F, -1F,-1F, 0F, -1F); // steelBullet1-2
		ammoModel[1].setRotationPoint(42.5F, -11.5F, -2F);

		ammoModel[2].addBox(0F, 0F, 0F, 4, 2, 4, 0F); // steelBullet2
		ammoModel[2].setRotationPoint(42.5F, -13.5F, -2F);


		pumpModel = new ModelRendererTurbo[6];
		pumpModel[0] = new ModelRendererTurbo(this, 1, 6, textureX, textureY); // bolt1
		pumpModel[1] = new ModelRendererTurbo(this, 1, 6, textureX, textureY); // bolt2-2
		pumpModel[2] = new ModelRendererTurbo(this, 16, 8, textureX, textureY); // bolt3
		pumpModel[3] = new ModelRendererTurbo(this, 16, 8, textureX, textureY); // bolt3-2
		pumpModel[4] = new ModelRendererTurbo(this, 16, 8, textureX, textureY); // bolt3-3
		pumpModel[5] = new ModelRendererTurbo(this, 29, 7, textureX, textureY); // loader1

		pumpModel[0].addShapeBox(0F, 0F, 0F, 2, 1, 5, 0F,-0.5F, 0F, 0F,-0.5F, 0F, 0F,-0.5F, 0F, 0F,-0.5F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F); // bolt1
		pumpModel[0].setRotationPoint(-1F, -18F, -8F);

		pumpModel[1].addShapeBox(0F, 0F, 0F, 2, 1, 5, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,-0.5F, 0F, 0F,-0.5F, 0F, 0F,-0.5F, 0F, 0F,-0.5F, 0F, 0F); // bolt2-2
		pumpModel[1].setRotationPoint(-1F, -17F, -8F);

		pumpModel[2].addShapeBox(0F, 0F, 0F, 3, 1, 3, 0F,-1F, 0F, -1F,-1F, 0F, -1F,-1F, 0F, -1F,-1F, 0F, -1F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F); // bolt3
		pumpModel[2].setRotationPoint(-1.5F, -18.5F, -10F);

		pumpModel[3].addBox(0F, 0F, 0F, 3, 1, 3, 0F); // bolt3-2
		pumpModel[3].setRotationPoint(-1.5F, -17.5F, -10F);

		pumpModel[4].addShapeBox(0F, 0F, 0F, 3, 1, 3, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,-1F, 0F, -1F,-1F, 0F, -1F,-1F, 0F, -1F,-1F, 0F, -1F); // bolt3-3
		pumpModel[4].setRotationPoint(-1.5F, -16.5F, -10F);

		pumpModel[5].addBox(0F, 0F, 0F, 2, 1, 4, 0F); // loader1
		pumpModel[5].setRotationPoint(-1F, -19.2F, -2F);



		barrelAttachPoint = new Vector3f(0F /16F, 0F /16F, 0F /16F);
		stockAttachPoint = new Vector3f(0F /16F, 0F /16F, 0F /16F);
		scopeAttachPoint = new Vector3f(0F /16F, 0F /16F, 0F /16F);
		gripAttachPoint = new Vector3f(0 /16F, 0F /16F, 0F /16F);

		gunSlideDistance = 0F;
		animationType = EnumAnimationType.END_LOADED;

		pumpTime = 8;
		pumpDelayAfterReload = 72;
		pumpHandleDistance = 0.6F;


		translateAll(4F, -6F, 0F);


		flipAll();
	}
}