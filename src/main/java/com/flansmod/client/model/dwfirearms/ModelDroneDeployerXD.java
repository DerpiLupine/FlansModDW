package com.flansmod.client.model.dwfirearms; //Path where the model is located

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelDroneDeployerXD extends ModelGun //Same as Filename
{
	int textureX = 512;
	int textureY = 256;

	public ModelDroneDeployerXD() //Same as Filename
	{
		gunModel = new ModelRendererTurbo[93];
		gunModel[0] = new ModelRendererTurbo(this, 112, 10, textureX, textureY); // barrelMainLeft
		gunModel[1] = new ModelRendererTurbo(this, 112, 1, textureX, textureY); // barrelMainMiddle
		gunModel[2] = new ModelRendererTurbo(this, 112, 10, textureX, textureY); // barrelMainRight
		gunModel[3] = new ModelRendererTurbo(this, 44, 96, textureX, textureY); // body1
		gunModel[4] = new ModelRendererTurbo(this, 1, 82, textureX, textureY); // body6
		gunModel[5] = new ModelRendererTurbo(this, 1, 37, textureX, textureY); // foreRail1
		gunModel[6] = new ModelRendererTurbo(this, 201, 67, textureX, textureY); // body12
		gunModel[7] = new ModelRendererTurbo(this, 66, 35, textureX, textureY); // body17
		gunModel[8] = new ModelRendererTurbo(this, 1, 96, textureX, textureY); // Import GU,body4
		gunModel[9] = new ModelRendererTurbo(this, 112, 22, textureX, textureY); // Import GU,body9
		gunModel[10] = new ModelRendererTurbo(this, 112, 33, textureX, textureY); // railPart3
		gunModel[11] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Import GU,tacRailEnd1
		gunModel[12] = new ModelRendererTurbo(this, 28, 9, textureX, textureY); // ironSightPart1
		gunModel[13] = new ModelRendererTurbo(this, 28, 1, textureX, textureY); // ironSightPart4
		gunModel[14] = new ModelRendererTurbo(this, 214, 86, textureX, textureY); // Box 36
		gunModel[15] = new ModelRendererTurbo(this, 214, 86, textureX, textureY); // Box 37
		gunModel[16] = new ModelRendererTurbo(this, 187, 85, textureX, textureY); // Box 38
		gunModel[17] = new ModelRendererTurbo(this, 242, 72, textureX, textureY); // Box 39
		gunModel[18] = new ModelRendererTurbo(this, 1, 66, textureX, textureY); // Box 1
		gunModel[19] = new ModelRendererTurbo(this, 242, 72, textureX, textureY); // Box 2
		gunModel[20] = new ModelRendererTurbo(this, 66, 40, textureX, textureY); // Box 3
		gunModel[21] = new ModelRendererTurbo(this, 243, 102, textureX, textureY); // Box 11
		gunModel[22] = new ModelRendererTurbo(this, 222, 101, textureX, textureY); // Box 12
		gunModel[23] = new ModelRendererTurbo(this, 243, 102, textureX, textureY); // Box 13
		gunModel[24] = new ModelRendererTurbo(this, 1, 25, textureX, textureY); // Box 14
		gunModel[25] = new ModelRendererTurbo(this, 112, 57, textureX, textureY); // Box 16
		gunModel[26] = new ModelRendererTurbo(this, 198, 102, textureX, textureY); // Box 22
		gunModel[27] = new ModelRendererTurbo(this, 209, 101, textureX, textureY); // Box 23
		gunModel[28] = new ModelRendererTurbo(this, 198, 102, textureX, textureY); // Box 24
		gunModel[29] = new ModelRendererTurbo(this, 185, 47, textureX, textureY); // Box 26
		gunModel[30] = new ModelRendererTurbo(this, 112, 46, textureX, textureY); // Box 27
		gunModel[31] = new ModelRendererTurbo(this, 178, 67, textureX, textureY); // Box 29
		gunModel[32] = new ModelRendererTurbo(this, 159, 67, textureX, textureY); // Box 30
		gunModel[33] = new ModelRendererTurbo(this, 159, 67, textureX, textureY); // Box 31
		gunModel[34] = new ModelRendererTurbo(this, 159, 67, textureX, textureY); // Box 32
		gunModel[35] = new ModelRendererTurbo(this, 112, 67, textureX, textureY); // Box 33
		gunModel[36] = new ModelRendererTurbo(this, 112, 80, textureX, textureY); // Box 34
		gunModel[37] = new ModelRendererTurbo(this, 112, 19, textureX, textureY); // Box 35
		gunModel[38] = new ModelRendererTurbo(this, 112, 19, textureX, textureY); // Box 36
		gunModel[39] = new ModelRendererTurbo(this, 1, 171, textureX, textureY); // Box 1
		gunModel[40] = new ModelRendererTurbo(this, 1, 145, textureX, textureY); // Box 2
		gunModel[41] = new ModelRendererTurbo(this, 1, 160, textureX, textureY); // Box 3
		gunModel[42] = new ModelRendererTurbo(this, 1, 109, textureX, textureY); // Box 4
		gunModel[43] = new ModelRendererTurbo(this, 1, 123, textureX, textureY); // Box 5
		gunModel[44] = new ModelRendererTurbo(this, 32, 134, textureX, textureY); // Box 6
		gunModel[45] = new ModelRendererTurbo(this, 32, 149, textureX, textureY); // Box 8
		gunModel[46] = new ModelRendererTurbo(this, 1, 192, textureX, textureY); // Box 9
		gunModel[47] = new ModelRendererTurbo(this, 1, 181, textureX, textureY); // Box 10
		gunModel[48] = new ModelRendererTurbo(this, 1, 192, textureX, textureY); // Box 11
		gunModel[49] = new ModelRendererTurbo(this, 40, 172, textureX, textureY); // Box 12
		gunModel[50] = new ModelRendererTurbo(this, 40, 172, textureX, textureY); // Box 13
		gunModel[51] = new ModelRendererTurbo(this, 40, 172, textureX, textureY); // Box 14
		gunModel[52] = new ModelRendererTurbo(this, 59, 206, textureX, textureY); // Box 15
		gunModel[53] = new ModelRendererTurbo(this, 1, 202, textureX, textureY); // Box 16
		gunModel[54] = new ModelRendererTurbo(this, 30, 202, textureX, textureY); // Box 17
		gunModel[55] = new ModelRendererTurbo(this, 59, 218, textureX, textureY); // Box 18
		gunModel[56] = new ModelRendererTurbo(this, 32, 149, textureX, textureY); // Box 19
		gunModel[57] = new ModelRendererTurbo(this, 76, 181, textureX, textureY); // Box 20
		gunModel[58] = new ModelRendererTurbo(this, 112, 152, textureX, textureY); // Box 21
		gunModel[59] = new ModelRendererTurbo(this, 112, 160, textureX, textureY); // Box 22
		gunModel[60] = new ModelRendererTurbo(this, 142, 166, textureX, textureY); // Box 23
		gunModel[61] = new ModelRendererTurbo(this, 127, 162, textureX, textureY); // Box 24
		gunModel[62] = new ModelRendererTurbo(this, 142, 166, textureX, textureY); // Box 25
		gunModel[63] = new ModelRendererTurbo(this, 1, 134, textureX, textureY); // Box 104
		gunModel[64] = new ModelRendererTurbo(this, 79, 135, textureX, textureY); // Box 105
		gunModel[65] = new ModelRendererTurbo(this, 79, 154, textureX, textureY); // Box 106
		gunModel[66] = new ModelRendererTurbo(this, 1, 56, textureX, textureY); // Box 107
		gunModel[67] = new ModelRendererTurbo(this, 77, 56, textureX, textureY); // Box 108
		gunModel[68] = new ModelRendererTurbo(this, 77, 56, textureX, textureY); // Box 109
		gunModel[69] = new ModelRendererTurbo(this, 77, 56, textureX, textureY); // Box 110
		gunModel[70] = new ModelRendererTurbo(this, 77, 56, textureX, textureY); // Box 111
		gunModel[71] = new ModelRendererTurbo(this, 239, 86, textureX, textureY); // Box 112
		gunModel[72] = new ModelRendererTurbo(this, 239, 86, textureX, textureY); // Box 113
		gunModel[73] = new ModelRendererTurbo(this, 239, 86, textureX, textureY); // Box 114
		gunModel[74] = new ModelRendererTurbo(this, 239, 86, textureX, textureY); // Box 115
		gunModel[75] = new ModelRendererTurbo(this, 56, 56, textureX, textureY); // Box 116
		gunModel[76] = new ModelRendererTurbo(this, 278, 38, textureX, textureY); // Box 67
		gunModel[77] = new ModelRendererTurbo(this, 297, 38, textureX, textureY); // Box 68
		gunModel[78] = new ModelRendererTurbo(this, 271, 37, textureX, textureY); // Box 69
		gunModel[79] = new ModelRendererTurbo(this, 306, 43, textureX, textureY); // Box 70
		gunModel[80] = new ModelRendererTurbo(this, 306, 43, textureX, textureY); // Box 71
		gunModel[81] = new ModelRendererTurbo(this, 306, 39, textureX, textureY); // Box 72
		gunModel[82] = new ModelRendererTurbo(this, 306, 39, textureX, textureY); // Box 73
		gunModel[83] = new ModelRendererTurbo(this, 291, 51, textureX, textureY); // Box 74
		gunModel[84] = new ModelRendererTurbo(this, 271, 23, textureX, textureY); // Box 78
		gunModel[85] = new ModelRendererTurbo(this, 271, 23, textureX, textureY); // Box 134
		gunModel[86] = new ModelRendererTurbo(this, 271, 37, textureX, textureY); // Box 135
		gunModel[87] = new ModelRendererTurbo(this, 300, 28, textureX, textureY); // Box 136
		gunModel[88] = new ModelRendererTurbo(this, 313, 31, textureX, textureY); // Box 137
		gunModel[89] = new ModelRendererTurbo(this, 313, 34, textureX, textureY); // Box 138
		gunModel[90] = new ModelRendererTurbo(this, 262, 100, textureX, textureY); // Box 0
		gunModel[91] = new ModelRendererTurbo(this, 262, 100, textureX, textureY); // Box 1
		gunModel[92] = new ModelRendererTurbo(this, 57, 1, textureX, textureY); // Box 114

		gunModel[0].addShapeBox(0F, 0F, 0F, 67, 6, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F); // barrelMainLeft
		gunModel[0].setRotationPoint(4F, -22.5F, 1F);

		gunModel[1].addBox(0F, 0F, 0F, 67, 6, 2, 0F); // barrelMainMiddle
		gunModel[1].setRotationPoint(4F, -22.5F, -1F);

		gunModel[2].addShapeBox(0F, 0F, 0F, 67, 6, 2, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // barrelMainRight
		gunModel[2].setRotationPoint(4F, -22.5F, -3F);

		gunModel[3].addShapeBox(0F, 0F, 0F, 12, 3, 9, 0F, -2F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, -2F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body1
		gunModel[3].setRotationPoint(-12F, -23F, -4.5F);

		gunModel[4].addShapeBox(0F, 0F, 0F, 25, 4, 9, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body6
		gunModel[4].setRotationPoint(-12F, -16F, -4.5F);

		gunModel[5].addBox(0F, 0F, 0F, 23, 9, 9, 0F); // foreRail1
		gunModel[5].setRotationPoint(51F, -17F, -4.5F);

		gunModel[6].addShapeBox(0F, 0F, 0F, 12, 4, 8, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body12
		gunModel[6].setRotationPoint(11F, -16F, -4F);

		gunModel[7].addBox(0F, 0F, 0F, 8, 3, 1, 0F); // body17
		gunModel[7].setRotationPoint(3F, -11.5F, 4F);

		gunModel[8].addBox(0F, 0F, 0F, 12, 3, 9, 0F); // Import GU,body4
		gunModel[8].setRotationPoint(-12F, -20F, -4.5F);

		gunModel[9].addBox(0F, 0F, 0F, 63, 1, 9, 0F); // Import GU,body9
		gunModel[9].setRotationPoint(-12F, -17F, -4.5F);

		gunModel[10].addBox(0F, 0F, 0F, 70, 3, 9, 0F); // railPart3
		gunModel[10].setRotationPoint(4F, -20F, -4.5F);

		gunModel[11].addBox(0F, 0F, 0F, 4, 8, 9, 0F); // Import GU,tacRailEnd1
		gunModel[11].setRotationPoint(0F, -25F, -4.5F);

		gunModel[12].addShapeBox(0F, 0F, 0F, 8, 3, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F); // ironSightPart1
		gunModel[12].setRotationPoint(-8F, -25F, -3F);

		gunModel[13].addBox(0F, 0F, 0F, 6, 1, 6, 0F); // ironSightPart4
		gunModel[13].setRotationPoint(-3F, -26F, -3F);

		gunModel[14].addShapeBox(0F, 0F, 0F, 10, 7, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F); // Box 36
		gunModel[14].setRotationPoint(11F, -23F, 1.5F);

		gunModel[15].addShapeBox(0F, 0F, 0F, 10, 7, 2, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 37
		gunModel[15].setRotationPoint(11F, -23F, -3.5F);

		gunModel[16].addBox(0F, 0F, 0F, 10, 7, 3, 0F); // Box 38
		gunModel[16].setRotationPoint(11F, -23F, -1.5F);

		gunModel[17].addShapeBox(0F, 0F, 0F, 17, 6, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F); // Box 39
		gunModel[17].setRotationPoint(54F, -15F, 4.5F);

		gunModel[18].addBox(0F, 0F, 0F, 30, 6, 9, 0F); // Box 1
		gunModel[18].setRotationPoint(-12F, -12F, -4.5F);

		gunModel[19].addShapeBox(0F, 0F, 0F, 17, 6, 1, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 2
		gunModel[19].setRotationPoint(54F, -15F, -5.5F);

		gunModel[20].addShapeBox(0F, 0F, 0F, 3, 6, 9, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F); // Box 3
		gunModel[20].setRotationPoint(18F, -12F, -4.5F);

		gunModel[21].addShapeBox(0F, 0F, 0F, 7, 7, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F); // Box 11
		gunModel[21].setRotationPoint(70F, -23F, 1.5F);

		gunModel[22].addBox(0F, 0F, 0F, 7, 7, 3, 0F); // Box 12
		gunModel[22].setRotationPoint(70F, -23F, -1.5F);

		gunModel[23].addShapeBox(0F, 0F, 0F, 7, 7, 2, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 13
		gunModel[23].setRotationPoint(70F, -23F, -3.5F);

		gunModel[24].addShapeBox(0F, 0F, 0F, 23, 2, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 14
		gunModel[24].setRotationPoint(51F, -8F, -4.5F);

		gunModel[25].addShapeBox(0F, 0F, 0F, 30, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 16
		gunModel[25].setRotationPoint(21F, -8F, -4F);

		gunModel[26].addShapeBox(0F, 0F, 0F, 3, 7, 2, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 22
		gunModel[26].setRotationPoint(74F, -15F, -3.5F);

		gunModel[27].addBox(0F, 0F, 0F, 3, 7, 3, 0F); // Box 23
		gunModel[27].setRotationPoint(74F, -15F, -1.5F);

		gunModel[28].addShapeBox(0F, 0F, 0F, 3, 7, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F); // Box 24
		gunModel[28].setRotationPoint(74F, -15F, 1.5F);

		gunModel[29].addShapeBox(0F, 0F, 0F, 28, 1, 8, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 26
		gunModel[29].setRotationPoint(23F, -16F, -4F);

		gunModel[30].addShapeBox(0F, 0F, 0F, 28, 2, 8, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 27
		gunModel[30].setRotationPoint(23F, -14F, -4F);

		gunModel[31].addShapeBox(0F, 0F, 0F, 3, 4, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F); // Box 29
		gunModel[31].setRotationPoint(48F, -12F, -4F);

		gunModel[32].addShapeBox(0F, 0F, 0F, 1, 4, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F); // Box 30
		gunModel[32].setRotationPoint(45F, -12F, -4F);

		gunModel[33].addShapeBox(0F, 0F, 0F, 1, 4, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F); // Box 31
		gunModel[33].setRotationPoint(42F, -12F, -4F);

		gunModel[34].addShapeBox(0F, 0F, 0F, 1, 4, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F); // Box 32
		gunModel[34].setRotationPoint(39F, -12F, -4F);

		gunModel[35].addShapeBox(0F, 0F, 0F, 15, 4, 8, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 33
		gunModel[35].setRotationPoint(20F, -12F, -4F);

		gunModel[36].addBox(0F, 0F, 0F, 30, 8, 7, 0F); // Box 34
		gunModel[36].setRotationPoint(21F, -16F, -3.5F);

		gunModel[37].addBox(0F, 0F, 0F, 67, 1, 1, 0F); // Box 35
		gunModel[37].setRotationPoint(4F, -22F, -2.5F);

		gunModel[38].addBox(0F, 0F, 0F, 67, 1, 1, 0F); // Box 36
		gunModel[38].setRotationPoint(4F, -22F, 1.5F);

		gunModel[39].addShapeBox(0F, 0F, 0F, 12, 2, 7, 0F, 3F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 3F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 1
		gunModel[39].setRotationPoint(-8F, -6F, -3.5F);

		gunModel[40].addShapeBox(0F, 0F, 0F, 9, 8, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F); // Box 2
		gunModel[40].setRotationPoint(-7F, -4F, -3F);

		gunModel[41].addBox(0F, 0F, 0F, 28, 4, 6, 0F); // Box 3
		gunModel[41].setRotationPoint(-37F, 4F, -3F);

		gunModel[42].addBox(0F, 0F, 0F, 32, 5, 8, 0F); // Box 4
		gunModel[42].setRotationPoint(-44F, -13F, -4F);

		gunModel[43].addShapeBox(0F, 0F, 0F, 32, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 5
		gunModel[43].setRotationPoint(-44F, -8F, -4F);

		gunModel[44].addShapeBox(0F, 0F, 0F, 17, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F); // Box 6
		gunModel[44].setRotationPoint(-18F, 8F, -3F);

		gunModel[45].addBox(0F, 0F, 0F, 7, 2, 8, 0F); // Box 8
		gunModel[45].setRotationPoint(-21F, -15F, -4F);

		gunModel[46].addShapeBox(0F, 0F, 0F, 30, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 9
		gunModel[46].setRotationPoint(-42F, -14F, -3.5F);

		gunModel[47].addBox(0F, 0F, 0F, 30, 3, 7, 0F); // Box 10
		gunModel[47].setRotationPoint(-42F, -17F, -3.5F);

		gunModel[48].addShapeBox(0F, 0F, 0F, 30, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 11
		gunModel[48].setRotationPoint(-42F, -19F, -3.5F);

		gunModel[49].addShapeBox(0F, 0F, 0F, 2, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 12
		gunModel[49].setRotationPoint(-44F, -14.5F, -3F);

		gunModel[50].addBox(0F, 0F, 0F, 2, 2, 6, 0F); // Box 13
		gunModel[50].setRotationPoint(-44F, -16.5F, -3F);

		gunModel[51].addShapeBox(0F, 0F, 0F, 2, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 14
		gunModel[51].setRotationPoint(-44F, -18.5F, -3F);

		gunModel[52].addShapeBox(0F, 0F, 0F, 5, 2, 9, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 15
		gunModel[52].setRotationPoint(-49F, -19.5F, -4.5F);

		gunModel[53].addBox(0F, 0F, 0F, 5, 22, 9, 0F); // Box 16
		gunModel[53].setRotationPoint(-49F, -17.5F, -4.5F);

		gunModel[54].addShapeBox(0F, 0F, 0F, 5, 6, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 17
		gunModel[54].setRotationPoint(-49F, 4.5F, -4.5F);

		gunModel[55].addShapeBox(0F, 0F, 0F, 5, 1, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 18
		gunModel[55].setRotationPoint(-49F, 10.5F, -2.5F);

		gunModel[56].addBox(0F, 0F, 0F, 7, 2, 8, 0F); // Box 19
		gunModel[56].setRotationPoint(-41F, -15F, -4F);

		gunModel[57].addBox(0F, 0F, 0F, 7, 14, 6, 0F); // Box 20
		gunModel[57].setRotationPoint(-44F, -6F, -3F);

		gunModel[58].addShapeBox(0F, 0F, 0F, 28, 1, 6, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 21
		gunModel[58].setRotationPoint(-37F, 3F, -3F);

		gunModel[59].addShapeBox(0F, 0F, 0F, 1, 10, 6, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 22
		gunModel[59].setRotationPoint(-37F, -6F, -3F);

		gunModel[60].addShapeBox(0F, 0F, 0F, 3, 4, 6, 0F, 0F, 0F, -1F, 0F, -3F, -1F, 0F, -3F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 23
		gunModel[60].setRotationPoint(-37F, 0F, -3F);

		gunModel[61].addShapeBox(0F, 0F, 0F, 1, 8, 6, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 2F, 0F, -1F, -2F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, -1F); // Box 24
		gunModel[61].setRotationPoint(-8F, -4F, -3F);

		gunModel[62].addShapeBox(0F, 0F, 0F, 3, 4, 6, 0F, 0F, -3F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, -3F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 25
		gunModel[62].setRotationPoint(-12F, 0F, -3F);

		gunModel[63].addShapeBox(0F, 0F, 0F, 9, 4, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F); // Box 104
		gunModel[63].setRotationPoint(-9F, 4F, -3F);

		gunModel[64].addShapeBox(0F, 0F, 0F, 2, 12, 6, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, -1.5F, -3F, 0F, -1.5F, 3F, 0F, 0F); // Box 105
		gunModel[64].setRotationPoint(2F, -4F, -3F);

		gunModel[65].addShapeBox(0F, 0F, 0F, 2, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, -1.5F, -3F, 0F, -1.5F, 3F, 0F, 0F); // Box 106
		gunModel[65].setRotationPoint(-1F, 8F, -3F);

		gunModel[66].addBox(0F, 0F, 0F, 20, 2, 7, 0F); // Box 107
		gunModel[66].setRotationPoint(41F, -6F, -3.5F);

		gunModel[67].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 108
		gunModel[67].setRotationPoint(41F, -4F, -3.5F);

		gunModel[68].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 109
		gunModel[68].setRotationPoint(47F, -4F, -3.5F);

		gunModel[69].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 110
		gunModel[69].setRotationPoint(59F, -4F, -3.5F);

		gunModel[70].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 111
		gunModel[70].setRotationPoint(53F, -4F, -3.5F);

		gunModel[71].addBox(0F, 0F, 0F, 3, 2, 7, 0F); // Box 112
		gunModel[71].setRotationPoint(60F, -21F, -3.5F);

		gunModel[72].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 113
		gunModel[72].setRotationPoint(65F, -23F, -3.5F);

		gunModel[73].addBox(0F, 0F, 0F, 3, 2, 7, 0F); // Box 114
		gunModel[73].setRotationPoint(65F, -21F, -3.5F);

		gunModel[74].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 115
		gunModel[74].setRotationPoint(60F, -23F, -3.5F);

		gunModel[75].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F); // Box 116
		gunModel[75].setRotationPoint(61F, -6F, -3.5F);

		gunModel[76].addBox(0F, 0F, 0F, 8, 7, 1, 0F); // Box 67
		gunModel[76].setRotationPoint(12F, -14.5F, 4F);

		gunModel[77].addBox(0F, 0F, 0F, 2, 6, 2, 0F); // Box 68
		gunModel[77].setRotationPoint(13.5F, -14F, 5F);

		gunModel[78].addShapeBox(-2F, -4F, 0F, 2, 8, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F); // Box 69
		gunModel[78].setRotationPoint(14.5F, -11F, 7F);
		gunModel[78].rotateAngleZ = -0.43633231F;

		gunModel[79].addShapeBox(0F, 0F, 0F, 4, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 70
		gunModel[79].setRotationPoint(15.5F, -13F, 5F);

		gunModel[80].addShapeBox(0F, 0F, 0F, 4, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 71
		gunModel[80].setRotationPoint(15.5F, -11F, 5F);

		gunModel[81].addShapeBox(-0.5F, 0.5F, 0.5F, 1, 1, 2, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 72
		gunModel[81].setRotationPoint(13.5F, -14F, 4.5F);

		gunModel[82].addShapeBox(-0.5F, 1.5F, 0.5F, 1, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 73
		gunModel[82].setRotationPoint(13.5F, -14F, 4.5F);

		gunModel[83].addShapeBox(-1F, -4F, 1F, 1, 24, 36, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -24F, 0F, 0F, -24F, 0F, -16F, 0F, 0F, -16F, 0F, 0F, -16F, -24F, 0F, -16F, -24F); // Box 74
		gunModel[83].setRotationPoint(14.5F, -11F, 7F);
		gunModel[83].rotateAngleZ = -0.43633231F;

		gunModel[84].addBox(-2F, -5F, 1F, 2, 1, 12, 0F); // Box 78
		gunModel[84].setRotationPoint(14.5F, -11F, 7F);
		gunModel[84].rotateAngleZ = -0.43633231F;

		gunModel[85].addBox(-2F, 4F, 1F, 2, 1, 12, 0F); // Box 134
		gunModel[85].setRotationPoint(14.5F, -11F, 7F);
		gunModel[85].rotateAngleZ = -0.43633231F;

		gunModel[86].addShapeBox(-2F, -4F, 13F, 2, 8, 1, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 135
		gunModel[86].setRotationPoint(14.5F, -11F, 7F);
		gunModel[86].rotateAngleZ = -0.43633231F;

		gunModel[87].addShapeBox(-2F, 1F, 1F, 2, 4, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 136
		gunModel[87].setRotationPoint(14.5F, -11F, 7F);
		gunModel[87].rotateAngleZ = -0.43633231F;

		gunModel[88].addBox(-2.3F, 3F, 2.5F, 1, 1, 1, 0F); // Box 137
		gunModel[88].setRotationPoint(14.5F, -11F, 7F);
		gunModel[88].rotateAngleZ = -0.43633231F;

		gunModel[89].addBox(-2.3F, 3F, 1F, 1, 1, 1, 0F); // Box 138
		gunModel[89].setRotationPoint(14.5F, -11F, 7F);
		gunModel[89].rotateAngleZ = -0.43633231F;

		gunModel[90].addShapeBox(0F, 0F, 0F, 5, 2, 9, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 0
		gunModel[90].setRotationPoint(4F, -24F, -4.5F);

		gunModel[91].addBox(0F, 0F, 0F, 5, 2, 9, 0F); // Box 1
		gunModel[91].setRotationPoint(4F, -22F, -4.5F);

		gunModel[92].addShapeBox(0F, 0F, 0F, 1, 12, 12, 0F, 0F, -6F, 0F, 0F, -6F, 0F, 0F, -6F, -6F, 0F, -6F, -6F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -6F, 0F, 0F, -6F); // Box 114
		gunModel[92].setRotationPoint(1F, -38F, -3F);


		ammoModel = new ModelRendererTurbo[18];
		ammoModel[0] = new ModelRendererTurbo(this, 112, 96, textureX, textureY); // Box 37
		ammoModel[1] = new ModelRendererTurbo(this, 112, 96, textureX, textureY); // Box 38
		ammoModel[2] = new ModelRendererTurbo(this, 112, 124, textureX, textureY); // Box 41
		ammoModel[3] = new ModelRendererTurbo(this, 112, 112, textureX, textureY); // Box 43
		ammoModel[4] = new ModelRendererTurbo(this, 232, 124, textureX, textureY); // Box 45
		ammoModel[5] = new ModelRendererTurbo(this, 232, 124, textureX, textureY); // Box 46
		ammoModel[6] = new ModelRendererTurbo(this, 205, 124, textureX, textureY); // Box 50
		ammoModel[7] = new ModelRendererTurbo(this, 205, 124, textureX, textureY); // Box 51
		ammoModel[8] = new ModelRendererTurbo(this, 213, 139, textureX, textureY); // Box 54
		ammoModel[9] = new ModelRendererTurbo(this, 165, 99, textureX, textureY); // Box 57
		ammoModel[10] = new ModelRendererTurbo(this, 165, 99, textureX, textureY); // Box 58
		ammoModel[11] = new ModelRendererTurbo(this, 253, 114, textureX, textureY); // Box 59
		ammoModel[12] = new ModelRendererTurbo(this, 258, 117, textureX, textureY); // Box 60
		ammoModel[13] = new ModelRendererTurbo(this, 205, 112, textureX, textureY); // Box 61
		ammoModel[14] = new ModelRendererTurbo(this, 112, 136, textureX, textureY); // Box 62
		ammoModel[15] = new ModelRendererTurbo(this, 232, 112, textureX, textureY); // Box 63
		ammoModel[16] = new ModelRendererTurbo(this, 228, 167, textureX, textureY); // Box 64
		ammoModel[17] = new ModelRendererTurbo(this, 161, 167, textureX, textureY); // Box 115

		ammoModel[0].addShapeBox(0F, 0F, 0F, 12, 1, 14, 0F, 12F, 0F, -1F, -16F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 12F, 0F, -1F, -16F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 37
		ammoModel[0].setRotationPoint(38F, -25F, -19F);

		ammoModel[1].addShapeBox(0F, 0F, 0F, 12, 1, 14, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -16F, 0F, 0F, 12F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, -16F, 0F, 0F, 12F, 0F, -1F); // Box 38
		ammoModel[1].setRotationPoint(37F, -25F, 6F);

		ammoModel[2].addShapeBox(0F, 0F, 0F, 38, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 2F); // Box 41
		ammoModel[2].setRotationPoint(17F, -29F, -4F);

		ammoModel[3].addShapeBox(0F, 0F, 0F, 38, 3, 8, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 43
		ammoModel[3].setRotationPoint(17F, -23F, -4F);

		ammoModel[4].addShapeBox(0F, 0F, 0F, 6, 3, 8, 0F, 0F, 0F, 0F, 0F, -2F, -3F, 0F, -2F, -3F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 2F); // Box 45
		ammoModel[4].setRotationPoint(55F, -29F, -4F);

		ammoModel[5].addShapeBox(0F, 0F, 0F, 6, 3, 8, 0F, 0F, 0F, 2F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, -2F, -3F, 0F, -2F, -3F, 0F, 0F, 0F); // Box 46
		ammoModel[5].setRotationPoint(55F, -23F, -4F);

		ammoModel[6].addShapeBox(0F, 0F, 0F, 5, 3, 8, 0F, 0F, 0F, -1F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -1F, 0F, -2F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, -3F); // Box 50
		ammoModel[6].setRotationPoint(12F, -23F, -4F);

		ammoModel[7].addShapeBox(0F, 0F, 0F, 5, 3, 8, 0F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -1F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -1F); // Box 51
		ammoModel[7].setRotationPoint(12F, -29F, -4F);

		ammoModel[8].addShapeBox(0F, 0F, 0F, 7, 10, 2, 0F, 3F, 0F, 0F, -7F, 0F, 0F, -7F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 54
		ammoModel[8].setRotationPoint(13F, -38F, -1F);

		ammoModel[9].addShapeBox(0F, 0F, 0F, 5, 1, 11, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -12F, 4F, 0F, 12F, 4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -12F, -4F, 0F, 12F, -4F, 0F); // Box 57
		ammoModel[9].setRotationPoint(14F, -28.5F, 1F);

		ammoModel[10].addShapeBox(0F, 0F, 0F, 5, 1, 11, 0F, 12F, 4F, 0F, -12F, 4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 12F, -4F, 0F, -12F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 58
		ammoModel[10].setRotationPoint(14F, -28.5F, -12F);

		ammoModel[11].addBox(0F, 0F, 0F, 1, 4, 1, 0F); // Box 59
		ammoModel[11].setRotationPoint(21F, -35F, -0.5F);

		ammoModel[12].addBox(0F, 0F, 0F, 1, 1, 1, 0F); // Box 60
		ammoModel[12].setRotationPoint(21F, -36F, -0.5F);

		ammoModel[13].addShapeBox(0F, 0F, 0F, 5, 3, 8, 0F, 0F, 0F, -1F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -1F); // Box 61
		ammoModel[13].setRotationPoint(12F, -26F, -4F);

		ammoModel[14].addBox(0F, 0F, 0F, 38, 3, 12, 0F); // Box 62
		ammoModel[14].setRotationPoint(17F, -26F, -6F);

		ammoModel[15].addShapeBox(0F, 0F, 0F, 6, 3, 8, 0F, 0F, 0F, 2F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 2F); // Box 63
		ammoModel[15].setRotationPoint(55F, -26F, -4F);

		ammoModel[16].addShapeBox(0F, 0F, 0F, 4, 2, 7, 0F, 0F, 0F, -2F, 0F, -2F, -2F, 0F, -2F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F); // Box 64
		ammoModel[16].setRotationPoint(46F, -31F, -3.5F);

		ammoModel[17].addShapeBox(0F, 0F, 0F, 26, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 115
		ammoModel[17].setRotationPoint(20F, -31F, -3.5F);

		endLoadedAmmoDistance = 5F;

		animationType = EnumAnimationType.END_LOADED;

		translateAll(0F, 0F, 0F);


		flipAll();
	}
}