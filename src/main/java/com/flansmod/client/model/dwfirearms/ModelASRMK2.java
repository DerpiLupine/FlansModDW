package com.flansmod.client.model.dwfirearms; //Path where the model is located

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelASRMK2 extends ModelGun //Same as Filename
{
	int textureX = 512;
	int textureY = 256;

	public ModelASRMK2() //Same as Filename
	{
		gunModel = new ModelRendererTurbo[74];
		gunModel[0] = new ModelRendererTurbo(this, 96, 1, textureX, textureY); // barrel1
		gunModel[1] = new ModelRendererTurbo(this, 96, 10, textureX, textureY); // barrel2
		gunModel[2] = new ModelRendererTurbo(this, 96, 19, textureX, textureY); // barrel3
		gunModel[3] = new ModelRendererTurbo(this, 160, 19, textureX, textureY); // barrelHolder
		gunModel[4] = new ModelRendererTurbo(this, 160, 19, textureX, textureY); // barrelHolder
		gunModel[5] = new ModelRendererTurbo(this, 139, 19, textureX, textureY); // barrelHolder2
		gunModel[6] = new ModelRendererTurbo(this, 139, 19, textureX, textureY); // barrelHolder2
		gunModel[7] = new ModelRendererTurbo(this, 96, 61, textureX, textureY); // body1
		gunModel[8] = new ModelRendererTurbo(this, 96, 91, textureX, textureY); // body2
		gunModel[9] = new ModelRendererTurbo(this, 157, 69, textureX, textureY); // body3
		gunModel[10] = new ModelRendererTurbo(this, 96, 80, textureX, textureY); // body4
		gunModel[11] = new ModelRendererTurbo(this, 214, 63, textureX, textureY); // body5
		gunModel[12] = new ModelRendererTurbo(this, 133, 80, textureX, textureY); // body6
		gunModel[13] = new ModelRendererTurbo(this, 96, 29, textureX, textureY); // body7
		gunModel[14] = new ModelRendererTurbo(this, 96, 103, textureX, textureY); // body8
		gunModel[15] = new ModelRendererTurbo(this, 96, 114, textureX, textureY); // body9
		gunModel[16] = new ModelRendererTurbo(this, 53, 31, textureX, textureY); // electricGrid
		gunModel[17] = new ModelRendererTurbo(this, 1, 7, textureX, textureY); // ironSightBack
		gunModel[18] = new ModelRendererTurbo(this, 126, 22, textureX, textureY); // lowerBarrel
		gunModel[19] = new ModelRendererTurbo(this, 126, 22, textureX, textureY); // lowerBarrel
		gunModel[20] = new ModelRendererTurbo(this, 113, 21, textureX, textureY); // lowerBarrel2
		gunModel[21] = new ModelRendererTurbo(this, 35, 9, textureX, textureY); // rail1
		gunModel[22] = new ModelRendererTurbo(this, 35, 1, textureX, textureY); // rail2
		gunModel[23] = new ModelRendererTurbo(this, 35, 15, textureX, textureY); // rail3
		gunModel[24] = new ModelRendererTurbo(this, 35, 15, textureX, textureY); // rail3-2
		gunModel[25] = new ModelRendererTurbo(this, 1, 15, textureX, textureY); // sparkCanister
		gunModel[26] = new ModelRendererTurbo(this, 1, 15, textureX, textureY); // sparkCanister
		gunModel[27] = new ModelRendererTurbo(this, 1, 15, textureX, textureY); // sparkCanister
		gunModel[28] = new ModelRendererTurbo(this, 1, 15, textureX, textureY); // sparkCanister
		gunModel[29] = new ModelRendererTurbo(this, 10, 15, textureX, textureY); // sparkCanister2
		gunModel[30] = new ModelRendererTurbo(this, 10, 15, textureX, textureY); // sparkCanister2
		gunModel[31] = new ModelRendererTurbo(this, 19, 19, textureX, textureY); // sparkPin
		gunModel[32] = new ModelRendererTurbo(this, 19, 19, textureX, textureY); // sparkPin
		gunModel[33] = new ModelRendererTurbo(this, 35, 18, textureX, textureY); // wire2
		gunModel[34] = new ModelRendererTurbo(this, 51, 18, textureX, textureY); // wire3
		gunModel[35] = new ModelRendererTurbo(this, 40, 18, textureX, textureY); // wire4
		gunModel[36] = new ModelRendererTurbo(this, 40, 21, textureX, textureY); // wire5
		gunModel[37] = new ModelRendererTurbo(this, 1, 90, textureX, textureY); // Box 1
		gunModel[38] = new ModelRendererTurbo(this, 1, 80, textureX, textureY); // Box 2
		gunModel[39] = new ModelRendererTurbo(this, 32, 98, textureX, textureY); // Box 3
		gunModel[40] = new ModelRendererTurbo(this, 47, 74, textureX, textureY); // Box 4
		gunModel[41] = new ModelRendererTurbo(this, 32, 98, textureX, textureY); // Box 5
		gunModel[42] = new ModelRendererTurbo(this, 32, 66, textureX, textureY); // Box 6
		gunModel[43] = new ModelRendererTurbo(this, 1, 66, textureX, textureY); // Box 7
		gunModel[44] = new ModelRendererTurbo(this, 32, 80, textureX, textureY); // Box 8
		gunModel[45] = new ModelRendererTurbo(this, 1, 99, textureX, textureY); // Box 9
		gunModel[46] = new ModelRendererTurbo(this, 47, 99, textureX, textureY); // Box 10
		gunModel[47] = new ModelRendererTurbo(this, 47, 99, textureX, textureY); // Box 11
		gunModel[48] = new ModelRendererTurbo(this, 1, 51, textureX, textureY); // canister1
		gunModel[49] = new ModelRendererTurbo(this, 1, 33, textureX, textureY); // canister2
		gunModel[50] = new ModelRendererTurbo(this, 1, 24, textureX, textureY); // canister3
		gunModel[51] = new ModelRendererTurbo(this, 1, 42, textureX, textureY); // canister4
		gunModel[52] = new ModelRendererTurbo(this, 34, 24, textureX, textureY); // meter1
		gunModel[53] = new ModelRendererTurbo(this, 34, 34, textureX, textureY); // meter1-2
		gunModel[54] = new ModelRendererTurbo(this, 34, 44, textureX, textureY); // meter2
		gunModel[55] = new ModelRendererTurbo(this, 18, 58, textureX, textureY); // pipe1
		gunModel[56] = new ModelRendererTurbo(this, 18, 51, textureX, textureY); // pipe2
		gunModel[57] = new ModelRendererTurbo(this, 1, 60, textureX, textureY); // pipe3
		gunModel[58] = new ModelRendererTurbo(this, 96, 44, textureX, textureY); // Box 12
		gunModel[59] = new ModelRendererTurbo(this, 192, 44, textureX, textureY); // Box 15
		gunModel[60] = new ModelRendererTurbo(this, 209, 39, textureX, textureY); // Box 16
		gunModel[61] = new ModelRendererTurbo(this, 209, 31, textureX, textureY); // Box 17
		gunModel[62] = new ModelRendererTurbo(this, 145, 44, textureX, textureY); // Box 19
		gunModel[63] = new ModelRendererTurbo(this, 53, 27, textureX, textureY); // Box 20
		gunModel[64] = new ModelRendererTurbo(this, 53, 24, textureX, textureY); // Box 21
		gunModel[65] = new ModelRendererTurbo(this, 47, 82, textureX, textureY); // Box 37
		gunModel[66] = new ModelRendererTurbo(this, 47, 90, textureX, textureY); // Box 38
		gunModel[67] = new ModelRendererTurbo(this, 19, 19, textureX, textureY); // Box 0
		gunModel[68] = new ModelRendererTurbo(this, 1, 15, textureX, textureY); // Box 1
		gunModel[69] = new ModelRendererTurbo(this, 10, 15, textureX, textureY); // Box 2
		gunModel[70] = new ModelRendererTurbo(this, 1, 15, textureX, textureY); // Box 3
		gunModel[71] = new ModelRendererTurbo(this, 145, 44, textureX, textureY); // Box 4
		gunModel[72] = new ModelRendererTurbo(this, 160, 19, textureX, textureY); // Box 5
		gunModel[73] = new ModelRendererTurbo(this, 139, 19, textureX, textureY); // Box 6

		gunModel[0].addShapeBox(0F, 0F, 0F, 50, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // barrel1
		gunModel[0].setRotationPoint(32F, -22F, -3F);

		gunModel[1].addBox(0F, 0F, 0F, 50, 2, 6, 0F); // barrel2
		gunModel[1].setRotationPoint(32F, -20F, -3F);

		gunModel[2].addShapeBox(0F, 0F, 0F, 2, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // barrel3
		gunModel[2].setRotationPoint(80F, -18F, -3F);

		gunModel[3].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // barrelHolder
		gunModel[3].setRotationPoint(44F, -22F, -3.5F);

		gunModel[4].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // barrelHolder
		gunModel[4].setRotationPoint(77F, -22F, -3.5F);

		gunModel[5].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // barrelHolder2
		gunModel[5].setRotationPoint(44F, -20F, -3.5F);

		gunModel[6].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // barrelHolder2
		gunModel[6].setRotationPoint(77F, -20F, -3.5F);

		gunModel[7].addBox(0F, 0F, 0F, 22, 10, 8, 0F); // body1
		gunModel[7].setRotationPoint(-9F, -20F, -4F);

		gunModel[8].addShapeBox(0F, 0F, 0F, 41, 3, 8, 0F, -2F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, -2F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body2
		gunModel[8].setRotationPoint(-9F, -23F, -4F);

		gunModel[9].addShapeBox(0F, 0F, 0F, 20, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // body3
		gunModel[9].setRotationPoint(-9F, -10F, -4F);

		gunModel[10].addBox(0F, 0F, 0F, 5, 2, 8, 0F); // body4
		gunModel[10].setRotationPoint(27F, -20F, -4F);

		gunModel[11].addShapeBox(0F, 0F, 0F, 5, 8, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F); // body5
		gunModel[11].setRotationPoint(27F, -18F, -4F);

		gunModel[12].addShapeBox(0F, 0F, 0F, 5, 2, 8, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -2F, -1F, 0F, -2F, -1F, 0F, 0F, -1F); // body6
		gunModel[12].setRotationPoint(27F, -10F, -4F);

		gunModel[13].addBox(0F, 0F, 0F, 48, 6, 8, 0F); // body7
		gunModel[13].setRotationPoint(32F, -18F, -4F);

		gunModel[14].addShapeBox(0F, 0F, 0F, 48, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // body8
		gunModel[14].setRotationPoint(32F, -12F, -4F);

		gunModel[15].addShapeBox(0F, 0F, 0F, 45, 1, 8, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body9
		gunModel[15].setRotationPoint(32F, -19F, -4F);

		gunModel[16].addBox(0F, 0F, 0F, 11, 4, 1, 0F); // electricGrid
		gunModel[16].setRotationPoint(8F, -20F, 4F);

		gunModel[17].addBox(0F, 0F, 0F, 6, 2, 4, 0F); // ironSightBack
		gunModel[17].setRotationPoint(-5F, -24F, -2F);

		gunModel[18].addShapeBox(0F, 0F, 0F, 2, 1, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // lowerBarrel
		gunModel[18].setRotationPoint(80F, -15F, -2F);

		gunModel[19].addShapeBox(0F, 0F, 0F, 2, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // lowerBarrel
		gunModel[19].setRotationPoint(80F, -12F, -2F);

		gunModel[20].addBox(0F, 0F, 0F, 2, 2, 4, 0F); // lowerBarrel2
		gunModel[20].setRotationPoint(80F, -14F, -2F);

		gunModel[21].addBox(0F, 0F, 0F, 18, 1, 4, 0F); // rail1
		gunModel[21].setRotationPoint(9F, -24F, -2F);

		gunModel[22].addBox(0F, 0F, 0F, 18, 1, 6, 0F); // rail2
		gunModel[22].setRotationPoint(9F, -25F, -3F);

		gunModel[23].addShapeBox(0F, 0F, 0F, 18, 1, 1, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // rail3
		gunModel[23].setRotationPoint(9F, -26F, 2F);

		gunModel[24].addShapeBox(0F, 0F, 0F, 18, 1, 1, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // rail3-2
		gunModel[24].setRotationPoint(9F, -26F, -3F);

		gunModel[25].addShapeBox(0F, 0F, 0F, 1, 5, 3, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // sparkCanister
		gunModel[25].setRotationPoint(18F, -22.5F, 10F);
		gunModel[25].rotateAngleX = -1.04719755F;

		gunModel[26].addShapeBox(0F, 0F, 0F, 1, 5, 3, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // sparkCanister
		gunModel[26].setRotationPoint(16F, -22.5F, 10F);
		gunModel[26].rotateAngleX = -1.04719755F;

		gunModel[27].addShapeBox(0F, 0F, 0F, 1, 5, 3, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // sparkCanister
		gunModel[27].setRotationPoint(14F, -22.5F, 10F);
		gunModel[27].rotateAngleX = -1.04719755F;

		gunModel[28].addShapeBox(0F, 0F, 0F, 1, 5, 3, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // sparkCanister
		gunModel[28].setRotationPoint(12F, -22.5F, 10F);
		gunModel[28].rotateAngleX = -1.04719755F;

		gunModel[29].addBox(0F, 0F, 0F, 1, 5, 3, 0F); // sparkCanister2
		gunModel[29].setRotationPoint(13F, -22.5F, 10F);
		gunModel[29].rotateAngleX = -1.04719755F;

		gunModel[30].addBox(0F, 0F, 0F, 1, 5, 3, 0F); // sparkCanister2
		gunModel[30].setRotationPoint(17F, -22.5F, 10F);
		gunModel[30].rotateAngleX = -1.04719755F;

		gunModel[31].addBox(0F, 0F, 0F, 1, 3, 1, 0F); // sparkPin
		gunModel[31].setRotationPoint(17F, -19.5F, 6.5F);
		gunModel[31].rotateAngleX = -1.04719755F;

		gunModel[32].addBox(0F, 0F, 0F, 1, 3, 1, 0F); // sparkPin
		gunModel[32].setRotationPoint(13F, -19.5F, 6.5F);
		gunModel[32].rotateAngleX = -1.04719755F;

		gunModel[33].addShapeBox(0F, 0F, 0F, 1, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // wire2
		gunModel[33].setRotationPoint(26F, -20.5F, 3.5F);

		gunModel[34].addShapeBox(0F, 0F, 0F, 1, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F); // wire3
		gunModel[34].setRotationPoint(26F, -16.5F, 3.5F);

		gunModel[35].addShapeBox(0F, 0F, 0F, 4, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F); // wire4
		gunModel[35].setRotationPoint(22F, -14.5F, 3.5F);

		gunModel[36].addShapeBox(0F, 0F, 0F, 3, 1, 1, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F); // wire5
		gunModel[36].setRotationPoint(19F, -14.5F, 3.5F);

		gunModel[37].addShapeBox(0F, 0F, 0F, 11, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F); // Box 1
		gunModel[37].setRotationPoint(-7F, -7F, -3F);

		gunModel[38].addShapeBox(0F, 0F, 0F, 9, 3, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F); // Box 2
		gunModel[38].setRotationPoint(-7F, -5F, -3F);

		gunModel[39].addShapeBox(0F, 0F, 0F, 1, 3, 6, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 2F, 0F, -1F, -2F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, -1F); // Box 3
		gunModel[39].setRotationPoint(-8F, -5F, -3F);

		gunModel[40].addShapeBox(0F, 0F, 0F, 1, 1, 6, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, -1F, 0F, -1F, 1F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, -1F); // Box 4
		gunModel[40].setRotationPoint(-9F, -8F, -3F);

		gunModel[41].addShapeBox(0F, 0F, 0F, 1, 3, 6, 0F, -2F, 0F, 0F, 2F, 0F, -1F, 2F, 0F, -1F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 5
		gunModel[41].setRotationPoint(0F, -5F, -3F);

		gunModel[42].addShapeBox(0F, 0F, 0F, 1, 7, 6, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 2F, 0F, -1F, -2F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, -1F); // Box 6
		gunModel[42].setRotationPoint(-10F, -2F, -3F);

		gunModel[43].addShapeBox(0F, 0F, 0F, 9, 7, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 2F, 0F, -2F, 2F, 0F, 2F, 0F, 0F); // Box 7
		gunModel[43].setRotationPoint(-9F, -2F, -3F);

		gunModel[44].addShapeBox(0F, 0F, 0F, 1, 9, 6, 0F, -2F, 0F, 0F, 2F, 0F, -1F, 2F, 0F, -1F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 8
		gunModel[44].setRotationPoint(-2F, -2F, -3F);

		gunModel[45].addShapeBox(0F, 0F, 0F, 9, 2, 6, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, -2F, -1F); // Box 9
		gunModel[45].setRotationPoint(-11F, 7F, -3F);

		gunModel[46].addShapeBox(0F, 0F, 0F, 1, 2, 6, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -0.5F, -2F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, -0.5F, -2F); // Box 10
		gunModel[46].setRotationPoint(-12F, 5F, -3F);

		gunModel[47].addShapeBox(0F, 0F, 0F, 1, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -0.5F, -2F, 0F, -0.5F, -2F, 0F, 0F, -1F); // Box 11
		gunModel[47].setRotationPoint(-2F, 7F, -3F);

		gunModel[48].addBox(0F, 0F, 0F, 6, 6, 2, 0F); // canister1
		gunModel[48].setRotationPoint(-5F, -21F, -6F);

		gunModel[49].addShapeBox(0F, 0F, 0F, 10, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // canister2
		gunModel[49].setRotationPoint(-6F, -17F, -10.5F);

		gunModel[50].addBox(0F, 0F, 0F, 10, 2, 6, 0F); // canister3
		gunModel[50].setRotationPoint(-6F, -19F, -10.5F);

		gunModel[51].addShapeBox(0F, 0F, 0F, 10, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // canister4
		gunModel[51].setRotationPoint(-6F, -21F, -10.5F);

		gunModel[52].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // meter1
		gunModel[52].setRotationPoint(-4F, -23F, -9.5F);

		gunModel[53].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // meter1-2
		gunModel[53].setRotationPoint(-4F, -28F, -9.5F);

		gunModel[54].addBox(0F, 0F, 0F, 2, 3, 7, 0F); // meter2
		gunModel[54].setRotationPoint(-4F, -26F, -9.5F);

		gunModel[55].addBox(0F, 0F, 0F, 3, 2, 2, 0F); // pipe1
		gunModel[55].setRotationPoint(4F, -19F, -8.5F);

		gunModel[56].addBox(0F, 0F, 0F, 2, 2, 4, 0F); // pipe2
		gunModel[56].setRotationPoint(7F, -19F, -8.5F);

		gunModel[57].addBox(0F, 0F, 0F, 7, 4, 1, 0F); // pipe3
		gunModel[57].setRotationPoint(6F, -20F, -5F);

		gunModel[58].addShapeBox(0F, 0F, 0F, 16, 8, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F); // Box 12
		gunModel[58].setRotationPoint(11F, -10F, -4F);

		gunModel[59].addBox(0F, 0F, 0F, 14, 10, 6, 0F); // Box 15
		gunModel[59].setRotationPoint(13F, -20F, -2F);

		gunModel[60].addBox(0F, 0F, 0F, 14, 2, 2, 0F); // Box 16
		gunModel[60].setRotationPoint(13F, -20F, -4F);

		gunModel[61].addBox(0F, 0F, 0F, 14, 5, 2, 0F); // Box 17
		gunModel[61].setRotationPoint(13F, -15F, -4F);

		gunModel[62].addBox(0F, 0F, 0F, 2, 3, 2, 0F); // Box 19
		gunModel[62].setRotationPoint(25F, -18F, -4F);

		gunModel[63].addShapeBox(0F, 0F, 0F, 12, 2, 1, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 20
		gunModel[63].setRotationPoint(13F, -15F, -5F);

		gunModel[64].addShapeBox(0F, 0F, 0F, 12, 1, 1, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -0.25F, -1F, 0F, -0.25F, -1F, 0F, -0.25F, -1F, 0F, -0.25F); // Box 21
		gunModel[64].setRotationPoint(13F, -13F, -5.5F);

		gunModel[65].addShapeBox(0F, 0F, 0F, 11, 1, 6, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 37
		gunModel[65].setRotationPoint(-7F, -8F, -3F);

		gunModel[66].addShapeBox(0F, 0F, 0F, 1, 2, 6, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 38
		gunModel[66].setRotationPoint(-8F, -7F, -3F);

		gunModel[67].addBox(0F, 0F, 0F, 1, 3, 1, 0F); // Box 0
		gunModel[67].setRotationPoint(9F, -19.5F, 6.5F);
		gunModel[67].rotateAngleX = -1.04719755F;

		gunModel[68].addShapeBox(0F, 0F, 0F, 1, 5, 3, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 1
		gunModel[68].setRotationPoint(10F, -22.5F, 10F);
		gunModel[68].rotateAngleX = -1.04719755F;

		gunModel[69].addBox(0F, 0F, 0F, 1, 5, 3, 0F); // Box 2
		gunModel[69].setRotationPoint(9F, -22.5F, 10F);
		gunModel[69].rotateAngleX = -1.04719755F;

		gunModel[70].addShapeBox(0F, 0F, 0F, 1, 5, 3, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 3
		gunModel[70].setRotationPoint(8F, -22.5F, 10F);
		gunModel[70].rotateAngleX = -1.04719755F;

		gunModel[71].addShapeBox(0F, 0F, 0F, 14, 7, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F); // Box 4
		gunModel[71].setRotationPoint(12F, -10F, -4.5F);

		gunModel[72].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 5
		gunModel[72].setRotationPoint(69F, -22F, -3.5F);

		gunModel[73].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // Box 6
		gunModel[73].setRotationPoint(69F, -20F, -3.5F);


		defaultBarrelModel = new ModelRendererTurbo[6];
		defaultBarrelModel[0] = new ModelRendererTurbo(this, 221, 1, textureX, textureY); // muzzle
		defaultBarrelModel[1] = new ModelRendererTurbo(this, 221, 1, textureX, textureY); // muzzle
		defaultBarrelModel[2] = new ModelRendererTurbo(this, 239, 11, textureX, textureY); // muzzle2
		defaultBarrelModel[3] = new ModelRendererTurbo(this, 230, 11, textureX, textureY); // muzzle2
		defaultBarrelModel[4] = new ModelRendererTurbo(this, 221, 11, textureX, textureY); // muzzle2
		defaultBarrelModel[5] = new ModelRendererTurbo(this, 248, 11, textureX, textureY); // muzzle2

		defaultBarrelModel[0].addShapeBox(0F, 0F, 0F, 11, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // muzzle
		defaultBarrelModel[0].setRotationPoint(82F, -22.5F, -3.5F);

		defaultBarrelModel[1].addShapeBox(0F, 0F, 0F, 11, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // muzzle
		defaultBarrelModel[1].setRotationPoint(82F, -17.5F, -3.5F);

		defaultBarrelModel[2].addBox(0F, 0F, 0F, 2, 3, 2, 0F); // muzzle2
		defaultBarrelModel[2].setRotationPoint(82F, -20.5F, -3.5F);

		defaultBarrelModel[3].addBox(0F, 0F, 0F, 2, 3, 2, 0F); // muzzle2
		defaultBarrelModel[3].setRotationPoint(91F, -20.5F, -3.5F);

		defaultBarrelModel[4].addBox(0F, 0F, 0F, 2, 3, 2, 0F); // muzzle2
		defaultBarrelModel[4].setRotationPoint(82F, -20.5F, 1.5F);

		defaultBarrelModel[5].addBox(0F, 0F, 0F, 2, 3, 2, 0F); // muzzle2
		defaultBarrelModel[5].setRotationPoint(91F, -20.5F, 1.5F);


		defaultScopeModel = new ModelRendererTurbo[13];
		defaultScopeModel[0] = new ModelRendererTurbo(this, 6, 1, textureX, textureY); // ironSight
		defaultScopeModel[1] = new ModelRendererTurbo(this, 6, 1, textureX, textureY); // ironSight
		defaultScopeModel[2] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // ironSight2
		defaultScopeModel[3] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // ironSight2
		defaultScopeModel[4] = new ModelRendererTurbo(this, 53, 47, textureX, textureY); // ironSight3
		defaultScopeModel[5] = new ModelRendererTurbo(this, 84, 1, textureX, textureY); // Box 13
		defaultScopeModel[6] = new ModelRendererTurbo(this, 75, 37, textureX, textureY); // Box 7
		defaultScopeModel[7] = new ModelRendererTurbo(this, 66, 47, textureX, textureY); // Box 8
		defaultScopeModel[8] = new ModelRendererTurbo(this, 76, 41, textureX, textureY); // Box 9
		defaultScopeModel[9] = new ModelRendererTurbo(this, 76, 45, textureX, textureY); // Box 10
		defaultScopeModel[10] = new ModelRendererTurbo(this, 64, 41, textureX, textureY); // Box 11
		defaultScopeModel[11] = new ModelRendererTurbo(this, 53, 37, textureX, textureY); // Box 12
		defaultScopeModel[12] = new ModelRendererTurbo(this, 64, 37, textureX, textureY); // Box 13

		defaultScopeModel[0].addShapeBox(0F, 0F, 0F, 1, 1, 4, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // ironSight
		defaultScopeModel[0].setRotationPoint(-1F, -30F, -2F);

		defaultScopeModel[1].addShapeBox(0F, 0F, 0F, 1, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F); // ironSight
		defaultScopeModel[1].setRotationPoint(-1F, -35F, -2F);

		defaultScopeModel[2].addBox(0F, 0F, 0F, 1, 4, 1, 0F); // ironSight2
		defaultScopeModel[2].setRotationPoint(-1F, -34F, -3F);

		defaultScopeModel[3].addBox(0F, 0F, 0F, 1, 4, 1, 0F); // ironSight2
		defaultScopeModel[3].setRotationPoint(-1F, -34F, 2F);

		defaultScopeModel[4].addBox(0F, 0F, 0F, 3, 4, 3, 0F); // ironSight3
		defaultScopeModel[4].setRotationPoint(77F, -26.5F, -1.5F);

		defaultScopeModel[5].addShapeBox(0F, 0F, 0F, 1, 3, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F); // Box 13
		defaultScopeModel[5].setRotationPoint(-1F, -29F, -2F);

		defaultScopeModel[6].addBox(0F, 0F, 0F, 6, 1, 2, 0F); // Box 7
		defaultScopeModel[6].setRotationPoint(71F, -23.5F, -1F);

		defaultScopeModel[7].addShapeBox(0F, 0F, 0F, 3, 4, 3, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 8
		defaultScopeModel[7].setRotationPoint(77F, -30.5F, -1.5F);

		defaultScopeModel[8].addShapeBox(0F, 0F, 0F, 3, 2, 1, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 9
		defaultScopeModel[8].setRotationPoint(77F, -32.5F, -1F);

		defaultScopeModel[9].addShapeBox(0F, 0F, 0F, 3, 2, 1, 0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 10
		defaultScopeModel[9].setRotationPoint(77F, -32.5F, 0F);

		defaultScopeModel[10].addBox(0F, 0F, 0F, 2, 1, 3, 0F); // Box 11
		defaultScopeModel[10].setRotationPoint(69F, -23.5F, -1.5F);

		defaultScopeModel[11].addShapeBox(0F, 0F, 0F, 2, 6, 3, 0F, -5F, 0F, -0.5F, 5F, 0F, -0.5F, 5F, 0F, -0.5F, -5F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 12
		defaultScopeModel[11].setRotationPoint(69F, -29.5F, -1.5F);

		defaultScopeModel[12].addBox(0F, 0F, 0F, 3, 1, 2, 0F); // Box 13
		defaultScopeModel[12].setRotationPoint(74F, -30.5F, -1F);


		defaultStockModel = new ModelRendererTurbo[8];
		defaultStockModel[0] = new ModelRendererTurbo(this, 96, 124, textureX, textureY); // Box 32
		defaultStockModel[1] = new ModelRendererTurbo(this, 163, 125, textureX, textureY); // Box 33
		defaultStockModel[2] = new ModelRendererTurbo(this, 96, 142, textureX, textureY); // Box 34
		defaultStockModel[3] = new ModelRendererTurbo(this, 96, 153, textureX, textureY); // Box 35
		defaultStockModel[4] = new ModelRendererTurbo(this, 163, 153, textureX, textureY); // Box 36
		defaultStockModel[5] = new ModelRendererTurbo(this, 200, 138, textureX, textureY); // Box 40
		defaultStockModel[6] = new ModelRendererTurbo(this, 179, 142, textureX, textureY); // Box 41
		defaultStockModel[7] = new ModelRendererTurbo(this, 179, 142, textureX, textureY); // Box 42

		defaultStockModel[0].addShapeBox(0F, 0F, 0F, 25, 9, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 8F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 8F, 0F); // Box 32
		defaultStockModel[0].setRotationPoint(-42F, -18F, -4F);

		defaultStockModel[1].addShapeBox(0F, 0F, 0F, 8, 8, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F); // Box 33
		defaultStockModel[1].setRotationPoint(-17F, -18F, -4F);

		defaultStockModel[2].addShapeBox(0F, 0F, 0F, 33, 2, 8, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 34
		defaultStockModel[2].setRotationPoint(-42F, -20F, -4F);

		defaultStockModel[3].addShapeBox(0F, 0F, 0F, 25, 2, 8, 0F, 0F, 0F, 0F, 0F, 8F, 0F, 0F, 8F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -8F, -1F, 0F, -8F, -1F, 0F, 0F, -1F); // Box 35
		defaultStockModel[3].setRotationPoint(-42F, -1F, -4F);

		defaultStockModel[4].addShapeBox(0F, 0F, 0F, 8, 2, 8, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, 0F, -1F); // Box 36
		defaultStockModel[4].setRotationPoint(-17F, -9F, -4F);

		defaultStockModel[5].addShapeBox(0F, 0F, 0F, 2, 17, 8, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 40
		defaultStockModel[5].setRotationPoint(-44F, -18F, -4F);

		defaultStockModel[6].addShapeBox(0F, 0F, 0F, 2, 2, 8, 0F, 0F, -0.5F, -2F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, -0.5F, -2F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 41
		defaultStockModel[6].setRotationPoint(-44F, -20F, -4F);

		defaultStockModel[7].addShapeBox(0F, 0F, 0F, 2, 2, 8, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -0.5F, -2F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, -0.5F, -2F); // Box 42
		defaultStockModel[7].setRotationPoint(-44F, -1F, -4F);


		ammoModel = new ModelRendererTurbo[10];
		ammoModel[0] = new ModelRendererTurbo(this, 1, 108, textureX, textureY); // Box 22
		ammoModel[1] = new ModelRendererTurbo(this, 30, 108, textureX, textureY); // Box 23
		ammoModel[2] = new ModelRendererTurbo(this, 30, 108, textureX, textureY); // Box 24
		ammoModel[3] = new ModelRendererTurbo(this, 47, 109, textureX, textureY); // Box 25
		ammoModel[4] = new ModelRendererTurbo(this, 47, 109, textureX, textureY); // Box 26
		ammoModel[5] = new ModelRendererTurbo(this, 1, 135, textureX, textureY); // Box 27
		ammoModel[6] = new ModelRendererTurbo(this, 1, 135, textureX, textureY); // Box 28
		ammoModel[7] = new ModelRendererTurbo(this, 1, 135, textureX, textureY); // Box 29
		ammoModel[8] = new ModelRendererTurbo(this, 46, 141, textureX, textureY); // Box 30
		ammoModel[9] = new ModelRendererTurbo(this, 46, 135, textureX, textureY); // Box 31

		ammoModel[0].addBox(0F, 0F, -3F, 8, 20, 6, 0F); // Box 22
		ammoModel[0].setRotationPoint(12F, -7F, 0F);

		ammoModel[1].addBox(9F, 0F, -3F, 2, 20, 6, 0F); // Box 23
		ammoModel[1].setRotationPoint(12F, -7F, 0F);

		ammoModel[2].addBox(12F, 0F, -3F, 2, 20, 6, 0F); // Box 24
		ammoModel[2].setRotationPoint(12F, -7F, 0F);

		ammoModel[3].addBox(8F, 0F, -2.5F, 1, 20, 5, 0F); // Box 25
		ammoModel[3].setRotationPoint(12F, -7F, 0F);

		ammoModel[4].addBox(11F, 0F, -2.5F, 1, 20, 5, 0F); // Box 26
		ammoModel[4].setRotationPoint(12F, -7F, 0F);

		ammoModel[5].addBox(-0.5F, 5F, -3.5F, 15, 3, 7, 0F); // Box 27
		ammoModel[5].setRotationPoint(12F, -7F, 0F);

		ammoModel[6].addShapeBox(-0.5F, 10F, -3.5F, 15, 3, 7, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F); // Box 28
		ammoModel[6].setRotationPoint(12F, -7F, 0F);

		ammoModel[7].addShapeBox(-0.5F, 11F, -3.5F, 15, 3, 7, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F); // Box 29
		ammoModel[7].setRotationPoint(12F, -7F, 0F);

		ammoModel[8].addShapeBox(1F, -1F, -2F, 8, 1, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 30
		ammoModel[8].setRotationPoint(12F, -7F, 0F);

		ammoModel[9].addShapeBox(9F, -1F, -2F, 4, 1, 4, 0F, 0F, 0F, -1F, 0F, -0.5F, -1.75F, 0F, -0.5F, -1.75F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -1.25F, 0F, 0F, -1.25F, 0F, 0F, 0F); // Box 31
		ammoModel[9].setRotationPoint(12F, -7F, 0F);


		slideModel = new ModelRendererTurbo[1];
		slideModel[0] = new ModelRendererTurbo(this, 56, 19, textureX, textureY); // Box 18

		slideModel[0].addShapeBox(0F, 0F, 0F, 12, 3, 1, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 18
		slideModel[0].setRotationPoint(13F, -18F, -3.5F);

		barrelAttachPoint = new Vector3f(82F /16F, 19F /16F, 0F /16F);
		stockAttachPoint = new Vector3f(-9F /16F, 14.5F /16F, 0F /16F);
		scopeAttachPoint = new Vector3f(19F /16F, 23F /16F, 0F /16F);

		gunSlideDistance = 0.8F;
		animationType = EnumAnimationType.BOTTOM_CLIP;

		translateAll(0F, 0F, 0F);


		flipAll();
	}
}