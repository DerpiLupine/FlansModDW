package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.ModelAttachment;
import com.flansmod.client.tmt.ModelRendererTurbo;

import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelLPMSharedSuppressor extends ModelAttachment 
{
	int textureX = 512;
	int textureY = 256;

	public ModelLPMSharedSuppressor()
	{	
		attachmentModel = new ModelRendererTurbo[5];
		attachmentModel[0] = new ModelRendererTurbo(this, 1, 25, textureX, textureY); // Box 5
		attachmentModel[1] = new ModelRendererTurbo(this, 1, 13, textureX, textureY); // Box 6
		attachmentModel[2] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 7
		attachmentModel[3] = new ModelRendererTurbo(this, 18, 37, textureX, textureY); // Box 9
		attachmentModel[4] = new ModelRendererTurbo(this, 1, 37, textureX, textureY); // Box 10

		attachmentModel[0].addBox(4F, -1.5F, -4F, 25, 3, 8, 0F); // Box 5
		attachmentModel[0].setRotationPoint(0F, 0F, 0F);

		attachmentModel[1].addShapeBox(4F, -4.5F, -4F, 25, 3, 8, 0F, 0F, -0.5F, -2.5F, 0F, -0.5F, -2.5F, 0F, -0.5F, -2.5F, 0F, -0.5F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 6
		attachmentModel[1].setRotationPoint(0F, 0F, 0F);

		attachmentModel[2].addShapeBox(4F, 1.5F, -4F, 25, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -2.5F, 0F, -0.5F, -2.5F, 0F, -0.5F, -2.5F, 0F, -0.5F, -2.5F); // Box 7
		attachmentModel[2].setRotationPoint(0F, 0F, 0F);

		attachmentModel[3].addShapeBox(0F, -4.5F, 0F, 4, 9, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F); // Box 9
		attachmentModel[3].setRotationPoint(0F, 0F, 0F);

		attachmentModel[4].addShapeBox(0F, -4.5F, -4F, 4, 9, 4, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 10
		attachmentModel[4].setRotationPoint(0F, 0F, 0F);

		renderOffset = 0F;

		flipAll();
	}
}