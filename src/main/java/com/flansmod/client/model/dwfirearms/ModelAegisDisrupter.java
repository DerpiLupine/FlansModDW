package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelAegisDisrupter extends ModelGun
{
	int textureX = 512;
	int textureY = 256;

	public ModelAegisDisrupter()
	{
		gunModel = new ModelRendererTurbo[99];
		gunModel[0] = new ModelRendererTurbo(this, 112, 10, textureX, textureY); // barrelPart2
		gunModel[1] = new ModelRendererTurbo(this, 154, 105, textureX, textureY); // body1
		gunModel[2] = new ModelRendererTurbo(this, 112, 91, textureX, textureY); // bodyRail1
		gunModel[3] = new ModelRendererTurbo(this, 33, 1, textureX, textureY); // frontSight
		gunModel[4] = new ModelRendererTurbo(this, 179, 90, textureX, textureY); // slide1
		gunModel[5] = new ModelRendererTurbo(this, 131, 52, textureX, textureY); // Box 36
		gunModel[6] = new ModelRendererTurbo(this, 198, 90, textureX, textureY); // Box 42
		gunModel[7] = new ModelRendererTurbo(this, 1, 62, textureX, textureY); // body1
		gunModel[8] = new ModelRendererTurbo(this, 22, 63, textureX, textureY); // body1
		gunModel[9] = new ModelRendererTurbo(this, 113, 79, textureX, textureY); // Box 0
		gunModel[10] = new ModelRendererTurbo(this, 1, 10, textureX, textureY); // Box 1
		gunModel[11] = new ModelRendererTurbo(this, 1, 20, textureX, textureY); // Box 2
		gunModel[12] = new ModelRendererTurbo(this, 1, 34, textureX, textureY); // Box 3
		gunModel[13] = new ModelRendererTurbo(this, 1, 51, textureX, textureY); // Box 4
		gunModel[14] = new ModelRendererTurbo(this, 34, 34, textureX, textureY); // Box 5
		gunModel[15] = new ModelRendererTurbo(this, 34, 20, textureX, textureY); // Box 6
		gunModel[16] = new ModelRendererTurbo(this, 34, 10, textureX, textureY); // Box 8
		gunModel[17] = new ModelRendererTurbo(this, 53, 20, textureX, textureY); // Box 9
		gunModel[18] = new ModelRendererTurbo(this, 53, 34, textureX, textureY); // Box 10
		gunModel[19] = new ModelRendererTurbo(this, 34, 51, textureX, textureY); // Box 11
		gunModel[20] = new ModelRendererTurbo(this, 112, 125, textureX, textureY); // Box 14
		gunModel[21] = new ModelRendererTurbo(this, 31, 73, textureX, textureY); // Box 15
		gunModel[22] = new ModelRendererTurbo(this, 1, 72, textureX, textureY); // Box 50
		gunModel[23] = new ModelRendererTurbo(this, 208, 117, textureX, textureY); // Box 51
		gunModel[24] = new ModelRendererTurbo(this, 169, 115, textureX, textureY); // Box 52
		gunModel[25] = new ModelRendererTurbo(this, 112, 19, textureX, textureY); // Box 53
		gunModel[26] = new ModelRendererTurbo(this, 16, 72, textureX, textureY); // Box 54
		gunModel[27] = new ModelRendererTurbo(this, 112, 29, textureX, textureY); // Box 58
		gunModel[28] = new ModelRendererTurbo(this, 160, 90, textureX, textureY); // Box 72
		gunModel[29] = new ModelRendererTurbo(this, 141, 90, textureX, textureY); // Box 73
		gunModel[30] = new ModelRendererTurbo(this, 141, 90, textureX, textureY); // Box 74
		gunModel[31] = new ModelRendererTurbo(this, 141, 90, textureX, textureY); // Box 75
		gunModel[32] = new ModelRendererTurbo(this, 112, 105, textureX, textureY); // Box 77
		gunModel[33] = new ModelRendererTurbo(this, 112, 52, textureX, textureY); // Box 79
		gunModel[34] = new ModelRendererTurbo(this, 80, 5, textureX, textureY); // Box 94
		gunModel[35] = new ModelRendererTurbo(this, 89, 1, textureX, textureY); // Box 95
		gunModel[36] = new ModelRendererTurbo(this, 80, 1, textureX, textureY); // Box 96
		gunModel[37] = new ModelRendererTurbo(this, 208, 127, textureX, textureY); // Box 2
		gunModel[38] = new ModelRendererTurbo(this, 208, 127, textureX, textureY); // Box 3
		gunModel[39] = new ModelRendererTurbo(this, 208, 127, textureX, textureY); // Box 4
		gunModel[40] = new ModelRendererTurbo(this, 208, 127, textureX, textureY); // Box 5
		gunModel[41] = new ModelRendererTurbo(this, 208, 105, textureX, textureY); // Box 7
		gunModel[42] = new ModelRendererTurbo(this, 151, 29, textureX, textureY); // Box 3
		gunModel[43] = new ModelRendererTurbo(this, 171, 125, textureX, textureY); // Box 6
		gunModel[44] = new ModelRendererTurbo(this, 186, 52, textureX, textureY); // Box 10
		gunModel[45] = new ModelRendererTurbo(this, 186, 62, textureX, textureY); // Box 11
		gunModel[46] = new ModelRendererTurbo(this, 143, 62, textureX, textureY); // Box 12
		gunModel[47] = new ModelRendererTurbo(this, 143, 67, textureX, textureY); // Box 13
		gunModel[48] = new ModelRendererTurbo(this, 189, 1, textureX, textureY); // Box 15
		gunModel[49] = new ModelRendererTurbo(this, 80, 10, textureX, textureY); // Box 2
		gunModel[50] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 3
		gunModel[51] = new ModelRendererTurbo(this, 112, 115, textureX, textureY); // Box 86
		gunModel[52] = new ModelRendererTurbo(this, 233, 90, textureX, textureY); // Box 0
		gunModel[53] = new ModelRendererTurbo(this, 168, 29, textureX, textureY); // Box 1
		gunModel[54] = new ModelRendererTurbo(this, 175, 107, textureX, textureY); // Box 2
		gunModel[55] = new ModelRendererTurbo(this, 175, 107, textureX, textureY); // Box 4
		gunModel[56] = new ModelRendererTurbo(this, 65, 114, textureX, textureY); // Box 5
		gunModel[57] = new ModelRendererTurbo(this, 65, 110, textureX, textureY); // Box 6
		gunModel[58] = new ModelRendererTurbo(this, 65, 110, textureX, textureY); // Box 7
		gunModel[59] = new ModelRendererTurbo(this, 65, 110, textureX, textureY); // Box 8
		gunModel[60] = new ModelRendererTurbo(this, 65, 114, textureX, textureY); // Box 9
		gunModel[61] = new ModelRendererTurbo(this, 65, 110, textureX, textureY); // Box 10
		gunModel[62] = new ModelRendererTurbo(this, 70, 25, textureX, textureY); // Box 11
		gunModel[63] = new ModelRendererTurbo(this, 142, 38, textureX, textureY); // Box 12
		gunModel[64] = new ModelRendererTurbo(this, 176, 42, textureX, textureY); // Box 13
		gunModel[65] = new ModelRendererTurbo(this, 161, 38, textureX, textureY); // Box 14
		gunModel[66] = new ModelRendererTurbo(this, 176, 47, textureX, textureY); // Box 15
		gunModel[67] = new ModelRendererTurbo(this, 161, 45, textureX, textureY); // Box 16
		gunModel[68] = new ModelRendererTurbo(this, 142, 45, textureX, textureY); // Box 17
		gunModel[69] = new ModelRendererTurbo(this, 129, 43, textureX, textureY); // Box 18
		gunModel[70] = new ModelRendererTurbo(this, 122, 38, textureX, textureY); // Box 19
		gunModel[71] = new ModelRendererTurbo(this, 112, 10, textureX, textureY); // Box 24
		gunModel[72] = new ModelRendererTurbo(this, 112, 1, textureX, textureY); // Box 25
		gunModel[73] = new ModelRendererTurbo(this, 176, 1, textureX, textureY); // Box 26
		gunModel[74] = new ModelRendererTurbo(this, 176, 7, textureX, textureY); // Box 27
		gunModel[75] = new ModelRendererTurbo(this, 176, 1, textureX, textureY); // Box 29
		gunModel[76] = new ModelRendererTurbo(this, 176, 1, textureX, textureY); // Box 30
		gunModel[77] = new ModelRendererTurbo(this, 176, 7, textureX, textureY); // Box 31
		gunModel[78] = new ModelRendererTurbo(this, 176, 1, textureX, textureY); // Box 32
		gunModel[79] = new ModelRendererTurbo(this, 157, 1, textureX, textureY); // Box 33
		gunModel[80] = new ModelRendererTurbo(this, 157, 7, textureX, textureY); // Box 34
		gunModel[81] = new ModelRendererTurbo(this, 157, 1, textureX, textureY); // Box 35
		gunModel[82] = new ModelRendererTurbo(this, 112, 38, textureX, textureY); // Box 0
		gunModel[83] = new ModelRendererTurbo(this, 205, 32, textureX, textureY); // Box 2
		gunModel[84] = new ModelRendererTurbo(this, 133, 106, textureX, textureY); // Box 3
		gunModel[85] = new ModelRendererTurbo(this, 96, 1, textureX, textureY); // Box 4
		gunModel[86] = new ModelRendererTurbo(this, 63, 4, textureX, textureY); // Box 5
		gunModel[87] = new ModelRendererTurbo(this, 63, 4, textureX, textureY); // Box 6
		gunModel[88] = new ModelRendererTurbo(this, 63, 4, textureX, textureY); // Box 7
		gunModel[89] = new ModelRendererTurbo(this, 63, 4, textureX, textureY); // Box 8
		gunModel[90] = new ModelRendererTurbo(this, 112, 10, textureX, textureY); // Box 9
		gunModel[91] = new ModelRendererTurbo(this, 112, 1, textureX, textureY); // Box 10
		gunModel[92] = new ModelRendererTurbo(this, 112, 10, textureX, textureY); // Box 11
		gunModel[93] = new ModelRendererTurbo(this, 157, 1, textureX, textureY); // Box 12
		gunModel[94] = new ModelRendererTurbo(this, 157, 7, textureX, textureY); // Box 13
		gunModel[95] = new ModelRendererTurbo(this, 157, 1, textureX, textureY); // Box 14
		gunModel[96] = new ModelRendererTurbo(this, 52, 2, textureX, textureY); // Box 15
		gunModel[97] = new ModelRendererTurbo(this, 20, 2, textureX, textureY); // Box 16
		gunModel[98] = new ModelRendererTurbo(this, 20, 2, textureX, textureY); // Box 17

		gunModel[0].addShapeBox(0F, 0F, 0F, 16, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // barrelPart2
		gunModel[0].setRotationPoint(39F, -22.5F, -3F);

		gunModel[1].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F); // body1
		gunModel[1].setRotationPoint(-19F, -13F, -3.5F);

		gunModel[2].addShapeBox(0F, 0F, 0F, 7, 6, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F); // bodyRail1
		gunModel[2].setRotationPoint(-17F, -22F, -3.5F);

		gunModel[3].addBox(0F, 0F, 0F, 5, 2, 4, 0F); // frontSight
		gunModel[3].setRotationPoint(31F, -26F, -2F);

		gunModel[4].addShapeBox(0F, 0F, 0F, 1, 6, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F); // slide1
		gunModel[4].setRotationPoint(-18F, -22F, -4F);

		gunModel[5].addShapeBox(0F, 0F, 0F, 20, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 36
		gunModel[5].setRotationPoint(-18F, -24F, -3.5F);

		gunModel[6].addBox(0F, 0F, 0F, 9, 6, 8, 0F); // Box 42
		gunModel[6].setRotationPoint(-7F, -22F, -4F);

		gunModel[7].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // body1
		gunModel[7].setRotationPoint(0F, 7F, -3.5F);

		gunModel[8].addShapeBox(0F, 0F, 0F, 3, 1, 7, 0F, 0F, 0F, 0F, -1F, 0F, -1F, -1F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // body1
		gunModel[8].setRotationPoint(0F, 6F, -3.5F);

		gunModel[9].addBox(0F, 0F, 0F, 29, 3, 7, 0F); // Box 0
		gunModel[9].setRotationPoint(-16F, -14F, -3.5F);

		gunModel[10].addBox(0F, 0F, 0F, 9, 2, 7, 0F); // Box 1
		gunModel[10].setRotationPoint(-4F, -11F, -3.5F);

		gunModel[11].addShapeBox(0F, 0F, 0F, 9, 6, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 3F, 0F, 0F); // Box 2
		gunModel[11].setRotationPoint(-4F, -9F, -3.5F);

		gunModel[12].addShapeBox(0F, 0F, 0F, 9, 9, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F); // Box 3
		gunModel[12].setRotationPoint(-7F, -3F, -3.5F);

		gunModel[13].addBox(0F, 0F, 0F, 9, 3, 7, 0F); // Box 4
		gunModel[13].setRotationPoint(-9F, 6F, -3.5F);

		gunModel[14].addShapeBox(0F, 0F, 0F, 2, 9, 7, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, -1F, -2F, 0F, -1F, 2F, 0F, 0F); // Box 5
		gunModel[14].setRotationPoint(2F, -3F, -3.5F);

		gunModel[15].addShapeBox(0F, 0F, 0F, 2, 6, 7, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, -1F, -3F, 0F, -1F, 3F, 0F, 0F); // Box 6
		gunModel[15].setRotationPoint(5F, -9F, -3.5F);

		gunModel[16].addShapeBox(0F, 0F, 0F, 1, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 8
		gunModel[16].setRotationPoint(-5F, -11F, -3.5F);

		gunModel[17].addShapeBox(0F, 0F, 0F, 1, 6, 7, 0F, -3F, 0F, -1F, 3F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 9
		gunModel[17].setRotationPoint(-8F, -9F, -3.5F);

		gunModel[18].addShapeBox(0F, 0F, 0F, 1, 9, 7, 0F, -2F, 0F, -1F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 10
		gunModel[18].setRotationPoint(-10F, -3F, -3.5F);

		gunModel[19].addShapeBox(0F, 0F, 0F, 1, 3, 7, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 11
		gunModel[19].setRotationPoint(-10F, 6F, -3.5F);

		gunModel[20].addBox(0F, 0F, 0F, 22, 2, 7, 0F); // Box 14
		gunModel[20].setRotationPoint(14F, -13F, -3.5F);

		gunModel[21].addBox(0F, 0F, 0F, 24, 1, 6, 0F); // Box 15
		gunModel[21].setRotationPoint(13F, -14F, -3F);

		gunModel[22].addShapeBox(0F, 0F, 0F, 1, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F); // Box 50
		gunModel[22].setRotationPoint(13F, -11F, -3F);

		gunModel[23].addShapeBox(0F, 0F, 0F, 1, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F); // Box 51
		gunModel[23].setRotationPoint(14F, -11F, -3.5F);

		gunModel[24].addShapeBox(0F, 0F, 0F, 8, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F); // Box 52
		gunModel[24].setRotationPoint(5F, -11F, -3.5F);

		gunModel[25].addBox(0F, 0F, 0F, 41, 2, 7, 0F); // Box 53
		gunModel[25].setRotationPoint(-19F, -16F, -3.5F);

		gunModel[26].addBox(0F, 0F, 0F, 1, 2, 6, 0F); // Box 54
		gunModel[26].setRotationPoint(13F, -13F, -3F);

		gunModel[27].addShapeBox(0F, 0F, 0F, 13, 2, 6, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 58
		gunModel[27].setRotationPoint(22F, -24F, -3F);

		gunModel[28].addShapeBox(0F, 0F, 0F, 1, 6, 8, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 72
		gunModel[28].setRotationPoint(-8F, -22F, -4F);

		gunModel[29].addShapeBox(0F, 0F, 0F, 1, 6, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F); // Box 73
		gunModel[29].setRotationPoint(-16F, -22F, -4F);

		gunModel[30].addShapeBox(0F, 0F, 0F, 1, 6, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F); // Box 74
		gunModel[30].setRotationPoint(-14F, -22F, -4F);

		gunModel[31].addShapeBox(0F, 0F, 0F, 1, 6, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F); // Box 75
		gunModel[31].setRotationPoint(-12F, -22F, -4F);

		gunModel[32].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F); // Box 77
		gunModel[32].setRotationPoint(-22F, -16F, -3.5F);

		gunModel[33].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, -1F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, -1F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 79
		gunModel[33].setRotationPoint(-20F, -24F, -3.5F);

		gunModel[34].addBox(0F, 0F, 0F, 12, 3, 1, 0F); // Box 94
		gunModel[34].setRotationPoint(-4F, -15F, 3F);

		gunModel[35].addBox(0F, 0F, 0F, 2, 2, 1, 0F); // Box 95
		gunModel[35].setRotationPoint(6F, -14.5F, 3.5F);

		gunModel[36].addShapeBox(0F, 0F, 0F, 3, 2, 1, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F); // Box 96
		gunModel[36].setRotationPoint(3F, -14.5F, 3.5F);

		gunModel[37].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 2
		gunModel[37].setRotationPoint(15F, -9F, -3.5F);

		gunModel[38].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 3
		gunModel[38].setRotationPoint(21F, -9F, -3.5F);

		gunModel[39].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 4
		gunModel[39].setRotationPoint(33F, -9F, -3.5F);

		gunModel[40].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 5
		gunModel[40].setRotationPoint(27F, -9F, -3.5F);

		gunModel[41].addShapeBox(0F, 0F, 0F, 1, 4, 7, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, -0.5F, 0F, -1F, -0.5F, 0F, 0F, 0F); // Box 7
		gunModel[41].setRotationPoint(36F, -13F, -3.5F);

		gunModel[42].addShapeBox(0F, 0F, 0F, 2, 2, 6, 0F, 0F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 3
		gunModel[42].setRotationPoint(35F, -24F, -3F);

		gunModel[43].addBox(0F, 0F, 0F, 10, 3, 8, 0F); // Box 6
		gunModel[43].setRotationPoint(2F, -19F, -4F);

		gunModel[44].addShapeBox(0F, 0F, 0F, 10, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 10
		gunModel[44].setRotationPoint(12F, -24F, -3.5F);

		gunModel[45].addBox(0F, 0F, 0F, 9, 6, 8, 0F); // Box 11
		gunModel[45].setRotationPoint(13F, -22F, -4F);

		gunModel[46].addShapeBox(0F, 0F, 0F, 10, 2, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 12
		gunModel[46].setRotationPoint(2F, -24F, 1.5F);

		gunModel[47].addBox(0F, 0F, 0F, 10, 4, 2, 0F); // Box 13
		gunModel[47].setRotationPoint(2F, -22F, 1.5F);

		gunModel[48].addBox(0F, 0F, 0F, 15, 6, 6, 0F); // Box 15
		gunModel[48].setRotationPoint(22F, -22F, -3F);

		gunModel[49].addBox(0F, 0F, 0F, 7, 2, 5, 0F); // Box 2
		gunModel[49].setRotationPoint(-17F, -25.5F, -2.5F);

		gunModel[50].addShapeBox(0F, 0F, 0F, 4, 1, 5, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 3
		gunModel[50].setRotationPoint(-14F, -26.5F, -2.5F);

		gunModel[51].addBox(0F, 0F, 0F, 21, 2, 7, 0F); // Box 86
		gunModel[51].setRotationPoint(15F, -11F, -3.5F);

		gunModel[52].addShapeBox(0F, 0F, 0F, 4, 6, 8, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 0
		gunModel[52].setRotationPoint(-22F, -22F, -4F);

		gunModel[53].addBox(0F, 0F, 0F, 15, 2, 6, 0F); // Box 1
		gunModel[53].setRotationPoint(22F, -16F, -3F);

		gunModel[54].addShapeBox(0F, 0F, 0F, 3, 1, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 2
		gunModel[54].setRotationPoint(-21F, -14F, -3F);

		gunModel[55].addBox(0F, 0F, 0F, 3, 1, 6, 0F); // Box 4
		gunModel[55].setRotationPoint(-21F, -15F, -3F);

		gunModel[56].addBox(0F, 0F, 0F, 4, 2, 2, 0F); // Box 5
		gunModel[56].setRotationPoint(-21.5F, -15.5F, -5F);

		gunModel[57].addShapeBox(0F, 0F, 0F, 4, 1, 2, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 6
		gunModel[57].setRotationPoint(-21.5F, -16.5F, -5F);

		gunModel[58].addShapeBox(0F, 0F, 0F, 4, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 7
		gunModel[58].setRotationPoint(-21.5F, -13.5F, -5F);

		gunModel[59].addShapeBox(0F, 0F, 0F, 4, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 8
		gunModel[59].setRotationPoint(-21.5F, -13.5F, 3F);

		gunModel[60].addBox(0F, 0F, 0F, 4, 2, 2, 0F); // Box 9
		gunModel[60].setRotationPoint(-21.5F, -15.5F, 3F);

		gunModel[61].addShapeBox(0F, 0F, 0F, 4, 1, 2, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 10
		gunModel[61].setRotationPoint(-21.5F, -16.5F, 3F);

		gunModel[62].addShapeBox(0F, 0F, 0F, 13, 1, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, -1F, 0F, -1F); // Box 11
		gunModel[62].setRotationPoint(-17F, -11F, -3.5F);

		gunModel[63].addBox(0F, 0F, 0F, 4, 1, 5, 0F); // Box 12
		gunModel[63].setRotationPoint(2F, -22F, 3.5F);

		gunModel[64].addBox(0F, 0F, 0F, 5, 1, 3, 0F); // Box 13
		gunModel[64].setRotationPoint(8F, -22F, 3.5F);

		gunModel[65].addShapeBox(0F, 0F, 0F, 2, 1, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F); // Box 14
		gunModel[65].setRotationPoint(6F, -22F, 3.5F);

		gunModel[66].addBox(0F, 0F, 0F, 5, 1, 3, 0F); // Box 15
		gunModel[66].setRotationPoint(8F, -18F, 3.5F);

		gunModel[67].addShapeBox(0F, 0F, 0F, 2, 1, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F); // Box 16
		gunModel[67].setRotationPoint(6F, -18F, 3.5F);

		gunModel[68].addBox(0F, 0F, 0F, 4, 1, 5, 0F); // Box 17
		gunModel[68].setRotationPoint(2F, -18F, 3.5F);

		gunModel[69].addBox(0F, 0F, 0F, 1, 3, 5, 0F); // Box 18
		gunModel[69].setRotationPoint(2F, -21F, 3.5F);

		gunModel[70].addBox(0F, 0F, 0F, 1, 3, 3, 0F); // Box 19
		gunModel[70].setRotationPoint(12F, -21F, 3.5F);

		gunModel[71].addShapeBox(0F, 0F, 0F, 16, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 24
		gunModel[71].setRotationPoint(39F, -18.5F, -3F);

		gunModel[72].addBox(0F, 0F, 0F, 16, 2, 6, 0F); // Box 25
		gunModel[72].setRotationPoint(39F, -20.5F, -3F);

		gunModel[73].addShapeBox(0F, 0F, 0F, 2, 1, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 26
		gunModel[73].setRotationPoint(37F, -15F, -2F);

		gunModel[74].addBox(0F, 0F, 0F, 2, 2, 4, 0F); // Box 27
		gunModel[74].setRotationPoint(37F, -14F, -2F);

		gunModel[75].addShapeBox(0F, 0F, 0F, 2, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 29
		gunModel[75].setRotationPoint(37F, -12F, -2F);

		gunModel[76].addShapeBox(0F, 0F, 0F, 2, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 30
		gunModel[76].setRotationPoint(37F, -18.5F, -2F);

		gunModel[77].addBox(0F, 0F, 0F, 2, 2, 4, 0F); // Box 31
		gunModel[77].setRotationPoint(37F, -20.5F, -2F);

		gunModel[78].addShapeBox(0F, 0F, 0F, 2, 1, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 32
		gunModel[78].setRotationPoint(37F, -21.5F, -2F);

		gunModel[79].addShapeBox(0F, 0F, 0F, 2, 1, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 33
		gunModel[79].setRotationPoint(55F, -21.5F, -2F);

		gunModel[80].addBox(0F, 0F, 0F, 2, 2, 4, 0F); // Box 34
		gunModel[80].setRotationPoint(55F, -20.5F, -2F);

		gunModel[81].addShapeBox(0F, 0F, 0F, 2, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 35
		gunModel[81].setRotationPoint(55F, -18.5F, -2F);

		gunModel[82].addBox(0F, 0F, 0F, 1, 6, 7, 0F); // Box 0
		gunModel[82].setRotationPoint(12F, -22F, -4F);

		gunModel[83].addBox(0F, 0F, 0F, 1, 1, 1, 0F); // Box 2
		gunModel[83].setRotationPoint(12F, -17F, 3F);

		gunModel[84].addBox(0F, 0F, 0F, 3, 1, 7, 0F); // Box 3
		gunModel[84].setRotationPoint(-19F, -14F, -3.5F);

		gunModel[85].addBox(0F, 0F, 0F, 1, 2, 1, 0F); // Box 4
		gunModel[85].setRotationPoint(-7F, -16F, -4.5F);

		gunModel[86].addShapeBox(0F, 0F, 0F, 4, 2, 1, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 5
		gunModel[86].setRotationPoint(-14F, -26.5F, -3.5F);

		gunModel[87].addShapeBox(0F, 0F, 0F, 4, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 6
		gunModel[87].setRotationPoint(-14F, -24.5F, -3.5F);

		gunModel[88].addShapeBox(0F, 0F, 0F, 4, 2, 1, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 7
		gunModel[88].setRotationPoint(-14F, -26.5F, 2.5F);

		gunModel[89].addShapeBox(0F, 0F, 0F, 4, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 8
		gunModel[89].setRotationPoint(-14F, -24.5F, 2.5F);

		gunModel[90].addShapeBox(0F, 0F, 0F, 16, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 9
		gunModel[90].setRotationPoint(39F, -12F, -3F);

		gunModel[91].addBox(0F, 0F, 0F, 16, 2, 6, 0F); // Box 10
		gunModel[91].setRotationPoint(39F, -14F, -3F);

		gunModel[92].addShapeBox(0F, 0F, 0F, 16, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 11
		gunModel[92].setRotationPoint(39F, -16F, -3F);

		gunModel[93].addShapeBox(0F, 0F, 0F, 2, 1, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 12
		gunModel[93].setRotationPoint(55F, -15F, -2F);

		gunModel[94].addBox(0F, 0F, 0F, 2, 2, 4, 0F); // Box 13
		gunModel[94].setRotationPoint(55F, -14F, -2F);

		gunModel[95].addShapeBox(0F, 0F, 0F, 2, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 14
		gunModel[95].setRotationPoint(55F, -12F, -2F);

		gunModel[96].addShapeBox(0F, 0F, 0F, 3, 3, 2, 0F, -1F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -1F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 15
		gunModel[96].setRotationPoint(-13F, -29.5F, -1F);

		gunModel[97].addShapeBox(0F, 0F, 0F, 5, 4, 1, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 16
		gunModel[97].setRotationPoint(31F, -30F, 1F);

		gunModel[98].addShapeBox(0F, 0F, 0F, 5, 4, 1, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 17
		gunModel[98].setRotationPoint(31F, -30F, -2F);


		defaultStockModel = new ModelRendererTurbo[18];
		defaultStockModel[0] = new ModelRendererTurbo(this, 1, 115, textureX, textureY); // Box 36
		defaultStockModel[1] = new ModelRendererTurbo(this, 1, 115, textureX, textureY); // Box 39
		defaultStockModel[2] = new ModelRendererTurbo(this, 1, 115, textureX, textureY); // Box 40
		defaultStockModel[3] = new ModelRendererTurbo(this, 1, 115, textureX, textureY); // Box 41
		defaultStockModel[4] = new ModelRendererTurbo(this, 28, 108, textureX, textureY); // Box 42
		defaultStockModel[5] = new ModelRendererTurbo(this, 1, 108, textureX, textureY); // Box 43
		defaultStockModel[6] = new ModelRendererTurbo(this, 62, 103, textureX, textureY); // Box 44
		defaultStockModel[7] = new ModelRendererTurbo(this, 1, 81, textureX, textureY); // Box 45
		defaultStockModel[8] = new ModelRendererTurbo(this, 32, 91, textureX, textureY); // Box 46
		defaultStockModel[9] = new ModelRendererTurbo(this, 47, 81, textureX, textureY); // Box 47
		defaultStockModel[10] = new ModelRendererTurbo(this, 32, 81, textureX, textureY); // Box 48
		defaultStockModel[11] = new ModelRendererTurbo(this, 28, 103, textureX, textureY); // Box 50
		defaultStockModel[12] = new ModelRendererTurbo(this, 1, 103, textureX, textureY); // Box 51
		defaultStockModel[13] = new ModelRendererTurbo(this, 55, 105, textureX, textureY); // Box 53
		defaultStockModel[14] = new ModelRendererTurbo(this, 61, 92, textureX, textureY); // Box 54
		defaultStockModel[15] = new ModelRendererTurbo(this, 54, 114, textureX, textureY); // Box 55
		defaultStockModel[16] = new ModelRendererTurbo(this, 54, 114, textureX, textureY); // Box 56
		defaultStockModel[17] = new ModelRendererTurbo(this, 44, 119, textureX, textureY); // Box 3

		defaultStockModel[0].addShapeBox(0F, 0F, 0F, 24, 1, 2, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 36
		defaultStockModel[0].setRotationPoint(-44.5F, -15.5F, -3F);

		defaultStockModel[1].addShapeBox(0F, 0F, 0F, 24, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 39
		defaultStockModel[1].setRotationPoint(-44.5F, -14.5F, -3F);

		defaultStockModel[2].addShapeBox(0F, 0F, 0F, 24, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 40
		defaultStockModel[2].setRotationPoint(-44.5F, -14.5F, 1F);

		defaultStockModel[3].addShapeBox(0F, 0F, 0F, 24, 1, 2, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 41
		defaultStockModel[3].setRotationPoint(-44.5F, -15.5F, 1F);

		defaultStockModel[4].addBox(0F, 0F, 0F, 10, 2, 3, 0F); // Box 42
		defaultStockModel[4].setRotationPoint(-54.5F, -15.5F, -4F);

		defaultStockModel[5].addBox(0F, 0F, 0F, 10, 2, 3, 0F); // Box 43
		defaultStockModel[5].setRotationPoint(-54.5F, -15.5F, 1F);

		defaultStockModel[6].addBox(0F, 0F, 0F, 6, 3, 2, 0F); // Box 44
		defaultStockModel[6].setRotationPoint(-54.5F, -16.5F, -1F);

		defaultStockModel[7].addShapeBox(0F, 0F, 0F, 5, 13, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F); // Box 45
		defaultStockModel[7].setRotationPoint(-52.5F, -10.5F, -4F);

		defaultStockModel[8].addBox(0F, 0F, 0F, 6, 3, 8, 0F); // Box 46
		defaultStockModel[8].setRotationPoint(-54.5F, -13.5F, -4F);

		defaultStockModel[9].addShapeBox(0F, 0F, 0F, 4, 3, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F); // Box 47
		defaultStockModel[9].setRotationPoint(-48.5F, -13.5F, -4F);

		defaultStockModel[10].addShapeBox(0F, 0F, 0F, 4, 3, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F); // Box 48
		defaultStockModel[10].setRotationPoint(-48.5F, -13.5F, 1F);

		defaultStockModel[11].addShapeBox(0F, 0F, 0F, 10, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 50
		defaultStockModel[11].setRotationPoint(-54.5F, -16.5F, -4F);

		defaultStockModel[12].addShapeBox(0F, 0F, 0F, 10, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 51
		defaultStockModel[12].setRotationPoint(-54.5F, -16.5F, 1F);

		defaultStockModel[13].addBox(0F, 0F, 0F, 1, 6, 2, 0F); // Box 53
		defaultStockModel[13].setRotationPoint(-48.5F, -16.5F, -1F);

		defaultStockModel[14].addShapeBox(0F, 0F, 0F, 3, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, -0.5F, 0F, -2F, -0.5F, 0F, -2F, 0F, 0F, -2F); // Box 54
		defaultStockModel[14].setRotationPoint(-54.5F, 2.5F, -4F);

		defaultStockModel[15].addShapeBox(0F, 0F, 0F, 2, 1, 3, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 55
		defaultStockModel[15].setRotationPoint(-43.5F, -15.5F, -1.5F);

		defaultStockModel[16].addShapeBox(0F, 0F, 0F, 2, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 56
		defaultStockModel[16].setRotationPoint(-43.5F, -14.5F, -1.5F);

		defaultStockModel[17].addBox(0F, 0F, 0F, 2, 13, 8, 0F); // Box 3
		defaultStockModel[17].setRotationPoint(-54.5F, -10.5F, -4F);


		ammoModel = new ModelRendererTurbo[3];
		ammoModel[0] = new ModelRendererTurbo(this, 1, 119, textureX, textureY); // Box 20
		ammoModel[1] = new ModelRendererTurbo(this, 8, 119, textureX, textureY); // Box 0
		ammoModel[2] = new ModelRendererTurbo(this, 1, 119, textureX, textureY); // Box 1

		ammoModel[0].addBox(0F, 0F, 0F, 10, 4, 22, 0F); // Box 20
		ammoModel[0].setRotationPoint(2.5F, -21.5F, 3.5F);

		ammoModel[1].addShapeBox(0F, 0F, 0F, 6, 2, 1, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 0
		ammoModel[1].setRotationPoint(3.5F, -20.5F, 2.5F);

		ammoModel[2].addShapeBox(0F, 0F, 0F, 2, 2, 1, 0F, 0F, -0.5F, 0F, -0.5F, -0.75F, -0.25F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, -0.5F, -0.75F, -0.25F, 0F, -0.5F, 0F, 0F, 0F, 0F); // Box 1
		ammoModel[2].setRotationPoint(9.5F, -20.5F, 2.5F);


		slideModel = new ModelRendererTurbo[2];
		slideModel[0] = new ModelRendererTurbo(this, 112, 62, textureX, textureY); // Box 8
		slideModel[1] = new ModelRendererTurbo(this, 112, 70, textureX, textureY); // Box 9

		slideModel[0].addShapeBox(0F, 0F, 0F, 10, 2, 5, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 8
		slideModel[0].setRotationPoint(2F, -23.5F, -3F);

		slideModel[1].addBox(0F, 0F, 0F, 10, 3, 5, 0F); // Box 9
		slideModel[1].setRotationPoint(2F, -21.5F, -3F);

		stockAttachPoint = new Vector3f(-10F /16F, 15F /16F, 0F /16F);

		
		animationType = EnumAnimationType.CUSTOM;

		/* ----Start of Reload Block---- */
		/* Reload Name: Amended Side Clip */
		rotateClipHorizontal = 120F;
		rotateClipVertical = 60F;
		translateClip = new Vector3f(0.5F, 0F, 0F);
		/* ----End of Reload Block---- */

		gunSlideDistance = 1.3F;

		flipAll();
		translateAll(0F, 0F, 0F);
	}
}