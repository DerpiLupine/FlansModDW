package com.flansmod.client.model.dwfirearms; //Path where the model is located

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelWolfstedtM2088 extends ModelGun //Same as Filename
{
	int textureX = 512;
	int textureY = 256;

	public ModelWolfstedtM2088() //Same as Filename
	{
		gunModel = new ModelRendererTurbo[63];
		gunModel[0] = new ModelRendererTurbo(this, 21, 11, textureX, textureY); // Box 0
		gunModel[1] = new ModelRendererTurbo(this, 140, 19, textureX, textureY); // Box 1
		gunModel[2] = new ModelRendererTurbo(this, 165, 86, textureX, textureY); // Box 2
		gunModel[3] = new ModelRendererTurbo(this, 140, 35, textureX, textureY); // Box 3
		gunModel[4] = new ModelRendererTurbo(this, 140, 48, textureX, textureY); // Box 4
		gunModel[5] = new ModelRendererTurbo(this, 140, 59, textureX, textureY); // Box 5
		gunModel[6] = new ModelRendererTurbo(this, 195, 90, textureX, textureY); // Box 6
		gunModel[7] = new ModelRendererTurbo(this, 195, 80, textureX, textureY); // Box 7
		gunModel[8] = new ModelRendererTurbo(this, 195, 75, textureX, textureY); // Box 8
		gunModel[9] = new ModelRendererTurbo(this, 195, 70, textureX, textureY); // Box 9
		gunModel[10] = new ModelRendererTurbo(this, 140, 70, textureX, textureY); // Box 12
		gunModel[11] = new ModelRendererTurbo(this, 195, 61, textureX, textureY); // Box 13
		gunModel[12] = new ModelRendererTurbo(this, 180, 92, textureX, textureY); // Box 14
		gunModel[13] = new ModelRendererTurbo(this, 140, 85, textureX, textureY); // Box 15
		gunModel[14] = new ModelRendererTurbo(this, 140, 10, textureX, textureY); // Box 19
		gunModel[15] = new ModelRendererTurbo(this, 140, 1, textureX, textureY); // Box 20
		gunModel[16] = new ModelRendererTurbo(this, 140, 1, textureX, textureY); // Box 21
		gunModel[17] = new ModelRendererTurbo(this, 1, 18, textureX, textureY); // Box 27
		gunModel[18] = new ModelRendererTurbo(this, 1, 56, textureX, textureY); // Box 28
		gunModel[19] = new ModelRendererTurbo(this, 32, 49, textureX, textureY); // Box 29
		gunModel[20] = new ModelRendererTurbo(this, 57, 21, textureX, textureY); // Box 30
		gunModel[21] = new ModelRendererTurbo(this, 32, 49, textureX, textureY); // Box 31
		gunModel[22] = new ModelRendererTurbo(this, 40, 18, textureX, textureY); // Box 32
		gunModel[23] = new ModelRendererTurbo(this, 49, 49, textureX, textureY); // Box 33
		gunModel[24] = new ModelRendererTurbo(this, 49, 49, textureX, textureY); // Box 34
		gunModel[25] = new ModelRendererTurbo(this, 32, 60, textureX, textureY); // Box 36
		gunModel[26] = new ModelRendererTurbo(this, 49, 49, textureX, textureY); // Box 38
		gunModel[27] = new ModelRendererTurbo(this, 49, 39, textureX, textureY); // Box 39
		gunModel[28] = new ModelRendererTurbo(this, 1, 37, textureX, textureY); // Box 40
		gunModel[29] = new ModelRendererTurbo(this, 1, 47, textureX, textureY); // Box 41
		gunModel[30] = new ModelRendererTurbo(this, 32, 37, textureX, textureY); // Box 42
		gunModel[31] = new ModelRendererTurbo(this, 43, 9, textureX, textureY); // rail1
		gunModel[32] = new ModelRendererTurbo(this, 43, 1, textureX, textureY); // rail2
		gunModel[33] = new ModelRendererTurbo(this, 43, 15, textureX, textureY); // rail3
		gunModel[34] = new ModelRendererTurbo(this, 43, 15, textureX, textureY); // rail3-2
		gunModel[35] = new ModelRendererTurbo(this, 74, 30, textureX, textureY); // Box 18
		gunModel[36] = new ModelRendererTurbo(this, 74, 23, textureX, textureY); // Box 19
		gunModel[37] = new ModelRendererTurbo(this, 110, 31, textureX, textureY); // Box 20
		gunModel[38] = new ModelRendererTurbo(this, 110, 31, textureX, textureY); // Box 21
		gunModel[39] = new ModelRendererTurbo(this, 110, 27, textureX, textureY); // Box 22
		gunModel[40] = new ModelRendererTurbo(this, 110, 27, textureX, textureY); // Box 23
		gunModel[41] = new ModelRendererTurbo(this, 110, 27, textureX, textureY); // Box 24
		gunModel[42] = new ModelRendererTurbo(this, 110, 27, textureX, textureY); // Box 25
		gunModel[43] = new ModelRendererTurbo(this, 103, 23, textureX, textureY); // Box 26
		gunModel[44] = new ModelRendererTurbo(this, 103, 23, textureX, textureY); // Box 27
		gunModel[45] = new ModelRendererTurbo(this, 74, 37, textureX, textureY); // Box 28
		gunModel[46] = new ModelRendererTurbo(this, 74, 37, textureX, textureY); // Box 29
		gunModel[47] = new ModelRendererTurbo(this, 110, 31, textureX, textureY); // Box 30
		gunModel[48] = new ModelRendererTurbo(this, 110, 31, textureX, textureY); // Box 31
		gunModel[49] = new ModelRendererTurbo(this, 74, 37, textureX, textureY); // Box 32
		gunModel[50] = new ModelRendererTurbo(this, 74, 37, textureX, textureY); // Box 33
		gunModel[51] = new ModelRendererTurbo(this, 123, 23, textureX, textureY); // Box 34
		gunModel[52] = new ModelRendererTurbo(this, 110, 23, textureX, textureY); // Box 35
		gunModel[53] = new ModelRendererTurbo(this, 199, 104, textureX, textureY); // Box 36
		gunModel[54] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 37
		gunModel[55] = new ModelRendererTurbo(this, 1, 11, textureX, textureY); // Box 38
		gunModel[56] = new ModelRendererTurbo(this, 87, 38, textureX, textureY); // Box 40
		gunModel[57] = new ModelRendererTurbo(this, 87, 38, textureX, textureY); // Box 41
		gunModel[58] = new ModelRendererTurbo(this, 100, 39, textureX, textureY); // Box 42
		gunModel[59] = new ModelRendererTurbo(this, 110, 23, textureX, textureY); // Box 43
		gunModel[60] = new ModelRendererTurbo(this, 110, 23, textureX, textureY); // Box 44
		gunModel[61] = new ModelRendererTurbo(this, 22, 125, textureX, textureY); // Box 85
		gunModel[62] = new ModelRendererTurbo(this, 1, 125, textureX, textureY); // Box 86

		gunModel[0].addShapeBox(0F, 0F, 0F, 8, 3, 1, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 0
		gunModel[0].setRotationPoint(-23F, -28F, 1.5F);

		gunModel[1].addBox(0F, 0F, 0F, 35, 8, 7, 0F); // Box 1
		gunModel[1].setRotationPoint(-24F, -23F, -3F);

		gunModel[2].addBox(0F, 0F, 0F, 1, 11, 6, 0F); // Box 2
		gunModel[2].setRotationPoint(-25F, -23F, -3F);

		gunModel[3].addBox(0F, 0F, 0F, 35, 3, 9, 0F); // Box 3
		gunModel[3].setRotationPoint(-24F, -15F, -4.5F);

		gunModel[4].addShapeBox(0F, 0F, 0F, 35, 1, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 4
		gunModel[4].setRotationPoint(-24F, -12F, -4.5F);

		gunModel[5].addShapeBox(0F, 0F, 0F, 20, 3, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F); // Box 5
		gunModel[5].setRotationPoint(-9F, -11F, -3.5F);

		gunModel[6].addBox(0F, 0F, 0F, 11, 8, 1, 0F); // Box 6
		gunModel[6].setRotationPoint(-24F, -23F, -4F);

		gunModel[7].addBox(0F, 0F, 0F, 10, 8, 1, 0F); // Box 7
		gunModel[7].setRotationPoint(1F, -23F, -4F);

		gunModel[8].addBox(0F, 0F, 0F, 14, 3, 1, 0F); // Box 8
		gunModel[8].setRotationPoint(-13F, -23F, -4F);

		gunModel[9].addBox(0F, 0F, 0F, 14, 3, 1, 0F); // Box 9
		gunModel[9].setRotationPoint(-13F, -18F, -4F);

		gunModel[10].addBox(0F, 0F, 0F, 12, 6, 8, 0F); // Box 12
		gunModel[10].setRotationPoint(11F, -21F, -4F);

		gunModel[11].addBox(0F, 0F, 0F, 12, 2, 6, 0F); // Box 13
		gunModel[11].setRotationPoint(11F, -23F, -2F);

		gunModel[12].addBox(0F, 0F, 0F, 2, 3, 8, 0F); // Box 14
		gunModel[12].setRotationPoint(21F, -15F, -4F);

		gunModel[13].addBox(0F, 0F, 0F, 5, 11, 7, 0F); // Box 15
		gunModel[13].setRotationPoint(23F, -23F, -3.5F);

		gunModel[14].addBox(0F, 0F, 0F, 37, 2, 6, 0F); // Box 19
		gunModel[14].setRotationPoint(28F, -20.5F, -3F);

		gunModel[15].addShapeBox(0F, 0F, 0F, 37, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 20
		gunModel[15].setRotationPoint(28F, -18.5F, -3F);

		gunModel[16].addShapeBox(0F, 0F, 0F, 37, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 21
		gunModel[16].setRotationPoint(28F, -22.5F, -3F);

		gunModel[17].addShapeBox(0F, 0F, 0F, 12, 11, 7, 0F, -5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -5F, 0F, 0F, 0F, -3F, 0F, -5F, 0F, 0F, -5F, 0F, 0F, 0F, -3F, 0F); // Box 27
		gunModel[17].setRotationPoint(-9F, -5F, -3.5F);

		gunModel[18].addBox(0F, 0F, 0F, 7, 3, 7, 0F); // Box 28
		gunModel[18].setRotationPoint(-4F, -8F, -3.5F);

		gunModel[19].addShapeBox(0F, 0F, 0F, 1, 3, 7, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 29
		gunModel[19].setRotationPoint(-5F, -8F, -3.5F);

		gunModel[20].addShapeBox(0F, 0F, 0F, 1, 8, 7, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 5F, 0F, -1F, -5F, 0F, 0F, -5F, 0F, 0F, 5F, 0F, -1F); // Box 30
		gunModel[20].setRotationPoint(-5F, -5F, -3.5F);

		gunModel[21].addShapeBox(0F, 0F, 0F, 1, 3, 7, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 31
		gunModel[21].setRotationPoint(3F, -8F, -3.5F);

		gunModel[22].addShapeBox(0F, 0F, 0F, 1, 11, 7, 0F, -5F, 0F, 0F, 5F, 0F, -1F, 5F, 0F, -1F, -5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 32
		gunModel[22].setRotationPoint(-2F, -5F, -3.5F);

		gunModel[23].addShapeBox(0F, 0F, 0F, 1, 1, 7, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0.5F, 0F, 0F, 1F, 0F, -1F, 1F, 0F, -1F, 0.5F, 0F, 0F); // Box 33
		gunModel[23].setRotationPoint(1F, -1F, -3.5F);

		gunModel[24].addShapeBox(0F, 0F, 0F, 1, 1, 7, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, -1F, 1F, 0F, -1F, -1F, 0F, 0F); // Box 34
		gunModel[24].setRotationPoint(-2F, 6F, -3.5F);

		gunModel[25].addShapeBox(0F, 0F, 0F, 1, 2, 7, 0F, 0.4F, 0F, 0F, 1F, 0F, -1F, 1F, 0F, -1F, 0.4F, 0F, 0F, 1.25F, 0F, 0F, 0.5F, 0F, -1F, 0.5F, 0F, -1F, 1.25F, 0F, 0F); // Box 36
		gunModel[25].setRotationPoint(1F, 0F, -3.5F);

		gunModel[26].addShapeBox(0F, 0F, 0F, 1, 1, 7, 0F, 0.25F, 0F, 0F, 1.5F, 0F, -1F, 1.5F, 0F, -1F, 0.25F, 0F, 0F, 0.65F, 0F, 0F, -0.65F, 0F, -1F, -0.65F, 0F, -1F, 0.65F, 0F, 0F); // Box 38
		gunModel[26].setRotationPoint(0F, 2F, -3.5F);

		gunModel[27].addShapeBox(0F, 0F, 0F, 1, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 39
		gunModel[27].setRotationPoint(-1F, 7F, -3.5F);

		gunModel[28].addShapeBox(0F, 0F, 0F, 8, 2, 7, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F); // Box 40
		gunModel[28].setRotationPoint(-9F, 7F, -3.5F);

		gunModel[29].addShapeBox(0F, 0F, 0F, 8, 1, 7, 0F, 0F, 3F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 3F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F); // Box 41
		gunModel[29].setRotationPoint(-9F, 6F, -3.5F);

		gunModel[30].addShapeBox(0F, 0F, 0F, 1, 4, 7, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -1F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, -1F); // Box 42
		gunModel[30].setRotationPoint(-10F, 3F, -3.5F);

		gunModel[31].addBox(0F, 0F, 0F, 18, 1, 4, 0F); // rail1
		gunModel[31].setRotationPoint(9F, -24F, -2F);

		gunModel[32].addBox(0F, 0F, 0F, 18, 1, 6, 0F); // rail2
		gunModel[32].setRotationPoint(9F, -25F, -3F);

		gunModel[33].addShapeBox(0F, 0F, 0F, 18, 1, 1, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // rail3
		gunModel[33].setRotationPoint(9F, -26F, 2F);

		gunModel[34].addShapeBox(0F, 0F, 0F, 18, 1, 1, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // rail3-2
		gunModel[34].setRotationPoint(9F, -26F, -3F);

		gunModel[35].addShapeBox(0F, 0F, 0F, 10, 2, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 18
		gunModel[35].setRotationPoint(12F, -21F, 4F);

		gunModel[36].addShapeBox(0F, 0F, 0F, 10, 2, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 19
		gunModel[36].setRotationPoint(12F, -19F, 4F);

		gunModel[37].addShapeBox(0F, 0F, 0F, 3, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 20
		gunModel[37].setRotationPoint(9F, -19F, 5F);

		gunModel[38].addShapeBox(0F, 0F, 0F, 3, 1, 2, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 21
		gunModel[38].setRotationPoint(9F, -20F, 5F);

		gunModel[39].addShapeBox(0F, 0F, 0F, 5, 1, 2, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 2F, -0.5F, 0F, 2F, -0.5F, 0F, 0F, -0.5F); // Box 22
		gunModel[39].setRotationPoint(4F, -21F, 5F);

		gunModel[40].addShapeBox(0F, 0F, 0F, 5, 1, 2, 0F, 0F, 0F, -0.5F, 0F, -2F, -0.5F, 0F, -2F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F); // Box 23
		gunModel[40].setRotationPoint(4F, -22F, 5F);

		gunModel[41].addShapeBox(0F, 0F, 0F, 5, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 24
		gunModel[41].setRotationPoint(-1F, -21F, 5F);

		gunModel[42].addShapeBox(0F, 0F, 0F, 5, 1, 2, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 25
		gunModel[42].setRotationPoint(-1F, -22F, 5F);

		gunModel[43].addShapeBox(0F, 0F, 0F, 1, 11, 2, 0F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F); // Box 26
		gunModel[43].setRotationPoint(-2F, -21F, 5F);

		gunModel[44].addShapeBox(0F, 0F, 0F, 1, 11, 2, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F); // Box 27
		gunModel[44].setRotationPoint(-1F, -21F, 5F);

		gunModel[45].addShapeBox(0F, 0F, 0F, 2, 1, 4, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 28
		gunModel[45].setRotationPoint(-2F, -11F, 3F);

		gunModel[46].addShapeBox(0F, 0F, 0F, 2, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 29
		gunModel[46].setRotationPoint(-2F, -10F, 3F);

		gunModel[47].addShapeBox(0F, 0F, 0F, 3, 1, 2, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 30
		gunModel[47].setRotationPoint(22F, -20F, 5F);

		gunModel[48].addShapeBox(0F, 0F, 0F, 3, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 31
		gunModel[48].setRotationPoint(22F, -19F, 5F);

		gunModel[49].addShapeBox(0F, 0F, 0F, 2, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 32
		gunModel[49].setRotationPoint(24F, -19F, 3F);

		gunModel[50].addShapeBox(0F, 0F, 0F, 2, 1, 4, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 33
		gunModel[50].setRotationPoint(24F, -20F, 3F);

		gunModel[51].addShapeBox(0F, 0F, 0F, 1, 1, 2, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F); // Box 34
		gunModel[51].setRotationPoint(-2F, -22F, 5F);

		gunModel[52].addBox(0F, 0F, 0F, 4, 1, 2, 0F); // Box 35
		gunModel[52].setRotationPoint(-1F, -21.5F, 4F);

		gunModel[53].addBox(0F, 0F, 0F, 9, 3, 1, 0F); // Box 36
		gunModel[53].setRotationPoint(-2.5F, -11.5F, 3.5F);

		gunModel[54].addBox(0F, 0F, 0F, 9, 2, 7, 0F); // Box 37
		gunModel[54].setRotationPoint(-23F, -25F, -3.5F);

		gunModel[55].addShapeBox(0F, 0F, 0F, 8, 3, 1, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 38
		gunModel[55].setRotationPoint(-23F, -28F, -2.5F);

		gunModel[56].addShapeBox(0F, 0F, 0F, 4, 2, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 40
		gunModel[56].setRotationPoint(0.5F, -17.5F, 3.5F);

		gunModel[57].addShapeBox(0F, 0F, 0F, 4, 2, 2, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 41
		gunModel[57].setRotationPoint(0.5F, -19.5F, 3.5F);

		gunModel[58].addShapeBox(0F, 0F, 0F, 2, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 42
		gunModel[58].setRotationPoint(3.5F, -17.5F, 3.5F);

		gunModel[59].addBox(0F, 0F, 0F, 4, 1, 2, 0F); // Box 43
		gunModel[59].setRotationPoint(15F, -21.5F, 4F);

		gunModel[60].addBox(0F, 0F, 0F, 4, 1, 2, 0F); // Box 44
		gunModel[60].setRotationPoint(15F, -17.5F, 4F);

		gunModel[61].addShapeBox(0F, 0F, 0F, 2, 1, 4, 0F, 0F, 0F, -1F, 0F, -0.75F, -1.25F, 0F, -0.75F, -1.25F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0.25F, -0.75F, 0F, 0.25F, -0.75F, 0F, 0F, 0F); // Box 85
		gunModel[61].setRotationPoint(18F, -18F, -2F);

		gunModel[62].addShapeBox(0F, 0F, 0F, 6, 1, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 86
		gunModel[62].setRotationPoint(12F, -18F, -2F);


		defaultBarrelModel = new ModelRendererTurbo[16];
		defaultBarrelModel[0] = new ModelRendererTurbo(this, 227, 11, textureX, textureY); // Box 16
		defaultBarrelModel[1] = new ModelRendererTurbo(this, 227, 1, textureX, textureY); // Box 17
		defaultBarrelModel[2] = new ModelRendererTurbo(this, 227, 1, textureX, textureY); // Box 18
		defaultBarrelModel[3] = new ModelRendererTurbo(this, 248, 11, textureX, textureY); // Box 6
		defaultBarrelModel[4] = new ModelRendererTurbo(this, 248, 1, textureX, textureY); // Box 7
		defaultBarrelModel[5] = new ModelRendererTurbo(this, 248, 11, textureX, textureY); // Box 8
		defaultBarrelModel[6] = new ModelRendererTurbo(this, 248, 1, textureX, textureY); // Box 9
		defaultBarrelModel[7] = new ModelRendererTurbo(this, 248, 11, textureX, textureY); // Box 10
		defaultBarrelModel[8] = new ModelRendererTurbo(this, 248, 1, textureX, textureY); // Box 11
		defaultBarrelModel[9] = new ModelRendererTurbo(this, 248, 11, textureX, textureY); // Box 12
		defaultBarrelModel[10] = new ModelRendererTurbo(this, 248, 1, textureX, textureY); // Box 13
		defaultBarrelModel[11] = new ModelRendererTurbo(this, 248, 11, textureX, textureY); // Box 14
		defaultBarrelModel[12] = new ModelRendererTurbo(this, 248, 1, textureX, textureY); // Box 15
		defaultBarrelModel[13] = new ModelRendererTurbo(this, 248, 11, textureX, textureY); // Box 16
		defaultBarrelModel[14] = new ModelRendererTurbo(this, 248, 1, textureX, textureY); // Box 17
		defaultBarrelModel[15] = new ModelRendererTurbo(this, 74, 43, textureX, textureY); // Box 39

		defaultBarrelModel[0].addBox(0F, 0F, 0F, 3, 3, 7, 0F); // Box 16
		defaultBarrelModel[0].setRotationPoint(65F, -21F, -3.5F);

		defaultBarrelModel[1].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 17
		defaultBarrelModel[1].setRotationPoint(65F, -23F, -3.5F);

		defaultBarrelModel[2].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 18
		defaultBarrelModel[2].setRotationPoint(65F, -18F, -3.5F);

		defaultBarrelModel[3].addBox(0F, 0F, 0F, 1, 3, 7, 0F); // Box 6
		defaultBarrelModel[3].setRotationPoint(30F, -21F, -3.5F);

		defaultBarrelModel[4].addShapeBox(0F, 0F, 0F, 1, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 7
		defaultBarrelModel[4].setRotationPoint(30F, -23F, -3.5F);

		defaultBarrelModel[5].addBox(0F, 0F, 0F, 1, 3, 7, 0F); // Box 8
		defaultBarrelModel[5].setRotationPoint(34F, -21F, -3.5F);

		defaultBarrelModel[6].addShapeBox(0F, 0F, 0F, 1, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 9
		defaultBarrelModel[6].setRotationPoint(34F, -23F, -3.5F);

		defaultBarrelModel[7].addBox(0F, 0F, 0F, 1, 3, 7, 0F); // Box 10
		defaultBarrelModel[7].setRotationPoint(42F, -21F, -3.5F);

		defaultBarrelModel[8].addShapeBox(0F, 0F, 0F, 1, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 11
		defaultBarrelModel[8].setRotationPoint(42F, -23F, -3.5F);

		defaultBarrelModel[9].addBox(0F, 0F, 0F, 1, 3, 7, 0F); // Box 12
		defaultBarrelModel[9].setRotationPoint(38F, -21F, -3.5F);

		defaultBarrelModel[10].addShapeBox(0F, 0F, 0F, 1, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 13
		defaultBarrelModel[10].setRotationPoint(38F, -23F, -3.5F);

		defaultBarrelModel[11].addBox(0F, 0F, 0F, 1, 3, 7, 0F); // Box 14
		defaultBarrelModel[11].setRotationPoint(50F, -21F, -3.5F);

		defaultBarrelModel[12].addShapeBox(0F, 0F, 0F, 1, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 15
		defaultBarrelModel[12].setRotationPoint(50F, -23F, -3.5F);

		defaultBarrelModel[13].addBox(0F, 0F, 0F, 1, 3, 7, 0F); // Box 16
		defaultBarrelModel[13].setRotationPoint(46F, -21F, -3.5F);

		defaultBarrelModel[14].addShapeBox(0F, 0F, 0F, 1, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 17
		defaultBarrelModel[14].setRotationPoint(46F, -23F, -3.5F);

		defaultBarrelModel[15].addShapeBox(0F, 0F, 0F, 3, 4, 3, 0F, 0F, 0F, -1F, -2F, 0F, -1F, -2F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 39
		defaultBarrelModel[15].setRotationPoint(65F, -27F, -1.5F);


		defaultStockModel = new ModelRendererTurbo[12];
		defaultStockModel[0] = new ModelRendererTurbo(this, 45, 146, textureX, textureY); // Box 41
		defaultStockModel[1] = new ModelRendererTurbo(this, 45, 132, textureX, textureY); // Box 42
		defaultStockModel[2] = new ModelRendererTurbo(this, 45, 159, textureX, textureY); // Box 43
		defaultStockModel[3] = new ModelRendererTurbo(this, 89, 157, textureX, textureY); // Box 44
		defaultStockModel[4] = new ModelRendererTurbo(this, 45, 170, textureX, textureY); // Box 45
		defaultStockModel[5] = new ModelRendererTurbo(this, 45, 71, textureX, textureY); // Box 46
		defaultStockModel[6] = new ModelRendererTurbo(this, 45, 110, textureX, textureY); // Box 47
		defaultStockModel[7] = new ModelRendererTurbo(this, 45, 97, textureX, textureY); // Box 0
		defaultStockModel[8] = new ModelRendererTurbo(this, 45, 121, textureX, textureY); // Box 1
		defaultStockModel[9] = new ModelRendererTurbo(this, 90, 132, textureX, textureY); // Box 2
		defaultStockModel[10] = new ModelRendererTurbo(this, 72, 170, textureX, textureY); // Box 3
		defaultStockModel[11] = new ModelRendererTurbo(this, 72, 170, textureX, textureY); // Box 4

		defaultStockModel[0].addShapeBox(0F, 0F, 0F, 10, 3, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F); // Box 41
		defaultStockModel[0].setRotationPoint(-24F, -12F, -4.5F);

		defaultStockModel[1].addShapeBox(0F, 0F, 0F, 13, 4, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 1F, 0F); // Box 42
		defaultStockModel[1].setRotationPoint(-27F, -9F, -4.5F);

		defaultStockModel[2].addShapeBox(0F, 0F, 0F, 10, 1, 9, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, -1F, -1F, -1F, -1F, -1F, -1F, 0F, 0F, -1F); // Box 43
		defaultStockModel[2].setRotationPoint(-27F, -4F, -4.5F);

		defaultStockModel[3].addShapeBox(0F, 0F, 0F, 4, 5, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F); // Box 44
		defaultStockModel[3].setRotationPoint(-31F, -9F, -4.5F);

		defaultStockModel[4].addShapeBox(0F, 0F, 0F, 4, 1, 9, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, 0F, -1F); // Box 45
		defaultStockModel[4].setRotationPoint(-31F, -3F, -4.5F);

		defaultStockModel[5].addShapeBox(0F, 0F, 0F, 30, 16, 9, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, -10F, 0F, 0F, -10F, 0F, 0F, 0F, 0F); // Box 46
		defaultStockModel[5].setRotationPoint(-61F, -9F, -4.5F);

		defaultStockModel[6].addShapeBox(0F, 0F, 0F, 30, 1, 9, 0F, 0F, 0F, 0F, 0F, 10F, 0F, 0F, 10F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -10F, -1F, 0F, -10F, -1F, 0F, 0F, -1F); // Box 47
		defaultStockModel[6].setRotationPoint(-61F, 7F, -4.5F);

		defaultStockModel[7].addShapeBox(0F, 0F, 0F, 30, 3, 9, 0F, 0F, -4F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, -4F, 0F, 0F, 4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F); // Box 0
		defaultStockModel[7].setRotationPoint(-61F, -12F, -4.5F);

		defaultStockModel[8].addShapeBox(0F, 0F, 0F, 27, 1, 9, 0F, 0F, -4F, -1F, -2F, 0F, -1F, -2F, 0F, -1F, 0F, -4F, -1F, 0F, 4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F); // Box 1
		defaultStockModel[8].setRotationPoint(-61F, -13F, -4.5F);

		defaultStockModel[9].addShapeBox(0F, 0F, 0F, 2, 15, 9, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 2
		defaultStockModel[9].setRotationPoint(-63F, -8F, -4.5F);

		defaultStockModel[10].addShapeBox(0F, 0F, 0F, 2, 1, 9, 0F, -1F, 0F, -2F, 0F, 0F, -1F, 0F, 0F, -1F, -1F, 0F, -2F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 3
		defaultStockModel[10].setRotationPoint(-63F, -9F, -4.5F);

		defaultStockModel[11].addShapeBox(0F, 0F, 0F, 2, 1, 9, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, -1F, 0F, -2F, 0F, 0F, -1F, 0F, 0F, -1F, -1F, 0F, -2F); // Box 4
		defaultStockModel[11].setRotationPoint(-63F, 7F, -4.5F);


		defaultGripModel = new ModelRendererTurbo[3];
		defaultGripModel[0] = new ModelRendererTurbo(this, 140, 104, textureX, textureY); // Box 24
		defaultGripModel[1] = new ModelRendererTurbo(this, 140, 130, textureX, textureY); // Box 25
		defaultGripModel[2] = new ModelRendererTurbo(this, 140, 115, textureX, textureY); // Box 26

		defaultGripModel[0].addShapeBox(0F, 0F, 0F, 25, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F); // Box 24
		defaultGripModel[0].setRotationPoint(28F, -13.5F, -4F);

		defaultGripModel[1].addShapeBox(0F, 0F, 0F, 25, 1, 8, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, 0F, -1F); // Box 25
		defaultGripModel[1].setRotationPoint(28F, -11.5F, -4F);

		defaultGripModel[2].addBox(0F, 0F, 0F, 25, 6, 8, 0F); // Box 26
		defaultGripModel[2].setRotationPoint(28F, -19.5F, -4F);


		ammoModel = new ModelRendererTurbo[3];
		ammoModel[0] = new ModelRendererTurbo(this, 1, 71, textureX, textureY); // Box 22
		ammoModel[1] = new ModelRendererTurbo(this, 32, 72, textureX, textureY); // Box 23
		ammoModel[2] = new ModelRendererTurbo(this, 1, 113, textureX, textureY); // Box 5

		ammoModel[0].addBox(0F, 0F, 0F, 9, 35, 6, 0F); // Box 22
		ammoModel[0].setRotationPoint(12F, -17F, -3F);

		ammoModel[1].addBox(0F, 0F, 0F, 1, 35, 5, 0F); // Box 23
		ammoModel[1].setRotationPoint(11F, -17F, -2.5F);

		ammoModel[2].addBox(0F, 0F, 0F, 11, 4, 7, 0F); // Box 5
		ammoModel[2].setRotationPoint(10.5F, -3F, -3.5F);


		slideModel = new ModelRendererTurbo[2];
		slideModel[0] = new ModelRendererTurbo(this, 87, 44, textureX, textureY); // Box 10
		slideModel[1] = new ModelRendererTurbo(this, 87, 44, textureX, textureY); // Box 11

		slideModel[0].addShapeBox(0F, 0F, 0F, 2, 1, 5, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 10
		slideModel[0].setRotationPoint(-1F, -20F, -8F);

		slideModel[1].addShapeBox(0F, 0F, 0F, 2, 1, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 11
		slideModel[1].setRotationPoint(-1F, -19F, -8F);

		barrelAttachPoint = new Vector3f(65F /16F, 19.5F /16F, 0F /16F);
		stockAttachPoint = new Vector3f(-19F /16F, 12F /16F, 0F /16F);
		scopeAttachPoint = new Vector3f(19F /16F, 23F /16F, 0F /16F);
		gripAttachPoint = new Vector3f(28 /16F, 17F /16F, 0F /16F);

		gunSlideDistance = 0.8F;
		animationType = EnumAnimationType.BOTTOM_CLIP;

		translateAll(0F, 0F, 0F);


		flipAll();
	}
}