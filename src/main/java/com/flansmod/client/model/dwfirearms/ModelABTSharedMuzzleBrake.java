package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.ModelAttachment;
import com.flansmod.client.tmt.ModelRendererTurbo;

import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelABTSharedMuzzleBrake extends ModelAttachment
{
	int textureX = 512;
	int textureY = 256;

	public ModelABTSharedMuzzleBrake()
	{
		attachmentModel = new ModelRendererTurbo[10];
		attachmentModel[0] = new ModelRendererTurbo(this, 1, 33, textureX, textureY); // muzzlePart1
		attachmentModel[1] = new ModelRendererTurbo(this, 1, 40, textureX, textureY); // muzzlePart3
		attachmentModel[2] = new ModelRendererTurbo(this, 46, 40, textureX, textureY); // muzzlePart3-3
		attachmentModel[3] = new ModelRendererTurbo(this, 22, 40, textureX, textureY); // muzzlePart3-2
		attachmentModel[4] = new ModelRendererTurbo(this, 46, 33, textureX, textureY); // muzzlePart2
		attachmentModel[5] = new ModelRendererTurbo(this, 33, 41, textureX, textureY); // muzzlePart4
		attachmentModel[6] = new ModelRendererTurbo(this, 33, 46, textureX, textureY); // muzzlePart4-2
		attachmentModel[7] = new ModelRendererTurbo(this, 67, 40, textureX, textureY); // muzzlePart4-3
		attachmentModel[8] = new ModelRendererTurbo(this, 33, 41, textureX, textureY); // muzzlePart4-4
		attachmentModel[9] = new ModelRendererTurbo(this, 33, 46, textureX, textureY); // muzzlePart4-3

		attachmentModel[0].addBox(0F, 2F, -2F, 15, 2, 4, 0F); // muzzlePart1
		attachmentModel[0].setRotationPoint(0F, 0F, 0F);

		attachmentModel[1].addShapeBox(0F, -4F, 2F, 8, 8, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F); // muzzlePart3
		attachmentModel[1].setRotationPoint(0F, 0F, 0F);

		attachmentModel[2].addShapeBox(0F, -4F, -4F, 8, 8, 2, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // muzzlePart3-3
		attachmentModel[2].setRotationPoint(0F, 0F, 0F);

		attachmentModel[3].addShapeBox(12F, -4F, 2F, 3, 8, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F); // muzzlePart3-2
		attachmentModel[3].setRotationPoint(0F, 0F, 0F);

		attachmentModel[4].addBox(0F, -4F, -2F, 15, 2, 4, 0F); // muzzlePart2
		attachmentModel[4].setRotationPoint(0F, 0F, 0F);

		attachmentModel[5].addShapeBox(8F, -4F, 2F, 4, 2, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // muzzlePart4
		attachmentModel[5].setRotationPoint(0F, 0F, 0F);

		attachmentModel[6].addShapeBox(8F, 2F, 2F, 4, 2, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F); // muzzlePart4-2
		attachmentModel[6].setRotationPoint(0F, 0F, 0F);

		attachmentModel[7].addShapeBox(12F, -4F, -4F, 3, 8, 2, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // muzzlePart4-3
		attachmentModel[7].setRotationPoint(0F, 0F, 0F);

		attachmentModel[8].addShapeBox(8F, -4F, -4F, 4, 2, 2, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // muzzlePart4-4
		attachmentModel[8].setRotationPoint(0F, 0F, 0F);

		attachmentModel[9].addShapeBox(8F, 2F, -4F, 4, 2, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // muzzlePart4-3
		attachmentModel[9].setRotationPoint(0F, 0F, 0F);

		renderOffset = 0F;

		flipAll();
	}
}