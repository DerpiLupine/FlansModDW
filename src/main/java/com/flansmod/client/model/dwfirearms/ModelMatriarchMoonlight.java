package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelMatriarchMoonlight extends ModelGun
{
	int textureX = 512;
	int textureY = 256;

	public ModelMatriarchMoonlight()
	{
		gunModel = new ModelRendererTurbo[62];
		gunModel[0] = new ModelRendererTurbo(this, 167, 86, textureX, textureY); // barrelFront
		gunModel[1] = new ModelRendererTurbo(this, 56, 55, textureX, textureY); // barrelFront2
		gunModel[2] = new ModelRendererTurbo(this, 56, 26, textureX, textureY); // barrelRail
		gunModel[3] = new ModelRendererTurbo(this, 81, 96, textureX, textureY); // body2
		gunModel[4] = new ModelRendererTurbo(this, 123, 59, textureX, textureY); // body3
		gunModel[5] = new ModelRendererTurbo(this, 83, 90, textureX, textureY); // body4
		gunModel[6] = new ModelRendererTurbo(this, 74, 90, textureX, textureY); // body4-2
		gunModel[7] = new ModelRendererTurbo(this, 83, 83, textureX, textureY); // body5
		gunModel[8] = new ModelRendererTurbo(this, 76, 83, textureX, textureY); // body5-2
		gunModel[9] = new ModelRendererTurbo(this, 56, 47, textureX, textureY); // body7
		gunModel[10] = new ModelRendererTurbo(this, 146, 61, textureX, textureY); // body9
		gunModel[11] = new ModelRendererTurbo(this, 116, 85, textureX, textureY); // grip1
		gunModel[12] = new ModelRendererTurbo(this, 1, 120, textureX, textureY); // grip2
		gunModel[13] = new ModelRendererTurbo(this, 1, 95, textureX, textureY); // grip3
		gunModel[14] = new ModelRendererTurbo(this, 1, 106, textureX, textureY); // grip4
		gunModel[15] = new ModelRendererTurbo(this, 1, 79, textureX, textureY); // grip5
		gunModel[16] = new ModelRendererTurbo(this, 107, 90, textureX, textureY); // hammer1
		gunModel[17] = new ModelRendererTurbo(this, 92, 90, textureX, textureY); // hammer2
		gunModel[18] = new ModelRendererTurbo(this, 24, 12, textureX, textureY); // ironSight1
		gunModel[19] = new ModelRendererTurbo(this, 35, 12, textureX, textureY); // ironSight1-2
		gunModel[20] = new ModelRendererTurbo(this, 36, 6, textureX, textureY); // ironSight2
		gunModel[21] = new ModelRendererTurbo(this, 56, 1, textureX, textureY); // mainBarrelTop
		gunModel[22] = new ModelRendererTurbo(this, 36, 3, textureX, textureY); // Box 3
		gunModel[23] = new ModelRendererTurbo(this, 1, 10, textureX, textureY); // Box 4
		gunModel[24] = new ModelRendererTurbo(this, 192, 86, textureX, textureY); // Box 1
		gunModel[25] = new ModelRendererTurbo(this, 56, 33, textureX, textureY); // Box 6
		gunModel[26] = new ModelRendererTurbo(this, 171, 70, textureX, textureY); // Box 7
		gunModel[27] = new ModelRendererTurbo(this, 56, 15, textureX, textureY); // Box 8
		gunModel[28] = new ModelRendererTurbo(this, 165, 40, textureX, textureY); // Box 9
		gunModel[29] = new ModelRendererTurbo(this, 1, 70, textureX, textureY); // Box 0
		gunModel[30] = new ModelRendererTurbo(this, 134, 85, textureX, textureY); // Box 1
		gunModel[31] = new ModelRendererTurbo(this, 102, 61, textureX, textureY); // Box 2
		gunModel[32] = new ModelRendererTurbo(this, 81, 61, textureX, textureY); // Box 3
		gunModel[33] = new ModelRendererTurbo(this, 132, 70, textureX, textureY); // Box 4
		gunModel[34] = new ModelRendererTurbo(this, 165, 51, textureX, textureY); // Box 5
		gunModel[35] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 55
		gunModel[36] = new ModelRendererTurbo(this, 122, 50, textureX, textureY); // Box 56
		gunModel[37] = new ModelRendererTurbo(this, 122, 40, textureX, textureY); // Box 57
		gunModel[38] = new ModelRendererTurbo(this, 57, 40, textureX, textureY); // Box 58
		gunModel[39] = new ModelRendererTurbo(this, 180, 31, textureX, textureY); // Box 59
		gunModel[40] = new ModelRendererTurbo(this, 180, 24, textureX, textureY); // Box 63
		gunModel[41] = new ModelRendererTurbo(this, 34, 79, textureX, textureY); // Box 64
		gunModel[42] = new ModelRendererTurbo(this, 34, 70, textureX, textureY); // Box 65
		gunModel[43] = new ModelRendererTurbo(this, 34, 119, textureX, textureY); // Box 66
		gunModel[44] = new ModelRendererTurbo(this, 34, 95, textureX, textureY); // Box 67
		gunModel[45] = new ModelRendererTurbo(this, 53, 95, textureX, textureY); // Box 68
		gunModel[46] = new ModelRendererTurbo(this, 32, 106, textureX, textureY); // Box 69
		gunModel[47] = new ModelRendererTurbo(this, 53, 79, textureX, textureY); // Box 70
		gunModel[48] = new ModelRendererTurbo(this, 53, 70, textureX, textureY); // Box 71
		gunModel[49] = new ModelRendererTurbo(this, 49, 107, textureX, textureY); // Box 72
		gunModel[50] = new ModelRendererTurbo(this, 40, 24, textureX, textureY); // Box 0
		gunModel[51] = new ModelRendererTurbo(this, 40, 24, textureX, textureY); // Box 1
		gunModel[52] = new ModelRendererTurbo(this, 160, 6, textureX, textureY); // Box 0
		gunModel[53] = new ModelRendererTurbo(this, 160, 6, textureX, textureY); // Box 1
		gunModel[54] = new ModelRendererTurbo(this, 175, 6, textureX, textureY); // Box 2
		gunModel[55] = new ModelRendererTurbo(this, 118, 4, textureX, textureY); // Box 3
		gunModel[56] = new ModelRendererTurbo(this, 81, 70, textureX, textureY); // Box 4
		gunModel[57] = new ModelRendererTurbo(this, 141, 15, textureX, textureY); // Box 5
		gunModel[58] = new ModelRendererTurbo(this, 162, 16, textureX, textureY); // Box 6
		gunModel[59] = new ModelRendererTurbo(this, 139, 5, textureX, textureY); // Box 7
		gunModel[60] = new ModelRendererTurbo(this, 100, 42, textureX, textureY); // Box 9
		gunModel[61] = new ModelRendererTurbo(this, 121, 41, textureX, textureY); // Box 10

		gunModel[0].addShapeBox(0F, 0F, 0F, 4, 1, 8, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // barrelFront
		gunModel[0].setRotationPoint(19F, -21F, -4F);

		gunModel[1].addBox(0F, 0F, 0F, 4, 6, 8, 0F); // barrelFront2
		gunModel[1].setRotationPoint(19F, -20F, -4F);

		gunModel[2].addShapeBox(0F, 0F, 0F, 51, 1, 5, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // barrelRail
		gunModel[2].setRotationPoint(6F, -25F, -2.5F);

		gunModel[3].addBox(0F, 0F, 0F, 7, 14, 6, 0F); // body2
		gunModel[3].setRotationPoint(-2F, -23F, -3F);

		gunModel[4].addBox(0F, 0F, 0F, 5, 4, 6, 0F); // body3
		gunModel[4].setRotationPoint(-7F, -16F, -3F);

		gunModel[5].addShapeBox(0F, 0F, 0F, 2, 3, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F); // body4
		gunModel[5].setRotationPoint(-4F, -19F, 1F);

		gunModel[6].addShapeBox(0F, 0F, 0F, 2, 3, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F); // body4-2
		gunModel[6].setRotationPoint(-4F, -19F, -3F);

		gunModel[7].addShapeBox(0F, 0F, 0F, 1, 4, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F); // body5
		gunModel[7].setRotationPoint(-3F, -23F, 1F);

		gunModel[8].addShapeBox(0F, 0F, 0F, 1, 4, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F); // body5-2
		gunModel[8].setRotationPoint(-3F, -23F, -3F);

		gunModel[9].addBox(0F, 0F, 0F, 26, 1, 6, 0F); // body7
		gunModel[9].setRotationPoint(-3F, -24F, -3F);

		gunModel[10].addShapeBox(0F, 0F, 0F, 18, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F); // body9
		gunModel[10].setRotationPoint(5F, -11F, -3F);

		gunModel[11].addShapeBox(0F, 0F, 0F, 1, 3, 7, 0F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F); // grip1
		gunModel[11].setRotationPoint(-7.5F, -15F, -3.5F);

		gunModel[12].addShapeBox(0F, 0F, 0F, 9, 4, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F); // grip2
		gunModel[12].setRotationPoint(-6.5F, -12F, -3.5F);

		gunModel[13].addShapeBox(0F, 0F, 0F, 9, 3, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 2F, 0F, 0F); // grip3
		gunModel[13].setRotationPoint(-10.5F, -8F, -3.5F);

		gunModel[14].addShapeBox(0F, 0F, 0F, 8, 5, 7, 0F, -2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // grip4
		gunModel[14].setRotationPoint(-14.5F, -5F, -3.5F);

		gunModel[15].addShapeBox(0F, 0F, 0F, 8, 8, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F); // grip5
		gunModel[15].setRotationPoint(-14.5F, 0F, -3.5F);

		gunModel[16].addBox(0F, 0F, 0F, 2, 3, 2, 0F); // hammer1
		gunModel[16].setRotationPoint(-4F, -19F, -1F);

		gunModel[17].addShapeBox(0F, 0F, 0F, 5, 3, 2, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F, -3F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, -3F, 0F); // hammer2
		gunModel[17].setRotationPoint(-7F, -20F, -1F);

		gunModel[18].addShapeBox(0F, 0F, 0F, 3, 2, 2, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // ironSight1
		gunModel[18].setRotationPoint(-1.9F, -29F, 0.5F);

		gunModel[19].addShapeBox(0F, 0F, 0F, 3, 2, 2, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // ironSight1-2
		gunModel[19].setRotationPoint(-1.9F, -29F, -2.5F);

		gunModel[20].addShapeBox(0F, 0F, 0F, 6, 2, 1, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // ironSight2
		gunModel[20].setRotationPoint(51F, -27F, -0.5F);

		gunModel[21].addShapeBox(0F, 0F, 0F, 22, 5, 8, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F); // mainBarrelTop
		gunModel[21].setRotationPoint(23F, -23F, -4F);

		gunModel[22].addShapeBox(0F, 0F, 0F, 3, 1, 1, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 3
		gunModel[22].setRotationPoint(54F, -28F, -0.5F);

		gunModel[23].addBox(0F, 0F, 0F, 6, 1, 5, 0F); // Box 4
		gunModel[23].setRotationPoint(-1.9F, -27F, -2.5F);

		gunModel[24].addShapeBox(0F, 0F, 0F, 4, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 1
		gunModel[24].setRotationPoint(19F, -14F, -4F);

		gunModel[25].addBox(0F, 0F, 0F, 34, 1, 5, 0F); // Box 6
		gunModel[25].setRotationPoint(23F, -24F, -2.5F);

		gunModel[26].addShapeBox(0F, 0F, 0F, 1, 5, 8, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F); // Box 7
		gunModel[26].setRotationPoint(45F, -23F, -4F);

		gunModel[27].addBox(0F, 0F, 0F, 34, 2, 8, 0F); // Box 8
		gunModel[27].setRotationPoint(23F, -18F, -4F);

		gunModel[28].addBox(0F, 0F, 0F, 18, 4, 6, 0F); // Box 9
		gunModel[28].setRotationPoint(23F, -16F, -3F);

		gunModel[29].addShapeBox(0F, 0F, 0F, 8, 1, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 0
		gunModel[29].setRotationPoint(-15.5F, 8F, -3.5F);

		gunModel[30].addShapeBox(0F, 0F, 0F, 9, 3, 7, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 1
		gunModel[30].setRotationPoint(-6.5F, -15F, -3.5F);

		gunModel[31].addBox(0F, 0F, 0F, 4, 2, 6, 0F); // Box 2
		gunModel[31].setRotationPoint(19F, -23F, -3F);

		gunModel[32].addBox(0F, 0F, 0F, 4, 2, 6, 0F); // Box 3
		gunModel[32].setRotationPoint(19F, -13F, -3F);

		gunModel[33].addShapeBox(0F, 0F, 0F, 11, 5, 8, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 4
		gunModel[33].setRotationPoint(46F, -23F, -4F);

		gunModel[34].addShapeBox(0F, 0F, 0F, 18, 1, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 5
		gunModel[34].setRotationPoint(23F, -12F, -3F);

		gunModel[35].addShapeBox(0F, 0F, 0F, 11, 2, 6, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 55
		gunModel[35].setRotationPoint(-3F, -26F, -3F);

		gunModel[36].addShapeBox(0F, 0F, 0F, 15, 1, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 56
		gunModel[36].setRotationPoint(42F, -12F, -3F);

		gunModel[37].addBox(0F, 0F, 0F, 15, 3, 6, 0F); // Box 57
		gunModel[37].setRotationPoint(42F, -15F, -3F);

		gunModel[38].addBox(0F, 0F, 0F, 16, 1, 5, 0F); // Box 58
		gunModel[38].setRotationPoint(41F, -16F, -2.5F);

		gunModel[39].addBox(0F, 0F, 0F, 1, 3, 5, 0F); // Box 59
		gunModel[39].setRotationPoint(41F, -15F, -2.5F);

		gunModel[40].addShapeBox(0F, 0F, 0F, 1, 1, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 63
		gunModel[40].setRotationPoint(41F, -12F, -2.5F);

		gunModel[41].addShapeBox(0F, 0F, 0F, 2, 8, 7, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, -1.5F, -1F, 0F, -1.5F, 1F, 0F, 0F); // Box 64
		gunModel[41].setRotationPoint(-6.5F, 0F, -3.5F);

		gunModel[42].addShapeBox(0F, 0F, 0F, 2, 1, 7, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -1F); // Box 65
		gunModel[42].setRotationPoint(-7.5F, 8F, -3.5F);

		gunModel[43].addShapeBox(0F, 0F, 0F, 2, 5, 7, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, -1.5F, -2F, 0F, -1.5F, 2F, 0F, 0F); // Box 66
		gunModel[43].setRotationPoint(-4.5F, -5F, -3.5F);

		gunModel[44].addShapeBox(0F, 0F, 0F, 2, 3, 7, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, -1.5F, -3F, 0F, -1.5F, 3F, 0F, 0F); // Box 67
		gunModel[44].setRotationPoint(-1.5F, -8F, -3.5F);

		gunModel[45].addShapeBox(0F, 0F, 0F, 1, 3, 7, 0F, -2F, 0F, -0.5F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F); // Box 68
		gunModel[45].setRotationPoint(-13.5F, -8F, -3.5F);

		gunModel[46].addShapeBox(0F, 0F, 0F, 1, 5, 7, 0F, -2F, 0F, -0.5F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F); // Box 69
		gunModel[46].setRotationPoint(-15.5F, -5F, -3.5F);

		gunModel[47].addShapeBox(0F, 0F, 0F, 1, 8, 7, 0F, -1F, 0F, -0.5F, 1F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F); // Box 70
		gunModel[47].setRotationPoint(-16.5F, 0F, -3.5F);

		gunModel[48].addShapeBox(0F, 0F, 0F, 1, 1, 7, 0F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -1.5F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1.5F); // Box 71
		gunModel[48].setRotationPoint(-16.5F, 8F, -3.5F);

		gunModel[49].addShapeBox(0F, 0F, 0F, 1, 4, 7, 0F, -4F, 0F, -0.5F, 4F, 0F, 0F, 4F, 0F, 0F, -4F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F); // Box 72
		gunModel[49].setRotationPoint(-11.5F, -12F, -3.5F);

		gunModel[50].addShapeBox(0F, 0F, 0F, 2, 8, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, -0.5F, 0F, -1.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, -0.5F, 0F, -1.5F, -0.5F); // Box 0
		gunModel[50].setRotationPoint(2.5F, -21F, 2.5F);

		gunModel[51].addShapeBox(0F, 0F, 0F, 2, 8, 3, 0F, 0F, -1.5F, -0.5F, 0F, -1.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, -0.5F, 0F, -1.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 1
		gunModel[51].setRotationPoint(2.5F, -21F, -5.5F);

		gunModel[52].addShapeBox(0F, 0F, 0F, 1, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 0
		gunModel[52].setRotationPoint(57F, -23F, -3F);

		gunModel[53].addShapeBox(0F, 0F, 0F, 1, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 1
		gunModel[53].setRotationPoint(57F, -19F, -3F);

		gunModel[54].addBox(0F, 0F, 0F, 1, 2, 6, 0F); // Box 2
		gunModel[54].setRotationPoint(57F, -21F, -3F);

		gunModel[55].addBox(0F, 0F, 0F, 2, 2, 8, 0F); // Box 3
		gunModel[55].setRotationPoint(23F, -13F, -4F);

		gunModel[56].addBox(0F, 0F, 0F, 17, 3, 8, 0F); // Box 4
		gunModel[56].setRotationPoint(23F, -16F, -4F);

		gunModel[57].addBox(0F, 0F, 0F, 2, 2, 8, 0F); // Box 5
		gunModel[57].setRotationPoint(38F, -13F, -4F);

		gunModel[58].addShapeBox(0F, 0F, 0F, 2, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 6
		gunModel[58].setRotationPoint(38F, -11F, -4F);

		gunModel[59].addShapeBox(0F, 0F, 0F, 2, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 7
		gunModel[59].setRotationPoint(23F, -11F, -4F);

		gunModel[60].addBox(0F, 0F, 0F, 9, 3, 1, 0F); // Box 9
		gunModel[60].setRotationPoint(45F, -15F, -4F);

		gunModel[61].addBox(0F, 0F, 0F, 1, 2, 2, 0F); // Box 10
		gunModel[61].setRotationPoint(56.5F, -15F, -1F);


		ammoModel = new ModelRendererTurbo[18];
		ammoModel[0] = new ModelRendererTurbo(this, 1, 55, textureX, textureY); // bullet1
		ammoModel[1] = new ModelRendererTurbo(this, 1, 55, textureX, textureY); // bullet1-2
		ammoModel[2] = new ModelRendererTurbo(this, 1, 55, textureX, textureY); // bullet1-3
		ammoModel[3] = new ModelRendererTurbo(this, 26, 55, textureX, textureY); // bullet2
		ammoModel[4] = new ModelRendererTurbo(this, 26, 55, textureX, textureY); // bullet2-2
		ammoModel[5] = new ModelRendererTurbo(this, 26, 55, textureX, textureY); // bullet2-3
		ammoModel[6] = new ModelRendererTurbo(this, 1, 60, textureX, textureY); // bullet3
		ammoModel[7] = new ModelRendererTurbo(this, 1, 60, textureX, textureY); // bullet3-2
		ammoModel[8] = new ModelRendererTurbo(this, 1, 60, textureX, textureY); // bullet3-3
		ammoModel[9] = new ModelRendererTurbo(this, 26, 60, textureX, textureY); // bullet4
		ammoModel[10] = new ModelRendererTurbo(this, 26, 60, textureX, textureY); // bullet4-2
		ammoModel[11] = new ModelRendererTurbo(this, 26, 60, textureX, textureY); // bullet4-3
		ammoModel[12] = new ModelRendererTurbo(this, 1, 65, textureX, textureY); // bullet5
		ammoModel[13] = new ModelRendererTurbo(this, 1, 65, textureX, textureY); // bullet5-2
		ammoModel[14] = new ModelRendererTurbo(this, 1, 65, textureX, textureY); // bullet5-3
		ammoModel[15] = new ModelRendererTurbo(this, 26, 65, textureX, textureY); // bullet6
		ammoModel[16] = new ModelRendererTurbo(this, 26, 65, textureX, textureY); // bullet6-2
		ammoModel[17] = new ModelRendererTurbo(this, 26, 65, textureX, textureY); // bullet6-3

		ammoModel[0].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // bullet1
		ammoModel[0].setRotationPoint(5.3F, -22.5F, -1.5F);

		ammoModel[1].addBox(0F, 0F, 0F, 9, 1, 3, 0F); // bullet1-2
		ammoModel[1].setRotationPoint(5.3F, -21.5F, -1.5F);

		ammoModel[2].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // bullet1-3
		ammoModel[2].setRotationPoint(5.3F, -20.5F, -1.5F);

		ammoModel[3].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // bullet2
		ammoModel[3].setRotationPoint(5.3F, -20.5F, -5F);

		ammoModel[4].addBox(0F, 0F, 0F, 9, 1, 3, 0F); // bullet2-2
		ammoModel[4].setRotationPoint(5.3F, -19.5F, -5F);

		ammoModel[5].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // bullet2-3
		ammoModel[5].setRotationPoint(5.3F, -18.5F, -5F);

		ammoModel[6].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // bullet3
		ammoModel[6].setRotationPoint(5.3F, -16.5F, -5F);

		ammoModel[7].addBox(0F, 0F, 0F, 9, 1, 3, 0F); // bullet3-2
		ammoModel[7].setRotationPoint(5.3F, -15.5F, -5F);

		ammoModel[8].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // bullet3-3
		ammoModel[8].setRotationPoint(5.3F, -14.5F, -5F);

		ammoModel[9].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // bullet4
		ammoModel[9].setRotationPoint(5.3F, -14.5F, -1.5F);

		ammoModel[10].addBox(0F, 0F, 0F, 9, 1, 3, 0F); // bullet4-2
		ammoModel[10].setRotationPoint(5.3F, -13.5F, -1.5F);

		ammoModel[11].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // bullet4-3
		ammoModel[11].setRotationPoint(5.3F, -12.5F, -1.5F);

		ammoModel[12].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // bullet5
		ammoModel[12].setRotationPoint(5.3F, -16.5F, 2F);

		ammoModel[13].addBox(0F, 0F, 0F, 9, 1, 3, 0F); // bullet5-2
		ammoModel[13].setRotationPoint(5.3F, -15.5F, 2F);

		ammoModel[14].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // bullet5-3
		ammoModel[14].setRotationPoint(5.3F, -14.5F, 2F);

		ammoModel[15].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // bullet6
		ammoModel[15].setRotationPoint(5.3F, -20.5F, 2F);

		ammoModel[16].addBox(0F, 0F, 0F, 9, 1, 3, 0F); // bullet6-2
		ammoModel[16].setRotationPoint(5.3F, -19.5F, 2F);

		ammoModel[17].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // bullet6-3
		ammoModel[17].setRotationPoint(5.3F, -18.5F, 2F);


		revolverBarrelModel = new ModelRendererTurbo[2];
		revolverBarrelModel[0] = new ModelRendererTurbo(this, 1, 17, textureX, textureY); // canister1
		revolverBarrelModel[1] = new ModelRendererTurbo(this, 1, 36, textureX, textureY); // Box 6

		revolverBarrelModel[0].addShapeBox(0F, 0F, 0F, 13, 12, 6, 0F, 0F, -3F, -0.5F, 0F, -3F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, -0.5F, 0F, -3F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // canister1
		revolverBarrelModel[0].setRotationPoint(5.5F, -23F, -6F);

		revolverBarrelModel[1].addShapeBox(0F, 0F, 0F, 13, 12, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, -0.5F, 0F, -3F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, -0.5F, 0F, -3F, -0.5F); // Box 6
		revolverBarrelModel[1].setRotationPoint(5.5F, -23F, 0F);

		barrelAttachPoint = new Vector3f(0F /16F, 0F /16F, 0F /16F);
		stockAttachPoint = new Vector3f(0F /16F, 0F /16F, 0F /16F);
		scopeAttachPoint = new Vector3f(0F /16F, 0F /16F, 0F /16F);
		gripAttachPoint = new Vector3f(0 /16F, 0F /16F, 0F /16F);

		gunSlideDistance = 0F;
		animationType = EnumAnimationType.REVOLVER;

		revolverFlipAngle = -20F;


		translateAll(0F, 0F, 0F);


		flipAll();
	}
}