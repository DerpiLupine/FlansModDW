package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.ModelAttachment;
import com.flansmod.client.tmt.ModelRendererTurbo;

import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelLPMWoodenStock extends ModelAttachment
{
	int textureX = 512;
	int textureY = 256;

	public ModelLPMWoodenStock()
	{
		attachmentModel = new ModelRendererTurbo[6];
		attachmentModel[0] = new ModelRendererTurbo(this, 1, 91, textureX, textureY); // stock2
		attachmentModel[1] = new ModelRendererTurbo(this, 82, 91, textureX, textureY); // stock3
		attachmentModel[2] = new ModelRendererTurbo(this, 1, 71, textureX, textureY); // stockPipe1
		attachmentModel[3] = new ModelRendererTurbo(this, 1, 81, textureX, textureY); // Box 0
		attachmentModel[4] = new ModelRendererTurbo(this, 82, 81, textureX, textureY); // Box 1
		attachmentModel[5] = new ModelRendererTurbo(this, 70, 71, textureX, textureY); // Box 2

		attachmentModel[0].addShapeBox(-33F, -2F, -3.5F, 33, 13, 7, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, -8F, 0F, 0F, -8F, 0F, 0F, 2F, 0F); // stock2
		attachmentModel[0].setRotationPoint(0F, 0F, 0F);

		attachmentModel[1].addBox(-35F, -2F, -3.5F, 2, 15, 7, 0F); // stock3
		attachmentModel[1].setRotationPoint(0F, 0F, 0F);

		attachmentModel[2].addShapeBox(-35F, -4F, -3.5F, 27, 2, 7, 0F, -1F, 0F, -1.5F, 0F, 0.8F, -1.5F, 0F, 0.8F, -1.5F, -1F, 0F, -1.5F, 0F, 0F, 0F, 0F, -0.8F, 0F, 0F, -0.8F, 0F, 0F, 0F, 0F); // stockPipe1
		attachmentModel[2].setRotationPoint(0F, 0F, 0F);

		attachmentModel[3].addShapeBox(-33F, 3F, -3.5F, 33, 2, 7, 0F, 0F, -10F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -10F, 0F, 0F, 10F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 10F, -1.5F); // Box 0
		attachmentModel[3].setRotationPoint(0F, 0F, 0F);

		attachmentModel[4].addShapeBox(-35F, 13F, -3.5F, 2, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, -1F, 0F, -1.5F); // Box 1
		attachmentModel[4].setRotationPoint(0F, 0F, 0F);

		attachmentModel[5].addShapeBox(-8F, -4.5F, -3.5F, 8, 2, 7, 0F, 0F, -0.3F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, -0.3F, -1.5F, 0F, -0.3F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.3F, 0F); // Box 2
		attachmentModel[5].setRotationPoint(0F, 0F, 0F);

		renderOffset = 0F;

		flipAll();
	}
}