package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelPWR57 extends ModelGun
{
	int textureX = 512;
	int textureY = 256;

	public ModelPWR57()
	{
		gunModel = new ModelRendererTurbo[65];
		gunModel[0] = new ModelRendererTurbo(this, 1, 16, textureX, textureY); // Box 32
		gunModel[1] = new ModelRendererTurbo(this, 44, 16, textureX, textureY); // Box 33
		gunModel[2] = new ModelRendererTurbo(this, 1, 28, textureX, textureY); // Box 34
		gunModel[3] = new ModelRendererTurbo(this, 1, 40, textureX, textureY); // Box 35
		gunModel[4] = new ModelRendererTurbo(this, 1, 54, textureX, textureY); // Box 36
		gunModel[5] = new ModelRendererTurbo(this, 1, 70, textureX, textureY); // Box 38
		gunModel[6] = new ModelRendererTurbo(this, 38, 27, textureX, textureY); // Box 39
		gunModel[7] = new ModelRendererTurbo(this, 38, 40, textureX, textureY); // Box 40
		gunModel[8] = new ModelRendererTurbo(this, 38, 54, textureX, textureY); // Box 41
		gunModel[9] = new ModelRendererTurbo(this, 38, 70, textureX, textureY); // Box 42
		gunModel[10] = new ModelRendererTurbo(this, 117, 28, textureX, textureY); // Box 0
		gunModel[11] = new ModelRendererTurbo(this, 1, 95, textureX, textureY); // Box 1
		gunModel[12] = new ModelRendererTurbo(this, 1, 108, textureX, textureY); // Box 2
		gunModel[13] = new ModelRendererTurbo(this, 181, 56, textureX, textureY); // Box 3
		gunModel[14] = new ModelRendererTurbo(this, 190, 31, textureX, textureY); // Box 4
		gunModel[15] = new ModelRendererTurbo(this, 117, 47, textureX, textureY); // Box 5
		gunModel[16] = new ModelRendererTurbo(this, 1, 81, textureX, textureY); // Box 7
		gunModel[17] = new ModelRendererTurbo(this, 48, 81, textureX, textureY); // Box 8
		gunModel[18] = new ModelRendererTurbo(this, 117, 58, textureX, textureY); // Box 9
		gunModel[19] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 10
		gunModel[20] = new ModelRendererTurbo(this, 117, 20, textureX, textureY); // Box 11
		gunModel[21] = new ModelRendererTurbo(this, 176, 20, textureX, textureY); // Box 13
		gunModel[22] = new ModelRendererTurbo(this, 158, 58, textureX, textureY); // Box 14
		gunModel[23] = new ModelRendererTurbo(this, 158, 47, textureX, textureY); // Box 15
		gunModel[24] = new ModelRendererTurbo(this, 117, 12, textureX, textureY); // Box 16
		gunModel[25] = new ModelRendererTurbo(this, 57, 93, textureX, textureY); // Box 17
		gunModel[26] = new ModelRendererTurbo(this, 48, 93, textureX, textureY); // Box 18
		gunModel[27] = new ModelRendererTurbo(this, 117, 68, textureX, textureY); // Box 19
		gunModel[28] = new ModelRendererTurbo(this, 288, 12, textureX, textureY); // Box 21
		gunModel[29] = new ModelRendererTurbo(this, 269, 12, textureX, textureY); // Box 22
		gunModel[30] = new ModelRendererTurbo(this, 269, 12, textureX, textureY); // Box 23
		gunModel[31] = new ModelRendererTurbo(this, 254, 12, textureX, textureY); // Box 24
		gunModel[32] = new ModelRendererTurbo(this, 254, 12, textureX, textureY); // Box 25
		gunModel[33] = new ModelRendererTurbo(this, 254, 12, textureX, textureY); // Box 26
		gunModel[34] = new ModelRendererTurbo(this, 48, 105, textureX, textureY); // Box 30
		gunModel[35] = new ModelRendererTurbo(this, 57, 105, textureX, textureY); // Box 31
		gunModel[36] = new ModelRendererTurbo(this, 82, 2, textureX, textureY); // Box 33
		gunModel[37] = new ModelRendererTurbo(this, 82, 2, textureX, textureY); // Box 34
		gunModel[38] = new ModelRendererTurbo(this, 82, 2, textureX, textureY); // Box 35
		gunModel[39] = new ModelRendererTurbo(this, 82, 2, textureX, textureY); // Box 36
		gunModel[40] = new ModelRendererTurbo(this, 82, 2, textureX, textureY); // Box 37
		gunModel[41] = new ModelRendererTurbo(this, 82, 2, textureX, textureY); // Box 38
		gunModel[42] = new ModelRendererTurbo(this, 72, 40, textureX, textureY); // Box 39
		gunModel[43] = new ModelRendererTurbo(this, 72, 28, textureX, textureY); // Box 40
		gunModel[44] = new ModelRendererTurbo(this, 93, 16, textureX, textureY); // Box 41
		gunModel[45] = new ModelRendererTurbo(this, 93, 16, textureX, textureY); // Box 42
		gunModel[46] = new ModelRendererTurbo(this, 72, 16, textureX, textureY); // Box 43
		gunModel[47] = new ModelRendererTurbo(this, 72, 75, textureX, textureY); // Box 44
		gunModel[48] = new ModelRendererTurbo(this, 72, 65, textureX, textureY); // Box 45
		gunModel[49] = new ModelRendererTurbo(this, 72, 54, textureX, textureY); // Box 46
		gunModel[50] = new ModelRendererTurbo(this, 117, 1, textureX, textureY); // Box 47
		gunModel[51] = new ModelRendererTurbo(this, 154, 79, textureX, textureY); // Box 0
		gunModel[52] = new ModelRendererTurbo(this, 117, 78, textureX, textureY); // Box 1
		gunModel[53] = new ModelRendererTurbo(this, 146, 92, textureX, textureY); // Box 6
		gunModel[54] = new ModelRendererTurbo(this, 117, 92, textureX, textureY); // Box 7
		gunModel[55] = new ModelRendererTurbo(this, 48, 111, textureX, textureY); // Box 8
		gunModel[56] = new ModelRendererTurbo(this, 48, 120, textureX, textureY); // Box 11
		gunModel[57] = new ModelRendererTurbo(this, 80, 125, textureX, textureY); // Box 13
		gunModel[58] = new ModelRendererTurbo(this, 80, 121, textureX, textureY); // Box 14
		gunModel[59] = new ModelRendererTurbo(this, 200, 57, textureX, textureY); // Box 16
		gunModel[60] = new ModelRendererTurbo(this, 200, 64, textureX, textureY); // Box 17
		gunModel[61] = new ModelRendererTurbo(this, 228, 53, textureX, textureY); // Box 18
		gunModel[62] = new ModelRendererTurbo(this, 228, 53, textureX, textureY); // Box 19
		gunModel[63] = new ModelRendererTurbo(this, 228, 53, textureX, textureY); // Box 20
		gunModel[64] = new ModelRendererTurbo(this, 228, 53, textureX, textureY); // Box 21

		gunModel[0].addShapeBox(0F, 0F, 0F, 13, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F); // Box 32
		gunModel[0].setRotationPoint(-11F, -4F, -4F);

		gunModel[1].addBox(0F, 0F, 0F, 3, 2, 8, 0F); // Box 33
		gunModel[1].setRotationPoint(2F, -4F, -4F);

		gunModel[2].addShapeBox(0F, 0F, 0F, 10, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 34
		gunModel[2].setRotationPoint(-8F, -1F, -4F);

		gunModel[3].addShapeBox(0F, 0F, 0F, 10, 5, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F); // Box 35
		gunModel[3].setRotationPoint(-8F, 2F, -4F);

		gunModel[4].addShapeBox(0F, 0F, 0F, 10, 7, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, -3F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 2F, -3F, 0F); // Box 36
		gunModel[4].setRotationPoint(-10F, 7F, -4F);

		gunModel[5].addShapeBox(0F, 0F, 0F, 9, 2, 8, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F); // Box 38
		gunModel[5].setRotationPoint(-12F, 14F, -4F);

		gunModel[6].addShapeBox(0F, 0F, 0F, 2, 4, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F); // Box 39
		gunModel[6].setRotationPoint(2F, -2F, -4F);

		gunModel[7].addShapeBox(0F, 0F, 0F, 2, 5, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, -1.5F, -2F, 0F, -1.5F, 2F, 0F, 0F); // Box 40
		gunModel[7].setRotationPoint(2F, 2F, -4F);

		gunModel[8].addShapeBox(0F, 0F, 0F, 2, 7, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, -1.5F, -3F, 0F, -1.5F, 3F, 0F, 0F); // Box 41
		gunModel[8].setRotationPoint(0F, 7F, -4F);

		gunModel[9].addShapeBox(0F, 0F, 0F, 2, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F); // Box 42
		gunModel[9].setRotationPoint(-3F, 14F, -4F);

		gunModel[10].addBox(0F, 0F, 0F, 28, 10, 8, 0F); // Box 0
		gunModel[10].setRotationPoint(-11F, -14F, -4F);

		gunModel[11].addBox(0F, 0F, 0F, 13, 4, 8, 0F); // Box 1
		gunModel[11].setRotationPoint(16F, -4F, -4F);

		gunModel[12].addShapeBox(0F, 0F, 0F, 13, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F); // Box 2
		gunModel[12].setRotationPoint(16F, 0F, -4F);

		gunModel[13].addShapeBox(0F, 0F, 0F, 1, 3, 8, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 3
		gunModel[13].setRotationPoint(28F, -7F, -4F);

		gunModel[14].addBox(0F, 0F, 0F, 21, 7, 8, 0F); // Box 4
		gunModel[14].setRotationPoint(28F, -14F, -4F);

		gunModel[15].addBox(0F, 0F, 0F, 13, 3, 7, 0F); // Box 5
		gunModel[15].setRotationPoint(29F, -5F, -3.5F);

		gunModel[16].addShapeBox(0F, 0F, 0F, 4, 5, 8, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 7
		gunModel[16].setRotationPoint(-11F, -19F, -4F);

		gunModel[17].addBox(0F, 0F, 0F, 6, 3, 8, 0F); // Box 8
		gunModel[17].setRotationPoint(43F, -19F, -4F);

		gunModel[18].addShapeBox(0F, 0F, 0F, 13, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 9
		gunModel[18].setRotationPoint(29F, -2F, -3.5F);

		gunModel[19].addBox(0F, 0F, 0F, 33, 3, 7, 0F); // Box 10
		gunModel[19].setRotationPoint(10F, -19F, -3.5F);

		gunModel[20].addShapeBox(0F, 0F, 0F, 28, 6, 1, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 11
		gunModel[20].setRotationPoint(-11F, -14F, -5F);

		gunModel[21].addShapeBox(0F, 0F, 0F, 21, 6, 1, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 13
		gunModel[21].setRotationPoint(28F, -14F, -5F);

		gunModel[22].addShapeBox(0F, 0F, 0F, 4, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, -2F, 0F, -2F, -2F, 0F, -2F, 0F, 0F, -2F); // Box 14
		gunModel[22].setRotationPoint(42F, -2F, -3.5F);

		gunModel[23].addShapeBox(0F, 0F, 0F, 6, 3, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F); // Box 15
		gunModel[23].setRotationPoint(42F, -5F, -3.5F);

		gunModel[24].addShapeBox(0F, 0F, 0F, 60, 6, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F); // Box 16
		gunModel[24].setRotationPoint(-11F, -14F, 4F);

		gunModel[25].addBox(0F, 0F, 0F, 3, 10, 1, 0F); // Box 17
		gunModel[25].setRotationPoint(49F, -19F, -3.5F);

		gunModel[26].addBox(0F, 0F, 0F, 3, 10, 1, 0F); // Box 18
		gunModel[26].setRotationPoint(49F, -19F, 2.5F);

		gunModel[27].addBox(0F, 0F, 0F, 20, 2, 7, 0F); // Box 19
		gunModel[27].setRotationPoint(29F, -7F, -3.5F);

		gunModel[28].addBox(0F, 0F, 0F, 7, 5, 2, 0F); // Box 21
		gunModel[28].setRotationPoint(49F, -13F, -1F);

		gunModel[29].addShapeBox(0F, 0F, 0F, 7, 5, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, -0.5F, 0F, -1.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, -0.5F, 0F, -1.5F, -0.5F); // Box 22
		gunModel[29].setRotationPoint(49F, -13F, 1F);

		gunModel[30].addShapeBox(0F, 0F, 0F, 7, 5, 2, 0F, 0F, -1.5F, -0.5F, 0F, -1.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, -0.5F, 0F, -1.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 23
		gunModel[30].setRotationPoint(49F, -13F, -3F);

		gunModel[31].addShapeBox(0F, 0F, 0F, 5, 5, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, -0.5F, 0F, -1.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, -0.5F, 0F, -1.5F, -0.5F); // Box 24
		gunModel[31].setRotationPoint(49F, -18.5F, 1F);

		gunModel[32].addBox(0F, 0F, 0F, 5, 5, 2, 0F); // Box 25
		gunModel[32].setRotationPoint(49F, -18.5F, -1F);

		gunModel[33].addShapeBox(0F, 0F, 0F, 5, 5, 2, 0F, 0F, -1.5F, -0.5F, 0F, -1.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, -0.5F, 0F, -1.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 26
		gunModel[33].setRotationPoint(49F, -18.5F, -3F);

		gunModel[34].addShapeBox(0F, 0F, 0F, 3, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F); // Box 30
		gunModel[34].setRotationPoint(49F, -9F, 2.5F);

		gunModel[35].addShapeBox(0F, 0F, 0F, 3, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F); // Box 31
		gunModel[35].setRotationPoint(49F, -9F, -3.5F);

		gunModel[36].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 33
		gunModel[36].setRotationPoint(10F, -21F, -3.5F);

		gunModel[37].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 34
		gunModel[37].setRotationPoint(22F, -21F, -3.5F);

		gunModel[38].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 35
		gunModel[38].setRotationPoint(16F, -21F, -3.5F);

		gunModel[39].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 36
		gunModel[39].setRotationPoint(40F, -21F, -3.5F);

		gunModel[40].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 37
		gunModel[40].setRotationPoint(34F, -21F, -3.5F);

		gunModel[41].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 38
		gunModel[41].setRotationPoint(28F, -21F, -3.5F);

		gunModel[42].addBox(0F, 0F, 0F, 7, 5, 8, 0F); // Box 39
		gunModel[42].setRotationPoint(-7F, -19F, -4F);

		gunModel[43].addShapeBox(0F, 0F, 0F, 2, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F); // Box 40
		gunModel[43].setRotationPoint(8F, -18F, -4F);

		gunModel[44].addShapeBox(0F, 0F, 0F, 1, 3, 8, 0F, -1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 41
		gunModel[44].setRotationPoint(3F, -18F, -4F);

		gunModel[45].addShapeBox(0F, 0F, 0F, 1, 3, 8, 0F, -1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 42
		gunModel[45].setRotationPoint(5F, -18F, -4F);

		gunModel[46].addShapeBox(0F, 0F, 0F, 2, 3, 8, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 43
		gunModel[46].setRotationPoint(0F, -18F, -4F);

		gunModel[47].addBox(0F, 0F, 0F, 10, 1, 8, 0F); // Box 44
		gunModel[47].setRotationPoint(0F, -19F, -4F);

		gunModel[48].addBox(0F, 0F, 0F, 10, 1, 8, 0F); // Box 45
		gunModel[48].setRotationPoint(0F, -15F, -4F);

		gunModel[49].addShapeBox(0F, 0F, 0F, 5, 3, 7, 0F, -1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 46
		gunModel[49].setRotationPoint(2F, -18F, -3.5F);

		gunModel[50].addBox(0F, 0F, 0F, 39, 2, 8, 0F); // Box 47
		gunModel[50].setRotationPoint(10F, -16F, -4F);

		gunModel[51].addBox(0F, 0F, 0F, 11, 4, 8, 0F); // Box 0
		gunModel[51].setRotationPoint(17F, -8F, -4F);

		gunModel[52].addBox(0F, 0F, 0F, 11, 6, 7, 0F); // Box 1
		gunModel[52].setRotationPoint(17F, -14F, -3F);

		gunModel[53].addShapeBox(0F, 0F, 0F, 18, 1, 8, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 6
		gunModel[53].setRotationPoint(-8F, -20F, -4F);

		gunModel[54].addShapeBox(0F, 0F, 0F, 6, 1, 8, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 7
		gunModel[54].setRotationPoint(43F, -20F, -4F);

		gunModel[55].addBox(0F, 0F, 0F, 10, 2, 6, 0F); // Box 8
		gunModel[55].setRotationPoint(-5F, -22F, -3F);

		gunModel[56].addBox(0F, 0F, 0F, 3, 2, 6, 0F); // Box 11
		gunModel[56].setRotationPoint(46F, -22F, -3F);

		gunModel[57].addBox(0F, 0F, 0F, 5, 1, 2, 0F); // Box 13
		gunModel[57].setRotationPoint(0F, -23F, -3F);

		gunModel[58].addBox(0F, 0F, 0F, 5, 1, 2, 0F); // Box 14
		gunModel[58].setRotationPoint(0F, -23F, 1F);

		gunModel[59].addBox(0F, 0F, 0F, 11, 4, 2, 0F); // Box 16
		gunModel[59].setRotationPoint(17F, -8.5F, -5F);

		gunModel[60].addShapeBox(0F, 0F, 0F, 11, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 17
		gunModel[60].setRotationPoint(17F, -4.5F, -5F);

		gunModel[61].addShapeBox(0F, 0F, 0F, 3, 1, 2, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 18
		gunModel[61].setRotationPoint(18F, -9F, -5.5F);

		gunModel[62].addShapeBox(0F, 0F, 0F, 3, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 19
		gunModel[62].setRotationPoint(18F, -8F, -5.5F);

		gunModel[63].addShapeBox(0F, 0F, 0F, 3, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 20
		gunModel[63].setRotationPoint(24F, -8F, -5.5F);

		gunModel[64].addShapeBox(0F, 0F, 0F, 3, 1, 2, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 21
		gunModel[64].setRotationPoint(24F, -9F, -5.5F);


		defaultBarrelModel = new ModelRendererTurbo[3];
		defaultBarrelModel[0] = new ModelRendererTurbo(this, 283, 3, textureX, textureY); // Box 27
		defaultBarrelModel[1] = new ModelRendererTurbo(this, 254, 3, textureX, textureY); // Box 28
		defaultBarrelModel[2] = new ModelRendererTurbo(this, 254, 3, textureX, textureY); // Box 29

		defaultBarrelModel[0].addBox(0F, 0F, 0F, 12, 6, 2, 0F); // Box 27
		defaultBarrelModel[0].setRotationPoint(56F, -13.5F, -1F);

		defaultBarrelModel[1].addShapeBox(0F, 0F, 0F, 12, 6, 2, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 28
		defaultBarrelModel[1].setRotationPoint(56F, -13.5F, -3F);

		defaultBarrelModel[2].addShapeBox(0F, 0F, 0F, 12, 6, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F); // Box 29
		defaultBarrelModel[2].setRotationPoint(56F, -13.5F, 1F);


		defaultScopeModel = new ModelRendererTurbo[3];
		defaultScopeModel[0] = new ModelRendererTurbo(this, 81, 110, textureX, textureY); // Box 9
		defaultScopeModel[1] = new ModelRendererTurbo(this, 81, 115, textureX, textureY); // Box 10
		defaultScopeModel[2] = new ModelRendererTurbo(this, 67, 121, textureX, textureY); // Box 12

		defaultScopeModel[0].addShapeBox(0F, 0F, 0F, 5, 2, 2, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 9
		defaultScopeModel[0].setRotationPoint(0F, -25F, -3F);

		defaultScopeModel[1].addShapeBox(0F, 0F, 0F, 5, 2, 2, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 10
		defaultScopeModel[1].setRotationPoint(0F, -25F, 1F);

		defaultScopeModel[2].addShapeBox(0F, 0F, 0F, 3, 4, 3, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 12
		defaultScopeModel[2].setRotationPoint(46F, -26F, -1F);


		defaultStockModel = new ModelRendererTurbo[11];
		defaultStockModel[0] = new ModelRendererTurbo(this, 224, 70, textureX, textureY); // Box 42
		defaultStockModel[1] = new ModelRendererTurbo(this, 224, 70, textureX, textureY); // Box 42
		defaultStockModel[2] = new ModelRendererTurbo(this, 224, 70, textureX, textureY); // Box 42
		defaultStockModel[3] = new ModelRendererTurbo(this, 211, 68, textureX, textureY); // Box 42
		defaultStockModel[4] = new ModelRendererTurbo(this, 200, 69, textureX, textureY); // Box 42
		defaultStockModel[5] = new ModelRendererTurbo(this, 200, 69, textureX, textureY); // Box 42
		defaultStockModel[6] = new ModelRendererTurbo(this, 218, 81, textureX, textureY); // Box 42
		defaultStockModel[7] = new ModelRendererTurbo(this, 200, 79, textureX, textureY); // Box 42
		defaultStockModel[8] = new ModelRendererTurbo(this, 200, 79, textureX, textureY); // Box 42
		defaultStockModel[9] = new ModelRendererTurbo(this, 213, 81, textureX, textureY); // Box 42
		defaultStockModel[10] = new ModelRendererTurbo(this, 213, 81, textureX, textureY); // Box 42

		defaultStockModel[0].addShapeBox(0F, 0F, 0F, 5, 6, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F); // Box 42
		defaultStockModel[0].setRotationPoint(-16F, -14F, 1F);

		defaultStockModel[1].addBox(0F, 0F, 0F, 5, 6, 2, 0F); // Box 42
		defaultStockModel[1].setRotationPoint(-16F, -14F, -1F);

		defaultStockModel[2].addShapeBox(0F, 0F, 0F, 5, 6, 2, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 42
		defaultStockModel[2].setRotationPoint(-16F, -14F, -3F);

		defaultStockModel[3].addBox(0F, 0F, 0F, 3, 7, 3, 0F); // Box 42
		defaultStockModel[3].setRotationPoint(-15.5F, -14.5F, -1.5F);

		defaultStockModel[4].addShapeBox(0F, 0F, 0F, 3, 7, 2, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 42
		defaultStockModel[4].setRotationPoint(-15.5F, -14.5F, -3.5F);

		defaultStockModel[5].addShapeBox(0F, 0F, 0F, 3, 7, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F); // Box 42
		defaultStockModel[5].setRotationPoint(-15.5F, -14.5F, 1.5F);

		defaultStockModel[6].addShapeBox(0F, 0F, 0F, 1, 2, 2, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F); // Box 42
		defaultStockModel[6].setRotationPoint(-17F, -12F, -1F);

		defaultStockModel[7].addShapeBox(0F, 0F, 0F, 1, 1, 5, 0F, 0F, 0F, -1F, -0.5F, 0F, -1F, -0.5F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F); // Box 42
		defaultStockModel[7].setRotationPoint(-16.5F, -11.5F, -2.5F);

		defaultStockModel[8].addShapeBox(0F, 0F, 0F, 1, 1, 5, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, -0.5F, 0F, -1F, -0.5F, 0F, -1F, 0F, 0F, -1F); // Box 42
		defaultStockModel[8].setRotationPoint(-16.5F, -7.5F, -2.5F);

		defaultStockModel[9].addShapeBox(0F, 0F, 0F, 1, 3, 1, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F); // Box 42
		defaultStockModel[9].setRotationPoint(-16.5F, -10.5F, -2.5F);

		defaultStockModel[10].addShapeBox(0F, 0F, 0F, 1, 3, 1, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F); // Box 42
		defaultStockModel[10].setRotationPoint(-16.5F, -10.5F, 1.5F);


		ammoModel = new ModelRendererTurbo[10];
		ammoModel[0] = new ModelRendererTurbo(this, 1, 134, textureX, textureY); // Box 22
		ammoModel[1] = new ModelRendererTurbo(this, 1, 146, textureX, textureY); // Box 23
		ammoModel[2] = new ModelRendererTurbo(this, 40, 143, textureX, textureY); // Box 24
		ammoModel[3] = new ModelRendererTurbo(this, 40, 129, textureX, textureY); // Box 26
		ammoModel[4] = new ModelRendererTurbo(this, 75, 130, textureX, textureY); // Box 28
		ammoModel[5] = new ModelRendererTurbo(this, 75, 143, textureX, textureY); // Box 29
		ammoModel[6] = new ModelRendererTurbo(this, 75, 155, textureX, textureY); // Box 30
		ammoModel[7] = new ModelRendererTurbo(this, 1, 126, textureX, textureY); // Box 31
		ammoModel[8] = new ModelRendererTurbo(this, 1, 120, textureX, textureY); // Box 32
		ammoModel[9] = new ModelRendererTurbo(this, 26, 120, textureX, textureY); // Box 33

		ammoModel[0].addBox(0F, 0F, 0F, 12, 4, 7, 0F); // Box 22
		ammoModel[0].setRotationPoint(16.5F, -3F, -3.5F);

		ammoModel[1].addShapeBox(0F, 0F, 0F, 12, 3, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F); // Box 23
		ammoModel[1].setRotationPoint(16.5F, 1F, -3.5F);

		ammoModel[2].addBox(0F, 0F, 0F, 11, 4, 6, 0F); // Box 24
		ammoModel[2].setRotationPoint(17F, 2F, -3F);

		ammoModel[3].addShapeBox(0F, 0F, 0F, 11, 8, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F); // Box 26
		ammoModel[3].setRotationPoint(17F, 6F, -3F);

		ammoModel[4].addShapeBox(0F, 0F, 0F, 11, 6, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 2.5F, -2F, 0F, 2.5F, -2F, 0F, -3F, 0F, 0F); // Box 28
		ammoModel[4].setRotationPoint(19F, 14F, -3F);

		ammoModel[5].addShapeBox(0F, 4F, 0F, 11, 4, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 29
		ammoModel[5].setRotationPoint(20.5F, 18F, -3.5F);
		ammoModel[5].rotateAngleZ = 0.45378561F;

		ammoModel[6].addShapeBox(0F, 0F, 0F, 7, 4, 7, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 30
		ammoModel[6].setRotationPoint(20.5F, 18F, -3.5F);
		ammoModel[6].rotateAngleZ = 0.45378561F;

		ammoModel[7].addBox(0F, 0F, 0F, 11, 1, 6, 0F); // Box 31
		ammoModel[7].setRotationPoint(17F, -3.5F, -3F);

		ammoModel[8].addShapeBox(0F, 0F, 0F, 8, 1, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 32
		ammoModel[8].setRotationPoint(17.5F, -4.5F, -2F);

		ammoModel[9].addShapeBox(0F, 0F, 0F, 2, 1, 4, 0F, 0F, 0F, -1F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 33
		ammoModel[9].setRotationPoint(25.5F, -4.5F, -2F);


		slideModel = new ModelRendererTurbo[6];
		slideModel[0] = new ModelRendererTurbo(this, 200, 50, textureX, textureY); // Box 15
		slideModel[1] = new ModelRendererTurbo(this, 240, 59, textureX, textureY); // Box 46
		slideModel[2] = new ModelRendererTurbo(this, 227, 57, textureX, textureY); // Box 47
		slideModel[3] = new ModelRendererTurbo(this, 239, 53, textureX, textureY); // Box 48
		slideModel[4] = new ModelRendererTurbo(this, 239, 49, textureX, textureY); // Box 49
		slideModel[5] = new ModelRendererTurbo(this, 219, 51, textureX, textureY); // Box 50

		slideModel[0].addShapeBox(0F, 0F, 0F, 8, 5, 1, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 15
		slideModel[0].setRotationPoint(17F, -14F, -4F);

		slideModel[1].addShapeBox(0F, 0F, 0F, 2, 1, 2, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 46
		slideModel[1].setRotationPoint(26F, -12.5F, -6F);

		slideModel[2].addShapeBox(0F, 0F, 0F, 2, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 47
		slideModel[2].setRotationPoint(26F, -11.5F, -8F);

		slideModel[3].addBox(0F, 0F, 0F, 2, 1, 2, 0F); // Box 48
		slideModel[3].setRotationPoint(26F, -12.5F, -8F);

		slideModel[4].addShapeBox(0F, 0F, 0F, 2, 1, 2, 0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 49
		slideModel[4].setRotationPoint(26F, -13.5F, -8F);

		slideModel[5].addShapeBox(0F, 0F, 0F, 3, 4, 1, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 50
		slideModel[5].setRotationPoint(25F, -13.5F, -4F);


		barrelAttachPoint = new Vector3f(71F /16F, 16.5F /16F, 0F /16F);
		stockAttachPoint = new Vector3f(-11F /16F, 17F /16F, 0F /16F);

		gunSlideDistance = 0.8F;
		animationType = EnumAnimationType.BOTTOM_CLIP;

		flipAll();

		translateAll(0F, 0F, 0F);
		
	}
}
