package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelMBTCrossbow extends ModelGun
{
	int textureX = 512;
	int textureY = 256;

	public ModelMBTCrossbow()
	{
		gunModel = new ModelRendererTurbo[47];
		gunModel[0] = new ModelRendererTurbo(this, 118, 54, textureX, textureY); // body1
		gunModel[1] = new ModelRendererTurbo(this, 119, 12, textureX, textureY); // body2
		gunModel[2] = new ModelRendererTurbo(this, 118, 33, textureX, textureY); // body3
		gunModel[3] = new ModelRendererTurbo(this, 118, 89, textureX, textureY); // body4
		gunModel[4] = new ModelRendererTurbo(this, 118, 89, textureX, textureY); // body5
		gunModel[5] = new ModelRendererTurbo(this, 118, 77, textureX, textureY); // body6
		gunModel[6] = new ModelRendererTurbo(this, 118, 67, textureX, textureY); // body7
		gunModel[7] = new ModelRendererTurbo(this, 118, 106, textureX, textureY); // body8
		gunModel[8] = new ModelRendererTurbo(this, 118, 99, textureX, textureY); // body9
		gunModel[9] = new ModelRendererTurbo(this, 1, 71, textureX, textureY); // bow1
		gunModel[10] = new ModelRendererTurbo(this, 66, 115, textureX, textureY); // bow1
		gunModel[11] = new ModelRendererTurbo(this, 26, 71, textureX, textureY); // bow2
		gunModel[12] = new ModelRendererTurbo(this, 26, 71, textureX, textureY); // bow2
		gunModel[13] = new ModelRendererTurbo(this, 49, 73, textureX, textureY); // bow3
		gunModel[14] = new ModelRendererTurbo(this, 49, 73, textureX, textureY); // bow3
		gunModel[15] = new ModelRendererTurbo(this, 1, 88, textureX, textureY); // bow4
		gunModel[16] = new ModelRendererTurbo(this, 1, 88, textureX, textureY); // bow4
		gunModel[17] = new ModelRendererTurbo(this, 20, 89, textureX, textureY); // bow5
		gunModel[18] = new ModelRendererTurbo(this, 20, 89, textureX, textureY); // bow5
		gunModel[19] = new ModelRendererTurbo(this, 39, 89, textureX, textureY); // bow6
		gunModel[20] = new ModelRendererTurbo(this, 39, 89, textureX, textureY); // bow6
		gunModel[21] = new ModelRendererTurbo(this, 118, 119, textureX, textureY); // foreGrip1
		gunModel[22] = new ModelRendererTurbo(this, 118, 133, textureX, textureY); // foreGrip2
		gunModel[23] = new ModelRendererTurbo(this, 1, 59, textureX, textureY); // grip1
		gunModel[24] = new ModelRendererTurbo(this, 24, 2, textureX, textureY); // ironSight1
		gunModel[25] = new ModelRendererTurbo(this, 70, 14, textureX, textureY); // rail1
		gunModel[26] = new ModelRendererTurbo(this, 70, 14, textureX, textureY); // rail2
		gunModel[27] = new ModelRendererTurbo(this, 70, 14, textureX, textureY); // rail3
		gunModel[28] = new ModelRendererTurbo(this, 70, 14, textureX, textureY); // rail4
		gunModel[29] = new ModelRendererTurbo(this, 70, 14, textureX, textureY); // rail5
		gunModel[30] = new ModelRendererTurbo(this, 47, 2, textureX, textureY); // railBase
		gunModel[31] = new ModelRendererTurbo(this, 47, 12, textureX, textureY); // railBolt
		gunModel[32] = new ModelRendererTurbo(this, 47, 12, textureX, textureY); // railBolt
		gunModel[33] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // sightBolt
		gunModel[34] = new ModelRendererTurbo(this, 1, 68, textureX, textureY); // string
		gunModel[35] = new ModelRendererTurbo(this, 1, 68, textureX, textureY); // string
		gunModel[36] = new ModelRendererTurbo(this, 71, 71, textureX, textureY); // stringHolder
		gunModel[37] = new ModelRendererTurbo(this, 71, 82, textureX, textureY); // stringHolder2
		gunModel[38] = new ModelRendererTurbo(this, 1, 24, textureX, textureY); // Box 59
		gunModel[39] = new ModelRendererTurbo(this, 1, 34, textureX, textureY); // Box 60
		gunModel[40] = new ModelRendererTurbo(this, 1, 48, textureX, textureY); // Box 61
		gunModel[41] = new ModelRendererTurbo(this, 30, 24, textureX, textureY); // Box 62
		gunModel[42] = new ModelRendererTurbo(this, 30, 48, textureX, textureY); // Box 63
		gunModel[43] = new ModelRendererTurbo(this, 30, 34, textureX, textureY); // Box 64
		gunModel[44] = new ModelRendererTurbo(this, 47, 24, textureX, textureY); // Box 65
		gunModel[45] = new ModelRendererTurbo(this, 47, 34, textureX, textureY); // Box 66
		gunModel[46] = new ModelRendererTurbo(this, 47, 49, textureX, textureY); // Box 67

		gunModel[0].addShapeBox(0F, 0F, 0F, 40, 2, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // body1
		gunModel[0].setRotationPoint(-16F, -8F, -5F);

		gunModel[1].addBox(0F, 0F, 0F, 40, 10, 10, 0F); // body2
		gunModel[1].setRotationPoint(-16F, -18F, -5F);

		gunModel[2].addBox(0F, 0F, 0F, 45, 12, 8, 0F); // body3
		gunModel[2].setRotationPoint(-15F, -20F, -4F);

		gunModel[3].addBox(0F, 0F, 0F, 45, 6, 3, 0F); // body4
		gunModel[3].setRotationPoint(30F, -20F, -4F);

		gunModel[4].addBox(0F, 0F, 0F, 45, 6, 3, 0F); // body5
		gunModel[4].setRotationPoint(30F, -20F, 1F);

		gunModel[5].addBox(0F, 0F, 0F, 45, 4, 7, 0F); // body6
		gunModel[5].setRotationPoint(30F, -14F, -3.5F);

		gunModel[6].addShapeBox(0F, 0F, 0F, 45, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // body7
		gunModel[6].setRotationPoint(30F, -10F, -3.5F);

		gunModel[7].addShapeBox(0F, 0F, 0F, 4, 4, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F); // body8
		gunModel[7].setRotationPoint(75F, -18F, -4F);

		gunModel[8].addBox(0F, 0F, 0F, 45, 4, 2, 0F); // body9
		gunModel[8].setRotationPoint(30F, -18F, -1F);

		gunModel[9].addBox(0F, 0F, 0F, 2, 6, 10, 0F); // bow1
		gunModel[9].setRotationPoint(73F, -20F, 4F);

		gunModel[10].addBox(0F, 0F, 0F, 2, 6, 10, 0F); // bow1
		gunModel[10].setRotationPoint(73F, -20F, -14F);

		gunModel[11].addShapeBox(0F, 0F, 0F, 1, 6, 10, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F); // bow2
		gunModel[11].setRotationPoint(72F, -20F, 4F);

		gunModel[12].addShapeBox(0F, 0F, 0F, 1, 6, 10, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F); // bow2
		gunModel[12].setRotationPoint(72F, -20F, -14F);

		gunModel[13].addShapeBox(0F, 0F, 0F, 2, 6, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, 4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, 4F, 0F, 0F); // bow3
		gunModel[13].setRotationPoint(73F, -20F, 14F);

		gunModel[14].addShapeBox(0F, 0F, 0F, 2, 6, 8, 0F, 4F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // bow3
		gunModel[14].setRotationPoint(73F, -20F, -22F);

		gunModel[15].addShapeBox(0F, 0F, 0F, 1, 6, 8, 0F, 4F, -1F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 4F, -1F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F); // bow4
		gunModel[15].setRotationPoint(72F, -20F, -22F);

		gunModel[16].addShapeBox(0F, 0F, 0F, 1, 6, 8, 0F, 0F, -1F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, 4F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, 4F, -1F, 0F); // bow4
		gunModel[16].setRotationPoint(72F, -20F, 14F);

		gunModel[17].addShapeBox(0F, 0F, 0F, 2, 6, 7, 0F, 8F, 0F, 0F, -8F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 8F, 0F, 0F, -8F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // bow5
		gunModel[17].setRotationPoint(69F, -20F, -29F);

		gunModel[18].addShapeBox(0F, 0F, 0F, 2, 6, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -8F, 0F, 0F, 8F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -8F, 0F, 0F, 8F, 0F, 0F); // bow5
		gunModel[18].setRotationPoint(69F, -20F, 22F);

		gunModel[19].addShapeBox(0F, 0F, 0F, 1, 6, 7, 0F, 0F, -1F, 0F, 0F, 0F, 0F, -8F, 0F, 0F, 8F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, -8F, 0F, 0F, 8F, -1F, 0F); // bow6
		gunModel[19].setRotationPoint(68F, -20F, 22F);

		gunModel[20].addShapeBox(0F, 0F, 0F, 1, 6, 7, 0F, 8F, -1F, 0F, -8F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 8F, -1F, 0F, -8F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F); // bow6
		gunModel[20].setRotationPoint(68F, -20F, -29F);

		gunModel[21].addBox(0F, 0F, 0F, 20, 4, 9, 0F); // foreGrip1
		gunModel[21].setRotationPoint(31F, -14F, -4.5F);

		gunModel[22].addShapeBox(0F, 0F, 0F, 20, 3, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // foreGrip2
		gunModel[22].setRotationPoint(31F, -10F, -4.5F);

		gunModel[23].addShapeBox(0F, 0F, 0F, 12, 2, 6, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // grip1
		gunModel[23].setRotationPoint(-12F, -6F, -3F);

		gunModel[24].addBox(0F, -1F, 0F, 4, 3, 7, 0F); // ironSight1
		gunModel[24].setRotationPoint(-14F, -22F, -3.5F);

		gunModel[25].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // rail1
		gunModel[25].setRotationPoint(-9F, -24F, -3.5F);

		gunModel[26].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // rail2
		gunModel[26].setRotationPoint(-3F, -24F, -3.5F);

		gunModel[27].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // rail3
		gunModel[27].setRotationPoint(3F, -24F, -3.5F);

		gunModel[28].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // rail4
		gunModel[28].setRotationPoint(9F, -24F, -3.5F);

		gunModel[29].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // rail5
		gunModel[29].setRotationPoint(15F, -24F, -3.5F);

		gunModel[30].addBox(0F, 0F, 0F, 27, 2, 7, 0F); // railBase
		gunModel[30].setRotationPoint(-9F, -22F, -3.5F);

		gunModel[31].addBox(0F, 0F, 0F, 2, 2, 9, 0F); // railBolt
		gunModel[31].setRotationPoint(15F, -21F, -4.5F);

		gunModel[32].addBox(0F, 0F, 0F, 2, 2, 9, 0F); // railBolt
		gunModel[32].setRotationPoint(-8F, -21F, -4.5F);

		gunModel[33].addBox(0F, 0F, 0F, 2, 2, 9, 0F); // sightBolt
		gunModel[33].setRotationPoint(-13F, -21F, -4.5F);

		gunModel[34].addBox(0F, 0F, 0F, 42, 1, 1, 0F); // string
		gunModel[34].setRotationPoint(29F, -21F, -4F);
		gunModel[34].rotateAngleY = -0.64577182F;
		gunModel[34].rotateAngleZ = -0.08726646F;

		gunModel[35].addBox(0F, 0F, 0F, 42, 1, 1, 0F); // string
		gunModel[35].setRotationPoint(29F, -21F, 3F);
		gunModel[35].rotateAngleY = 0.64577182F;
		gunModel[35].rotateAngleZ = -0.08726646F;

		gunModel[36].addBox(0F, 0F, 0F, 2, 2, 8, 0F); // stringHolder
		gunModel[36].setRotationPoint(28F, -22F, -4F);

		gunModel[37].addShapeBox(0F, 0F, 0F, 1, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F); // stringHolder2
		gunModel[37].setRotationPoint(27F, -22F, -3F);

		gunModel[38].addShapeBox(0F, 0F, 0F, 8, 3, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 59
		gunModel[38].setRotationPoint(-10F, -4F, -3F);

		gunModel[39].addShapeBox(0F, 0F, 0F, 8, 7, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 3F, 0F, 0F); // Box 60
		gunModel[39].setRotationPoint(-10F, -1F, -3F);

		gunModel[40].addShapeBox(0F, 0F, 0F, 8, 4, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F); // Box 61
		gunModel[40].setRotationPoint(-13F, 6F, -3F);

		gunModel[41].addShapeBox(0F, 0F, 0F, 2, 3, 6, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 62
		gunModel[41].setRotationPoint(-2F, -4F, -3F);

		gunModel[42].addShapeBox(0F, 0F, 0F, 2, 4, 6, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.25F, -1F, 0F, 0.25F, -1F, 0F, 0F, 0F); // Box 63
		gunModel[42].setRotationPoint(-5F, 6F, -3F);

		gunModel[43].addShapeBox(0F, 0F, 0F, 2, 7, 6, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, -1F, -3F, 0F, -1F, 3F, 0F, 0F); // Box 64
		gunModel[43].setRotationPoint(-2F, -1F, -3F);

		gunModel[44].addShapeBox(0F, 0F, 0F, 1, 3, 6, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 65
		gunModel[44].setRotationPoint(-11F, -4F, -3F);

		gunModel[45].addShapeBox(0F, 0F, 0F, 1, 7, 6, 0F, -3F, 0F, -1F, 3F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 66
		gunModel[45].setRotationPoint(-14F, -1F, -3F);

		gunModel[46].addShapeBox(0F, 0F, 0F, 1, 3, 6, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -0.25F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.25F, -1F); // Box 67
		gunModel[46].setRotationPoint(-14F, 6F, -3F);


		defaultScopeModel = new ModelRendererTurbo[10];
		defaultScopeModel[0] = new ModelRendererTurbo(this, 24, 2, textureX, textureY); // ironSight2
		defaultScopeModel[1] = new ModelRendererTurbo(this, 15, 4, textureX, textureY); // ironSight3
		defaultScopeModel[2] = new ModelRendererTurbo(this, 15, 4, textureX, textureY); // ironSight3
		defaultScopeModel[3] = new ModelRendererTurbo(this, 1, 13, textureX, textureY); // sightFront1
		defaultScopeModel[4] = new ModelRendererTurbo(this, 1, 13, textureX, textureY); // sightFront1
		defaultScopeModel[5] = new ModelRendererTurbo(this, 24, 19, textureX, textureY); // sightFront2
		defaultScopeModel[6] = new ModelRendererTurbo(this, 24, 19, textureX, textureY); // sightFront2
		defaultScopeModel[7] = new ModelRendererTurbo(this, 24, 19, textureX, textureY); // sightFront2
		defaultScopeModel[8] = new ModelRendererTurbo(this, 24, 19, textureX, textureY); // sightFront2
		defaultScopeModel[9] = new ModelRendererTurbo(this, 24, 14, textureX, textureY); // sightFront3

		defaultScopeModel[0].addShapeBox(0F, 0F, 0F, 4, 3, 7, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // ironSight2
		defaultScopeModel[0].setRotationPoint(-14F, -26F, -3.5F);

		defaultScopeModel[1].addBox(0F, 0F, 0F, 3, 4, 1, 0F); // ironSight3
		defaultScopeModel[1].setRotationPoint(-13F, -30F, 2.5F);

		defaultScopeModel[2].addBox(0F, 0F, 0F, 3, 4, 1, 0F); // ironSight3
		defaultScopeModel[2].setRotationPoint(-13F, -30F, -3.5F);

		defaultScopeModel[3].addBox(0F, 0F, 0F, 2, 1, 9, 0F); // sightFront1
		defaultScopeModel[3].setRotationPoint(28F, -29F, -4.5F);

		defaultScopeModel[4].addBox(0F, 0F, 0F, 2, 1, 9, 0F); // sightFront1
		defaultScopeModel[4].setRotationPoint(28F, -23F, -4.5F);

		defaultScopeModel[5].addShapeBox(0F, 0F, 0F, 2, 3, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F, 2F); // sightFront2
		defaultScopeModel[5].setRotationPoint(28F, -29F, 3.5F);

		defaultScopeModel[6].addShapeBox(0F, 0F, 0F, 2, 3, 1, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // sightFront2
		defaultScopeModel[6].setRotationPoint(28F, -26F, 3.5F);

		defaultScopeModel[7].addShapeBox(0F, 0F, 0F, 2, 3, 1, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // sightFront2
		defaultScopeModel[7].setRotationPoint(28F, -29F, -6.5F);

		defaultScopeModel[8].addShapeBox(0F, 0F, 0F, 2, 3, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F, 2F); // sightFront2
		defaultScopeModel[8].setRotationPoint(28F, -26F, -6.5F);

		defaultScopeModel[9].addBox(0F, 0F, 0F, 2, 3, 1, 0F); // sightFront3
		defaultScopeModel[9].setRotationPoint(28F, -26F, -0.5F);


		defaultStockModel = new ModelRendererTurbo[9];
		defaultStockModel[0] = new ModelRendererTurbo(this, 1, 154, textureX, textureY); // Box 82
		defaultStockModel[1] = new ModelRendererTurbo(this, 1, 116, textureX, textureY); // Box 83
		defaultStockModel[2] = new ModelRendererTurbo(this, 1, 107, textureX, textureY); // Box 84
		defaultStockModel[3] = new ModelRendererTurbo(this, 39, 145, textureX, textureY); // Box 85
		defaultStockModel[4] = new ModelRendererTurbo(this, 26, 160, textureX, textureY); // Box 86
		defaultStockModel[5] = new ModelRendererTurbo(this, 39, 132, textureX, textureY); // Box 87
		defaultStockModel[6] = new ModelRendererTurbo(this, 1, 133, textureX, textureY); // Box 88
		defaultStockModel[7] = new ModelRendererTurbo(this, 19, 154, textureX, textureY); // Box 89
		defaultStockModel[8] = new ModelRendererTurbo(this, 26, 132, textureX, textureY); // Box 90

		defaultStockModel[0].addShapeBox(0F, 0F, 0F, 5, 7, 7, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F); // Box 82
		defaultStockModel[0].setRotationPoint(-21F, -15F, -3.5F);

		defaultStockModel[1].addShapeBox(0F, 0F, 0F, 25, 8, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 10F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 10F, 0F); // Box 83
		defaultStockModel[1].setRotationPoint(-46F, -15F, -3.5F);

		defaultStockModel[2].addShapeBox(0F, 0F, 0F, 25, 1, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 84
		defaultStockModel[2].setRotationPoint(-46F, -16F, -3.5F);

		defaultStockModel[3].addShapeBox(0F, 0F, 0F, 5, 1, 7, 0F, 0F, -2F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, -2F, -1F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F); // Box 85
		defaultStockModel[3].setRotationPoint(-21F, -18F, -3.5F);

		defaultStockModel[4].addShapeBox(0F, 0F, 0F, 6, 1, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 86
		defaultStockModel[4].setRotationPoint(-53F, -16F, -3.5F);

		defaultStockModel[5].addShapeBox(0F, 0F, 0F, 6, 5, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F); // Box 87
		defaultStockModel[5].setRotationPoint(-53F, -15F, -3.5F);

		defaultStockModel[6].addBox(0F, 0F, 0F, 5, 13, 7, 0F); // Box 88
		defaultStockModel[6].setRotationPoint(-52F, -10F, -3.5F);

		defaultStockModel[7].addShapeBox(0F, 0F, 0F, 1, 1, 5, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 89
		defaultStockModel[7].setRotationPoint(-47F, -15F, -2.5F);

		defaultStockModel[8].addBox(0F, 0F, 0F, 1, 16, 5, 0F); // Box 90
		defaultStockModel[8].setRotationPoint(-47F, -14F, -2.5F);


		ammoModel = new ModelRendererTurbo[3];
		ammoModel[0] = new ModelRendererTurbo(this, 120, 2, textureX, textureY); // bolt
		ammoModel[1] = new ModelRendererTurbo(this, 120, 7, textureX, textureY); // bolt
		ammoModel[2] = new ModelRendererTurbo(this, 220, 5, textureX, textureY); // boltTip

		ammoModel[0].addBox(0F, 0F, 0F, 48, 1, 3, 0F); // bolt
		ammoModel[0].setRotationPoint(30F, -21F, -1.5F);

		ammoModel[1].addBox(0F, 0F, 0F, 48, 3, 1, 0F); // bolt
		ammoModel[1].setRotationPoint(30F, -22F, -0.5F);

		ammoModel[2].addShapeBox(0F, 0F, 0F, 5, 3, 3, 0F, 0F, 0F, 0F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, 0F, 0F); // boltTip
		ammoModel[2].setRotationPoint(78F, -22F, -1.5F);

		stockAttachPoint = new Vector3f(-16F /16F, 13F /16F, 0F /16F);
		scopeAttachPoint = new Vector3f(5F /16F, 22F /16F, 0F /16F);

		endLoadedAmmoDistance = 5F;

		gunSlideDistance = 0F;
		animationType = EnumAnimationType.END_LOADED;


		translateAll(0F, 0F, 0F);


		flipAll();
	}
}