package com.flansmod.client.model.dwfirearms; //Path where the model is located

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelSkyshocker extends ModelGun
{
	int textureX = 512;
	int textureY = 256;

	public ModelSkyshocker()
	{
		gunModel = new ModelRendererTurbo[37];
		gunModel[0] = new ModelRendererTurbo(this, 1, 86, textureX, textureY); // Box 1
		gunModel[1] = new ModelRendererTurbo(this, 54, 86, textureX, textureY); // Box 2
		gunModel[2] = new ModelRendererTurbo(this, 54, 86, textureX, textureY); // Box 3
		gunModel[3] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 4
		gunModel[4] = new ModelRendererTurbo(this, 32, 1, textureX, textureY); // Box 5
		gunModel[5] = new ModelRendererTurbo(this, 53, 49, textureX, textureY); // Box 6
		gunModel[6] = new ModelRendererTurbo(this, 53, 40, textureX, textureY); // Box 7
		gunModel[7] = new ModelRendererTurbo(this, 53, 30, textureX, textureY); // Box 8
		gunModel[8] = new ModelRendererTurbo(this, 53, 20, textureX, textureY); // Box 9
		gunModel[9] = new ModelRendererTurbo(this, 53, 20, textureX, textureY); // Box 10
		gunModel[10] = new ModelRendererTurbo(this, 53, 40, textureX, textureY); // Box 11
		gunModel[11] = new ModelRendererTurbo(this, 53, 30, textureX, textureY); // Box 12
		gunModel[12] = new ModelRendererTurbo(this, 53, 20, textureX, textureY); // Box 13
		gunModel[13] = new ModelRendererTurbo(this, 53, 40, textureX, textureY); // Box 14
		gunModel[14] = new ModelRendererTurbo(this, 53, 30, textureX, textureY); // Box 15
		gunModel[15] = new ModelRendererTurbo(this, 53, 20, textureX, textureY); // Box 16
		gunModel[16] = new ModelRendererTurbo(this, 53, 40, textureX, textureY); // Box 17
		gunModel[17] = new ModelRendererTurbo(this, 53, 30, textureX, textureY); // Box 18
		gunModel[18] = new ModelRendererTurbo(this, 53, 9, textureX, textureY); // Box 19
		gunModel[19] = new ModelRendererTurbo(this, 84, 69, textureX, textureY); // Box 20
		gunModel[20] = new ModelRendererTurbo(this, 1, 98, textureX, textureY); // Box 22
		gunModel[21] = new ModelRendererTurbo(this, 34, 98, textureX, textureY); // Box 23
		gunModel[22] = new ModelRendererTurbo(this, 51, 121, textureX, textureY); // Box 24
		gunModel[23] = new ModelRendererTurbo(this, 51, 121, textureX, textureY); // Box 27
		gunModel[24] = new ModelRendererTurbo(this, 68, 125, textureX, textureY); // Box 28
		gunModel[25] = new ModelRendererTurbo(this, 68, 125, textureX, textureY); // Box 29
		gunModel[26] = new ModelRendererTurbo(this, 51, 101, textureX, textureY); // Box 30
		gunModel[27] = new ModelRendererTurbo(this, 101, 39, textureX, textureY); // Box 31
		gunModel[28] = new ModelRendererTurbo(this, 84, 46, textureX, textureY); // Box 32
		gunModel[29] = new ModelRendererTurbo(this, 132, 48, textureX, textureY); // Box 33
		gunModel[30] = new ModelRendererTurbo(this, 137, 52, textureX, textureY); // Box 34
		gunModel[31] = new ModelRendererTurbo(this, 132, 48, textureX, textureY); // Box 35
		gunModel[32] = new ModelRendererTurbo(this, 132, 52, textureX, textureY); // Box 36
		gunModel[33] = new ModelRendererTurbo(this, 101, 63, textureX, textureY); // Box 37
		gunModel[34] = new ModelRendererTurbo(this, 108, 90, textureX, textureY); // Box 38
		gunModel[35] = new ModelRendererTurbo(this, 108, 90, textureX, textureY); // Box 39
		gunModel[36] = new ModelRendererTurbo(this, 75, 90, textureX, textureY); // Box 40

		gunModel[0].addBox(-15F, -25F, -4F, 18, 3, 8, 0F); // Box 1
		gunModel[0].setRotationPoint(5F, 11F, 0F);
		gunModel[0].rotateAngleZ = 0.12217305F;

		gunModel[1].addShapeBox(-17F, -25F, -4F, 2, 3, 8, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 2
		gunModel[1].setRotationPoint(5F, 11F, 0F);
		gunModel[1].rotateAngleZ = 0.12217305F;

		gunModel[2].addShapeBox(3F, -25F, -4F, 2, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 3
		gunModel[2].setRotationPoint(5F, 11F, 0F);
		gunModel[2].rotateAngleZ = 0.12217305F;

		gunModel[3].addBox(-14F, -105F, -2F, 11, 80, 4, 0F); // Box 4
		gunModel[3].setRotationPoint(5F, 11F, 0F);
		gunModel[3].rotateAngleZ = 0.12217305F;

		gunModel[4].addShapeBox(-3F, -105F, -2F, 6, 80, 4, 0F, 0F, 0F, 0F, 0F, -8F, -1.5F, 0F, -8F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F); // Box 5
		gunModel[4].setRotationPoint(5F, 11F, 0F);
		gunModel[4].rotateAngleZ = 0.12217305F;

		gunModel[5].addBox(-15F, -55F, -3F, 9, 30, 6, 0F); // Box 6
		gunModel[5].setRotationPoint(5F, 11F, 0F);
		gunModel[5].rotateAngleZ = 0.12217305F;

		gunModel[6].addBox(-15F, -60F, -3F, 9, 2, 6, 0F); // Box 7
		gunModel[6].setRotationPoint(5F, 11F, 0F);
		gunModel[6].rotateAngleZ = 0.12217305F;

		gunModel[7].addBox(-15F, -58F, -3F, 3, 3, 6, 0F); // Box 8
		gunModel[7].setRotationPoint(5F, 11F, 0F);
		gunModel[7].rotateAngleZ = 0.12217305F;

		gunModel[8].addBox(-9F, -58F, -3F, 3, 3, 6, 0F); // Box 9
		gunModel[8].setRotationPoint(5F, 11F, 0F);
		gunModel[8].rotateAngleZ = 0.12217305F;

		gunModel[9].addBox(-9F, -63F, -3F, 3, 3, 6, 0F); // Box 10
		gunModel[9].setRotationPoint(5F, 11F, 0F);
		gunModel[9].rotateAngleZ = 0.12217305F;

		gunModel[10].addBox(-15F, -65F, -3F, 9, 2, 6, 0F); // Box 11
		gunModel[10].setRotationPoint(5F, 11F, 0F);
		gunModel[10].rotateAngleZ = 0.12217305F;

		gunModel[11].addBox(-15F, -63F, -3F, 3, 3, 6, 0F); // Box 12
		gunModel[11].setRotationPoint(5F, 11F, 0F);
		gunModel[11].rotateAngleZ = 0.12217305F;

		gunModel[12].addBox(-9F, -68F, -3F, 3, 3, 6, 0F); // Box 13
		gunModel[12].setRotationPoint(5F, 11F, 0F);
		gunModel[12].rotateAngleZ = 0.12217305F;

		gunModel[13].addBox(-15F, -70F, -3F, 9, 2, 6, 0F); // Box 14
		gunModel[13].setRotationPoint(5F, 11F, 0F);
		gunModel[13].rotateAngleZ = 0.12217305F;

		gunModel[14].addBox(-15F, -68F, -3F, 3, 3, 6, 0F); // Box 15
		gunModel[14].setRotationPoint(5F, 11F, 0F);
		gunModel[14].rotateAngleZ = 0.12217305F;

		gunModel[15].addBox(-9F, -73F, -3F, 3, 3, 6, 0F); // Box 16
		gunModel[15].setRotationPoint(5F, 11F, 0F);
		gunModel[15].rotateAngleZ = 0.12217305F;

		gunModel[16].addBox(-15F, -75F, -3F, 9, 2, 6, 0F); // Box 17
		gunModel[16].setRotationPoint(5F, 11F, 0F);
		gunModel[16].rotateAngleZ = 0.12217305F;

		gunModel[17].addBox(-15F, -73F, -3F, 3, 3, 6, 0F); // Box 18
		gunModel[17].setRotationPoint(5F, 11F, 0F);
		gunModel[17].rotateAngleZ = 0.12217305F;

		gunModel[18].addShapeBox(-14F, -79F, -3F, 8, 4, 6, 0F, 0F, 6F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 6F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 19
		gunModel[18].setRotationPoint(5F, 11F, 0F);
		gunModel[18].rotateAngleZ = 0.12217305F;

		gunModel[19].addBox(-15F, -85F, -3F, 1, 10, 6, 0F); // Box 20
		gunModel[19].setRotationPoint(5F, 11F, 0F);
		gunModel[19].rotateAngleZ = 0.12217305F;

		gunModel[20].addShapeBox(-12F, -22F, -3F, 9, 29, 6, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F); // Box 22
		gunModel[20].setRotationPoint(5F, 11F, 0F);
		gunModel[20].rotateAngleZ = 0.12217305F;

		gunModel[21].addShapeBox(-14F, -22F, -3F, 2, 29, 6, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 23
		gunModel[21].setRotationPoint(5F, 11F, 0F);
		gunModel[21].rotateAngleZ = 0.12217305F;

		gunModel[22].addShapeBox(-2F, -22F, -3F, 2, 6, 6, 0F, 0F, 0F, 0F, 1F, 0F, -1F, 1F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 24
		gunModel[22].setRotationPoint(5F, 11F, 0F);
		gunModel[22].rotateAngleZ = 0.12217305F;

		gunModel[23].addShapeBox(-2F, 1F, -3F, 2, 6, 6, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 27
		gunModel[23].setRotationPoint(5F, 11F, 0F);
		gunModel[23].rotateAngleZ = 0.12217305F;

		gunModel[24].addShapeBox(-2F, -16F, -3F, 2, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -1F, -1F, 0F, -1F, 0F, 0F, 0F); // Box 28
		gunModel[24].setRotationPoint(5F, 11F, 0F);
		gunModel[24].rotateAngleZ = 0.12217305F;

		gunModel[25].addShapeBox(-2F, -1F, -3F, 2, 2, 6, 0F, 0F, 0F, 0F, -1F, 0F, -1F, -1F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 29
		gunModel[25].setRotationPoint(5F, 11F, 0F);
		gunModel[25].rotateAngleZ = 0.12217305F;

		gunModel[26].addShapeBox(-2F, -14F, -3F, 1, 13, 6, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 30
		gunModel[26].setRotationPoint(5F, 11F, 0F);
		gunModel[26].rotateAngleZ = 0.12217305F;

		gunModel[27].addBox(-15F, -41F, -3.5F, 8, 16, 7, 0F); // Box 31
		gunModel[27].setRotationPoint(5F, 11F, 0F);
		gunModel[27].rotateAngleZ = 0.12217305F;

		gunModel[28].addShapeBox(-16F, -41F, -3.5F, 1, 16, 7, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 32
		gunModel[28].setRotationPoint(5F, 11F, 0F);
		gunModel[28].rotateAngleZ = 0.12217305F;

		gunModel[29].addShapeBox(-12F, -39F, 3F, 1, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 33
		gunModel[29].setRotationPoint(5F, 11F, 0F);
		gunModel[29].rotateAngleZ = 0.12217305F;

		gunModel[30].addShapeBox(-10F, -29F, 4F, 1, 9, 1, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 34
		gunModel[30].setRotationPoint(5F, 11F, 0F);
		gunModel[30].rotateAngleZ = 0.12217305F;

		gunModel[31].addShapeBox(-10F, -20F, 3F, 1, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 35
		gunModel[31].setRotationPoint(5F, 11F, 0F);
		gunModel[31].rotateAngleZ = 0.12217305F;

		gunModel[32].addBox(-12F, -38F, 4F, 1, 9, 1, 0F); // Box 36
		gunModel[32].setRotationPoint(5F, 11F, 0F);
		gunModel[32].rotateAngleZ = 0.12217305F;

		gunModel[33].addShapeBox(-13F, -40F, -4F, 10, 14, 8, 0F, 0F, 0F, 0F, -5F, 0F, 0F, -5F, 0F, 0F, 0F, 0F, 0F, 0F, -7F, 0F, -5F, -7F, 0F, -5F, -7F, 0F, 0F, -7F, 0F); // Box 37
		gunModel[33].setRotationPoint(5F, 11F, 0F);
		gunModel[33].rotateAngleZ = 0.12217305F;

		gunModel[34].addShapeBox(-14F, 7F, -3F, 2, 1, 6, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -2F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -2F); // Box 38
		gunModel[34].setRotationPoint(5F, 11F, 0F);
		gunModel[34].rotateAngleZ = 0.12217305F;

		gunModel[35].addShapeBox(-2F, 7F, -3F, 2, 1, 6, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -1F); // Box 39
		gunModel[35].setRotationPoint(5F, 11F, 0F);
		gunModel[35].rotateAngleZ = 0.12217305F;

		gunModel[36].addShapeBox(-12F, 7F, -3F, 10, 1, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 40
		gunModel[36].setRotationPoint(5F, 11F, 0F);
		gunModel[36].rotateAngleZ = 0.12217305F;



		translateAll(0F, 0F, 0F);


		flipAll();
	}
}