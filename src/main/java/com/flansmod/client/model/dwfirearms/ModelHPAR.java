package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelHPAR extends ModelGun
{
	int textureX = 512;
	int textureY = 256;

	public ModelHPAR()
	{
		gunModel = new ModelRendererTurbo[88];
		gunModel[0] = new ModelRendererTurbo(this, 227, 140, textureX, textureY); // barrelMainLeft
		gunModel[1] = new ModelRendererTurbo(this, 227, 149, textureX, textureY); // barrelMainMiddle
		gunModel[2] = new ModelRendererTurbo(this, 227, 140, textureX, textureY); // barrelMainRight
		gunModel[3] = new ModelRendererTurbo(this, 112, 120, textureX, textureY); // body1
		gunModel[4] = new ModelRendererTurbo(this, 200, 141, textureX, textureY); // body6
		gunModel[5] = new ModelRendererTurbo(this, 185, 95, textureX, textureY); // Import GU,body8
		gunModel[6] = new ModelRendererTurbo(this, 227, 158, textureX, textureY); // Import GU,body9
		gunModel[7] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // railPart3
		gunModel[8] = new ModelRendererTurbo(this, 79, 11, textureX, textureY); // railPart1
		gunModel[9] = new ModelRendererTurbo(this, 58, 11, textureX, textureY); // railPart12
		gunModel[10] = new ModelRendererTurbo(this, 79, 11, textureX, textureY); // railPart2
		gunModel[11] = new ModelRendererTurbo(this, 79, 11, textureX, textureY); // railPart3
		gunModel[12] = new ModelRendererTurbo(this, 79, 11, textureX, textureY); // railPart4
		gunModel[13] = new ModelRendererTurbo(this, 79, 11, textureX, textureY); // railPart5
		gunModel[14] = new ModelRendererTurbo(this, 58, 11, textureX, textureY); // railPart10
		gunModel[15] = new ModelRendererTurbo(this, 58, 11, textureX, textureY); // railPart11
		gunModel[16] = new ModelRendererTurbo(this, 311, 217, textureX, textureY); // stockPart1
		gunModel[17] = new ModelRendererTurbo(this, 227, 232, textureX, textureY); // stockPart7
		gunModel[18] = new ModelRendererTurbo(this, 1, 81, textureX, textureY); // Box 32
		gunModel[19] = new ModelRendererTurbo(this, 40, 81, textureX, textureY); // Box 33
		gunModel[20] = new ModelRendererTurbo(this, 1, 27, textureX, textureY); // Box 34
		gunModel[21] = new ModelRendererTurbo(this, 1, 39, textureX, textureY); // Box 35
		gunModel[22] = new ModelRendererTurbo(this, 1, 70, textureX, textureY); // Box 38
		gunModel[23] = new ModelRendererTurbo(this, 40, 27, textureX, textureY); // Box 39
		gunModel[24] = new ModelRendererTurbo(this, 38, 39, textureX, textureY); // Box 40
		gunModel[25] = new ModelRendererTurbo(this, 36, 70, textureX, textureY); // Box 42
		gunModel[26] = new ModelRendererTurbo(this, 112, 148, textureX, textureY); // Box 0
		gunModel[27] = new ModelRendererTurbo(this, 112, 133, textureX, textureY); // Box 1
		gunModel[28] = new ModelRendererTurbo(this, 342, 141, textureX, textureY); // Box 3
		gunModel[29] = new ModelRendererTurbo(this, 254, 170, textureX, textureY); // Box 4
		gunModel[30] = new ModelRendererTurbo(this, 112, 175, textureX, textureY); // Box 6
		gunModel[31] = new ModelRendererTurbo(this, 112, 161, textureX, textureY); // Box 7
		gunModel[32] = new ModelRendererTurbo(this, 112, 190, textureX, textureY); // Box 8
		gunModel[33] = new ModelRendererTurbo(this, 112, 87, textureX, textureY); // Box 9
		gunModel[34] = new ModelRendererTurbo(this, 112, 97, textureX, textureY); // Box 10
		gunModel[35] = new ModelRendererTurbo(this, 161, 61, textureX, textureY); // Box 11
		gunModel[36] = new ModelRendererTurbo(this, 112, 76, textureX, textureY); // Box 12
		gunModel[37] = new ModelRendererTurbo(this, 145, 190, textureX, textureY); // Box 20
		gunModel[38] = new ModelRendererTurbo(this, 145, 161, textureX, textureY); // Box 21
		gunModel[39] = new ModelRendererTurbo(this, 145, 175, textureX, textureY); // Box 22
		gunModel[40] = new ModelRendererTurbo(this, 112, 45, textureX, textureY); // Box 23
		gunModel[41] = new ModelRendererTurbo(this, 1, 11, textureX, textureY); // Box 24
		gunModel[42] = new ModelRendererTurbo(this, 58, 11, textureX, textureY); // Box 25
		gunModel[43] = new ModelRendererTurbo(this, 191, 141, textureX, textureY); // Box 28
		gunModel[44] = new ModelRendererTurbo(this, 184, 64, textureX, textureY); // Box 29
		gunModel[45] = new ModelRendererTurbo(this, 184, 54, textureX, textureY); // Box 30
		gunModel[46] = new ModelRendererTurbo(this, 1, 93, textureX, textureY); // Box 32
		gunModel[47] = new ModelRendererTurbo(this, 173, 151, textureX, textureY); // Box 33
		gunModel[48] = new ModelRendererTurbo(this, 198, 151, textureX, textureY); // Box 34
		gunModel[49] = new ModelRendererTurbo(this, 80, 61, textureX, textureY); // Box 38
		gunModel[50] = new ModelRendererTurbo(this, 71, 62, textureX, textureY); // Box 39
		gunModel[51] = new ModelRendererTurbo(this, 71, 62, textureX, textureY); // Box 40
		gunModel[52] = new ModelRendererTurbo(this, 26, 94, textureX, textureY); // Box 45
		gunModel[53] = new ModelRendererTurbo(this, 79, 11, textureX, textureY); // Box 49
		gunModel[54] = new ModelRendererTurbo(this, 79, 11, textureX, textureY); // Box 50
		gunModel[55] = new ModelRendererTurbo(this, 181, 133, textureX, textureY); // Box 58
		gunModel[56] = new ModelRendererTurbo(this, 260, 191, textureX, textureY); // Box 88
		gunModel[57] = new ModelRendererTurbo(this, 289, 203, textureX, textureY); // Box 89
		gunModel[58] = new ModelRendererTurbo(this, 61, 29, textureX, textureY); // Box 94
		gunModel[59] = new ModelRendererTurbo(this, 71, 62, textureX, textureY); // Box 118
		gunModel[60] = new ModelRendererTurbo(this, 80, 61, textureX, textureY); // Box 119
		gunModel[61] = new ModelRendererTurbo(this, 71, 62, textureX, textureY); // Box 120
		gunModel[62] = new ModelRendererTurbo(this, 112, 33, textureX, textureY); // Box 121
		gunModel[63] = new ModelRendererTurbo(this, 112, 242, textureX, textureY); // Box 128
		gunModel[64] = new ModelRendererTurbo(this, 1, 55, textureX, textureY); // Box 131
		gunModel[65] = new ModelRendererTurbo(this, 38, 55, textureX, textureY); // Box 132
		gunModel[66] = new ModelRendererTurbo(this, 196, 162, textureX, textureY); // Box 133
		gunModel[67] = new ModelRendererTurbo(this, 196, 162, textureX, textureY); // Box 134
		gunModel[68] = new ModelRendererTurbo(this, 196, 162, textureX, textureY); // Box 135
		gunModel[69] = new ModelRendererTurbo(this, 173, 45, textureX, textureY); // Box 136
		gunModel[70] = new ModelRendererTurbo(this, 26, 94, textureX, textureY); // Box 2
		gunModel[71] = new ModelRendererTurbo(this, 26, 94, textureX, textureY); // Box 3
		gunModel[72] = new ModelRendererTurbo(this, 331, 173, textureX, textureY); // Box 136
		gunModel[73] = new ModelRendererTurbo(this, 363, 144, textureX, textureY); // Box 137
		gunModel[74] = new ModelRendererTurbo(this, 227, 170, textureX, textureY); // Box 141
		gunModel[75] = new ModelRendererTurbo(this, 308, 158, textureX, textureY); // Box 142
		gunModel[76] = new ModelRendererTurbo(this, 227, 183, textureX, textureY); // Box 150
		gunModel[77] = new ModelRendererTurbo(this, 316, 232, textureX, textureY); // Box 2
		gunModel[78] = new ModelRendererTurbo(this, 280, 220, textureX, textureY); // Box 6
		gunModel[79] = new ModelRendererTurbo(this, 56, 243, textureX, textureY); // Box 8
		gunModel[80] = new ModelRendererTurbo(this, 289, 191, textureX, textureY); // Box 9
		gunModel[81] = new ModelRendererTurbo(this, 318, 205, textureX, textureY); // Box 10
		gunModel[82] = new ModelRendererTurbo(this, 227, 220, textureX, textureY); // Box 14
		gunModel[83] = new ModelRendererTurbo(this, 56, 232, textureX, textureY); // Box 15
		gunModel[84] = new ModelRendererTurbo(this, 227, 194, textureX, textureY); // Box 16
		gunModel[85] = new ModelRendererTurbo(this, 1, 243, textureX, textureY); // Box 17
		gunModel[86] = new ModelRendererTurbo(this, 318, 193, textureX, textureY); // Box 18
		gunModel[87] = new ModelRendererTurbo(this, 1, 232, textureX, textureY); // Box 19

		gunModel[0].addShapeBox(0F, 0F, 0F, 55, 6, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F); // barrelMainLeft
		gunModel[0].setRotationPoint(39F, -17F, 1F);

		gunModel[1].addBox(0F, 0F, 0F, 55, 6, 2, 0F); // barrelMainMiddle
		gunModel[1].setRotationPoint(39F, -17F, -1F);

		gunModel[2].addShapeBox(0F, 0F, 0F, 55, 6, 2, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // barrelMainRight
		gunModel[2].setRotationPoint(39F, -17F, -3F);

		gunModel[3].addShapeBox(0F, 0F, 0F, 47, 2, 9, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body1
		gunModel[3].setRotationPoint(-14F, -21F, -4.5F);

		gunModel[4].addShapeBox(0F, 0F, 0F, 4, 4, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, 0F, 0F, 0F); // body6
		gunModel[4].setRotationPoint(4F, -16F, -5F);

		gunModel[5].addShapeBox(0F, 0F, 0F, 4, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F); // Import GU,body8
		gunModel[5].setRotationPoint(-14F, -8F, -4F);

		gunModel[6].addBox(0F, 0F, 0F, 30, 1, 10, 0F); // Import GU,body9
		gunModel[6].setRotationPoint(-14F, -17F, -5F);

		gunModel[7].addBox(0F, 0F, 0F, 39, 2, 7, 0F); // railPart3
		gunModel[7].setRotationPoint(-7F, -22F, -3.5F);

		gunModel[8].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // railPart1
		gunModel[8].setRotationPoint(5F, -24F, -3.5F);

		gunModel[9].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // railPart12
		gunModel[9].setRotationPoint(53F, -2F, -3.5F);

		gunModel[10].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // railPart2
		gunModel[10].setRotationPoint(11F, -24F, -3.5F);

		gunModel[11].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // railPart3
		gunModel[11].setRotationPoint(17F, -24F, -3.5F);

		gunModel[12].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // railPart4
		gunModel[12].setRotationPoint(23F, -24F, -3.5F);

		gunModel[13].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // railPart5
		gunModel[13].setRotationPoint(29F, -24F, -3.5F);

		gunModel[14].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // railPart10
		gunModel[14].setRotationPoint(41F, -2F, -3.5F);

		gunModel[15].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // railPart11
		gunModel[15].setRotationPoint(47F, -2F, -3.5F);

		gunModel[16].addBox(0F, 0F, 0F, 10, 5, 9, 0F); // stockPart1
		gunModel[16].setRotationPoint(-24F, -17F, -4.5F);

		gunModel[17].addShapeBox(0F, 0F, 0F, 35, 2, 9, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // stockPart7
		gunModel[17].setRotationPoint(-49F, -19F, -4.5F);

		gunModel[18].addShapeBox(0F, 0F, 0F, 11, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F); // Box 32
		gunModel[18].setRotationPoint(-11F, -5F, -4F);

		gunModel[19].addBox(0F, 0F, 0F, 6, 3, 8, 0F); // Box 33
		gunModel[19].setRotationPoint(0F, -5F, -4F);

		gunModel[20].addShapeBox(0F, 0F, 0F, 10, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 34
		gunModel[20].setRotationPoint(-10F, -2F, -4F);

		gunModel[21].addShapeBox(0F, 0F, 0F, 10, 7, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 4F, 0F, 0F); // Box 35
		gunModel[21].setRotationPoint(-10F, 1F, -4F);

		gunModel[22].addShapeBox(0F, 0F, 0F, 9, 2, 8, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F); // Box 38
		gunModel[22].setRotationPoint(-16F, 14F, -4F);

		gunModel[23].addShapeBox(0F, 0F, 0F, 2, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F); // Box 39
		gunModel[23].setRotationPoint(0F, -2F, -4F);

		gunModel[24].addShapeBox(0F, 0F, 0F, 2, 7, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 4F, 0F, 0F, -4F, 0F, -1.5F, -4F, 0F, -1.5F, 4F, 0F, 0F); // Box 40
		gunModel[24].setRotationPoint(0F, 1F, -4F);

		gunModel[25].addShapeBox(0F, 0F, 0F, 2, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F); // Box 42
		gunModel[25].setRotationPoint(-7F, 14F, -4F);

		gunModel[26].addBox(0F, 0F, 0F, 22, 4, 8, 0F); // Box 0
		gunModel[26].setRotationPoint(-14F, -12F, -4F);

		gunModel[27].addShapeBox(0F, 0F, 0F, 29, 4, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F); // Box 1
		gunModel[27].setRotationPoint(4F, -12F, -5F);

		gunModel[28].addBox(0F, 0F, 0F, 2, 8, 8, 0F); // Box 3
		gunModel[28].setRotationPoint(14F, -5F, -4F);

		gunModel[29].addBox(0F, 0F, 0F, 30, 4, 8, 0F); // Box 4
		gunModel[29].setRotationPoint(-14F, -16F, -3F);

		gunModel[30].addBox(0F, 0F, 0F, 6, 4, 10, 0F); // Box 6
		gunModel[30].setRotationPoint(34F, -16F, -5F);

		gunModel[31].addShapeBox(0F, 0F, 0F, 6, 3, 10, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 7
		gunModel[31].setRotationPoint(34F, -19F, -5F);

		gunModel[32].addShapeBox(0F, 0F, 0F, 6, 3, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 8
		gunModel[32].setRotationPoint(34F, -12F, -5F);

		gunModel[33].addShapeBox(0F, 0F, 0F, 29, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 9
		gunModel[33].setRotationPoint(34F, -21F, -3.5F);

		gunModel[34].addBox(0F, 0F, 0F, 29, 2, 7, 0F); // Box 10
		gunModel[34].setRotationPoint(34F, -19F, -3.5F);

		gunModel[35].addShapeBox(0F, 0F, 0F, 3, 6, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F); // Box 11
		gunModel[35].setRotationPoint(30F, -5F, -4F);

		gunModel[36].addBox(0F, 0F, 0F, 29, 3, 7, 0F); // Box 12
		gunModel[36].setRotationPoint(34F, -11F, -3.5F);

		gunModel[37].addShapeBox(0F, 0F, 0F, 15, 3, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 20
		gunModel[37].setRotationPoint(57F, -12F, -5F);

		gunModel[38].addShapeBox(0F, 0F, 0F, 15, 3, 10, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 21
		gunModel[38].setRotationPoint(57F, -19F, -5F);

		gunModel[39].addBox(0F, 0F, 0F, 15, 4, 10, 0F); // Box 22
		gunModel[39].setRotationPoint(57F, -16F, -5F);

		gunModel[40].addBox(0F, 0F, 0F, 22, 4, 8, 0F); // Box 23
		gunModel[40].setRotationPoint(34F, -8F, -4F);

		gunModel[41].addShapeBox(0F, 0F, 0F, 21, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 24
		gunModel[41].setRotationPoint(35F, -4F, -3.5F);

		gunModel[42].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 25
		gunModel[42].setRotationPoint(35F, -2F, -3.5F);

		gunModel[43].addShapeBox(0F, 0F, 0F, 2, 4, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 28
		gunModel[43].setRotationPoint(31F, -16F, -5F);

		gunModel[44].addShapeBox(0F, 0F, 0F, 4, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F); // Box 29
		gunModel[44].setRotationPoint(56F, -7F, -4F);

		gunModel[45].addBox(0F, 0F, 0F, 4, 1, 8, 0F); // Box 30
		gunModel[45].setRotationPoint(56F, -8F, -4F);

		gunModel[46].addBox(0F, 0F, 0F, 5, 11, 7, 0F); // Box 32
		gunModel[46].setRotationPoint(63F, -19F, -3.5F);

		gunModel[47].addShapeBox(0F, 0F, 0F, 5, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 33
		gunModel[47].setRotationPoint(63F, -21F, -3.5F);

		gunModel[48].addShapeBox(0F, 0F, 0F, 8, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 34
		gunModel[48].setRotationPoint(60F, -8F, -3.5F);

		gunModel[49].addBox(0F, 0F, 0F, 2, 7, 3, 0F); // Box 38
		gunModel[49].setRotationPoint(72F, -17.5F, -1.5F);

		gunModel[50].addShapeBox(0F, 0F, 0F, 2, 7, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F); // Box 39
		gunModel[50].setRotationPoint(72F, -17.5F, 1.5F);

		gunModel[51].addShapeBox(0F, 0F, 0F, 2, 7, 2, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 40
		gunModel[51].setRotationPoint(72F, -17.5F, -3.5F);

		gunModel[52].addShapeBox(0F, 0F, 0F, 8, 6, 2, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 45
		gunModel[52].setRotationPoint(7F, -17F, -4F);

		gunModel[53].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 49
		gunModel[53].setRotationPoint(-7F, -24F, -3.5F);

		gunModel[54].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 50
		gunModel[54].setRotationPoint(-1F, -24F, -3.5F);

		gunModel[55].addBox(0F, 0F, 0F, 18, 4, 2, 0F); // Box 58
		gunModel[55].setRotationPoint(-14F, -16F, -5F);

		gunModel[56].addBox(0F, 0F, 0F, 5, 19, 9, 0F); // Box 88
		gunModel[56].setRotationPoint(-54F, -17F, -4.5F);

		gunModel[57].addShapeBox(0F, 0F, 0F, 5, 2, 9, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 89
		gunModel[57].setRotationPoint(-54F, -19F, -4.5F);

		gunModel[58].addBox(0F, 0F, 0F, 6, 3, 6, 0F); // Box 94
		gunModel[58].setRotationPoint(-13F, -24F, -3F);

		gunModel[59].addShapeBox(0F, 0F, 0F, 2, 7, 2, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 118
		gunModel[59].setRotationPoint(76F, -17.5F, -3.5F);

		gunModel[60].addBox(0F, 0F, 0F, 2, 7, 3, 0F); // Box 119
		gunModel[60].setRotationPoint(76F, -17.5F, -1.5F);

		gunModel[61].addShapeBox(0F, 0F, 0F, 2, 7, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F); // Box 120
		gunModel[61].setRotationPoint(76F, -17.5F, 1.5F);

		gunModel[62].addBox(0F, 0F, 0F, 43, 3, 8, 0F); // Box 121
		gunModel[62].setRotationPoint(-10F, -8F, -4F);

		gunModel[63].addBox(0F, 0F, 0F, 47, 2, 9, 0F); // Box 128
		gunModel[63].setRotationPoint(-14F, -19F, -4.5F);

		gunModel[64].addShapeBox(0F, 0F, 0F, 10, 6, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, -3F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 2F, -3F, 0F); // Box 131
		gunModel[64].setRotationPoint(-14F, 8F, -4F);

		gunModel[65].addShapeBox(0F, 0F, 0F, 2, 6, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, -1.5F, -3F, 0F, -1.5F, 3F, 0F, 0F); // Box 132
		gunModel[65].setRotationPoint(-4F, 8F, -4F);

		gunModel[66].addShapeBox(0F, 0F, 0F, 1, 3, 9, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 133
		gunModel[66].setRotationPoint(33F, -18.5F, -4.5F);

		gunModel[67].addBox(0F, 0F, 0F, 1, 3, 9, 0F); // Box 134
		gunModel[67].setRotationPoint(33F, -15.5F, -4.5F);

		gunModel[68].addShapeBox(0F, 0F, 0F, 1, 3, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 135
		gunModel[68].setRotationPoint(33F, -12.5F, -4.5F);

		gunModel[69].addBox(0F, 0F, 0F, 1, 7, 6, 0F); // Box 136
		gunModel[69].setRotationPoint(33F, -11.5F, -3F);

		gunModel[70].addShapeBox(0F, 0F, 0F, 8, 6, 2, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 2
		gunModel[70].setRotationPoint(15F, -17F, -4F);

		gunModel[71].addShapeBox(0F, 0F, 0F, 8, 6, 2, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 3
		gunModel[71].setRotationPoint(23F, -17F, -4F);

		gunModel[72].addShapeBox(0F, 0F, 0F, 14, 8, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F); // Box 136
		gunModel[72].setRotationPoint(16F, -5F, -4F);

		gunModel[73].addBox(0F, 0F, 0F, 1, 6, 7, 0F); // Box 137
		gunModel[73].setRotationPoint(29F, -5F, -3F);

		gunModel[74].addBox(0F, 0F, 0F, 5, 4, 8, 0F); // Box 141
		gunModel[74].setRotationPoint(28F, -16F, -3F);

		gunModel[75].addBox(0F, 0F, 0F, 17, 1, 10, 0F); // Box 142
		gunModel[75].setRotationPoint(16F, -17F, -5F);

		gunModel[76].addBox(0F, 0F, 0F, 12, 4, 6, 0F); // Box 150
		gunModel[76].setRotationPoint(16F, -16F, -3F);

		gunModel[77].addBox(0F, 0F, 0F, 10, 3, 8, 0F); // Box 2
		gunModel[77].setRotationPoint(-24F, -12F, -4F);

		gunModel[78].addBox(0F, 0F, 0F, 7, 3, 8, 0F); // Box 6
		gunModel[78].setRotationPoint(-49F, -1F, -4F);

		gunModel[79].addShapeBox(0F, 0F, 0F, 18, 1, 8, 0F, 0F, -11F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -11F, 0F, 0F, 11F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 11F, -1F); // Box 8
		gunModel[79].setRotationPoint(-42F, -9F, -4F);

		gunModel[80].addShapeBox(0F, 0F, 0F, 5, 2, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, -1F, 0F, -1F); // Box 9
		gunModel[80].setRotationPoint(-54F, 2F, -4.5F);

		gunModel[81].addShapeBox(0F, 0F, 0F, 10, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 10
		gunModel[81].setRotationPoint(-24F, -9F, -4F);

		gunModel[82].addShapeBox(0F, 0F, 0F, 18, 3, 8, 0F, 0F, -11F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -11F, 0F, 0F, 11F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 11F, 0F); // Box 14
		gunModel[82].setRotationPoint(-42F, -12F, -4F);

		gunModel[83].addShapeBox(0F, 0F, 0F, 18, 1, 9, 0F, 0F, -11F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -11F, 0F, 0F, 11F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 11F, 0F); // Box 15
		gunModel[83].setRotationPoint(-42F, -13F, -4.5F);

		gunModel[84].addBox(0F, 0F, 0F, 7, 16, 9, 0F); // Box 16
		gunModel[84].setRotationPoint(-49F, -17F, -4.5F);

		gunModel[85].addBox(0F, 0F, 0F, 18, 3, 9, 0F); // Box 17
		gunModel[85].setRotationPoint(-42F, -17F, -4.5F);

		gunModel[86].addShapeBox(0F, 0F, 0F, 7, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 18
		gunModel[86].setRotationPoint(-49F, 2F, -4F);

		gunModel[87].addShapeBox(0F, 0F, 0F, 18, 1, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 11F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 11F, 0F); // Box 19
		gunModel[87].setRotationPoint(-42F, -14F, -4.5F);


		defaultBarrelModel = new ModelRendererTurbo[3];
		defaultBarrelModel[0] = new ModelRendererTurbo(this, 71, 72, textureX, textureY); // Box 41
		defaultBarrelModel[1] = new ModelRendererTurbo(this, 71, 83, textureX, textureY); // Box 42
		defaultBarrelModel[2] = new ModelRendererTurbo(this, 71, 83, textureX, textureY); // Box 43

		defaultBarrelModel[0].addBox(0F, 0F, 0F, 12, 7, 3, 0F); // Box 41
		defaultBarrelModel[0].setRotationPoint(94F, -17.5F, -1.5F);

		defaultBarrelModel[1].addShapeBox(0F, 0F, 0F, 12, 7, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F); // Box 42
		defaultBarrelModel[1].setRotationPoint(94F, -17.5F, 1.5F);

		defaultBarrelModel[2].addShapeBox(0F, 0F, 0F, 12, 7, 2, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 43
		defaultBarrelModel[2].setRotationPoint(94F, -17.5F, -3.5F);


		defaultScopeModel = new ModelRendererTurbo[21];
		defaultScopeModel[0] = new ModelRendererTurbo(this, 80, 39, textureX, textureY); // Box 106
		defaultScopeModel[1] = new ModelRendererTurbo(this, 64, 23, textureX, textureY); // Box 107
		defaultScopeModel[2] = new ModelRendererTurbo(this, 86, 29, textureX, textureY); // Box 108
		defaultScopeModel[3] = new ModelRendererTurbo(this, 95, 22, textureX, textureY); // Box 110
		defaultScopeModel[4] = new ModelRendererTurbo(this, 73, 23, textureX, textureY); // Box 111
		defaultScopeModel[5] = new ModelRendererTurbo(this, 73, 26, textureX, textureY); // Box 112
		defaultScopeModel[6] = new ModelRendererTurbo(this, 84, 22, textureX, textureY); // Box 113
		defaultScopeModel[7] = new ModelRendererTurbo(this, 75, 44, textureX, textureY); // Box 115
		defaultScopeModel[8] = new ModelRendererTurbo(this, 68, 40, textureX, textureY); // Box 116
		defaultScopeModel[9] = new ModelRendererTurbo(this, 61, 39, textureX, textureY); // Box 117
		defaultScopeModel[10] = new ModelRendererTurbo(this, 71, 44, textureX, textureY); // Box 118
		defaultScopeModel[11] = new ModelRendererTurbo(this, 61, 44, textureX, textureY); // Box 119
		defaultScopeModel[12] = new ModelRendererTurbo(this, 66, 44, textureX, textureY); // Box 120
		defaultScopeModel[13] = new ModelRendererTurbo(this, 53, 22, textureX, textureY); // Box 93
		defaultScopeModel[14] = new ModelRendererTurbo(this, 37, 26, textureX, textureY); // Box 95
		defaultScopeModel[15] = new ModelRendererTurbo(this, 61, 48, textureX, textureY); // Box 0
		defaultScopeModel[16] = new ModelRendererTurbo(this, 80, 39, textureX, textureY); // Box 1
		defaultScopeModel[17] = new ModelRendererTurbo(this, 93, 44, textureX, textureY); // Box 4
		defaultScopeModel[18] = new ModelRendererTurbo(this, 93, 44, textureX, textureY); // Box 6
		defaultScopeModel[19] = new ModelRendererTurbo(this, 80, 44, textureX, textureY); // Box 7
		defaultScopeModel[20] = new ModelRendererTurbo(this, 80, 44, textureX, textureY); // Box 8

		defaultScopeModel[0].addShapeBox(0F, 0F, 0F, 7, 1, 3, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 106
		defaultScopeModel[0].setRotationPoint(63F, -27F, -1.5F);

		defaultScopeModel[1].addShapeBox(0F, 0F, 0F, 2, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F); // Box 107
		defaultScopeModel[1].setRotationPoint(67F, -31F, -0.5F);

		defaultScopeModel[2].addShapeBox(0F, 0F, 0F, 4, 3, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F); // Box 108
		defaultScopeModel[2].setRotationPoint(-11F, -27F, -3F);

		defaultScopeModel[3].addBox(0F, 0F, 0F, 4, 5, 1, 0F); // Box 110
		defaultScopeModel[3].setRotationPoint(-11F, -34F, 2F);

		defaultScopeModel[4].addShapeBox(0F, 0F, 0F, 4, 1, 1, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 111
		defaultScopeModel[4].setRotationPoint(-11F, -35F, 2F);

		defaultScopeModel[5].addShapeBox(0F, 0F, 0F, 4, 1, 1, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 112
		defaultScopeModel[5].setRotationPoint(-11F, -35F, -3F);

		defaultScopeModel[6].addBox(0F, 0F, 0F, 4, 5, 1, 0F); // Box 113
		defaultScopeModel[6].setRotationPoint(-11F, -34F, -3F);

		defaultScopeModel[7].addShapeBox(0F, 0F, 0F, 1, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F, 1F); // Box 115
		defaultScopeModel[7].setRotationPoint(-10F, -32F, -2F);

		defaultScopeModel[8].addBox(0F, 0F, 0F, 1, 1, 2, 0F); // Box 116
		defaultScopeModel[8].setRotationPoint(-10F, -34F, -1F);

		defaultScopeModel[9].addBox(0F, 0F, 0F, 1, 2, 2, 0F); // Box 117
		defaultScopeModel[9].setRotationPoint(-10F, -31F, -1F);

		defaultScopeModel[10].addShapeBox(0F, 0F, 0F, 1, 2, 1, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 118
		defaultScopeModel[10].setRotationPoint(-10F, -34F, -2F);

		defaultScopeModel[11].addShapeBox(0F, 0F, 0F, 1, 2, 1, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 119
		defaultScopeModel[11].setRotationPoint(-10F, -34F, 1F);

		defaultScopeModel[12].addShapeBox(0F, 0F, 0F, 1, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 120
		defaultScopeModel[12].setRotationPoint(-10F, -32F, 1F);

		defaultScopeModel[13].addBox(0F, 0F, 0F, 2, 9, 3, 0F); // Box 93
		defaultScopeModel[13].setRotationPoint(68F, -25F, -1.5F);

		defaultScopeModel[14].addShapeBox(0F, 0F, 0F, 2, 5, 3, 0F, -4F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 95
		defaultScopeModel[14].setRotationPoint(59F, -26F, -1.5F);

		defaultScopeModel[15].addBox(0F, 0F, 0F, 4, 2, 6, 0F); // Box 0
		defaultScopeModel[15].setRotationPoint(-11F, -29F, -3F);

		defaultScopeModel[16].addBox(0F, 0F, 0F, 7, 1, 3, 0F); // Box 1
		defaultScopeModel[16].setRotationPoint(63F, -26F, -1.5F);

		defaultScopeModel[17].addShapeBox(0F, 0F, 0F, 4, 2, 2, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 4
		defaultScopeModel[17].setRotationPoint(-11F, -29F, -4.5F);

		defaultScopeModel[18].addShapeBox(0F, 0F, 0F, 4, 2, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 6
		defaultScopeModel[18].setRotationPoint(-11F, -27F, -4.5F);

		defaultScopeModel[19].addShapeBox(0F, 0F, 0F, 4, 2, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 7
		defaultScopeModel[19].setRotationPoint(-11F, -27F, 2.5F);

		defaultScopeModel[20].addShapeBox(0F, 0F, 0F, 4, 2, 2, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 8
		defaultScopeModel[20].setRotationPoint(-11F, -29F, 2.5F);


		ammoModel = new ModelRendererTurbo[13];
		ammoModel[0] = new ModelRendererTurbo(this, 301, 6, textureX, textureY); // clipPart1
		ammoModel[1] = new ModelRendererTurbo(this, 389, 10, textureX, textureY); // clipPart2
		ammoModel[2] = new ModelRendererTurbo(this, 340, 6, textureX, textureY); // clipPart3
		ammoModel[3] = new ModelRendererTurbo(this, 386, 69, textureX, textureY); // Box 9
		ammoModel[4] = new ModelRendererTurbo(this, 326, 40, textureX, textureY); // Box 133
		ammoModel[5] = new ModelRendererTurbo(this, 341, 43, textureX, textureY); // Box 134
		ammoModel[6] = new ModelRendererTurbo(this, 301, 40, textureX, textureY); // Box 135
		ammoModel[7] = new ModelRendererTurbo(this, 337, 73, textureX, textureY); // Box 138
		ammoModel[8] = new ModelRendererTurbo(this, 389, 50, textureX, textureY); // Box 139
		ammoModel[9] = new ModelRendererTurbo(this, 389, 54, textureX, textureY); // Box 140
		ammoModel[10] = new ModelRendererTurbo(this, 356, 43, textureX, textureY); // Box 144
		ammoModel[11] = new ModelRendererTurbo(this, 356, 53, textureX, textureY); // Box 147
		ammoModel[12] = new ModelRendererTurbo(this, 389, 44, textureX, textureY); // Box 148

		ammoModel[0].addBox(0F, 0F, 0F, 3, 17, 16, 0F); // clipPart1
		ammoModel[0].setRotationPoint(15F, -5F, -3F);

		ammoModel[1].addBox(0F, 0F, 0F, 3, 14, 15, 0F); // clipPart2
		ammoModel[1].setRotationPoint(18F, -5F, -2.5F);

		ammoModel[2].addBox(0F, 0F, 0F, 8, 17, 16, 0F); // clipPart3
		ammoModel[2].setRotationPoint(21F, -5F, -3F);

		ammoModel[3].addBox(0F, 0F, 0F, 3, 3, 20, 0F); // Box 9
		ammoModel[3].setRotationPoint(18F, 9F, -3F);

		ammoModel[4].addShapeBox(0F, 0F, 0F, 3, 17, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 133
		ammoModel[4].setRotationPoint(15F, -5F, 13F);

		ammoModel[5].addShapeBox(0F, 0F, 0F, 3, 14, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 134
		ammoModel[5].setRotationPoint(18F, -5F, 12.5F);

		ammoModel[6].addShapeBox(0F, 0F, 0F, 8, 17, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 135
		ammoModel[6].setRotationPoint(21F, -5F, 13F);

		ammoModel[7].addShapeBox(0F, 0F, 0F, 14, 3, 20, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, -2F, -2F, 0F, -2F, -2F, 0F, -2F, -2F, 0F, -2F); // Box 138
		ammoModel[7].setRotationPoint(15F, 12F, -3F);

		ammoModel[8].addShapeBox(0F, 0F, 0F, 14, 2, 1, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 139
		ammoModel[8].setRotationPoint(15F, -7F, 4F);

		ammoModel[9].addBox(0F, 0F, 0F, 14, 2, 5, 0F); // Box 140
		ammoModel[9].setRotationPoint(15F, -7F, 5F);

		ammoModel[10].addBox(0F, 0F, 0F, 12, 5, 4, 0F); // Box 144
		ammoModel[10].setRotationPoint(16F, -12F, 5.5F);

		ammoModel[11].addShapeBox(0F, 0F, 0F, 12, 4, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, -1F, 0F, -1.5F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 147
		ammoModel[11].setRotationPoint(16F, -16F, 5.5F);

		ammoModel[12].addBox(0F, 0F, 0F, 12, 4, 1, 0F); // Box 148
		ammoModel[12].setRotationPoint(16F, -16F, 4.5F);


		slideModel = new ModelRendererTurbo[3];
		slideModel[0] = new ModelRendererTurbo(this, 26, 103, textureX, textureY); // Box 27
		slideModel[1] = new ModelRendererTurbo(this, 47, 96, textureX, textureY); // Box 46
		slideModel[2] = new ModelRendererTurbo(this, 47, 96, textureX, textureY); // Box 48

		slideModel[0].addShapeBox(0F, 0F, 0F, 15, 6, 2, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 27
		slideModel[0].setRotationPoint(16F, -17F, -4.5F);

		slideModel[1].addShapeBox(0F, 0F, 0F, 1, 2, 4, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F); // Box 46
		slideModel[1].setRotationPoint(28F, -15F, -8.5F);

		slideModel[2].addShapeBox(0F, 0F, 0F, 1, 2, 4, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F); // Box 48
		slideModel[2].setRotationPoint(29F, -15F, -8.5F);
		
		barrelAttachPoint = new Vector3f(94F / 16F, 14F / 16F, 0F / 16F);
		stockAttachPoint = new Vector3f(-14F / 16F, 14F / 16F, 0F / 16F);
		scopeAttachPoint = new Vector3f(15F / 16F, 20.5F / 16F, 0F / 16F);
		gripAttachPoint = new Vector3f(45 / 16F, 4F / 16F, 0F / 16F);

		gunSlideDistance = 0.8F;
		animationType = EnumAnimationType.BOTTOM_CLIP;

		flipAll();

		translateAll(0F, 0F, 0F);
	}
}
