package com.flansmod.client.model.dwfirearms; //Path where the model is located

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelSF116BP extends ModelGun //Same as Filename
{
	int textureX = 512;
	int textureY = 256;

	public ModelSF116BP() //Same as Filename
	{
		gunModel = new ModelRendererTurbo[70];
		gunModel[0] = new ModelRendererTurbo(this, 186, 125, textureX, textureY); // body1
		gunModel[1] = new ModelRendererTurbo(this, 72, 43, textureX, textureY); // body10
		gunModel[2] = new ModelRendererTurbo(this, 52, 22, textureX, textureY); // body11
		gunModel[3] = new ModelRendererTurbo(this, 72, 127, textureX, textureY); // body3
		gunModel[4] = new ModelRendererTurbo(this, 135, 128, textureX, textureY); // body5
		gunModel[5] = new ModelRendererTurbo(this, 145, 1, textureX, textureY); // body6
		gunModel[6] = new ModelRendererTurbo(this, 72, 65, textureX, textureY); // body8
		gunModel[7] = new ModelRendererTurbo(this, 72, 54, textureX, textureY); // body9
		gunModel[8] = new ModelRendererTurbo(this, 212, 31, textureX, textureY); // Box 1
		gunModel[9] = new ModelRendererTurbo(this, 187, 29, textureX, textureY); // Box 3
		gunModel[10] = new ModelRendererTurbo(this, 212, 31, textureX, textureY); // Box 4
		gunModel[11] = new ModelRendererTurbo(this, 187, 29, textureX, textureY); // Box 5
		gunModel[12] = new ModelRendererTurbo(this, 53, 12, textureX, textureY); // Box 71
		gunModel[13] = new ModelRendererTurbo(this, 53, 12, textureX, textureY); // Box 72
		gunModel[14] = new ModelRendererTurbo(this, 53, 12, textureX, textureY); // Box 73
		gunModel[15] = new ModelRendererTurbo(this, 53, 12, textureX, textureY); // Box 74
		gunModel[16] = new ModelRendererTurbo(this, 1, 16, textureX, textureY); // grip1
		gunModel[17] = new ModelRendererTurbo(this, 22, 50, textureX, textureY); // grip2
		gunModel[18] = new ModelRendererTurbo(this, 1, 82, textureX, textureY); // grip3
		gunModel[19] = new ModelRendererTurbo(this, 1, 82, textureX, textureY); // grip4
		gunModel[20] = new ModelRendererTurbo(this, 72, 78, textureX, textureY); // handGuard1
		gunModel[21] = new ModelRendererTurbo(this, 186, 84, textureX, textureY); // handGuard2
		gunModel[22] = new ModelRendererTurbo(this, 187, 70, textureX, textureY); // handGuard3
		gunModel[23] = new ModelRendererTurbo(this, 1, 27, textureX, textureY); // ironSightBase
		gunModel[24] = new ModelRendererTurbo(this, 72, 19, textureX, textureY); // mainBarrelBottom
		gunModel[25] = new ModelRendererTurbo(this, 72, 10, textureX, textureY); // mainBarrelMiddle
		gunModel[26] = new ModelRendererTurbo(this, 72, 1, textureX, textureY); // mainBarrelTop
		gunModel[27] = new ModelRendererTurbo(this, 186, 98, textureX, textureY); // railBase2
		gunModel[28] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // scopeBase
		gunModel[29] = new ModelRendererTurbo(this, 32, 72, textureX, textureY); // Box 68
		gunModel[30] = new ModelRendererTurbo(this, 44, 40, textureX, textureY); // Box 69
		gunModel[31] = new ModelRendererTurbo(this, 44, 29, textureX, textureY); // Box 70
		gunModel[32] = new ModelRendererTurbo(this, 53, 52, textureX, textureY); // Box 71
		gunModel[33] = new ModelRendererTurbo(this, 51, 80, textureX, textureY); // Box 73
		gunModel[34] = new ModelRendererTurbo(this, 72, 114, textureX, textureY); // Box 74
		gunModel[35] = new ModelRendererTurbo(this, 135, 98, textureX, textureY); // Box 75
		gunModel[36] = new ModelRendererTurbo(this, 72, 29, textureX, textureY); // Box 76
		gunModel[37] = new ModelRendererTurbo(this, 187, 43, textureX, textureY); // Box 77
		gunModel[38] = new ModelRendererTurbo(this, 187, 56, textureX, textureY); // Box 78
		gunModel[39] = new ModelRendererTurbo(this, 72, 184, textureX, textureY); // Box 79
		gunModel[40] = new ModelRendererTurbo(this, 72, 173, textureX, textureY); // Box 80
		gunModel[41] = new ModelRendererTurbo(this, 147, 173, textureX, textureY); // Box 81
		gunModel[42] = new ModelRendererTurbo(this, 72, 98, textureX, textureY); // Box 82
		gunModel[43] = new ModelRendererTurbo(this, 141, 113, textureX, textureY); // Box 83
		gunModel[44] = new ModelRendererTurbo(this, 72, 153, textureX, textureY); // Box 84
		gunModel[45] = new ModelRendererTurbo(this, 147, 173, textureX, textureY); // Box 85
		gunModel[46] = new ModelRendererTurbo(this, 227, 125, textureX, textureY); // Box 86
		gunModel[47] = new ModelRendererTurbo(this, 141, 184, textureX, textureY); // Box 87
		gunModel[48] = new ModelRendererTurbo(this, 227, 139, textureX, textureY); // Box 89
		gunModel[49] = new ModelRendererTurbo(this, 186, 139, textureX, textureY); // Box 90
		gunModel[50] = new ModelRendererTurbo(this, 186, 108, textureX, textureY); // Box 91
		gunModel[51] = new ModelRendererTurbo(this, 262, 43, textureX, textureY); // Box 92
		gunModel[52] = new ModelRendererTurbo(this, 262, 87, textureX, textureY); // Box 93
		gunModel[53] = new ModelRendererTurbo(this, 262, 99, textureX, textureY); // Box 94
		gunModel[54] = new ModelRendererTurbo(this, 262, 69, textureX, textureY); // Box 95
		gunModel[55] = new ModelRendererTurbo(this, 1, 46, textureX, textureY); // Box 96
		gunModel[56] = new ModelRendererTurbo(this, 23, 39, textureX, textureY); // Box 97
		gunModel[57] = new ModelRendererTurbo(this, 1, 75, textureX, textureY); // Box 98
		gunModel[58] = new ModelRendererTurbo(this, 72, 195, textureX, textureY); // Box 99
		gunModel[59] = new ModelRendererTurbo(this, 186, 153, textureX, textureY); // Box 100
		gunModel[60] = new ModelRendererTurbo(this, 186, 153, textureX, textureY); // Box 101
		gunModel[61] = new ModelRendererTurbo(this, 186, 153, textureX, textureY); // Box 102
		gunModel[62] = new ModelRendererTurbo(this, 186, 153, textureX, textureY); // Box 103
		gunModel[63] = new ModelRendererTurbo(this, 72, 127, textureX, textureY); // Box 104
		gunModel[64] = new ModelRendererTurbo(this, 144, 158, textureX, textureY); // Box 105
		gunModel[65] = new ModelRendererTurbo(this, 145, 143, textureX, textureY); // Box 106
		gunModel[66] = new ModelRendererTurbo(this, 72, 139, textureX, textureY); // Box 107
		gunModel[67] = new ModelRendererTurbo(this, 119, 158, textureX, textureY); // Box 108
		gunModel[68] = new ModelRendererTurbo(this, 119, 158, textureX, textureY); // Box 109
		gunModel[69] = new ModelRendererTurbo(this, 119, 158, textureX, textureY); // Box 110

		gunModel[0].addShapeBox(0F, 0F, 0F, 8, 3, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F); // body1
		gunModel[0].setRotationPoint(-14F, -14F, -5F);

		gunModel[1].addShapeBox(0F, 0F, 0F, 36, 2, 8, 0F, -1F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, -1F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body10
		gunModel[1].setRotationPoint(-24F, -23F, -4F);

		gunModel[2].addShapeBox(0F, 0F, 0F, 3, 2, 3, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body11
		gunModel[2].setRotationPoint(-25F, -22F, -1.5F);

		gunModel[3].addBox(0F, 0F, 0F, 21, 1, 10, 0F); // body3
		gunModel[3].setRotationPoint(-6F, -17F, -5F);

		gunModel[4].addShapeBox(0F, 0F, 0F, 15, 2, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body5
		gunModel[4].setRotationPoint(-6F, -19F, -4F);

		gunModel[5].addShapeBox(0F, 0F, 0F, 3, 2, 10, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body6
		gunModel[5].setRotationPoint(9F, -19F, -5F);

		gunModel[6].addShapeBox(-1F, 0F, 0F, 47, 2, 10, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body8
		gunModel[6].setRotationPoint(-52F, -19F, -5F);

		gunModel[7].addShapeBox(0F, 0F, 0F, 37, 2, 8, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body9
		gunModel[7].setRotationPoint(-25F, -21F, -4F);

		gunModel[8].addShapeBox(0F, 0F, 0F, 3, 2, 9, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 1
		gunModel[8].setRotationPoint(-16F, -24F, -4.5F);

		gunModel[9].addBox(0F, 0F, 0F, 3, 4, 9, 0F); // Box 3
		gunModel[9].setRotationPoint(-16F, -22F, -4.5F);

		gunModel[10].addShapeBox(0F, 0F, 0F, 3, 2, 9, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 4
		gunModel[10].setRotationPoint(-2F, -24F, -4.5F);

		gunModel[11].addBox(0F, 0F, 0F, 3, 4, 9, 0F); // Box 5
		gunModel[11].setRotationPoint(-2F, -22F, -4.5F);

		gunModel[12].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 71
		gunModel[12].setRotationPoint(18F, -4F, -3.5F);

		gunModel[13].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 72
		gunModel[13].setRotationPoint(24F, -4F, -3.5F);

		gunModel[14].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 73
		gunModel[14].setRotationPoint(30F, -4F, -3.5F);

		gunModel[15].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 74
		gunModel[15].setRotationPoint(36F, -4F, -3.5F);

		gunModel[16].addShapeBox(0F, 0F, 0F, 12, 3, 7, 0F, 0F, 0F, 1F, 3F, 0F, 1F, 3F, 0F, 1F, 0F, 0F, 1F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F); // grip1
		gunModel[16].setRotationPoint(-8F, -7F, -3.5F);

		gunModel[17].addShapeBox(0F, 0F, 0F, 8, 12, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, -2F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 4F, -2F, 0F); // grip2
		gunModel[17].setRotationPoint(-6F, -4F, -3.5F);

		gunModel[18].addShapeBox(0F, 0F, 0F, 8, 2, 7, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, -2F, 0F); // grip3
		gunModel[18].setRotationPoint(-10F, 8F, -3.5F);

		gunModel[19].addShapeBox(0F, 0F, 0F, 8, 2, 7, 0F, 0F, 2F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, -2F, 0F); // grip4
		gunModel[19].setRotationPoint(-10F, 10F, -3.5F);

		gunModel[20].addBox(0F, 0F, 0F, 27, 9, 10, 0F); // handGuard1
		gunModel[20].setRotationPoint(15F, -17F, -5F);

		gunModel[21].addBox(0F, 0F, 0F, 18, 3, 10, 0F); // handGuard2
		gunModel[21].setRotationPoint(24F, -20F, -5F);

		gunModel[22].addShapeBox(0F, 0F, 0F, 18, 3, 10, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // handGuard3
		gunModel[22].setRotationPoint(24F, -23F, -5F);

		gunModel[23].addShapeBox(0F, 0F, 0F, 12, 2, 9, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // ironSightBase
		gunModel[23].setRotationPoint(12F, -24F, -4.5F);

		gunModel[24].addShapeBox(0F, 0F, 0F, 30, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // mainBarrelBottom
		gunModel[24].setRotationPoint(42F, -13.5F, -3F);

		gunModel[25].addBox(0F, 0F, 0F, 30, 2, 6, 0F); // mainBarrelMiddle
		gunModel[25].setRotationPoint(42F, -15.5F, -3F);

		gunModel[26].addShapeBox(0F, 0F, 0F, 30, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // mainBarrelTop
		gunModel[26].setRotationPoint(42F, -17.5F, -3F);

		gunModel[27].addBox(0F, 0F, 0F, 21, 2, 7, 0F); // railBase2
		gunModel[27].setRotationPoint(18F, -6F, -3.5F);

		gunModel[28].addBox(0F, 0F, 0F, 12, 5, 9, 0F); // scopeBase
		gunModel[28].setRotationPoint(12F, -22F, -4.5F);

		gunModel[29].addShapeBox(0F, 0F, 0F, 2, 12, 7, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 4F, 0F, 0F, -4F, 0F, -1F, -4F, 0F, -1F, 4F, 0F, 0F); // Box 68
		gunModel[29].setRotationPoint(2F, -4F, -3.5F);

		gunModel[30].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, -1F, 1F, 0F, -1F, -1F, 0F, 0F); // Box 69
		gunModel[30].setRotationPoint(-2F, 8F, -3.5F);

		gunModel[31].addBox(0F, 0F, 0F, 11, 2, 7, 0F); // Box 70
		gunModel[31].setRotationPoint(-1F, 10F, -3.5F);

		gunModel[32].addShapeBox(0F, 0F, 0F, 1, 10, 7, 0F, -4F, 0F, -1F, 4F, 0F, 0F, 4F, 0F, 0F, -4F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 71
		gunModel[32].setRotationPoint(-11F, -4F, -3.5F);

		gunModel[33].addShapeBox(0F, 0F, 0F, 1, 4, 7, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -0.5F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -1F); // Box 73
		gunModel[33].setRotationPoint(-11F, 6F, -3.5F);

		gunModel[34].addBox(0F, 0F, 0F, 25, 3, 9, 0F); // Box 74
		gunModel[34].setRotationPoint(-10F, -10F, -4.5F);

		gunModel[35].addShapeBox(0F, 0F, 0F, 14, 5, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, -1F, 0F); // Box 75
		gunModel[35].setRotationPoint(-32F, -7F, -4.5F);

		gunModel[36].addBox(0F, 0F, 0F, 47, 3, 10, 0F); // Box 76
		gunModel[36].setRotationPoint(-53F, -17F, -5F);

		gunModel[37].addShapeBox(0F, 0F, 0F, 27, 2, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 77
		gunModel[37].setRotationPoint(15F, -8F, -5F);

		gunModel[38].addBox(0F, 0F, 0F, 16, 2, 11, 0F); // Box 78
		gunModel[38].setRotationPoint(25F, -19.5F, -5.5F);

		gunModel[39].addShapeBox(0F, 0F, 0F, 25, 1, 9, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 79
		gunModel[39].setRotationPoint(-10F, -11F, -4.5F);

		gunModel[40].addShapeBox(0F, 0F, 0F, 28, 1, 9, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 80
		gunModel[40].setRotationPoint(-42F, -14F, -4.5F);

		gunModel[41].addShapeBox(0F, 0F, 0F, 4, 1, 9, 0F, 0F, 0F, 0.5F, 0F, -3F, 0.5F, 0F, -3F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F); // Box 81
		gunModel[41].setRotationPoint(-14F, -14F, -4.5F);

		gunModel[42].addBox(0F, 0F, 0F, 22, 6, 9, 0F); // Box 82
		gunModel[42].setRotationPoint(-32F, -13F, -4.5F);

		gunModel[43].addShapeBox(0F, 0F, 0F, 10, 5, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, -1F, 0F); // Box 83
		gunModel[43].setRotationPoint(-42F, -3F, -4.5F);

		gunModel[44].addBox(0F, 0F, 0F, 14, 10, 9, 0F); // Box 84
		gunModel[44].setRotationPoint(-46F, -13F, -4.5F);

		gunModel[45].addShapeBox(0F, 0F, 0F, 4, 1, 9, 0F, 0F, -3F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, -3F, 0.5F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F); // Box 85
		gunModel[45].setRotationPoint(-46F, -14F, -4.5F);

		gunModel[46].addShapeBox(-1F, 0F, 0F, 11, 3, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F); // Box 86
		gunModel[46].setRotationPoint(-52F, -14F, -5F);

		gunModel[47].addShapeBox(-1F, 0F, 0F, 7, 1, 9, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 87
		gunModel[47].setRotationPoint(-52F, -11F, -4.5F);

		gunModel[48].addShapeBox(-1F, 0F, 0F, 11, 6, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -5F, 0F, 0F, -5F, 0F, 0F, 0F, 0F, 0F); // Box 89
		gunModel[48].setRotationPoint(-52F, 1F, -4.5F);

		gunModel[49].addBox(-1F, 0F, 0F, 11, 4, 9, 0F); // Box 90
		gunModel[49].setRotationPoint(-52F, -3F, -4.5F);

		gunModel[50].addBox(-1F, 0F, 0F, 7, 7, 9, 0F); // Box 91
		gunModel[50].setRotationPoint(-52F, -10F, -4.5F);

		gunModel[51].addBox(-1F, 0F, 0F, 4, 17, 10, 0F); // Box 92
		gunModel[51].setRotationPoint(-56F, -10F, -5F);

		gunModel[52].addShapeBox(-1F, 0F, 0F, 4, 1, 10, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 93
		gunModel[52].setRotationPoint(-56F, -11F, -5F);

		gunModel[53].addShapeBox(-1F, 0F, 0F, 4, 2, 9, 0F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2.5F, 0F, 0F, 2F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 2F, 1F); // Box 94
		gunModel[53].setRotationPoint(-56F, -19F, -4.5F);

		gunModel[54].addShapeBox(-1F, 0F, 0F, 4, 6, 11, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 95
		gunModel[54].setRotationPoint(-56F, -17F, -5.5F);

		gunModel[55].addShapeBox(0F, 0F, 0F, 3, 17, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 5F, 0F, 0F, -5F, 0F, 0F, -5F, 0F, 0F, 5F, 0F, 0F); // Box 96
		gunModel[55].setRotationPoint(12F, -6F, -3.5F);

		gunModel[56].addBox(0F, 0F, 0F, 3, 1, 7, 0F); // Box 97
		gunModel[56].setRotationPoint(12F, -7F, -3.5F);

		gunModel[57].addShapeBox(0F, 0F, 0F, 10, 4, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 98
		gunModel[57].setRotationPoint(-42F, -7F, 4F);

		gunModel[58].addShapeBox(-1F, 0F, 0F, 25, 1, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 99
		gunModel[58].setRotationPoint(-48F, -20F, -3.5F);

		gunModel[59].addShapeBox(-1F, 0F, 0F, 2, 1, 10, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 100
		gunModel[59].setRotationPoint(-35F, -1F, -5F);

		gunModel[60].addShapeBox(-1F, 0F, 0F, 2, 1, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 101
		gunModel[60].setRotationPoint(-35F, 0F, -5F);

		gunModel[61].addShapeBox(-1F, 0F, 0F, 2, 1, 10, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 102
		gunModel[61].setRotationPoint(-44F, -2F, -5F);

		gunModel[62].addShapeBox(-1F, 0F, 0F, 2, 1, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 103
		gunModel[62].setRotationPoint(-44F, -1F, -5F);

		gunModel[63].addBox(0F, 0F, 0F, 21, 1, 10, 0F); // Box 104
		gunModel[63].setRotationPoint(-6F, -12F, -5F);

		gunModel[64].addShapeBox(0F, 0F, 0F, 5, 4, 10, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 105
		gunModel[64].setRotationPoint(-6F, -16F, -5F);

		gunModel[65].addShapeBox(0F, 0F, 0F, 4, 4, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F); // Box 106
		gunModel[65].setRotationPoint(11F, -16F, -5F);

		gunModel[66].addBox(0F, 0F, 0F, 16, 4, 9, 0F); // Box 107
		gunModel[66].setRotationPoint(-3F, -16F, -4.5F);

		gunModel[67].addShapeBox(0F, 0F, 0F, 2, 4, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F); // Box 108
		gunModel[67].setRotationPoint(3F, -16F, -5F);

		gunModel[68].addShapeBox(0F, 0F, 0F, 2, 4, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F); // Box 109
		gunModel[68].setRotationPoint(7F, -16F, -5F);

		gunModel[69].addShapeBox(0F, 0F, 0F, 2, 4, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F); // Box 110
		gunModel[69].setRotationPoint(-1F, -16F, -5F);


		defaultBarrelModel = new ModelRendererTurbo[3];
		defaultBarrelModel[0] = new ModelRendererTurbo(this, 203, 9, textureX, textureY); // muzzle1
		defaultBarrelModel[1] = new ModelRendererTurbo(this, 203, 19, textureX, textureY); // muzzle2
		defaultBarrelModel[2] = new ModelRendererTurbo(this, 203, 9, textureX, textureY); // muzzle1-2

		defaultBarrelModel[0].addShapeBox(0F, 0F, 0F, 10, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // muzzle1
		defaultBarrelModel[0].setRotationPoint(72F, -18F, -3.5F);

		defaultBarrelModel[1].addBox(0F, 0F, 0F, 10, 3, 7, 0F); // muzzle2
		defaultBarrelModel[1].setRotationPoint(72F, -16F, -3.5F);

		defaultBarrelModel[2].addShapeBox(0F, 0F, 0F, 10, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // muzzle1-2
		defaultBarrelModel[2].setRotationPoint(72F, -13F, -3.5F);


		defaultScopeModel = new ModelRendererTurbo[7];
		defaultScopeModel[0] = new ModelRendererTurbo(this, 53, 1, textureX, textureY); // scopeBaseFront
		defaultScopeModel[1] = new ModelRendererTurbo(this, 44, 1, textureX, textureY); // battery
		defaultScopeModel[2] = new ModelRendererTurbo(this, 44, 12, textureX, textureY); // ironSight1
		defaultScopeModel[3] = new ModelRendererTurbo(this, 44, 12, textureX, textureY); // ironSight1-2
		defaultScopeModel[4] = new ModelRendererTurbo(this, 44, 21, textureX, textureY); // ironSight2
		defaultScopeModel[5] = new ModelRendererTurbo(this, 53, 1, textureX, textureY); // ironSight3
		defaultScopeModel[6] = new ModelRendererTurbo(this, 1, 39, textureX, textureY); // ironSight4

		defaultScopeModel[0].addBox(0F, 0F, 0F, 3, 4, 6, 0F); // scopeBaseFront
		defaultScopeModel[0].setRotationPoint(38.9F, -26F, -3F);

		defaultScopeModel[1].addShapeBox(0F, 0F, 0F, 6, 3, 1, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // battery
		defaultScopeModel[1].setRotationPoint(12F, -20F, -5.5F);

		defaultScopeModel[2].addBox(0F, 0F, 0F, 3, 7, 1, 0F); // ironSight1
		defaultScopeModel[2].setRotationPoint(38.9F, -31.5F, 2F);

		defaultScopeModel[3].addBox(0F, 0F, 0F, 3, 7, 1, 0F); // ironSight1-2
		defaultScopeModel[3].setRotationPoint(38.9F, -31.5F, -3F);

		defaultScopeModel[4].addBox(0F, 0F, 0F, 2, 2, 1, 0F); // ironSight2
		defaultScopeModel[4].setRotationPoint(-20F, -27F, -0.5F);

		defaultScopeModel[5].addBox(0F, 0F, 0F, 3, 4, 6, 0F); // ironSight3
		defaultScopeModel[5].setRotationPoint(-20F, -25.5F, -3F);

		defaultScopeModel[6].addBox(0F, 0F, 0F, 9, 1, 5, 0F); // ironSight4
		defaultScopeModel[6].setRotationPoint(12F, -25F, -2.5F);


		ammoModel = new ModelRendererTurbo[5];
		ammoModel[0] = new ModelRendererTurbo(this, 16, 146, textureX, textureY); // bullet
		ammoModel[1] = new ModelRendererTurbo(this, 1, 146, textureX, textureY); // bulletTip
		ammoModel[2] = new ModelRendererTurbo(this, 1, 132, textureX, textureY); // clip1
		ammoModel[3] = new ModelRendererTurbo(this, 1, 112, textureX, textureY); // clip2
		ammoModel[4] = new ModelRendererTurbo(this, 1, 92, textureX, textureY); // clip3

		ammoModel[0].addShapeBox(0F, 0F, 0F, 8, 1, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // bullet
		ammoModel[0].setRotationPoint(-33F, -6F, -2F);

		ammoModel[1].addShapeBox(0F, 0F, 0F, 3, 1, 4, 0F, 0F, 0F, -1F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // bulletTip
		ammoModel[1].setRotationPoint(-25F, -6F, -2F);

		ammoModel[2].addBox(0F, 0F, 0F, 13, 6, 7, 0F); // clip1
		ammoModel[2].setRotationPoint(-34F, -5F, -3.5F);

		ammoModel[3].addShapeBox(0F, 0F, 0F, 13, 12, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F); // clip2
		ammoModel[3].setRotationPoint(-34F, 1F, -3.5F);

		ammoModel[4].addShapeBox(0F, 0F, 0F, 13, 12, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, 3F, -4F, 0F, 3F, -4F, 0F, -4F, 0F, 0F); // clip3
		ammoModel[4].setRotationPoint(-32F, 13F, -3.5F);

		slideModel = new ModelRendererTurbo[1];
		slideModel[0] = new ModelRendererTurbo(this, 44, 6, textureX, textureY); // bolt1

		slideModel[0].addBox(0F, 0F, 0F, 1, 2, 3, 0F); // bolt1
		slideModel[0].setRotationPoint(8F, -19F, -7F);

		barrelAttachPoint = new Vector3f(72F /16F, 14.5F /16F, 0F /16F);
		scopeAttachPoint = new Vector3f(-8F /16F, 22F /16F, 0F /16F);
		gripAttachPoint = new Vector3f(29 /16F, 5F /16F, 0F /16F);

		gunSlideDistance = 0.8F;
		animationType = EnumAnimationType.CUSTOM;

		/* ----Start of Reload Block---- */
		/* Reload Name: Amended Side Clip */
		rotateGunVertical = 70F;
		tiltGun = 10F;
		translateGun = new Vector3f(0.5F, -0.2F, 0F);

		rotateClipVertical = -90F;
		translateClip = new Vector3f(-2F, -2F, 0F);
		/* ----End of Reload Block---- */

		translateAll(0F, 0F, 0F);


		flipAll();
	}
}