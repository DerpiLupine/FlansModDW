package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.ModelAttachment;
import com.flansmod.client.tmt.ModelRendererTurbo;

import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelABTReflexSight extends ModelAttachment
{
	int textureX = 512;
	int textureY = 256;

	public ModelABTReflexSight()
	{
		attachmentModel = new ModelRendererTurbo[58];
		attachmentModel[0] = new ModelRendererTurbo(this, 1, 92, textureX, textureY); // Box 0
		attachmentModel[1] = new ModelRendererTurbo(this, 24, 103, textureX, textureY); // scopeBase2-2
		attachmentModel[2] = new ModelRendererTurbo(this, 1, 116, textureX, textureY); // brassScope1
		attachmentModel[3] = new ModelRendererTurbo(this, 1, 116, textureX, textureY); // brassScope1-2
		attachmentModel[4] = new ModelRendererTurbo(this, 32, 128, textureX, textureY); // brassScope2
		attachmentModel[5] = new ModelRendererTurbo(this, 32, 122, textureX, textureY); // brassScope3
		attachmentModel[6] = new ModelRendererTurbo(this, 32, 122, textureX, textureY); // brassScope3-2
		attachmentModel[7] = new ModelRendererTurbo(this, 1, 140, textureX, textureY); // knob1
		attachmentModel[8] = new ModelRendererTurbo(this, 1, 140, textureX, textureY); // knob2
		attachmentModel[9] = new ModelRendererTurbo(this, 1, 140, textureX, textureY); // knob3
		attachmentModel[10] = new ModelRendererTurbo(this, 1, 103, textureX, textureY); // scopeBase1
		attachmentModel[11] = new ModelRendererTurbo(this, 24, 103, textureX, textureY); // scopeBase2
		attachmentModel[12] = new ModelRendererTurbo(this, 1, 134, textureX, textureY); // scopeFront1
		attachmentModel[13] = new ModelRendererTurbo(this, 1, 134, textureX, textureY); // scopeFront1-2
		attachmentModel[14] = new ModelRendererTurbo(this, 14, 134, textureX, textureY); // scopeFront2
		attachmentModel[15] = new ModelRendererTurbo(this, 14, 134, textureX, textureY); // scopeFront2-2
		attachmentModel[16] = new ModelRendererTurbo(this, 1, 110, textureX, textureY); // scopeMain1
		attachmentModel[17] = new ModelRendererTurbo(this, 1, 122, textureX, textureY); // scopeMain2
		attachmentModel[18] = new ModelRendererTurbo(this, 1, 122, textureX, textureY); // scopeMain2-2
		attachmentModel[19] = new ModelRendererTurbo(this, 1, 128, textureX, textureY); // scopeMain3-2
		attachmentModel[20] = new ModelRendererTurbo(this, 38, 111, textureX, textureY); // scopeSight1
		attachmentModel[21] = new ModelRendererTurbo(this, 38, 111, textureX, textureY); // scopeSight1-2
		attachmentModel[22] = new ModelRendererTurbo(this, 47, 111, textureX, textureY); // scopeSight2-1
		attachmentModel[23] = new ModelRendererTurbo(this, 1, 134, textureX, textureY); // Box 0
		attachmentModel[24] = new ModelRendererTurbo(this, 14, 134, textureX, textureY); // Box 2
		attachmentModel[25] = new ModelRendererTurbo(this, 21, 134, textureX, textureY); // Box 3
		attachmentModel[26] = new ModelRendererTurbo(this, 1, 134, textureX, textureY); // Box 4
		attachmentModel[27] = new ModelRendererTurbo(this, 14, 134, textureX, textureY); // Box 6
		attachmentModel[28] = new ModelRendererTurbo(this, 14, 134, textureX, textureY); // Box 8
		attachmentModel[29] = new ModelRendererTurbo(this, 1, 134, textureX, textureY); // Box 9
		attachmentModel[30] = new ModelRendererTurbo(this, 14, 134, textureX, textureY); // Box 12
		attachmentModel[31] = new ModelRendererTurbo(this, 1, 134, textureX, textureY); // Box 14
		attachmentModel[32] = new ModelRendererTurbo(this, 47, 111, textureX, textureY); // Box 16
		attachmentModel[33] = new ModelRendererTurbo(this, 21, 134, textureX, textureY); // Box 48
		attachmentModel[34] = new ModelRendererTurbo(this, 21, 134, textureX, textureY); // Box 49
		attachmentModel[35] = new ModelRendererTurbo(this, 21, 134, textureX, textureY); // Box 50
		attachmentModel[36] = new ModelRendererTurbo(this, 21, 134, textureX, textureY); // Box 51
		attachmentModel[37] = new ModelRendererTurbo(this, 21, 134, textureX, textureY); // Box 52
		attachmentModel[38] = new ModelRendererTurbo(this, 21, 134, textureX, textureY); // Box 53
		attachmentModel[39] = new ModelRendererTurbo(this, 21, 134, textureX, textureY); // Box 54
		attachmentModel[40] = new ModelRendererTurbo(this, 21, 134, textureX, textureY); // Box 55
		attachmentModel[41] = new ModelRendererTurbo(this, 21, 134, textureX, textureY); // Box 56
		attachmentModel[42] = new ModelRendererTurbo(this, 21, 134, textureX, textureY); // Box 57
		attachmentModel[43] = new ModelRendererTurbo(this, 21, 134, textureX, textureY); // Box 58
		attachmentModel[44] = new ModelRendererTurbo(this, 1, 128, textureX, textureY); // Box 59
		attachmentModel[45] = new ModelRendererTurbo(this, 32, 128, textureX, textureY); // Box 60
		attachmentModel[46] = new ModelRendererTurbo(this, 32, 128, textureX, textureY); // Box 61
		attachmentModel[47] = new ModelRendererTurbo(this, 32, 128, textureX, textureY); // Box 62
		attachmentModel[48] = new ModelRendererTurbo(this, 1, 134, textureX, textureY); // Box 63
		attachmentModel[49] = new ModelRendererTurbo(this, 21, 134, textureX, textureY); // Box 64
		attachmentModel[50] = new ModelRendererTurbo(this, 14, 134, textureX, textureY); // Box 65
		attachmentModel[51] = new ModelRendererTurbo(this, 21, 134, textureX, textureY); // Box 66
		attachmentModel[52] = new ModelRendererTurbo(this, 1, 134, textureX, textureY); // Box 67
		attachmentModel[53] = new ModelRendererTurbo(this, 21, 134, textureX, textureY); // Box 68
		attachmentModel[54] = new ModelRendererTurbo(this, 14, 134, textureX, textureY); // Box 69
		attachmentModel[55] = new ModelRendererTurbo(this, 21, 134, textureX, textureY); // Box 70
		attachmentModel[56] = new ModelRendererTurbo(this, 1, 134, textureX, textureY); // Box 72
		attachmentModel[57] = new ModelRendererTurbo(this, 1, 134, textureX, textureY); // Box 73

		attachmentModel[0].addBox(-9F, -3F, -3.5F, 18, 3, 7, 0F); // Box 0
		attachmentModel[0].setRotationPoint(0F, 0F, 0F);

		attachmentModel[1].addBox(-3F, -4F, -3F, 6, 2, 1, 0F); // scopeBase2-2
		attachmentModel[1].setRotationPoint(0F, 0F, 0F);

		attachmentModel[2].addShapeBox(-10F, -14F, -2F, 25, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F); // brassScope1
		attachmentModel[2].setRotationPoint(0F, 0F, 0F);

		attachmentModel[3].addShapeBox(-10F, -5F, -2F, 25, 1, 4, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // brassScope1-2
		attachmentModel[3].setRotationPoint(0F, 0F, 0F);

		attachmentModel[4].addShapeBox(-10F, -14F, -2F, 25, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, -1F, 3F, 0F, -1F, 3F, 0F, -0.5F, -3F, 0F, -0.5F, -3F); // brassScope2
		attachmentModel[4].setRotationPoint(0F, 0F, 0F);

		attachmentModel[5].addShapeBox(-10F, -11F, 4F, 25, 4, 1, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // brassScope3
		attachmentModel[5].setRotationPoint(0F, 0F, 0F);

		attachmentModel[6].addShapeBox(-10F, -11F, -5F, 25, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F); // brassScope3-2
		attachmentModel[6].setRotationPoint(0F, 0F, 0F);

		attachmentModel[7].addShapeBox(-3F, -16F, -3F, 2, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F); // knob1
		attachmentModel[7].setRotationPoint(0F, 0F, 0F);

		attachmentModel[8].addBox(-1F, -16F, -3F, 2, 2, 6, 0F); // knob2
		attachmentModel[8].setRotationPoint(0F, 0F, 0F);

		attachmentModel[9].addShapeBox(1F, -16F, -3F, 2, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F); // knob3
		attachmentModel[9].setRotationPoint(0F, 0F, 0F);

		attachmentModel[10].addBox(-3.5F, -4F, -2F, 7, 2, 4, 0F); // scopeBase1
		attachmentModel[10].setRotationPoint(0F, 0F, 0F);

		attachmentModel[11].addBox(-3F, -4F, 2F, 6, 2, 1, 0F); // scopeBase2
		attachmentModel[11].setRotationPoint(0F, 0F, 0F);

		attachmentModel[12].addShapeBox(15F, -15F, -2F, 2, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F); // scopeFront1
		attachmentModel[12].setRotationPoint(0F, 0F, 0F);

		attachmentModel[13].addShapeBox(15F, -4F, -2F, 2, 1, 4, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scopeFront1-2
		attachmentModel[13].setRotationPoint(0F, 0F, 0F);

		attachmentModel[14].addShapeBox(15F, -11F, 5F, 2, 4, 1, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scopeFront2
		attachmentModel[14].setRotationPoint(0F, 0F, 0F);

		attachmentModel[15].addShapeBox(15F, -11F, -6F, 2, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F); // scopeFront2-2
		attachmentModel[15].setRotationPoint(0F, 0F, 0F);

		attachmentModel[16].addShapeBox(-4F, -15F, -2F, 14, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F); // scopeMain1
		attachmentModel[16].setRotationPoint(0F, 0F, 0F);

		attachmentModel[17].addShapeBox(-4F, -11F, 5F, 14, 4, 1, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scopeMain2
		attachmentModel[17].setRotationPoint(0F, 0F, 0F);

		attachmentModel[18].addShapeBox(-4F, -11F, -6F, 14, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F); // scopeMain2-2
		attachmentModel[18].setRotationPoint(0F, 0F, 0F);

		attachmentModel[19].addShapeBox(-4F, -15F, 1F, 14, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 4F, 0F, 0F, 4F); // scopeMain3-2
		attachmentModel[19].setRotationPoint(0F, 0F, 0F);

		attachmentModel[20].addBox(-9F, -9.5F, 1F, 1, 1, 3, 0F); // scopeSight1
		attachmentModel[20].setRotationPoint(0F, 0F, 0F);

		attachmentModel[21].addBox(-9F, -9.5F, -4F, 1, 1, 3, 0F); // scopeSight1-2
		attachmentModel[21].setRotationPoint(0F, 0F, 0F);

		attachmentModel[22].addBox(13F, -8F, -0.5F, 1, 3, 1, 0F); // scopeSight2-1
		attachmentModel[22].setRotationPoint(0F, 0F, 0F);

		attachmentModel[23].addShapeBox(-12F, -4F, -2F, 2, 1, 4, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 0
		attachmentModel[23].setRotationPoint(0F, 0F, 0F);

		attachmentModel[24].addShapeBox(-12F, -11F, -6F, 2, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F); // Box 2
		attachmentModel[24].setRotationPoint(0F, 0F, 0F);

		attachmentModel[25].addShapeBox(-12F, -15F, -2F, 2, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0.5F, -4F, 0F, 0.5F, -4F); // Box 3
		attachmentModel[25].setRotationPoint(0F, 0F, 0F);

		attachmentModel[26].addShapeBox(-12F, -15F, -2F, 2, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F); // Box 4
		attachmentModel[26].setRotationPoint(0F, 0F, 0F);

		attachmentModel[27].addShapeBox(-12F, -11F, 5F, 2, 4, 1, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 6
		attachmentModel[27].setRotationPoint(0F, 0F, 0F);

		attachmentModel[28].addShapeBox(-9F, -11F, 4.85F, 2, 4, 1, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 8
		attachmentModel[28].setRotationPoint(0F, 0F, 0F);

		attachmentModel[29].addShapeBox(-9F, -4.2F, -2F, 2, 1, 4, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 9
		attachmentModel[29].setRotationPoint(0F, 0F, 0F);

		attachmentModel[30].addShapeBox(-9F, -11F, -5.8F, 2, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F); // Box 12
		attachmentModel[30].setRotationPoint(0F, 0F, 0F);

		attachmentModel[31].addShapeBox(-9F, -15F, -2F, 2, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F); // Box 14
		attachmentModel[31].setRotationPoint(0F, 0F, 0F);

		attachmentModel[32].addBox(13F, -13F, -0.5F, 1, 3, 1, 0F); // Box 16
		attachmentModel[32].setRotationPoint(0F, 0F, 0F);

		attachmentModel[33].addShapeBox(-12F, -7F, -2F, 2, 4, 1, 0F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0.5F, -4F, 0F, 0.5F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // Box 48
		attachmentModel[33].setRotationPoint(0F, 0F, 0F);

		attachmentModel[34].addShapeBox(-12F, -7F, 1F, 2, 4, 1, 0F, 0F, 0.5F, -4F, 0F, 0.5F, -4F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 49
		attachmentModel[34].setRotationPoint(0F, 0F, 0F);

		attachmentModel[35].addShapeBox(-12F, -15F, 1F, 2, 4, 1, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, -4F, 0F, 0.5F, -4F, 0F, 0F, 4F, 0F, 0F, 4F); // Box 50
		attachmentModel[35].setRotationPoint(0F, 0F, 0F);

		attachmentModel[36].addShapeBox(-9F, -7F, 1F, 2, 4, 1, 0F, 0F, 0.5F, -4F, 0F, 0.5F, -4F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 51
		attachmentModel[36].setRotationPoint(0F, 0F, 0F);

		attachmentModel[37].addShapeBox(-9F, -15F, 1F, 2, 4, 1, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, -4F, 0F, 0.5F, -4F, 0F, 0F, 4F, 0F, 0F, 4F); // Box 52
		attachmentModel[37].setRotationPoint(0F, 0F, 0F);

		attachmentModel[38].addShapeBox(-9F, -15F, -2F, 2, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0.5F, -4F, 0F, 0.5F, -4F); // Box 53
		attachmentModel[38].setRotationPoint(0F, 0F, 0F);

		attachmentModel[39].addShapeBox(-9F, -7F, -2F, 2, 4, 1, 0F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0.5F, -4F, 0F, 0.5F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // Box 54
		attachmentModel[39].setRotationPoint(0F, 0F, 0F);

		attachmentModel[40].addShapeBox(15F, -7F, 1F, 2, 4, 1, 0F, 0F, 0.5F, -4F, 0F, 0.5F, -4F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 55
		attachmentModel[40].setRotationPoint(0F, 0F, 0F);

		attachmentModel[41].addShapeBox(15F, -15F, 1F, 2, 4, 1, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, -4F, 0F, 0.5F, -4F, 0F, 0F, 4F, 0F, 0F, 4F); // Box 56
		attachmentModel[41].setRotationPoint(0F, 0F, 0F);

		attachmentModel[42].addShapeBox(15F, -15F, -2F, 2, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0.5F, -4F, 0F, 0.5F, -4F); // Box 57
		attachmentModel[42].setRotationPoint(0F, 0F, 0F);

		attachmentModel[43].addShapeBox(15F, -7F, -2F, 2, 4, 1, 0F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0.5F, -4F, 0F, 0.5F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // Box 58
		attachmentModel[43].setRotationPoint(0F, 0F, 0F);

		attachmentModel[44].addShapeBox(-4F, -15F, -2F, 14, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, -4F, 0F, 0F, -4F); // Box 59
		attachmentModel[44].setRotationPoint(0F, 0F, 0F);

		attachmentModel[45].addShapeBox(-10F, -8F, -2F, 25, 4, 1, 0F, 0F, -1F, 3F, 0F, -1F, 3F, 0F, -0.5F, -3F, 0F, -0.5F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // Box 60
		attachmentModel[45].setRotationPoint(0F, 0F, 0F);

		attachmentModel[46].addShapeBox(-10F, -8F, 1F, 25, 4, 1, 0F, 0F, -0.5F, -3F, 0F, -0.5F, -3F, 0F, -1F, 3F, 0F, -1F, 3F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 61
		attachmentModel[46].setRotationPoint(0F, 0F, 0F);

		attachmentModel[47].addShapeBox(-10F, -14F, 1F, 25, 4, 1, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -3F, 0F, -0.5F, -3F, 0F, -1F, 3F, 0F, -1F, 3F); // Box 62
		attachmentModel[47].setRotationPoint(0F, 0F, 0F);

		attachmentModel[48].addShapeBox(21F, -15F, -2F, 2, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F); // Box 63
		attachmentModel[48].setRotationPoint(0F, 0F, 0F);

		attachmentModel[49].addShapeBox(21F, -15F, -2F, 2, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0.5F, -4F, 0F, 0.5F, -4F); // Box 64
		attachmentModel[49].setRotationPoint(0F, 0F, 0F);

		attachmentModel[50].addShapeBox(21F, -11F, -6F, 2, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F); // Box 65
		attachmentModel[50].setRotationPoint(0F, 0F, 0F);

		attachmentModel[51].addShapeBox(21F, -7F, -2F, 2, 4, 1, 0F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0.5F, -4F, 0F, 0.5F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // Box 66
		attachmentModel[51].setRotationPoint(0F, 0F, 0F);

		attachmentModel[52].addShapeBox(21F, -4F, -2F, 2, 1, 4, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 67
		attachmentModel[52].setRotationPoint(0F, 0F, 0F);

		attachmentModel[53].addShapeBox(21F, -7F, 1F, 2, 4, 1, 0F, 0F, 0.5F, -4F, 0F, 0.5F, -4F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 68
		attachmentModel[53].setRotationPoint(0F, 0F, 0F);

		attachmentModel[54].addShapeBox(21F, -11F, 5F, 2, 4, 1, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 69
		attachmentModel[54].setRotationPoint(0F, 0F, 0F);

		attachmentModel[55].addShapeBox(21F, -15F, 1F, 2, 4, 1, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, -4F, 0F, 0.5F, -4F, 0F, 0F, 4F, 0F, 0F, 4F); // Box 70
		attachmentModel[55].setRotationPoint(0F, 0F, 0F);

		attachmentModel[56].addShapeBox(-1F, -17F, -2F, 2, 1, 4, 0F, 0F, 0F, -1F, 22F, 0F, -1F, 22F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 22F, 0F, -1F, 22F, 0F, -1F, 0F, 0F, -1F); // Box 72
		attachmentModel[56].setRotationPoint(0F, 0F, 0F);

		attachmentModel[57].addShapeBox(15F, -16F, -2F, 2, 1, 4, 0F, 0F, 0F, -1F, 6F, 0F, -1F, 6F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 6F, 0F, -1F, 6F, 0F, -1F, 0F, 0F, -1F); // Box 73
		attachmentModel[57].setRotationPoint(0F, 0F, 0F);

		renderOffset = 0F;

		flipAll();
	}
}