package com.flansmod.client.model.dwfirearms;

import net.minecraft.client.model.ModelBase;
import net.minecraft.entity.Entity;

import com.flansmod.client.tmt.ModelRendererTurbo;

public class ModelFlameGrenadeLPM extends ModelBase 
{
	public ModelRendererTurbo[] grenadeModel;

	int textureX = 64;
	int textureY = 32;

	public ModelFlameGrenadeLPM()
	{
		grenadeModel = new ModelRendererTurbo[13];
		grenadeModel[0] = new ModelRendererTurbo(this, 2, 2, textureX, textureY); // Box 1
		grenadeModel[1] = new ModelRendererTurbo(this, 20, 2, textureX, textureY); // Box 2
		grenadeModel[2] = new ModelRendererTurbo(this, 11, 2, textureX, textureY); // Box 3
		grenadeModel[3] = new ModelRendererTurbo(this, 2, 9, textureX, textureY); // Box 1
		grenadeModel[4] = new ModelRendererTurbo(this, 2, 13, textureX, textureY); // Box 2
		grenadeModel[5] = new ModelRendererTurbo(this, 2, 9, textureX, textureY); // Box 3
		grenadeModel[6] = new ModelRendererTurbo(this, 11, 9, textureX, textureY); // Box 6
		grenadeModel[7] = new ModelRendererTurbo(this, 11, 9, textureX, textureY); // Box 8
		grenadeModel[8] = new ModelRendererTurbo(this, 11, 9, textureX, textureY); // Box 9
		grenadeModel[9] = new ModelRendererTurbo(this, 11, 9, textureX, textureY); // Box 0
		grenadeModel[10] = new ModelRendererTurbo(this, 11, 9, textureX, textureY); // Box 1
		grenadeModel[11] = new ModelRendererTurbo(this, 11, 9, textureX, textureY); // Box 2
		grenadeModel[12] = new ModelRendererTurbo(this, 20, 9, textureX, textureY); // Box 3

		grenadeModel[0].addShapeBox(-1.5F, 0F, -1.5F, 3, 5, 1, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 1
		grenadeModel[0].setRotationPoint(0F, 0F, 0F);

		grenadeModel[1].addShapeBox(-1.5F, 0F, 0.5F, 3, 5, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 2
		grenadeModel[1].setRotationPoint(0F, 0F, 0F);

		grenadeModel[2].addBox(-1.5F, 0F, -0.5F, 3, 5, 1, 0F); // Box 3
		grenadeModel[2].setRotationPoint(0F, 0F, 0F);

		grenadeModel[3].addShapeBox(-1.5F, -0.4F, 0.5F, 3, 2, 1, 0F, 0.25F, -0.5F, 0F, 0.25F, -0.5F, 0F, -0.8F, -0.5F, 0.25F, -0.8F, -0.5F, 0.25F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, -0.8F, 0F, 0.25F, -0.8F, 0F, 0.25F); // Box 1
		grenadeModel[3].setRotationPoint(0F, 0F, 0F);

		grenadeModel[4].addShapeBox(-1.5F, -0.4F, -0.5F, 3, 2, 1, 0F, 0.25F, -0.5F, 0F, 0.25F, -0.5F, 0F, 0.25F, -0.5F, 0F, 0.25F, -0.5F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0F); // Box 2
		grenadeModel[4].setRotationPoint(0F, 0F, 0F);

		grenadeModel[5].addShapeBox(-1.5F, -0.4F, -1.5F, 3, 2, 1, 0F, -0.8F, -0.5F, 0.25F, -0.8F, -0.5F, 0.25F, 0.25F, -0.5F, 0F, 0.25F, -0.5F, 0F, -0.8F, 0F, 0.25F, -0.8F, 0F, 0.25F, 0.25F, 0F, 0F, 0.25F, 0F, 0F); // Box 3
		grenadeModel[5].setRotationPoint(0F, 0F, 0F);

		grenadeModel[6].addShapeBox(-1.5F, -1F, 0.5F, 3, 1, 1, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -1F, -0.5F, -0.5F, -1F, -0.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 6
		grenadeModel[6].setRotationPoint(0F, 0F, 0F);

		grenadeModel[7].addShapeBox(-1.5F, -1F, -0.5F, 3, 1, 1, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 8
		grenadeModel[7].setRotationPoint(0F, 0F, 0F);

		grenadeModel[8].addShapeBox(-1.5F, -1F, -1.5F, 3, 1, 1, 0F, -1F, -0.5F, -0.5F, -1F, -0.5F, -0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 9
		grenadeModel[8].setRotationPoint(0F, 0F, 0F);

		grenadeModel[9].addShapeBox(-1.5F, 5F, -1.5F, 3, 1, 1, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, -0.75F, -0.25F, -1F, -0.75F, -0.25F, -0.25F, -0.75F, 0F, -0.25F, -0.75F, 0F); // Box 0
		grenadeModel[9].setRotationPoint(0F, 0F, 0F);

		grenadeModel[10].addShapeBox(-1.5F, 5F, -0.5F, 3, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.25F, -0.75F, 0F, -0.25F, -0.75F, 0F, -0.25F, -0.75F, 0F, -0.25F, -0.75F, 0F); // Box 1
		grenadeModel[10].setRotationPoint(0F, 0F, 0F);

		grenadeModel[11].addShapeBox(-1.5F, 5F, 0.5F, 3, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -0.25F, -0.75F, 0F, -0.25F, -0.75F, 0F, -1F, -0.75F, -0.25F, -1F, -0.75F, -0.25F); // Box 2
		grenadeModel[11].setRotationPoint(0F, 0F, 0F);

		grenadeModel[12].addBox(-0.5F, -1.5F, -0.5F, 1, 1, 1, 0F); // Box 3
		grenadeModel[12].setRotationPoint(0F, 0F, 0F);

		for(int i = 0; i < 13; i++)
			grenadeModel[i].rotateAngleZ = 3.14159265F;
	}

	@Override
	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
	{
		for(ModelRendererTurbo mineModelBit : grenadeModel)
			mineModelBit.render(f5);
	}
}