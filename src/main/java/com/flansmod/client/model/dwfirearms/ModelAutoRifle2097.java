package com.flansmod.client.model.dwfirearms; //Path where the model is located

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelAutoRifle2097 extends ModelGun //Same as Filename
{
	int textureX = 512;
	int textureY = 256;

	public ModelAutoRifle2097() //Same as Filename
	{
		gunModel = new ModelRendererTurbo[112];
		gunModel[0] = new ModelRendererTurbo(this, 120, 53, textureX, textureY); // Box 5
		gunModel[1] = new ModelRendererTurbo(this, 1, 17, textureX, textureY); // Box 27
		gunModel[2] = new ModelRendererTurbo(this, 1, 56, textureX, textureY); // Box 28
		gunModel[3] = new ModelRendererTurbo(this, 32, 49, textureX, textureY); // Box 29
		gunModel[4] = new ModelRendererTurbo(this, 57, 20, textureX, textureY); // Box 30
		gunModel[5] = new ModelRendererTurbo(this, 32, 49, textureX, textureY); // Box 31
		gunModel[6] = new ModelRendererTurbo(this, 40, 17, textureX, textureY); // Box 32
		gunModel[7] = new ModelRendererTurbo(this, 49, 49, textureX, textureY); // Box 34
		gunModel[8] = new ModelRendererTurbo(this, 49, 39, textureX, textureY); // Box 39
		gunModel[9] = new ModelRendererTurbo(this, 1, 37, textureX, textureY); // Box 40
		gunModel[10] = new ModelRendererTurbo(this, 1, 47, textureX, textureY); // Box 41
		gunModel[11] = new ModelRendererTurbo(this, 32, 37, textureX, textureY); // Box 42
		gunModel[12] = new ModelRendererTurbo(this, 120, 64, textureX, textureY); // Box 0
		gunModel[13] = new ModelRendererTurbo(this, 120, 14, textureX, textureY); // Box 1
		gunModel[14] = new ModelRendererTurbo(this, 120, 1, textureX, textureY); // Box 2
		gunModel[15] = new ModelRendererTurbo(this, 120, 1, textureX, textureY); // Box 3
		gunModel[16] = new ModelRendererTurbo(this, 120, 49, textureX, textureY); // Box 4
		gunModel[17] = new ModelRendererTurbo(this, 120, 45, textureX, textureY); // Box 5
		gunModel[18] = new ModelRendererTurbo(this, 158, 47, textureX, textureY); // Box 6
		gunModel[19] = new ModelRendererTurbo(this, 149, 47, textureX, textureY); // Box 7
		gunModel[20] = new ModelRendererTurbo(this, 32, 67, textureX, textureY); // Box 11
		gunModel[21] = new ModelRendererTurbo(this, 32, 67, textureX, textureY); // Box 16
		gunModel[22] = new ModelRendererTurbo(this, 32, 88, textureX, textureY); // Box 21
		gunModel[23] = new ModelRendererTurbo(this, 32, 83, textureX, textureY); // Box 22
		gunModel[24] = new ModelRendererTurbo(this, 32, 83, textureX, textureY); // Box 23
		gunModel[25] = new ModelRendererTurbo(this, 43, 83, textureX, textureY); // Box 24
		gunModel[26] = new ModelRendererTurbo(this, 43, 83, textureX, textureY); // Box 25
		gunModel[27] = new ModelRendererTurbo(this, 19, 1, textureX, textureY); // Box 26
		gunModel[28] = new ModelRendererTurbo(this, 54, 1, textureX, textureY); // Box 27
		gunModel[29] = new ModelRendererTurbo(this, 54, 1, textureX, textureY); // Box 28
		gunModel[30] = new ModelRendererTurbo(this, 6, 6, textureX, textureY); // Box 31
		gunModel[31] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 32
		gunModel[32] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 33
		gunModel[33] = new ModelRendererTurbo(this, 61, 12, textureX, textureY); // Box 34
		gunModel[34] = new ModelRendererTurbo(this, 6, 1, textureX, textureY); // Box 35
		gunModel[35] = new ModelRendererTurbo(this, 1, 13, textureX, textureY); // Box 36
		gunModel[36] = new ModelRendererTurbo(this, 1, 13, textureX, textureY); // Box 38
		gunModel[37] = new ModelRendererTurbo(this, 229, 1, textureX, textureY); // Box 39
		gunModel[38] = new ModelRendererTurbo(this, 229, 1, textureX, textureY); // Box 40
		gunModel[39] = new ModelRendererTurbo(this, 229, 15, textureX, textureY); // Box 41
		gunModel[40] = new ModelRendererTurbo(this, 120, 27, textureX, textureY); // Box 43
		gunModel[41] = new ModelRendererTurbo(this, 190, 54, textureX, textureY); // Box 50
		gunModel[42] = new ModelRendererTurbo(this, 181, 64, textureX, textureY); // Box 51
		gunModel[43] = new ModelRendererTurbo(this, 190, 54, textureX, textureY); // Box 55
		gunModel[44] = new ModelRendererTurbo(this, 181, 64, textureX, textureY); // Box 56
		gunModel[45] = new ModelRendererTurbo(this, 190, 54, textureX, textureY); // Box 57
		gunModel[46] = new ModelRendererTurbo(this, 181, 64, textureX, textureY); // Box 58
		gunModel[47] = new ModelRendererTurbo(this, 181, 64, textureX, textureY); // Box 59
		gunModel[48] = new ModelRendererTurbo(this, 190, 54, textureX, textureY); // Box 60
		gunModel[49] = new ModelRendererTurbo(this, 190, 54, textureX, textureY); // Box 61
		gunModel[50] = new ModelRendererTurbo(this, 181, 64, textureX, textureY); // Box 62
		gunModel[51] = new ModelRendererTurbo(this, 181, 64, textureX, textureY); // Box 63
		gunModel[52] = new ModelRendererTurbo(this, 190, 54, textureX, textureY); // Box 64
		gunModel[53] = new ModelRendererTurbo(this, 73, 2, textureX, textureY); // Box 72
		gunModel[54] = new ModelRendererTurbo(this, 185, 30, textureX, textureY); // Box 73
		gunModel[55] = new ModelRendererTurbo(this, 185, 39, textureX, textureY); // Box 74
		gunModel[56] = new ModelRendererTurbo(this, 185, 30, textureX, textureY); // Box 75
		gunModel[57] = new ModelRendererTurbo(this, 209, 54, textureX, textureY); // Box 76
		gunModel[58] = new ModelRendererTurbo(this, 238, 53, textureX, textureY); // Box 77
		gunModel[59] = new ModelRendererTurbo(this, 209, 54, textureX, textureY); // Box 78
		gunModel[60] = new ModelRendererTurbo(this, 73, 2, textureX, textureY); // Box 79
		gunModel[61] = new ModelRendererTurbo(this, 209, 54, textureX, textureY); // Box 80
		gunModel[62] = new ModelRendererTurbo(this, 238, 53, textureX, textureY); // Box 81
		gunModel[63] = new ModelRendererTurbo(this, 209, 54, textureX, textureY); // Box 82
		gunModel[64] = new ModelRendererTurbo(this, 82, 5, textureX, textureY); // Box 83
		gunModel[65] = new ModelRendererTurbo(this, 229, 1, textureX, textureY); // Box 84
		gunModel[66] = new ModelRendererTurbo(this, 229, 15, textureX, textureY); // Box 85
		gunModel[67] = new ModelRendererTurbo(this, 229, 1, textureX, textureY); // Box 86
		gunModel[68] = new ModelRendererTurbo(this, 120, 27, textureX, textureY); // Box 93
		gunModel[69] = new ModelRendererTurbo(this, 120, 36, textureX, textureY); // Box 94
		gunModel[70] = new ModelRendererTurbo(this, 95, 5, textureX, textureY); // Box 105
		gunModel[71] = new ModelRendererTurbo(this, 95, 5, textureX, textureY); // Box 106
		gunModel[72] = new ModelRendererTurbo(this, 95, 5, textureX, textureY); // Box 107
		gunModel[73] = new ModelRendererTurbo(this, 95, 5, textureX, textureY); // Box 108
		gunModel[74] = new ModelRendererTurbo(this, 82, 5, textureX, textureY); // Box 109
		gunModel[75] = new ModelRendererTurbo(this, 79, 1, textureX, textureY); // Box 110
		gunModel[76] = new ModelRendererTurbo(this, 82, 5, textureX, textureY); // Box 111
		gunModel[77] = new ModelRendererTurbo(this, 30, 11, textureX, textureY); // Box 112
		gunModel[78] = new ModelRendererTurbo(this, 19, 10, textureX, textureY); // Box 113
		gunModel[79] = new ModelRendererTurbo(this, 48, 10, textureX, textureY); // Box 114
		gunModel[80] = new ModelRendererTurbo(this, 48, 10, textureX, textureY); // Box 115
		gunModel[81] = new ModelRendererTurbo(this, 43, 12, textureX, textureY); // Box 116
		gunModel[82] = new ModelRendererTurbo(this, 43, 12, textureX, textureY); // Box 117
		gunModel[83] = new ModelRendererTurbo(this, 100, 41, textureX, textureY); // canister1
		gunModel[84] = new ModelRendererTurbo(this, 74, 21, textureX, textureY); // canister2
		gunModel[85] = new ModelRendererTurbo(this, 74, 30, textureX, textureY); // canister3
		gunModel[86] = new ModelRendererTurbo(this, 74, 21, textureX, textureY); // canister4
		gunModel[87] = new ModelRendererTurbo(this, 107, 20, textureX, textureY); // meter1
		gunModel[88] = new ModelRendererTurbo(this, 107, 20, textureX, textureY); // meter1-2
		gunModel[89] = new ModelRendererTurbo(this, 107, 30, textureX, textureY); // meter2
		gunModel[90] = new ModelRendererTurbo(this, 89, 46, textureX, textureY); // pipe1
		gunModel[91] = new ModelRendererTurbo(this, 87, 39, textureX, textureY); // pipe2
		gunModel[92] = new ModelRendererTurbo(this, 74, 39, textureX, textureY); // pipe3
		gunModel[93] = new ModelRendererTurbo(this, 267, 50, textureX, textureY); // Box 118
		gunModel[94] = new ModelRendererTurbo(this, 172, 50, textureX, textureY); // Box 119
		gunModel[95] = new ModelRendererTurbo(this, 190, 54, textureX, textureY); // Box 127
		gunModel[96] = new ModelRendererTurbo(this, 190, 54, textureX, textureY); // Box 128
		gunModel[97] = new ModelRendererTurbo(this, 190, 54, textureX, textureY); // Box 129
		gunModel[98] = new ModelRendererTurbo(this, 190, 54, textureX, textureY); // Box 130
		gunModel[99] = new ModelRendererTurbo(this, 190, 54, textureX, textureY); // Box 131
		gunModel[100] = new ModelRendererTurbo(this, 190, 54, textureX, textureY); // Box 132
		gunModel[101] = new ModelRendererTurbo(this, 95, 65, textureX, textureY); // Box 1
		gunModel[102] = new ModelRendererTurbo(this, 95, 55, textureX, textureY); // Box 3
		gunModel[103] = new ModelRendererTurbo(this, 95, 60, textureX, textureY); // Box 4
		gunModel[104] = new ModelRendererTurbo(this, 75, 55, textureX, textureY); // Box 5
		gunModel[105] = new ModelRendererTurbo(this, 86, 55, textureX, textureY); // Box 6
		gunModel[106] = new ModelRendererTurbo(this, 95, 55, textureX, textureY); // Box 7
		gunModel[107] = new ModelRendererTurbo(this, 95, 60, textureX, textureY); // Box 8
		gunModel[108] = new ModelRendererTurbo(this, 75, 55, textureX, textureY); // Box 9
		gunModel[109] = new ModelRendererTurbo(this, 86, 55, textureX, textureY); // Box 10
		gunModel[110] = new ModelRendererTurbo(this, 73, 62, textureX, textureY); // Box 11
		gunModel[111] = new ModelRendererTurbo(this, 73, 62, textureX, textureY); // Box 12

		gunModel[0].addShapeBox(0F, 0F, 0F, 22, 3, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F); // Box 5
		gunModel[0].setRotationPoint(-9F, -14F, -3.5F);

		gunModel[1].addShapeBox(0F, 0F, 0F, 12, 12, 7, 0F, -5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -5F, 0F, 0F, 0F, -3F, 0F, -5F, 0F, 0F, -5F, 0F, 0F, 0F, -3F, 0F); // Box 27
		gunModel[1].setRotationPoint(-9F, -8F, -3.5F);

		gunModel[2].addBox(0F, 0F, 0F, 7, 3, 7, 0F); // Box 28
		gunModel[2].setRotationPoint(-4F, -11F, -3.5F);

		gunModel[3].addShapeBox(0F, 0F, 0F, 1, 3, 7, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 29
		gunModel[3].setRotationPoint(-5F, -11F, -3.5F);

		gunModel[4].addShapeBox(0F, 0F, 0F, 1, 9, 7, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 5F, 0F, -1F, -5F, 0F, 0F, -5F, 0F, 0F, 5F, 0F, -1F); // Box 30
		gunModel[4].setRotationPoint(-5F, -8F, -3.5F);

		gunModel[5].addShapeBox(0F, 0F, 0F, 1, 3, 7, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 31
		gunModel[5].setRotationPoint(3F, -11F, -3.5F);

		gunModel[6].addShapeBox(0F, 0F, 0F, 1, 12, 7, 0F, -5F, 0F, 0F, 5F, 0F, -1F, 5F, 0F, -1F, -5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 32
		gunModel[6].setRotationPoint(-2F, -8F, -3.5F);

		gunModel[7].addShapeBox(0F, 0F, 0F, 1, 1, 7, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, -1F, 1F, 0F, -1F, -1F, 0F, 0F); // Box 34
		gunModel[7].setRotationPoint(-2F, 4F, -3.5F);

		gunModel[8].addShapeBox(0F, 0F, 0F, 1, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 39
		gunModel[8].setRotationPoint(-1F, 5F, -3.5F);

		gunModel[9].addShapeBox(0F, 0F, 0F, 8, 2, 7, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F); // Box 40
		gunModel[9].setRotationPoint(-9F, 5F, -3.5F);

		gunModel[10].addShapeBox(0F, 0F, 0F, 8, 1, 7, 0F, 0F, 3F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 3F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F); // Box 41
		gunModel[10].setRotationPoint(-9F, 4F, -3.5F);

		gunModel[11].addShapeBox(0F, 0F, 0F, 1, 4, 7, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -1F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, -1F); // Box 42
		gunModel[11].setRotationPoint(-10F, 1F, -3.5F);

		gunModel[12].addBox(0F, 0F, 0F, 23, 3, 7, 0F); // Box 0
		gunModel[12].setRotationPoint(-9F, -17F, -3.5F);

		gunModel[13].addBox(0F, 0F, 0F, 40, 3, 9, 0F); // Box 1
		gunModel[13].setRotationPoint(-11F, -21F, -4.5F);

		gunModel[14].addShapeBox(0F, 0F, 0F, 40, 3, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 2
		gunModel[14].setRotationPoint(-11F, -18F, -4.5F);

		gunModel[15].addShapeBox(0F, 0F, 0F, 40, 3, 9, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 3
		gunModel[15].setRotationPoint(-11F, -24F, -4.5F);

		gunModel[16].addBox(0F, 0F, 0F, 13, 2, 1, 0F); // Box 4
		gunModel[16].setRotationPoint(14F, -16.5F, -2.5F);

		gunModel[17].addBox(0F, 0F, 0F, 13, 2, 1, 0F); // Box 5
		gunModel[17].setRotationPoint(14F, -16.5F, 1.5F);

		gunModel[18].addBox(0F, 0F, 0F, 1, 2, 3, 0F); // Box 6
		gunModel[18].setRotationPoint(14F, -16.5F, -1.5F);

		gunModel[19].addBox(0F, 0F, 0F, 1, 2, 3, 0F); // Box 7
		gunModel[19].setRotationPoint(26F, -16.5F, -1.5F);

		gunModel[20].addBox(0F, -9F, -1F, 13, 6, 4, 0F); // Box 11
		gunModel[20].setRotationPoint(14F, -19.5F, 0F);
		gunModel[20].rotateAngleX = -0.55850536F;

		gunModel[21].addBox(0F, -9F, -3F, 13, 6, 4, 0F); // Box 16
		gunModel[21].setRotationPoint(14F, -19.5F, 0F);
		gunModel[21].rotateAngleX = 0.55850536F;

		gunModel[22].addBox(0F, 0F, 0F, 3, 1, 8, 0F); // Box 21
		gunModel[22].setRotationPoint(14.5F, -27.5F, -4F);

		gunModel[23].addShapeBox(0F, 0F, 0F, 3, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 22
		gunModel[23].setRotationPoint(14.5F, -26.5F, -3.5F);

		gunModel[24].addShapeBox(0F, 0F, 0F, 3, 1, 2, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 23
		gunModel[24].setRotationPoint(14.5F, -26.5F, 1.5F);

		gunModel[25].addBox(0F, 0F, 0F, 3, 2, 2, 0F); // Box 24
		gunModel[25].setRotationPoint(14.5F, -25.5F, -3.5F);

		gunModel[26].addBox(0F, 0F, 0F, 3, 2, 2, 0F); // Box 25
		gunModel[26].setRotationPoint(14.5F, -25.5F, 1.5F);

		gunModel[27].addBox(0F, 0F, 0F, 11, 2, 6, 0F); // Box 26
		gunModel[27].setRotationPoint(-2.5F, -24.5F, -3F);

		gunModel[28].addShapeBox(0F, 0F, 0F, 2, 1, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 27
		gunModel[28].setRotationPoint(1.5F, -23F, -3.5F);

		gunModel[29].addShapeBox(0F, 0F, 0F, 2, 1, 7, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 28
		gunModel[29].setRotationPoint(1.5F, -24F, -3.5F);

		gunModel[30].addBox(0F, 0F, 0F, 1, 1, 5, 0F); // Box 31
		gunModel[30].setRotationPoint(7F, -35.5F, -2.5F);

		gunModel[31].addBox(0F, 0F, 0F, 1, 10, 1, 0F); // Box 32
		gunModel[31].setRotationPoint(7F, -34.5F, -2.5F);

		gunModel[32].addBox(0F, 0F, 0F, 1, 10, 1, 0F); // Box 33
		gunModel[32].setRotationPoint(7F, -34.5F, 1.5F);

		gunModel[33].addBox(0F, 0F, 0F, 1, 1, 3, 0F); // Box 34
		gunModel[33].setRotationPoint(7F, -31.5F, -1.5F);

		gunModel[34].addBox(0F, 0F, 0F, 3, 1, 3, 0F); // Box 35
		gunModel[34].setRotationPoint(14.5F, -28.5F, -1.5F);

		gunModel[35].addShapeBox(0F, 0F, 0F, 3, 1, 1, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 36
		gunModel[35].setRotationPoint(14.5F, -29.5F, -1.5F);

		gunModel[36].addShapeBox(0F, 0F, 0F, 3, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 38
		gunModel[36].setRotationPoint(14.5F, -29.5F, 1F);

		gunModel[37].addShapeBox(0F, 0F, 0F, 2, 3, 10, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 39
		gunModel[37].setRotationPoint(29F, -24.5F, -5F);

		gunModel[38].addShapeBox(0F, 0F, 0F, 2, 3, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 40
		gunModel[38].setRotationPoint(29F, -17.5F, -5F);

		gunModel[39].addBox(0F, 0F, 0F, 2, 4, 10, 0F); // Box 41
		gunModel[39].setRotationPoint(29F, -21.5F, -5F);

		gunModel[40].addShapeBox(0F, 0F, 0F, 28, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 43
		gunModel[40].setRotationPoint(31F, -23.5F, -3F);

		gunModel[41].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 50
		gunModel[41].setRotationPoint(33F, -24F, -3.5F);

		gunModel[42].addBox(0F, 0F, 0F, 2, 3, 7, 0F); // Box 51
		gunModel[42].setRotationPoint(33F, -22F, -3.5F);

		gunModel[43].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 55
		gunModel[43].setRotationPoint(37F, -24F, -3.5F);

		gunModel[44].addBox(0F, 0F, 0F, 2, 3, 7, 0F); // Box 56
		gunModel[44].setRotationPoint(37F, -22F, -3.5F);

		gunModel[45].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 57
		gunModel[45].setRotationPoint(45F, -24F, -3.5F);

		gunModel[46].addBox(0F, 0F, 0F, 2, 3, 7, 0F); // Box 58
		gunModel[46].setRotationPoint(45F, -22F, -3.5F);

		gunModel[47].addBox(0F, 0F, 0F, 2, 3, 7, 0F); // Box 59
		gunModel[47].setRotationPoint(41F, -22F, -3.5F);

		gunModel[48].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 60
		gunModel[48].setRotationPoint(41F, -24F, -3.5F);

		gunModel[49].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 61
		gunModel[49].setRotationPoint(53F, -24F, -3.5F);

		gunModel[50].addBox(0F, 0F, 0F, 2, 3, 7, 0F); // Box 62
		gunModel[50].setRotationPoint(53F, -22F, -3.5F);

		gunModel[51].addBox(0F, 0F, 0F, 2, 3, 7, 0F); // Box 63
		gunModel[51].setRotationPoint(49F, -22F, -3.5F);

		gunModel[52].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 64
		gunModel[52].setRotationPoint(49F, -24F, -3.5F);

		gunModel[53].addShapeBox(0F, 0F, 0F, 1, 4, 3, 0F, -2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 72
		gunModel[53].setRotationPoint(60F, -28F, -1.5F);

		gunModel[54].addShapeBox(0F, 0F, 0F, 35, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 73
		gunModel[54].setRotationPoint(59F, -23.5F, -3F);

		gunModel[55].addBox(0F, 0F, 0F, 35, 2, 6, 0F); // Box 74
		gunModel[55].setRotationPoint(59F, -21.5F, -3F);

		gunModel[56].addShapeBox(0F, 0F, 0F, 35, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 75
		gunModel[56].setRotationPoint(59F, -19.5F, -3F);

		gunModel[57].addShapeBox(0F, 0F, 0F, 7, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 76
		gunModel[57].setRotationPoint(60F, -24F, -3.5F);

		gunModel[58].addBox(0F, 0F, 0F, 7, 3, 7, 0F); // Box 77
		gunModel[58].setRotationPoint(60F, -22F, -3.5F);

		gunModel[59].addShapeBox(0F, 0F, 0F, 7, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 78
		gunModel[59].setRotationPoint(60F, -19F, -3.5F);

		gunModel[60].addShapeBox(0F, 0F, 0F, 1, 4, 3, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 79
		gunModel[60].setRotationPoint(66F, -28F, -1.5F);

		gunModel[61].addShapeBox(0F, 0F, 0F, 7, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 80
		gunModel[61].setRotationPoint(82F, -24F, -3.5F);

		gunModel[62].addBox(0F, 0F, 0F, 7, 3, 7, 0F); // Box 81
		gunModel[62].setRotationPoint(82F, -22F, -3.5F);

		gunModel[63].addShapeBox(0F, 0F, 0F, 7, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 82
		gunModel[63].setRotationPoint(82F, -19F, -3.5F);

		gunModel[64].addBox(0F, 0F, 0F, 3, 1, 3, 0F); // Box 83
		gunModel[64].setRotationPoint(62F, -29F, -1.5F);

		gunModel[65].addShapeBox(0F, 0F, 0F, 2, 3, 10, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 84
		gunModel[65].setRotationPoint(-13F, -24.5F, -5F);

		gunModel[66].addBox(0F, 0F, 0F, 2, 4, 10, 0F); // Box 85
		gunModel[66].setRotationPoint(-13F, -21.5F, -5F);

		gunModel[67].addShapeBox(0F, 0F, 0F, 2, 3, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 86
		gunModel[67].setRotationPoint(-13F, -17.5F, -5F);

		gunModel[68].addShapeBox(0F, 0F, 0F, 28, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 93
		gunModel[68].setRotationPoint(31F, -19.5F, -3F);

		gunModel[69].addBox(0F, 0F, 0F, 28, 2, 6, 0F); // Box 94
		gunModel[69].setRotationPoint(31F, -21.5F, -3F);

		gunModel[70].addShapeBox(0F, 0F, 0F, 3, 3, 1, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 105
		gunModel[70].setRotationPoint(62F, -31F, -1.5F);

		gunModel[71].addShapeBox(0F, 0F, 0F, 3, 3, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 106
		gunModel[71].setRotationPoint(62F, -34F, -1.5F);

		gunModel[72].addShapeBox(0F, 0F, 0F, 3, 3, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 107
		gunModel[72].setRotationPoint(62F, -31F, 2.5F);

		gunModel[73].addShapeBox(0F, 0F, 0F, 3, 3, 1, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 108
		gunModel[73].setRotationPoint(62F, -34F, 2.5F);

		gunModel[74].addBox(0F, 0F, 0F, 3, 1, 3, 0F); // Box 109
		gunModel[74].setRotationPoint(62F, -34F, -1.5F);

		gunModel[75].addBox(0F, 0F, 0F, 1, 2, 1, 0F); // Box 110
		gunModel[75].setRotationPoint(63F, -31F, -0.5F);

		gunModel[76].addBox(0F, 0F, 0F, 3, 1, 3, 0F); // Box 111
		gunModel[76].setRotationPoint(62F, -28F, -1.5F);

		gunModel[77].addBox(0F, 0F, 0F, 3, 2, 3, 0F); // Box 112
		gunModel[77].setRotationPoint(86F, -17F, -1.5F);

		gunModel[78].addBox(0F, 0F, 0F, 2, 3, 3, 0F); // Box 113
		gunModel[78].setRotationPoint(82F, -17F, -1.5F);

		gunModel[79].addShapeBox(0F, 0F, 0F, 1, 1, 5, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 114
		gunModel[79].setRotationPoint(82.5F, -14F, -2.5F);

		gunModel[80].addShapeBox(0F, 0F, 0F, 1, 1, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 115
		gunModel[80].setRotationPoint(82.5F, -10F, -2.5F);

		gunModel[81].addBox(0F, 0F, 0F, 1, 3, 1, 0F); // Box 116
		gunModel[81].setRotationPoint(82.5F, -13F, -2.5F);

		gunModel[82].addBox(0F, 0F, 0F, 1, 3, 1, 0F); // Box 117
		gunModel[82].setRotationPoint(82.5F, -13F, 1.5F);

		gunModel[83].addBox(0F, 0F, 0F, 6, 6, 3, 0F); // canister1
		gunModel[83].setRotationPoint(-5F, -23F, -6F);

		gunModel[84].addShapeBox(0F, 0F, 0F, 10, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // canister2
		gunModel[84].setRotationPoint(-6F, -19F, -10.5F);

		gunModel[85].addBox(0F, 0F, 0F, 10, 2, 6, 0F); // canister3
		gunModel[85].setRotationPoint(-6F, -21F, -10.5F);

		gunModel[86].addShapeBox(0F, 0F, 0F, 10, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // canister4
		gunModel[86].setRotationPoint(-6F, -23F, -10.5F);

		gunModel[87].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // meter1
		gunModel[87].setRotationPoint(-4F, -25F, -9.5F);

		gunModel[88].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // meter1-2
		gunModel[88].setRotationPoint(-4F, -30F, -9.5F);

		gunModel[89].addBox(0F, 0F, 0F, 2, 3, 7, 0F); // meter2
		gunModel[89].setRotationPoint(-4F, -28F, -9.5F);

		gunModel[90].addBox(0F, 0F, 0F, 3, 2, 2, 0F); // pipe1
		gunModel[90].setRotationPoint(4F, -21F, -8.5F);

		gunModel[91].addBox(0F, 0F, 0F, 2, 2, 4, 0F); // pipe2
		gunModel[91].setRotationPoint(7F, -21F, -8.5F);

		gunModel[92].addBox(0F, 0F, 0F, 4, 4, 2, 0F); // pipe3
		gunModel[92].setRotationPoint(6F, -22F, -5F);

		gunModel[93].addBox(0F, 0F, 0F, 2, 6, 7, 0F); // Box 118
		gunModel[93].setRotationPoint(27F, -17F, -3.5F);

		gunModel[94].addBox(0F, 0F, 0F, 2, 3, 4, 0F); // Box 119
		gunModel[94].setRotationPoint(29F, -15F, -2F);

		gunModel[95].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 127
		gunModel[95].setRotationPoint(53F, -19F, -3.5F);

		gunModel[96].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 128
		gunModel[96].setRotationPoint(49F, -19F, -3.5F);

		gunModel[97].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 129
		gunModel[97].setRotationPoint(45F, -19F, -3.5F);

		gunModel[98].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 130
		gunModel[98].setRotationPoint(41F, -19F, -3.5F);

		gunModel[99].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 131
		gunModel[99].setRotationPoint(37F, -19F, -3.5F);

		gunModel[100].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 132
		gunModel[100].setRotationPoint(33F, -19F, -3.5F);

		gunModel[101].addBox(0F, 0F, 0F, 6, 3, 3, 0F); // Box 1
		gunModel[101].setRotationPoint(60F, -17F, -1.5F);

		gunModel[102].addBox(0F, 0F, 0F, 12, 2, 2, 0F); // Box 3
		gunModel[102].setRotationPoint(62F, -16.5F, -0.5F);
		gunModel[102].rotateAngleY = 0.34906585F;

		gunModel[103].addShapeBox(10F, 0F, 0F, 10, 2, 2, 0F, 0F, -0.25F, -0.25F, 0F, -0.25F, -0.25F, 0F, -0.25F, -0.25F, 0F, -0.25F, -0.25F, 0F, -0.25F, -0.25F, 0F, -0.25F, -0.25F, 0F, -0.25F, -0.25F, 0F, -0.25F, -0.25F); // Box 4
		gunModel[103].setRotationPoint(64F, -16.5F, -0.5F);
		gunModel[103].rotateAngleY = 0.34906585F;

		gunModel[104].addBox(19F, 0F, 0F, 2, 3, 3, 0F); // Box 5
		gunModel[104].setRotationPoint(64F, -17F, -1F);
		gunModel[104].rotateAngleY = 0.34906585F;

		gunModel[105].addShapeBox(21F, 0F, 0F, 1, 3, 3, 0F, 0F, 0F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, 0F); // Box 6
		gunModel[105].setRotationPoint(64F, -17F, -1F);
		gunModel[105].rotateAngleY = 0.34906585F;

		gunModel[106].addBox(0F, 0F, 0F, 12, 2, 2, 0F); // Box 7
		gunModel[106].setRotationPoint(62F, -16.5F, -1.5F);
		gunModel[106].rotateAngleY = -0.34906585F;

		gunModel[107].addShapeBox(10F, 0F, 0F, 10, 2, 2, 0F, 0F, -0.25F, -0.25F, 0F, -0.25F, -0.25F, 0F, -0.25F, -0.25F, 0F, -0.25F, -0.25F, 0F, -0.25F, -0.25F, 0F, -0.25F, -0.25F, 0F, -0.25F, -0.25F, 0F, -0.25F, -0.25F); // Box 8
		gunModel[107].setRotationPoint(64F, -16.5F, -1.5F);
		gunModel[107].rotateAngleY = -0.34906585F;

		gunModel[108].addBox(19F, 0F, 0F, 2, 3, 3, 0F); // Box 9
		gunModel[108].setRotationPoint(64F, -17F, -2F);
		gunModel[108].rotateAngleY = -0.34906585F;

		gunModel[109].addShapeBox(21F, 0F, 0F, 1, 3, 3, 0F, 0F, 0F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, 0F); // Box 10
		gunModel[109].setRotationPoint(64F, -17F, -2F);
		gunModel[109].rotateAngleY = -0.34906585F;

		gunModel[110].addShapeBox(0F, 0F, 0F, 2, 1, 4, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 11
		gunModel[110].setRotationPoint(61F, -16.5F, -2F);

		gunModel[111].addShapeBox(0F, 0F, 0F, 2, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 12
		gunModel[111].setRotationPoint(61F, -15.5F, -2F);


		defaultStockModel = new ModelRendererTurbo[22];
		defaultStockModel[0] = new ModelRendererTurbo(this, 59, 141, textureX, textureY); // Box 87
		defaultStockModel[1] = new ModelRendererTurbo(this, 90, 141, textureX, textureY); // Box 88
		defaultStockModel[2] = new ModelRendererTurbo(this, 59, 141, textureX, textureY); // Box 89
		defaultStockModel[3] = new ModelRendererTurbo(this, 1, 138, textureX, textureY); // Box 96
		defaultStockModel[4] = new ModelRendererTurbo(this, 30, 141, textureX, textureY); // Box 97
		defaultStockModel[5] = new ModelRendererTurbo(this, 30, 141, textureX, textureY); // Box 98
		defaultStockModel[6] = new ModelRendererTurbo(this, 1, 119, textureX, textureY); // Box 99
		defaultStockModel[7] = new ModelRendererTurbo(this, 60, 125, textureX, textureY); // Box 100
		defaultStockModel[8] = new ModelRendererTurbo(this, 60, 125, textureX, textureY); // Box 101
		defaultStockModel[9] = new ModelRendererTurbo(this, 1, 154, textureX, textureY); // Box 102
		defaultStockModel[10] = new ModelRendererTurbo(this, 26, 154, textureX, textureY); // Box 103
		defaultStockModel[11] = new ModelRendererTurbo(this, 26, 154, textureX, textureY); // Box 104
		defaultStockModel[12] = new ModelRendererTurbo(this, 133, 114, textureX, textureY); // Box 133
		defaultStockModel[13] = new ModelRendererTurbo(this, 120, 112, textureX, textureY); // Box 134
		defaultStockModel[14] = new ModelRendererTurbo(this, 170, 119, textureX, textureY); // Box 135
		defaultStockModel[15] = new ModelRendererTurbo(this, 170, 112, textureX, textureY); // Box 136
		defaultStockModel[16] = new ModelRendererTurbo(this, 120, 130, textureX, textureY); // Box 137
		defaultStockModel[17] = new ModelRendererTurbo(this, 142, 136, textureX, textureY); // Box 138
		defaultStockModel[18] = new ModelRendererTurbo(this, 131, 130, textureX, textureY); // Box 139
		defaultStockModel[19] = new ModelRendererTurbo(this, 163, 132, textureX, textureY); // Box 140
		defaultStockModel[20] = new ModelRendererTurbo(this, 186, 133, textureX, textureY); // Box 141
		defaultStockModel[21] = new ModelRendererTurbo(this, 163, 132, textureX, textureY); // Box 142

		defaultStockModel[0].addShapeBox(0F, 0F, 0F, 6, 3, 9, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 87
		defaultStockModel[0].setRotationPoint(-19F, -24F, -4.5F);

		defaultStockModel[1].addShapeBox(0F, 0F, 0F, 6, 3, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F); // Box 88
		defaultStockModel[1].setRotationPoint(-19F, -21F, -4.5F);

		defaultStockModel[2].addShapeBox(0F, 0F, 0F, 6, 3, 9, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 3F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 3F, -3F); // Box 89
		defaultStockModel[2].setRotationPoint(-19F, -18F, -4.5F);

		defaultStockModel[3].addShapeBox(0F, 0F, 0F, 5, 6, 9, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 6F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 6F, 0F); // Box 96
		defaultStockModel[3].setRotationPoint(-24F, -21F, -4.5F);

		defaultStockModel[4].addShapeBox(0F, 0F, 0F, 5, 3, 9, 0F, 0F, -6F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -6F, 0F, 0F, 6F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 6F, -3F); // Box 97
		defaultStockModel[4].setRotationPoint(-24F, -15F, -4.5F);

		defaultStockModel[5].addShapeBox(0F, 0F, 0F, 5, 3, 9, 0F, 0F, -3F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, -3F, -3F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F); // Box 98
		defaultStockModel[5].setRotationPoint(-24F, -24F, -4.5F);

		defaultStockModel[6].addShapeBox(0F, 0F, 0F, 20, 9, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F); // Box 99
		defaultStockModel[6].setRotationPoint(-44F, -18F, -4.5F);

		defaultStockModel[7].addShapeBox(0F, 0F, 0F, 20, 3, 9, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 100
		defaultStockModel[7].setRotationPoint(-44F, -21F, -4.5F);

		defaultStockModel[8].addShapeBox(0F, 0F, 0F, 20, 3, 9, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, -4F, -3F, 0F, -4F, -3F, 0F, 0F, -3F); // Box 101
		defaultStockModel[8].setRotationPoint(-44F, -5F, -4.5F);

		defaultStockModel[9].addShapeBox(0F, 0F, 0F, 3, 13, 9, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F); // Box 102
		defaultStockModel[9].setRotationPoint(-47F, -18F, -4.5F);

		defaultStockModel[10].addShapeBox(0F, 0F, 0F, 3, 3, 9, 0F, -1F, -1F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, -1F, -1F, -3F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F); // Box 103
		defaultStockModel[10].setRotationPoint(-47F, -21F, -4.5F);

		defaultStockModel[11].addShapeBox(0F, 0F, 0F, 3, 3, 9, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, -1F, -1F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, -1F, -1F, -3F); // Box 104
		defaultStockModel[11].setRotationPoint(-47F, -5F, -4.5F);

		defaultStockModel[12].addBox(0F, 0F, 0F, 15, 12, 3, 0F); // Box 133
		defaultStockModel[12].setRotationPoint(-41F, -18F, -7.5F);

		defaultStockModel[13].addBox(0F, 0F, 0F, 2, 13, 4, 0F); // Box 134
		defaultStockModel[13].setRotationPoint(-42F, -18.5F, -8F);

		defaultStockModel[14].addShapeBox(0F, 0F, 0F, 1, 7, 3, 0F, 0F, 0F, -0.25F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -0.25F); // Box 135
		defaultStockModel[14].setRotationPoint(-26F, -17.5F, -7.5F);

		defaultStockModel[15].addShapeBox(0F, 0F, 0F, 1, 3, 3, 0F, 0F, 0F, -0.25F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -0.25F, 0F, 0F, -0.75F, -0.5F, -0.5F, -1.25F, -0.5F, -0.5F, -1.25F, 0F, 0F, -0.75F); // Box 136
		defaultStockModel[15].setRotationPoint(-26F, -10.5F, -7.5F);

		defaultStockModel[16].addBox(0F, 0F, 0F, 4, 12, 1, 0F); // Box 137
		defaultStockModel[16].setRotationPoint(-36F, -18F, -8.5F);

		defaultStockModel[17].addShapeBox(0F, 0F, 0F, 4, 1, 6, 0F, 0F, -4F, 1F, 0F, -4F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 138
		defaultStockModel[17].setRotationPoint(-36F, -22F, -7.5F);

		defaultStockModel[18].addBox(0F, 0F, 0F, 4, 12, 1, 0F); // Box 139
		defaultStockModel[18].setRotationPoint(-36F, -18F, 4.5F);

		defaultStockModel[19].addShapeBox(0F, 0F, 0F, 4, 4, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 140
		defaultStockModel[19].setRotationPoint(-36F, -22F, -1.5F);

		defaultStockModel[20].addShapeBox(0F, 0F, 0F, 4, 1, 6, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 1F, 0F, -3F, 1F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 141
		defaultStockModel[20].setRotationPoint(-36F, -4F, -7.5F);

		defaultStockModel[21].addShapeBox(0F, 0F, 0F, 4, 3, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, -4F); // Box 142
		defaultStockModel[21].setRotationPoint(-36F, -6F, -1.5F);


		defaultGripModel = new ModelRendererTurbo[10];
		defaultGripModel[0] = new ModelRendererTurbo(this, 120, 75, textureX, textureY); // Box 45
		defaultGripModel[1] = new ModelRendererTurbo(this, 195, 76, textureX, textureY); // Box 45
		defaultGripModel[2] = new ModelRendererTurbo(this, 120, 101, textureX, textureY); // Box 45
		defaultGripModel[3] = new ModelRendererTurbo(this, 149, 88, textureX, textureY); // Box 45
		defaultGripModel[4] = new ModelRendererTurbo(this, 120, 88, textureX, textureY); // Box 45
		defaultGripModel[5] = new ModelRendererTurbo(this, 176, 87, textureX, textureY); // Box 45
		defaultGripModel[6] = new ModelRendererTurbo(this, 223, 87, textureX, textureY); // Box 45
		defaultGripModel[7] = new ModelRendererTurbo(this, 279, 1, textureX, textureY); // Box 45
		defaultGripModel[8] = new ModelRendererTurbo(this, 254, 1, textureX, textureY); // Box 45
		defaultGripModel[9] = new ModelRendererTurbo(this, 254, 1, textureX, textureY); // Box 45

		defaultGripModel[0].addBox(0F, 0F, 0F, 26, 2, 9, 0F); // Box 45
		defaultGripModel[0].setRotationPoint(31F, -20.5F, -4.5F);

		defaultGripModel[1].addBox(0F, 0F, 0F, 26, 1, 9, 0F); // Box 45
		defaultGripModel[1].setRotationPoint(31F, -16.5F, -4.5F);

		defaultGripModel[2].addBox(0F, 0F, 0F, 18, 2, 8, 0F); // Box 45
		defaultGripModel[2].setRotationPoint(35F, -18.5F, -4F);

		defaultGripModel[3].addBox(0F, 0F, 0F, 4, 2, 9, 0F); // Box 45
		defaultGripModel[3].setRotationPoint(31F, -18.5F, -4.5F);

		defaultGripModel[4].addBox(0F, 0F, 0F, 4, 2, 9, 0F); // Box 45
		defaultGripModel[4].setRotationPoint(53F, -18.5F, -4.5F);

		defaultGripModel[5].addShapeBox(0F, 0F, 0F, 14, 3, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 45
		defaultGripModel[5].setRotationPoint(31F, -15.5F, -4.5F);

		defaultGripModel[6].addShapeBox(0F, 0F, 0F, 12, 3, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, -1F, -2F, 0F, -1F, -2F, 0F, 0F, -2F); // Box 45
		defaultGripModel[6].setRotationPoint(45F, -15.5F, -4.5F);

		defaultGripModel[7].addBox(0F, 0F, 0F, 2, 5, 10, 0F); // Box 45
		defaultGripModel[7].setRotationPoint(57F, -21.5F, -5F);

		defaultGripModel[8].addShapeBox(0F, 0F, 0F, 2, 3, 10, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 45
		defaultGripModel[8].setRotationPoint(57F, -24.5F, -5F);

		defaultGripModel[9].addShapeBox(0F, 0F, 0F, 2, 3, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 45
		defaultGripModel[9].setRotationPoint(57F, -16.5F, -5F);


		ammoModel = new ModelRendererTurbo[8];
		ammoModel[0] = new ModelRendererTurbo(this, 1, 67, textureX, textureY); // Box 12
		ammoModel[1] = new ModelRendererTurbo(this, 32, 78, textureX, textureY); // Box 13
		ammoModel[2] = new ModelRendererTurbo(this, 53, 78, textureX, textureY); // Box 14
		ammoModel[3] = new ModelRendererTurbo(this, 1, 86, textureX, textureY); // Box 15
		ammoModel[4] = new ModelRendererTurbo(this, 53, 78, textureX, textureY); // Box 17
		ammoModel[5] = new ModelRendererTurbo(this, 32, 78, textureX, textureY); // Box 18
		ammoModel[6] = new ModelRendererTurbo(this, 1, 93, textureX, textureY); // Box 19
		ammoModel[7] = new ModelRendererTurbo(this, 1, 112, textureX, textureY); // Box 20

		ammoModel[0].addBox(0.5F, -21F, -0.5F, 12, 15, 3, 0F); // Box 12
		ammoModel[0].setRotationPoint(14F, -19.5F, 0F);
		ammoModel[0].rotateAngleX = -0.55850536F;

		ammoModel[1].addShapeBox(1.5F, -6F, -0.5F, 7, 1, 3, 0F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 13
		ammoModel[1].setRotationPoint(14F, -19.5F, 0F);
		ammoModel[1].rotateAngleX = -0.55850536F;

		ammoModel[2].addShapeBox(8.5F, -6F, -0.5F, 3, 1, 3, 0F, 0F, 0F, -0.25F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, -0.25F, 0F, 0F, -1F, -0.5F, -0.25F, -1.25F, -0.5F, -0.25F, -1.25F, 0F, 0F, -1F); // Box 14
		ammoModel[2].setRotationPoint(14F, -19.5F, 0F);
		ammoModel[2].rotateAngleX = -0.55850536F;

		ammoModel[3].addBox(0F, -22F, -1F, 13, 2, 4, 0F); // Box 15
		ammoModel[3].setRotationPoint(14F, -19.5F, 0F);
		ammoModel[3].rotateAngleX = -0.55850536F;

		ammoModel[4].addShapeBox(8.5F, -6F, -2.5F, 3, 1, 3, 0F, 0F, 0F, -0.25F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, -0.25F, 0F, 0F, -1F, -0.5F, -0.25F, -1.25F, -0.5F, -0.25F, -1.25F, 0F, 0F, -1F); // Box 17
		ammoModel[4].setRotationPoint(14F, -19.5F, 0F);
		ammoModel[4].rotateAngleX = 0.55850536F;

		ammoModel[5].addShapeBox(1.5F, -6F, -2.5F, 7, 1, 3, 0F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 18
		ammoModel[5].setRotationPoint(14F, -19.5F, 0F);
		ammoModel[5].rotateAngleX = 0.55850536F;

		ammoModel[6].addBox(0.5F, -21F, -2.5F, 12, 15, 3, 0F); // Box 19
		ammoModel[6].setRotationPoint(14F, -19.5F, 0F);
		ammoModel[6].rotateAngleX = 0.55850536F;

		ammoModel[7].addBox(0F, -22F, -3F, 13, 2, 4, 0F); // Box 20
		ammoModel[7].setRotationPoint(14F, -19.5F, 0F);
		ammoModel[7].rotateAngleX = 0.55850536F;


		slideModel = new ModelRendererTurbo[2];
		slideModel[0] = new ModelRendererTurbo(this, 167, 48, textureX, textureY); // Box 9
		slideModel[1] = new ModelRendererTurbo(this, 179, 58, textureX, textureY); // Box 10

		slideModel[0].addBox(0F, 0F, 0F, 2, 2, 2, 0F); // Box 9
		slideModel[0].setRotationPoint(24F, -15.5F, -1F);

		slideModel[1].addShapeBox(0F, 0F, 0F, 2, 3, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, -2F, 0F, 0F); // Box 10
		slideModel[1].setRotationPoint(24F, -13.5F, -1F);

		animationType = EnumAnimationType.GENERIC;
		gunSlideDistance = 0.3F;

		translateAll(0F, 0F, 0F);


		flipAll();
	}
}