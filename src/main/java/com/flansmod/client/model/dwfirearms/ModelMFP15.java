package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelMFP15 extends ModelGun
{
	int textureX = 512;
	int textureY = 256;

	public ModelMFP15()
	{
		gunModel = new ModelRendererTurbo[10];
		gunModel[0] = new ModelRendererTurbo(this, 1, 14, textureX, textureY); // body1
		gunModel[1] = new ModelRendererTurbo(this, 1, 28, textureX, textureY); // body3
		gunModel[2] = new ModelRendererTurbo(this, 52, 35, textureX, textureY); // body2
		gunModel[3] = new ModelRendererTurbo(this, 1, 40, textureX, textureY); // body4
		gunModel[4] = new ModelRendererTurbo(this, 88, 22, textureX, textureY); // mainBarrelBottom
		gunModel[5] = new ModelRendererTurbo(this, 88, 11, textureX, textureY); // mainBarrelMiddle
		gunModel[6] = new ModelRendererTurbo(this, 88, 1, textureX, textureY); // mainBarrelTop
		gunModel[7] = new ModelRendererTurbo(this, 88, 32, textureX, textureY); // pin1
		gunModel[8] = new ModelRendererTurbo(this, 88, 32, textureX, textureY); // pin1-2
		gunModel[9] = new ModelRendererTurbo(this, 88, 32, textureX, textureY); // pin1-3

		gunModel[0].addBox(0F, 0F, 0F, 25, 4, 9, 0F); // body1
		gunModel[0].setRotationPoint(-7F, -14F, -4.5F);

		gunModel[1].addBox(0F, 0F, 0F, 15, 3, 8, 0F); // body3
		gunModel[1].setRotationPoint(18F, -14F, -4F);

		gunModel[2].addShapeBox(0F, 0F, 0F, 5, 2, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F); // body2
		gunModel[2].setRotationPoint(-12F, -14F, -4.5F);

		gunModel[3].addShapeBox(0F, 0F, 0F, 15, 1, 5, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body4
		gunModel[3].setRotationPoint(18F, -11F, -2.5F);

		gunModel[4].addShapeBox(0F, 0F, 0F, 25, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // mainBarrelBottom
		gunModel[4].setRotationPoint(10F, -18F, -3.5F);

		gunModel[5].addBox(0F, 0F, 0F, 25, 3, 7, 0F); // mainBarrelMiddle
		gunModel[5].setRotationPoint(10F, -21F, -3.5F);

		gunModel[6].addShapeBox(0F, 0F, 0F, 25, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // mainBarrelTop
		gunModel[6].setRotationPoint(10F, -23F, -3.5F);

		gunModel[7].addShapeBox(0F, 0F, 0F, 12, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // pin1
		gunModel[7].setRotationPoint(22F, -15F, -1.5F);

		gunModel[8].addBox(0F, 0F, 0F, 12, 1, 3, 0F); // pin1-2
		gunModel[8].setRotationPoint(22F, -14F, -1.5F);

		gunModel[9].addShapeBox(0F, 0F, 0F, 12, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // pin1-3
		gunModel[9].setRotationPoint(22F, -13F, -1.5F);


		defaultGripModel = new ModelRendererTurbo[3];
		defaultGripModel[0] = new ModelRendererTurbo(this, 1, 73, textureX, textureY); // grip1
		defaultGripModel[1] = new ModelRendererTurbo(this, 1, 47, textureX, textureY); // grip2
		defaultGripModel[2] = new ModelRendererTurbo(this, 1, 87, textureX, textureY); // grip3

		defaultGripModel[0].addShapeBox(0F, 0F, 0F, 13, 4, 9, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F); // grip1
		defaultGripModel[0].setRotationPoint(-7F, -10F, -4.5F);

		defaultGripModel[1].addShapeBox(0F, 0F, 0F, 12, 14, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 4F, 0F, 0F); // grip2
		defaultGripModel[1].setRotationPoint(-6F, -6F, -4.5F);

		defaultGripModel[2].addBox(0F, 0F, 0F, 12, 2, 9, 0F); // grip3
		defaultGripModel[2].setRotationPoint(-10F, 8F, -4.5F);


		ammoModel = new ModelRendererTurbo[4];
		ammoModel[0] = new ModelRendererTurbo(this, 1, 99, textureX, textureY); // clip
		ammoModel[1] = new ModelRendererTurbo(this, 1, 124, textureX, textureY); // clip2
		ammoModel[2] = new ModelRendererTurbo(this, 38, 119, textureX, textureY); // bullet
		ammoModel[3] = new ModelRendererTurbo(this, 38, 114, textureX, textureY); // bulletTip

		ammoModel[0].addShapeBox(0F, 0F, 0F, 10, 16, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4.5F, 0F, 0F, -4.5F, 0F, 0F, -4.5F, 0F, 0F, 4.5F, 0F, 0F); // clip
		ammoModel[0].setRotationPoint(-5F, -5F, -3F);

		ammoModel[1].addBox(0F, 0F, 0F, 12, 3, 7, 0F); // clip2
		ammoModel[1].setRotationPoint(-10F, 11F, -3.5F);

		ammoModel[2].addShapeBox(0F, 0F, 0F, 6, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // bullet
		ammoModel[2].setRotationPoint(-4F, -6F, -1.5F);

		ammoModel[3].addShapeBox(0F, 0F, 0F, 2, 1, 3, 0F, 0F, 0F, -1F, 0F, -0.5F, -1F, 0F, -0.5F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F); // bulletTip
		ammoModel[3].setRotationPoint(2F, -6F, -1.5F);


		slideModel = new ModelRendererTurbo[17];
		slideModel[0] = new ModelRendererTurbo(this, 1, 9, textureX, textureY); // ironSight3-2
		slideModel[1] = new ModelRendererTurbo(this, 1, 9, textureX, textureY); // ironSight3
		slideModel[2] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // ironSight2
		slideModel[3] = new ModelRendererTurbo(this, 14, 9, textureX, textureY); // ironSight1
		slideModel[4] = new ModelRendererTurbo(this, 88, 37, textureX, textureY); // slide1
		slideModel[5] = new ModelRendererTurbo(this, 88, 55, textureX, textureY); // slide2
		slideModel[6] = new ModelRendererTurbo(this, 88, 93, textureX, textureY); // slide3
		slideModel[7] = new ModelRendererTurbo(this, 135, 83, textureX, textureY); // slide4
		slideModel[8] = new ModelRendererTurbo(this, 135, 93, textureX, textureY); // slide4-2
		slideModel[9] = new ModelRendererTurbo(this, 88, 73, textureX, textureY); // slide5
		slideModel[10] = new ModelRendererTurbo(this, 88, 83, textureX, textureY); // slide6
		slideModel[11] = new ModelRendererTurbo(this, 135, 55, textureX, textureY); // Box 28
		slideModel[12] = new ModelRendererTurbo(this, 135, 55, textureX, textureY); // Box 34
		slideModel[13] = new ModelRendererTurbo(this, 135, 55, textureX, textureY); // Box 35
		slideModel[14] = new ModelRendererTurbo(this, 135, 55, textureX, textureY); // Box 36
		slideModel[15] = new ModelRendererTurbo(this, 135, 55, textureX, textureY); // Box 37
		slideModel[16] = new ModelRendererTurbo(this, 135, 55, textureX, textureY); // Box 38

		slideModel[0].addBox(0F, 0F, 0F, 4, 2, 2, 0F); // ironSight3-2
		slideModel[0].setRotationPoint(-11F, -27F, -3F);

		slideModel[1].addBox(0F, 0F, 0F, 4, 2, 2, 0F); // ironSight3
		slideModel[1].setRotationPoint(-11F, -27F, 1F);

		slideModel[2].addBox(0F, 0F, 0F, 6, 1, 6, 0F); // ironSight2
		slideModel[2].setRotationPoint(-11F, -25F, -3F);

		slideModel[3].addShapeBox(0F, 0F, 0F, 4, 2, 1, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // ironSight1
		slideModel[3].setRotationPoint(29F, -26F, -0.5F);

		slideModel[4].addBox(0F, 0F, 0F, 22, 9, 8, 0F); // slide1
		slideModel[4].setRotationPoint(-12F, -23F, -4F);

		slideModel[5].addBox(0F, 0F, 0F, 15, 9, 8, 0F); // slide2
		slideModel[5].setRotationPoint(18F, -23F, -4F);

		slideModel[6].addBox(0F, 0F, 0F, 8, 2, 8, 0F); // slide3
		slideModel[6].setRotationPoint(10F, -16F, -4F);

		slideModel[7].addShapeBox(0F, 0F, 0F, 8, 8, 1, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // slide4
		slideModel[7].setRotationPoint(10F, -24F, -4F);

		slideModel[8].addShapeBox(0F, 0F, 0F, 8, 8, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // slide4-2
		slideModel[8].setRotationPoint(10F, -24F, 3F);

		slideModel[9].addShapeBox(0F, 0F, 0F, 22, 1, 8, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // slide5
		slideModel[9].setRotationPoint(-12F, -24F, -4F);

		slideModel[10].addShapeBox(0F, 0F, 0F, 15, 1, 8, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // slide6
		slideModel[10].setRotationPoint(18F, -24F, -4F);

		slideModel[11].addShapeBox(0F, 0F, 0F, 1, 9, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // Box 28
		slideModel[11].setRotationPoint(-9F, -23F, -4.25F);

		slideModel[12].addShapeBox(0F, 0F, 0F, 1, 9, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // Box 34
		slideModel[12].setRotationPoint(-7F, -23F, -4.25F);

		slideModel[13].addShapeBox(0F, 0F, 0F, 1, 9, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // Box 35
		slideModel[13].setRotationPoint(-5F, -23F, -4.25F);

		slideModel[14].addShapeBox(0F, 0F, 0F, 1, 9, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // Box 36
		slideModel[14].setRotationPoint(-3F, -23F, -4.25F);

		slideModel[15].addShapeBox(0F, 0F, 0F, 1, 9, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // Box 37
		slideModel[15].setRotationPoint(-1F, -23F, -4.25F);

		slideModel[16].addShapeBox(0F, 0F, 0F, 1, 9, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // Box 38
		slideModel[16].setRotationPoint(1F, -23F, -4.25F);

		barrelAttachPoint = new Vector3f(35F /16F, 19.5F /16F, 0F /16F);
		gripAttachPoint = new Vector3f(0 /16F, 10F /16F, 0F /16F);

		gunSlideDistance = 0.8F;
		animationType = EnumAnimationType.CUSTOM;

		/* ----Start of Reload Block---- */
		/* Reload Name: Alternate Pistol Clip */
		rotateGunVertical = 10F;
		translateGun = new Vector3f(0.15F, 0.25F, 0F);

		rotateClipVertical = 5F;
		translateClip = new Vector3f(-0.5F, -3F, 0F);
		/* ----End of Reload Block---- */


		translateAll(0F, 0F, 0F);


		flipAll();
	}
}