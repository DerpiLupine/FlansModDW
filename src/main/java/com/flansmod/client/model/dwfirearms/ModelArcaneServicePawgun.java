package com.flansmod.client.model.dwfirearms; //Path where the model is located

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelArcaneServicePawgun extends ModelGun //Same as Filename
{
	int textureX = 512;
	int textureY = 256;

	public ModelArcaneServicePawgun() //Same as Filename
	{
		gunModel = new ModelRendererTurbo[44];
		gunModel[0] = new ModelRendererTurbo(this, 1, 32, textureX, textureY); // body1
		gunModel[1] = new ModelRendererTurbo(this, 38, 46, textureX, textureY); // body2
		gunModel[2] = new ModelRendererTurbo(this, 1, 92, textureX, textureY); // grip1
		gunModel[3] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // ironSight2
		gunModel[4] = new ModelRendererTurbo(this, 88, 8, textureX, textureY); // mainBarrelMiddle
		gunModel[5] = new ModelRendererTurbo(this, 88, 1, textureX, textureY); // mainBarrelTop
		gunModel[6] = new ModelRendererTurbo(this, 88, 27, textureX, textureY); // pin1
		gunModel[7] = new ModelRendererTurbo(this, 88, 27, textureX, textureY); // pin1-2
		gunModel[8] = new ModelRendererTurbo(this, 88, 27, textureX, textureY); // pin1-3
		gunModel[9] = new ModelRendererTurbo(this, 88, 1, textureX, textureY); // Box 0
		gunModel[10] = new ModelRendererTurbo(this, 42, 57, textureX, textureY); // Box 3
		gunModel[11] = new ModelRendererTurbo(this, 44, 79, textureX, textureY); // Box 4
		gunModel[12] = new ModelRendererTurbo(this, 44, 79, textureX, textureY); // Box 5
		gunModel[13] = new ModelRendererTurbo(this, 139, 96, textureX, textureY); // Box 6
		gunModel[14] = new ModelRendererTurbo(this, 139, 86, textureX, textureY); // Box 16
		gunModel[15] = new ModelRendererTurbo(this, 88, 16, textureX, textureY); // Box 17
		gunModel[16] = new ModelRendererTurbo(this, 1, 22, textureX, textureY); // Box 18
		gunModel[17] = new ModelRendererTurbo(this, 1, 22, textureX, textureY); // Box 19
		gunModel[18] = new ModelRendererTurbo(this, 1, 22, textureX, textureY); // Box 20
		gunModel[19] = new ModelRendererTurbo(this, 1, 22, textureX, textureY); // Box 21
		gunModel[20] = new ModelRendererTurbo(this, 1, 11, textureX, textureY); // Box 26
		gunModel[21] = new ModelRendererTurbo(this, 22, 22, textureX, textureY); // Box 27
		gunModel[22] = new ModelRendererTurbo(this, 1, 45, textureX, textureY); // Box 30
		gunModel[23] = new ModelRendererTurbo(this, 22, 22, textureX, textureY); // Box 31
		gunModel[24] = new ModelRendererTurbo(this, 22, 22, textureX, textureY); // Box 32
		gunModel[25] = new ModelRendererTurbo(this, 1, 57, textureX, textureY); // Box 33
		gunModel[26] = new ModelRendererTurbo(this, 61, 57, textureX, textureY); // Box 34
		gunModel[27] = new ModelRendererTurbo(this, 63, 79, textureX, textureY); // Box 35
		gunModel[28] = new ModelRendererTurbo(this, 63, 79, textureX, textureY); // Box 36
		gunModel[29] = new ModelRendererTurbo(this, 1, 79, textureX, textureY); // Box 37
		gunModel[30] = new ModelRendererTurbo(this, 22, 22, textureX, textureY); // Box 38
		gunModel[31] = new ModelRendererTurbo(this, 22, 22, textureX, textureY); // Box 39
		gunModel[32] = new ModelRendererTurbo(this, 22, 22, textureX, textureY); // Box 40
		gunModel[33] = new ModelRendererTurbo(this, 54, 35, textureX, textureY); // Box 47
		gunModel[34] = new ModelRendererTurbo(this, 54, 35, textureX, textureY); // Box 48
		gunModel[35] = new ModelRendererTurbo(this, 54, 35, textureX, textureY); // Box 49
		gunModel[36] = new ModelRendererTurbo(this, 54, 35, textureX, textureY); // Box 50
		gunModel[37] = new ModelRendererTurbo(this, 119, 27, textureX, textureY); // Box 55
		gunModel[38] = new ModelRendererTurbo(this, 88, 114, textureX, textureY); // Box 70
		gunModel[39] = new ModelRendererTurbo(this, 40, 22, textureX, textureY); // Box 73
		gunModel[40] = new ModelRendererTurbo(this, 40, 27, textureX, textureY); // Box 74
		gunModel[41] = new ModelRendererTurbo(this, 88, 134, textureX, textureY); // Box 78
		gunModel[42] = new ModelRendererTurbo(this, 88, 114, textureX, textureY); // Box 79
		gunModel[43] = new ModelRendererTurbo(this, 88, 134, textureX, textureY); // Box 80

		gunModel[0].addBox(0F, 0F, 0F, 18, 4, 8, 0F); // body1
		gunModel[0].setRotationPoint(-11F, -11F, -4F);

		gunModel[1].addShapeBox(0F, 0F, 0F, 3, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F); // body2
		gunModel[1].setRotationPoint(-14F, -11F, -4F);

		gunModel[2].addShapeBox(0F, 0F, 0F, 13, 4, 8, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F); // grip1
		gunModel[2].setRotationPoint(-10F, -7F, -4F);

		gunModel[3].addBox(0F, 0F, 0F, 21, 2, 7, 0F); // ironSight2
		gunModel[3].setRotationPoint(-9F, -22.5F, -3.5F);

		gunModel[4].addBox(0F, 0F, 0F, 22, 2, 5, 0F); // mainBarrelMiddle
		gunModel[4].setRotationPoint(20F, -19F, -2.5F);

		gunModel[5].addShapeBox(0F, 0F, 0F, 22, 1, 5, 0F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // mainBarrelTop
		gunModel[5].setRotationPoint(20F, -20F, -2.5F);

		gunModel[6].addShapeBox(0F, 0F, 0F, 12, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // pin1
		gunModel[6].setRotationPoint(28.5F, -12F, -1.5F);

		gunModel[7].addBox(0F, 0F, 0F, 12, 1, 3, 0F); // pin1-2
		gunModel[7].setRotationPoint(28.5F, -11F, -1.5F);

		gunModel[8].addShapeBox(0F, 0F, 0F, 12, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // pin1-3
		gunModel[8].setRotationPoint(28.5F, -10F, -1.5F);

		gunModel[9].addShapeBox(0F, 0F, 0F, 22, 1, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F); // Box 0
		gunModel[9].setRotationPoint(20F, -17F, -2.5F);

		gunModel[10].addShapeBox(0F, 0F, 0F, 1, 13, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, -1F, -3F, 0F, -1F, 3F, 0F, 0F); // Box 3
		gunModel[10].setRotationPoint(3F, -3F, -4F);

		gunModel[11].addShapeBox(0F, 0F, 0F, 1, 4, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, -1F, -3F, 0F, -1F, 3F, 0F, 0F); // Box 4
		gunModel[11].setRotationPoint(6F, -7F, -4F);

		gunModel[12].addShapeBox(0F, 0F, 0F, 1, 4, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 5
		gunModel[12].setRotationPoint(0F, 10F, -4F);

		gunModel[13].addBox(0F, 0F, 0F, 15, 1, 6, 0F); // Box 6
		gunModel[13].setRotationPoint(25F, -9F, -3F);

		gunModel[14].addBox(0F, 0F, 0F, 15, 2, 7, 0F); // Box 16
		gunModel[14].setRotationPoint(25F, -11F, -3.5F);

		gunModel[15].addBox(0F, 0F, 0F, 18, 3, 7, 0F); // Box 17
		gunModel[15].setRotationPoint(7F, -11F, -3.5F);

		gunModel[16].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 18
		gunModel[16].setRotationPoint(-9F, -24.5F, -3.5F);

		gunModel[17].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 19
		gunModel[17].setRotationPoint(-3F, -24.5F, -3.5F);

		gunModel[18].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 20
		gunModel[18].setRotationPoint(3F, -24.5F, -3.5F);

		gunModel[19].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 21
		gunModel[19].setRotationPoint(9F, -24.5F, -3.5F);

		gunModel[20].addBox(0F, 0F, 0F, 21, 2, 8, 0F); // Box 26
		gunModel[20].setRotationPoint(-9F, -20.5F, -4F);

		gunModel[21].addShapeBox(0F, 0F, 0F, 2, 8, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, -4F, 0F, 0F); // Box 27
		gunModel[21].setRotationPoint(4F, -18.5F, -4F);

		gunModel[22].addBox(0F, 0F, 0F, 10, 3, 8, 0F); // Box 30
		gunModel[22].setRotationPoint(7F, -10.5F, -4F);

		gunModel[23].addShapeBox(0F, 0F, 0F, 2, 8, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, -4F, 0F, 0F); // Box 31
		gunModel[23].setRotationPoint(7F, -18.5F, -4F);

		gunModel[24].addShapeBox(0F, 0F, 0F, 2, 8, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, -4F, 0F, 0F); // Box 32
		gunModel[24].setRotationPoint(10F, -18.5F, -4F);

		gunModel[25].addShapeBox(0F, 0F, 0F, 12, 13, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 4F, 0F, 0F); // Box 33
		gunModel[25].setRotationPoint(-9F, -3F, -4F);

		gunModel[26].addShapeBox(0F, 0F, 0F, 1, 13, 8, 0F, -4F, 0F, -1F, 4F, 0F, 0F, 4F, 0F, 0F, -4F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 34
		gunModel[26].setRotationPoint(-14F, -3F, -4F);

		gunModel[27].addShapeBox(0F, 0F, 0F, 1, 4, 8, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, -1F, 0F, -1F, 1F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, -1F); // Box 35
		gunModel[27].setRotationPoint(-11F, -7F, -4F);

		gunModel[28].addShapeBox(0F, 0F, 0F, 1, 4, 8, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 36
		gunModel[28].setRotationPoint(-14F, 10F, -4F);

		gunModel[29].addBox(0F, 0F, 0F, 13, 4, 8, 0F); // Box 37
		gunModel[29].setRotationPoint(-13F, 10F, -4F);

		gunModel[30].addShapeBox(0F, 0F, 0F, 2, 8, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, -4F, 0F, 0F); // Box 38
		gunModel[30].setRotationPoint(4F, -18.5F, 3F);

		gunModel[31].addShapeBox(0F, 0F, 0F, 2, 8, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, -4F, 0F, 0F); // Box 39
		gunModel[31].setRotationPoint(7F, -18.5F, 3F);

		gunModel[32].addShapeBox(0F, 0F, 0F, 2, 8, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, -4F, 0F, 0F); // Box 40
		gunModel[32].setRotationPoint(10F, -18.5F, 3F);

		gunModel[33].addShapeBox(0F, 0F, 0F, 2, 1, 8, 0F, -0.5F, 0F, 0.2F, -0.5F, 0F, 0.2F, -0.5F, 0F, 0.2F, -0.5F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0F, 0.2F); // Box 47
		gunModel[33].setRotationPoint(7.5F, -10F, -4F);

		gunModel[34].addShapeBox(0F, 1F, 0F, 2, 1, 8, 0F, 0F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0F, 0.2F, -0.5F, 0F, 0.2F, -0.5F, 0F, 0.2F, -0.5F, 0F, 0.2F, -0.5F, 0F, 0.2F); // Box 48
		gunModel[34].setRotationPoint(7.5F, -10F, -4F);

		gunModel[35].addShapeBox(0F, 1F, 0F, 2, 1, 8, 0F, 0F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0F, 0.2F, -0.5F, 0F, 0.2F, -0.5F, 0F, 0.2F, -0.5F, 0F, 0.2F, -0.5F, 0F, 0.2F); // Box 49
		gunModel[35].setRotationPoint(14.5F, -10F, -4F);

		gunModel[36].addShapeBox(0F, 0F, 0F, 2, 1, 8, 0F, -0.5F, 0F, 0.2F, -0.5F, 0F, 0.2F, -0.5F, 0F, 0.2F, -0.5F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0F, 0.2F); // Box 50
		gunModel[36].setRotationPoint(14.5F, -10F, -4F);

		gunModel[37].addShapeBox(0F, 0F, 0F, 5, 3, 1, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 55
		gunModel[37].setRotationPoint(-16F, -14F, -0.5F);

		gunModel[38].addShapeBox(0F, 0F, 0F, 10, 18, 1, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, -5F, 0F, 0F, -4F, 0F, 0F, 5F, 0F, 0F); // Box 70
		gunModel[38].setRotationPoint(-7.5F, -5F, -4.5F);

		gunModel[39].addBox(0F, 0F, 0F, 8, 3, 1, 0F); // Box 73
		gunModel[39].setRotationPoint(-11F, -10.5F, 3.5F);

		gunModel[40].addShapeBox(0F, 0F, 0F, 4, 3, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F); // Box 74
		gunModel[40].setRotationPoint(-3F, -10.5F, 4F);

		gunModel[41].addShapeBox(0F, 0F, 0F, 10, 2, 1, 0F, -3F, -0.5F, 0F, -3F, -0.5F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 78
		gunModel[41].setRotationPoint(-7.5F, -7F, -4.5F);

		gunModel[42].addShapeBox(0F, 0F, 0F, 10, 18, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 5F, 0F, 0F, -4F, 0F, 0F, -5F, 0F, 0F, 4F, 0F, 0F); // Box 79
		gunModel[42].setRotationPoint(-7.5F, -5F, 3.5F);

		gunModel[43].addShapeBox(0F, 0F, 0F, 10, 2, 1, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -3F, -0.5F, 0F, -3F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 80
		gunModel[43].setRotationPoint(-7.5F, -7F, 3.5F);


		ammoModel = new ModelRendererTurbo[5];
		ammoModel[0] = new ModelRendererTurbo(this, 40, 133, textureX, textureY); // bullet
		ammoModel[1] = new ModelRendererTurbo(this, 40, 128, textureX, textureY); // bulletTip
		ammoModel[2] = new ModelRendererTurbo(this, 1, 105, textureX, textureY); // clip
		ammoModel[3] = new ModelRendererTurbo(this, 1, 128, textureX, textureY); // clip2
		ammoModel[4] = new ModelRendererTurbo(this, 30, 106, textureX, textureY); // clip3

		ammoModel[0].addShapeBox(0F, 0F, 0F, 7, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // bullet
		ammoModel[0].setRotationPoint(-8.5F, -3F, -1.5F);

		ammoModel[1].addShapeBox(0F, 0F, 0F, 2, 1, 3, 0F, 0F, 0F, -1F, 0F, -0.5F, -1F, 0F, -0.5F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F); // bulletTip
		ammoModel[1].setRotationPoint(-1.5F, -3F, -1.5F);

		ammoModel[2].addShapeBox(0F, 0F, 0F, 8, 16, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, 2.5F, 0F, 0F); // clip
		ammoModel[2].setRotationPoint(-9.5F, -2F, -3F);

		ammoModel[3].addBox(0F, 0F, 0F, 12, 2, 7, 0F); // clip2
		ammoModel[3].setRotationPoint(-12.5F, 14F, -3.5F);

		ammoModel[4].addShapeBox(0F, 0F, 0F, 3, 16, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, 2.5F, 0F, 0F); // clip3
		ammoModel[4].setRotationPoint(-1.5F, -2F, -2.5F);


		slideModel = new ModelRendererTurbo[30];
		slideModel[0] = new ModelRendererTurbo(this, 29, 26, textureX, textureY); // ironSight1
		slideModel[1] = new ModelRendererTurbo(this, 88, 32, textureX, textureY); // slide1
		slideModel[2] = new ModelRendererTurbo(this, 111, 48, textureX, textureY); // slide2
		slideModel[3] = new ModelRendererTurbo(this, 107, 89, textureX, textureY); // slide4
		slideModel[4] = new ModelRendererTurbo(this, 88, 86, textureX, textureY); // slide4-2
		slideModel[5] = new ModelRendererTurbo(this, 88, 76, textureX, textureY); // slide5
		slideModel[6] = new ModelRendererTurbo(this, 154, 66, textureX, textureY); // slide6
		slideModel[7] = new ModelRendererTurbo(this, 88, 105, textureX, textureY); // slide7-2
		slideModel[8] = new ModelRendererTurbo(this, 178, 36, textureX, textureY); // Box 1
		slideModel[9] = new ModelRendererTurbo(this, 165, 36, textureX, textureY); // Box 2
		slideModel[10] = new ModelRendererTurbo(this, 171, 66, textureX, textureY); // Box 7
		slideModel[11] = new ModelRendererTurbo(this, 131, 66, textureX, textureY); // Box 8
		slideModel[12] = new ModelRendererTurbo(this, 171, 66, textureX, textureY); // Box 9
		slideModel[13] = new ModelRendererTurbo(this, 104, 67, textureX, textureY); // Box 10
		slideModel[14] = new ModelRendererTurbo(this, 88, 48, textureX, textureY); // Box 11
		slideModel[15] = new ModelRendererTurbo(this, 155, 52, textureX, textureY); // Box 12
		slideModel[16] = new ModelRendererTurbo(this, 87, 64, textureX, textureY); // Box 13
		slideModel[17] = new ModelRendererTurbo(this, 87, 64, textureX, textureY); // Box 14
		slideModel[18] = new ModelRendererTurbo(this, 128, 53, textureX, textureY); // Box 15
		slideModel[19] = new ModelRendererTurbo(this, 29, 22, textureX, textureY); // Box 22
		slideModel[20] = new ModelRendererTurbo(this, 88, 98, textureX, textureY); // Box 23
		slideModel[21] = new ModelRendererTurbo(this, 121, 107, textureX, textureY); // Box 25
		slideModel[22] = new ModelRendererTurbo(this, 88, 105, textureX, textureY); // Box 44
		slideModel[23] = new ModelRendererTurbo(this, 121, 107, textureX, textureY); // Box 45
		slideModel[24] = new ModelRendererTurbo(this, 88, 98, textureX, textureY); // Box 46
		slideModel[25] = new ModelRendererTurbo(this, 87, 64, textureX, textureY); // Box 53
		slideModel[26] = new ModelRendererTurbo(this, 171, 66, textureX, textureY); // Box 54
		slideModel[27] = new ModelRendererTurbo(this, 104, 67, textureX, textureY); // Box 77
		slideModel[28] = new ModelRendererTurbo(this, 104, 67, textureX, textureY); // Box 78
		slideModel[29] = new ModelRendererTurbo(this, 104, 67, textureX, textureY); // Box 79

		slideModel[0].addShapeBox(0F, 0F, 0F, 4, 2, 1, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // ironSight1
		slideModel[0].setRotationPoint(36F, -25F, -0.5F);

		slideModel[1].addBox(0F, 0F, 0F, 31, 8, 7, 0F); // slide1
		slideModel[1].setRotationPoint(-11F, -19F, -3.5F);

		slideModel[2].addBox(0F, 0F, 0F, 1, 8, 7, 0F); // slide2
		slideModel[2].setRotationPoint(28F, -19F, -3.5F);

		slideModel[3].addBox(0F, 0F, 0F, 8, 7, 1, 0F); // slide4
		slideModel[3].setRotationPoint(20F, -18F, -3.5F);

		slideModel[4].addShapeBox(0F, 0F, 0F, 8, 10, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // slide4-2
		slideModel[4].setRotationPoint(20F, -21F, 2.5F);

		slideModel[5].addShapeBox(0F, 0F, 0F, 33, 2, 7, 0F, -0.5F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, -0.5F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // slide5
		slideModel[5].setRotationPoint(-13F, -21F, -3.5F);

		slideModel[6].addShapeBox(0F, 0F, 0F, 1, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // slide6
		slideModel[6].setRotationPoint(28F, -21F, -3.5F);

		slideModel[7].addShapeBox(0F, 0F, 0F, 15, 7, 1, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.75F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0.75F, 0F, 0F); // slide7-2
		slideModel[7].setRotationPoint(-13F, -18F, -3.8F);

		slideModel[8].addShapeBox(0F, 0F, 0F, 3, 8, 3, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 1
		slideModel[8].setRotationPoint(-14F, -19F, -3.5F);

		slideModel[9].addShapeBox(0F, 0F, 0F, 3, 8, 3, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 2
		slideModel[9].setRotationPoint(-14F, -19F, 0.5F);

		slideModel[10].addShapeBox(0F, 0F, 0F, 1, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 7
		slideModel[10].setRotationPoint(30F, -21F, -3.5F);

		slideModel[11].addShapeBox(0F, 0F, 0F, 4, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 8
		slideModel[11].setRotationPoint(36F, -21F, -3.5F);

		slideModel[12].addShapeBox(0F, 0F, 0F, 1, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 9
		slideModel[12].setRotationPoint(32F, -21F, -3.5F);

		slideModel[13].addShapeBox(0F, 0F, 0F, 1, 2, 6, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 10
		slideModel[13].setRotationPoint(29F, -21F, -3F);

		slideModel[14].addBox(0F, 0F, 0F, 4, 8, 7, 0F); // Box 11
		slideModel[14].setRotationPoint(36F, -19F, -3.5F);

		slideModel[15].addBox(0F, 0F, 0F, 7, 4, 7, 0F); // Box 12
		slideModel[15].setRotationPoint(29F, -15F, -3.5F);

		slideModel[16].addBox(0F, 0F, 0F, 1, 4, 7, 0F); // Box 13
		slideModel[16].setRotationPoint(30F, -19F, -3.5F);

		slideModel[17].addBox(0F, 0F, 0F, 1, 4, 7, 0F); // Box 14
		slideModel[17].setRotationPoint(32F, -19F, -3.5F);

		slideModel[18].addBox(0F, 0F, 0F, 7, 4, 6, 0F); // Box 15
		slideModel[18].setRotationPoint(29F, -19F, -3F);

		slideModel[19].addBox(0F, 0F, 0F, 4, 2, 1, 0F); // Box 22
		slideModel[19].setRotationPoint(36F, -23F, -0.5F);

		slideModel[20].addShapeBox(0F, 0F, 0F, 24, 5, 1, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 23
		slideModel[20].setRotationPoint(4F, -18F, -3.8F);

		slideModel[21].addShapeBox(0F, 0F, 0F, 2, 5, 1, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 2F, 0F); // Box 25
		slideModel[21].setRotationPoint(2F, -18F, -3.8F);

		slideModel[22].addShapeBox(0F, 0F, 0F, 15, 7, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0.75F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0.75F, -1F, 0F); // Box 44
		slideModel[22].setRotationPoint(-13F, -18F, 2.8F);

		slideModel[23].addShapeBox(0F, 0F, 0F, 2, 5, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 1F, 0F); // Box 45
		slideModel[23].setRotationPoint(2F, -18F, 2.8F);

		slideModel[24].addShapeBox(0F, 0F, 0F, 24, 5, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F); // Box 46
		slideModel[24].setRotationPoint(4F, -18F, 2.8F);

		slideModel[25].addBox(0F, 0F, 0F, 1, 4, 7, 0F); // Box 53
		slideModel[25].setRotationPoint(34F, -19F, -3.5F);

		slideModel[26].addShapeBox(0F, 0F, 0F, 1, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 54
		slideModel[26].setRotationPoint(34F, -21F, -3.5F);

		slideModel[27].addShapeBox(0F, 0F, 0F, 1, 2, 6, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 77
		slideModel[27].setRotationPoint(35F, -21F, -3F);

		slideModel[28].addShapeBox(0F, 0F, 0F, 1, 2, 6, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 78
		slideModel[28].setRotationPoint(33F, -21F, -3F);

		slideModel[29].addShapeBox(0F, 0F, 0F, 1, 2, 6, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 79
		slideModel[29].setRotationPoint(31F, -21F, -3F);

		barrelAttachPoint = new Vector3f(42F /16F, 18F /16F, 0F /16F);
		scopeAttachPoint = new Vector3f(1F /16F, 22F /16F, 0F /16F);
		gripAttachPoint = new Vector3f(32 /16F, 10F /16F, 0F /16F);

		gunSlideDistance = 0.8F;
		animationType = EnumAnimationType.CUSTOM;

		/* ----Start of Reload Block---- */
		/* Reload Name: Alternate Pistol Clip */
		rotateGunVertical = 10F;
		translateGun = new Vector3f(0.15F, 0.25F, 0F);

		rotateClipVertical = 5F;
		translateClip = new Vector3f(-0.5F, -3F, 0F);
		/* ----End of Reload Block---- */

		translateAll(0F, 0F, 0F);


		flipAll();
	}
}