package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelMakoteptnMP extends ModelGun
{
	int textureX = 512;
	int textureY = 256;

	public ModelMakoteptnMP()
	{
		gunModel = new ModelRendererTurbo[17];
		gunModel[0] = new ModelRendererTurbo(this, 1, 18, textureX, textureY); // body1
		gunModel[1] = new ModelRendererTurbo(this, 1, 32, textureX, textureY); // body3
		gunModel[2] = new ModelRendererTurbo(this, 30, 32, textureX, textureY); // body2
		gunModel[3] = new ModelRendererTurbo(this, 1, 42, textureX, textureY); // body4
		gunModel[4] = new ModelRendererTurbo(this, 1, 49, textureX, textureY); // grip1
		gunModel[5] = new ModelRendererTurbo(this, 80, 20, textureX, textureY); // mainBarrelBottom
		gunModel[6] = new ModelRendererTurbo(this, 80, 10, textureX, textureY); // mainBarrelMiddle
		gunModel[7] = new ModelRendererTurbo(this, 80, 1, textureX, textureY); // mainBarrelTop
		gunModel[8] = new ModelRendererTurbo(this, 80, 29, textureX, textureY); // pin1
		gunModel[9] = new ModelRendererTurbo(this, 80, 29, textureX, textureY); // pin1-2
		gunModel[10] = new ModelRendererTurbo(this, 80, 29, textureX, textureY); // pin1-3
		gunModel[11] = new ModelRendererTurbo(this, 1, 63, textureX, textureY); // grip2
		gunModel[12] = new ModelRendererTurbo(this, 153, 1, textureX, textureY); // Box 15
		gunModel[13] = new ModelRendererTurbo(this, 153, 11, textureX, textureY); // Box 16
		gunModel[14] = new ModelRendererTurbo(this, 153, 22, textureX, textureY); // Box 17
		gunModel[15] = new ModelRendererTurbo(this, 80, 117, textureX, textureY); // Box 18
		gunModel[16] = new ModelRendererTurbo(this, 80, 104, textureX, textureY); // Box 19

		gunModel[0].addBox(0F, 0F, 0F, 27, 4, 9, 0F); // body1
		gunModel[0].setRotationPoint(-7F, -14F, -4.5F);

		gunModel[1].addShapeBox(0F, 0F, 0F, 8, 3, 6, 0F, 15F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 15F, 0F, 0F, 15F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 15F, 0F, 0F); // body3
		gunModel[1].setRotationPoint(35F, -14F, -3F);

		gunModel[2].addShapeBox(0F, 0F, 0F, 5, 2, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F); // body2
		gunModel[2].setRotationPoint(-12F, -14F, -4.5F);

		gunModel[3].addShapeBox(0F, 0F, 0F, 8, 1, 5, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body4
		gunModel[3].setRotationPoint(35F, -11F, -2.5F);

		gunModel[4].addShapeBox(0F, 0F, 0F, 14, 4, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F); // grip1
		gunModel[4].setRotationPoint(-5F, -10F, -4.5F);

		gunModel[5].addShapeBox(0F, 0F, 0F, 30, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // mainBarrelBottom
		gunModel[5].setRotationPoint(27F, -19F, -3F);

		gunModel[6].addBox(0F, 0F, 0F, 30, 2, 6, 0F); // mainBarrelMiddle
		gunModel[6].setRotationPoint(27F, -21F, -3F);

		gunModel[7].addShapeBox(0F, 0F, 0F, 30, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // mainBarrelTop
		gunModel[7].setRotationPoint(27F, -23F, -3F);

		gunModel[8].addShapeBox(0F, 0F, 0F, 10, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // pin1
		gunModel[8].setRotationPoint(33.5F, -15F, -1.5F);

		gunModel[9].addBox(0F, 0F, 0F, 10, 1, 3, 0F); // pin1-2
		gunModel[9].setRotationPoint(33.5F, -14F, -1.5F);

		gunModel[10].addShapeBox(0F, 0F, 0F, 10, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // pin1-3
		gunModel[10].setRotationPoint(33.5F, -13F, -1.5F);

		gunModel[11].addBox(0F, 0F, 0F, 12, 12, 8, 0F); // grip2
		gunModel[11].setRotationPoint(-3.5F, -6F, -4F);

		gunModel[12].addShapeBox(0F, 0F, 0F, 5, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 15
		gunModel[12].setRotationPoint(45F, -23.5F, -3.5F);

		gunModel[13].addBox(0F, 0F, 0F, 5, 3, 7, 0F); // Box 16
		gunModel[13].setRotationPoint(45F, -21.5F, -3.5F);

		gunModel[14].addShapeBox(0F, 0F, 0F, 5, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 17
		gunModel[14].setRotationPoint(45F, -18.5F, -3.5F);

		gunModel[15].addShapeBox(0F, 0F, 0F, 10, 1, 6, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 18
		gunModel[15].setRotationPoint(15F, -23.5F, -3F);

		gunModel[16].addBox(0F, 0F, 0F, 10, 6, 6, 0F); // Box 19
		gunModel[16].setRotationPoint(15F, -22.5F, -3F);


		defaultGripModel = new ModelRendererTurbo[11];
		defaultGripModel[0] = new ModelRendererTurbo(this, 1, 84, textureX, textureY); // foreGrip1-2
		defaultGripModel[1] = new ModelRendererTurbo(this, 40, 83, textureX, textureY); // foreGrip1-2
		defaultGripModel[2] = new ModelRendererTurbo(this, 1, 113, textureX, textureY); // foreGrip1-2
		defaultGripModel[3] = new ModelRendererTurbo(this, 1, 131, textureX, textureY); // foreGrip1-2
		defaultGripModel[4] = new ModelRendererTurbo(this, 1, 122, textureX, textureY); // foreGrip1-2
		defaultGripModel[5] = new ModelRendererTurbo(this, 1, 96, textureX, textureY); // foreGrip1-2
		defaultGripModel[6] = new ModelRendererTurbo(this, 1, 145, textureX, textureY); // foreGrip1-2
		defaultGripModel[7] = new ModelRendererTurbo(this, 30, 113, textureX, textureY); // foreGrip1-2
		defaultGripModel[8] = new ModelRendererTurbo(this, 30, 122, textureX, textureY); // foreGrip1-2
		defaultGripModel[9] = new ModelRendererTurbo(this, 28, 131, textureX, textureY); // foreGrip1-2
		defaultGripModel[10] = new ModelRendererTurbo(this, 30, 96, textureX, textureY); // foreGrip1-2

		defaultGripModel[0].addBox(0F, 0F, 0F, 15, 3, 8, 0F); // foreGrip1-2
		defaultGripModel[0].setRotationPoint(20F, -14F, -4F);

		defaultGripModel[1].addShapeBox(0F, 0F, 0F, 15, 2, 6, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // foreGrip1-2
		defaultGripModel[1].setRotationPoint(20F, -11F, -3F);

		defaultGripModel[2].addShapeBox(0F, 0F, 0F, 8, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F); // foreGrip1-2
		defaultGripModel[2].setRotationPoint(24F, -9F, -3F);

		defaultGripModel[3].addBox(0F, 0F, 0F, 7, 7, 6, 0F); // foreGrip1-2
		defaultGripModel[3].setRotationPoint(24F, -7F, -3F);

		defaultGripModel[4].addShapeBox(0F, 0F, 0F, 8, 2, 6, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // foreGrip1-2
		defaultGripModel[4].setRotationPoint(24F, 0F, -3F);

		defaultGripModel[5].addBox(0F, 0F, 0F, 8, 10, 6, 0F); // foreGrip1-2
		defaultGripModel[5].setRotationPoint(24F, 2F, -3F);

		defaultGripModel[6].addShapeBox(0F, 0F, 0F, 2, 21, 6, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F); // foreGrip1-2
		defaultGripModel[6].setRotationPoint(22F, -9F, -3F);

		defaultGripModel[7].addShapeBox(0F, 0F, 0F, 1, 2, 6, 0F, -1F, 0F, 0F, 1F, 0F, -1F, 1F, 0F, -1F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // foreGrip1-2
		defaultGripModel[7].setRotationPoint(31F, -9F, -3F);

		defaultGripModel[8].addShapeBox(0F, 0F, 0F, 1, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, -1F, 1F, 0F, -1F, -1F, 0F, 0F); // foreGrip1-2
		defaultGripModel[8].setRotationPoint(31F, 0F, -3F);

		defaultGripModel[9].addShapeBox(0F, 0F, 0F, 1, 7, 6, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // foreGrip1-2
		defaultGripModel[9].setRotationPoint(31F, -7F, -3F);

		defaultGripModel[10].addShapeBox(0F, 0F, 0F, 1, 10, 6, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // foreGrip1-2
		defaultGripModel[10].setRotationPoint(32F, 2F, -3F);


		ammoModel = new ModelRendererTurbo[8];
		ammoModel[0] = new ModelRendererTurbo(this, 60, 174, textureX, textureY); // bullet
		ammoModel[1] = new ModelRendererTurbo(this, 67, 179, textureX, textureY); // bulletTip
		ammoModel[2] = new ModelRendererTurbo(this, 1, 173, textureX, textureY); // clip1
		ammoModel[3] = new ModelRendererTurbo(this, 41, 143, textureX, textureY); // clip2
		ammoModel[4] = new ModelRendererTurbo(this, 30, 174, textureX, textureY); // clip3
		ammoModel[5] = new ModelRendererTurbo(this, 71, 146, textureX, textureY); // Box 20
		ammoModel[6] = new ModelRendererTurbo(this, 56, 147, textureX, textureY); // Box 21
		ammoModel[7] = new ModelRendererTurbo(this, 18, 146, textureX, textureY); // Box 22

		ammoModel[0].addShapeBox(0F, 0F, 0F, 6, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // bullet
		ammoModel[0].setRotationPoint(-1.5F, -2F, -1.5F);

		ammoModel[1].addShapeBox(0F, 0F, 0F, 2, 1, 3, 0F, 0F, 0F, -1F, 0F, -0.5F, -1F, 0F, -0.5F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F); // bulletTip
		ammoModel[1].setRotationPoint(4.5F, -2F, -1.5F);

		ammoModel[2].addBox(0F, 0F, 0F, 8, 4, 6, 0F); // clip1
		ammoModel[2].setRotationPoint(-2.5F, 19F, -3F);

		ammoModel[3].addBox(0F, 0F, 0F, 2, 24, 5, 0F); // clip2
		ammoModel[3].setRotationPoint(5.5F, -1F, -2.5F);

		ammoModel[4].addBox(0F, 0F, 0F, 11, 2, 7, 0F); // clip3
		ammoModel[4].setRotationPoint(-3F, 23F, -3.5F);

		ammoModel[5].addBox(0F, 0F, 0F, 1, 20, 6, 0F); // Box 20
		ammoModel[5].setRotationPoint(-2.5F, -1F, -3F);

		ammoModel[6].addBox(0F, 0F, 0F, 2, 20, 5, 0F); // Box 21
		ammoModel[6].setRotationPoint(-1.5F, -1F, -2.5F);

		ammoModel[7].addBox(0F, 0F, 0F, 5, 20, 6, 0F); // Box 22
		ammoModel[7].setRotationPoint(0.5F, -1F, -3F);


		slideModel = new ModelRendererTurbo[19];
		slideModel[0] = new ModelRendererTurbo(this, 1, 10, textureX, textureY); // ironSight1
		slideModel[1] = new ModelRendererTurbo(this, 14, 10, textureX, textureY); // ironSight1
		slideModel[2] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // ironSight1
		slideModel[3] = new ModelRendererTurbo(this, 27, 11, textureX, textureY); // ironSight1
		slideModel[4] = new ModelRendererTurbo(this, 80, 34, textureX, textureY); // ironSight1
		slideModel[5] = new ModelRendererTurbo(this, 80, 50, textureX, textureY); // ironSight1
		slideModel[6] = new ModelRendererTurbo(this, 152, 76, textureX, textureY); // ironSight1
		slideModel[7] = new ModelRendererTurbo(this, 152, 55, textureX, textureY); // ironSight1
		slideModel[8] = new ModelRendererTurbo(this, 80, 76, textureX, textureY); // ironSight1
		slideModel[9] = new ModelRendererTurbo(this, 80, 95, textureX, textureY); // ironSight1
		slideModel[10] = new ModelRendererTurbo(this, 133, 50, textureX, textureY); // ironSight1
		slideModel[11] = new ModelRendererTurbo(this, 133, 50, textureX, textureY); // ironSight1
		slideModel[12] = new ModelRendererTurbo(this, 133, 50, textureX, textureY); // ironSight1
		slideModel[13] = new ModelRendererTurbo(this, 107, 34, textureX, textureY); // ironSight1
		slideModel[14] = new ModelRendererTurbo(this, 150, 35, textureX, textureY); // ironSight1
		slideModel[15] = new ModelRendererTurbo(this, 152, 67, textureX, textureY); // ironSight1
		slideModel[16] = new ModelRendererTurbo(this, 133, 50, textureX, textureY); // ironSight1
		slideModel[17] = new ModelRendererTurbo(this, 80, 66, textureX, textureY); // ironSight1
		slideModel[18] = new ModelRendererTurbo(this, 80, 85, textureX, textureY); // ironSight1

		slideModel[0].addBox(0F, 0F, 0F, 4, 2, 2, 0F); // ironSight1
		slideModel[0].setRotationPoint(-11F, -27F, -3F);

		slideModel[1].addBox(0F, 0F, 0F, 4, 2, 2, 0F); // ironSight1
		slideModel[1].setRotationPoint(-11F, -27F, 1F);

		slideModel[2].addBox(0F, 0F, 0F, 6, 2, 6, 0F); // ironSight1
		slideModel[2].setRotationPoint(-11F, -25F, -3F);

		slideModel[3].addShapeBox(0F, 0F, 0F, 4, 2, 1, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // ironSight1
		slideModel[3].setRotationPoint(39F, -26F, -0.5F);

		slideModel[4].addBox(0F, 0F, 0F, 5, 7, 8, 0F); // ironSight1
		slideModel[4].setRotationPoint(-12F, -21F, -4F);

		slideModel[5].addBox(0F, 0F, 0F, 18, 7, 8, 0F); // ironSight1
		slideModel[5].setRotationPoint(25F, -21F, -4F);

		slideModel[6].addBox(0F, 0F, 0F, 10, 7, 1, 0F); // ironSight1
		slideModel[6].setRotationPoint(15F, -21F, -4F);

		slideModel[7].addShapeBox(0F, 0F, 0F, 10, 10, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // ironSight1
		slideModel[7].setRotationPoint(15F, -24F, 2.5F);

		slideModel[8].addShapeBox(0F, 0F, 0F, 27, 1, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // ironSight1
		slideModel[8].setRotationPoint(-12F, -24F, -3.5F);

		slideModel[9].addShapeBox(0F, 0F, 0F, 18, 1, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // ironSight1
		slideModel[9].setRotationPoint(25F, -24F, -3.5F);

		slideModel[10].addBox(0F, 0F, 0F, 1, 7, 8, 0F); // ironSight1
		slideModel[10].setRotationPoint(-6F, -21F, -4F);

		slideModel[11].addBox(0F, 0F, 0F, 1, 7, 8, 0F); // ironSight1
		slideModel[11].setRotationPoint(-4F, -21F, -4F);

		slideModel[12].addBox(0F, 0F, 0F, 1, 7, 8, 0F); // ironSight1
		slideModel[12].setRotationPoint(-2F, -21F, -4F);

		slideModel[13].addBox(0F, 0F, 0F, 13, 7, 8, 0F); // ironSight1
		slideModel[13].setRotationPoint(2F, -21F, -4F);

		slideModel[14].addBox(0F, 0F, 0F, 9, 7, 7, 0F); // ironSight1
		slideModel[14].setRotationPoint(-7F, -21F, -3.5F);

		slideModel[15].addBox(0F, 0F, 0F, 10, 7, 1, 0F); // ironSight1
		slideModel[15].setRotationPoint(15F, -21F, 3F);

		slideModel[16].addBox(0F, 0F, 0F, 1, 7, 8, 0F); // ironSight1
		slideModel[16].setRotationPoint(0F, -21F, -4F);

		slideModel[17].addBox(0F, 0F, 0F, 27, 2, 7, 0F); // ironSight1
		slideModel[17].setRotationPoint(-12F, -23F, -3.5F);

		slideModel[18].addBox(0F, 0F, 0F, 18, 2, 7, 0F); // ironSight1
		slideModel[18].setRotationPoint(25F, -23F, -3.5F);

		barrelAttachPoint = new Vector3f(57F /16F, 19.5F /16F, 0F /16F);
		gripAttachPoint = new Vector3f(27 /16F, 13F /16F, 0F /16F);

		gunSlideDistance = 0.5F;
		animationType = EnumAnimationType.PISTOL_CLIP;


		translateAll(0F, 0F, 0F);


		flipAll();
	}
}