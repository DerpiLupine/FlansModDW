package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelHVFPeforator extends ModelGun
{
	int textureX = 512;
	int textureY = 256;

	public ModelHVFPeforator()
	{
		gunModel = new ModelRendererTurbo[34];
		gunModel[0] = new ModelRendererTurbo(this, 72, 32, textureX, textureY); // Import body1
		gunModel[1] = new ModelRendererTurbo(this, 72, 53, textureX, textureY); // Import body10
		gunModel[2] = new ModelRendererTurbo(this, 99, 145, textureX, textureY); // Import body11
		gunModel[3] = new ModelRendererTurbo(this, 99, 151, textureX, textureY); // Import body12
		gunModel[4] = new ModelRendererTurbo(this, 72, 89, textureX, textureY); // Import body2
		gunModel[5] = new ModelRendererTurbo(this, 72, 104, textureX, textureY); // Import body3
		gunModel[6] = new ModelRendererTurbo(this, 129, 99, textureX, textureY); // Import body4
		gunModel[7] = new ModelRendererTurbo(this, 72, 129, textureX, textureY); // Import body5
		gunModel[8] = new ModelRendererTurbo(this, 72, 142, textureX, textureY); // Import body6
		gunModel[9] = new ModelRendererTurbo(this, 72, 75, textureX, textureY); // Import body8
		gunModel[10] = new ModelRendererTurbo(this, 72, 64, textureX, textureY); // Import body9
		gunModel[11] = new ModelRendererTurbo(this, 49, 1, textureX, textureY); // Import ironSight3
		gunModel[12] = new ModelRendererTurbo(this, 190, 11, textureX, textureY); // Import Box17
		gunModel[13] = new ModelRendererTurbo(this, 190, 21, textureX, textureY); // Import Box18
		gunModel[14] = new ModelRendererTurbo(this, 190, 11, textureX, textureY); // Import Box19
		gunModel[15] = new ModelRendererTurbo(this, 1, 189, textureX, textureY); // Import clipPart
		gunModel[16] = new ModelRendererTurbo(this, 169, 2, textureX, textureY); // Import gasBlock1
		gunModel[17] = new ModelRendererTurbo(this, 169, 2, textureX, textureY); // Import gasBlock1-2
		gunModel[18] = new ModelRendererTurbo(this, 169, 2, textureX, textureY); // Import gasBlock1-3
		gunModel[19] = new ModelRendererTurbo(this, 169, 32, textureX, textureY); // Import gasBlockConnector
		gunModel[20] = new ModelRendererTurbo(this, 169, 11, textureX, textureY); // Import gasBlockConnector2
		gunModel[21] = new ModelRendererTurbo(this, 169, 21, textureX, textureY); // Import gasBlockConnector2-2
		gunModel[22] = new ModelRendererTurbo(this, 169, 11, textureX, textureY); // Import gasBlockConnector2-3
		gunModel[23] = new ModelRendererTurbo(this, 1, 117, textureX, textureY); // Import grip1
		gunModel[24] = new ModelRendererTurbo(this, 1, 94, textureX, textureY); // Import grip2
		gunModel[25] = new ModelRendererTurbo(this, 1, 131, textureX, textureY); // Import grip4
		gunModel[26] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Import ironSight1
		gunModel[27] = new ModelRendererTurbo(this, 28, 1, textureX, textureY); // Import ironSight2
		gunModel[28] = new ModelRendererTurbo(this, 72, 20, textureX, textureY); // Import mainBarrelBottom
		gunModel[29] = new ModelRendererTurbo(this, 72, 11, textureX, textureY); // Import mainBarrelMiddle
		gunModel[30] = new ModelRendererTurbo(this, 72, 2, textureX, textureY); // Import mainBarrelTop
		gunModel[31] = new ModelRendererTurbo(this, 213, 11, textureX, textureY); // Import ring1
		gunModel[32] = new ModelRendererTurbo(this, 213, 11, textureX, textureY); // Import ring1-2
		gunModel[33] = new ModelRendererTurbo(this, 213, 21, textureX, textureY); // Import ring2

		gunModel[0].addBox(0F, 0F, 0F, 25, 10, 10, 0F); // Import body1
		gunModel[0].setRotationPoint(-10F, -20F, -5F);

		gunModel[1].addShapeBox(0F, 0F, 0F, 46, 1, 8, 0F, -1F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, -1F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import body10
		gunModel[1].setRotationPoint(-8F, -26F, -4F);

		gunModel[2].addShapeBox(0F, 0F, 0F, 3, 2, 3, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import body11
		gunModel[2].setRotationPoint(-9.5F, -25F, -1.5F);

		gunModel[3].addBox(0F, 0F, 0F, 12, 3, 1, 0F); // Import body12
		gunModel[3].setRotationPoint(5F, -19F, 4.5F);

		gunModel[4].addBox(0F, 0F, 0F, 18, 5, 9, 0F); // Import body2
		gunModel[4].setRotationPoint(15F, -15F, -4.5F);

		gunModel[5].addBox(0F, 0F, 0F, 18, 5, 10, 0F); // Import body3
		gunModel[5].setRotationPoint(15F, -20F, -5F);

		gunModel[6].addBox(0F, 0F, 0F, 5, 10, 10, 0F); // Import body4
		gunModel[6].setRotationPoint(33F, -20F, -5F);

		gunModel[7].addShapeBox(0F, 0F, 0F, 20, 3, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import body5
		gunModel[7].setRotationPoint(15F, -23F, -4F);

		gunModel[8].addShapeBox(0F, 0F, 0F, 3, 3, 10, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import body6
		gunModel[8].setRotationPoint(35F, -23F, -5F);

		gunModel[9].addShapeBox(0F, 0F, 0F, 25, 3, 10, 0F, -1F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, -1F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import body8
		gunModel[9].setRotationPoint(-10F, -23F, -5F);

		gunModel[10].addShapeBox(0F, 0F, 0F, 47, 2, 8, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import body9
		gunModel[10].setRotationPoint(-9F, -25F, -4F);

		gunModel[11].addBox(0F, 0F, 0F, 3, 6, 3, 0F); // Import ironSight3
		gunModel[11].setRotationPoint(106F, -25F, -1.5F);

		gunModel[12].addShapeBox(0F, 0F, 0F, 4, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import Box17
		gunModel[12].setRotationPoint(69F, -19F, -3.5F);

		gunModel[13].addBox(0F, 0F, 0F, 4, 3, 7, 0F); // Import Box18
		gunModel[13].setRotationPoint(69F, -17F, -3.5F);

		gunModel[14].addShapeBox(0F, 0F, 0F, 4, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Import Box19
		gunModel[14].setRotationPoint(69F, -14F, -3.5F);

		gunModel[15].addBox(0F, 0F, 0F, 3, 5, 9, 0F); // Import clipPart
		gunModel[15].setRotationPoint(12F, -10F, -4.5F);

		gunModel[16].addShapeBox(0F, 0F, 0F, 12, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import gasBlock1
		gunModel[16].setRotationPoint(69F, -26F, -3F);

		gunModel[17].addBox(0F, 0F, 0F, 12, 2, 6, 0F); // Import gasBlock1-2
		gunModel[17].setRotationPoint(69F, -24F, -3F);

		gunModel[18].addShapeBox(0F, 0F, 0F, 12, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Import gasBlock1-3
		gunModel[18].setRotationPoint(69F, -22F, -3F);

		gunModel[19].addBox(0F, 0F, 0F, 3, 1, 3, 0F); // Import gasBlockConnector
		gunModel[19].setRotationPoint(78F, -20F, -1.5F);

		gunModel[20].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import gasBlockConnector2
		gunModel[20].setRotationPoint(78F, -19F, -3.5F);

		gunModel[21].addBox(0F, 0F, 0F, 3, 3, 7, 0F); // Import gasBlockConnector2-2
		gunModel[21].setRotationPoint(78F, -17F, -3.5F);

		gunModel[22].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Import gasBlockConnector2-3
		gunModel[22].setRotationPoint(78F, -14F, -3.5F);

		gunModel[23].addShapeBox(0F, 0F, 0F, 12, 3, 10, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F); // Import grip1
		gunModel[23].setRotationPoint(-10F, -10F, -5F);

		gunModel[24].addShapeBox(0F, 0F, 0F, 11, 12, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 4F, 0F, 0F); // Import grip2
		gunModel[24].setRotationPoint(-9F, -7F, -5F);

		gunModel[25].addShapeBox(0F, 0F, 0F, 10, 3, 10, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F); // Import grip4
		gunModel[25].setRotationPoint(-13F, 5F, -5F);

		gunModel[26].addBox(0F, 0F, 0F, 8, 1, 5, 0F); // Import ironSight1
		gunModel[26].setRotationPoint(38F, -28F, -2.5F);

		gunModel[27].addBox(0F, 0F, 0F, 5, 2, 5, 0F); // Import ironSight2
		gunModel[27].setRotationPoint(33F, -28F, -2.5F);

		gunModel[28].addShapeBox(0F, 0F, 0F, 42, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Import mainBarrelBottom
		gunModel[28].setRotationPoint(69F, -14.5F, -3F);

		gunModel[29].addBox(0F, 0F, 0F, 42, 2, 6, 0F); // Import mainBarrelMiddle
		gunModel[29].setRotationPoint(69F, -16.5F, -3F);

		gunModel[30].addShapeBox(0F, 0F, 0F, 42, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import mainBarrelTop
		gunModel[30].setRotationPoint(69F, -18.5F, -3F);

		gunModel[31].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import ring1
		gunModel[31].setRotationPoint(106F, -19F, -3.5F);

		gunModel[32].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Import ring1-2
		gunModel[32].setRotationPoint(106F, -14F, -3.5F);

		gunModel[33].addBox(0F, 0F, 0F, 3, 3, 7, 0F); // Import ring2
		gunModel[33].setRotationPoint(106F, -17F, -3.5F);


		defaultScopeModel = new ModelRendererTurbo[32];
		defaultScopeModel[0] = new ModelRendererTurbo(this, 223, 86, textureX, textureY); // Import scopeBase
		defaultScopeModel[1] = new ModelRendererTurbo(this, 184, 40, textureX, textureY); // Import main1
		defaultScopeModel[2] = new ModelRendererTurbo(this, 184, 53, textureX, textureY); // Import main2
		defaultScopeModel[3] = new ModelRendererTurbo(this, 184, 68, textureX, textureY); // Import main3
		defaultScopeModel[4] = new ModelRendererTurbo(this, 184, 53, textureX, textureY); // Import main2-2
		defaultScopeModel[5] = new ModelRendererTurbo(this, 184, 40, textureX, textureY); // Import main1-2
		defaultScopeModel[6] = new ModelRendererTurbo(this, 184, 98, textureX, textureY); // Import brassMain2
		defaultScopeModel[7] = new ModelRendererTurbo(this, 184, 85, textureX, textureY); // Import brassMain1-2
		defaultScopeModel[8] = new ModelRendererTurbo(this, 184, 98, textureX, textureY); // Import brassMain2-2
		defaultScopeModel[9] = new ModelRendererTurbo(this, 184, 113, textureX, textureY); // Import brassMain3
		defaultScopeModel[10] = new ModelRendererTurbo(this, 184, 85, textureX, textureY); // Import brassMain1
		defaultScopeModel[11] = new ModelRendererTurbo(this, 184, 130, textureX, textureY); // Import Box70
		defaultScopeModel[12] = new ModelRendererTurbo(this, 184, 130, textureX, textureY); // Import Box71
		defaultScopeModel[13] = new ModelRendererTurbo(this, 184, 130, textureX, textureY); // Import Box72
		defaultScopeModel[14] = new ModelRendererTurbo(this, 213, 145, textureX, textureY); // Import endPart1
		defaultScopeModel[15] = new ModelRendererTurbo(this, 184, 144, textureX, textureY); // Import endPart2
		defaultScopeModel[16] = new ModelRendererTurbo(this, 213, 145, textureX, textureY); // Import endPart1-2
		defaultScopeModel[17] = new ModelRendererTurbo(this, 227, 99, textureX, textureY); // Import Box76
		defaultScopeModel[18] = new ModelRendererTurbo(this, 227, 99, textureX, textureY); // Import Box77
		defaultScopeModel[19] = new ModelRendererTurbo(this, 227, 99, textureX, textureY); // Import Box78
		defaultScopeModel[20] = new ModelRendererTurbo(this, 227, 113, textureX, textureY); // Import Box79
		defaultScopeModel[21] = new ModelRendererTurbo(this, 227, 113, textureX, textureY); // Import Box80
		defaultScopeModel[22] = new ModelRendererTurbo(this, 227, 113, textureX, textureY); // Import Box81
		defaultScopeModel[23] = new ModelRendererTurbo(this, 184, 55, textureX, textureY); // Import Box82
		defaultScopeModel[24] = new ModelRendererTurbo(this, 184, 55, textureX, textureY); // Import Box83
		defaultScopeModel[25] = new ModelRendererTurbo(this, 184, 55, textureX, textureY); // Import Box84
		defaultScopeModel[26] = new ModelRendererTurbo(this, 227, 99, textureX, textureY); // Import Box86
		defaultScopeModel[27] = new ModelRendererTurbo(this, 227, 99, textureX, textureY); // Import Box87
		defaultScopeModel[28] = new ModelRendererTurbo(this, 227, 99, textureX, textureY); // Import Box88
		defaultScopeModel[29] = new ModelRendererTurbo(this, 227, 99, textureX, textureY); // Import Box89
		defaultScopeModel[30] = new ModelRendererTurbo(this, 227, 99, textureX, textureY); // Import Box90
		defaultScopeModel[31] = new ModelRendererTurbo(this, 227, 99, textureX, textureY); // Import Box91

		defaultScopeModel[0].addBox(0F, 0F, 0F, 12, 4, 7, 0F); // Import scopeBase
		defaultScopeModel[0].setRotationPoint(6F, -29F, -3.5F);

		defaultScopeModel[1].addShapeBox(0F, 0F, 0F, 18, 2, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Import main1
		defaultScopeModel[1].setRotationPoint(3F, -30F, -5F);

		defaultScopeModel[2].addShapeBox(0F, 0F, 0F, 18, 2, 12, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Import main2
		defaultScopeModel[2].setRotationPoint(3F, -32F, -6F);

		defaultScopeModel[3].addBox(0F, 0F, 0F, 18, 4, 12, 0F); // Import main3
		defaultScopeModel[3].setRotationPoint(3F, -36F, -6F);

		defaultScopeModel[4].addShapeBox(0F, 0F, 0F, 18, 2, 12, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import main2-2
		defaultScopeModel[4].setRotationPoint(3F, -38F, -6F);

		defaultScopeModel[5].addShapeBox(0F, 0F, 0F, 18, 2, 10, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import main1-2
		defaultScopeModel[5].setRotationPoint(3F, -40F, -5F);

		defaultScopeModel[6].addShapeBox(0F, 0F, 0F, 9, 2, 12, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import brassMain2
		defaultScopeModel[6].setRotationPoint(-7F, -38F, -6F);

		defaultScopeModel[7].addShapeBox(0F, 0F, 0F, 9, 2, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Import brassMain1-2
		defaultScopeModel[7].setRotationPoint(-7F, -30F, -5F);

		defaultScopeModel[8].addShapeBox(0F, 0F, 0F, 9, 2, 12, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Import brassMain2-2
		defaultScopeModel[8].setRotationPoint(-7F, -32F, -6F);

		defaultScopeModel[9].addBox(0F, 0F, 0F, 9, 4, 12, 0F); // Import brassMain3
		defaultScopeModel[9].setRotationPoint(-7F, -36F, -6F);

		defaultScopeModel[10].addShapeBox(0F, 0F, 0F, 9, 2, 10, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import brassMain1
		defaultScopeModel[10].setRotationPoint(-7F, -40F, -5F);

		defaultScopeModel[11].addBox(0F, 0F, 0F, 13, 3, 10, 0F); // Import Box70
		defaultScopeModel[11].setRotationPoint(21F, -35.5F, -5F);

		defaultScopeModel[12].addShapeBox(0F, 0F, 0F, 13, 3, 10, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import Box71
		defaultScopeModel[12].setRotationPoint(21F, -38.5F, -5F);

		defaultScopeModel[13].addShapeBox(0F, 0F, 0F, 13, 3, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Import Box72
		defaultScopeModel[13].setRotationPoint(21F, -32.5F, -5F);

		defaultScopeModel[14].addShapeBox(0F, 0F, 0F, 3, 3, 11, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import endPart1
		defaultScopeModel[14].setRotationPoint(34F, -39F, -5.5F);

		defaultScopeModel[15].addBox(0F, 0F, 0F, 3, 4, 11, 0F); // Import endPart2
		defaultScopeModel[15].setRotationPoint(34F, -36F, -5.5F);

		defaultScopeModel[16].addShapeBox(0F, 0F, 0F, 3, 3, 11, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Import endPart1-2
		defaultScopeModel[16].setRotationPoint(34F, -32F, -5.5F);

		defaultScopeModel[17].addShapeBox(0F, 0F, 0F, 1, 3, 10, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import Box76
		defaultScopeModel[17].setRotationPoint(-7.1F, -38.5F, -5F);

		defaultScopeModel[18].addBox(0F, 0F, 0F, 1, 3, 10, 0F); // Import Box77
		defaultScopeModel[18].setRotationPoint(-7.1F, -35.5F, -5F);

		defaultScopeModel[19].addShapeBox(0F, 0F, 0F, 1, 3, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Import Box78
		defaultScopeModel[19].setRotationPoint(-7.1F, -32.5F, -5F);

		defaultScopeModel[20].addShapeBox(0F, 0F, 0F, 2, 3, 6, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F); // Import Box79
		defaultScopeModel[20].setRotationPoint(9F, -42F, -3F);

		defaultScopeModel[21].addShapeBox(0F, 0F, 0F, 2, 3, 6, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F); // Import Box80
		defaultScopeModel[21].setRotationPoint(13F, -42F, -3F);

		defaultScopeModel[22].addBox(0F, 0F, 0F, 2, 3, 6, 0F); // Import Box81
		defaultScopeModel[22].setRotationPoint(11F, -42F, -3F);

		defaultScopeModel[23].addShapeBox(0F, 0F, 0F, 2, 6, 3, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F); // Import Box82
		defaultScopeModel[23].setRotationPoint(9F, -37F, -8F);

		defaultScopeModel[24].addBox(0F, 0F, 0F, 2, 6, 3, 0F); // Import Box83
		defaultScopeModel[24].setRotationPoint(11F, -37F, -8F);

		defaultScopeModel[25].addShapeBox(0F, 0F, 0F, 2, 6, 3, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F); // Import Box84
		defaultScopeModel[25].setRotationPoint(13F, -37F, -8F);

		defaultScopeModel[26].addShapeBox(0F, 0F, 0F, 1, 3, 10, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import Box86
		defaultScopeModel[26].setRotationPoint(1.9F, -38.5F, -5F);

		defaultScopeModel[27].addBox(0F, 0F, 0F, 1, 3, 10, 0F); // Import Box87
		defaultScopeModel[27].setRotationPoint(1.9F, -35.5F, -5F);

		defaultScopeModel[28].addShapeBox(0F, 0F, 0F, 1, 3, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Import Box88
		defaultScopeModel[28].setRotationPoint(1.9F, -32.5F, -5F);

		defaultScopeModel[29].addShapeBox(0F, 0F, 0F, 1, 3, 10, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import Box89
		defaultScopeModel[29].setRotationPoint(36.1F, -38.5F, -5F);

		defaultScopeModel[30].addBox(0F, 0F, 0F, 1, 3, 10, 0F); // Import Box90
		defaultScopeModel[30].setRotationPoint(36.1F, -35.5F, -5F);

		defaultScopeModel[31].addShapeBox(0F, 0F, 0F, 1, 3, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Import Box91
		defaultScopeModel[31].setRotationPoint(36.1F, -32.5F, -5F);


		defaultStockModel = new ModelRendererTurbo[9];
		defaultStockModel[0] = new ModelRendererTurbo(this, 157, 159, textureX, textureY); // Import stockEnd1
		defaultStockModel[1] = new ModelRendererTurbo(this, 157, 159, textureX, textureY); // Import stockEnd1-2
		defaultStockModel[2] = new ModelRendererTurbo(this, 157, 170, textureX, textureY); // Import stockEnd2
		defaultStockModel[3] = new ModelRendererTurbo(this, 72, 160, textureX, textureY); // Import stockPipe1
		defaultStockModel[4] = new ModelRendererTurbo(this, 72, 181, textureX, textureY); // Import stockPipe1-2
		defaultStockModel[5] = new ModelRendererTurbo(this, 72, 170, textureX, textureY); // Import stockPipe1-3
		defaultStockModel[6] = new ModelRendererTurbo(this, 72, 170, textureX, textureY); // Import stockPipe1-4
		defaultStockModel[7] = new ModelRendererTurbo(this, 72, 170, textureX, textureY); // Import stockPipe2
		defaultStockModel[8] = new ModelRendererTurbo(this, 72, 170, textureX, textureY); // Import stockPipe2-2

		defaultStockModel[0].addShapeBox(0F, 0F, 0F, 5, 2, 8, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import stockEnd1
		defaultStockModel[0].setRotationPoint(-50F, -20.5F, -4F);

		defaultStockModel[1].addShapeBox(0F, 0F, 0F, 5, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Import stockEnd1-2
		defaultStockModel[1].setRotationPoint(-50F, 0.5F, -4F);

		defaultStockModel[2].addBox(0F, 0F, 0F, 5, 19, 8, 0F); // Import stockEnd2
		defaultStockModel[2].setRotationPoint(-50F, -18.5F, -4F);

		defaultStockModel[3].addShapeBox(0F, 0F, 0F, 35, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import stockPipe1
		defaultStockModel[3].setRotationPoint(-45F, -20F, -3.5F);

		defaultStockModel[4].addShapeBox(0F, 0F, 0F, 35, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Import stockPipe1-2
		defaultStockModel[4].setRotationPoint(-45F, -15F, -3.5F);

		defaultStockModel[5].addShapeBox(0F, 0F, 0F, 35, 2, 7, 0F, 0F, -12F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, -12F, -2F, 0F, 12F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 12F, 0F); // Import stockPipe1-3
		defaultStockModel[5].setRotationPoint(-45F, -17F, -3.5F);

		defaultStockModel[6].addShapeBox(0F, 0F, 0F, 35, 2, 7, 0F, 0F, -12F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -12F, 0F, 0F, 12F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 12F, -2F); // Import stockPipe1-4
		defaultStockModel[6].setRotationPoint(-45F, -12F, -3.5F);

		defaultStockModel[7].addBox(0F, 0F, 0F, 35, 3, 7, 0F); // Import stockPipe2
		defaultStockModel[7].setRotationPoint(-45F, -18F, -3.5F);

		defaultStockModel[8].addShapeBox(0F, 0F, 0F, 35, 3, 7, 0F, 0F, -12F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -12F, 0F, 0F, 12F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 12F, 0F); // Import stockPipe2-2
		defaultStockModel[8].setRotationPoint(-45F, -15F, -3.5F);


		defaultGripModel = new ModelRendererTurbo[10];
		defaultGripModel[0] = new ModelRendererTurbo(this, 55, 70, textureX, textureY); // Import brassPart1
		defaultGripModel[1] = new ModelRendererTurbo(this, 55, 70, textureX, textureY); // Import brassPart1-2
		defaultGripModel[2] = new ModelRendererTurbo(this, 55, 70, textureX, textureY); // Import brassPart1-3
		defaultGripModel[3] = new ModelRendererTurbo(this, 55, 70, textureX, textureY); // Import brassPart1-4
		defaultGripModel[4] = new ModelRendererTurbo(this, 28, 70, textureX, textureY); // Import brassPart2
		defaultGripModel[5] = new ModelRendererTurbo(this, 1, 70, textureX, textureY); // Import brassPart3
		defaultGripModel[6] = new ModelRendererTurbo(this, 1, 11, textureX, textureY); // Import foreRail1
		defaultGripModel[7] = new ModelRendererTurbo(this, 1, 24, textureX, textureY); // Import foreRail2
		defaultGripModel[8] = new ModelRendererTurbo(this, 1, 41, textureX, textureY); // Import foreRail3
		defaultGripModel[9] = new ModelRendererTurbo(this, 1, 57, textureX, textureY); // Import foreRail4

		defaultGripModel[0].addShapeBox(0F, 0F, 0F, 3, 2, 10, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import brassPart1
		defaultGripModel[0].setRotationPoint(38F, -27F, -5F);

		defaultGripModel[1].addShapeBox(0F, 0F, 0F, 3, 2, 10, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import brassPart1-2
		defaultGripModel[1].setRotationPoint(66F, -27F, -5F);

		defaultGripModel[2].addShapeBox(0F, 0F, 0F, 3, 2, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Import brassPart1-3
		defaultGripModel[2].setRotationPoint(66F, -14F, -5F);

		defaultGripModel[3].addShapeBox(0F, 0F, 0F, 3, 2, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Import brassPart1-4
		defaultGripModel[3].setRotationPoint(38F, -12F, -5F);

		defaultGripModel[4].addBox(0F, 0F, 0F, 3, 13, 10, 0F); // Import brassPart2
		defaultGripModel[4].setRotationPoint(38F, -25F, -5F);

		defaultGripModel[5].addBox(0F, 0F, 0F, 3, 11, 10, 0F); // Import brassPart3
		defaultGripModel[5].setRotationPoint(66F, -25F, -5F);

		defaultGripModel[6].addShapeBox(0F, 0F, 0F, 25, 2, 10, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import foreRail1
		defaultGripModel[6].setRotationPoint(41F, -27F, -5F);

		defaultGripModel[7].addBox(0F, 0F, 0F, 25, 6, 10, 0F); // Import foreRail2
		defaultGripModel[7].setRotationPoint(41F, -25F, -5F);

		defaultGripModel[8].addShapeBox(0F, 0F, 0F, 25, 5, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 2F, 0.5F); // Import foreRail3
		defaultGripModel[8].setRotationPoint(41F, -19F, -5F);

		defaultGripModel[9].addShapeBox(0F, 0F, 0F, 25, 2, 10, 0F, 0F, 0F, 0.5F, 0F, 2F, 0.5F, 0F, 2F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, -1F, 0F, -2F, -1F, 0F, -2F, -1F, 0F, 0F, -1F); // Import foreRail4
		defaultGripModel[9].setRotationPoint(41F, -12F, -5F);


		ammoModel = new ModelRendererTurbo[5];
		ammoModel[0] = new ModelRendererTurbo(this, 22, 181, textureX, textureY); // Import bullet
		ammoModel[1] = new ModelRendererTurbo(this, 1, 181, textureX, textureY); // Import bulletTip
		ammoModel[2] = new ModelRendererTurbo(this, 1, 164, textureX, textureY); // Import clip1
		ammoModel[3] = new ModelRendererTurbo(this, 1, 145, textureX, textureY); // Import clip2
		ammoModel[4] = new ModelRendererTurbo(this, 1, 204, textureX, textureY); // Import Box85

		ammoModel[0].addShapeBox(0F, 0F, 0F, 12, 1, 6, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import bullet
		ammoModel[0].setRotationPoint(16F, -11F, -3F);

		ammoModel[1].addShapeBox(0F, 0F, 0F, 4, 1, 6, 0F, 0F, 0F, -1.5F, 0F, -0.5F, -2.5F, 0F, -0.5F, -2.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F); // Import bulletTip
		ammoModel[1].setRotationPoint(28F, -11F, -3F);

		ammoModel[2].addBox(0F, 0F, 0F, 18, 8, 8, 0F); // Import clip1
		ammoModel[2].setRotationPoint(15F, -10F, -4F);

		ammoModel[3].addBox(0F, 0F, 0F, 18, 10, 8, 0F); // Import clip2
		ammoModel[3].setRotationPoint(15F, -2F, -4F);
		ammoModel[3].rotateAngleZ = 0.10471976F;

		ammoModel[4].addBox(0F, 0F, 0F, 19, 4, 9, 0F); // Import Box85
		ammoModel[4].setRotationPoint(14.5F, -2F, -4.5F);
		ammoModel[4].rotateAngleZ = 0.10471976F;


		slideModel = new ModelRendererTurbo[2];
		slideModel[0] = new ModelRendererTurbo(this, 135, 149, textureX, textureY); // Import bolt1
		slideModel[1] = new ModelRendererTurbo(this, 126, 151, textureX, textureY); // Import bolt2

		slideModel[0].addBox(0F, 0F, 0F, 1, 3, 3, 0F); // Import bolt1
		slideModel[0].setRotationPoint(34F, -23F, -7F);

		slideModel[1].addBox(0F, 0F, 0F, 3, 3, 1, 0F); // Import bolt2
		slideModel[1].setRotationPoint(31F, -23F, -4.5F);

		stockAttachPoint = new Vector3f(-10F /16F, 15F /16F, 0F /16F);
		scopeAttachPoint = new Vector3f(12F /16F, 25F /16F, 0F /16F);
		gripAttachPoint = new Vector3f(38 /16F, 19F /16F, 0F /16F);

		gunSlideDistance = 0.8F;
		animationType = EnumAnimationType.BOTTOM_CLIP;


		translateAll(0F, 0F, 0F);


		flipAll();
	}
}