package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelLuabellaCarbine extends ModelGun
{
	int textureX = 512;
	int textureY = 256;

	public ModelLuabellaCarbine()
	{
		gunModel = new ModelRendererTurbo[30];
		gunModel[0] = new ModelRendererTurbo(this, 80, 1, textureX, textureY); // barrel1
		gunModel[1] = new ModelRendererTurbo(this, 80, 1, textureX, textureY); // barrel1-2
		gunModel[2] = new ModelRendererTurbo(this, 80, 10, textureX, textureY); // barrel2
		gunModel[3] = new ModelRendererTurbo(this, 80, 36, textureX, textureY); // body2
		gunModel[4] = new ModelRendererTurbo(this, 80, 49, textureX, textureY); // body3
		gunModel[5] = new ModelRendererTurbo(this, 80, 111, textureX, textureY); // body4
		gunModel[6] = new ModelRendererTurbo(this, 80, 127, textureX, textureY); // body5
		gunModel[7] = new ModelRendererTurbo(this, 168, 56, textureX, textureY); // body6
		gunModel[8] = new ModelRendererTurbo(this, 168, 67, textureX, textureY); // body7
		gunModel[9] = new ModelRendererTurbo(this, 151, 54, textureX, textureY); // body8
		gunModel[10] = new ModelRendererTurbo(this, 80, 19, textureX, textureY); // Box 0
		gunModel[11] = new ModelRendererTurbo(this, 46, 39, textureX, textureY); // Box 23
		gunModel[12] = new ModelRendererTurbo(this, 46, 39, textureX, textureY); // Box 24
		gunModel[13] = new ModelRendererTurbo(this, 46, 39, textureX, textureY); // Box 25
		gunModel[14] = new ModelRendererTurbo(this, 46, 39, textureX, textureY); // Box 26
		gunModel[15] = new ModelRendererTurbo(this, 46, 39, textureX, textureY); // Box 46
		gunModel[16] = new ModelRendererTurbo(this, 80, 131, textureX, textureY); // Box 54
		gunModel[17] = new ModelRendererTurbo(this, 46, 29, textureX, textureY); // Box 7
		gunModel[18] = new ModelRendererTurbo(this, 168, 76, textureX, textureY); // clipConnector
		gunModel[19] = new ModelRendererTurbo(this, 141, 68, textureX, textureY); // foreRailConnector
		gunModel[20] = new ModelRendererTurbo(this, 163, 10, textureX, textureY); // gasBlock
		gunModel[21] = new ModelRendererTurbo(this, 163, 10, textureX, textureY); // gasBlock1-2
		gunModel[22] = new ModelRendererTurbo(this, 163, 10, textureX, textureY); // gasBlock1-3
		gunModel[23] = new ModelRendererTurbo(this, 1, 65, textureX, textureY); // grip1
		gunModel[24] = new ModelRendererTurbo(this, 1, 93, textureX, textureY); // grip2
		gunModel[25] = new ModelRendererTurbo(this, 1, 36, textureX, textureY); // gripConnector
		gunModel[26] = new ModelRendererTurbo(this, 1, 49, textureX, textureY); // gripConnector2
		gunModel[27] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // ironSightBase1
		gunModel[28] = new ModelRendererTurbo(this, 0, 138, textureX, textureY); // rail
		gunModel[29] = new ModelRendererTurbo(this, 46, 39, textureX, textureY); // railBlock

		gunModel[0].addShapeBox(0F, 0F, 0F, 35, 2, 6, 0F,0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // barrel1
		gunModel[0].setRotationPoint(32F, -17F, -3F);

		gunModel[1].addShapeBox(0F, 0F, 0F, 35, 2, 6, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // barrel1-2
		gunModel[1].setRotationPoint(32F, -13F, -3F);

		gunModel[2].addBox(0F, 0F, 0F, 35, 2, 6, 0F); // barrel2
		gunModel[2].setRotationPoint(32F, -15F, -3F);

		gunModel[3].addShapeBox(0F, 0F, 0F, 44, 2, 10, 0F,0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body2
		gunModel[3].setRotationPoint(-16F, -23F, -5F);

		gunModel[4].addBox(0F, 0F, 0F, 30, 3, 10, 0F); // body3
		gunModel[4].setRotationPoint(-2F, -13F, -5F);

		gunModel[5].addBox(0F, 0F, 0F, 17, 5, 10, 0F); // body4
		gunModel[5].setRotationPoint(8F, -10F, -5F);

		gunModel[6].addBox(0F, 0F, 0F, 42, 2, 1, 0F); // body5
		gunModel[6].setRotationPoint(-15F, -20F, 4.5F);

		gunModel[7].addBox(0F, 0F, 0F, 25, 8, 2, 0F); // body6
		gunModel[7].setRotationPoint(-16F, -21F, -5F);

		gunModel[8].addBox(0F, 0F, 0F, 19, 6, 2, 0F); // body7
		gunModel[8].setRotationPoint(9F, -19F, -5F);

		gunModel[9].addBox(0F, 0F, 0F, 2, 2, 2, 0F); // body8
		gunModel[9].setRotationPoint(26F, -21F, -5F);

		gunModel[10].addBox(0F, 0F, 0F, 44, 8, 8, 0F); // Box 0
		gunModel[10].setRotationPoint(-16F, -21F, -3F);

		gunModel[11].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 23
		gunModel[11].setRotationPoint(1F, -29F, -3.5F);

		gunModel[12].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 24
		gunModel[12].setRotationPoint(13F, -29F, -3.5F);

		gunModel[13].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 25
		gunModel[13].setRotationPoint(7F, -29F, -3.5F);

		gunModel[14].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 26
		gunModel[14].setRotationPoint(19F, -29F, -3.5F);

		gunModel[15].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 46
		gunModel[15].setRotationPoint(25F, -29F, -3.5F);

		gunModel[16].addBox(0F, 0F, 0F, 8, 2, 1, 0F); // Box 54
		gunModel[16].setRotationPoint(-1F, -13F, 4.5F);

		gunModel[17].addBox(0F, 0F, 0F, 4, 2, 7, 0F); // Box 7
		gunModel[17].setRotationPoint(-16F, -25F, -3.5F);

		gunModel[18].addBox(0F, 0F, 0F, 17, 2, 9, 0F); // clipConnector
		gunModel[18].setRotationPoint(8F, -5F, -4.5F);

		gunModel[19].addBox(0F, 0F, 0F, 1, 10, 6, 0F); // foreRailConnector
		gunModel[19].setRotationPoint(28F, -23F, -3F);

		gunModel[20].addShapeBox(0F, 0F, 0F, 5, 2, 6, 0F,0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // gasBlock
		gunModel[20].setRotationPoint(49F, -24F, -3F);

		gunModel[21].addBox(0F, 0F, 0F, 5, 2, 6, 0F); // gasBlock1-2
		gunModel[21].setRotationPoint(49F, -22F, -3F);

		gunModel[22].addShapeBox(0F, 0F, 0F, 5, 2, 6, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // gasBlock1-3
		gunModel[22].setRotationPoint(49F, -20F, -3F);

		gunModel[23].addShapeBox(0F, 0F, 0F, 12, 18, 9, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, -2F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 4F, -2F, 0F); // grip1
		gunModel[23].setRotationPoint(-14F, -11F, -4.5F);

		gunModel[24].addBox(0F, 0F, 0F, 13, 2, 9, 0F); // grip2
		gunModel[24].setRotationPoint(-18F, 5F, -4.5F);
		gunModel[24].rotateAngleZ = -0.17453293F;

		gunModel[25].addShapeBox(0F, 0F, 0F, 12, 2, 10, 0F,2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // gripConnector
		gunModel[25].setRotationPoint(-14F, -13F, -5F);

		gunModel[26].addBox(0F, 0F, 0F, 7, 5, 10, 0F); // gripConnector2
		gunModel[26].setRotationPoint(-9F, -11F, -5F);

		gunModel[27].addBox(0F, 0F, 0F, 7, 6, 9, 0F); // ironSightBase1
		gunModel[27].setRotationPoint(-12F, -28F, -4.5F);

		gunModel[28].addBox(0F, 0F, 0F, 33, 4, 7, 0F); // rail
		gunModel[28].setRotationPoint(-5F, -27F, -3.5F);

		gunModel[29].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // railBlock
		gunModel[29].setRotationPoint(-5F, -29F, -3.5F);


		defaultScopeModel = new ModelRendererTurbo[9];
		defaultScopeModel[0] = new ModelRendererTurbo(this, 34, 11, textureX, textureY); // Box 42
		defaultScopeModel[1] = new ModelRendererTurbo(this, 34, 11, textureX, textureY); // Box 43
		defaultScopeModel[2] = new ModelRendererTurbo(this, 41, 14, textureX, textureY); // Box 45
		defaultScopeModel[3] = new ModelRendererTurbo(this, 47, 16, textureX, textureY); // frontSight1
		defaultScopeModel[4] = new ModelRendererTurbo(this, 1, 5, textureX, textureY); // ironBolt1
		defaultScopeModel[5] = new ModelRendererTurbo(this, 1, 5, textureX, textureY); // ironBolt1-2
		defaultScopeModel[6] = new ModelRendererTurbo(this, 32, 19, textureX, textureY); // ironSight1
		defaultScopeModel[7] = new ModelRendererTurbo(this, 32, 19, textureX, textureY); // ironSight1-2
		defaultScopeModel[8] = new ModelRendererTurbo(this, 1, 17, textureX, textureY); // ironSightBase2

		defaultScopeModel[0].addBox(0F, 0F, 0F, 2, 6, 1, 0F); // Box 42
		defaultScopeModel[0].setRotationPoint(49F, -33F, 2F);

		defaultScopeModel[1].addBox(0F, 0F, 0F, 2, 6, 1, 0F); // Box 43
		defaultScopeModel[1].setRotationPoint(49F, -33F, -3F);

		defaultScopeModel[2].addBox(0F, 0F, 0F, 2, 3, 1, 0F); // Box 45
		defaultScopeModel[2].setRotationPoint(49F, -30F, -0.5F);

		defaultScopeModel[3].addBox(0F, 0F, 0F, 2, 5, 6, 0F); // frontSight1
		defaultScopeModel[3].setRotationPoint(49F, -27F, -3F);

		defaultScopeModel[4].addBox(0F, 0F, 0F, 3, 3, 1, 0F); // ironBolt1
		defaultScopeModel[4].setRotationPoint(-9F, -32F, 4F);

		defaultScopeModel[5].addBox(0F, 0F, 0F, 3, 3, 1, 0F); // ironBolt1-2
		defaultScopeModel[5].setRotationPoint(-9F, -32F, -5F);

		defaultScopeModel[6].addBox(0F, 0F, 0F, 5, 6, 2, 0F); // ironSight1
		defaultScopeModel[6].setRotationPoint(-10F, -36F, 2F);

		defaultScopeModel[7].addBox(0F, 0F, 0F, 5, 6, 2, 0F); // ironSight1-2
		defaultScopeModel[7].setRotationPoint(-10F, -36F, -4F);

		defaultScopeModel[8].addBox(0F, 0F, 0F, 7, 2, 8, 0F); // ironSightBase2
		defaultScopeModel[8].setRotationPoint(-12F, -30F, -4F);


		defaultStockModel = new ModelRendererTurbo[8];
		defaultStockModel[0] = new ModelRendererTurbo(this, 80, 145, textureX, textureY); // stock1
		defaultStockModel[1] = new ModelRendererTurbo(this, 165, 146, textureX, textureY); // stock2
		defaultStockModel[2] = new ModelRendererTurbo(this, 80, 136, textureX, textureY); // stock3
		defaultStockModel[3] = new ModelRendererTurbo(this, 144, 137, textureX, textureY); // stock4
		defaultStockModel[4] = new ModelRendererTurbo(this, 161, 178, textureX, textureY); // stock5
		defaultStockModel[5] = new ModelRendererTurbo(this, 144, 172, textureX, textureY); // stock6
		defaultStockModel[6] = new ModelRendererTurbo(this, 80, 178, textureX, textureY); // stock7
		defaultStockModel[7] = new ModelRendererTurbo(this, 80, 162, textureX, textureY); // stock8

		defaultStockModel[0].addShapeBox(0F, 0F, 0F, 22, 10, 6, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 6F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 6F, 0F); // stock1
		defaultStockModel[0].setRotationPoint(-40F, -22F, -3F);

		defaultStockModel[1].addShapeBox(0F, 0F, 0F, 6, 16, 6, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 6F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 6F, 0F); // stock2
		defaultStockModel[1].setRotationPoint(-46F, -22F, -3F);

		defaultStockModel[2].addShapeBox(0F, 0F, 0F, 28, 2, 6, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // stock3
		defaultStockModel[2].setRotationPoint(-46F, -24F, -3F);

		defaultStockModel[3].addBox(0F, 0F, 0F, 2, 23, 8, 0F); // stock4
		defaultStockModel[3].setRotationPoint(-48F, -22F, -4F);

		defaultStockModel[4].addShapeBox(0F, 0F, 0F, 2, 2, 8, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // stock5
		defaultStockModel[4].setRotationPoint(-48F, -24F, -4F);

		defaultStockModel[5].addBox(0F, 0F, 0F, 2, 10, 6, 0F); // stock6
		defaultStockModel[5].setRotationPoint(-18F, -23F, -3F);

		defaultStockModel[6].addShapeBox(0F, 0F, 0F, 15, 2, 8, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // stock7
		defaultStockModel[6].setRotationPoint(-39F, -24.5F, -4F);

		defaultStockModel[7].addBox(0F, 0F, 0F, 15, 7, 8, 0F); // stock8
		defaultStockModel[7].setRotationPoint(-39F, -22.5F, -4F);


		defaultGripModel = new ModelRendererTurbo[5];
		defaultGripModel[0] = new ModelRendererTurbo(this, 80, 85, textureX, textureY); // foreRail1
		defaultGripModel[1] = new ModelRendererTurbo(this, 80, 85, textureX, textureY); // foreRail1-2
		defaultGripModel[2] = new ModelRendererTurbo(this, 80, 63, textureX, textureY); // foreRail2
		defaultGripModel[3] = new ModelRendererTurbo(this, 80, 98, textureX, textureY); // foreRail3
		defaultGripModel[4] = new ModelRendererTurbo(this, 80, 98, textureX, textureY); // foreRail3-2

		defaultGripModel[0].addShapeBox(0F, 0F, 0F, 20, 2, 10, 0F,0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // foreRail1
		defaultGripModel[0].setRotationPoint(29F, -25F, -5F);

		defaultGripModel[1].addShapeBox(0F, 0F, 0F, 20, 2, 10, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // foreRail1-2
		defaultGripModel[1].setRotationPoint(29F, -12F, -5F);

		defaultGripModel[2].addBox(0F, 0F, 0F, 20, 11, 10, 0F); // foreRail2
		defaultGripModel[2].setRotationPoint(29F, -23F, -5F);

		defaultGripModel[3].addBox(0F, 0F, 0F, 18, 1, 11, 0F); // foreRail3
		defaultGripModel[3].setRotationPoint(30F, -17F, -5.5F);

		defaultGripModel[4].addBox(0F, 0F, 0F, 18, 1, 11, 0F); // foreRail3-2
		defaultGripModel[4].setRotationPoint(30F, -15F, -5.5F);


		ammoModel = new ModelRendererTurbo[3];
		ammoModel[0] = new ModelRendererTurbo(this, 1, 129, textureX, textureY); // bullet
		ammoModel[1] = new ModelRendererTurbo(this, 32, 129, textureX, textureY); // bulletTip
		ammoModel[2] = new ModelRendererTurbo(this, 1, 105, textureX, textureY); // clip1

		ammoModel[0].addShapeBox(0F, 0F, 0F, 9, 1, 6, 0F,0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // bullet
		ammoModel[0].setRotationPoint(10F, -7F, -3F);

		ammoModel[1].addShapeBox(0F, 0F, 0F, 4, 1, 6, 0F,0F, 0F, -1.5F, 0F, -0.5F, -2.5F, 0F, -0.5F, -2.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // bulletTip
		ammoModel[1].setRotationPoint(19F, -7F, -3F);

		ammoModel[2].addBox(0F, 0F, 0F, 15, 15, 8, 0F); // clip1
		ammoModel[2].setRotationPoint(9F, -6F, -4F);


		slideModel = new ModelRendererTurbo[2];
		slideModel[0] = new ModelRendererTurbo(this, 61, 6, textureX, textureY); // slider
		slideModel[1] = new ModelRendererTurbo(this, 61, 1, textureX, textureY); // slider2

		slideModel[0].addBox(0F, 0F, 0F, 2, 2, 6, 0F); // slider
		slideModel[0].setRotationPoint(24F, -21F, -9F);

		slideModel[1].addBox(0F, 0F, 0F, 7, 2, 2, 0F); // slider2
		slideModel[1].setRotationPoint(17F, -21F, -4.5F);



		barrelAttachPoint = new Vector3f(0F /16F, 0F /16F, 0F /16F);
		stockAttachPoint = new Vector3f(0F /16F, 0F /16F, 0F /16F);
		scopeAttachPoint = new Vector3f(0F /16F, 0F /16F, 0F /16F);
		gripAttachPoint = new Vector3f(0 /16F, 0F /16F, 0F /16F);

		gunSlideDistance = 0.8F;
		animationType = EnumAnimationType.BOTTOM_CLIP;


		translateAll(0F, 0F, 0F);


		flipAll();
	}
}