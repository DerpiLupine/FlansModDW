package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.ModelAttachment;
import com.flansmod.client.tmt.ModelRendererTurbo;

import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelLPMAssaultFlashlight extends ModelAttachment 
{
	int textureX = 512;
	int textureY = 256;

	public ModelLPMAssaultFlashlight()
	{
		attachmentModel = new ModelRendererTurbo[19];
		attachmentModel[0] = new ModelRendererTurbo(this, 240, 61, textureX, textureY); // flashlight1
		attachmentModel[1] = new ModelRendererTurbo(this, 286, 80, textureX, textureY); // grip2
		attachmentModel[2] = new ModelRendererTurbo(this, 240, 36, textureX, textureY); // gripBase
		attachmentModel[3] = new ModelRendererTurbo(this, 283, 37, textureX, textureY); // gripBase2
		attachmentModel[4] = new ModelRendererTurbo(this, 240, 61, textureX, textureY); // flashlight1
		attachmentModel[5] = new ModelRendererTurbo(this, 240, 48, textureX, textureY); // flashlight2
		attachmentModel[6] = new ModelRendererTurbo(this, 263, 75, textureX, textureY); // grip1
		attachmentModel[7] = new ModelRendererTurbo(this, 263, 75, textureX, textureY); // grip1
		attachmentModel[8] = new ModelRendererTurbo(this, 263, 75, textureX, textureY); // grip1
		attachmentModel[9] = new ModelRendererTurbo(this, 263, 75, textureX, textureY); // grip1
		attachmentModel[10] = new ModelRendererTurbo(this, 240, 72, textureX, textureY); // grip2
		attachmentModel[11] = new ModelRendererTurbo(this, 240, 72, textureX, textureY); // grip2
		attachmentModel[12] = new ModelRendererTurbo(this, 297, 61, textureX, textureY); // frontLight1
		attachmentModel[13] = new ModelRendererTurbo(this, 297, 48, textureX, textureY); // frontLight2
		attachmentModel[14] = new ModelRendererTurbo(this, 297, 61, textureX, textureY); // frontLight1
		attachmentModel[15] = new ModelRendererTurbo(this, 304, 39, textureX, textureY); // Box 29
		attachmentModel[16] = new ModelRendererTurbo(this, 304, 39, textureX, textureY); // Box 30
		attachmentModel[17] = new ModelRendererTurbo(this, 304, 39, textureX, textureY); // Box 31
		attachmentModel[18] = new ModelRendererTurbo(this, 286, 73, textureX, textureY); // unit

		attachmentModel[0].addShapeBox(-11F, 4F, -4F, 20, 2, 8, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // flashlight1
		attachmentModel[0].setRotationPoint(0F, 0F, 0F);

		attachmentModel[1].addBox(-5F, 3F, -2.5F, 9, 1, 5, 0F); // grip2
		attachmentModel[1].setRotationPoint(0F, 0F, 0F);

		attachmentModel[2].addBox(-7F, 0F, -4F, 13, 3, 8, 0F); // gripBase
		attachmentModel[2].setRotationPoint(0F, 0F, 0F);

		attachmentModel[3].addShapeBox(6F, 1F, -4F, 2, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // gripBase2
		attachmentModel[3].setRotationPoint(0F, 0F, 0F);

		attachmentModel[4].addShapeBox(-11F, 10F, -4F, 20, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // flashlight1
		attachmentModel[4].setRotationPoint(0F, 0F, 0F);

		attachmentModel[5].addBox(-11F, 6F, -4F, 20, 4, 8, 0F); // flashlight2
		attachmentModel[5].setRotationPoint(0F, 0F, 0F);

		attachmentModel[6].addShapeBox(-4F, 3.5F, -4.5F, 2, 2, 9, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // grip1
		attachmentModel[6].setRotationPoint(0F, 0F, 0F);

		attachmentModel[7].addShapeBox(1F, 3.5F, -4.5F, 2, 2, 9, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // grip1
		attachmentModel[7].setRotationPoint(0F, 0F, 0F);

		attachmentModel[8].addShapeBox(1F, 10.5F, -4.5F, 2, 2, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // grip1
		attachmentModel[8].setRotationPoint(0F, 0F, 0F);

		attachmentModel[9].addShapeBox(-4F, 10.5F, -4.5F, 2, 2, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // grip1
		attachmentModel[9].setRotationPoint(0F, 0F, 0F);

		attachmentModel[10].addBox(1F, 5.5F, -4.5F, 2, 5, 9, 0F); // grip2
		attachmentModel[10].setRotationPoint(0F, 0F, 0F);

		attachmentModel[11].addBox(-4F, 5.5F, -4.5F, 2, 5, 9, 0F); // grip2
		attachmentModel[11].setRotationPoint(0F, 0F, 0F);

		attachmentModel[12].addShapeBox(10F, 4F, -4F, 7, 2, 8, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // frontLight1
		attachmentModel[12].setRotationPoint(0F, 0F, 0F);

		attachmentModel[13].addBox(10F, 6F, -4F, 7, 4, 8, 0F); // frontLight2
		attachmentModel[13].setRotationPoint(0F, 0F, 0F);

		attachmentModel[14].addShapeBox(10F, 10F, -4F, 7, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // frontLight1
		attachmentModel[14].setRotationPoint(0F, 0F, 0F);

		attachmentModel[15].addBox(9F, 7F, -3F, 1, 2, 6, 0F); // Box 29
		attachmentModel[15].setRotationPoint(0F, 0F, 0F);

		attachmentModel[16].addShapeBox(9F, 5F, -3F, 1, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 30
		attachmentModel[16].setRotationPoint(0F, 0F, 0F);

		attachmentModel[17].addShapeBox(9F, 9F, -3F, 1, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 31
		attachmentModel[17].setRotationPoint(0F, 0F, 0F);

		attachmentModel[18].addBox(-5F, 6F, 3F, 9, 4, 2, 0F); // unit
		attachmentModel[18].setRotationPoint(0F, 0F, 0F);

		renderOffset = 0F;
		
		flipAll();
	}
}