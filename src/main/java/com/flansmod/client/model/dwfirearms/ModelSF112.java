package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelSF112 extends ModelGun
{
	int textureX = 512;
	int textureY = 256;

	public ModelSF112()
	{
		gunModel = new ModelRendererTurbo[47];
		gunModel[0] = new ModelRendererTurbo(this, 72, 32, textureX, textureY); // body1
		gunModel[1] = new ModelRendererTurbo(this, 72, 53, textureX, textureY); // body10
		gunModel[2] = new ModelRendererTurbo(this, 98, 133, textureX, textureY); // body11
		gunModel[3] = new ModelRendererTurbo(this, 99, 139, textureX, textureY); // body12
		gunModel[4] = new ModelRendererTurbo(this, 72, 88, textureX, textureY); // body2
		gunModel[5] = new ModelRendererTurbo(this, 72, 103, textureX, textureY); // body3
		gunModel[6] = new ModelRendererTurbo(this, 72, 119, textureX, textureY); // body5
		gunModel[7] = new ModelRendererTurbo(this, 72, 131, textureX, textureY); // body6
		gunModel[8] = new ModelRendererTurbo(this, 72, 75, textureX, textureY); // body8
		gunModel[9] = new ModelRendererTurbo(this, 72, 64, textureX, textureY); // body9
		gunModel[10] = new ModelRendererTurbo(this, 201, 37, textureX, textureY); // Box 1
		gunModel[11] = new ModelRendererTurbo(this, 48, 34, textureX, textureY); // scopeBaseFront
		gunModel[12] = new ModelRendererTurbo(this, 176, 35, textureX, textureY); // Box 3
		gunModel[13] = new ModelRendererTurbo(this, 201, 37, textureX, textureY); // Box 4
		gunModel[14] = new ModelRendererTurbo(this, 176, 35, textureX, textureY); // Box 5
		gunModel[15] = new ModelRendererTurbo(this, 72, 144, textureX, textureY); // Box 71
		gunModel[16] = new ModelRendererTurbo(this, 72, 144, textureX, textureY); // Box 72
		gunModel[17] = new ModelRendererTurbo(this, 72, 144, textureX, textureY); // Box 73
		gunModel[18] = new ModelRendererTurbo(this, 72, 144, textureX, textureY); // Box 74
		gunModel[19] = new ModelRendererTurbo(this, 1, 175, textureX, textureY); // clipPart
		gunModel[20] = new ModelRendererTurbo(this, 1, 155, textureX, textureY); // clipPart2
		gunModel[21] = new ModelRendererTurbo(this, 157, 2, textureX, textureY); // gasBlock1
		gunModel[22] = new ModelRendererTurbo(this, 157, 2, textureX, textureY); // gasBlock1-2
		gunModel[23] = new ModelRendererTurbo(this, 157, 2, textureX, textureY); // gasBlock1-3
		gunModel[24] = new ModelRendererTurbo(this, 153, 32, textureX, textureY); // gasBlockConnector
		gunModel[25] = new ModelRendererTurbo(this, 157, 11, textureX, textureY); // gasBlockConnector2
		gunModel[26] = new ModelRendererTurbo(this, 157, 21, textureX, textureY); // gasBlockConnector2-2
		gunModel[27] = new ModelRendererTurbo(this, 157, 11, textureX, textureY); // gasBlockConnector2-3
		gunModel[28] = new ModelRendererTurbo(this, 1, 63, textureX, textureY); // grip1
		gunModel[29] = new ModelRendererTurbo(this, 1, 35, textureX, textureY); // grip2
		gunModel[30] = new ModelRendererTurbo(this, 1, 77, textureX, textureY); // grip3
		gunModel[31] = new ModelRendererTurbo(this, 1, 77, textureX, textureY); // grip4
		gunModel[32] = new ModelRendererTurbo(this, 152, 80, textureX, textureY); // handGuard1
		gunModel[33] = new ModelRendererTurbo(this, 197, 112, textureX, textureY); // handGuard2
		gunModel[34] = new ModelRendererTurbo(this, 152, 112, textureX, textureY); // handGuard3
		gunModel[35] = new ModelRendererTurbo(this, 152, 98, textureX, textureY); // handGuard4
		gunModel[36] = new ModelRendererTurbo(this, 1, 16, textureX, textureY); // ironSightBase
		gunModel[37] = new ModelRendererTurbo(this, 72, 20, textureX, textureY); // mainBarrelBottom
		gunModel[38] = new ModelRendererTurbo(this, 72, 11, textureX, textureY); // mainBarrelMiddle
		gunModel[39] = new ModelRendererTurbo(this, 72, 2, textureX, textureY); // mainBarrelTop
		gunModel[40] = new ModelRendererTurbo(this, 72, 29, textureX, textureY); // pipeCleaner
		gunModel[41] = new ModelRendererTurbo(this, 152, 135, textureX, textureY); // railBase
		gunModel[42] = new ModelRendererTurbo(this, 152, 126, textureX, textureY); // railBase2
		gunModel[43] = new ModelRendererTurbo(this, 182, 11, textureX, textureY); // ring1
		gunModel[44] = new ModelRendererTurbo(this, 182, 11, textureX, textureY); // ring1-2
		gunModel[45] = new ModelRendererTurbo(this, 182, 21, textureX, textureY); // ring2
		gunModel[46] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // scopeBase

		gunModel[0].addBox(0F, 0F, 0F, 20, 10, 10, 0F); // body1
		gunModel[0].setRotationPoint(-10F, -17F, -5F);

		gunModel[1].addShapeBox(0F, 0F, 0F, 36, 2, 8, 0F, -1F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, -1F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body10
		gunModel[1].setRotationPoint(-8F, -23F, -4F);

		gunModel[2].addShapeBox(0F, 0F, 0F, 3, 2, 3, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body11
		gunModel[2].setRotationPoint(-9F, -22F, -1.5F);

		gunModel[3].addBox(0F, 0F, 0F, 12, 3, 1, 0F); // body12
		gunModel[3].setRotationPoint(14F, -16F, 4.5F);

		gunModel[4].addBox(0F, 0F, 0F, 21, 5, 9, 0F); // body2
		gunModel[4].setRotationPoint(10F, -12F, -4.5F);

		gunModel[5].addBox(0F, 0F, 0F, 21, 5, 10, 0F); // body3
		gunModel[5].setRotationPoint(10F, -17F, -5F);

		gunModel[6].addShapeBox(0F, 0F, 0F, 15, 2, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body5
		gunModel[6].setRotationPoint(10F, -19F, -4F);

		gunModel[7].addShapeBox(0F, 0F, 0F, 3, 2, 10, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body6
		gunModel[7].setRotationPoint(25F, -19F, -5F);

		gunModel[8].addShapeBox(0F, 0F, 0F, 20, 2, 10, 0F, -1F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, -1F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body8
		gunModel[8].setRotationPoint(-10F, -19F, -5F);

		gunModel[9].addShapeBox(0F, 0F, 0F, 37, 2, 8, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body9
		gunModel[9].setRotationPoint(-9F, -21F, -4F);

		gunModel[10].addShapeBox(0F, 0F, 0F, 3, 2, 9, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 1
		gunModel[10].setRotationPoint(0F, -24F, -4.5F);

		gunModel[11].addBox(0F, 0F, 0F, 3, 4, 6, 0F); // scopeBaseFront
		gunModel[11].setRotationPoint(60.9F, -24F, -3F);

		gunModel[12].addBox(0F, 0F, 0F, 3, 4, 9, 0F); // Box 3
		gunModel[12].setRotationPoint(0F, -22F, -4.5F);

		gunModel[13].addShapeBox(0F, 0F, 0F, 3, 2, 9, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 4
		gunModel[13].setRotationPoint(14F, -24F, -4.5F);

		gunModel[14].addBox(0F, 0F, 0F, 3, 4, 9, 0F); // Box 5
		gunModel[14].setRotationPoint(14F, -22F, -4.5F);

		gunModel[15].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 71
		gunModel[15].setRotationPoint(31F, -6F, -3.5F);

		gunModel[16].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 72
		gunModel[16].setRotationPoint(37F, -6F, -3.5F);

		gunModel[17].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 73
		gunModel[17].setRotationPoint(43F, -6F, -3.5F);

		gunModel[18].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 74
		gunModel[18].setRotationPoint(49F, -6F, -3.5F);

		gunModel[19].addShapeBox(0F, 0F, 0F, 15, 8, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F); // clipPart
		gunModel[19].setRotationPoint(10F, -7F, -4.5F);

		gunModel[20].addShapeBox(0F, 0F, 0F, 13, 9, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F); // clipPart2
		gunModel[20].setRotationPoint(11F, -9F, -5F);

		gunModel[21].addShapeBox(0F, 0F, 0F, 12, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // gasBlock1
		gunModel[21].setRotationPoint(52F, -22F, -3F);

		gunModel[22].addBox(0F, 0F, 0F, 12, 2, 6, 0F); // gasBlock1-2
		gunModel[22].setRotationPoint(52F, -20F, -3F);

		gunModel[23].addShapeBox(0F, 0F, 0F, 12, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // gasBlock1-3
		gunModel[23].setRotationPoint(52F, -18F, -3F);

		gunModel[24].addBox(0F, 0F, 0F, 5, 1, 3, 0F); // gasBlockConnector
		gunModel[24].setRotationPoint(59F, -16F, -1.5F);

		gunModel[25].addShapeBox(0F, 0F, 0F, 5, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // gasBlockConnector2
		gunModel[25].setRotationPoint(59F, -15F, -3.5F);

		gunModel[26].addBox(0F, 0F, 0F, 5, 3, 7, 0F); // gasBlockConnector2-2
		gunModel[26].setRotationPoint(59F, -13F, -3.5F);

		gunModel[27].addShapeBox(0F, 0F, 0F, 5, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // gasBlockConnector2-3
		gunModel[27].setRotationPoint(59F, -10F, -3.5F);

		gunModel[28].addShapeBox(0F, 0F, 0F, 13, 3, 10, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F); // grip1
		gunModel[28].setRotationPoint(-10F, -7F, -5F);

		gunModel[29].addShapeBox(0F, 0F, 0F, 12, 12, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 4F, 0F, 0F); // grip2
		gunModel[29].setRotationPoint(-9F, -4F, -5F);

		gunModel[30].addShapeBox(0F, 0F, 0F, 12, 2, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F); // grip3
		gunModel[30].setRotationPoint(-13F, 8F, -5F);

		gunModel[31].addShapeBox(0F, 0F, 0F, 12, 2, 10, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F); // grip4
		gunModel[31].setRotationPoint(-13F, 10F, -5F);

		gunModel[32].addBox(0F, 0F, 0F, 21, 7, 10, 0F); // handGuard1
		gunModel[32].setRotationPoint(31F, -17F, -5F);

		gunModel[33].addBox(0F, 0F, 0F, 12, 3, 10, 0F); // handGuard2
		gunModel[33].setRotationPoint(40F, -20F, -5F);

		gunModel[34].addShapeBox(0F, 0F, 0F, 12, 3, 10, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // handGuard3
		gunModel[34].setRotationPoint(40F, -23F, -5F);

		gunModel[35].addShapeBox(0F, 0F, 0F, 21, 3, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // handGuard4
		gunModel[35].setRotationPoint(31F, -10F, -5F);

		gunModel[36].addShapeBox(0F, 0F, 0F, 12, 2, 9, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // ironSightBase
		gunModel[36].setRotationPoint(28F, -24F, -4.5F);

		gunModel[37].addShapeBox(0F, 0F, 0F, 36, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // mainBarrelBottom
		gunModel[37].setRotationPoint(52F, -10.5F, -3F);

		gunModel[38].addBox(0F, 0F, 0F, 36, 2, 6, 0F); // mainBarrelMiddle
		gunModel[38].setRotationPoint(52F, -12.5F, -3F);

		gunModel[39].addShapeBox(0F, 0F, 0F, 36, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // mainBarrelTop
		gunModel[39].setRotationPoint(52F, -14.5F, -3F);

		gunModel[40].addBox(0F, 0F, 0F, 31, 1, 1, 0F); // pipeCleaner
		gunModel[40].setRotationPoint(52F, -8F, -0.5F);

		gunModel[41].addBox(0F, 0F, 0F, 15, 2, 7, 0F); // railBase
		gunModel[41].setRotationPoint(34F, -9F, -3.5F);

		gunModel[42].addBox(0F, 0F, 0F, 21, 1, 7, 0F); // railBase2
		gunModel[42].setRotationPoint(31F, -7F, -3.5F);

		gunModel[43].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // ring1
		gunModel[43].setRotationPoint(80F, -15F, -3.5F);

		gunModel[44].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // ring1-2
		gunModel[44].setRotationPoint(80F, -10F, -3.5F);

		gunModel[45].addBox(0F, 0F, 0F, 3, 3, 7, 0F); // ring2
		gunModel[45].setRotationPoint(80F, -13F, -3.5F);

		gunModel[46].addBox(0F, 0F, 0F, 12, 5, 9, 0F); // scopeBase
		gunModel[46].setRotationPoint(28F, -22F, -4.5F);


		defaultBarrelModel = new ModelRendererTurbo[3];
		defaultBarrelModel[0] = new ModelRendererTurbo(this, 203, 11, textureX, textureY); // muzzle1
		defaultBarrelModel[1] = new ModelRendererTurbo(this, 203, 21, textureX, textureY); // muzzle2
		defaultBarrelModel[2] = new ModelRendererTurbo(this, 203, 11, textureX, textureY); // muzzle1-2

		defaultBarrelModel[0].addShapeBox(0F, 0F, 0F, 10, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // muzzle1
		defaultBarrelModel[0].setRotationPoint(88F, -15F, -3.5F);

		defaultBarrelModel[1].addBox(0F, 0F, 0F, 10, 3, 7, 0F); // muzzle2
		defaultBarrelModel[1].setRotationPoint(88F, -13F, -3.5F);

		defaultBarrelModel[2].addShapeBox(0F, 0F, 0F, 10, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // muzzle1-2
		defaultBarrelModel[2].setRotationPoint(88F, -10F, -3.5F);


		defaultScopeModel = new ModelRendererTurbo[6];
		defaultScopeModel[0] = new ModelRendererTurbo(this, 48, 5, textureX, textureY); // battery
		defaultScopeModel[1] = new ModelRendererTurbo(this, 48, 25, textureX, textureY); // ironSight1
		defaultScopeModel[2] = new ModelRendererTurbo(this, 48, 25, textureX, textureY); // ironSight1-2
		defaultScopeModel[3] = new ModelRendererTurbo(this, 57, 30, textureX, textureY); // ironSight2
		defaultScopeModel[4] = new ModelRendererTurbo(this, 48, 34, textureX, textureY); // ironSight3
		defaultScopeModel[5] = new ModelRendererTurbo(this, 1, 28, textureX, textureY); // ironSight4

		defaultScopeModel[0].addShapeBox(0F, 0F, 0F, 6, 3, 1, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // battery
		defaultScopeModel[0].setRotationPoint(28F, -20F, -5.5F);

		defaultScopeModel[1].addBox(0F, 0F, 0F, 3, 7, 1, 0F); // ironSight1
		defaultScopeModel[1].setRotationPoint(60.9F, -29.5F, 2F);

		defaultScopeModel[2].addBox(0F, 0F, 0F, 3, 7, 1, 0F); // ironSight1-2
		defaultScopeModel[2].setRotationPoint(60.9F, -29.5F, -3F);

		defaultScopeModel[3].addBox(0F, 0F, 0F, 2, 2, 1, 0F); // ironSight2
		defaultScopeModel[3].setRotationPoint(-4F, -27F, -0.5F);

		defaultScopeModel[4].addBox(0F, 0F, 0F, 3, 4, 6, 0F); // ironSight3
		defaultScopeModel[4].setRotationPoint(-4F, -25.5F, -3F);

		defaultScopeModel[5].addBox(0F, 0F, 0F, 9, 1, 5, 0F); // ironSight4
		defaultScopeModel[5].setRotationPoint(28F, -25F, -2.5F);


		defaultStockModel = new ModelRendererTurbo[14];
		defaultStockModel[0] = new ModelRendererTurbo(this, 72, 191, textureX, textureY); // stock2
		defaultStockModel[1] = new ModelRendererTurbo(this, 72, 200, textureX, textureY); // stock3
		defaultStockModel[2] = new ModelRendererTurbo(this, 72, 160, textureX, textureY); // stockPipe1
		defaultStockModel[3] = new ModelRendererTurbo(this, 72, 170, textureX, textureY); // stockPipe2
		defaultStockModel[4] = new ModelRendererTurbo(this, 143, 170, textureX, textureY); // Box 68
		defaultStockModel[5] = new ModelRendererTurbo(this, 143, 179, textureX, textureY); // Box 69
		defaultStockModel[6] = new ModelRendererTurbo(this, 143, 161, textureX, textureY); // Box 70
		defaultStockModel[7] = new ModelRendererTurbo(this, 72, 181, textureX, textureY); // Box 71
		defaultStockModel[8] = new ModelRendererTurbo(this, 170, 188, textureX, textureY); // Box 72
		defaultStockModel[9] = new ModelRendererTurbo(this, 143, 188, textureX, textureY); // Box 73
		defaultStockModel[10] = new ModelRendererTurbo(this, 103, 191, textureX, textureY); // Box 74
		defaultStockModel[11] = new ModelRendererTurbo(this, 72, 215, textureX, textureY); // Box 75
		defaultStockModel[12] = new ModelRendererTurbo(this, 91, 202, textureX, textureY); // Box 76
		defaultStockModel[13] = new ModelRendererTurbo(this, 91, 220, textureX, textureY); // Box 77

		defaultStockModel[0].addShapeBox(0F, 0F, 0F, 10, 3, 5, 0F, 0F, -9F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -9F, 0F, 0F, 9F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 9F, 0F); // stock2
		defaultStockModel[0].setRotationPoint(-44F, -6.5F, -2.5F);

		defaultStockModel[1].addBox(0F, 0F, 0F, 2, 7, 7, 0F); // stock3
		defaultStockModel[1].setRotationPoint(-50F, -14.5F, -3.5F);

		defaultStockModel[2].addShapeBox(0F, 0F, 0F, 28, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // stockPipe1
		defaultStockModel[2].setRotationPoint(-50F, -16.5F, -3.5F);

		defaultStockModel[3].addBox(0F, 0F, 0F, 26, 3, 7, 0F); // stockPipe2
		defaultStockModel[3].setRotationPoint(-48F, -14.5F, -3.5F);

		defaultStockModel[4].addBox(0F, 0F, 0F, 12, 2, 6, 0F); // Box 68
		defaultStockModel[4].setRotationPoint(-22F, -14F, -3F);

		defaultStockModel[5].addShapeBox(0F, 0F, 0F, 12, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 69
		defaultStockModel[5].setRotationPoint(-22F, -12F, -3F);

		defaultStockModel[6].addShapeBox(0F, 0F, 0F, 12, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 70
		defaultStockModel[6].setRotationPoint(-22F, -16F, -3F);

		defaultStockModel[7].addBox(0F, 0F, 0F, 26, 4, 5, 0F); // Box 71
		defaultStockModel[7].setRotationPoint(-48F, -11.5F, -2.5F);

		defaultStockModel[8].addShapeBox(0F, 0F, 0F, 1, 4, 5, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 72
		defaultStockModel[8].setRotationPoint(-34F, -7.5F, -2.5F);

		defaultStockModel[9].addBox(0F, 0F, 0F, 8, 4, 5, 0F); // Box 73
		defaultStockModel[9].setRotationPoint(-33F, -7.5F, -2.5F);

		defaultStockModel[10].addBox(0F, 0F, 0F, 2, 3, 5, 0F); // Box 74
		defaultStockModel[10].setRotationPoint(-46F, 2.5F, -2.5F);

		defaultStockModel[11].addBox(0F, 0F, 0F, 2, 7, 7, 0F); // Box 75
		defaultStockModel[11].setRotationPoint(-48F, -2.5F, -3.5F);

		defaultStockModel[12].addShapeBox(0F, 0F, 0F, 2, 5, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F); // Box 76
		defaultStockModel[12].setRotationPoint(-50F, -7.5F, -3.5F);

		defaultStockModel[13].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F); // Box 77
		defaultStockModel[13].setRotationPoint(-48F, 4.5F, -3.5F);


		ammoModel = new ModelRendererTurbo[5];
		ammoModel[0] = new ModelRendererTurbo(this, 22, 147, textureX, textureY); // bullet
		ammoModel[1] = new ModelRendererTurbo(this, 1, 147, textureX, textureY); // bulletTip
		ammoModel[2] = new ModelRendererTurbo(this, 1, 132, textureX, textureY); // clip1
		ammoModel[3] = new ModelRendererTurbo(this, 1, 111, textureX, textureY); // clip2
		ammoModel[4] = new ModelRendererTurbo(this, 1, 90, textureX, textureY); // clip3

		ammoModel[0].addShapeBox(0F, 0F, 0F, 8, 1, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // bullet
		ammoModel[0].setRotationPoint(12F, -8F, -2F);

		ammoModel[1].addShapeBox(0F, 0F, 0F, 3, 1, 4, 0F, 0F, 0F, -1F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // bulletTip
		ammoModel[1].setRotationPoint(20F, -8F, -2F);

		ammoModel[2].addBox(0F, 0F, 0F, 13, 6, 8, 0F); // clip1
		ammoModel[2].setRotationPoint(11F, -7F, -4F);

		ammoModel[3].addShapeBox(0F, 0F, 0F, 13, 12, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 1F, -3F, 0F, 1F, -3F, 0F, -1F, 0F, 0F); // clip2
		ammoModel[3].setRotationPoint(11F, -1F, -4F);

		ammoModel[4].addShapeBox(0F, 0F, 0F, 13, 12, 8, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, 4F, -4F, 0F, 4F, -4F, 0F, -4F, 0F, 0F); // clip3
		ammoModel[4].setRotationPoint(12F, 11F, -4F);

		slideModel = new ModelRendererTurbo[1];
		slideModel[0] = new ModelRendererTurbo(this, 48, 10, textureX, textureY); // bolt1

		slideModel[0].addBox(0F, 0F, 0F, 1, 2, 3, 0F); // bolt1
		slideModel[0].setRotationPoint(24F, -19F, -7F);

		barrelAttachPoint = new Vector3f(88F /16F, 11.5F /16F, 0F /16F);
		stockAttachPoint = new Vector3f(-10F /16F, 12F /16F, 0F /16F);
		scopeAttachPoint = new Vector3f(9F /16F, 22F /16F, 0F /16F);
		gripAttachPoint = new Vector3f(42 /16F, 7F /16F, 0F /16F);

		gunSlideDistance = 0.5F;
		animationType = EnumAnimationType.BOTTOM_CLIP;


		translateAll(0F, 0F, 0F);


		flipAll();
	}
}