package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelStumbreonSteamer extends ModelGun
{
	int textureX = 512;
	int textureY = 256;

	public ModelStumbreonSteamer()
	{
		gunModel = new ModelRendererTurbo[66];
		gunModel[0] = new ModelRendererTurbo(this, 160, 1, textureX, textureY); // barrel
		gunModel[1] = new ModelRendererTurbo(this, 160, 1, textureX, textureY); // barrel
		gunModel[2] = new ModelRendererTurbo(this, 160, 10, textureX, textureY); // barrel2
		gunModel[3] = new ModelRendererTurbo(this, 226, 12, textureX, textureY); // barrelWasher1
		gunModel[4] = new ModelRendererTurbo(this, 226, 12, textureX, textureY); // barrelWasher1
		gunModel[5] = new ModelRendererTurbo(this, 205, 8, textureX, textureY); // barrelWasher2
		gunModel[6] = new ModelRendererTurbo(this, 205, 8, textureX, textureY); // barrelWasher2
		gunModel[7] = new ModelRendererTurbo(this, 205, 8, textureX, textureY); // barrelWasher2
		gunModel[8] = new ModelRendererTurbo(this, 160, 45, textureX, textureY); // body1
		gunModel[9] = new ModelRendererTurbo(this, 211, 45, textureX, textureY); // body2
		gunModel[10] = new ModelRendererTurbo(this, 211, 45, textureX, textureY); // body2
		gunModel[11] = new ModelRendererTurbo(this, 160, 34, textureX, textureY); // body3
		gunModel[12] = new ModelRendererTurbo(this, 160, 34, textureX, textureY); // body3
		gunModel[13] = new ModelRendererTurbo(this, 160, 19, textureX, textureY); // body4
		gunModel[14] = new ModelRendererTurbo(this, 214, 69, textureX, textureY); // body5
		gunModel[15] = new ModelRendererTurbo(this, 233, 60, textureX, textureY); // body6
		gunModel[16] = new ModelRendererTurbo(this, 187, 68, textureX, textureY); // body7
		gunModel[17] = new ModelRendererTurbo(this, 160, 63, textureX, textureY); // body8
		gunModel[18] = new ModelRendererTurbo(this, 233, 69, textureX, textureY); // bodyStrap1
		gunModel[19] = new ModelRendererTurbo(this, 233, 69, textureX, textureY); // bodyStrap1
		gunModel[20] = new ModelRendererTurbo(this, 258, 65, textureX, textureY); // bodyStrap2
		gunModel[21] = new ModelRendererTurbo(this, 199, 81, textureX, textureY); // canisterConnector1
		gunModel[22] = new ModelRendererTurbo(this, 199, 81, textureX, textureY); // canisterConnector1
		gunModel[23] = new ModelRendererTurbo(this, 199, 85, textureX, textureY); // canisterConnector2
		gunModel[24] = new ModelRendererTurbo(this, 258, 86, textureX, textureY); // cylinderBody1
		gunModel[25] = new ModelRendererTurbo(this, 258, 86, textureX, textureY); // cylinderBody1
		gunModel[26] = new ModelRendererTurbo(this, 209, 82, textureX, textureY); // cylinderBody2
		gunModel[27] = new ModelRendererTurbo(this, 209, 82, textureX, textureY); // cylinderBody2
		gunModel[28] = new ModelRendererTurbo(this, 160, 80, textureX, textureY); // cylinderBody3
		gunModel[29] = new ModelRendererTurbo(this, 72, 1, textureX, textureY); // gearPart1
		gunModel[30] = new ModelRendererTurbo(this, 72, 5, textureX, textureY); // gearPart2
		gunModel[31] = new ModelRendererTurbo(this, 72, 1, textureX, textureY); // gearPart1
		gunModel[32] = new ModelRendererTurbo(this, 87, 1, textureX, textureY); // gearPart4
		gunModel[33] = new ModelRendererTurbo(this, 87, 9, textureX, textureY); // gearPart5
		gunModel[34] = new ModelRendererTurbo(this, 87, 5, textureX, textureY); // gearPart6
		gunModel[35] = new ModelRendererTurbo(this, 87, 9, textureX, textureY); // gearPart5
		gunModel[36] = new ModelRendererTurbo(this, 87, 1, textureX, textureY); // gearPart4
		gunModel[37] = new ModelRendererTurbo(this, 72, 9, textureX, textureY); // gearStub1
		gunModel[38] = new ModelRendererTurbo(this, 72, 9, textureX, textureY); // gearStub2
		gunModel[39] = new ModelRendererTurbo(this, 79, 9, textureX, textureY); // gearStub3
		gunModel[40] = new ModelRendererTurbo(this, 79, 9, textureX, textureY); // gearStub4
		gunModel[41] = new ModelRendererTurbo(this, 72, 9, textureX, textureY); // gearStub5
		gunModel[42] = new ModelRendererTurbo(this, 72, 9, textureX, textureY); // gearStub6
		gunModel[43] = new ModelRendererTurbo(this, 72, 9, textureX, textureY); // gearStub7
		gunModel[44] = new ModelRendererTurbo(this, 72, 9, textureX, textureY); // gearStub8
		gunModel[45] = new ModelRendererTurbo(this, 70, 27, textureX, textureY); // grip1
		gunModel[46] = new ModelRendererTurbo(this, 99, 35, textureX, textureY); // grip2
		gunModel[47] = new ModelRendererTurbo(this, 99, 23, textureX, textureY); // grip3
		gunModel[48] = new ModelRendererTurbo(this, 99, 23, textureX, textureY); // grip3
		gunModel[49] = new ModelRendererTurbo(this, 114, 38, textureX, textureY); // grip3
		gunModel[50] = new ModelRendererTurbo(this, 99, 23, textureX, textureY); // grip4
		gunModel[51] = new ModelRendererTurbo(this, 10, 7, textureX, textureY); // ironSightBack
		gunModel[52] = new ModelRendererTurbo(this, 10, 1, textureX, textureY); // ironSightBack2
		gunModel[53] = new ModelRendererTurbo(this, 160, 102, textureX, textureY); // pumpPipe1
		gunModel[54] = new ModelRendererTurbo(this, 160, 102, textureX, textureY); // pumpPipe1
		gunModel[55] = new ModelRendererTurbo(this, 160, 95, textureX, textureY); // pumpPipe2
		gunModel[56] = new ModelRendererTurbo(this, 171, 111, textureX, textureY); // pumpPipe3
		gunModel[57] = new ModelRendererTurbo(this, 188, 111, textureX, textureY); // pumpPipe4
		gunModel[58] = new ModelRendererTurbo(this, 188, 111, textureX, textureY); // pumpPipe4
		gunModel[59] = new ModelRendererTurbo(this, 188, 115, textureX, textureY); // pumpPipe5
		gunModel[60] = new ModelRendererTurbo(this, 160, 108, textureX, textureY); // pumpPipe6
		gunModel[61] = new ModelRendererTurbo(this, 27, 9, textureX, textureY); // rail1
		gunModel[62] = new ModelRendererTurbo(this, 27, 1, textureX, textureY); // rail2
		gunModel[63] = new ModelRendererTurbo(this, 27, 15, textureX, textureY); // rail3
		gunModel[64] = new ModelRendererTurbo(this, 27, 15, textureX, textureY); // rail3-2
		gunModel[65] = new ModelRendererTurbo(this, 112, 8, textureX, textureY); // valve

		gunModel[0].addShapeBox(0F, 0F, 0F, 16, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // barrel
		gunModel[0].setRotationPoint(72F, -18F, -3F);

		gunModel[1].addShapeBox(0F, 0F, 0F, 16, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // barrel
		gunModel[1].setRotationPoint(72F, -22F, -3F);

		gunModel[2].addBox(0F, 0F, 0F, 16, 2, 6, 0F); // barrel2
		gunModel[2].setRotationPoint(72F, -20F, -3F);

		gunModel[3].addShapeBox(0F, 0F, 0F, 2, 1, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F); // barrelWasher1
		gunModel[3].setRotationPoint(70F, -16F, -2.5F);

		gunModel[4].addShapeBox(0F, 0F, 0F, 2, 1, 5, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // barrelWasher1
		gunModel[4].setRotationPoint(70F, -23F, -2.5F);

		gunModel[5].addShapeBox(0F, 0F, 0F, 2, 2, 8, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // barrelWasher2
		gunModel[5].setRotationPoint(70F, -22F, -4F);

		gunModel[6].addBox(0F, 0F, 0F, 2, 2, 8, 0F); // barrelWasher2
		gunModel[6].setRotationPoint(70F, -20F, -4F);

		gunModel[7].addShapeBox(0F, 0F, 0F, 2, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F); // barrelWasher2
		gunModel[7].setRotationPoint(70F, -18F, -4F);

		gunModel[8].addBox(0F, 0F, 0F, 17, 9, 8, 0F); // body1
		gunModel[8].setRotationPoint(-6F, -21F, -4F);

		gunModel[9].addShapeBox(0F, 0F, 0F, 17, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // body2
		gunModel[9].setRotationPoint(-6F, -12F, -4F);

		gunModel[10].addShapeBox(0F, 0F, 0F, 17, 2, 8, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body2
		gunModel[10].setRotationPoint(-6F, -23F, -4F);

		gunModel[11].addShapeBox(0F, 0F, 0F, 45, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // body3
		gunModel[11].setRotationPoint(25F, -15F, -4F);

		gunModel[12].addShapeBox(0F, 0F, 0F, 45, 2, 8, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body3
		gunModel[12].setRotationPoint(25F, -23F, -4F);

		gunModel[13].addBox(0F, 0F, 0F, 45, 6, 8, 0F); // body4
		gunModel[13].setRotationPoint(25F, -21F, -4F);

		gunModel[14].addBox(0F, -2F, 0F, 4, 5, 5, 0F); // body5
		gunModel[14].setRotationPoint(65.8F, -10F, -2.5F);

		gunModel[15].addShapeBox(0F, 0F, 0F, 4, 1, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // body6
		gunModel[15].setRotationPoint(65.8F, -13F, -3.5F);

		gunModel[16].addShapeBox(0F, 0F, 0F, 4, 2, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // body7
		gunModel[16].setRotationPoint(65.8F, -15F, -4.5F);

		gunModel[17].addBox(0F, 0F, 0F, 4, 7, 9, 0F); // body8
		gunModel[17].setRotationPoint(65.8F, -22F, -4.5F);

		gunModel[18].addShapeBox(0F, 0F, 0F, 4, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.2F, -1F, 0F, 0.2F, -1F, 0F, 0.2F, -0.6F, 0F, 0.2F, -0.6F); // bodyStrap1
		gunModel[18].setRotationPoint(43F, -15F, -4.2F);

		gunModel[19].addShapeBox(0F, 0F, 0F, 4, 2, 8, 0F, 0F, 0.2F, -1F, 0F, 0.2F, -1F, 0F, 0.2F, -0.6F, 0F, 0.2F, -0.6F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F); // bodyStrap1
		gunModel[19].setRotationPoint(43F, -23F, -4.2F);

		gunModel[20].addShapeBox(0F, 0F, 0F, 4, 6, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F); // bodyStrap2
		gunModel[20].setRotationPoint(43F, -21F, -4.2F);

		gunModel[21].addShapeBox(0F, 0F, 0F, 4, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // canisterConnector1
		gunModel[21].setRotationPoint(17F, -7F, 1F);

		gunModel[22].addShapeBox(0F, 0F, 0F, 4, 2, 1, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // canisterConnector1
		gunModel[22].setRotationPoint(17F, -7F, -2F);

		gunModel[23].addBox(0F, 0F, 0F, 4, 2, 2, 0F); // canisterConnector2
		gunModel[23].setRotationPoint(17F, -7F, -1F);

		gunModel[24].addShapeBox(0F, 0F, 0F, 14, 1, 7, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // cylinderBody1
		gunModel[24].setRotationPoint(11F, -23F, -3.5F);

		gunModel[25].addShapeBox(0F, 0F, 0F, 14, 1, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F); // cylinderBody1
		gunModel[25].setRotationPoint(11F, -14F, -3.5F);

		gunModel[26].addShapeBox(0F, 0F, 0F, 14, 2, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F); // cylinderBody2
		gunModel[26].setRotationPoint(11F, -16F, -5F);

		gunModel[27].addShapeBox(0F, 0F, 0F, 14, 2, 10, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // cylinderBody2
		gunModel[27].setRotationPoint(11F, -22F, -5F);

		gunModel[28].addBox(0F, 0F, 0F, 14, 4, 10, 0F); // cylinderBody3
		gunModel[28].setRotationPoint(11F, -20F, -5F);

		gunModel[29].addShapeBox(0F, 0F, 0F, 6, 2, 1, 0F, -1.75F, 0F, 0F, -1.75F, 0F, 0F, -1.75F, 0F, 0F, -1.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // gearPart1
		gunModel[29].setRotationPoint(-3F, -20.5F, -5.5F);

		gunModel[30].addShapeBox(0F, 0F, 0F, 6, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // gearPart2
		gunModel[30].setRotationPoint(-3F, -18.5F, -5.5F);

		gunModel[31].addShapeBox(0F, 0F, 0F, 6, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.75F, 0F, 0F, -1.75F, 0F, 0F, -1.75F, 0F, 0F, -1.75F, 0F, 0F); // gearPart1
		gunModel[31].setRotationPoint(-3F, -16.5F, -5.5F);

		gunModel[32].addShapeBox(0F, 0F, 0F, 6, 2, 1, 0F, -2F, -0.75F, 0F, -2F, -0.75F, 0F, -2F, -0.75F, 0F, -2F, -0.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // gearPart4
		gunModel[32].setRotationPoint(-3F, -22.5F, -5F);

		gunModel[33].addShapeBox(0F, 0F, 0F, 10, 2, 1, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // gearPart5
		gunModel[33].setRotationPoint(-5F, -20.5F, -5F);

		gunModel[34].addShapeBox(0F, 0F, 0F, 8, 2, 1, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F); // gearPart6
		gunModel[34].setRotationPoint(-4F, -18.5F, -5F);

		gunModel[35].addShapeBox(0F, 0F, 0F, 10, 2, 1, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F); // gearPart5
		gunModel[35].setRotationPoint(-5F, -16.5F, -5F);

		gunModel[36].addShapeBox(0F, 0F, 0F, 6, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, -0.75F, 0F, -2F, -0.75F, 0F, -2F, -0.75F, 0F, -2F, -0.75F, 0F); // gearPart4
		gunModel[36].setRotationPoint(-3F, -14.5F, -5F);

		gunModel[37].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // gearStub1
		gunModel[37].setRotationPoint(-1F, -22.7F, -5F);

		gunModel[38].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F); // gearStub2
		gunModel[38].setRotationPoint(-1F, -13.3F, -5F);

		gunModel[39].addShapeBox(0F, 0F, 0F, 1, 2, 1, 0F, 0.25F, -0.3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.25F, -0.3F, 0F, 0.25F, -0.3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.25F, -0.3F, 0F); // gearStub3
		gunModel[39].setRotationPoint(-5.5F, -18.7F, -5F);

		gunModel[40].addShapeBox(0F, 0F, 0F, 1, 2, 1, 0F, 0F, 0F, 0F, 0.25F, -0.3F, 0F, 0.25F, -0.3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.25F, -0.3F, 0F, 0.25F, -0.3F, 0F, 0F, 0F, 0F); // gearStub4
		gunModel[40].setRotationPoint(4.5F, -18.7F, -5F);

		gunModel[41].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // gearStub5
		gunModel[41].setRotationPoint(-4F, -20.7F, -5F);
		gunModel[41].rotateAngleZ = 0.6981317F;

		gunModel[42].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F); // gearStub6
		gunModel[42].setRotationPoint(2.2F, -14F, -5F);
		gunModel[42].rotateAngleZ = 0.78539816F;

		gunModel[43].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // gearStub7
		gunModel[43].setRotationPoint(3F, -21.8F, -5F);
		gunModel[43].rotateAngleZ = -0.78539816F;

		gunModel[44].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F); // gearStub8
		gunModel[44].setRotationPoint(-3.7F, -15.5F, -5F);
		gunModel[44].rotateAngleZ = -0.78539816F;

		gunModel[45].addBox(1F, 3F, 0F, 8, 16, 6, 0F); // grip1
		gunModel[45].setRotationPoint(-4F, -15F, -3F);
		gunModel[45].rotateAngleZ = -0.20943951F;

		gunModel[46].addShapeBox(8F, 6F, 0F, 1, 8, 6, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -1F, 0F, 0F); // grip2
		gunModel[46].setRotationPoint(-4F, -15F, -3F);
		gunModel[46].rotateAngleZ = -0.20943951F;

		gunModel[47].addShapeBox(1F, 14F, 0F, 1, 5, 6, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F); // grip3
		gunModel[47].setRotationPoint(-4F, -15F, -3F);
		gunModel[47].rotateAngleZ = -0.20943951F;

		gunModel[48].addShapeBox(1F, 9F, 0F, 1, 5, 6, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F); // grip3
		gunModel[48].setRotationPoint(-4F, -15F, -3F);
		gunModel[48].rotateAngleZ = -0.20943951F;

		gunModel[49].addBox(9F, 14F, 0F, 2, 5, 6, 0F); // grip3
		gunModel[49].setRotationPoint(-4F, -15F, -3F);
		gunModel[49].rotateAngleZ = -0.20943951F;

		gunModel[50].addShapeBox(1F, 3F, 0F, 1, 5, 6, 0F, 2F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F); // grip4
		gunModel[50].setRotationPoint(-4F, -15F, -3F);
		gunModel[50].rotateAngleZ = -0.20943951F;

		gunModel[51].addBox(0F, 0F, 0F, 5, 1, 3, 0F); // ironSightBack
		gunModel[51].setRotationPoint(5F, -24F, -1.5F);

		gunModel[52].addShapeBox(0F, 0F, 0F, 3, 2, 3, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 1F, 0F, 0F); // ironSightBack2
		gunModel[52].setRotationPoint(7F, -26F, -2.5F);

		gunModel[53].addShapeBox(0F, 0F, 0F, 45, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // pumpPipe1
		gunModel[53].setRotationPoint(21F, -8F, -2F);

		gunModel[54].addShapeBox(0F, 0F, 0F, 45, 1, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // pumpPipe1
		gunModel[54].setRotationPoint(21F, -11F, -2F);

		gunModel[55].addBox(0F, 0F, 0F, 45, 2, 4, 0F); // pumpPipe2
		gunModel[55].setRotationPoint(21F, -10F, -2F);

		gunModel[56].addBox(0F, 0F, 0F, 4, 4, 4, 0F); // pumpPipe3
		gunModel[56].setRotationPoint(17F, -11F, -2F);

		gunModel[57].addShapeBox(0F, 0F, 0F, 4, 2, 1, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // pumpPipe4
		gunModel[57].setRotationPoint(17F, -13F, -2F);

		gunModel[58].addShapeBox(0F, 0F, 0F, 4, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // pumpPipe4
		gunModel[58].setRotationPoint(17F, -13F, 1F);

		gunModel[59].addBox(0F, 0F, 0F, 4, 2, 2, 0F); // pumpPipe5
		gunModel[59].setRotationPoint(17F, -13F, -1F);

		gunModel[60].addBox(0F, 0F, 0F, 2, 8, 3, 0F); // pumpPipe6
		gunModel[60].setRotationPoint(12F, -13F, -1.5F);
		gunModel[60].rotateAngleZ = 0.78539816F;

		gunModel[61].addBox(0F, 0F, 0F, 16, 1, 4, 0F); // rail1
		gunModel[61].setRotationPoint(26F, -24F, -2F);

		gunModel[62].addBox(0F, 0F, 0F, 16, 1, 6, 0F); // rail2
		gunModel[62].setRotationPoint(26F, -25F, -3F);

		gunModel[63].addShapeBox(0F, 0F, 0F, 16, 1, 1, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // rail3
		gunModel[63].setRotationPoint(26F, -26F, 2F);

		gunModel[64].addShapeBox(0F, 0F, 0F, 16, 1, 1, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // rail3-2
		gunModel[64].setRotationPoint(26F, -26F, -3F);

		gunModel[65].addBox(0F, 0F, 0F, 2, 2, 2, 0F); // valve
		gunModel[65].setRotationPoint(18F, -10F, 2F);


		defaultScopeModel = new ModelRendererTurbo[7];
		defaultScopeModel[0] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // ironSight
		defaultScopeModel[1] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // ironSight
		defaultScopeModel[2] = new ModelRendererTurbo(this, 1, 10, textureX, textureY); // ironSight2
		defaultScopeModel[3] = new ModelRendererTurbo(this, 1, 10, textureX, textureY); // ironSight2-2
		defaultScopeModel[4] = new ModelRendererTurbo(this, 12, 17, textureX, textureY); // ironSight3-1
		defaultScopeModel[5] = new ModelRendererTurbo(this, 12, 17, textureX, textureY); // ironSight3-2
		defaultScopeModel[6] = new ModelRendererTurbo(this, 1, 16, textureX, textureY); // ironSight4

		defaultScopeModel[0].addBox(0F, 0F, 0F, 3, 7, 1, 0F); // ironSight
		defaultScopeModel[0].setRotationPoint(66.8F, -29F, -4.5F);

		defaultScopeModel[1].addBox(0F, 0F, 0F, 3, 7, 1, 0F); // ironSight
		defaultScopeModel[1].setRotationPoint(66.8F, -29F, 3.5F);

		defaultScopeModel[2].addShapeBox(0F, 0F, 0F, 1, 1, 4, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // ironSight2
		defaultScopeModel[2].setRotationPoint(68F, -25F, -2F);

		defaultScopeModel[3].addShapeBox(0F, 0F, 0F, 1, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F); // ironSight2-2
		defaultScopeModel[3].setRotationPoint(68F, -30F, -2F);

		defaultScopeModel[4].addBox(0F, 0F, 0F, 1, 4, 1, 0F); // ironSight3-1
		defaultScopeModel[4].setRotationPoint(68F, -29F, -3F);

		defaultScopeModel[5].addBox(0F, 0F, 0F, 1, 4, 1, 0F); // ironSight3-2
		defaultScopeModel[5].setRotationPoint(68F, -29F, 2F);

		defaultScopeModel[6].addBox(0F, 0F, 0F, 1, 2, 4, 0F); // ironSight4
		defaultScopeModel[6].setRotationPoint(68F, -24F, -2F);


		defaultStockModel = new ModelRendererTurbo[31];
		defaultStockModel[0] = new ModelRendererTurbo(this, 1, 130, textureX, textureY); // stock1
		defaultStockModel[1] = new ModelRendererTurbo(this, 1, 173, textureX, textureY); // stock2
		defaultStockModel[2] = new ModelRendererTurbo(this, 1, 162, textureX, textureY); // stock3
		defaultStockModel[3] = new ModelRendererTurbo(this, 1, 148, textureX, textureY); // stock4
		defaultStockModel[4] = new ModelRendererTurbo(this, 1, 50, textureX, textureY); // stockCanister1
		defaultStockModel[5] = new ModelRendererTurbo(this, 28, 50, textureX, textureY); // stockCanister1
		defaultStockModel[6] = new ModelRendererTurbo(this, 55, 50, textureX, textureY); // stockCanister2
		defaultStockModel[7] = new ModelRendererTurbo(this, 30, 105, textureX, textureY); // stockCanister3
		defaultStockModel[8] = new ModelRendererTurbo(this, 30, 105, textureX, textureY); // stockCanister3
		defaultStockModel[9] = new ModelRendererTurbo(this, 1, 105, textureX, textureY); // stockCanister4
		defaultStockModel[10] = new ModelRendererTurbo(this, 30, 92, textureX, textureY); // stockCanister5
		defaultStockModel[11] = new ModelRendererTurbo(this, 30, 92, textureX, textureY); // stockCanister5
		defaultStockModel[12] = new ModelRendererTurbo(this, 1, 92, textureX, textureY); // stockCanister6
		defaultStockModel[13] = new ModelRendererTurbo(this, 57, 108, textureX, textureY); // stockCanister7
		defaultStockModel[14] = new ModelRendererTurbo(this, 57, 108, textureX, textureY); // stockCanister7
		defaultStockModel[15] = new ModelRendererTurbo(this, 57, 108, textureX, textureY); // stockCanister7
		defaultStockModel[16] = new ModelRendererTurbo(this, 1, 124, textureX, textureY); // stockPipe1
		defaultStockModel[17] = new ModelRendererTurbo(this, 1, 124, textureX, textureY); // stockPipe1
		defaultStockModel[18] = new ModelRendererTurbo(this, 1, 117, textureX, textureY); // stockPipe2
		defaultStockModel[19] = new ModelRendererTurbo(this, 74, 124, textureX, textureY); // stockPipe3
		defaultStockModel[20] = new ModelRendererTurbo(this, 74, 124, textureX, textureY); // stockPipe3
		defaultStockModel[21] = new ModelRendererTurbo(this, 74, 117, textureX, textureY); // stockPipe4
		defaultStockModel[22] = new ModelRendererTurbo(this, 43, 171, textureX, textureY); // stockStrap1
		defaultStockModel[23] = new ModelRendererTurbo(this, 32, 166, textureX, textureY); // stockStrap2
		defaultStockModel[24] = new ModelRendererTurbo(this, 32, 166, textureX, textureY); // stockStrap2
		defaultStockModel[25] = new ModelRendererTurbo(this, 32, 148, textureX, textureY); // stockStrap3
		defaultStockModel[26] = new ModelRendererTurbo(this, 32, 130, textureX, textureY); // stockStrap4
		defaultStockModel[27] = new ModelRendererTurbo(this, 112, 4, textureX, textureY); // turningValve1
		defaultStockModel[28] = new ModelRendererTurbo(this, 121, 9, textureX, textureY); // turningValve2
		defaultStockModel[29] = new ModelRendererTurbo(this, 121, 9, textureX, textureY); // turningValve2
		defaultStockModel[30] = new ModelRendererTurbo(this, 112, 8, textureX, textureY); // valve

		defaultStockModel[0].addBox(0F, 0F, 0F, 3, 9, 8, 0F); // stock1
		defaultStockModel[0].setRotationPoint(-40.5F, -21F, -4F);

		defaultStockModel[1].addShapeBox(0F, 0F, 0F, 3, 2, 8, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // stock2
		defaultStockModel[1].setRotationPoint(-40.5F, -23F, -4F);

		defaultStockModel[2].addShapeBox(0F, 0F, 0F, 5, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // stock3
		defaultStockModel[2].setRotationPoint(-40.5F, -7F, -4F);

		defaultStockModel[3].addShapeBox(0F, 0F, 0F, 3, 5, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F); // stock4
		defaultStockModel[3].setRotationPoint(-40.5F, -12F, -4F);

		defaultStockModel[4].addShapeBox(-5F, 3F, 0F, 3, 14, 10, 0F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F); // stockCanister1
		defaultStockModel[4].setRotationPoint(-19F, -14.5F, -5F);
		defaultStockModel[4].rotateAngleZ = -1.22173048F;

		defaultStockModel[5].addShapeBox(2F, 3F, 0F, 3, 14, 10, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F); // stockCanister1
		defaultStockModel[5].setRotationPoint(-19F, -14.5F, -5F);
		defaultStockModel[5].rotateAngleZ = -1.22173048F;

		defaultStockModel[6].addBox(-2F, 3F, 0F, 4, 14, 10, 0F); // stockCanister2
		defaultStockModel[6].setRotationPoint(-19F, -14.5F, -5F);
		defaultStockModel[6].rotateAngleZ = -1.22173048F;

		defaultStockModel[7].addShapeBox(-5F, 17F, 0F, 3, 1, 10, 0F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, -1F, 0F, -3F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, -1F, 0F, -3F); // stockCanister3
		defaultStockModel[7].setRotationPoint(-19F, -14.5F, -5F);
		defaultStockModel[7].rotateAngleZ = -1.22173048F;

		defaultStockModel[8].addShapeBox(2F, 17F, 0F, 3, 1, 10, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, -1.5F, -1F, 0F, -3F, -1F, 0F, -3F, 0F, 0F, -1.5F); // stockCanister3
		defaultStockModel[8].setRotationPoint(-19F, -14.5F, -5F);
		defaultStockModel[8].rotateAngleZ = -1.22173048F;

		defaultStockModel[9].addShapeBox(-2F, 17F, 0F, 4, 1, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F); // stockCanister4
		defaultStockModel[9].setRotationPoint(-19F, -14.5F, -5F);
		defaultStockModel[9].rotateAngleZ = -1.22173048F;

		defaultStockModel[10].addShapeBox(-5F, 1F, 0F, 3, 2, 10, 0F, -1F, 0F, -3F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, -1F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F); // stockCanister5
		defaultStockModel[10].setRotationPoint(-19F, -14.5F, -5F);
		defaultStockModel[10].rotateAngleZ = -1.22173048F;

		defaultStockModel[11].addShapeBox(2F, 1F, 0F, 3, 2, 10, 0F, 0F, 0F, -1.5F, -1F, 0F, -3F, -1F, 0F, -3F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F); // stockCanister5
		defaultStockModel[11].setRotationPoint(-19F, -14.5F, -5F);
		defaultStockModel[11].rotateAngleZ = -1.22173048F;

		defaultStockModel[12].addShapeBox(-2F, 1F, 0F, 4, 2, 10, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // stockCanister6
		defaultStockModel[12].setRotationPoint(-19F, -14.5F, -5F);
		defaultStockModel[12].rotateAngleZ = -1.22173048F;

		defaultStockModel[13].addShapeBox(1F, 0F, 0F, 2, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F); // stockCanister7
		defaultStockModel[13].setRotationPoint(-18F, -15F, -3F);
		defaultStockModel[13].rotateAngleZ = -1.22173048F;

		defaultStockModel[14].addShapeBox(-3F, 0F, 0F, 2, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F); // stockCanister7
		defaultStockModel[14].setRotationPoint(-18F, -15F, -3F);
		defaultStockModel[14].rotateAngleZ = -1.22173048F;

		defaultStockModel[15].addBox(-1F, 0F, 0F, 2, 2, 6, 0F); // stockCanister7
		defaultStockModel[15].setRotationPoint(-18F, -15F, -3F);
		defaultStockModel[15].rotateAngleZ = -1.22173048F;

		defaultStockModel[16].addShapeBox(0F, 0F, 0F, 32, 1, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // stockPipe1
		defaultStockModel[16].setRotationPoint(-38F, -22F, -2F);

		defaultStockModel[17].addShapeBox(0F, 0F, 0F, 32, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // stockPipe1
		defaultStockModel[17].setRotationPoint(-38F, -19F, -2F);

		defaultStockModel[18].addBox(0F, 0F, 0F, 32, 2, 4, 0F); // stockPipe2
		defaultStockModel[18].setRotationPoint(-38F, -21F, -2F);

		defaultStockModel[19].addShapeBox(0F, 0F, 0F, 13, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // stockPipe3
		defaultStockModel[19].setRotationPoint(-19F, -14F, -2F);

		defaultStockModel[20].addShapeBox(0F, 0F, 0F, 13, 1, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // stockPipe3
		defaultStockModel[20].setRotationPoint(-19F, -17F, -2F);

		defaultStockModel[21].addBox(0F, 0F, 0F, 13, 2, 4, 0F); // stockPipe4
		defaultStockModel[21].setRotationPoint(-19F, -16F, -2F);

		defaultStockModel[22].addShapeBox(0F, 0F, 0F, 4, 1, 5, 0F, 0F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // stockStrap1
		defaultStockModel[22].setRotationPoint(-35F, -22.5F, -2.5F);

		defaultStockModel[23].addShapeBox(0F, 0F, 0F, 4, 10, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, -0.5F, -3F, 4F, -2F, -3F, 4F, -2F, 3F, -4F, -0.5F, 3F); // stockStrap2
		defaultStockModel[23].setRotationPoint(-35F, -21.5F, 1.5F);

		defaultStockModel[24].addShapeBox(0F, 0F, 0F, 4, 10, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, -0.5F, 3F, 4F, -2F, 3F, 4F, -2F, -3F, -4F, -0.5F, -3F); // stockStrap2
		defaultStockModel[24].setRotationPoint(-35F, -21.5F, -2.5F);

		defaultStockModel[25].addShapeBox(0F, 0F, 0F, 4, 6, 11, 0F, 0F, -2F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -2F, 0F, -1.5F, -0.5F, 0F, 1.5F, -2F, 0F, 1.5F, -2F, 0F, -1.5F, -0.5F, 0F); // stockStrap3
		defaultStockModel[25].setRotationPoint(-31F, -14F, -5.5F);

		defaultStockModel[26].addShapeBox(0F, 0F, 0F, 4, 6, 11, 0F, 0F, -2F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -2F, 0F, -1.5F, -0.5F, -3.5F, 1.5F, -2F, -3.5F, 1.5F, -2F, -3.5F, -1.5F, -0.5F, -3.5F); // stockStrap4
		defaultStockModel[26].setRotationPoint(-29.5F, -10.5F, -5.5F);

		defaultStockModel[27].addBox(0F, 0F, 0F, 6, 2, 1, 0F); // turningValve1
		defaultStockModel[27].setRotationPoint(-14F, -16F, -4F);

		defaultStockModel[28].addShapeBox(0F, 0F, 0F, 6, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F); // turningValve2
		defaultStockModel[28].setRotationPoint(-14F, -14F, -4F);

		defaultStockModel[29].addShapeBox(0F, 0F, 0F, 6, 2, 1, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // turningValve2
		defaultStockModel[29].setRotationPoint(-14F, -18F, -4F);

		defaultStockModel[30].addBox(0F, 0F, 0F, 2, 2, 2, 0F); // valve
		defaultStockModel[30].setRotationPoint(-12F, -16F, -3F);


		defaultGripModel = new ModelRendererTurbo[10];
		defaultGripModel[0] = new ModelRendererTurbo(this, 53, 41, textureX, textureY); // canister1
		defaultGripModel[1] = new ModelRendererTurbo(this, 1, 32, textureX, textureY); // canister2
		defaultGripModel[2] = new ModelRendererTurbo(this, 1, 23, textureX, textureY); // canister3
		defaultGripModel[3] = new ModelRendererTurbo(this, 1, 41, textureX, textureY); // canister4
		defaultGripModel[4] = new ModelRendererTurbo(this, 34, 19, textureX, textureY); // meter1
		defaultGripModel[5] = new ModelRendererTurbo(this, 34, 29, textureX, textureY); // meter1-2
		defaultGripModel[6] = new ModelRendererTurbo(this, 34, 39, textureX, textureY); // meter2
		defaultGripModel[7] = new ModelRendererTurbo(this, 53, 23, textureX, textureY); // pipe1
		defaultGripModel[8] = new ModelRendererTurbo(this, 53, 28, textureX, textureY); // pipe2
		defaultGripModel[9] = new ModelRendererTurbo(this, 53, 35, textureX, textureY); // pipe3

		defaultGripModel[0].addBox(0F, 0F, 0F, 6, 6, 2, 0F); // canister1
		defaultGripModel[0].setRotationPoint(-1F, -21F, 3.5F);

		defaultGripModel[1].addShapeBox(0F, 0F, 0F, 10, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // canister2
		defaultGripModel[1].setRotationPoint(-2F, -17F, 4F);

		defaultGripModel[2].addBox(0F, 0F, 0F, 10, 2, 6, 0F); // canister3
		defaultGripModel[2].setRotationPoint(-2F, -19F, 4F);

		defaultGripModel[3].addShapeBox(0F, 0F, 0F, 10, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // canister4
		defaultGripModel[3].setRotationPoint(-2F, -21F, 4F);

		defaultGripModel[4].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // meter1
		defaultGripModel[4].setRotationPoint(0F, -23F, 4F);

		defaultGripModel[5].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // meter1-2
		defaultGripModel[5].setRotationPoint(0F, -28F, 4F);

		defaultGripModel[6].addBox(0F, 0F, 0F, 2, 3, 7, 0F); // meter2
		defaultGripModel[6].setRotationPoint(0F, -26F, 4F);

		defaultGripModel[7].addBox(0F, 0F, 0F, 5, 2, 2, 0F); // pipe1
		defaultGripModel[7].setRotationPoint(8F, -19F, 6F);

		defaultGripModel[8].addBox(0F, 0F, 0F, 2, 2, 4, 0F); // pipe2
		defaultGripModel[8].setRotationPoint(13F, -19F, 4F);

		defaultGripModel[9].addBox(0F, 0F, 0F, 4, 4, 1, 0F); // pipe3
		defaultGripModel[9].setRotationPoint(12F, -20F, 4.5F);


		ammoModel = new ModelRendererTurbo[15];
		ammoModel[0] = new ModelRendererTurbo(this, 117, 93, textureX, textureY); // gasCanister8
		ammoModel[1] = new ModelRendererTurbo(this, 38, 75, textureX, textureY); // canisterStrap1
		ammoModel[2] = new ModelRendererTurbo(this, 38, 75, textureX, textureY); // canisterStrap1
		ammoModel[3] = new ModelRendererTurbo(this, 1, 75, textureX, textureY); // canisterStrap2
		ammoModel[4] = new ModelRendererTurbo(this, 84, 50, textureX, textureY); // gasCanister1
		ammoModel[5] = new ModelRendererTurbo(this, 113, 50, textureX, textureY); // gasCanister2
		ammoModel[6] = new ModelRendererTurbo(this, 84, 50, textureX, textureY); // gasCanister3
		ammoModel[7] = new ModelRendererTurbo(this, 117, 80, textureX, textureY); // gasCanister6
		ammoModel[8] = new ModelRendererTurbo(this, 117, 80, textureX, textureY); // gasCanister6
		ammoModel[9] = new ModelRendererTurbo(this, 84, 80, textureX, textureY); // gasCanister7
		ammoModel[10] = new ModelRendererTurbo(this, 117, 93, textureX, textureY); // gasCanister8
		ammoModel[11] = new ModelRendererTurbo(this, 84, 93, textureX, textureY); // gasCanister9
		ammoModel[12] = new ModelRendererTurbo(this, 84, 107, textureX, textureY); // gasNozzle1
		ammoModel[13] = new ModelRendererTurbo(this, 101, 107, textureX, textureY); // gasNozzle1
		ammoModel[14] = new ModelRendererTurbo(this, 84, 107, textureX, textureY); // gasNozzle1

		ammoModel[0].addShapeBox(2.5F, 1F, 0F, 3, 2, 11, 0F, 0F, 0F, -2F, -2F, 0F, -3.5F, -2F, 0F, -3.5F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F); // gasCanister8
		ammoModel[0].setRotationPoint(19F, -6F, -5.5F);
		ammoModel[0].rotateAngleZ = 0.27925268F;

		ammoModel[1].addShapeBox(-5F, 10F, 0F, 3, 4, 12, 0F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F); // canisterStrap1
		ammoModel[1].setRotationPoint(18F, -6F, -6F);
		ammoModel[1].rotateAngleZ = 0.27925268F;

		ammoModel[2].addShapeBox(4F, 10F, 0F, 3, 4, 12, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F); // canisterStrap1
		ammoModel[2].setRotationPoint(18F, -6F, -6F);
		ammoModel[2].rotateAngleZ = 0.27925268F;

		ammoModel[3].addBox(-2F, 10F, 0F, 6, 4, 12, 0F); // canisterStrap2
		ammoModel[3].setRotationPoint(18F, -6F, -6F);
		ammoModel[3].rotateAngleZ = 0.27925268F;

		ammoModel[4].addShapeBox(-5.5F, 3F, 0F, 3, 18, 11, 0F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F); // gasCanister1
		ammoModel[4].setRotationPoint(19F, -6F, -5.5F);
		ammoModel[4].rotateAngleZ = 0.27925268F;

		ammoModel[5].addBox(-2.5F, 3F, 0F, 5, 18, 11, 0F); // gasCanister2
		ammoModel[5].setRotationPoint(19F, -6F, -5.5F);
		ammoModel[5].rotateAngleZ = 0.27925268F;

		ammoModel[6].addShapeBox(2.5F, 3F, 0F, 3, 18, 11, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F); // gasCanister3
		ammoModel[6].setRotationPoint(19F, -6F, -5.5F);
		ammoModel[6].rotateAngleZ = 0.27925268F;

		ammoModel[7].addShapeBox(2.5F, 21F, 0F, 3, 1, 11, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, -1.5F, -1F, 0F, -3F, -1F, 0F, -3F, 0F, 0F, -1.5F); // gasCanister6
		ammoModel[7].setRotationPoint(19F, -6F, -5.5F);
		ammoModel[7].rotateAngleZ = 0.27925268F;

		ammoModel[8].addShapeBox(-5.5F, 21F, 0F, 3, 1, 11, 0F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, -1F, 0F, -3F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, -1F, 0F, -3F); // gasCanister6
		ammoModel[8].setRotationPoint(19F, -6F, -5.5F);
		ammoModel[8].rotateAngleZ = 0.27925268F;

		ammoModel[9].addShapeBox(-2.5F, 21F, 0F, 5, 1, 11, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F); // gasCanister7
		ammoModel[9].setRotationPoint(19F, -6F, -5.5F);
		ammoModel[9].rotateAngleZ = 0.27925268F;

		ammoModel[10].addShapeBox(-5.5F, 1F, 0F, 3, 2, 11, 0F, -2F, 0F, -3.5F, 0F, 0F, -2F, 0F, 0F, -2F, -2F, 0F, -3.5F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F); // gasCanister8
		ammoModel[10].setRotationPoint(19F, -6F, -5.5F);
		ammoModel[10].rotateAngleZ = 0.27925268F;

		ammoModel[11].addShapeBox(-2.5F, 1F, 0F, 5, 2, 11, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // gasCanister9
		ammoModel[11].setRotationPoint(19F, -6F, -5.5F);
		ammoModel[11].rotateAngleZ = 0.27925268F;

		ammoModel[12].addShapeBox(-3.5F, 0F, 0F, 2, 1, 6, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F); // gasNozzle1
		ammoModel[12].setRotationPoint(19F, -6F, -3F);
		ammoModel[12].rotateAngleZ = 0.27925268F;

		ammoModel[13].addBox(-1.5F, 0F, 0F, 3, 1, 6, 0F); // gasNozzle1
		ammoModel[13].setRotationPoint(19F, -6F, -3F);
		ammoModel[13].rotateAngleZ = 0.27925268F;

		ammoModel[14].addShapeBox(1.5F, 0F, 0F, 2, 1, 6, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F); // gasNozzle1
		ammoModel[14].setRotationPoint(19F, -6F, -3F);
		ammoModel[14].rotateAngleZ = 0.27925268F;


		pumpModel = new ModelRendererTurbo[5];
		pumpModel[0] = new ModelRendererTurbo(this, 160, 120, textureX, textureY); // pump1
		pumpModel[1] = new ModelRendererTurbo(this, 160, 120, textureX, textureY); // pump1
		pumpModel[2] = new ModelRendererTurbo(this, 203, 120, textureX, textureY); // pump2
		pumpModel[3] = new ModelRendererTurbo(this, 246, 118, textureX, textureY); // pump3
		pumpModel[4] = new ModelRendererTurbo(this, 246, 118, textureX, textureY); // pump3

		pumpModel[0].addShapeBox(0F, 0F, 0F, 15, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // pump1
		pumpModel[0].setRotationPoint(48F, -12F, -3F);

		pumpModel[1].addBox(0F, 0F, 0F, 15, 2, 6, 0F); // pump1
		pumpModel[1].setRotationPoint(48F, -10F, -3F);

		pumpModel[2].addShapeBox(0F, 0F, 0F, 15, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // pump2
		pumpModel[2].setRotationPoint(48F, -8F, -3F);

		pumpModel[3].addShapeBox(0F, 0F, 0F, 2, 4, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // pump3
		pumpModel[3].setRotationPoint(48.2F, -8F, -3F);

		pumpModel[4].addShapeBox(0F, 0F, 0F, 2, 4, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, -2F, -2F, 0F, -2F, -2F, 0F, 0F, -2F); // pump3
		pumpModel[4].setRotationPoint(50.2F, -8F, -3F);


		breakActionModel = new ModelRendererTurbo[3];
		breakActionModel[0] = new ModelRendererTurbo(this, 112, 4, textureX, textureY); // Import turningValve1
		breakActionModel[1] = new ModelRendererTurbo(this, 121, 9, textureX, textureY); // Import turningValve2
		breakActionModel[2] = new ModelRendererTurbo(this, 121, 9, textureX, textureY); // Import turningValve2

		breakActionModel[0].addBox(-3F, -1F, 0F, 6, 2, 1, 0F); // Import turningValve1
		breakActionModel[0].setRotationPoint(19F, -9F, 4F);

		breakActionModel[1].addShapeBox(-3F, -3F, 0F, 6, 2, 1, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import turningValve2
		breakActionModel[1].setRotationPoint(19F, -9F, 4F);

		breakActionModel[2].addShapeBox(-3F, 1F, 0F, 6, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F); // Import turningValve2
		breakActionModel[2].setRotationPoint(19F, -9F, 4F);

		barrelBreakPoint = new Vector3f(19F / 16F, 9F / 16F, 0F);
		breakAngle = 45F;

		stockAttachPoint = new Vector3f(-6F /16F, 17F /16F, 0F /16F);
		scopeAttachPoint = new Vector3f(33F /16F, 23F /16F, 0F /16F);


		gunSlideDistance = 0F;
		animationType = EnumAnimationType.BOTTOM_CLIP;

		pumpTime = 6;
		pumpDelayAfterReload = 75;
		pumpHandleDistance = 1.2F;

		translateAll(0F, 0F, 0F);


		flipAll();
	}
}