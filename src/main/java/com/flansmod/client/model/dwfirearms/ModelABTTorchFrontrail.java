package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.ModelAttachment;
import com.flansmod.client.tmt.ModelRendererTurbo;

import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelABTTorchFrontrail extends ModelAttachment
{
	int textureX = 512;
	int textureY = 256;

	public ModelABTTorchFrontrail()
	{	
		attachmentModel = new ModelRendererTurbo[36];
		attachmentModel[0] = new ModelRendererTurbo(this, 110, 1, textureX, textureY); // topRail
		attachmentModel[1] = new ModelRendererTurbo(this, 110, 35, textureX, textureY); // bodyRail3
		attachmentModel[2] = new ModelRendererTurbo(this, 110, 14, textureX, textureY); // bodyRail1
		attachmentModel[3] = new ModelRendererTurbo(this, 110, 57, textureX, textureY); // woodenGrip2
		attachmentModel[4] = new ModelRendererTurbo(this, 110, 48, textureX, textureY); // Box 12
		attachmentModel[5] = new ModelRendererTurbo(this, 110, 57, textureX, textureY); // Box 13
		attachmentModel[6] = new ModelRendererTurbo(this, 110, 57, textureX, textureY); // Box 14
		attachmentModel[7] = new ModelRendererTurbo(this, 110, 57, textureX, textureY); // Box 15
		attachmentModel[8] = new ModelRendererTurbo(this, 110, 57, textureX, textureY); // Box 16
		attachmentModel[9] = new ModelRendererTurbo(this, 110, 57, textureX, textureY); // Box 17
		attachmentModel[10] = new ModelRendererTurbo(this, 110, 57, textureX, textureY); // Box 18
		attachmentModel[11] = new ModelRendererTurbo(this, 129, 60, textureX, textureY); // Box 20
		attachmentModel[12] = new ModelRendererTurbo(this, 129, 66, textureX, textureY); // Box 21
		attachmentModel[13] = new ModelRendererTurbo(this, 129, 57, textureX, textureY); // Box 22
		attachmentModel[14] = new ModelRendererTurbo(this, 129, 57, textureX, textureY); // Box 23
		attachmentModel[15] = new ModelRendererTurbo(this, 129, 60, textureX, textureY); // Box 24
		attachmentModel[16] = new ModelRendererTurbo(this, 129, 66, textureX, textureY); // Box 25
		attachmentModel[17] = new ModelRendererTurbo(this, 129, 57, textureX, textureY); // Box 26
		attachmentModel[18] = new ModelRendererTurbo(this, 129, 57, textureX, textureY); // Box 27
		attachmentModel[19] = new ModelRendererTurbo(this, 110, 57, textureX, textureY); // Box 28
		attachmentModel[20] = new ModelRendererTurbo(this, 195, 15, textureX, textureY); // Box 29
		attachmentModel[21] = new ModelRendererTurbo(this, 110, 86, textureX, textureY); // Box 0
		attachmentModel[22] = new ModelRendererTurbo(this, 135, 74, textureX, textureY); // Box 1
		attachmentModel[23] = new ModelRendererTurbo(this, 135, 74, textureX, textureY); // Box 2
		attachmentModel[24] = new ModelRendererTurbo(this, 138, 87, textureX, textureY); // Box 6
		attachmentModel[25] = new ModelRendererTurbo(this, 129, 86, textureX, textureY); // Box 7
		attachmentModel[26] = new ModelRendererTurbo(this, 138, 87, textureX, textureY); // Box 8
		attachmentModel[27] = new ModelRendererTurbo(this, 145, 88, textureX, textureY); // Box 9
		attachmentModel[28] = new ModelRendererTurbo(this, 145, 88, textureX, textureY); // Box 10
		attachmentModel[29] = new ModelRendererTurbo(this, 145, 88, textureX, textureY); // Box 11
		attachmentModel[30] = new ModelRendererTurbo(this, 110, 74, textureX, textureY); // Box 12
		attachmentModel[31] = new ModelRendererTurbo(this, 152, 74, textureX, textureY); // Box 13
		attachmentModel[32] = new ModelRendererTurbo(this, 152, 74, textureX, textureY); // Box 14
		attachmentModel[33] = new ModelRendererTurbo(this, 138, 87, textureX, textureY); // Box 15
		attachmentModel[34] = new ModelRendererTurbo(this, 138, 87, textureX, textureY); // Box 16
		attachmentModel[35] = new ModelRendererTurbo(this, 129, 86, textureX, textureY); // Box 17

		attachmentModel[0].addShapeBox(0F, -7F, -5F, 32, 2, 10, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // topRail
		attachmentModel[0].setRotationPoint(0F, 0F, 0F);

		attachmentModel[1].addShapeBox(0F, 5F, -5F, 32, 2, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // bodyRail3
		attachmentModel[1].setRotationPoint(0F, 0F, 0F);

		attachmentModel[2].addBox(0F, -5F, -5F, 32, 10, 10, 0F); // bodyRail1
		attachmentModel[2].setRotationPoint(0F, 0F, 0F);

		attachmentModel[3].addShapeBox(1F, 7.5F, -3.5F, 2, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // woodenGrip2
		attachmentModel[3].setRotationPoint(0F, 0F, 0F);

		attachmentModel[4].addBox(1F, 6.5F, -3.5F, 30, 1, 7, 0F); // Box 12
		attachmentModel[4].setRotationPoint(0F, 0F, 0F);

		attachmentModel[5].addShapeBox(5F, 7.5F, -3.5F, 2, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 13
		attachmentModel[5].setRotationPoint(0F, 0F, 0F);

		attachmentModel[6].addShapeBox(9F, 7.5F, -3.5F, 2, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 14
		attachmentModel[6].setRotationPoint(0F, 0F, 0F);

		attachmentModel[7].addShapeBox(13F, 7.5F, -3.5F, 2, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 15
		attachmentModel[7].setRotationPoint(0F, 0F, 0F);

		attachmentModel[8].addShapeBox(17F, 7.5F, -3.5F, 2, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 16
		attachmentModel[8].setRotationPoint(0F, 0F, 0F);

		attachmentModel[9].addShapeBox(21F, 7.5F, -3.5F, 2, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 17
		attachmentModel[9].setRotationPoint(0F, 0F, 0F);

		attachmentModel[10].addShapeBox(25F, 7.5F, -3.5F, 2, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 18
		attachmentModel[10].setRotationPoint(0F, 0F, 0F);

		attachmentModel[11].addBox(5F, -2F, -5.5F, 26, 4, 1, 0F); // Box 20
		attachmentModel[11].setRotationPoint(0F, 0F, 0F);

		attachmentModel[12].addShapeBox(5F, -3F, -6.5F, 26, 6, 1, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 21
		attachmentModel[12].setRotationPoint(0F, 0F, 0F);

		attachmentModel[13].addShapeBox(5F, -3F, -7F, 26, 1, 1, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 22
		attachmentModel[13].setRotationPoint(0F, 0F, 0F);

		attachmentModel[14].addShapeBox(5F, 2F, -7F, 26, 1, 1, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 23
		attachmentModel[14].setRotationPoint(0F, 0F, 0F);

		attachmentModel[15].addBox(5F, -2F, 4.5F, 26, 4, 1, 0F); // Box 24
		attachmentModel[15].setRotationPoint(0F, 0F, 0F);

		attachmentModel[16].addShapeBox(5F, -3F, 5F, 26, 6, 1, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 25
		attachmentModel[16].setRotationPoint(0F, 0F, 0F);

		attachmentModel[17].addShapeBox(5F, -3F, 5.5F, 26, 1, 1, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 26
		attachmentModel[17].setRotationPoint(0F, 0F, 0F);

		attachmentModel[18].addShapeBox(5F, 2F, 5.5F, 26, 1, 1, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 27
		attachmentModel[18].setRotationPoint(0F, 0F, 0F);

		attachmentModel[19].addShapeBox(29F, 7.5F, -3.5F, 2, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 28
		attachmentModel[19].setRotationPoint(0F, 0F, 0F);

		attachmentModel[20].addBox(1F, -4F, -5.5F, 3, 8, 11, 0F); // Box 29
		attachmentModel[20].setRotationPoint(0F, 0F, 0F);

		attachmentModel[21].addBox(26F, -3.5F, -11.5F, 6, 7, 3, 0F); // Box 0
		attachmentModel[21].setRotationPoint(0F, 0F, 0F);

		attachmentModel[22].addShapeBox(26F, -3.5F, -13.5F, 6, 7, 2, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 1
		attachmentModel[22].setRotationPoint(0F, 0F, 0F);

		attachmentModel[23].addShapeBox(26F, -3.5F, -8.5F, 6, 7, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F); // Box 2
		attachmentModel[23].setRotationPoint(0F, 0F, 0F);

		attachmentModel[24].addShapeBox(11F, -3.5F, -13.5F, 1, 7, 2, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 6
		attachmentModel[24].setRotationPoint(0F, 0F, 0F);

		attachmentModel[25].addBox(11F, -3.5F, -11.5F, 1, 7, 3, 0F); // Box 7
		attachmentModel[25].setRotationPoint(0F, 0F, 0F);

		attachmentModel[26].addShapeBox(11F, -3.5F, -8.5F, 1, 7, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F); // Box 8
		attachmentModel[26].setRotationPoint(0F, 0F, 0F);

		attachmentModel[27].addBox(10F, -3F, -11F, 16, 6, 2, 0F); // Box 9
		attachmentModel[27].setRotationPoint(0F, 0F, 0F);

		attachmentModel[28].addShapeBox(10F, -3F, -9F, 16, 6, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F); // Box 10
		attachmentModel[28].setRotationPoint(0F, 0F, 0F);

		attachmentModel[29].addShapeBox(10F, -3F, -13F, 16, 6, 2, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 11
		attachmentModel[29].setRotationPoint(0F, 0F, 0F);

		attachmentModel[30].addBox(17F, -3.5F, -9F, 8, 7, 4, 0F); // Box 12
		attachmentModel[30].setRotationPoint(0F, 0F, 0F);

		attachmentModel[31].addBox(19F, -3.5F, -11F, 4, 7, 2, 0F); // Box 13
		attachmentModel[31].setRotationPoint(0F, 0F, 0F);

		attachmentModel[32].addShapeBox(19F, -3.5F, -13F, 4, 7, 2, 0F, 0F, -2F, 0.3F, 0F, -2F, 0.3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0.3F, 0F, -2F, 0.3F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 14
		attachmentModel[32].setRotationPoint(0F, 0F, 0F);

		attachmentModel[33].addShapeBox(13F, -3.5F, -13.5F, 1, 7, 2, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 15
		attachmentModel[33].setRotationPoint(0F, 0F, 0F);

		attachmentModel[34].addShapeBox(13F, -3.5F, -8.5F, 1, 7, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F); // Box 16
		attachmentModel[34].setRotationPoint(0F, 0F, 0F);

		attachmentModel[35].addBox(13F, -3.5F, -11.5F, 1, 7, 3, 0F); // Box 17
		attachmentModel[35].setRotationPoint(0F, 0F, 0F);

		renderOffset = 0F;

		//Based off the Ambircon SteamRifle positioning.

		flipAll();
	}
}

