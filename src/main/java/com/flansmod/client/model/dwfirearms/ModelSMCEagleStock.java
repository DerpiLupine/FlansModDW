package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.ModelAttachment;
import com.flansmod.client.tmt.ModelRendererTurbo;

import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelSMCEagleStock extends ModelAttachment
{
	int textureX = 512;
	int textureY = 256;

	public ModelSMCEagleStock()
	{
		attachmentModel = new ModelRendererTurbo[18];
		attachmentModel[0] = new ModelRendererTurbo(this, 48, 143, textureX, textureY); // Box 64
		attachmentModel[1] = new ModelRendererTurbo(this, 1, 139, textureX, textureY); // Box 65
		attachmentModel[2] = new ModelRendererTurbo(this, 1, 171, textureX, textureY); // Box 66
		attachmentModel[3] = new ModelRendererTurbo(this, 1, 159, textureX, textureY); // Box 67
		attachmentModel[4] = new ModelRendererTurbo(this, 65, 148, textureX, textureY); // Box 68
		attachmentModel[5] = new ModelRendererTurbo(this, 65, 148, textureX, textureY); // Box 69
		attachmentModel[6] = new ModelRendererTurbo(this, 83, 168, textureX, textureY); // Box 70
		attachmentModel[7] = new ModelRendererTurbo(this, 95, 187, textureX, textureY); // Box 71
		attachmentModel[8] = new ModelRendererTurbo(this, 1, 206, textureX, textureY); // Box 72
		attachmentModel[9] = new ModelRendererTurbo(this, 1, 195, textureX, textureY); // Box 74
		attachmentModel[10] = new ModelRendererTurbo(this, 1, 183, textureX, textureY); // Box 75
		attachmentModel[11] = new ModelRendererTurbo(this, 48, 187, textureX, textureY); // Box 76
		attachmentModel[12] = new ModelRendererTurbo(this, 102, 217, textureX, textureY); // Box 78
		attachmentModel[13] = new ModelRendererTurbo(this, 73, 221, textureX, textureY); // Box 79
		attachmentModel[14] = new ModelRendererTurbo(this, 48, 211, textureX, textureY); // Box 80
		attachmentModel[15] = new ModelRendererTurbo(this, 73, 209, textureX, textureY); // Box 81
		attachmentModel[16] = new ModelRendererTurbo(this, 48, 199, textureX, textureY); // Box 82
		attachmentModel[17] = new ModelRendererTurbo(this, 48, 175, textureX, textureY); // Box 83

		attachmentModel[0].addBox(-1F, -3.5F, -3.5F, 1, 7, 7, 0F); // Box 64
		attachmentModel[0].setRotationPoint(0F, 0F, 0F);

		attachmentModel[1].addBox(-15F, -1.5F, -4.5F, 14, 6, 9, 0F); // Box 65
		attachmentModel[1].setRotationPoint(0F, 0F, 0F);

		attachmentModel[2].addShapeBox(-15F, 4.5F, -4.5F, 14, 2, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 66
		attachmentModel[2].setRotationPoint(0F, 0F, 0F);

		attachmentModel[3].addShapeBox(-35F, -6.5F, -4.5F, 20, 2, 9, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 67
		attachmentModel[3].setRotationPoint(0F, 0F, 0F);

		attachmentModel[4].addShapeBox(-1F, -5.5F, -3.5F, 1, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 68
		attachmentModel[4].setRotationPoint(0F, 0F, 0F);

		attachmentModel[5].addShapeBox(-1F, 3.5F, -3.5F, 1, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 69
		attachmentModel[5].setRotationPoint(0F, 0F, 0F);

		attachmentModel[6].addShapeBox(-21F, -4.5F, -4.5F, 6, 9, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 10F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 10F, 0F); // Box 70
		attachmentModel[6].setRotationPoint(0F, 0F, 0F);

		attachmentModel[7].addShapeBox(-21F, 4.5F, -4.5F, 6, 2, 9, 0F, 0F, -10F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -10F, 0F, 0F, 10F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 10F, -2F); // Box 71
		attachmentModel[7].setRotationPoint(0F, 0F, 0F);

		attachmentModel[8].addBox(-35F, -4.5F, -4.5F, 14, 19, 9, 0F); // Box 72
		attachmentModel[8].setRotationPoint(0F, 0F, 0F);

		attachmentModel[9].addShapeBox(-15F, -6F, -4F, 14, 2, 8, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 74
		attachmentModel[9].setRotationPoint(0F, 0F, 0F);

		attachmentModel[10].addBox(-15F, -4F, -4F, 14, 3, 8, 0F); // Box 75
		attachmentModel[10].setRotationPoint(0F, 0F, 0F);

		attachmentModel[11].addShapeBox(-35F, 14.5F, -4.5F, 14, 2, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 76
		attachmentModel[11].setRotationPoint(0F, 0F, 0F);

		attachmentModel[12].addBox(-37F, -6F, -5F, 4, 7, 10, 0F); // Box 78
		attachmentModel[12].setRotationPoint(0F, 0F, 0F);

		attachmentModel[13].addShapeBox(-37F, 1F, -5F, 4, 3, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F); // Box 79
		attachmentModel[13].setRotationPoint(0F, 0F, 0F);

		attachmentModel[14].addBox(-37F, 4F, -5F, 2, 13, 10, 0F); // Box 80
		attachmentModel[14].setRotationPoint(0F, 0F, 0F);

		attachmentModel[15].addShapeBox(-37F, -7F, -5F, 4, 1, 10, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 81
		attachmentModel[15].setRotationPoint(0F, 0F, 0F);

		attachmentModel[16].addShapeBox(-37F, 17F, -5F, 2, 1, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 82
		attachmentModel[16].setRotationPoint(0F, 0F, 0F);

		attachmentModel[17].addBox(-35F, 14.5F, -3.5F, 10, 4, 7, 0F); // Box 83
		attachmentModel[17].setRotationPoint(0F, 0F, 0F);

		renderOffset = 0F;

		flipAll();
	}
}