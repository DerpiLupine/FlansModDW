package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelFeathershockAR extends ModelGun
{
	int textureX = 512;
	int textureY = 256;

	public ModelFeathershockAR()
	{
		gunModel = new ModelRendererTurbo[113];
		gunModel[0] = new ModelRendererTurbo(this, 51, 36, textureX, textureY); // Box 32
		gunModel[1] = new ModelRendererTurbo(this, 143, 72, textureX, textureY); // Box 33
		gunModel[2] = new ModelRendererTurbo(this, 1, 28, textureX, textureY); // Box 34
		gunModel[3] = new ModelRendererTurbo(this, 1, 40, textureX, textureY); // Box 35
		gunModel[4] = new ModelRendererTurbo(this, 1, 54, textureX, textureY); // Box 36
		gunModel[5] = new ModelRendererTurbo(this, 1, 70, textureX, textureY); // Box 38
		gunModel[6] = new ModelRendererTurbo(this, 38, 28, textureX, textureY); // Box 39
		gunModel[7] = new ModelRendererTurbo(this, 38, 40, textureX, textureY); // Box 40
		gunModel[8] = new ModelRendererTurbo(this, 38, 54, textureX, textureY); // Box 41
		gunModel[9] = new ModelRendererTurbo(this, 38, 70, textureX, textureY); // Box 42
		gunModel[10] = new ModelRendererTurbo(this, 96, 13, textureX, textureY); // body1
		gunModel[11] = new ModelRendererTurbo(this, 192, 111, textureX, textureY); // body10
		gunModel[12] = new ModelRendererTurbo(this, 239, 112, textureX, textureY); // body11
		gunModel[13] = new ModelRendererTurbo(this, 96, 1, textureX, textureY); // body2
		gunModel[14] = new ModelRendererTurbo(this, 94, 84, textureX, textureY); // body6
		gunModel[15] = new ModelRendererTurbo(this, 23, 1, textureX, textureY); // switch1
		gunModel[16] = new ModelRendererTurbo(this, 23, 1, textureX, textureY); // switch2
		gunModel[17] = new ModelRendererTurbo(this, 23, 1, textureX, textureY); // switch3
		gunModel[18] = new ModelRendererTurbo(this, 23, 6, textureX, textureY); // switch4
		gunModel[19] = new ModelRendererTurbo(this, 94, 169, textureX, textureY); // Box 16
		gunModel[20] = new ModelRendererTurbo(this, 94, 108, textureX, textureY); // Box 17
		gunModel[21] = new ModelRendererTurbo(this, 94, 122, textureX, textureY); // Box 18
		gunModel[22] = new ModelRendererTurbo(this, 94, 108, textureX, textureY); // Box 19
		gunModel[23] = new ModelRendererTurbo(this, 94, 145, textureX, textureY); // Box 23
		gunModel[24] = new ModelRendererTurbo(this, 109, 137, textureX, textureY); // Box 24
		gunModel[25] = new ModelRendererTurbo(this, 94, 137, textureX, textureY); // Box 26
		gunModel[26] = new ModelRendererTurbo(this, 109, 145, textureX, textureY); // Box 27
		gunModel[27] = new ModelRendererTurbo(this, 94, 153, textureX, textureY); // Box 29
		gunModel[28] = new ModelRendererTurbo(this, 94, 100, textureX, textureY); // Box 30
		gunModel[29] = new ModelRendererTurbo(this, 94, 104, textureX, textureY); // Box 31
		gunModel[30] = new ModelRendererTurbo(this, 94, 100, textureX, textureY); // Box 32
		gunModel[31] = new ModelRendererTurbo(this, 94, 104, textureX, textureY); // Box 33
		gunModel[32] = new ModelRendererTurbo(this, 192, 122, textureX, textureY); // Box 34
		gunModel[33] = new ModelRendererTurbo(this, 249, 135, textureX, textureY); // Box 35
		gunModel[34] = new ModelRendererTurbo(this, 192, 135, textureX, textureY); // Box 36
		gunModel[35] = new ModelRendererTurbo(this, 109, 185, textureX, textureY); // Box 40
		gunModel[36] = new ModelRendererTurbo(this, 94, 185, textureX, textureY); // Box 41
		gunModel[37] = new ModelRendererTurbo(this, 145, 137, textureX, textureY); // Box 42
		gunModel[38] = new ModelRendererTurbo(this, 109, 193, textureX, textureY); // Box 43
		gunModel[39] = new ModelRendererTurbo(this, 94, 193, textureX, textureY); // Box 44
		gunModel[40] = new ModelRendererTurbo(this, 124, 137, textureX, textureY); // Box 45
		gunModel[41] = new ModelRendererTurbo(this, 192, 159, textureX, textureY); // Box 66
		gunModel[42] = new ModelRendererTurbo(this, 192, 153, textureX, textureY); // Box 69
		gunModel[43] = new ModelRendererTurbo(this, 192, 146, textureX, textureY); // Box 70
		gunModel[44] = new ModelRendererTurbo(this, 192, 146, textureX, textureY); // Box 71
		gunModel[45] = new ModelRendererTurbo(this, 145, 137, textureX, textureY); // Box 0
		gunModel[46] = new ModelRendererTurbo(this, 192, 100, textureX, textureY); // Box 10
		gunModel[47] = new ModelRendererTurbo(this, 299, 80, textureX, textureY); // Box 11
		gunModel[48] = new ModelRendererTurbo(this, 272, 80, textureX, textureY); // Box 12
		gunModel[49] = new ModelRendererTurbo(this, 251, 83, textureX, textureY); // Box 13
		gunModel[50] = new ModelRendererTurbo(this, 192, 83, textureX, textureY); // Box 14
		gunModel[51] = new ModelRendererTurbo(this, 192, 73, textureX, textureY); // Box 15
		gunModel[52] = new ModelRendererTurbo(this, 251, 73, textureX, textureY); // Box 16
		gunModel[53] = new ModelRendererTurbo(this, 192, 166, textureX, textureY); // Box 74
		gunModel[54] = new ModelRendererTurbo(this, 169, 120, textureX, textureY); // Box 75
		gunModel[55] = new ModelRendererTurbo(this, 169, 120, textureX, textureY); // Box 76
		gunModel[56] = new ModelRendererTurbo(this, 273, 101, textureX, textureY); // Box 78
		gunModel[57] = new ModelRendererTurbo(this, 143, 89, textureX, textureY); // Box 79
		gunModel[58] = new ModelRendererTurbo(this, 12, 1, textureX, textureY); // Box 84
		gunModel[59] = new ModelRendererTurbo(this, 96, 42, textureX, textureY); // Box 89
		gunModel[60] = new ModelRendererTurbo(this, 96, 61, textureX, textureY); // Box 90
		gunModel[61] = new ModelRendererTurbo(this, 131, 35, textureX, textureY); // Box 1
		gunModel[62] = new ModelRendererTurbo(this, 236, 26, textureX, textureY); // Box 2
		gunModel[63] = new ModelRendererTurbo(this, 223, 26, textureX, textureY); // Box 3
		gunModel[64] = new ModelRendererTurbo(this, 67, 11, textureX, textureY); // Box 4
		gunModel[65] = new ModelRendererTurbo(this, 193, 13, textureX, textureY); // Box 8
		gunModel[66] = new ModelRendererTurbo(this, 165, 49, textureX, textureY); // Box 9
		gunModel[67] = new ModelRendererTurbo(this, 165, 61, textureX, textureY); // Box 10
		gunModel[68] = new ModelRendererTurbo(this, 96, 35, textureX, textureY); // Box 11
		gunModel[69] = new ModelRendererTurbo(this, 168, 37, textureX, textureY); // Box 12
		gunModel[70] = new ModelRendererTurbo(this, 94, 72, textureX, textureY); // Box 13
		gunModel[71] = new ModelRendererTurbo(this, 187, 39, textureX, textureY); // Box 14
		gunModel[72] = new ModelRendererTurbo(this, 187, 39, textureX, textureY); // Box 15
		gunModel[73] = new ModelRendererTurbo(this, 123, 1, textureX, textureY); // Box 17
		gunModel[74] = new ModelRendererTurbo(this, 123, 13, textureX, textureY); // Box 18
		gunModel[75] = new ModelRendererTurbo(this, 150, 12, textureX, textureY); // Box 19
		gunModel[76] = new ModelRendererTurbo(this, 150, 3, textureX, textureY); // Box 20
		gunModel[77] = new ModelRendererTurbo(this, 176, 23, textureX, textureY); // Box 22
		gunModel[78] = new ModelRendererTurbo(this, 155, 23, textureX, textureY); // Box 23
		gunModel[79] = new ModelRendererTurbo(this, 1, 9, textureX, textureY); // Box 0
		gunModel[80] = new ModelRendererTurbo(this, 41, 6, textureX, textureY); // Box 0
		gunModel[81] = new ModelRendererTurbo(this, 41, 6, textureX, textureY); // Box 1
		gunModel[82] = new ModelRendererTurbo(this, 41, 6, textureX, textureY); // Box 2
		gunModel[83] = new ModelRendererTurbo(this, 192, 93, textureX, textureY); // Box 5
		gunModel[84] = new ModelRendererTurbo(this, 166, 142, textureX, textureY); // Box 7
		gunModel[85] = new ModelRendererTurbo(this, 124, 148, textureX, textureY); // Box 8
		gunModel[86] = new ModelRendererTurbo(this, 167, 147, textureX, textureY); // Box 10
		gunModel[87] = new ModelRendererTurbo(this, 158, 147, textureX, textureY); // Box 11
		gunModel[88] = new ModelRendererTurbo(this, 149, 148, textureX, textureY); // Box 12
		gunModel[89] = new ModelRendererTurbo(this, 272, 73, textureX, textureY); // Box 13
		gunModel[90] = new ModelRendererTurbo(this, 16, 19, textureX, textureY); // Box 14
		gunModel[91] = new ModelRendererTurbo(this, 16, 19, textureX, textureY); // Box 15
		gunModel[92] = new ModelRendererTurbo(this, 1, 19, textureX, textureY); // Box 16
		gunModel[93] = new ModelRendererTurbo(this, 1, 19, textureX, textureY); // Box 17
		gunModel[94] = new ModelRendererTurbo(this, 161, 157, textureX, textureY); // Box 18
		gunModel[95] = new ModelRendererTurbo(this, 96, 23, textureX, textureY); // Box 19
		gunModel[96] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 20
		gunModel[97] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 21
		gunModel[98] = new ModelRendererTurbo(this, 96, 29, textureX, textureY); // Box 22
		gunModel[99] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 23
		gunModel[100] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 24
		gunModel[101] = new ModelRendererTurbo(this, 12, 1, textureX, textureY); // Box 25
		gunModel[102] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 26
		gunModel[103] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 27
		gunModel[104] = new ModelRendererTurbo(this, 12, 1, textureX, textureY); // Box 28
		gunModel[105] = new ModelRendererTurbo(this, 210, 25, textureX, textureY); // Box 30
		gunModel[106] = new ModelRendererTurbo(this, 197, 25, textureX, textureY); // Box 31
		gunModel[107] = new ModelRendererTurbo(this, 125, 23, textureX, textureY); // Box 32
		gunModel[108] = new ModelRendererTurbo(this, 125, 31, textureX, textureY); // Box 33
		gunModel[109] = new ModelRendererTurbo(this, 75, 15, textureX, textureY); // Box 34
		gunModel[110] = new ModelRendererTurbo(this, 67, 7, textureX, textureY); // Box 52
		gunModel[111] = new ModelRendererTurbo(this, 67, 7, textureX, textureY); // Box 53
		gunModel[112] = new ModelRendererTurbo(this, 59, 53, textureX, textureY); // Box 0

		gunModel[0].addShapeBox(0F, 0F, 0F, 12, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F); // Box 32
		gunModel[0].setRotationPoint(-11F, -8F, -4F);

		gunModel[1].addBox(0F, 0F, 0F, 13, 3, 8, 0F); // Box 33
		gunModel[1].setRotationPoint(1F, -8F, -4F);

		gunModel[2].addShapeBox(0F, 0F, 0F, 10, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 34
		gunModel[2].setRotationPoint(-8F, -5F, -4F);

		gunModel[3].addShapeBox(0F, 0F, 0F, 10, 5, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 3F, 0F, 0F); // Box 35
		gunModel[3].setRotationPoint(-8F, -2F, -4F);

		gunModel[4].addShapeBox(0F, 0F, 0F, 10, 7, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, -3F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 3F, -3F, 0F); // Box 36
		gunModel[4].setRotationPoint(-11F, 3F, -4F);

		gunModel[5].addShapeBox(0F, 0F, 0F, 9, 2, 8, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F); // Box 38
		gunModel[5].setRotationPoint(-14F, 10F, -4F);

		gunModel[6].addShapeBox(0F, 0F, 0F, 2, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F); // Box 39
		gunModel[6].setRotationPoint(2F, -5F, -4F);

		gunModel[7].addShapeBox(0F, 0F, 0F, 2, 5, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, -1.5F, -3F, 0F, -1.5F, 3F, 0F, 0F); // Box 40
		gunModel[7].setRotationPoint(2F, -2F, -4F);

		gunModel[8].addShapeBox(0F, 0F, 0F, 2, 7, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 4F, 0F, 0F, -4F, 0F, -1.5F, -4F, 0F, -1.5F, 4F, 0F, 0F); // Box 41
		gunModel[8].setRotationPoint(-1F, 3F, -4F);

		gunModel[9].addShapeBox(0F, 0F, 0F, 2, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F); // Box 42
		gunModel[9].setRotationPoint(-5F, 10F, -4F);

		gunModel[10].addShapeBox(0F, 0F, 0F, 5, 1, 8, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body1
		gunModel[10].setRotationPoint(-8F, -22F, -4F);

		gunModel[11].addBox(0F, 0F, 0F, 16, 3, 7, 0F); // body10
		gunModel[11].setRotationPoint(14F, -17F, -3.5F);

		gunModel[12].addShapeBox(0F, 0F, 0F, 16, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body11
		gunModel[12].setRotationPoint(14F, -19F, -3.5F);

		gunModel[13].addShapeBox(0F, 0F, 0F, 5, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // body2
		gunModel[13].setRotationPoint(-8F, -21F, -4F);

		gunModel[14].addBox(0F, 0F, 0F, 16, 7, 8, 0F); // body6
		gunModel[14].setRotationPoint(14F, -8F, -4F);

		gunModel[15].addShapeBox(0F, 0F, 0F, 1, 3, 1, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F); // switch1
		gunModel[15].setRotationPoint(-5F, -8F, -4.5F);

		gunModel[16].addBox(0F, 0F, 0F, 1, 3, 1, 0F); // switch2
		gunModel[16].setRotationPoint(-4F, -8F, -4.5F);

		gunModel[17].addShapeBox(0F, 0F, 0F, 1, 3, 1, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F); // switch3
		gunModel[17].setRotationPoint(-3F, -8F, -4.5F);

		gunModel[18].addBox(0F, 0F, 0F, 1, 1, 1, 0F); // switch4
		gunModel[18].setRotationPoint(-2F, -7F, -4.5F);

		gunModel[19].addShapeBox(0F, 0F, 0F, 27, 3, 12, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 16
		gunModel[19].setRotationPoint(33F, -22F, -6F);

		gunModel[20].addShapeBox(0F, 0F, 0F, 31, 3, 10, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 17
		gunModel[20].setRotationPoint(31F, -20.5F, -5F);

		gunModel[21].addBox(0F, 0F, 0F, 31, 4, 10, 0F); // Box 18
		gunModel[21].setRotationPoint(31F, -17.5F, -5F);

		gunModel[22].addShapeBox(0F, 0F, 0F, 31, 3, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F); // Box 19
		gunModel[22].setRotationPoint(31F, -13.5F, -5F);

		gunModel[23].addShapeBox(0F, 0F, 0F, 3, 3, 4, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F); // Box 23
		gunModel[23].setRotationPoint(30F, -22F, -6F);

		gunModel[24].addShapeBox(0F, 0F, 0F, 3, 3, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 24
		gunModel[24].setRotationPoint(30F, -22F, 2F);

		gunModel[25].addShapeBox(0F, 0F, 0F, 3, 3, 4, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F); // Box 26
		gunModel[25].setRotationPoint(30F, -12F, 2F);

		gunModel[26].addShapeBox(0F, 0F, 0F, 3, 3, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 27
		gunModel[26].setRotationPoint(30F, -12F, -6F);

		gunModel[27].addShapeBox(0F, 0F, 0F, 27, 3, 12, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F); // Box 29
		gunModel[27].setRotationPoint(33F, -12F, -6F);

		gunModel[28].addShapeBox(0F, 0F, 0F, 33, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 30
		gunModel[28].setRotationPoint(30F, -19F, -6F);

		gunModel[29].addShapeBox(0F, 0F, 0F, 33, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 31
		gunModel[29].setRotationPoint(30F, -13F, -6F);

		gunModel[30].addShapeBox(0F, 0F, 0F, 33, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 32
		gunModel[30].setRotationPoint(30F, -13F, 4.5F);

		gunModel[31].addShapeBox(0F, 0F, 0F, 33, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 33
		gunModel[31].setRotationPoint(30F, -19F, 4.5F);

		gunModel[32].addBox(0F, 0F, 0F, 20, 4, 8, 0F); // Box 34
		gunModel[32].setRotationPoint(63F, -17.5F, -4F);

		gunModel[33].addShapeBox(0F, 0F, 0F, 20, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 35
		gunModel[33].setRotationPoint(63F, -13.5F, -4F);

		gunModel[34].addShapeBox(0F, 0F, 0F, 20, 2, 8, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 36
		gunModel[34].setRotationPoint(63F, -19.5F, -4F);

		gunModel[35].addShapeBox(0F, 0F, 0F, 3, 3, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 40
		gunModel[35].setRotationPoint(60F, -22F, 2F);

		gunModel[36].addShapeBox(0F, 0F, 0F, 3, 3, 4, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F); // Box 41
		gunModel[36].setRotationPoint(60F, -12F, 2F);

		gunModel[37].addBox(0F, 0F, 0F, 3, 1, 7, 0F); // Box 42
		gunModel[37].setRotationPoint(60F, -10F, -3.5F);

		gunModel[38].addShapeBox(0F, 0F, 0F, 3, 3, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 43
		gunModel[38].setRotationPoint(60F, -12F, -6F);

		gunModel[39].addShapeBox(0F, 0F, 0F, 3, 3, 4, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F); // Box 44
		gunModel[39].setRotationPoint(60F, -22F, -6F);

		gunModel[40].addBox(0F, 0F, 0F, 3, 1, 7, 0F); // Box 45
		gunModel[40].setRotationPoint(60F, -22F, -3.5F);

		gunModel[41].addShapeBox(0F, 0F, 0F, 20, 2, 4, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 66
		gunModel[41].setRotationPoint(63F, -21.5F, -2F);

		gunModel[42].addBox(0F, 0F, 0F, 20, 1, 4, 0F); // Box 69
		gunModel[42].setRotationPoint(63F, -11.5F, -2F);

		gunModel[43].addShapeBox(0F, 0F, 0F, 22, 2, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 70
		gunModel[43].setRotationPoint(62F, -17.5F, -2F);

		gunModel[44].addShapeBox(0F, 0F, 0F, 22, 2, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 71
		gunModel[44].setRotationPoint(62F, -15.5F, -2F);

		gunModel[45].addBox(0F, 0F, 0F, 3, 1, 7, 0F); // Box 0
		gunModel[45].setRotationPoint(30F, -22F, -3.5F);

		gunModel[46].addBox(0F, 0F, 0F, 32, 2, 8, 0F); // Box 10
		gunModel[46].setRotationPoint(30F, -10F, -4F);

		gunModel[47].addShapeBox(0F, 0F, 0F, 3, 4, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F); // Box 11
		gunModel[47].setRotationPoint(35F, -8F, -4F);

		gunModel[48].addBox(0F, 0F, 0F, 5, 4, 8, 0F); // Box 12
		gunModel[48].setRotationPoint(30F, -8F, -4F);

		gunModel[49].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, -2F, 0F, -1.5F, -2F, 0F, -1.5F, 0F, 0F, -1.5F); // Box 13
		gunModel[49].setRotationPoint(58F, -6F, -3.5F);

		gunModel[50].addShapeBox(0F, 0F, 0F, 22, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F); // Box 14
		gunModel[50].setRotationPoint(36F, -6F, -3.5F);

		gunModel[51].addBox(0F, 0F, 0F, 22, 2, 7, 0F); // Box 15
		gunModel[51].setRotationPoint(36F, -8F, -3.5F);

		gunModel[52].addBox(0F, 0F, 0F, 3, 2, 7, 0F); // Box 16
		gunModel[52].setRotationPoint(58F, -8F, -3.5F);

		gunModel[53].addBox(0F, 0F, 0F, 1, 4, 8, 0F); // Box 74
		gunModel[53].setRotationPoint(30F, -17.5F, -4F);

		gunModel[54].addShapeBox(0F, 0F, 0F, 1, 2, 8, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 75
		gunModel[54].setRotationPoint(30F, -19.5F, -4F);

		gunModel[55].addShapeBox(0F, 0F, 0F, 1, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 76
		gunModel[55].setRotationPoint(30F, -13.5F, -4F);

		gunModel[56].addShapeBox(0F, 0F, 0F, 16, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 78
		gunModel[56].setRotationPoint(14F, -14F, -3.5F);

		gunModel[57].addBox(0F, 0F, 0F, 16, 2, 8, 0F); // Box 79
		gunModel[57].setRotationPoint(14F, -20F, -4F);

		gunModel[58].addBox(-0.5F, -1.5F, -4F, 1, 3, 4, 0F); // Box 84
		gunModel[58].setRotationPoint(12F, -16F, -5F);
		gunModel[58].rotateAngleX = -0.61086524F;

		gunModel[59].addBox(0F, 0F, 0F, 25, 9, 9, 0F); // Box 89
		gunModel[59].setRotationPoint(-11F, -18F, -4.5F);

		gunModel[60].addShapeBox(0F, 0F, 0F, 25, 1, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 90
		gunModel[60].setRotationPoint(-11F, -9F, -4.5F);

		gunModel[61].addShapeBox(0F, 0F, 0F, 16, 4, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F); // Box 1
		gunModel[61].setRotationPoint(14F, -14F, 4.5F);

		gunModel[62].addShapeBox(0F, 0F, 0F, 4, 4, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 2
		gunModel[62].setRotationPoint(26F, -18F, 4.5F);

		gunModel[63].addBox(0F, 0F, 0F, 4, 4, 2, 0F); // Box 3
		gunModel[63].setRotationPoint(26F, -18F, 2.5F);

		gunModel[64].addShapeBox(0F, 0F, 0F, 10, 1, 2, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 4
		gunModel[64].setRotationPoint(16F, -15F, 3.5F);

		gunModel[65].addBox(0F, 0F, 0F, 12, 2, 7, 0F); // Box 8
		gunModel[65].setRotationPoint(-3F, -21F, -3.5F);

		gunModel[66].addBox(0F, 0F, 0F, 16, 4, 7, 0F); // Box 9
		gunModel[66].setRotationPoint(14F, -13F, -4.5F);

		gunModel[67].addShapeBox(0F, 0F, 0F, 16, 1, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 10
		gunModel[67].setRotationPoint(14F, -9F, -4.5F);

		gunModel[68].addBox(0F, 0F, 0F, 16, 5, 1, 0F); // Box 11
		gunModel[68].setRotationPoint(14F, -14F, 3.5F);

		gunModel[69].addShapeBox(0F, 0F, 0F, 1, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 12
		gunModel[69].setRotationPoint(30F, -4F, -4F);

		gunModel[70].addShapeBox(0F, 0F, 0F, 16, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F); // Box 13
		gunModel[70].setRotationPoint(14F, -1F, -4F);

		gunModel[71].addShapeBox(0F, 0F, 0F, 1, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, -0.25F, -1F, 1F, -0.25F, -1F, 0F, 0F, 0F); // Box 14
		gunModel[71].setRotationPoint(30F, -1F, -4F);

		gunModel[72].addShapeBox(0F, 0F, 0F, 1, 1, 8, 0F, 0F, 0F, 0F, 1F, 0.25F, -1F, 1F, 0.25F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, -0.25F, -1F, 1F, -0.25F, -1F, 0F, 0F, 0F); // Box 15
		gunModel[72].setRotationPoint(30F, 0F, -4F);

		gunModel[73].addShapeBox(0F, 0F, 0F, 5, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // Box 17
		gunModel[73].setRotationPoint(9F, -21F, -4F);

		gunModel[74].addShapeBox(0F, 0F, 0F, 5, 1, 8, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 18
		gunModel[74].setRotationPoint(9F, -22F, -4F);

		gunModel[75].addShapeBox(0F, 0F, 0F, 12, 1, 9, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 19
		gunModel[75].setRotationPoint(-3F, -19F, -4.5F);

		gunModel[76].addShapeBox(0F, 0F, 0F, 12, 1, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 20
		gunModel[76].setRotationPoint(-3F, -22F, -3.5F);

		gunModel[77].addBox(0F, 0F, 0F, 3, 2, 7, 0F); // Box 22
		gunModel[77].setRotationPoint(-11F, -20F, -3.5F);

		gunModel[78].addShapeBox(0F, 0F, 0F, 3, 1, 7, 0F, -1F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, -1F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 23
		gunModel[78].setRotationPoint(-11F, -21F, -3.5F);

		gunModel[79].addBox(0F, 0F, 0F, 16, 2, 7, 0F); // Box 0
		gunModel[79].setRotationPoint(14F, -22F, -3.5F);

		gunModel[80].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 0
		gunModel[80].setRotationPoint(14.5F, -24F, -3.5F);

		gunModel[81].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 1
		gunModel[81].setRotationPoint(20.5F, -24F, -3.5F);

		gunModel[82].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 2
		gunModel[82].setRotationPoint(26.5F, -24F, -3.5F);

		gunModel[83].addShapeBox(0F, 0F, 0F, 53, 2, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 5
		gunModel[83].setRotationPoint(30F, -24F, -2F);

		gunModel[84].addShapeBox(0F, 0F, 0F, 3, 2, 2, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 7
		gunModel[84].setRotationPoint(70F, -26F, -1F);

		gunModel[85].addBox(0F, 0F, 0F, 10, 2, 2, 0F); // Box 8
		gunModel[85].setRotationPoint(73F, -26F, -1F);

		gunModel[86].addBox(0F, 0F, 0F, 2, 3, 2, 0F); // Box 10
		gunModel[86].setRotationPoint(83F, -24F, -1F);

		gunModel[87].addShapeBox(0F, 0F, 0F, 2, 3, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F); // Box 11
		gunModel[87].setRotationPoint(83F, -21F, -1F);

		gunModel[88].addShapeBox(0F, 0F, 0F, 2, 2, 2, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 12
		gunModel[88].setRotationPoint(83F, -26F, -1F);

		gunModel[89].addShapeBox(0F, 0F, 0F, 7, 2, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F); // Box 13
		gunModel[89].setRotationPoint(52F, -4F, -2F);

		gunModel[90].addShapeBox(0F, 0F, 0F, 5, 1, 2, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 14
		gunModel[90].setRotationPoint(79F, -12.5F, 1.5F);

		gunModel[91].addShapeBox(0F, 0F, 0F, 5, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 15
		gunModel[91].setRotationPoint(79F, -11.5F, 1.5F);

		gunModel[92].addShapeBox(0F, 0F, 0F, 5, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 16
		gunModel[92].setRotationPoint(79F, -11.5F, -3.5F);

		gunModel[93].addShapeBox(0F, 0F, 0F, 5, 1, 2, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 17
		gunModel[93].setRotationPoint(79F, -12.5F, -3.5F);

		gunModel[94].addShapeBox(0F, 0F, 0F, 4, 1, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 18
		gunModel[94].setRotationPoint(79F, -10.5F, -3F);

		gunModel[95].addShapeBox(0F, 0F, 0F, 12, 3, 2, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 19
		gunModel[95].setRotationPoint(2F, -17F, -6.5F);

		gunModel[96].addShapeBox(-1.5F, -1.5F, -4F, 1, 3, 4, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F); // Box 20
		gunModel[96].setRotationPoint(12F, -16F, -5F);
		gunModel[96].rotateAngleX = -0.61086524F;

		gunModel[97].addShapeBox(0.5F, -1.5F, -4F, 1, 3, 4, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F); // Box 21
		gunModel[97].setRotationPoint(12F, -16F, -5F);
		gunModel[97].rotateAngleX = -0.61086524F;

		gunModel[98].addShapeBox(0F, 0F, 0F, 12, 3, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 22
		gunModel[98].setRotationPoint(2F, -13F, -6.5F);

		gunModel[99].addShapeBox(0.5F, -1.5F, -4F, 1, 3, 4, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F); // Box 23
		gunModel[99].setRotationPoint(8F, -16F, -5F);
		gunModel[99].rotateAngleX = -0.61086524F;

		gunModel[100].addShapeBox(-1.5F, -1.5F, -4F, 1, 3, 4, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F); // Box 24
		gunModel[100].setRotationPoint(8F, -16F, -5F);
		gunModel[100].rotateAngleX = -0.61086524F;

		gunModel[101].addBox(-0.5F, -1.5F, -4F, 1, 3, 4, 0F); // Box 25
		gunModel[101].setRotationPoint(8F, -16F, -5F);
		gunModel[101].rotateAngleX = -0.61086524F;

		gunModel[102].addShapeBox(0.5F, -1.5F, -4F, 1, 3, 4, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F); // Box 26
		gunModel[102].setRotationPoint(4F, -16F, -5F);
		gunModel[102].rotateAngleX = -0.61086524F;

		gunModel[103].addShapeBox(-1.5F, -1.5F, -4F, 1, 3, 4, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F); // Box 27
		gunModel[103].setRotationPoint(4F, -16F, -5F);
		gunModel[103].rotateAngleX = -0.61086524F;

		gunModel[104].addBox(-0.5F, -1.5F, -4F, 1, 3, 4, 0F); // Box 28
		gunModel[104].setRotationPoint(4F, -16F, -5F);
		gunModel[104].rotateAngleX = -0.61086524F;

		gunModel[105].addBox(0F, 0F, 0F, 4, 5, 2, 0F); // Box 30
		gunModel[105].setRotationPoint(26F, -18F, -4.5F);

		gunModel[106].addShapeBox(0F, 0F, 0F, 4, 5, 2, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 31
		gunModel[106].setRotationPoint(26F, -18F, -6.5F);

		gunModel[107].addShapeBox(0F, 0F, 0F, 16, 3, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 32
		gunModel[107].setRotationPoint(14F, -13F, -6.5F);

		gunModel[108].addBox(0F, 0F, 0F, 11, 1, 2, 0F); // Box 33
		gunModel[108].setRotationPoint(2.5F, -14F, -6F);

		gunModel[109].addBox(0F, 0F, 0F, 6, 2, 1, 0F); // Box 34
		gunModel[109].setRotationPoint(-2.5F, -21F, 3F);

		gunModel[110].addShapeBox(0F, 0F, 0F, 4, 1, 2, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 52
		gunModel[110].setRotationPoint(-7F, -23F, -3F);

		gunModel[111].addShapeBox(0F, 0F, 0F, 4, 1, 2, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 53
		gunModel[111].setRotationPoint(-7F, -23F, 1F);

		gunModel[112].addShapeBox(0F, 0F, 0F, 7, 7, 1, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, -3F, -3F, 0F, -3F, -3F, 0F, 0F, -3F, 0F); // Box 0
		gunModel[112].setRotationPoint(-2.5F, -17F, -4.6F);


		defaultScopeModel = new ModelRendererTurbo[7];
		defaultScopeModel[0] = new ModelRendererTurbo(this, 216, 40, textureX, textureY); // Box 6
		defaultScopeModel[1] = new ModelRendererTurbo(this, 259, 59, textureX, textureY); // Box 6
		defaultScopeModel[2] = new ModelRendererTurbo(this, 216, 62, textureX, textureY); // Box 6
		defaultScopeModel[3] = new ModelRendererTurbo(this, 216, 53, textureX, textureY); // Box 6
		defaultScopeModel[4] = new ModelRendererTurbo(this, 54, 8, textureX, textureY); // Box 6
		defaultScopeModel[5] = new ModelRendererTurbo(this, 54, 8, textureX, textureY); // Box 6
		defaultScopeModel[6] = new ModelRendererTurbo(this, 59, 63, textureX, textureY); // Box 6

		defaultScopeModel[0].addBox(0F, 0F, 0F, 10, 5, 7, 0F); // Box 6
		defaultScopeModel[0].setRotationPoint(19F, -29F, -3.5F);

		defaultScopeModel[1].addShapeBox(0F, 0F, 0F, 4, 5, 7, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 6
		defaultScopeModel[1].setRotationPoint(15F, -29F, -3.5F);

		defaultScopeModel[2].addBox(0F, 0F, 0F, 14, 2, 7, 0F); // Box 6
		defaultScopeModel[2].setRotationPoint(15F, -24F, -3.5F);

		defaultScopeModel[3].addShapeBox(0F, 0F, 0F, 11, 1, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 6
		defaultScopeModel[3].setRotationPoint(18F, -30F, -3.5F);

		defaultScopeModel[4].addBox(0F, 0F, 0F, 2, 2, 8, 0F); // Box 6
		defaultScopeModel[4].setRotationPoint(16F, -23F, -4F);

		defaultScopeModel[5].addBox(0F, 0F, 0F, 2, 2, 8, 0F); // Box 6
		defaultScopeModel[5].setRotationPoint(26F, -23F, -4F);

		defaultScopeModel[6].addShapeBox(0F, 0F, 0F, 3, 8, 9, 0F, -2.25F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, -2.25F, 0F, -4F, 0F, -4F, 0F, -2.25F, -4F, 0F, -2.25F, -4F, -4F, 0F, -4F, -4F); // Box 6
		defaultScopeModel[6].setRotationPoint(15.5F, -29F, -2.5F);


		defaultStockModel = new ModelRendererTurbo[21];
		defaultStockModel[0] = new ModelRendererTurbo(this, 1, 92, textureX, textureY); // Box 30
		defaultStockModel[1] = new ModelRendererTurbo(this, 65, 153, textureX, textureY); // Box 30
		defaultStockModel[2] = new ModelRendererTurbo(this, 48, 107, textureX, textureY); // Box 30
		defaultStockModel[3] = new ModelRendererTurbo(this, 48, 100, textureX, textureY); // Box 30
		defaultStockModel[4] = new ModelRendererTurbo(this, 67, 121, textureX, textureY); // Box 30
		defaultStockModel[5] = new ModelRendererTurbo(this, 48, 137, textureX, textureY); // Box 30
		defaultStockModel[6] = new ModelRendererTurbo(this, 1, 165, textureX, textureY); // Box 30
		defaultStockModel[7] = new ModelRendererTurbo(this, 1, 85, textureX, textureY); // Box 30
		defaultStockModel[8] = new ModelRendererTurbo(this, 1, 178, textureX, textureY); // Box 30
		defaultStockModel[9] = new ModelRendererTurbo(this, 20, 145, textureX, textureY); // Box 30
		defaultStockModel[10] = new ModelRendererTurbo(this, 1, 130, textureX, textureY); // Box 30
		defaultStockModel[11] = new ModelRendererTurbo(this, 28, 156, textureX, textureY); // Box 30
		defaultStockModel[12] = new ModelRendererTurbo(this, 1, 145, textureX, textureY); // Box 30
		defaultStockModel[13] = new ModelRendererTurbo(this, 25, 131, textureX, textureY); // Box 30
		defaultStockModel[14] = new ModelRendererTurbo(this, 16, 132, textureX, textureY); // Box 30
		defaultStockModel[15] = new ModelRendererTurbo(this, 1, 125, textureX, textureY); // Box 30
		defaultStockModel[16] = new ModelRendererTurbo(this, 25, 140, textureX, textureY); // Box 30
		defaultStockModel[17] = new ModelRendererTurbo(this, 48, 123, textureX, textureY); // Box 30
		defaultStockModel[18] = new ModelRendererTurbo(this, 1, 156, textureX, textureY); // Box 30
		defaultStockModel[19] = new ModelRendererTurbo(this, 1, 114, textureX, textureY); // Box 30
		defaultStockModel[20] = new ModelRendererTurbo(this, 1, 100, textureX, textureY); // Box 30

		defaultStockModel[0].addBox(0F, 0F, 0F, 25, 2, 5, 0F); // Box 30
		defaultStockModel[0].setRotationPoint(-42F, -10.5F, -2.5F);

		defaultStockModel[1].addShapeBox(0F, 0F, 0F, 4, 2, 9, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 30
		defaultStockModel[1].setRotationPoint(-46F, -19.5F, -4.5F);

		defaultStockModel[2].addBox(0F, 0F, 0F, 4, 8, 7, 0F); // Box 30
		defaultStockModel[2].setRotationPoint(-46F, -10.5F, -3.5F);

		defaultStockModel[3].addShapeBox(0F, 0F, 0F, 12, 1, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 30
		defaultStockModel[3].setRotationPoint(-46F, 5.5F, -2.5F);

		defaultStockModel[4].addBox(0F, 0F, 0F, 4, 6, 9, 0F); // Box 30
		defaultStockModel[4].setRotationPoint(-46F, -17.5F, -4.5F);

		defaultStockModel[5].addShapeBox(0F, 0F, 0F, 4, 1, 7, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 30
		defaultStockModel[5].setRotationPoint(-46F, -11.5F, -3.5F);

		defaultStockModel[6].addBox(0F, 0F, 0F, 31, 5, 7, 0F); // Box 30
		defaultStockModel[6].setRotationPoint(-42F, -16.5F, -3.5F);

		defaultStockModel[7].addShapeBox(0F, 0F, 0F, 31, 1, 5, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 30
		defaultStockModel[7].setRotationPoint(-42F, -11.5F, -2.5F);

		defaultStockModel[8].addShapeBox(0F, 0F, 0F, 31, 2, 7, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 30
		defaultStockModel[8].setRotationPoint(-42F, -18.5F, -3.5F);

		defaultStockModel[9].addBox(0F, 0F, 0F, 8, 5, 5, 0F); // Box 30
		defaultStockModel[9].setRotationPoint(-42F, 0.5F, -2.5F);

		defaultStockModel[10].addBox(0F, 0F, 0F, 2, 9, 5, 0F); // Box 30
		defaultStockModel[10].setRotationPoint(-42F, -8.5F, -2.5F);

		defaultStockModel[11].addShapeBox(0F, 0F, 0F, 13, 3, 5, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 0F, 0F); // Box 30
		defaultStockModel[11].setRotationPoint(-34F, 0.5F, -2.5F);

		defaultStockModel[12].addBox(0F, 0F, 0F, 4, 5, 5, 0F); // Box 30
		defaultStockModel[12].setRotationPoint(-21F, -8.5F, -2.5F);

		defaultStockModel[13].addShapeBox(0F, 0F, 0F, 4, 3, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F); // Box 30
		defaultStockModel[13].setRotationPoint(-21F, -3.5F, -2.5F);

		defaultStockModel[14].addBox(0F, 0F, 0F, 1, 9, 3, 0F); // Box 30
		defaultStockModel[14].setRotationPoint(-40F, -8.5F, -1.5F);

		defaultStockModel[15].addBox(0F, 0F, 0F, 18, 1, 3, 0F); // Box 30
		defaultStockModel[15].setRotationPoint(-39F, -8.5F, -1.5F);

		defaultStockModel[16].addBox(0F, 0F, 0F, 8, 1, 3, 0F); // Box 30
		defaultStockModel[16].setRotationPoint(-39F, -0.5F, -1.5F);

		defaultStockModel[17].addBox(0F, 0F, 0F, 4, 8, 5, 0F); // Box 30
		defaultStockModel[17].setRotationPoint(-46F, -2.5F, -2.5F);

		defaultStockModel[18].addBox(0F, 0F, 0F, 6, 3, 4, 0F); // Box 30
		defaultStockModel[18].setRotationPoint(-17F, -11F, -2F);

		defaultStockModel[19].addShapeBox(0F, 0F, 0F, 15, 2, 8, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 30
		defaultStockModel[19].setRotationPoint(-34F, -19.5F, -4F);

		defaultStockModel[20].addBox(0F, 0F, 0F, 15, 5, 8, 0F); // Box 30
		defaultStockModel[20].setRotationPoint(-34F, -17.5F, -4F);


		ammoModel = new ModelRendererTurbo[7];
		ammoModel[0] = new ModelRendererTurbo(this, 1, 206, textureX, textureY); // ammoClip1
		ammoModel[1] = new ModelRendererTurbo(this, 1, 225, textureX, textureY); // bullet1
		ammoModel[2] = new ModelRendererTurbo(this, 30, 188, textureX, textureY); // Box 45
		ammoModel[3] = new ModelRendererTurbo(this, 1, 189, textureX, textureY); // Box 47
		ammoModel[4] = new ModelRendererTurbo(this, 1, 216, textureX, textureY); // Box 48
		ammoModel[5] = new ModelRendererTurbo(this, 40, 214, textureX, textureY); // Box 50
		ammoModel[6] = new ModelRendererTurbo(this, 55, 192, textureX, textureY); // Box 51

		ammoModel[0].addBox(0F, 0F, -3F, 15, 3, 6, 0F); // ammoClip1
		ammoModel[0].setRotationPoint(14F, -5F, 0F);
		ammoModel[0].rotateAngleZ = 0.08726646F;

		ammoModel[1].addShapeBox(1F, -1F, -2F, 13, 1, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // bullet1
		ammoModel[1].setRotationPoint(14F, -5F, 0F);
		ammoModel[1].rotateAngleZ = 0.08726646F;

		ammoModel[2].addBox(0F, 3F, -3F, 6, 11, 6, 0F); // Box 45
		ammoModel[2].setRotationPoint(14F, -5F, 0F);
		ammoModel[2].rotateAngleZ = 0.08726646F;

		ammoModel[3].addBox(5.5F, 3F, -2.5F, 9, 11, 5, 0F); // Box 47
		ammoModel[3].setRotationPoint(14F, -5F, 0F);
		ammoModel[3].rotateAngleZ = 0.08726646F;

		ammoModel[4].addShapeBox(0.5F, 14F, -2.5F, 14, 3, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F); // Box 48
		ammoModel[4].setRotationPoint(14F, -5F, 0F);
		ammoModel[4].rotateAngleZ = 0.08726646F;

		ammoModel[5].addShapeBox(6F, 10F, -3F, 3, 4, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F); // Box 50
		ammoModel[5].setRotationPoint(14F, -5F, 0F);
		ammoModel[5].rotateAngleZ = 0.08726646F;

		ammoModel[6].addBox(6F, 3F, -3F, 3, 7, 6, 0F); // Box 51
		ammoModel[6].setRotationPoint(14F, -5F, 0F);
		ammoModel[6].rotateAngleZ = 0.08726646F;

		stockAttachPoint = new Vector3f(-12F /16F, 16F /16F, 0F /16F);

		gunSlideDistance = 0F;
		animationType = EnumAnimationType.CUSTOM;

		/* ----Start of Reload Block---- */
		/* Reload Name: Amended Bottom Clip */
		translateGun = new Vector3f(0F, 0.1F, 0F);
		translateClip = new Vector3f(0F, -3F, 0F);
		/* ----End of Reload Block---- */
		
		flipAll();
		
		translateAll(0F, 0F, 0F);
	}
}
