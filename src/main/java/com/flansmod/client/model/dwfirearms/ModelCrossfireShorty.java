package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelCrossfireShorty extends ModelGun
{
	int textureX = 512;
	int textureY = 256;

	public ModelCrossfireShorty()
	{
		gunModel = new ModelRendererTurbo[76];
		gunModel[0] = new ModelRendererTurbo(this, 1, 16, textureX, textureY); // Box 32
		gunModel[1] = new ModelRendererTurbo(this, 59, 29, textureX, textureY); // Box 33
		gunModel[2] = new ModelRendererTurbo(this, 1, 28, textureX, textureY); // Box 34
		gunModel[3] = new ModelRendererTurbo(this, 1, 40, textureX, textureY); // Box 35
		gunModel[4] = new ModelRendererTurbo(this, 1, 54, textureX, textureY); // Box 36
		gunModel[5] = new ModelRendererTurbo(this, 1, 70, textureX, textureY); // Box 38
		gunModel[6] = new ModelRendererTurbo(this, 38, 25, textureX, textureY); // Box 39
		gunModel[7] = new ModelRendererTurbo(this, 38, 40, textureX, textureY); // Box 40
		gunModel[8] = new ModelRendererTurbo(this, 38, 54, textureX, textureY); // Box 41
		gunModel[9] = new ModelRendererTurbo(this, 38, 70, textureX, textureY); // Box 42
		gunModel[10] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // railPart1
		gunModel[11] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // railPart2
		gunModel[12] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // railPart3
		gunModel[13] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // railPart4
		gunModel[14] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // railPart5
		gunModel[15] = new ModelRendererTurbo(this, 28, 1, textureX, textureY); // railBlock
		gunModel[16] = new ModelRendererTurbo(this, 229, 42, textureX, textureY); // foreRail1
		gunModel[17] = new ModelRendererTurbo(this, 231, 109, textureX, textureY); // barrelFrontRight
		gunModel[18] = new ModelRendererTurbo(this, 231, 109, textureX, textureY); // barrelFrontLeft
		gunModel[19] = new ModelRendererTurbo(this, 231, 100, textureX, textureY); // barrelFrontMiddle
		gunModel[20] = new ModelRendererTurbo(this, 59, 66, textureX, textureY); // barrelEndRight
		gunModel[21] = new ModelRendererTurbo(this, 68, 65, textureX, textureY); // barrelEndMiddle
		gunModel[22] = new ModelRendererTurbo(this, 59, 66, textureX, textureY); // barrelEndLeft
		gunModel[23] = new ModelRendererTurbo(this, 151, 127, textureX, textureY); // foreRail2
		gunModel[24] = new ModelRendererTurbo(this, 151, 139, textureX, textureY); // foreRail3
		gunModel[25] = new ModelRendererTurbo(this, 120, 65, textureX, textureY); // body4
		gunModel[26] = new ModelRendererTurbo(this, 1, 93, textureX, textureY); // shellPart1
		gunModel[27] = new ModelRendererTurbo(this, 1, 93, textureX, textureY); // shellPart2
		gunModel[28] = new ModelRendererTurbo(this, 1, 93, textureX, textureY); // shellPart3
		gunModel[29] = new ModelRendererTurbo(this, 120, 28, textureX, textureY); // Box 0
		gunModel[30] = new ModelRendererTurbo(this, 120, 116, textureX, textureY); // Box 2
		gunModel[31] = new ModelRendererTurbo(this, 120, 105, textureX, textureY); // Box 3
		gunModel[32] = new ModelRendererTurbo(this, 120, 56, textureX, textureY); // Box 4
		gunModel[33] = new ModelRendererTurbo(this, 169, 92, textureX, textureY); // Box 6
		gunModel[34] = new ModelRendererTurbo(this, 1, 81, textureX, textureY); // Box 9
		gunModel[35] = new ModelRendererTurbo(this, 162, 81, textureX, textureY); // Box 10
		gunModel[36] = new ModelRendererTurbo(this, 120, 149, textureX, textureY); // Box 11
		gunModel[37] = new ModelRendererTurbo(this, 198, 86, textureX, textureY); // Box 12
		gunModel[38] = new ModelRendererTurbo(this, 40, 86, textureX, textureY); // Box 13
		gunModel[39] = new ModelRendererTurbo(this, 120, 105, textureX, textureY); // Box 0
		gunModel[40] = new ModelRendererTurbo(this, 120, 90, textureX, textureY); // Box 1
		gunModel[41] = new ModelRendererTurbo(this, 120, 42, textureX, textureY); // Box 2
		gunModel[42] = new ModelRendererTurbo(this, 145, 83, textureX, textureY); // Box 7
		gunModel[43] = new ModelRendererTurbo(this, 231, 82, textureX, textureY); // Box 8
		gunModel[44] = new ModelRendererTurbo(this, 231, 91, textureX, textureY); // Box 9
		gunModel[45] = new ModelRendererTurbo(this, 231, 82, textureX, textureY); // Box 10
		gunModel[46] = new ModelRendererTurbo(this, 174, 135, textureX, textureY); // Box 11
		gunModel[47] = new ModelRendererTurbo(this, 228, 19, textureX, textureY); // Box 14
		gunModel[48] = new ModelRendererTurbo(this, 229, 29, textureX, textureY); // Box 15
		gunModel[49] = new ModelRendererTurbo(this, 229, 56, textureX, textureY); // Box 16
		gunModel[50] = new ModelRendererTurbo(this, 199, 138, textureX, textureY); // Box 17
		gunModel[51] = new ModelRendererTurbo(this, 174, 121, textureX, textureY); // Box 18
		gunModel[52] = new ModelRendererTurbo(this, 174, 109, textureX, textureY); // Box 19
		gunModel[53] = new ModelRendererTurbo(this, 59, 44, textureX, textureY); // Box 23
		gunModel[54] = new ModelRendererTurbo(this, 68, 43, textureX, textureY); // Box 24
		gunModel[55] = new ModelRendererTurbo(this, 59, 44, textureX, textureY); // Box 25
		gunModel[56] = new ModelRendererTurbo(this, 73, 117, textureX, textureY); // Box 26
		gunModel[57] = new ModelRendererTurbo(this, 48, 103, textureX, textureY); // Box 27
		gunModel[58] = new ModelRendererTurbo(this, 48, 117, textureX, textureY); // Box 28
		gunModel[59] = new ModelRendererTurbo(this, 73, 103, textureX, textureY); // Box 29
		gunModel[60] = new ModelRendererTurbo(this, 1, 117, textureX, textureY); // Box 30
		gunModel[61] = new ModelRendererTurbo(this, 1, 102, textureX, textureY); // Box 31
		gunModel[62] = new ModelRendererTurbo(this, 1, 129, textureX, textureY); // Box 34
		gunModel[63] = new ModelRendererTurbo(this, 120, 127, textureX, textureY); // Box 39
		gunModel[64] = new ModelRendererTurbo(this, 120, 139, textureX, textureY); // Box 40
		gunModel[65] = new ModelRendererTurbo(this, 120, 83, textureX, textureY); // Box 41
		gunModel[66] = new ModelRendererTurbo(this, 192, 80, textureX, textureY); // Box 42
		gunModel[67] = new ModelRendererTurbo(this, 183, 80, textureX, textureY); // Box 43
		gunModel[68] = new ModelRendererTurbo(this, 120, 77, textureX, textureY); // Box 44
		gunModel[69] = new ModelRendererTurbo(this, 145, 77, textureX, textureY); // Box 45
		gunModel[70] = new ModelRendererTurbo(this, 229, 73, textureX, textureY); // Box 46
		gunModel[71] = new ModelRendererTurbo(this, 229, 67, textureX, textureY); // Box 47
		gunModel[72] = new ModelRendererTurbo(this, 99, 18, textureX, textureY); // Box 49
		gunModel[73] = new ModelRendererTurbo(this, 99, 23, textureX, textureY); // Box 52
		gunModel[74] = new ModelRendererTurbo(this, 120, 165, textureX, textureY); // Box 0
		gunModel[75] = new ModelRendererTurbo(this, 161, 154, textureX, textureY); // Box 4

		gunModel[0].addShapeBox(0F, 0F, 0F, 13, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F); // Box 32
		gunModel[0].setRotationPoint(-11F, -5F, -4F);

		gunModel[1].addBox(0F, 0F, 0F, 6, 2, 8, 0F); // Box 33
		gunModel[1].setRotationPoint(-11F, -7F, -4F);

		gunModel[2].addShapeBox(0F, 0F, 0F, 10, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 34
		gunModel[2].setRotationPoint(-8F, -2F, -4F);

		gunModel[3].addShapeBox(0F, 0F, 0F, 10, 5, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 3F, 0F, 0F); // Box 35
		gunModel[3].setRotationPoint(-8F, 1F, -4F);

		gunModel[4].addShapeBox(0F, 0F, 0F, 10, 7, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, -1F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 4F, -1F, 0F); // Box 36
		gunModel[4].setRotationPoint(-11F, 6F, -4F);

		gunModel[5].addShapeBox(0F, 0F, 0F, 10, 2, 8, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F); // Box 38
		gunModel[5].setRotationPoint(-15F, 13F, -4F);

		gunModel[6].addShapeBox(0F, 0F, 0F, 2, 6, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F); // Box 39
		gunModel[6].setRotationPoint(2F, -5F, -4F);

		gunModel[7].addShapeBox(0F, 0F, 0F, 2, 5, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, -1.5F, -3F, 0F, -1.5F, 3F, 0F, 0F); // Box 40
		gunModel[7].setRotationPoint(2F, 1F, -4F);

		gunModel[8].addShapeBox(0F, 0F, 0F, 2, 7, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 4F, 0F, 0F, -4F, 0F, -1.5F, -4F, 0F, -1.5F, 4F, 0F, 0F); // Box 41
		gunModel[8].setRotationPoint(-1F, 6F, -4F);

		gunModel[9].addShapeBox(0F, 0F, 0F, 2, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F); // Box 42
		gunModel[9].setRotationPoint(-5F, 13F, -4F);

		gunModel[10].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // railPart1
		gunModel[10].setRotationPoint(0F, -24F, -3.5F);

		gunModel[11].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // railPart2
		gunModel[11].setRotationPoint(6F, -24F, -3.5F);

		gunModel[12].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // railPart3
		gunModel[12].setRotationPoint(12F, -24F, -3.5F);

		gunModel[13].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // railPart4
		gunModel[13].setRotationPoint(18F, -24F, -3.5F);

		gunModel[14].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // railPart5
		gunModel[14].setRotationPoint(24F, -24F, -3.5F);

		gunModel[15].addBox(0F, 0F, 0F, 27, 3, 7, 0F); // railBlock
		gunModel[15].setRotationPoint(0F, -22F, -3.5F);

		gunModel[16].addShapeBox(0F, 0F, 0F, 24, 4, 9, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // foreRail1
		gunModel[16].setRotationPoint(38F, -16F, -4.5F);

		gunModel[17].addShapeBox(0F, 0F, 0F, 33, 6, 2, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // barrelFrontRight
		gunModel[17].setRotationPoint(32F, -20F, -3F);

		gunModel[18].addShapeBox(0F, 0F, 0F, 33, 6, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F); // barrelFrontLeft
		gunModel[18].setRotationPoint(32F, -20F, 1F);

		gunModel[19].addBox(0F, 0F, 0F, 33, 6, 2, 0F); // barrelFrontMiddle
		gunModel[19].setRotationPoint(32F, -20F, -1F);

		gunModel[20].addShapeBox(0F, 0F, 0F, 2, 7, 2, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // barrelEndRight
		gunModel[20].setRotationPoint(62F, -20.5F, -3.5F);

		gunModel[21].addBox(0F, 0F, 0F, 2, 7, 3, 0F); // barrelEndMiddle
		gunModel[21].setRotationPoint(62F, -20.5F, -1.5F);

		gunModel[22].addShapeBox(0F, 0F, 0F, 2, 7, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F); // barrelEndLeft
		gunModel[22].setRotationPoint(62F, -20.5F, 1.5F);

		gunModel[23].addBox(0F, 0F, 0F, 4, 4, 7, 0F); // foreRail2
		gunModel[23].setRotationPoint(58F, -11F, -3.5F);

		gunModel[24].addShapeBox(0F, 0F, 0F, 4, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // foreRail3
		gunModel[24].setRotationPoint(58F, -7F, -3.5F);

		gunModel[25].addBox(0F, 0F, 0F, 37, 2, 9, 0F); // body4
		gunModel[25].setRotationPoint(-5F, -7F, -4.5F);

		gunModel[26].addShapeBox(0F, 0F, 0F, 12, 6, 2, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // shellPart1
		gunModel[26].setRotationPoint(16F, -12F, -3F);

		gunModel[27].addBox(0F, 0F, 0F, 12, 6, 2, 0F); // shellPart2
		gunModel[27].setRotationPoint(16F, -12F, -1F);

		gunModel[28].addShapeBox(0F, 0F, 0F, 12, 6, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F); // shellPart3
		gunModel[28].setRotationPoint(16F, -12F, 1F);

		gunModel[29].addBox(0F, 0F, 0F, 30, 4, 9, 0F); // Box 0
		gunModel[29].setRotationPoint(-15F, -11F, -4.5F);

		gunModel[30].addBox(0F, 0F, 0F, 16, 2, 7, 0F); // Box 2
		gunModel[30].setRotationPoint(-15F, -14F, -3.5F);

		gunModel[31].addShapeBox(0F, 0F, 0F, 16, 1, 9, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 3
		gunModel[31].setRotationPoint(-15F, -12F, -4.5F);

		gunModel[32].addShapeBox(0F, 0F, 0F, 42, 1, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 4
		gunModel[32].setRotationPoint(-10F, -21F, -3.5F);

		gunModel[33].addShapeBox(0F, 0F, 0F, 5, 3, 9, 0F, -2F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, -2F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 6
		gunModel[33].setRotationPoint(-15F, -20F, -4.5F);

		gunModel[34].addShapeBox(0F, 0F, 0F, 10, 2, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, -1F, 0F, -1F); // Box 9
		gunModel[34].setRotationPoint(-15F, -7F, -4.5F);

		gunModel[35].addShapeBox(0F, 0F, 0F, 3, 1, 7, 0F, -1F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, -1F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 10
		gunModel[35].setRotationPoint(-13F, -21F, -3.5F);

		gunModel[36].addBox(0F, 0F, 0F, 14, 9, 6, 0F); // Box 11
		gunModel[36].setRotationPoint(15F, -16F, -2.5F);

		gunModel[37].addBox(0F, 0F, 0F, 3, 9, 9, 0F); // Box 12
		gunModel[37].setRotationPoint(29F, -16F, -4.5F);

		gunModel[38].addShapeBox(0F, 0F, 0F, 14, 5, 1, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 13
		gunModel[38].setRotationPoint(15F, -16F, -4.25F);

		gunModel[39].addShapeBox(0F, 0F, 0F, 16, 1, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 0
		gunModel[39].setRotationPoint(-15F, -15F, -4.5F);

		gunModel[40].addBox(0F, 0F, 0F, 14, 5, 9, 0F); // Box 1
		gunModel[40].setRotationPoint(1F, -16F, -4.5F);

		gunModel[41].addShapeBox(0F, 0F, 0F, 42, 3, 9, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 2
		gunModel[41].setRotationPoint(-10F, -20F, -4.5F);

		gunModel[42].addBox(0F, 0F, 0F, 2, 4, 2, 0F); // Box 7
		gunModel[42].setRotationPoint(15F, -16F, -4.5F);

		gunModel[43].addShapeBox(0F, 0F, 0F, 26, 6, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F); // Box 8
		gunModel[43].setRotationPoint(32F, -12F, 1F);

		gunModel[44].addBox(0F, 0F, 0F, 26, 6, 2, 0F); // Box 9
		gunModel[44].setRotationPoint(32F, -12F, -1F);

		gunModel[45].addShapeBox(0F, 0F, 0F, 26, 6, 2, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 10
		gunModel[45].setRotationPoint(32F, -12F, -3F);

		gunModel[46].addShapeBox(0F, 0F, 0F, 3, 4, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F); // Box 11
		gunModel[46].setRotationPoint(32F, -16F, -4.5F);

		gunModel[47].addShapeBox(0F, 0F, 0F, 27, 1, 8, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 14
		gunModel[47].setRotationPoint(35F, -21F, -4F);

		gunModel[48].addBox(0F, 0F, 0F, 27, 4, 8, 0F); // Box 15
		gunModel[48].setRotationPoint(35F, -20F, -4F);

		gunModel[49].addShapeBox(0F, 0F, 0F, 24, 1, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, -1F, 0F, -1F); // Box 16
		gunModel[49].setRotationPoint(38F, -12F, -4.5F);

		gunModel[50].addShapeBox(0F, 0F, 0F, 6, 1, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 1F, 0F, -1F, 1F, 0F, -1F, 0F, 0F, -1F); // Box 17
		gunModel[50].setRotationPoint(32F, -12F, -4.5F);

		gunModel[51].addBox(0F, 0F, 0F, 3, 4, 9, 0F); // Box 18
		gunModel[51].setRotationPoint(32F, -20F, -4.5F);

		gunModel[52].addShapeBox(0F, 0F, 0F, 3, 2, 9, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 19
		gunModel[52].setRotationPoint(32F, -22F, -4.5F);

		gunModel[53].addShapeBox(0F, 0F, 0F, 2, 7, 2, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 23
		gunModel[53].setRotationPoint(62F, -12.5F, -3.5F);

		gunModel[54].addBox(0F, 0F, 0F, 2, 7, 3, 0F); // Box 24
		gunModel[54].setRotationPoint(62F, -12.5F, -1.5F);

		gunModel[55].addShapeBox(0F, 0F, 0F, 2, 7, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F); // Box 25
		gunModel[55].setRotationPoint(62F, -12.5F, 1.5F);

		gunModel[56].addShapeBox(0F, 0F, 0F, 3, 2, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 26
		gunModel[56].setRotationPoint(54F, -7F, -4.5F);

		gunModel[57].addBox(0F, 0F, 0F, 3, 4, 9, 0F); // Box 27
		gunModel[57].setRotationPoint(54F, -11F, -4.5F);

		gunModel[58].addShapeBox(0F, 0F, 0F, 3, 2, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 28
		gunModel[58].setRotationPoint(39F, -7F, -4.5F);

		gunModel[59].addBox(0F, 0F, 0F, 3, 4, 9, 0F); // Box 29
		gunModel[59].setRotationPoint(39F, -11F, -4.5F);

		gunModel[60].addShapeBox(0F, 0F, 0F, 12, 2, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 30
		gunModel[60].setRotationPoint(42F, -6F, -4.5F);

		gunModel[61].addBox(0F, 0F, 0F, 12, 5, 9, 0F); // Box 31
		gunModel[61].setRotationPoint(42F, -11F, -4.5F);

		gunModel[62].addShapeBox(0F, 0F, 0F, 18, 2, 9, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 34
		gunModel[62].setRotationPoint(39F, -13F, -4.5F);

		gunModel[63].addBox(0F, 0F, 0F, 7, 4, 7, 0F); // Box 39
		gunModel[63].setRotationPoint(32F, -11F, -3.5F);

		gunModel[64].addShapeBox(0F, 0F, 0F, 7, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 40
		gunModel[64].setRotationPoint(32F, -7F, -3.5F);

		gunModel[65].addShapeBox(0F, 0F, 0F, 10, 4, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F); // Box 41
		gunModel[65].setRotationPoint(17F, -11F, 3.5F);

		gunModel[66].addShapeBox(0F, 0F, 0F, 2, 7, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F); // Box 42
		gunModel[66].setRotationPoint(15F, -14F, 3.5F);

		gunModel[67].addShapeBox(0F, 0F, 0F, 2, 7, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F); // Box 43
		gunModel[67].setRotationPoint(27F, -14F, 3.5F);

		gunModel[68].addBox(0F, 0F, 0F, 10, 1, 2, 0F); // Box 44
		gunModel[68].setRotationPoint(17F, -11.5F, 3.3F);

		gunModel[69].addShapeBox(0F, 0F, 0F, 10, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 45
		gunModel[69].setRotationPoint(17F, -12.5F, 3.3F);

		gunModel[70].addBox(0F, 0F, 0F, 14, 4, 2, 0F); // Box 46
		gunModel[70].setRotationPoint(15F, -11F, -4.5F);

		gunModel[71].addBox(0F, 0F, 0F, 14, 3, 2, 0F); // Box 47
		gunModel[71].setRotationPoint(15F, -16F, 2.5F);

		gunModel[72].addShapeBox(0F, 0F, 0F, 8, 3, 1, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 49
		gunModel[72].setRotationPoint(54F, -24F, -3F);

		gunModel[73].addShapeBox(0F, 0F, 0F, 8, 3, 1, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 52
		gunModel[73].setRotationPoint(54F, -24F, 2F);

		gunModel[74].addBox(0F, 0F, 0F, 47, 1, 9, 0F); // Box 0
		gunModel[74].setRotationPoint(-15F, -17F, -4.5F);

		gunModel[75].addBox(0F, 0F, 0F, 16, 1, 9, 0F); // Box 4
		gunModel[75].setRotationPoint(-15F, -16F, -4.5F);


		defaultScopeModel = new ModelRendererTurbo[3];
		defaultScopeModel[0] = new ModelRendererTurbo(this, 88, 18, textureX, textureY); // Box 51
		defaultScopeModel[1] = new ModelRendererTurbo(this, 88, 31, textureX, textureY); // Box 53
		defaultScopeModel[2] = new ModelRendererTurbo(this, 88, 31, textureX, textureY); // Box 54

		defaultScopeModel[0].addShapeBox(0F, 0F, 0F, 3, 7, 2, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 51
		defaultScopeModel[0].setRotationPoint(56.5F, -28F, -1F);

		defaultScopeModel[1].addShapeBox(0F, 0F, 0F, 2, 1, 7, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 53
		defaultScopeModel[1].setRotationPoint(57F, -23F, -3.5F);

		defaultScopeModel[2].addShapeBox(0F, 0F, 0F, 2, 1, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 54
		defaultScopeModel[2].setRotationPoint(57F, -22F, -3.5F);


		defaultStockModel = new ModelRendererTurbo[18];
		defaultStockModel[0] = new ModelRendererTurbo(this, 55, 147, textureX, textureY); // Box 13
		defaultStockModel[1] = new ModelRendererTurbo(this, 55, 147, textureX, textureY); // Box 13
		defaultStockModel[2] = new ModelRendererTurbo(this, 55, 147, textureX, textureY); // Box 13
		defaultStockModel[3] = new ModelRendererTurbo(this, 55, 147, textureX, textureY); // Box 13
		defaultStockModel[4] = new ModelRendererTurbo(this, 36, 141, textureX, textureY); // Box 13
		defaultStockModel[5] = new ModelRendererTurbo(this, 1, 154, textureX, textureY); // Box 13
		defaultStockModel[6] = new ModelRendererTurbo(this, 1, 141, textureX, textureY); // Box 13
		defaultStockModel[7] = new ModelRendererTurbo(this, 1, 163, textureX, textureY); // Box 13
		defaultStockModel[8] = new ModelRendererTurbo(this, 59, 163, textureX, textureY); // Box 13
		defaultStockModel[9] = new ModelRendererTurbo(this, 36, 141, textureX, textureY); // Box 13
		defaultStockModel[10] = new ModelRendererTurbo(this, 58, 141, textureX, textureY); // Box 13
		defaultStockModel[11] = new ModelRendererTurbo(this, 58, 141, textureX, textureY); // Box 13
		defaultStockModel[12] = new ModelRendererTurbo(this, 36, 147, textureX, textureY); // Box 13
		defaultStockModel[13] = new ModelRendererTurbo(this, 36, 147, textureX, textureY); // Box 13
		defaultStockModel[14] = new ModelRendererTurbo(this, 26, 163, textureX, textureY); // Box 13
		defaultStockModel[15] = new ModelRendererTurbo(this, 88, 160, textureX, textureY); // Box 13
		defaultStockModel[16] = new ModelRendererTurbo(this, 1, 176, textureX, textureY); // Box 13
		defaultStockModel[17] = new ModelRendererTurbo(this, 59, 151, textureX, textureY); // Box 13

		defaultStockModel[0].addShapeBox(0F, 0F, 0F, 24, 2, 1, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 13
		defaultStockModel[0].setRotationPoint(-23F, -14F, -5.5F);

		defaultStockModel[1].addShapeBox(0F, 0F, 0F, 24, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 13
		defaultStockModel[1].setRotationPoint(-23F, -14F, -4.5F);

		defaultStockModel[2].addShapeBox(0F, 0F, 0F, 24, 2, 1, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 13
		defaultStockModel[2].setRotationPoint(-23F, -14F, 3.5F);

		defaultStockModel[3].addShapeBox(0F, 0F, 0F, 24, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 13
		defaultStockModel[3].setRotationPoint(-23F, -14F, 4.5F);

		defaultStockModel[4].addShapeBox(0F, 0F, 0F, 8, 3, 2, 0F, 0F, -0.75F, -0.5F, 0F, -0.75F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.75F, -0.5F, 0F, -0.75F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 13
		defaultStockModel[4].setRotationPoint(-23F, -14.5F, -6.5F);

		defaultStockModel[5].addBox(0F, 0F, 0F, 8, 2, 6, 0F); // Box 13
		defaultStockModel[5].setRotationPoint(-23F, -13.5F, -3F);

		defaultStockModel[6].addShapeBox(0F, 0F, 0F, 8, 3, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 13
		defaultStockModel[6].setRotationPoint(-23F, -11.5F, -4.5F);

		defaultStockModel[7].addShapeBox(0F, 0F, 0F, 3, 3, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 13
		defaultStockModel[7].setRotationPoint(-26F, -11.5F, -4.5F);

		defaultStockModel[8].addShapeBox(0F, 0F, 0F, 5, 15, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F); // Box 13
		defaultStockModel[8].setRotationPoint(-30F, -11.5F, -4.5F);

		defaultStockModel[9].addShapeBox(0F, 0F, 0F, 8, 3, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, -0.5F, 0F, -1F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 13
		defaultStockModel[9].setRotationPoint(-23F, -14.5F, -4.5F);

		defaultStockModel[10].addShapeBox(0F, 0F, 0F, 8, 3, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.75F, -0.5F, 0F, -0.75F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.75F, -0.5F, 0F, -0.75F, -0.5F); // Box 13
		defaultStockModel[10].setRotationPoint(-23F, -14.5F, 4.5F);

		defaultStockModel[11].addShapeBox(0F, 0F, 0F, 8, 3, 2, 0F, 0F, -1F, -0.5F, 0F, -1F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 13
		defaultStockModel[11].setRotationPoint(-23F, -14.5F, 2.5F);

		defaultStockModel[12].addShapeBox(0F, 0F, 0F, 7, 3, 2, 0F, 0F, -0.75F, -0.5F, 0F, -0.75F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.75F, -0.5F, 0F, -0.75F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 13
		defaultStockModel[12].setRotationPoint(-30F, -14.5F, -6.5F);

		defaultStockModel[13].addShapeBox(0F, 0F, 0F, 7, 3, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.75F, -0.5F, 0F, -0.75F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.75F, -0.5F, 0F, -0.75F, -0.5F); // Box 13
		defaultStockModel[13].setRotationPoint(-30F, -14.5F, 4.5F);

		defaultStockModel[14].addBox(0F, 0F, 0F, 7, 3, 9, 0F); // Box 13
		defaultStockModel[14].setRotationPoint(-30F, -14.5F, -4.5F);

		defaultStockModel[15].addBox(0F, 0F, 0F, 5, 18, 9, 0F); // Box 13
		defaultStockModel[15].setRotationPoint(-35F, -14.5F, -4.5F);

		defaultStockModel[16].addShapeBox(0F, 0F, 0F, 12, 2, 9, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 13
		defaultStockModel[16].setRotationPoint(-35F, -16.5F, -4.5F);

		defaultStockModel[17].addShapeBox(0F, 0F, 0F, 6, 2, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, -1F, 0F, -2F, -1F, 0F, -2F, 0F, 0F, -2F); // Box 13
		defaultStockModel[17].setRotationPoint(-35F, 3.5F, -4.5F);

		barrelAttachPoint = new Vector3f(97F /16F, 16.5F /16F, 0F /16F);
		stockAttachPoint = new Vector3f(-15F /16F, 13F /16F, 0F /16F);
		scopeAttachPoint = new Vector3f(10F /16F, 20F /16F, 0F /16F);
		gripAttachPoint = new Vector3f(0F, 0F, 0F);

		//pumpDelay = 10;
		//pumpTime = 5;
		//pumpDelayAfterReload = 118;

		//pumpHandleDistance = 1.2F;

		numBulletsInReloadAnimation = 4;
		tiltGunTime = 0.160F;
		unloadClipTime = 0.0F;
		loadClipTime = 0.700F;
		untiltGunTime = 0.140F;

		gunSlideDistance = 0F;
		animationType = EnumAnimationType.SHOTGUN;

		flipAll();

		translateAll(0F, 0F, 0F);
	}
}