package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.ModelAttachment;
import com.flansmod.client.tmt.ModelRendererTurbo;

import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelABTApertureSight extends ModelAttachment
{
	int textureX = 512;
	int textureY = 256;

	public ModelABTApertureSight()
	{
		attachmentModel = new ModelRendererTurbo[46];
		attachmentModel[0] = new ModelRendererTurbo(this, 96, 1, textureX, textureY); // mounter
		attachmentModel[1] = new ModelRendererTurbo(this, 124, 18, textureX, textureY); // mounterBase2
		attachmentModel[2] = new ModelRendererTurbo(this, 96, 30, textureX, textureY); // smallScope1
		attachmentModel[3] = new ModelRendererTurbo(this, 96, 30, textureX, textureY); // smallScope1
		attachmentModel[4] = new ModelRendererTurbo(this, 109, 30, textureX, textureY); // smallScope2
		attachmentModel[5] = new ModelRendererTurbo(this, 109, 30, textureX, textureY); // smallScope2
		attachmentModel[6] = new ModelRendererTurbo(this, 109, 30, textureX, textureY); // smallScope2
		attachmentModel[7] = new ModelRendererTurbo(this, 96, 12, textureX, textureY); // mounterBase3
		attachmentModel[8] = new ModelRendererTurbo(this, 124, 18, textureX, textureY); // mounterBase2
		attachmentModel[9] = new ModelRendererTurbo(this, 96, 18, textureX, textureY); // ironSight2
		attachmentModel[10] = new ModelRendererTurbo(this, 116, 30, textureX, textureY); // largeScope1
		attachmentModel[11] = new ModelRendererTurbo(this, 129, 30, textureX, textureY); // largeScope2
		attachmentModel[12] = new ModelRendererTurbo(this, 116, 30, textureX, textureY); // largeScope1
		attachmentModel[13] = new ModelRendererTurbo(this, 129, 30, textureX, textureY); // largeScope2
		attachmentModel[14] = new ModelRendererTurbo(this, 129, 30, textureX, textureY); // largeScope2
		attachmentModel[15] = new ModelRendererTurbo(this, 119, 18, textureX, textureY); // ironSight1
		attachmentModel[16] = new ModelRendererTurbo(this, 129, 30, textureX, textureY); // largeScope2
		attachmentModel[17] = new ModelRendererTurbo(this, 116, 30, textureX, textureY); // largeScope1
		attachmentModel[18] = new ModelRendererTurbo(this, 129, 30, textureX, textureY); // largeScope2
		attachmentModel[19] = new ModelRendererTurbo(this, 96, 30, textureX, textureY); // smallScope1
		attachmentModel[20] = new ModelRendererTurbo(this, 109, 30, textureX, textureY); // smallScope2
		attachmentModel[21] = new ModelRendererTurbo(this, 109, 30, textureX, textureY); // smallScope2
		attachmentModel[22] = new ModelRendererTurbo(this, 96, 30, textureX, textureY); // smallScope1
		attachmentModel[23] = new ModelRendererTurbo(this, 116, 30, textureX, textureY); // largeScope1
		attachmentModel[24] = new ModelRendererTurbo(this, 109, 30, textureX, textureY); // Box 38
		attachmentModel[25] = new ModelRendererTurbo(this, 129, 30, textureX, textureY); // Box 39
		attachmentModel[26] = new ModelRendererTurbo(this, 129, 30, textureX, textureY); // Box 40
		attachmentModel[27] = new ModelRendererTurbo(this, 109, 30, textureX, textureY); // Box 41
		attachmentModel[28] = new ModelRendererTurbo(this, 109, 30, textureX, textureY); // Box 42
		attachmentModel[29] = new ModelRendererTurbo(this, 129, 30, textureX, textureY); // Box 43
		attachmentModel[30] = new ModelRendererTurbo(this, 129, 30, textureX, textureY); // Box 44
		attachmentModel[31] = new ModelRendererTurbo(this, 129, 30, textureX, textureY); // Box 45
		attachmentModel[32] = new ModelRendererTurbo(this, 129, 30, textureX, textureY); // Box 46
		attachmentModel[33] = new ModelRendererTurbo(this, 129, 30, textureX, textureY); // Box 47
		attachmentModel[34] = new ModelRendererTurbo(this, 109, 30, textureX, textureY); // Box 48
		attachmentModel[35] = new ModelRendererTurbo(this, 109, 30, textureX, textureY); // Box 49
		attachmentModel[36] = new ModelRendererTurbo(this, 109, 30, textureX, textureY); // Box 50
		attachmentModel[37] = new ModelRendererTurbo(this, 109, 30, textureX, textureY); // Box 51
		attachmentModel[38] = new ModelRendererTurbo(this, 96, 30, textureX, textureY); // Box 52
		attachmentModel[39] = new ModelRendererTurbo(this, 109, 30, textureX, textureY); // Box 53
		attachmentModel[40] = new ModelRendererTurbo(this, 109, 30, textureX, textureY); // Box 54
		attachmentModel[41] = new ModelRendererTurbo(this, 109, 30, textureX, textureY); // Box 55
		attachmentModel[42] = new ModelRendererTurbo(this, 109, 30, textureX, textureY); // Box 56
		attachmentModel[43] = new ModelRendererTurbo(this, 96, 30, textureX, textureY); // Box 57
		attachmentModel[44] = new ModelRendererTurbo(this, 109, 30, textureX, textureY); // Box 58
		attachmentModel[45] = new ModelRendererTurbo(this, 109, 30, textureX, textureY); // Box 59

		attachmentModel[0].addBox(-9F, -3F, -3.5F, 18, 3, 7, 0F); // mounter
		attachmentModel[0].setRotationPoint(0F, 0F, 0F);

		attachmentModel[1].addBox(-3F, -4F, -3F, 6, 1, 1, 0F); // mounterBase2
		attachmentModel[1].setRotationPoint(0F, 0F, 0F);

		attachmentModel[2].addShapeBox(-12F, -14F, -2F, 2, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F); // smallScope1
		attachmentModel[2].setRotationPoint(0F, 0F, 0F);

		attachmentModel[3].addShapeBox(-12F, -5F, -2F, 2, 1, 4, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // smallScope1
		attachmentModel[3].setRotationPoint(0F, 0F, 0F);

		attachmentModel[4].addShapeBox(-12F, -14F, 1F, 2, 4, 1, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -3F, 0F, -0.5F, -3F, 0F, -1F, 3F, 0F, -1F, 3F); // smallScope2
		attachmentModel[4].setRotationPoint(0F, 0F, 0F);

		attachmentModel[5].addShapeBox(-12F, -11F, 4F, 2, 4, 1, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // smallScope2
		attachmentModel[5].setRotationPoint(0F, 0F, 0F);

		attachmentModel[6].addShapeBox(-12F, -11F, -5F, 2, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F); // smallScope2
		attachmentModel[6].setRotationPoint(0F, 0F, 0F);

		attachmentModel[7].addBox(-10.5F, -4F, -2F, 25, 1, 4, 0F); // mounterBase3
		attachmentModel[7].setRotationPoint(0F, 0F, 0F);

		attachmentModel[8].addBox(-3F, -4F, 2F, 6, 1, 1, 0F); // mounterBase2
		attachmentModel[8].setRotationPoint(0F, 0F, 0F);

		attachmentModel[9].addBox(-0.5F, -9.5F, -5F, 1, 1, 10, 0F); // ironSight2
		attachmentModel[9].setRotationPoint(0F, 0F, 0F);

		attachmentModel[10].addShapeBox(-11F, -4F, -2F, 2, 1, 4, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // largeScope1
		attachmentModel[10].setRotationPoint(0F, 0F, 0F);

		attachmentModel[11].addShapeBox(-11F, -11F, -6F, 2, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F); // largeScope2
		attachmentModel[11].setRotationPoint(0F, 0F, 0F);

		attachmentModel[12].addShapeBox(-11F, -15F, -2F, 2, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F); // largeScope1
		attachmentModel[12].setRotationPoint(0F, 0F, 0F);

		attachmentModel[13].addShapeBox(-11F, -15F, 1F, 2, 4, 1, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, -4F, 0F, 0.5F, -4F, 0F, 0F, 4F, 0F, 0F, 4F); // largeScope2
		attachmentModel[13].setRotationPoint(0F, 0F, 0F);

		attachmentModel[14].addShapeBox(-11F, -11F, 5F, 2, 4, 1, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // largeScope2
		attachmentModel[14].setRotationPoint(0F, 0F, 0F);

		attachmentModel[15].addBox(-0.5F, -14F, -0.5F, 1, 10, 1, 0F); // ironSight1
		attachmentModel[15].setRotationPoint(0F, 0F, 0F);

		attachmentModel[16].addShapeBox(-1F, -11F, -6F, 2, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F); // largeScope2
		attachmentModel[16].setRotationPoint(0F, 0F, 0F);

		attachmentModel[17].addShapeBox(-1F, -15F, -2F, 2, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F); // largeScope1
		attachmentModel[17].setRotationPoint(0F, 0F, 0F);

		attachmentModel[18].addShapeBox(-1F, -11F, 5F, 2, 4, 1, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // largeScope2
		attachmentModel[18].setRotationPoint(0F, 0F, 0F);

		attachmentModel[19].addShapeBox(13F, -14F, -2F, 2, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F); // smallScope1
		attachmentModel[19].setRotationPoint(0F, 0F, 0F);

		attachmentModel[20].addShapeBox(13F, -11F, -5F, 2, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F); // smallScope2
		attachmentModel[20].setRotationPoint(0F, 0F, 0F);

		attachmentModel[21].addShapeBox(13F, -11F, 4F, 2, 4, 1, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // smallScope2
		attachmentModel[21].setRotationPoint(0F, 0F, 0F);

		attachmentModel[22].addShapeBox(13F, -5F, -2F, 2, 1, 4, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // smallScope1
		attachmentModel[22].setRotationPoint(0F, 0F, 0F);

		attachmentModel[23].addShapeBox(-1F, -4.2F, -2F, 2, 1, 4, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // largeScope1
		attachmentModel[23].setRotationPoint(0F, 0F, 0F);

		attachmentModel[24].addShapeBox(-12F, -8F, 1F, 2, 4, 1, 0F, 0F, -0.5F, -3F, 0F, -0.5F, -3F, 0F, -1F, 3F, 0F, -1F, 3F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 38
		attachmentModel[24].setRotationPoint(0F, 0F, 0F);

		attachmentModel[25].addShapeBox(-11F, -7F, 1F, 2, 4, 1, 0F, 0F, 0.5F, -4F, 0F, 0.5F, -4F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 39
		attachmentModel[25].setRotationPoint(0F, 0F, 0F);

		attachmentModel[26].addShapeBox(-11F, -7F, -2F, 2, 4, 1, 0F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0.5F, -4F, 0F, 0.5F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // Box 40
		attachmentModel[26].setRotationPoint(0F, 0F, 0F);

		attachmentModel[27].addShapeBox(-12F, -8F, -2F, 2, 4, 1, 0F, 0F, -1F, 3F, 0F, -1F, 3F, 0F, -0.5F, -3F, 0F, -0.5F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // Box 41
		attachmentModel[27].setRotationPoint(0F, 0F, 0F);

		attachmentModel[28].addShapeBox(-12F, -14F, -2F, 2, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, -1F, 3F, 0F, -1F, 3F, 0F, -0.5F, -3F, 0F, -0.5F, -3F); // Box 42
		attachmentModel[28].setRotationPoint(0F, 0F, 0F);

		attachmentModel[29].addShapeBox(-11F, -15F, -2F, 2, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0.5F, -4F, 0F, 0.5F, -4F); // Box 43
		attachmentModel[29].setRotationPoint(0F, 0F, 0F);

		attachmentModel[30].addShapeBox(-1F, -7F, 1F, 2, 4, 1, 0F, 0F, 0.5F, -4F, 0F, 0.5F, -4F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 44
		attachmentModel[30].setRotationPoint(0F, 0F, 0F);

		attachmentModel[31].addShapeBox(-1F, -15F, 1F, 2, 4, 1, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, -4F, 0F, 0.5F, -4F, 0F, 0F, 4F, 0F, 0F, 4F); // Box 45
		attachmentModel[31].setRotationPoint(0F, 0F, 0F);

		attachmentModel[32].addShapeBox(-1F, -7F, -2F, 2, 4, 1, 0F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0.5F, -4F, 0F, 0.5F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // Box 46
		attachmentModel[32].setRotationPoint(0F, 0F, 0F);

		attachmentModel[33].addShapeBox(-1F, -15F, -2F, 2, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0.5F, -4F, 0F, 0.5F, -4F); // Box 47
		attachmentModel[33].setRotationPoint(0F, 0F, 0F);

		attachmentModel[34].addShapeBox(13F, -14F, -2F, 2, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, -1F, 3F, 0F, -1F, 3F, 0F, -0.5F, -3F, 0F, -0.5F, -3F); // Box 48
		attachmentModel[34].setRotationPoint(0F, 0F, 0F);

		attachmentModel[35].addShapeBox(13F, -14F, 1F, 2, 4, 1, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -3F, 0F, -0.5F, -3F, 0F, -1F, 3F, 0F, -1F, 3F); // Box 49
		attachmentModel[35].setRotationPoint(0F, 0F, 0F);

		attachmentModel[36].addShapeBox(13F, -8F, 1F, 2, 4, 1, 0F, 0F, -0.5F, -3F, 0F, -0.5F, -3F, 0F, -1F, 3F, 0F, -1F, 3F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 50
		attachmentModel[36].setRotationPoint(0F, 0F, 0F);

		attachmentModel[37].addShapeBox(13F, -8F, -2F, 2, 4, 1, 0F, 0F, -1F, 3F, 0F, -1F, 3F, 0F, -0.5F, -3F, 0F, -0.5F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // Box 51
		attachmentModel[37].setRotationPoint(0F, 0F, 0F);

		attachmentModel[38].addShapeBox(-14F, -14F, -2F, 2, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F); // Box 52
		attachmentModel[38].setRotationPoint(0F, 0F, 0F);

		attachmentModel[39].addShapeBox(-14F, -14F, 1F, 2, 4, 1, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -3F, 0F, -0.5F, -3F, 0F, -1F, 3F, 0F, -1F, 3F); // Box 53
		attachmentModel[39].setRotationPoint(0F, 0F, 0F);

		attachmentModel[40].addShapeBox(-14F, -14F, -2F, 2, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, -1F, 3F, 0F, -1F, 3F, 0F, -0.5F, -3F, 0F, -0.5F, -3F); // Box 54
		attachmentModel[40].setRotationPoint(0F, 0F, 0F);

		attachmentModel[41].addShapeBox(-14F, -11F, -5F, 2, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F); // Box 55
		attachmentModel[41].setRotationPoint(0F, 0F, 0F);

		attachmentModel[42].addShapeBox(-14F, -8F, -2F, 2, 4, 1, 0F, 0F, -1F, 3F, 0F, -1F, 3F, 0F, -0.5F, -3F, 0F, -0.5F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // Box 56
		attachmentModel[42].setRotationPoint(0F, 0F, 0F);

		attachmentModel[43].addShapeBox(-14F, -5F, -2F, 2, 1, 4, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 57
		attachmentModel[43].setRotationPoint(0F, 0F, 0F);

		attachmentModel[44].addShapeBox(-14F, -8F, 1F, 2, 4, 1, 0F, 0F, -0.5F, -3F, 0F, -0.5F, -3F, 0F, -1F, 3F, 0F, -1F, 3F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 58
		attachmentModel[44].setRotationPoint(0F, 0F, 0F);

		attachmentModel[45].addShapeBox(-14F, -11F, 4F, 2, 4, 1, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 59
		attachmentModel[45].setRotationPoint(0F, 0F, 0F);

		renderOffset = 0F;

		flipAll();
	}
}