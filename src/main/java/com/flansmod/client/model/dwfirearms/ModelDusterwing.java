package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelDusterwing extends ModelGun
{
	int textureX = 512;
	int textureY = 256;

	public ModelDusterwing()
	{
		gunModel = new ModelRendererTurbo[61];
		gunModel[0] = new ModelRendererTurbo(this, 1, 82, textureX, textureY); // Box 33
		gunModel[1] = new ModelRendererTurbo(this, 1, 28, textureX, textureY); // Box 34
		gunModel[2] = new ModelRendererTurbo(this, 1, 40, textureX, textureY); // Box 35
		gunModel[3] = new ModelRendererTurbo(this, 1, 54, textureX, textureY); // Box 36
		gunModel[4] = new ModelRendererTurbo(this, 1, 71, textureX, textureY); // Box 38
		gunModel[5] = new ModelRendererTurbo(this, 36, 28, textureX, textureY); // Box 39
		gunModel[6] = new ModelRendererTurbo(this, 36, 40, textureX, textureY); // Box 40
		gunModel[7] = new ModelRendererTurbo(this, 36, 54, textureX, textureY); // Box 41
		gunModel[8] = new ModelRendererTurbo(this, 36, 71, textureX, textureY); // Box 42
		gunModel[9] = new ModelRendererTurbo(this, 57, 28, textureX, textureY); // Box 0
		gunModel[10] = new ModelRendererTurbo(this, 57, 40, textureX, textureY); // Box 1
		gunModel[11] = new ModelRendererTurbo(this, 57, 40, textureX, textureY); // Box 3
		gunModel[12] = new ModelRendererTurbo(this, 57, 71, textureX, textureY); // Box 4
		gunModel[13] = new ModelRendererTurbo(this, 91, 81, textureX, textureY); // Box 5
		gunModel[14] = new ModelRendererTurbo(this, 91, 1, textureX, textureY); // Box 6
		gunModel[15] = new ModelRendererTurbo(this, 91, 41, textureX, textureY); // Box 7
		gunModel[16] = new ModelRendererTurbo(this, 118, 41, textureX, textureY); // Box 9
		gunModel[17] = new ModelRendererTurbo(this, 57, 28, textureX, textureY); // Box 10
		gunModel[18] = new ModelRendererTurbo(this, 1, 94, textureX, textureY); // Box 11
		gunModel[19] = new ModelRendererTurbo(this, 120, 28, textureX, textureY); // Box 12
		gunModel[20] = new ModelRendererTurbo(this, 91, 28, textureX, textureY); // Box 13
		gunModel[21] = new ModelRendererTurbo(this, 91, 17, textureX, textureY); // Box 14
		gunModel[22] = new ModelRendererTurbo(this, 145, 28, textureX, textureY); // Box 15
		gunModel[23] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 16
		gunModel[24] = new ModelRendererTurbo(this, 152, 71, textureX, textureY); // Box 17
		gunModel[25] = new ModelRendererTurbo(this, 142, 81, textureX, textureY); // Box 18
		gunModel[26] = new ModelRendererTurbo(this, 178, 29, textureX, textureY); // Box 19
		gunModel[27] = new ModelRendererTurbo(this, 62, 16, textureX, textureY); // Box 20
		gunModel[28] = new ModelRendererTurbo(this, 157, 45, textureX, textureY); // Box 21
		gunModel[29] = new ModelRendererTurbo(this, 42, 91, textureX, textureY); // Box 22
		gunModel[30] = new ModelRendererTurbo(this, 42, 82, textureX, textureY); // Box 23
		gunModel[31] = new ModelRendererTurbo(this, 91, 55, textureX, textureY); // Box 24
		gunModel[32] = new ModelRendererTurbo(this, 91, 71, textureX, textureY); // Box 25
		gunModel[33] = new ModelRendererTurbo(this, 187, 8, textureX, textureY); // Box 26
		gunModel[34] = new ModelRendererTurbo(this, 187, 1, textureX, textureY); // Box 27
		gunModel[35] = new ModelRendererTurbo(this, 187, 1, textureX, textureY); // Box 28
		gunModel[36] = new ModelRendererTurbo(this, 172, 1, textureX, textureY); // Box 33
		gunModel[37] = new ModelRendererTurbo(this, 172, 8, textureX, textureY); // Box 34
		gunModel[38] = new ModelRendererTurbo(this, 172, 1, textureX, textureY); // Box 35
		gunModel[39] = new ModelRendererTurbo(this, 62, 16, textureX, textureY); // Box 40
		gunModel[40] = new ModelRendererTurbo(this, 201, 29, textureX, textureY); // Box 41
		gunModel[41] = new ModelRendererTurbo(this, 62, 16, textureX, textureY); // Box 42
		gunModel[42] = new ModelRendererTurbo(this, 45, 17, textureX, textureY); // Box 43
		gunModel[43] = new ModelRendererTurbo(this, 45, 17, textureX, textureY); // Box 44
		gunModel[44] = new ModelRendererTurbo(this, 45, 17, textureX, textureY); // Box 45
		gunModel[45] = new ModelRendererTurbo(this, 70, 1, textureX, textureY); // Box 46
		gunModel[46] = new ModelRendererTurbo(this, 70, 1, textureX, textureY); // Box 47
		gunModel[47] = new ModelRendererTurbo(this, 70, 1, textureX, textureY); // Box 48
		gunModel[48] = new ModelRendererTurbo(this, 70, 1, textureX, textureY); // Box 49
		gunModel[49] = new ModelRendererTurbo(this, 70, 1, textureX, textureY); // Box 52
		gunModel[50] = new ModelRendererTurbo(this, 154, 57, textureX, textureY); // Box 0
		gunModel[51] = new ModelRendererTurbo(this, 22, 19, textureX, textureY); // Box 1
		gunModel[52] = new ModelRendererTurbo(this, 22, 23, textureX, textureY); // Box 2
		gunModel[53] = new ModelRendererTurbo(this, 1, 14, textureX, textureY); // Box 3
		gunModel[54] = new ModelRendererTurbo(this, 197, 64, textureX, textureY); // Box 0
		gunModel[55] = new ModelRendererTurbo(this, 197, 64, textureX, textureY); // Box 1
		gunModel[56] = new ModelRendererTurbo(this, 197, 55, textureX, textureY); // Box 2
		gunModel[57] = new ModelRendererTurbo(this, 197, 55, textureX, textureY); // Box 3
		gunModel[58] = new ModelRendererTurbo(this, 242, 11, textureX, textureY); // Box 4
		gunModel[59] = new ModelRendererTurbo(this, 185, 87, textureX, textureY); // Box 24
		gunModel[60] = new ModelRendererTurbo(this, 202, 6, textureX, textureY); // Box 27

		gunModel[0].addBox(0F, 0F, 0F, 12, 3, 8, 0F); // Box 33
		gunModel[0].setRotationPoint(-8F, -10F, -4F);

		gunModel[1].addShapeBox(0F, 0F, 0F, 9, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 34
		gunModel[1].setRotationPoint(-9F, -7F, -4F);

		gunModel[2].addShapeBox(0F, 0F, 0F, 9, 5, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F); // Box 35
		gunModel[2].setRotationPoint(-9F, -4F, -4F);

		gunModel[3].addShapeBox(0F, 0F, 0F, 9, 8, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, -3F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 2F, -3F, 0F); // Box 36
		gunModel[3].setRotationPoint(-11F, 1F, -4F);

		gunModel[4].addShapeBox(0F, 0F, 0F, 8, 2, 8, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F); // Box 38
		gunModel[4].setRotationPoint(-13F, 9F, -4F);

		gunModel[5].addShapeBox(0F, 0F, 0F, 2, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F); // Box 39
		gunModel[5].setRotationPoint(0F, -7F, -4F);

		gunModel[6].addShapeBox(0F, 0F, 0F, 2, 5, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, -1.5F, -2F, 0F, -1.5F, 2F, 0F, 0F); // Box 40
		gunModel[6].setRotationPoint(0F, -4F, -4F);

		gunModel[7].addShapeBox(0F, 0F, 0F, 2, 8, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, -1.5F, -3F, 0F, -1.5F, 3F, 0F, 0F); // Box 41
		gunModel[7].setRotationPoint(-2F, 1F, -4F);

		gunModel[8].addShapeBox(0F, 0F, 0F, 2, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F); // Box 42
		gunModel[8].setRotationPoint(-5F, 9F, -4F);

		gunModel[9].addShapeBox(0F, 0F, 0F, 1, 3, 8, 0F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F); // Box 0
		gunModel[9].setRotationPoint(-10F, -7F, -4F);

		gunModel[10].addShapeBox(0F, 0F, 0F, 1, 5, 8, 0F, -2F, 0F, -1.5F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F); // Box 1
		gunModel[10].setRotationPoint(-12F, -4F, -4F);

		gunModel[11].addShapeBox(0F, 0F, 0F, 1, 5, 8, 0F, -2F, 0F, -1.5F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F); // Box 3
		gunModel[11].setRotationPoint(-14F, 1F, -4F);

		gunModel[12].addShapeBox(0F, 0F, 0F, 1, 2, 8, 0F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F); // Box 4
		gunModel[12].setRotationPoint(-14F, 6F, -4F);

		gunModel[13].addShapeBox(0F, 0F, 0F, 17, 4, 8, 0F, 3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 5
		gunModel[13].setRotationPoint(-13F, -14F, -4F);

		gunModel[14].addBox(0F, 0F, 0F, 31, 6, 9, 0F); // Box 6
		gunModel[14].setRotationPoint(-16F, -20F, -4.5F);

		gunModel[15].addShapeBox(0F, 0F, 0F, 4, 4, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F); // Box 7
		gunModel[15].setRotationPoint(1F, -14F, -4.5F);

		gunModel[16].addBox(0F, 0F, 0F, 10, 4, 9, 0F); // Box 9
		gunModel[16].setRotationPoint(5F, -14F, -4.5F);

		gunModel[17].addShapeBox(0F, 0F, 0F, 1, 3, 8, 0F, 2F, 0F, -1.5F, -2F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F); // Box 10
		gunModel[17].setRotationPoint(-10F, -10F, -4F);

		gunModel[18].addShapeBox(0F, 0F, 0F, 3, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F); // Box 11
		gunModel[18].setRotationPoint(-11F, -10F, -4F);

		gunModel[19].addShapeBox(0F, 0F, 0F, 4, 4, 8, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 12
		gunModel[19].setRotationPoint(-16F, -24F, -4F);

		gunModel[20].addBox(0F, 0F, 0F, 6, 4, 8, 0F); // Box 13
		gunModel[20].setRotationPoint(-12F, -24F, -4F);

		gunModel[21].addBox(0F, 0F, 0F, 27, 2, 8, 0F); // Box 14
		gunModel[21].setRotationPoint(-6F, -22F, -4F);

		gunModel[22].addBox(0F, 0F, 0F, 8, 4, 8, 0F); // Box 15
		gunModel[22].setRotationPoint(21F, -24F, -4F);

		gunModel[23].addBox(0F, 0F, 0F, 27, 2, 7, 0F); // Box 16
		gunModel[23].setRotationPoint(-6F, -24F, -3.5F);

		gunModel[24].addBox(0F, 0F, 0F, 14, 1, 8, 0F); // Box 17
		gunModel[24].setRotationPoint(15F, -14F, -4F);

		gunModel[25].addShapeBox(0F, 0F, 0F, 13, 4, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F); // Box 18
		gunModel[25].setRotationPoint(4F, -13F, -4F);

		gunModel[26].addBox(0F, 0F, 0F, 3, 3, 8, 0F); // Box 19
		gunModel[26].setRotationPoint(29F, -24F, -4F);

		gunModel[27].addShapeBox(0F, 0F, 0F, 1, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, 0F); // Box 20
		gunModel[27].setRotationPoint(36F, -24F, -4F);

		gunModel[28].addBox(0F, 0F, 0F, 22, 1, 8, 0F); // Box 21
		gunModel[28].setRotationPoint(29F, -21F, -4F);

		gunModel[29].addShapeBox(0F, 0F, 0F, 12, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2.5F, 0F, 0F); // Box 22
		gunModel[29].setRotationPoint(39F, -23F, -3.5F);

		gunModel[30].addShapeBox(0F, 0F, 0F, 12, 1, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 23
		gunModel[30].setRotationPoint(39F, -24F, -3.5F);

		gunModel[31].addBox(0F, 0F, 0F, 22, 6, 9, 0F); // Box 24
		gunModel[31].setRotationPoint(29F, -20F, -4.5F);

		gunModel[32].addBox(0F, 0F, 0F, 22, 1, 8, 0F); // Box 25
		gunModel[32].setRotationPoint(29F, -14F, -4F);

		gunModel[33].addBox(0F, 0F, 0F, 2, 2, 5, 0F); // Box 26
		gunModel[33].setRotationPoint(51F, -17F, -2.5F);

		gunModel[34].addShapeBox(0F, 0F, 0F, 2, 1, 5, 0F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 27
		gunModel[34].setRotationPoint(51F, -18F, -2.5F);

		gunModel[35].addShapeBox(0F, 0F, 0F, 2, 1, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F); // Box 28
		gunModel[35].setRotationPoint(51F, -15F, -2.5F);

		gunModel[36].addShapeBox(0F, 0F, 0F, 2, 1, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F); // Box 33
		gunModel[36].setRotationPoint(51F, -20F, -2.5F);

		gunModel[37].addBox(0F, 0F, 0F, 2, 2, 5, 0F); // Box 34
		gunModel[37].setRotationPoint(51F, -22F, -2.5F);

		gunModel[38].addShapeBox(0F, 0F, 0F, 2, 1, 5, 0F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 35
		gunModel[38].setRotationPoint(51F, -23F, -2.5F);

		gunModel[39].addShapeBox(0F, 0F, 0F, 1, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, 0F); // Box 40
		gunModel[39].setRotationPoint(34F, -24F, -4F);

		gunModel[40].addShapeBox(0F, 0F, 0F, 1, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F); // Box 41
		gunModel[40].setRotationPoint(32F, -24F, -4F);

		gunModel[41].addShapeBox(0F, 0F, 0F, 1, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, 0F); // Box 42
		gunModel[41].setRotationPoint(38F, -24F, -4F);

		gunModel[42].addShapeBox(0F, 0F, 0F, 1, 3, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, 0F); // Box 43
		gunModel[42].setRotationPoint(33F, -24F, -3.5F);

		gunModel[43].addShapeBox(0F, 0F, 0F, 1, 3, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, 0F); // Box 44
		gunModel[43].setRotationPoint(35F, -24F, -3.5F);

		gunModel[44].addShapeBox(0F, 0F, 0F, 1, 3, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, 0F); // Box 45
		gunModel[44].setRotationPoint(37F, -24F, -3.5F);

		gunModel[45].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 46
		gunModel[45].setRotationPoint(-6F, -26F, -3.5F);

		gunModel[46].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 47
		gunModel[46].setRotationPoint(0F, -26F, -3.5F);

		gunModel[47].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 48
		gunModel[47].setRotationPoint(12F, -26F, -3.5F);

		gunModel[48].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 49
		gunModel[48].setRotationPoint(6F, -26F, -3.5F);

		gunModel[49].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 52
		gunModel[49].setRotationPoint(18F, -26F, -3.5F);

		gunModel[50].addBox(0F, 0F, 0F, 14, 6, 7, 0F); // Box 0
		gunModel[50].setRotationPoint(15F, -20F, -2.5F);

		gunModel[51].addBox(0F, 0F, 0F, 9, 1, 2, 0F); // Box 1
		gunModel[51].setRotationPoint(15F, -20F, -4.5F);

		gunModel[52].addBox(0F, 0F, 0F, 9, 2, 2, 0F); // Box 2
		gunModel[52].setRotationPoint(15F, -16F, -4.5F);

		gunModel[53].addBox(0F, 0F, 0F, 5, 6, 2, 0F); // Box 3
		gunModel[53].setRotationPoint(24F, -20F, -4.5F);

		gunModel[54].addShapeBox(0F, 0F, 0F, 17, 7, 1, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 0
		gunModel[54].setRotationPoint(31F, -20.5F, -6F);

		gunModel[55].addBox(0F, 0F, 0F, 17, 7, 1, 0F); // Box 1
		gunModel[55].setRotationPoint(31F, -20.5F, -5F);

		gunModel[56].addBox(0F, 0F, 0F, 17, 7, 1, 0F); // Box 2
		gunModel[56].setRotationPoint(31F, -20.5F, 4F);

		gunModel[57].addShapeBox(0F, 0F, 0F, 17, 7, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F); // Box 3
		gunModel[57].setRotationPoint(31F, -20.5F, 5F);

		gunModel[58].addShapeBox(0F, 0F, 0F, 4, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 4
		gunModel[58].setRotationPoint(46F, -25F, -1.5F);

		gunModel[59].addBox(0F, 0F, 0F, 8, 1, 5, 0F); // Box 24
		gunModel[59].setRotationPoint(19F, -13.5F, -2.5F);

		gunModel[60].addBox(0F, 0F, 0F, 5, 2, 7, 0F); // Box 27
		gunModel[60].setRotationPoint(-12F, -26F, -3.5F);


		defaultScopeModel = new ModelRendererTurbo[3];
		defaultScopeModel[0] = new ModelRendererTurbo(this, 242, 6, textureX, textureY); // Box 26
		defaultScopeModel[1] = new ModelRendererTurbo(this, 227, 4, textureX, textureY); // Box 26
		defaultScopeModel[2] = new ModelRendererTurbo(this, 227, 10, textureX, textureY); // Box 26

		defaultScopeModel[0].addShapeBox(0F, 0F, 0F, 4, 3, 1, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 26
		defaultScopeModel[0].setRotationPoint(46F, -28F, -0.5F);

		defaultScopeModel[1].addShapeBox(0F, 0F, 0F, 5, 3, 2, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 26
		defaultScopeModel[1].setRotationPoint(-12F, -29F, -3.5F);

		defaultScopeModel[2].addShapeBox(0F, 0F, 0F, 5, 3, 2, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 26
		defaultScopeModel[2].setRotationPoint(-12F, -29F, 1.5F);


		defaultStockModel = new ModelRendererTurbo[19];
		defaultStockModel[0] = new ModelRendererTurbo(this, 38, 146, textureX, textureY); // Box 23
		defaultStockModel[1] = new ModelRendererTurbo(this, 38, 157, textureX, textureY); // Box 23
		defaultStockModel[2] = new ModelRendererTurbo(this, 38, 157, textureX, textureY); // Box 23
		defaultStockModel[3] = new ModelRendererTurbo(this, 1, 146, textureX, textureY); // Box 23
		defaultStockModel[4] = new ModelRendererTurbo(this, 1, 155, textureX, textureY); // Box 23
		defaultStockModel[5] = new ModelRendererTurbo(this, 1, 146, textureX, textureY); // Box 23
		defaultStockModel[6] = new ModelRendererTurbo(this, 73, 157, textureX, textureY); // Box 23
		defaultStockModel[7] = new ModelRendererTurbo(this, 73, 146, textureX, textureY); // Box 23
		defaultStockModel[8] = new ModelRendererTurbo(this, 73, 157, textureX, textureY); // Box 23
		defaultStockModel[9] = new ModelRendererTurbo(this, 1, 119, textureX, textureY); // Box 23
		defaultStockModel[10] = new ModelRendererTurbo(this, 1, 136, textureX, textureY); // Box 23
		defaultStockModel[11] = new ModelRendererTurbo(this, 65, 109, textureX, textureY); // Box 23
		defaultStockModel[12] = new ModelRendererTurbo(this, 38, 134, textureX, textureY); // Box 23
		defaultStockModel[13] = new ModelRendererTurbo(this, 1, 106, textureX, textureY); // Box 23
		defaultStockModel[14] = new ModelRendererTurbo(this, 65, 122, textureX, textureY); // Box 23
		defaultStockModel[15] = new ModelRendererTurbo(this, 38, 113, textureX, textureY); // Box 23
		defaultStockModel[16] = new ModelRendererTurbo(this, 38, 103, textureX, textureY); // Box 23
		defaultStockModel[17] = new ModelRendererTurbo(this, 18, 164, textureX, textureY); // Box 23
		defaultStockModel[18] = new ModelRendererTurbo(this, 1, 164, textureX, textureY); // Box 23

		defaultStockModel[0].addBox(0F, 0F, 0F, 10, 3, 7, 0F); // Box 23
		defaultStockModel[0].setRotationPoint(-25F, -18.5F, -3.5F);

		defaultStockModel[1].addShapeBox(0F, 0F, 0F, 10, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 23
		defaultStockModel[1].setRotationPoint(-25F, -20.5F, -3.5F);

		defaultStockModel[2].addShapeBox(0F, 0F, 0F, 10, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 23
		defaultStockModel[2].setRotationPoint(-25F, -15.5F, -3.5F);

		defaultStockModel[3].addShapeBox(0F, 0F, 0F, 12, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 23
		defaultStockModel[3].setRotationPoint(-37F, -16F, -3F);

		defaultStockModel[4].addBox(0F, 0F, 0F, 12, 2, 6, 0F); // Box 23
		defaultStockModel[4].setRotationPoint(-37F, -18F, -3F);

		defaultStockModel[5].addShapeBox(0F, 0F, 0F, 12, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 23
		defaultStockModel[5].setRotationPoint(-37F, -20F, -3F);

		defaultStockModel[6].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 23
		defaultStockModel[6].setRotationPoint(-40F, -20.5F, -3.5F);

		defaultStockModel[7].addBox(0F, 0F, 0F, 3, 3, 7, 0F); // Box 23
		defaultStockModel[7].setRotationPoint(-40F, -18.5F, -3.5F);

		defaultStockModel[8].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 23
		defaultStockModel[8].setRotationPoint(-40F, -15.5F, -3.5F);

		defaultStockModel[9].addBox(0F, 0F, 0F, 10, 6, 8, 0F); // Box 23
		defaultStockModel[9].setRotationPoint(-50F, -20F, -4F);

		defaultStockModel[10].addShapeBox(0F, 0F, 0F, 10, 1, 8, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 23
		defaultStockModel[10].setRotationPoint(-50F, -21F, -4F);

		defaultStockModel[11].addShapeBox(0F, 0F, 0F, 1, 4, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F); // Box 23
		defaultStockModel[11].setRotationPoint(-41F, -14F, -4F);

		defaultStockModel[12].addBox(0F, 0F, 0F, 13, 3, 8, 0F); // Box 23
		defaultStockModel[12].setRotationPoint(-50F, -10F, -4F);

		defaultStockModel[13].addBox(0F, 0F, 0F, 9, 4, 8, 0F); // Box 23
		defaultStockModel[13].setRotationPoint(-50F, -14F, -4F);

		defaultStockModel[14].addShapeBox(0F, 0F, 0F, 1, 3, 8, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 23
		defaultStockModel[14].setRotationPoint(-45F, -7F, -4F);

		defaultStockModel[15].addBox(0F, 0F, 0F, 5, 11, 8, 0F); // Box 23
		defaultStockModel[15].setRotationPoint(-50F, -7F, -4F);

		defaultStockModel[16].addShapeBox(0F, 0F, 0F, 5, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 23
		defaultStockModel[16].setRotationPoint(-50F, 4F, -4F);

		defaultStockModel[17].addShapeBox(0F, 0F, 0F, 2, 12, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F); // Box 23
		defaultStockModel[17].setRotationPoint(-52F, -20F, -3F);

		defaultStockModel[18].addShapeBox(0F, 0F, 0F, 2, 12, 6, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 23
		defaultStockModel[18].setRotationPoint(-52F, -8F, -3F);


		ammoModel = new ModelRendererTurbo[9];
		ammoModel[0] = new ModelRendererTurbo(this, 91, 137, textureX, textureY); // Box 29
		ammoModel[1] = new ModelRendererTurbo(this, 91, 94, textureX, textureY); // Box 30
		ammoModel[2] = new ModelRendererTurbo(this, 91, 109, textureX, textureY); // Box 31
		ammoModel[3] = new ModelRendererTurbo(this, 91, 123, textureX, textureY); // Box 32
		ammoModel[4] = new ModelRendererTurbo(this, 176, 94, textureX, textureY); // Box 36
		ammoModel[5] = new ModelRendererTurbo(this, 176, 109, textureX, textureY); // Box 37
		ammoModel[6] = new ModelRendererTurbo(this, 176, 123, textureX, textureY); // Box 39
		ammoModel[7] = new ModelRendererTurbo(this, 126, 142, textureX, textureY); // Box 91
		ammoModel[8] = new ModelRendererTurbo(this, 147, 142, textureX, textureY); // Box 92

		ammoModel[0].addBox(0F, 0F, 0F, 10, 3, 7, 0F); // Box 29
		ammoModel[0].setRotationPoint(18F, -14F, -3.5F);

		ammoModel[1].addBox(0F, 0F, 0F, 32, 4, 10, 0F); // Box 30
		ammoModel[1].setRotationPoint(17F, -10F, -5F);

		ammoModel[2].addShapeBox(0F, 0F, 0F, 32, 3, 10, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 31
		ammoModel[2].setRotationPoint(17F, -13F, -5F);

		ammoModel[3].addShapeBox(0F, 0F, 0F, 32, 3, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 32
		ammoModel[3].setRotationPoint(17F, -6F, -5F);

		ammoModel[4].addShapeBox(0F, 0F, 0F, 2, 4, 10, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F); // Box 36
		ammoModel[4].setRotationPoint(49F, -10F, -5F);

		ammoModel[5].addShapeBox(0F, 0F, 0F, 2, 3, 10, 0F, 0F, 0F, -3F, 0F, -1.5F, -3.5F, 0F, -1.5F, -3.5F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F); // Box 37
		ammoModel[5].setRotationPoint(49F, -13F, -5F);

		ammoModel[6].addShapeBox(0F, 0F, 0F, 2, 3, 10, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, -1.5F, -3.5F, 0F, -1.5F, -3.5F, 0F, 0F, -3F); // Box 39
		ammoModel[6].setRotationPoint(49F, -6F, -5F);

		ammoModel[7].addShapeBox(0F, 0F, 0F, 6, 1, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 91
		ammoModel[7].setRotationPoint(19F, -15F, -2.5F);

		ammoModel[8].addShapeBox(0F, 0F, 0F, 2, 1, 4, 0F, 0F, 0F, -1F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 92
		ammoModel[8].setRotationPoint(25F, -15F, -2.5F);


		slideModel = new ModelRendererTurbo[1];
		slideModel[0] = new ModelRendererTurbo(this, 1, 23, textureX, textureY); // Box 4

		slideModel[0].addShapeBox(0F, 0F, 0F, 9, 3, 1, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 4
		slideModel[0].setRotationPoint(15F, -19F, -4F);

		barrelAttachPoint = new Vector3f(57F /16F, 15F /16F, 0F /16F);
		stockAttachPoint = new Vector3f(-12F /16F, 13F /16F, 0F /16F);
		scopeAttachPoint = new Vector3f(13F /16F, 20F /16F, 0F /16F);

		gunSlideDistance = 0.5F;
		animationType = EnumAnimationType.BOTTOM_CLIP;

		flipAll();
		translateAll(0F, 0F, 0F);
	}
}
