package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.ModelAttachment;
import com.flansmod.client.tmt.ModelRendererTurbo;

import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelABTAmbirconSight extends ModelAttachment
{
	int textureX = 512;
	int textureY = 256;

	public ModelABTAmbirconSight()
	{
		attachmentModel = new ModelRendererTurbo[42];
		attachmentModel[0] = new ModelRendererTurbo(this, 1, 29, textureX, textureY); // Box 0
		attachmentModel[1] = new ModelRendererTurbo(this, 20, 40, textureX, textureY); // scopeBase2-2
		attachmentModel[2] = new ModelRendererTurbo(this, 26, 47, textureX, textureY); // brassScope1
		attachmentModel[3] = new ModelRendererTurbo(this, 26, 47, textureX, textureY); // brassScope1-2
		attachmentModel[4] = new ModelRendererTurbo(this, 26, 53, textureX, textureY); // brassScope3
		attachmentModel[5] = new ModelRendererTurbo(this, 26, 53, textureX, textureY); // brassScope3-2
		attachmentModel[6] = new ModelRendererTurbo(this, 1, 83, textureX, textureY); // knob1
		attachmentModel[7] = new ModelRendererTurbo(this, 1, 83, textureX, textureY); // knob2
		attachmentModel[8] = new ModelRendererTurbo(this, 1, 83, textureX, textureY); // knob3
		attachmentModel[9] = new ModelRendererTurbo(this, 1, 40, textureX, textureY); // scopeBase1
		attachmentModel[10] = new ModelRendererTurbo(this, 20, 40, textureX, textureY); // scopeBase2
		attachmentModel[11] = new ModelRendererTurbo(this, 26, 65, textureX, textureY); // scopeFront1
		attachmentModel[12] = new ModelRendererTurbo(this, 26, 65, textureX, textureY); // scopeFront1-2
		attachmentModel[13] = new ModelRendererTurbo(this, 26, 71, textureX, textureY); // scopeFront2
		attachmentModel[14] = new ModelRendererTurbo(this, 26, 71, textureX, textureY); // scopeFront2-2
		attachmentModel[15] = new ModelRendererTurbo(this, 26, 77, textureX, textureY); // scopeFront3-2
		attachmentModel[16] = new ModelRendererTurbo(this, 1, 47, textureX, textureY); // scopeMain1
		attachmentModel[17] = new ModelRendererTurbo(this, 1, 47, textureX, textureY); // scopeMain1-2
		attachmentModel[18] = new ModelRendererTurbo(this, 1, 53, textureX, textureY); // scopeMain2
		attachmentModel[19] = new ModelRendererTurbo(this, 1, 53, textureX, textureY); // scopeMain2-2
		attachmentModel[20] = new ModelRendererTurbo(this, 1, 59, textureX, textureY); // scopeMain3
		attachmentModel[21] = new ModelRendererTurbo(this, 1, 65, textureX, textureY); // scopeMain4
		attachmentModel[22] = new ModelRendererTurbo(this, 1, 65, textureX, textureY); // scopeMain4-2
		attachmentModel[23] = new ModelRendererTurbo(this, 1, 71, textureX, textureY); // scopeMain5
		attachmentModel[24] = new ModelRendererTurbo(this, 1, 71, textureX, textureY); // scopeMain5-2
		attachmentModel[25] = new ModelRendererTurbo(this, 1, 77, textureX, textureY); // scopeMain6
		attachmentModel[26] = new ModelRendererTurbo(this, 41, 47, textureX, textureY); // scopeSight1
		attachmentModel[27] = new ModelRendererTurbo(this, 41, 47, textureX, textureY); // scopeSight1-2
		attachmentModel[28] = new ModelRendererTurbo(this, 41, 52, textureX, textureY); // scopeSight2-1
		attachmentModel[29] = new ModelRendererTurbo(this, 26, 59, textureX, textureY); // Box 42
		attachmentModel[30] = new ModelRendererTurbo(this, 26, 59, textureX, textureY); // Box 43
		attachmentModel[31] = new ModelRendererTurbo(this, 26, 59, textureX, textureY); // Box 44
		attachmentModel[32] = new ModelRendererTurbo(this, 26, 59, textureX, textureY); // Box 45
		attachmentModel[33] = new ModelRendererTurbo(this, 26, 77, textureX, textureY); // Box 46
		attachmentModel[34] = new ModelRendererTurbo(this, 26, 77, textureX, textureY); // Box 47
		attachmentModel[35] = new ModelRendererTurbo(this, 26, 77, textureX, textureY); // Box 48
		attachmentModel[36] = new ModelRendererTurbo(this, 1, 59, textureX, textureY); // Box 49
		attachmentModel[37] = new ModelRendererTurbo(this, 1, 59, textureX, textureY); // Box 50
		attachmentModel[38] = new ModelRendererTurbo(this, 1, 59, textureX, textureY); // Box 51
		attachmentModel[39] = new ModelRendererTurbo(this, 1, 77, textureX, textureY); // Box 52
		attachmentModel[40] = new ModelRendererTurbo(this, 1, 77, textureX, textureY); // Box 53
		attachmentModel[41] = new ModelRendererTurbo(this, 1, 77, textureX, textureY); // Box 54

		attachmentModel[0].addBox(-6F, -3F, -3.5F, 12, 3, 7, 0F); // Box 0
		attachmentModel[0].setRotationPoint(0F, 0F, 0F);

		attachmentModel[1].addBox(-3F, -4F, -3F, 6, 2, 1, 0F); // scopeBase2-2
		attachmentModel[1].setRotationPoint(0F, 0F, 0F);

		attachmentModel[2].addShapeBox(-10F, -14F, -2F, 3, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F); // brassScope1
		attachmentModel[2].setRotationPoint(0F, 0F, 0F);

		attachmentModel[3].addShapeBox(-10F, -5F, -2F, 3, 1, 4, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // brassScope1-2
		attachmentModel[3].setRotationPoint(0F, 0F, 0F);

		attachmentModel[4].addShapeBox(-10F, -11F, 4F, 6, 4, 1, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // brassScope3
		attachmentModel[4].setRotationPoint(0F, 0F, 0F);

		attachmentModel[5].addShapeBox(-10F, -11F, -5F, 6, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F); // brassScope3-2
		attachmentModel[5].setRotationPoint(0F, 0F, 0F);

		attachmentModel[6].addShapeBox(-3F, -16F, -3F, 2, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F); // knob1
		attachmentModel[6].setRotationPoint(0F, 0F, 0F);

		attachmentModel[7].addBox(-1F, -16F, -3F, 2, 2, 6, 0F); // knob2
		attachmentModel[7].setRotationPoint(0F, 0F, 0F);

		attachmentModel[8].addShapeBox(1F, -16F, -3F, 2, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F); // knob3
		attachmentModel[8].setRotationPoint(0F, 0F, 0F);

		attachmentModel[9].addBox(-3.5F, -4F, -2F, 7, 2, 4, 0F); // scopeBase1
		attachmentModel[9].setRotationPoint(0F, 0F, 0F);

		attachmentModel[10].addBox(-3F, -4F, 2F, 6, 2, 1, 0F); // scopeBase2
		attachmentModel[10].setRotationPoint(0F, 0F, 0F);

		attachmentModel[11].addShapeBox(12F, -15F, -2F, 2, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F); // scopeFront1
		attachmentModel[11].setRotationPoint(0F, 0F, 0F);

		attachmentModel[12].addShapeBox(12F, -4F, -2F, 2, 1, 4, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scopeFront1-2
		attachmentModel[12].setRotationPoint(0F, 0F, 0F);

		attachmentModel[13].addShapeBox(12F, -11F, 5F, 2, 4, 1, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scopeFront2
		attachmentModel[13].setRotationPoint(0F, 0F, 0F);

		attachmentModel[14].addShapeBox(12F, -11F, -6F, 2, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F); // scopeFront2-2
		attachmentModel[14].setRotationPoint(0F, 0F, 0F);

		attachmentModel[15].addShapeBox(12F, -15F, 1F, 2, 4, 1, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, -4F, 0F, 0.5F, -4F, 0F, 0F, 4F, 0F, 0F, 4F); // scopeFront3-2
		attachmentModel[15].setRotationPoint(0F, 0F, 0F);

		attachmentModel[16].addShapeBox(-4F, -15F, -2F, 8, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F); // scopeMain1
		attachmentModel[16].setRotationPoint(0F, 0F, 0F);

		attachmentModel[17].addShapeBox(-4F, -4F, -2F, 8, 1, 4, 0F, 0F, 0.25F, 1F, 0F, 0.25F, 1F, 0F, 0.25F, 1F, 0F, 0.25F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scopeMain1-2
		attachmentModel[17].setRotationPoint(0F, 0F, 0F);

		attachmentModel[18].addShapeBox(-4F, -11F, 5F, 8, 4, 1, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scopeMain2
		attachmentModel[18].setRotationPoint(0F, 0F, 0F);

		attachmentModel[19].addShapeBox(-4F, -11F, -6F, 8, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F); // scopeMain2-2
		attachmentModel[19].setRotationPoint(0F, 0F, 0F);

		attachmentModel[20].addShapeBox(-4F, -15F, -2F, 8, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0.5F, -4F, 0F, 0.5F, -4F); // scopeMain3
		attachmentModel[20].setRotationPoint(0F, 0F, 0F);

		attachmentModel[21].addShapeBox(4F, -14F, -2F, 8, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F); // scopeMain4
		attachmentModel[21].setRotationPoint(0F, 0F, 0F);

		attachmentModel[22].addShapeBox(4F, -5F, -2F, 8, 1, 4, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scopeMain4-2
		attachmentModel[22].setRotationPoint(0F, 0F, 0F);

		attachmentModel[23].addShapeBox(4F, -11F, 4F, 8, 4, 1, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scopeMain5
		attachmentModel[23].setRotationPoint(0F, 0F, 0F);

		attachmentModel[24].addShapeBox(4F, -11F, -5F, 8, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F); // scopeMain5-2
		attachmentModel[24].setRotationPoint(0F, 0F, 0F);

		attachmentModel[25].addShapeBox(4F, -14F, -2F, 8, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, -1F, 3F, 0F, -1F, 3F, 0F, -0.5F, -3F, 0F, -0.5F, -3F); // scopeMain6
		attachmentModel[25].setRotationPoint(0F, 0F, 0F);

		attachmentModel[26].addBox(-9F, -9.5F, 1F, 1, 1, 3, 0F); // scopeSight1
		attachmentModel[26].setRotationPoint(0F, 0F, 0F);

		attachmentModel[27].addBox(-9F, -9.5F, -4F, 1, 1, 3, 0F); // scopeSight1-2
		attachmentModel[27].setRotationPoint(0F, 0F, 0F);

		attachmentModel[28].addBox(-9F, -8F, -0.5F, 1, 3, 1, 0F); // scopeSight2-1
		attachmentModel[28].setRotationPoint(0F, 0F, 0F);

		attachmentModel[29].addShapeBox(-10F, -14F, 1F, 3, 4, 1, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -3F, 0F, -0.5F, -3F, 0F, -1F, 3F, 0F, -1F, 3F); // Box 42
		attachmentModel[29].setRotationPoint(0F, 0F, 0F);

		attachmentModel[30].addShapeBox(-10F, -14F, -2F, 3, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, -1F, 3F, 0F, -1F, 3F, 0F, -0.5F, -3F, 0F, -0.5F, -3F); // Box 43
		attachmentModel[30].setRotationPoint(0F, 0F, 0F);

		attachmentModel[31].addShapeBox(-10F, -8F, -2F, 3, 4, 1, 0F, 0F, -1F, 3F, 0F, -1F, 3F, 0F, -0.5F, -3F, 0F, -0.5F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // Box 44
		attachmentModel[31].setRotationPoint(0F, 0F, 0F);

		attachmentModel[32].addShapeBox(-10F, -8F, 1F, 3, 4, 1, 0F, 0F, -0.5F, -3F, 0F, -0.5F, -3F, 0F, -1F, 3F, 0F, -1F, 3F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 45
		attachmentModel[32].setRotationPoint(0F, 0F, 0F);

		attachmentModel[33].addShapeBox(12F, -15F, -2F, 2, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0.5F, -4F, 0F, 0.5F, -4F); // Box 46
		attachmentModel[33].setRotationPoint(0F, 0F, 0F);

		attachmentModel[34].addShapeBox(12F, -7F, -2F, 2, 4, 1, 0F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0.5F, -4F, 0F, 0.5F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // Box 47
		attachmentModel[34].setRotationPoint(0F, 0F, 0F);

		attachmentModel[35].addShapeBox(12F, -7F, 1F, 2, 4, 1, 0F, 0F, 0.5F, -4F, 0F, 0.5F, -4F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 48
		attachmentModel[35].setRotationPoint(0F, 0F, 0F);

		attachmentModel[36].addShapeBox(-4F, -7F, -2F, 8, 4, 1, 0F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0.5F, -4F, 0F, 0.5F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // Box 49
		attachmentModel[36].setRotationPoint(0F, 0F, 0F);

		attachmentModel[37].addShapeBox(-4F, -7F, 1F, 8, 4, 1, 0F, 0F, 0.5F, -4F, 0F, 0.5F, -4F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 50
		attachmentModel[37].setRotationPoint(0F, 0F, 0F);

		attachmentModel[38].addShapeBox(-4F, -15F, 1F, 8, 4, 1, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, -4F, 0F, 0.5F, -4F, 0F, 0F, 4F, 0F, 0F, 4F); // Box 51
		attachmentModel[38].setRotationPoint(0F, 0F, 0F);

		attachmentModel[39].addShapeBox(4F, -14F, 1F, 8, 4, 1, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -3F, 0F, -0.5F, -3F, 0F, -1F, 3F, 0F, -1F, 3F); // Box 52
		attachmentModel[39].setRotationPoint(0F, 0F, 0F);

		attachmentModel[40].addShapeBox(4F, -8F, 1F, 8, 4, 1, 0F, 0F, -0.5F, -3F, 0F, -0.5F, -3F, 0F, -1F, 3F, 0F, -1F, 3F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 53
		attachmentModel[40].setRotationPoint(0F, 0F, 0F);

		attachmentModel[41].addShapeBox(4F, -8F, -2F, 8, 4, 1, 0F, 0F, -1F, 3F, 0F, -1F, 3F, 0F, -0.5F, -3F, 0F, -0.5F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // Box 54
		attachmentModel[41].setRotationPoint(0F, 0F, 0F);

		renderOffset = 0F;

		//Based off the Ambircon SteamRifle positioning.

		flipAll();
	}
}