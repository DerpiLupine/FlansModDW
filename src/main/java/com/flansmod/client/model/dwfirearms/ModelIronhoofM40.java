package com.flansmod.client.model.dwfirearms; //Path where the model is located

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelIronhoofM40 extends ModelGun //Same as Filename
{
	int textureX = 512;
	int textureY = 256;

	public ModelIronhoofM40() //Same as Filename
	{
		gunModel = new ModelRendererTurbo[89];
		gunModel[0] = new ModelRendererTurbo(this, 157, 121, textureX, textureY); // body1
		gunModel[1] = new ModelRendererTurbo(this, 181, 105, textureX, textureY); // body6
		gunModel[2] = new ModelRendererTurbo(this, 1, 57, textureX, textureY); // foreRail1
		gunModel[3] = new ModelRendererTurbo(this, 191, 62, textureX, textureY); // body12
		gunModel[4] = new ModelRendererTurbo(this, 93, 4, textureX, textureY); // body17
		gunModel[5] = new ModelRendererTurbo(this, 112, 121, textureX, textureY); // Import GU,body4
		gunModel[6] = new ModelRendererTurbo(this, 112, 29, textureX, textureY); // Import GU,body9
		gunModel[7] = new ModelRendererTurbo(this, 112, 15, textureX, textureY); // railPart3
		gunModel[8] = new ModelRendererTurbo(this, 1, 3, textureX, textureY); // Import GU,tacRailEnd1
		gunModel[9] = new ModelRendererTurbo(this, 32, 10, textureX, textureY); // ironSightPart1
		gunModel[10] = new ModelRendererTurbo(this, 32, 1, textureX, textureY); // ironSightPart4
		gunModel[11] = new ModelRendererTurbo(this, 232, 68, textureX, textureY); // Box 39
		gunModel[12] = new ModelRendererTurbo(this, 112, 104, textureX, textureY); // Box 1
		gunModel[13] = new ModelRendererTurbo(this, 232, 68, textureX, textureY); // Box 2
		gunModel[14] = new ModelRendererTurbo(this, 58, 69, textureX, textureY); // Box 3
		gunModel[15] = new ModelRendererTurbo(this, 1, 76, textureX, textureY); // Box 14
		gunModel[16] = new ModelRendererTurbo(this, 112, 53, textureX, textureY); // Box 16
		gunModel[17] = new ModelRendererTurbo(this, 195, 42, textureX, textureY); // Box 26
		gunModel[18] = new ModelRendererTurbo(this, 112, 42, textureX, textureY); // Box 27
		gunModel[19] = new ModelRendererTurbo(this, 168, 63, textureX, textureY); // Box 29
		gunModel[20] = new ModelRendererTurbo(this, 149, 63, textureX, textureY); // Box 30
		gunModel[21] = new ModelRendererTurbo(this, 149, 63, textureX, textureY); // Box 31
		gunModel[22] = new ModelRendererTurbo(this, 149, 63, textureX, textureY); // Box 32
		gunModel[23] = new ModelRendererTurbo(this, 112, 63, textureX, textureY); // Box 33
		gunModel[24] = new ModelRendererTurbo(this, 112, 76, textureX, textureY); // Box 34
		gunModel[25] = new ModelRendererTurbo(this, 153, 88, textureX, textureY); // Box 84
		gunModel[26] = new ModelRendererTurbo(this, 153, 88, textureX, textureY); // Box 85
		gunModel[27] = new ModelRendererTurbo(this, 153, 88, textureX, textureY); // Box 86
		gunModel[28] = new ModelRendererTurbo(this, 299, 1, textureX, textureY); // Box 0
		gunModel[29] = new ModelRendererTurbo(this, 112, 1, textureX, textureY); // Box 104
		gunModel[30] = new ModelRendererTurbo(this, 112, 87, textureX, textureY); // Box 127
		gunModel[31] = new ModelRendererTurbo(this, 254, 122, textureX, textureY); // Box 135
		gunModel[32] = new ModelRendererTurbo(this, 252, 107, textureX, textureY); // Box 136
		gunModel[33] = new ModelRendererTurbo(this, 93, 1, textureX, textureY); // Box 137
		gunModel[34] = new ModelRendererTurbo(this, 98, 1, textureX, textureY); // Box 138
		gunModel[35] = new ModelRendererTurbo(this, 202, 123, textureX, textureY); // Box 154
		gunModel[36] = new ModelRendererTurbo(this, 277, 123, textureX, textureY); // Box 156
		gunModel[37] = new ModelRendererTurbo(this, 254, 122, textureX, textureY); // Box 157
		gunModel[38] = new ModelRendererTurbo(this, 277, 123, textureX, textureY); // Box 158
		gunModel[39] = new ModelRendererTurbo(this, 165, 3, textureX, textureY); // Box 159
		gunModel[40] = new ModelRendererTurbo(this, 266, 15, textureX, textureY); // Box 160
		gunModel[41] = new ModelRendererTurbo(this, 234, 4, textureX, textureY); // Box 162
		gunModel[42] = new ModelRendererTurbo(this, 64, 13, textureX, textureY); // Box 2
		gunModel[43] = new ModelRendererTurbo(this, 64, 9, textureX, textureY); // Box 3
		gunModel[44] = new ModelRendererTurbo(this, 64, 9, textureX, textureY); // Box 4
		gunModel[45] = new ModelRendererTurbo(this, 64, 13, textureX, textureY); // Box 5
		gunModel[46] = new ModelRendererTurbo(this, 64, 2, textureX, textureY); // Box 6
		gunModel[47] = new ModelRendererTurbo(this, 220, 90, textureX, textureY); // Box 0
		gunModel[48] = new ModelRendererTurbo(this, 194, 88, textureX, textureY); // Box 1
		gunModel[49] = new ModelRendererTurbo(this, 209, 83, textureX, textureY); // Box 2
		gunModel[50] = new ModelRendererTurbo(this, 209, 90, textureX, textureY); // Box 3
		gunModel[51] = new ModelRendererTurbo(this, 307, 6, textureX, textureY); // Box 10
		gunModel[52] = new ModelRendererTurbo(this, 307, 6, textureX, textureY); // Box 11
		gunModel[53] = new ModelRendererTurbo(this, 307, 6, textureX, textureY); // Box 12
		gunModel[54] = new ModelRendererTurbo(this, 153, 88, textureX, textureY); // Box 13
		gunModel[55] = new ModelRendererTurbo(this, 153, 88, textureX, textureY); // Box 14
		gunModel[56] = new ModelRendererTurbo(this, 153, 88, textureX, textureY); // Box 15
		gunModel[57] = new ModelRendererTurbo(this, 225, 24, textureX, textureY); // Box 16
		gunModel[58] = new ModelRendererTurbo(this, 225, 19, textureX, textureY); // Box 17
		gunModel[59] = new ModelRendererTurbo(this, 235, 91, textureX, textureY); // Box 26
		gunModel[60] = new ModelRendererTurbo(this, 235, 83, textureX, textureY); // Box 27
		gunModel[61] = new ModelRendererTurbo(this, 1, 40, textureX, textureY); // Box 35
		gunModel[62] = new ModelRendererTurbo(this, 1, 32, textureX, textureY); // Box 36
		gunModel[63] = new ModelRendererTurbo(this, 1, 48, textureX, textureY); // Box 37
		gunModel[64] = new ModelRendererTurbo(this, 72, 48, textureX, textureY); // Box 38
		gunModel[65] = new ModelRendererTurbo(this, 85, 51, textureX, textureY); // Box 39
		gunModel[66] = new ModelRendererTurbo(this, 299, 74, textureX, textureY); // Box 42
		gunModel[67] = new ModelRendererTurbo(this, 299, 81, textureX, textureY); // Box 43
		gunModel[68] = new ModelRendererTurbo(this, 62, 40, textureX, textureY); // Box 44
		gunModel[69] = new ModelRendererTurbo(this, 62, 32, textureX, textureY); // Box 45
		gunModel[70] = new ModelRendererTurbo(this, 84, 94, textureX, textureY); // body12
		gunModel[71] = new ModelRendererTurbo(this, 1, 93, textureX, textureY); // railPart1
		gunModel[72] = new ModelRendererTurbo(this, 63, 90, textureX, textureY); // railPart2
		gunModel[73] = new ModelRendererTurbo(this, 63, 90, textureX, textureY); // railPart3
		gunModel[74] = new ModelRendererTurbo(this, 63, 90, textureX, textureY); // railPart4
		gunModel[75] = new ModelRendererTurbo(this, 63, 90, textureX, textureY); // railPart5
		gunModel[76] = new ModelRendererTurbo(this, 63, 90, textureX, textureY); // railPart6
		gunModel[77] = new ModelRendererTurbo(this, 84, 86, textureX, textureY); // Box 0
		gunModel[78] = new ModelRendererTurbo(this, 1, 156, textureX, textureY); // Box 32
		gunModel[79] = new ModelRendererTurbo(this, 40, 156, textureX, textureY); // Box 33
		gunModel[80] = new ModelRendererTurbo(this, 1, 103, textureX, textureY); // Box 34
		gunModel[81] = new ModelRendererTurbo(this, 1, 115, textureX, textureY); // Box 35
		gunModel[82] = new ModelRendererTurbo(this, 1, 129, textureX, textureY); // Box 36
		gunModel[83] = new ModelRendererTurbo(this, 1, 145, textureX, textureY); // Box 38
		gunModel[84] = new ModelRendererTurbo(this, 38, 103, textureX, textureY); // Box 39
		gunModel[85] = new ModelRendererTurbo(this, 38, 115, textureX, textureY); // Box 40
		gunModel[86] = new ModelRendererTurbo(this, 38, 129, textureX, textureY); // Box 41
		gunModel[87] = new ModelRendererTurbo(this, 38, 145, textureX, textureY); // Box 42
		gunModel[88] = new ModelRendererTurbo(this, 86, 80, textureX, textureY); // Box 0

		gunModel[0].addShapeBox(0F, 0F, 0F, 12, 3, 10, 0F, -2F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, -2F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body1
		gunModel[0].setRotationPoint(-8F, -19F, -5F);

		gunModel[1].addShapeBox(0F, 0F, 0F, 25, 4, 10, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body6
		gunModel[1].setRotationPoint(-8F, -14F, -5F);

		gunModel[2].addBox(0F, 0F, 0F, 23, 8, 10, 0F); // foreRail1
		gunModel[2].setRotationPoint(60F, -14F, -5F);

		gunModel[3].addShapeBox(0F, 0F, 0F, 12, 4, 8, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body12
		gunModel[3].setRotationPoint(15F, -14F, -4F);

		gunModel[4].addBox(0F, 0F, 0F, 8, 3, 1, 0F); // body17
		gunModel[4].setRotationPoint(-1F, -9.5F, 4.5F);

		gunModel[5].addBox(0F, 0F, 0F, 12, 2, 10, 0F); // Import GU,body4
		gunModel[5].setRotationPoint(-8F, -16F, -5F);

		gunModel[6].addBox(0F, 0F, 0F, 75, 1, 10, 0F); // Import GU,body9
		gunModel[6].setRotationPoint(8F, -15F, -5F);

		gunModel[7].addBox(0F, 0F, 0F, 46, 3, 10, 0F); // railPart3
		gunModel[7].setRotationPoint(37F, -18F, -5F);

		gunModel[8].addBox(0F, 0F, 0F, 4, 7, 11, 0F); // Import GU,tacRailEnd1
		gunModel[8].setRotationPoint(4F, -21F, -5.5F);

		gunModel[9].addShapeBox(0F, 0F, 0F, 8, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F); // ironSightPart1
		gunModel[9].setRotationPoint(-4F, -21F, -4F);

		gunModel[10].addBox(0F, 0F, 0F, 6, 2, 6, 0F); // ironSightPart4
		gunModel[10].setRotationPoint(1F, -22.5F, -3F);

		gunModel[11].addShapeBox(0F, 0F, 0F, 17, 6, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F); // Box 39
		gunModel[11].setRotationPoint(63F, -13F, 5F);

		gunModel[12].addShapeBox(0F, 0F, 0F, 22, 6, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 1
		gunModel[12].setRotationPoint(-8F, -10F, -5F);

		gunModel[13].addShapeBox(0F, 0F, 0F, 17, 6, 1, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 2
		gunModel[13].setRotationPoint(63F, -13F, -6F);

		gunModel[14].addShapeBox(0F, 0F, 0F, 3, 6, 10, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F); // Box 3
		gunModel[14].setRotationPoint(33F, -10F, -5F);

		gunModel[15].addShapeBox(0F, 0F, 0F, 23, 2, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 14
		gunModel[15].setRotationPoint(60F, -6F, -5F);

		gunModel[16].addShapeBox(0F, 0F, 0F, 25, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 16
		gunModel[16].setRotationPoint(35F, -6F, -4F);

		gunModel[17].addShapeBox(0F, 0F, 0F, 33, 1, 8, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 26
		gunModel[17].setRotationPoint(27F, -14F, -4F);

		gunModel[18].addShapeBox(0F, 0F, 0F, 33, 2, 8, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 27
		gunModel[18].setRotationPoint(27F, -12F, -4F);

		gunModel[19].addShapeBox(0F, 0F, 0F, 3, 4, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F); // Box 29
		gunModel[19].setRotationPoint(57F, -10F, -4F);

		gunModel[20].addShapeBox(0F, 0F, 0F, 1, 4, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F); // Box 30
		gunModel[20].setRotationPoint(54F, -10F, -4F);

		gunModel[21].addShapeBox(0F, 0F, 0F, 1, 4, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F); // Box 31
		gunModel[21].setRotationPoint(51F, -10F, -4F);

		gunModel[22].addShapeBox(0F, 0F, 0F, 1, 4, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F); // Box 32
		gunModel[22].setRotationPoint(48F, -10F, -4F);

		gunModel[23].addShapeBox(0F, 0F, 0F, 9, 4, 8, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 33
		gunModel[23].setRotationPoint(35F, -10F, -4F);

		gunModel[24].addBox(0F, 0F, 0F, 35, 2, 7, 0F); // Box 34
		gunModel[24].setRotationPoint(25F, -14F, -3.5F);

		gunModel[25].addShapeBox(0F, 0F, 0F, 5, 7, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 84
		gunModel[25].setRotationPoint(8F, -20.5F, 1.5F);

		gunModel[26].addBox(0F, 0F, 0F, 5, 7, 3, 0F); // Box 85
		gunModel[26].setRotationPoint(8F, -20.5F, -1.5F);

		gunModel[27].addShapeBox(0F, 0F, 0F, 5, 7, 3, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 86
		gunModel[27].setRotationPoint(8F, -20.5F, -4.5F);

		gunModel[28].addShapeBox(0F, 0F, 0F, 1, 32, 40, 0F, 0F, -16F, 0F, 0F, -16F, 0F, 0F, -16F, -20F, 0F, -16F, -20F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -20F, 0F, 0F, -20F); // Box 0
		gunModel[28].setRotationPoint(5F, -49F, -5F);

		gunModel[29].addShapeBox(0F, 0F, 0F, 16, 3, 10, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F); // Box 104
		gunModel[29].setRotationPoint(37F, -21F, -5F);

		gunModel[30].addBox(0F, 0F, 0F, 13, 4, 7, 0F); // Box 127
		gunModel[30].setRotationPoint(44F, -10F, -3.5F);

		gunModel[31].addShapeBox(0F, 0F, 0F, 1, 2, 10, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, -1F, 0F, -1.5F, -1F, 0F, 0F, 0F); // Box 135
		gunModel[31].setRotationPoint(14F, -6F, -5F);

		gunModel[32].addBox(0F, 0F, 0F, 19, 3, 10, 0F); // Box 136
		gunModel[32].setRotationPoint(14F, -10F, -5F);

		gunModel[33].addBox(0F, 0F, 0F, 1, 1, 1, 0F); // Box 137
		gunModel[33].setRotationPoint(8F, -9.5F, 4.2F);

		gunModel[34].addBox(0F, 0F, 0F, 1, 1, 1, 0F); // Box 138
		gunModel[34].setRotationPoint(7.5F, -7.5F, 4.2F);

		gunModel[35].addShapeBox(0F, 0F, 0F, 17, 1, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 154
		gunModel[35].setRotationPoint(15F, -7F, -5F);

		gunModel[36].addShapeBox(0F, 0F, 0F, 1, 1, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 156
		gunModel[36].setRotationPoint(14F, -7F, -5F);

		gunModel[37].addShapeBox(0F, 0F, 0F, 1, 2, 10, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -1.5F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, -1F); // Box 157
		gunModel[37].setRotationPoint(32F, -6F, -5F);

		gunModel[38].addShapeBox(0F, 0F, 0F, 1, 1, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 158
		gunModel[38].setRotationPoint(32F, -7F, -5F);

		gunModel[39].addShapeBox(0F, 0F, 0F, 25, 2, 9, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0.5F, -0.5F, 2F, 0.5F, -0.5F, 2F, 0.5F, -0.5F, 0F, 0.5F, -0.5F); // Box 159
		gunModel[39].setRotationPoint(53F, -21F, -4.5F);

		gunModel[40].addShapeBox(0F, 0F, 0F, 5, 3, 10, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F); // Box 160
		gunModel[40].setRotationPoint(78F, -21F, -5F);

		gunModel[41].addShapeBox(0F, 0F, 0F, 27, 1, 9, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -2F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, -2F, 0F, 0.5F); // Box 162
		gunModel[41].setRotationPoint(53F, -19F, -4.5F);

		gunModel[42].addShapeBox(0F, 0F, 0F, 5, 1, 1, 0F, -2F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 2
		gunModel[42].setRotationPoint(78F, -25F, -2.5F);

		gunModel[43].addShapeBox(0F, 0F, 0F, 8, 2, 1, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 3
		gunModel[43].setRotationPoint(75F, -24F, -2.5F);

		gunModel[44].addShapeBox(0F, 0F, 0F, 8, 2, 1, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 4
		gunModel[44].setRotationPoint(75F, -24F, 1.5F);

		gunModel[45].addShapeBox(0F, 0F, 0F, 5, 1, 1, 0F, -2F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 5
		gunModel[45].setRotationPoint(78F, -25F, 1.5F);

		gunModel[46].addBox(0F, 0F, 0F, 9, 1, 5, 0F); // Box 6
		gunModel[46].setRotationPoint(74F, -22F, -2.5F);

		gunModel[47].addShapeBox(0F, 0F, 0F, 1, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 0
		gunModel[47].setRotationPoint(13F, 0F, -3F);

		gunModel[48].addBox(0F, 0F, 0F, 1, 4, 6, 0F); // Box 1
		gunModel[48].setRotationPoint(13F, -4F, -3F);

		gunModel[49].addShapeBox(0F, 0F, 0F, 1, 2, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F); // Box 2
		gunModel[49].setRotationPoint(13F, 2F, -2F);

		gunModel[50].addBox(0F, 0F, 0F, 1, 4, 4, 0F); // Box 3
		gunModel[50].setRotationPoint(12F, 4F, -2F);

		gunModel[51].addShapeBox(0F, 0F, 0F, 2, 6, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F); // Box 10
		gunModel[51].setRotationPoint(82.5F, -18.5F, 1F);

		gunModel[52].addBox(0F, 0F, 0F, 2, 6, 2, 0F); // Box 11
		gunModel[52].setRotationPoint(82.5F, -18.5F, -1F);

		gunModel[53].addShapeBox(0F, 0F, 0F, 2, 6, 2, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 12
		gunModel[53].setRotationPoint(82.5F, -18.5F, -3F);

		gunModel[54].addShapeBox(0F, 0F, 0F, 5, 7, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 13
		gunModel[54].setRotationPoint(32F, -20.5F, 1.5F);

		gunModel[55].addBox(0F, 0F, 0F, 5, 7, 3, 0F); // Box 14
		gunModel[55].setRotationPoint(32F, -20.5F, -1.5F);

		gunModel[56].addShapeBox(0F, 0F, 0F, 5, 7, 3, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 15
		gunModel[56].setRotationPoint(32F, -20.5F, -4.5F);

		gunModel[57].addBox(0F, 0F, 0F, 19, 3, 1, 0F); // Box 16
		gunModel[57].setRotationPoint(13F, -18F, -4F);

		gunModel[58].addBox(0F, 0F, 0F, 19, 3, 1, 0F); // Box 17
		gunModel[58].setRotationPoint(13F, -18F, 3F);

		gunModel[59].addBox(0F, 0F, 0F, 5, 2, 5, 0F); // Box 26
		gunModel[59].setRotationPoint(27F, -16.5F, -2.5F);

		gunModel[60].addShapeBox(0F, 0F, 0F, 5, 2, 5, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 27
		gunModel[60].setRotationPoint(27F, -18.5F, -2.5F);

		gunModel[61].addShapeBox(0F, 0F, 0F, 25, 1, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 35
		gunModel[61].setRotationPoint(38F, 2F, -2.5F);

		gunModel[62].addBox(0F, 0F, 0F, 25, 2, 5, 0F); // Box 36
		gunModel[62].setRotationPoint(38F, 0F, -2.5F);

		gunModel[63].addShapeBox(0F, 0F, 0F, 32, 5, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -5F, 0F, 0F, -5F, 0F, 0F, 0F, 0F, 0F); // Box 37
		gunModel[63].setRotationPoint(36F, -3F, -1.5F);

		gunModel[64].addShapeBox(0F, 0F, 0F, 3, 5, 3, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 38
		gunModel[64].setRotationPoint(34F, -5F, -1.5F);

		gunModel[65].addShapeBox(0F, 0F, 0F, 2, 2, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -1F, 0F); // Box 39
		gunModel[65].setRotationPoint(34F, 0F, -1.5F);

		gunModel[66].addShapeBox(0F, 0F, 0F, 17, 1, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 42
		gunModel[66].setRotationPoint(67F, -1F, -2.5F);

		gunModel[67].addBox(0F, 0F, 0F, 17, 12, 5, 0F); // Box 43
		gunModel[67].setRotationPoint(67F, -13F, -2.5F);

		gunModel[68].addShapeBox(0F, 0F, 0F, 4, 1, 5, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -3F, -1F, 0F, -3F, -1F, 0F, 0F, -1F); // Box 44
		gunModel[68].setRotationPoint(63F, 2F, -2.5F);

		gunModel[69].addShapeBox(0F, 0F, 0F, 4, 2, 5, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F); // Box 45
		gunModel[69].setRotationPoint(63F, 0F, -2.5F);

		gunModel[70].addBox(0F, 0F, 0F, 3, 3, 2, 0F); // body12
		gunModel[70].setRotationPoint(38.5F, -17.5F, 4F);

		gunModel[71].addBox(0F, 0F, 0F, 27, 2, 7, 0F); // railPart1
		gunModel[71].setRotationPoint(15F, -22.5F, -3.5F);

		gunModel[72].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // railPart2
		gunModel[72].setRotationPoint(15F, -24.5F, -3.5F);

		gunModel[73].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // railPart3
		gunModel[73].setRotationPoint(21F, -24.5F, -3.5F);

		gunModel[74].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // railPart4
		gunModel[74].setRotationPoint(27F, -24.5F, -3.5F);

		gunModel[75].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // railPart5
		gunModel[75].setRotationPoint(33F, -24.5F, -3.5F);

		gunModel[76].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // railPart6
		gunModel[76].setRotationPoint(39F, -24.5F, -3.5F);

		gunModel[77].addBox(0F, 0F, 0F, 4, 4, 2, 0F); // Box 0
		gunModel[77].setRotationPoint(38F, -19.5F, 3.5F);

		gunModel[78].addShapeBox(0F, 0F, 0F, 11, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F); // Box 32
		gunModel[78].setRotationPoint(-8F, -4F, -4F);

		gunModel[79].addBox(0F, 0F, 0F, 6, 3, 8, 0F); // Box 33
		gunModel[79].setRotationPoint(3F, -4F, -4F);

		gunModel[80].addShapeBox(0F, 0F, 0F, 10, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 34
		gunModel[80].setRotationPoint(-6F, -1F, -4F);

		gunModel[81].addShapeBox(0F, 0F, 0F, 10, 5, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F); // Box 35
		gunModel[81].setRotationPoint(-6F, 2F, -4F);

		gunModel[82].addShapeBox(0F, 0F, 0F, 10, 7, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, -3F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 3F, -3F, 0F); // Box 36
		gunModel[82].setRotationPoint(-8F, 7F, -4F);

		gunModel[83].addShapeBox(0F, 0F, 0F, 10, 2, 8, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F); // Box 38
		gunModel[83].setRotationPoint(-11F, 14F, -4F);

		gunModel[84].addShapeBox(0F, 0F, 0F, 2, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F); // Box 39
		gunModel[84].setRotationPoint(4F, -1F, -4F);

		gunModel[85].addShapeBox(0F, 0F, 0F, 2, 5, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, -1.5F, -2F, 0F, -1.5F, 2F, 0F, 0F); // Box 40
		gunModel[85].setRotationPoint(4F, 2F, -4F);

		gunModel[86].addShapeBox(0F, 0F, 0F, 2, 7, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, -1.5F, -3F, 0F, -1.5F, 3F, 0F, 0F); // Box 41
		gunModel[86].setRotationPoint(2F, 7F, -4F);

		gunModel[87].addShapeBox(0F, 0F, 0F, 2, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F); // Box 42
		gunModel[87].setRotationPoint(-1F, 14F, -4F);

		gunModel[88].addShapeBox(0F, 0F, 0F, 4, 3, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 0
		gunModel[88].setRotationPoint(38F, -22.5F, 3.5F);


		defaultBarrelModel = new ModelRendererTurbo[3];
		defaultBarrelModel[0] = new ModelRendererTurbo(this, 183, 89, textureX, textureY); // Box 11
		defaultBarrelModel[1] = new ModelRendererTurbo(this, 170, 88, textureX, textureY); // Box 12
		defaultBarrelModel[2] = new ModelRendererTurbo(this, 183, 89, textureX, textureY); // Box 13

		defaultBarrelModel[0].addShapeBox(0F, 0F, 0F, 3, 7, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F); // Box 11
		defaultBarrelModel[0].setRotationPoint(84F, -19F, 1.5F);

		defaultBarrelModel[1].addBox(0F, 0F, 0F, 3, 7, 3, 0F); // Box 12
		defaultBarrelModel[1].setRotationPoint(84F, -19F, -1.5F);

		defaultBarrelModel[2].addShapeBox(0F, 0F, 0F, 3, 7, 2, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 13
		defaultBarrelModel[2].setRotationPoint(84F, -19F, -3.5F);


		defaultStockModel = new ModelRendererTurbo[12];
		defaultStockModel[0] = new ModelRendererTurbo(this, 1, 181, textureX, textureY); // Box 8
		defaultStockModel[1] = new ModelRendererTurbo(this, 1, 220, textureX, textureY); // Box 15
		defaultStockModel[2] = new ModelRendererTurbo(this, 1, 199, textureX, textureY); // Box 16
		defaultStockModel[3] = new ModelRendererTurbo(this, 1, 193, textureX, textureY); // Box 111
		defaultStockModel[4] = new ModelRendererTurbo(this, 1, 193, textureX, textureY); // Box 112
		defaultStockModel[5] = new ModelRendererTurbo(this, 1, 193, textureX, textureY); // Box 128
		defaultStockModel[6] = new ModelRendererTurbo(this, 1, 181, textureX, textureY); // Box 129
		defaultStockModel[7] = new ModelRendererTurbo(this, 1, 193, textureX, textureY); // Box 130
		defaultStockModel[8] = new ModelRendererTurbo(this, 1, 220, textureX, textureY); // Box 131
		defaultStockModel[9] = new ModelRendererTurbo(this, 36, 203, textureX, textureY); // Box 132
		defaultStockModel[10] = new ModelRendererTurbo(this, 36, 221, textureX, textureY); // Box 133
		defaultStockModel[11] = new ModelRendererTurbo(this, 67, 221, textureX, textureY); // Box 134

		defaultStockModel[0].addBox(0F, 0F, 0F, 34, 8, 3, 0F); // Box 8
		defaultStockModel[0].setRotationPoint(-42F, -14F, -4.5F);

		defaultStockModel[1].addShapeBox(0F, 0F, 0F, 7, 2, 10, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 15
		defaultStockModel[1].setRotationPoint(-49F, -17F, -5F);

		defaultStockModel[2].addShapeBox(0F, 0F, 0F, 7, 10, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 16
		defaultStockModel[2].setRotationPoint(-49F, -15F, -5F);

		defaultStockModel[3].addShapeBox(0F, 0F, 0F, 34, 2, 3, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 111
		defaultStockModel[3].setRotationPoint(-42F, -16F, -4.5F);

		defaultStockModel[4].addShapeBox(0F, 0F, 0F, 34, 2, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 112
		defaultStockModel[4].setRotationPoint(-42F, -6F, -4.5F);

		defaultStockModel[5].addShapeBox(0F, 0F, 0F, 34, 2, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 128
		defaultStockModel[5].setRotationPoint(-42F, -6F, 1.5F);

		defaultStockModel[6].addBox(0F, 0F, 0F, 34, 8, 3, 0F); // Box 129
		defaultStockModel[6].setRotationPoint(-42F, -14F, 1.5F);

		defaultStockModel[7].addShapeBox(0F, 0F, 0F, 34, 2, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 130
		defaultStockModel[7].setRotationPoint(-42F, -16F, 1.5F);

		defaultStockModel[8].addShapeBox(0F, 0F, 0F, 7, 2, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 131
		defaultStockModel[8].setRotationPoint(-49F, -5F, -5F);

		defaultStockModel[9].addShapeBox(0F, 0F, 0F, 7, 8, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 132
		defaultStockModel[9].setRotationPoint(-49F, -3F, -4F);

		defaultStockModel[10].addShapeBox(0F, 0F, 0F, 7, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, -3F, 0F, -1F, -3F, 0F, -1F, 0F, 0F, -1F); // Box 133
		defaultStockModel[10].setRotationPoint(-49F, 5F, -4F);

		defaultStockModel[11].addShapeBox(0F, 0F, 0F, 4, 6, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 0F, 0F); // Box 134
		defaultStockModel[11].setRotationPoint(-42F, -4F, -2.5F);


		ammoModel = new ModelRendererTurbo[18];
		ammoModel[0] = new ModelRendererTurbo(this, 254, 172, textureX, textureY); // Box 141
		ammoModel[1] = new ModelRendererTurbo(this, 197, 170, textureX, textureY); // Box 146
		ammoModel[2] = new ModelRendererTurbo(this, 112, 170, textureX, textureY); // Box 151
		ammoModel[3] = new ModelRendererTurbo(this, 282, 143, textureX, textureY); // Box 4
		ammoModel[4] = new ModelRendererTurbo(this, 197, 144, textureX, textureY); // Box 5
		ammoModel[5] = new ModelRendererTurbo(this, 197, 144, textureX, textureY); // Box 6
		ammoModel[6] = new ModelRendererTurbo(this, 112, 207, textureX, textureY); // Box 7
		ammoModel[7] = new ModelRendererTurbo(this, 305, 183, textureX, textureY); // Box 8
		ammoModel[8] = new ModelRendererTurbo(this, 112, 144, textureX, textureY); // Box 9
		ammoModel[9] = new ModelRendererTurbo(this, 197, 209, textureX, textureY); // Box 21
		ammoModel[10] = new ModelRendererTurbo(this, 197, 220, textureX, textureY); // Box 22
		ammoModel[11] = new ModelRendererTurbo(this, 173, 144, textureX, textureY); // Box 24
		ammoModel[12] = new ModelRendererTurbo(this, 173, 150, textureX, textureY); // Box 25
		ammoModel[13] = new ModelRendererTurbo(this, 112, 161, textureX, textureY); // Box 28
		ammoModel[14] = new ModelRendererTurbo(this, 112, 153, textureX, textureY); // Box 29
		ammoModel[15] = new ModelRendererTurbo(this, 112, 153, textureX, textureY); // Box 30
		ammoModel[16] = new ModelRendererTurbo(this, 112, 161, textureX, textureY); // Box 31
		ammoModel[17] = new ModelRendererTurbo(this, 112, 148, textureX, textureY); // Box 32

		ammoModel[0].addBox(0F, 0F, 0F, 1, 12, 24, 0F); // Box 141
		ammoModel[0].setRotationPoint(14.5F, -2.5F, -8F);

		ammoModel[1].addBox(0F, 0F, 0F, 5, 15, 23, 0F); // Box 146
		ammoModel[1].setRotationPoint(15.5F, -5F, -7.5F);

		ammoModel[2].addBox(0F, 0F, 0F, 12, 12, 24, 0F); // Box 151
		ammoModel[2].setRotationPoint(20.5F, -2.5F, -8F);

		ammoModel[3].addBox(0F, 0F, 0F, 18, 2, 24, 0F); // Box 4
		ammoModel[3].setRotationPoint(14.5F, 9.5F, -8F);

		ammoModel[4].addBox(0F, 0F, 0F, 18, 1, 24, 0F); // Box 5
		ammoModel[4].setRotationPoint(14.5F, -3.5F, -8F);

		ammoModel[5].addBox(0F, 0F, 0F, 18, 1, 24, 0F); // Box 6
		ammoModel[5].setRotationPoint(14.5F, -5.5F, -8F);

		ammoModel[6].addBox(0F, 0F, 0F, 12, 1, 24, 0F); // Box 7
		ammoModel[6].setRotationPoint(20.5F, -4.5F, -8F);

		ammoModel[7].addBox(0F, 0F, 0F, 1, 1, 24, 0F); // Box 8
		ammoModel[7].setRotationPoint(14.5F, -4.5F, -8F);

		ammoModel[8].addShapeBox(0F, 0F, 0F, 18, 1, 24, 0F, -1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 9
		ammoModel[8].setRotationPoint(14.5F, -6.5F, -8F);

		ammoModel[9].addShapeBox(0F, 0F, 0F, 16, 2, 8, 0F, -1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 21
		ammoModel[9].setRotationPoint(15.5F, -9.5F, -4F);

		ammoModel[10].addBox(0F, 0F, 0F, 16, 1, 8, 0F); // Box 22
		ammoModel[10].setRotationPoint(15.5F, -7.5F, -4F);

		ammoModel[11].addShapeBox(0F, 0F, 0F, 9, 1, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 24
		ammoModel[11].setRotationPoint(17.5F, -10.5F, -2F);

		ammoModel[12].addShapeBox(0F, 0F, 0F, 3, 1, 4, 0F, 0F, 0F, -1F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 25
		ammoModel[12].setRotationPoint(26.5F, -10.5F, -2F);

		ammoModel[13].addShapeBox(0F, 0F, 0F, 3, 1, 5, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -3F, -1F, -0.5F, -3F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, 4F, 0F); // Box 28
		ammoModel[13].setRotationPoint(17.5F, -9.5F, 5F);

		ammoModel[14].addShapeBox(0F, 0F, 0F, 3, 2, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, -1F, 0F, -3F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, 4F, 0F); // Box 29
		ammoModel[14].setRotationPoint(17.5F, -8.5F, 5F);

		ammoModel[15].addShapeBox(0F, 0F, 0F, 3, 2, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, -1F, 0F, -3F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, 4F, 0F); // Box 30
		ammoModel[15].setRotationPoint(27.5F, -8.5F, 5F);

		ammoModel[16].addShapeBox(0F, 0F, 0F, 3, 1, 5, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -3F, -1F, -0.5F, -3F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, 4F, 0F); // Box 31
		ammoModel[16].setRotationPoint(27.5F, -9.5F, 5F);

		ammoModel[17].addShapeBox(0F, 0F, 0F, 7, 2, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 32
		ammoModel[17].setRotationPoint(20.5F, -8.5F, 5F);


		slideModel = new ModelRendererTurbo[5];
		slideModel[0] = new ModelRendererTurbo(this, 256, 90, textureX, textureY); // Box 18
		slideModel[1] = new ModelRendererTurbo(this, 256, 81, textureX, textureY); // Box 19
		slideModel[2] = new ModelRendererTurbo(this, 256, 90, textureX, textureY); // Box 20
		slideModel[3] = new ModelRendererTurbo(this, 93, 9, textureX, textureY); // Box 33
		slideModel[4] = new ModelRendererTurbo(this, 93, 9, textureX, textureY); // Box 34

		slideModel[0].addShapeBox(0F, 0F, 0F, 19, 6, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F); // Box 18
		slideModel[0].setRotationPoint(13F, -20F, 1F);

		slideModel[1].addBox(0F, 0F, 0F, 19, 6, 2, 0F); // Box 19
		slideModel[1].setRotationPoint(13F, -20F, -1F);

		slideModel[2].addShapeBox(0F, 0F, 0F, 19, 6, 2, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 20
		slideModel[2].setRotationPoint(13F, -20F, -3F);

		slideModel[3].addShapeBox(0F, 0F, 0F, 2, 1, 5, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 33
		slideModel[3].setRotationPoint(29F, -17.5F, -9F);

		slideModel[4].addShapeBox(0F, 0F, 0F, 2, 1, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 34
		slideModel[4].setRotationPoint(29F, -16.5F, -9F);

		stockAttachPoint = new Vector3f(-8F /16F, 10F /16F, 0F /16F);
		scopeAttachPoint = new Vector3f(28F /16F, 20F /16F, 0F /16F);

		gunSlideDistance = 0.8F;
		animationType = EnumAnimationType.BOTTOM_CLIP;

		translateAll(0F, 0F, 0F);


		flipAll();
	}
}