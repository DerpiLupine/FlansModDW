package com.flansmod.client.model.dwfirearms; //Path where the model is located

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelPAR38 extends ModelGun //Same as Filename
{
	int textureX = 512;
	int textureY = 256;

	public ModelPAR38() //Same as Filename
	{
		gunModel = new ModelRendererTurbo[69];
		gunModel[0] = new ModelRendererTurbo(this, 100, 15, textureX, textureY); // Box 1
		gunModel[1] = new ModelRendererTurbo(this, 100, 42, textureX, textureY); // Box 2
		gunModel[2] = new ModelRendererTurbo(this, 135, 61, textureX, textureY); // Box 3
		gunModel[3] = new ModelRendererTurbo(this, 100, 61, textureX, textureY); // Box 5
		gunModel[4] = new ModelRendererTurbo(this, 167, 42, textureX, textureY); // Box 7
		gunModel[5] = new ModelRendererTurbo(this, 100, 94, textureX, textureY); // Box 8
		gunModel[6] = new ModelRendererTurbo(this, 144, 42, textureX, textureY); // Box 9
		gunModel[7] = new ModelRendererTurbo(this, 1, 63, textureX, textureY); // Box 10
		gunModel[8] = new ModelRendererTurbo(this, 38, 63, textureX, textureY); // Box 11
		gunModel[9] = new ModelRendererTurbo(this, 1, 74, textureX, textureY); // Box 14
		gunModel[10] = new ModelRendererTurbo(this, 32, 74, textureX, textureY); // Box 15
		gunModel[11] = new ModelRendererTurbo(this, 1, 53, textureX, textureY); // Box 16
		gunModel[12] = new ModelRendererTurbo(this, 1, 26, textureX, textureY); // Box 17
		gunModel[13] = new ModelRendererTurbo(this, 100, 77, textureX, textureY); // Box 18
		gunModel[14] = new ModelRendererTurbo(this, 163, 102, textureX, textureY); // Box 19
		gunModel[15] = new ModelRendererTurbo(this, 100, 39, textureX, textureY); // Box 20
		gunModel[16] = new ModelRendererTurbo(this, 100, 84, textureX, textureY); // Box 22
		gunModel[17] = new ModelRendererTurbo(this, 100, 27, textureX, textureY); // Box 23
		gunModel[18] = new ModelRendererTurbo(this, 46, 37, textureX, textureY); // Box 24
		gunModel[19] = new ModelRendererTurbo(this, 156, 62, textureX, textureY); // Box 33
		gunModel[20] = new ModelRendererTurbo(this, 173, 39, textureX, textureY); // Box 34
		gunModel[21] = new ModelRendererTurbo(this, 145, 102, textureX, textureY); // Box 38
		gunModel[22] = new ModelRendererTurbo(this, 150, 107, textureX, textureY); // Box 39
		gunModel[23] = new ModelRendererTurbo(this, 150, 102, textureX, textureY); // Box 40
		gunModel[24] = new ModelRendererTurbo(this, 145, 102, textureX, textureY); // Box 41
		gunModel[25] = new ModelRendererTurbo(this, 150, 102, textureX, textureY); // Box 42
		gunModel[26] = new ModelRendererTurbo(this, 150, 107, textureX, textureY); // Box 43
		gunModel[27] = new ModelRendererTurbo(this, 200, 102, textureX, textureY); // Box 44
		gunModel[28] = new ModelRendererTurbo(this, 150, 102, textureX, textureY); // Box 45
		gunModel[29] = new ModelRendererTurbo(this, 150, 107, textureX, textureY); // Box 46
		gunModel[30] = new ModelRendererTurbo(this, 150, 99, textureX, textureY); // Box 47
		gunModel[31] = new ModelRendererTurbo(this, 150, 99, textureX, textureY); // Box 48
		gunModel[32] = new ModelRendererTurbo(this, 150, 99, textureX, textureY); // Box 49
		gunModel[33] = new ModelRendererTurbo(this, 145, 94, textureX, textureY); // Box 50
		gunModel[34] = new ModelRendererTurbo(this, 150, 94, textureX, textureY); // Box 51
		gunModel[35] = new ModelRendererTurbo(this, 150, 94, textureX, textureY); // Box 52
		gunModel[36] = new ModelRendererTurbo(this, 145, 94, textureX, textureY); // Box 53
		gunModel[37] = new ModelRendererTurbo(this, 150, 94, textureX, textureY); // Box 54
		gunModel[38] = new ModelRendererTurbo(this, 200, 94, textureX, textureY); // Box 55
		gunModel[39] = new ModelRendererTurbo(this, 163, 94, textureX, textureY); // Box 56
		gunModel[40] = new ModelRendererTurbo(this, 100, 2, textureX, textureY); // Box 57
		gunModel[41] = new ModelRendererTurbo(this, 100, 8, textureX, textureY); // Box 58
		gunModel[42] = new ModelRendererTurbo(this, 100, 2, textureX, textureY); // Box 60
		gunModel[43] = new ModelRendererTurbo(this, 230, 2, textureX, textureY); // Box 64
		gunModel[44] = new ModelRendererTurbo(this, 230, 10, textureX, textureY); // Box 65
		gunModel[45] = new ModelRendererTurbo(this, 230, 2, textureX, textureY); // Box 66
		gunModel[46] = new ModelRendererTurbo(this, 1, 43, textureX, textureY); // Box 67
		gunModel[47] = new ModelRendererTurbo(this, 22, 1, textureX, textureY); // Box 68
		gunModel[48] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 69
		gunModel[49] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 70
		gunModel[50] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 71
		gunModel[51] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 72
		gunModel[52] = new ModelRendererTurbo(this, 46, 45, textureX, textureY); // Box 75
		gunModel[53] = new ModelRendererTurbo(this, 100, 110, textureX, textureY); // Box 76
		gunModel[54] = new ModelRendererTurbo(this, 141, 111, textureX, textureY); // Box 77
		gunModel[55] = new ModelRendererTurbo(this, 1, 19, textureX, textureY); // Box 78
		gunModel[56] = new ModelRendererTurbo(this, 1, 35, textureX, textureY); // Box 80
		gunModel[57] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 81
		gunModel[58] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 82
		gunModel[59] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 83
		gunModel[60] = new ModelRendererTurbo(this, 69, 1, textureX, textureY); // Box 85
		gunModel[61] = new ModelRendererTurbo(this, 121, 42, textureX, textureY); // Box 89
		gunModel[62] = new ModelRendererTurbo(this, 1, 140, textureX, textureY); // Box 90
		gunModel[63] = new ModelRendererTurbo(this, 44, 15, textureX, textureY); // Box 91
		gunModel[64] = new ModelRendererTurbo(this, 44, 15, textureX, textureY); // Box 92
		gunModel[65] = new ModelRendererTurbo(this, 174, 111, textureX, textureY); // Box 93
		gunModel[66] = new ModelRendererTurbo(this, 174, 114, textureX, textureY); // Box 94
		gunModel[67] = new ModelRendererTurbo(this, 49, 84, textureX, textureY); // Box 116
		gunModel[68] = new ModelRendererTurbo(this, 49, 74, textureX, textureY); // Box 117

		gunModel[0].addShapeBox(0F, 0F, 0F, 45, 3, 8, 0F, -3F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, -3F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 1
		gunModel[0].setRotationPoint(-10F, -19F, -4F);

		gunModel[1].addShapeBox(0F, 0F, 0F, 2, 10, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F); // Box 2
		gunModel[1].setRotationPoint(-10F, -16F, -4F);

		gunModel[2].addShapeBox(0F, 0F, 0F, 2, 7, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F); // Box 3
		gunModel[2].setRotationPoint(-5F, -16F, -4F);

		gunModel[3].addBox(0F, 0F, 0F, 9, 7, 8, 0F); // Box 5
		gunModel[3].setRotationPoint(-3F, -16F, -4F);

		gunModel[4].addBox(0F, 0F, 0F, 6, 10, 8, 0F); // Box 7
		gunModel[4].setRotationPoint(6F, -16F, -4F);

		gunModel[5].addShapeBox(0F, 0F, 0F, 14, 7, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F); // Box 8
		gunModel[5].setRotationPoint(12F, -11F, -4F);

		gunModel[6].addShapeBox(0F, 0F, 0F, 3, 10, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 0F, 0F); // Box 9
		gunModel[6].setRotationPoint(26F, -16F, -4F);

		gunModel[7].addBox(0F, 0F, 0F, 11, 3, 7, 0F); // Box 10
		gunModel[7].setRotationPoint(-5F, -9F, -3.5F);

		gunModel[8].addShapeBox(0F, 0F, 0F, 11, 3, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F); // Box 11
		gunModel[8].setRotationPoint(-5F, -6F, -3.5F);

		gunModel[9].addShapeBox(0F, 0F, 0F, 8, 11, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, -2F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 4F, -2F, 0F); // Box 14
		gunModel[9].setRotationPoint(-3F, -3F, -3.5F);

		gunModel[10].addShapeBox(0F, 0F, 0F, 1, 11, 7, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 4F, 0F, 0F, -4F, 0F, -1F, -4F, 0F, -1F, 4F, 0F, 0F); // Box 15
		gunModel[10].setRotationPoint(5F, -3F, -3.5F);

		gunModel[11].addBox(0F, 0F, 0F, 16, 2, 7, 0F); // Box 16
		gunModel[11].setRotationPoint(-6F, -19.2F, -3.5F);

		gunModel[12].addBox(0F, 0F, 0F, 15, 1, 7, 0F); // Box 17
		gunModel[12].setRotationPoint(-6F, -20.2F, -3.5F);

		gunModel[13].addBox(0F, 0F, 0F, 26, 1, 5, 0F); // Box 18
		gunModel[13].setRotationPoint(9F, -20F, -2.5F);

		gunModel[14].addShapeBox(0F, 0F, 0F, 17, 6, 1, 0F, -3F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, -3F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 19
		gunModel[14].setRotationPoint(26F, -17.5F, -5F);

		gunModel[15].addBox(0F, 0F, 0F, 35, 1, 1, 0F); // Box 20
		gunModel[15].setRotationPoint(26F, -11.5F, -5F);

		gunModel[16].addShapeBox(0F, 0F, 0F, 32, 1, 8, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 22
		gunModel[16].setRotationPoint(29F, -18.5F, -4F);

		gunModel[17].addShapeBox(0F, 0F, 0F, 32, 1, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 23
		gunModel[17].setRotationPoint(29F, -10.5F, -5F);

		gunModel[18].addBox(0F, 0F, 0F, 5, 2, 5, 0F); // Box 24
		gunModel[18].setRotationPoint(35F, -20.5F, -2.5F);

		gunModel[19].addBox(0F, 0F, 0F, 6, 6, 8, 0F); // Box 33
		gunModel[19].setRotationPoint(29F, -16F, -4F);

		gunModel[20].addBox(0F, 0F, 0F, 35, 1, 1, 0F); // Box 34
		gunModel[20].setRotationPoint(26F, -11.5F, 4F);

		gunModel[21].addShapeBox(0F, 0F, 0F, 1, 6, 1, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 38
		gunModel[21].setRotationPoint(48F, -17.5F, -5F);

		gunModel[22].addShapeBox(0F, 0F, 0F, 5, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.15F, 0F, 0F, 0.15F, 0F, 0F, -0.15F, 0F, 0F, -0.15F); // Box 39
		gunModel[22].setRotationPoint(43F, -17.5F, -4F);

		gunModel[23].addShapeBox(0F, 0F, 0F, 5, 3, 1, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 40
		gunModel[23].setRotationPoint(43F, -14.5F, -5F);

		gunModel[24].addShapeBox(0F, 0F, 0F, 1, 6, 1, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 41
		gunModel[24].setRotationPoint(54F, -17.5F, -5F);

		gunModel[25].addShapeBox(0F, 0F, 0F, 5, 3, 1, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 42
		gunModel[25].setRotationPoint(49F, -14.5F, -5F);

		gunModel[26].addShapeBox(0F, 0F, 0F, 5, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.15F, 0F, 0F, 0.15F, 0F, 0F, -0.15F, 0F, 0F, -0.15F); // Box 43
		gunModel[26].setRotationPoint(49F, -17.5F, -4F);

		gunModel[27].addShapeBox(0F, 0F, 0F, 1, 6, 1, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 44
		gunModel[27].setRotationPoint(60F, -17.5F, -5F);

		gunModel[28].addShapeBox(0F, 0F, 0F, 5, 3, 1, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 45
		gunModel[28].setRotationPoint(55F, -14.5F, -5F);

		gunModel[29].addShapeBox(0F, 0F, 0F, 5, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.15F, 0F, 0F, 0.15F, 0F, 0F, -0.15F, 0F, 0F, -0.15F); // Box 46
		gunModel[29].setRotationPoint(55F, -17.5F, -4F);

		gunModel[30].addShapeBox(0F, 0F, 0F, 5, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.15F, 0F, 0F, -0.15F, 0F, 0F, 0.15F, 0F, 0F, 0.15F); // Box 47
		gunModel[30].setRotationPoint(55F, -17.5F, 3F);

		gunModel[31].addShapeBox(0F, 0F, 0F, 5, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.15F, 0F, 0F, -0.15F, 0F, 0F, 0.15F, 0F, 0F, 0.15F); // Box 48
		gunModel[31].setRotationPoint(49F, -17.5F, 3F);

		gunModel[32].addShapeBox(0F, 0F, 0F, 5, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.15F, 0F, 0F, -0.15F, 0F, 0F, 0.15F, 0F, 0F, 0.15F); // Box 49
		gunModel[32].setRotationPoint(43F, -17.5F, 3F);

		gunModel[33].addShapeBox(0F, 0F, 0F, 1, 6, 1, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 50
		gunModel[33].setRotationPoint(48F, -17.5F, 4F);

		gunModel[34].addShapeBox(0F, 0F, 0F, 5, 3, 1, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 51
		gunModel[34].setRotationPoint(43F, -14.5F, 4F);

		gunModel[35].addShapeBox(0F, 0F, 0F, 5, 3, 1, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 52
		gunModel[35].setRotationPoint(49F, -14.5F, 4F);

		gunModel[36].addShapeBox(0F, 0F, 0F, 1, 6, 1, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 53
		gunModel[36].setRotationPoint(54F, -17.5F, 4F);

		gunModel[37].addShapeBox(0F, 0F, 0F, 5, 3, 1, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 54
		gunModel[37].setRotationPoint(55F, -14.5F, 4F);

		gunModel[38].addShapeBox(0F, 0F, 0F, 1, 6, 1, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 55
		gunModel[38].setRotationPoint(60F, -17.5F, 4F);

		gunModel[39].addShapeBox(0F, 0F, 0F, 17, 6, 1, 0F, -3F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, -1F, -3F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 56
		gunModel[39].setRotationPoint(26F, -17.5F, 4F);

		gunModel[40].addShapeBox(0F, 0F, 0F, 45, 1, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 57
		gunModel[40].setRotationPoint(35F, -15F, -2F);

		gunModel[41].addBox(0F, 0F, 0F, 45, 2, 4, 0F); // Box 58
		gunModel[41].setRotationPoint(35F, -14F, -2F);

		gunModel[42].addShapeBox(0F, 0F, 0F, 45, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 60
		gunModel[42].setRotationPoint(35F, -12F, -2F);

		gunModel[43].addShapeBox(0F, 0F, 0F, 3, 2, 5, 0F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 64
		gunModel[43].setRotationPoint(60F, -16F, -2.5F);

		gunModel[44].addBox(0F, 0F, 0F, 3, 2, 5, 0F); // Box 65
		gunModel[44].setRotationPoint(60F, -14F, -2.5F);

		gunModel[45].addShapeBox(0F, 0F, 0F, 3, 2, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F); // Box 66
		gunModel[45].setRotationPoint(60F, -12F, -2.5F);

		gunModel[46].addBox(0F, 0F, 0F, 15, 2, 7, 0F); // Box 67
		gunModel[46].setRotationPoint(-6F, -22.5F, -3.5F);

		gunModel[47].addBox(0F, 0F, 0F, 21, 2, 7, 0F); // Box 68
		gunModel[47].setRotationPoint(38F, -9F, -3.5F);

		gunModel[48].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 69
		gunModel[48].setRotationPoint(38F, -7F, -3.5F);

		gunModel[49].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 70
		gunModel[49].setRotationPoint(44F, -7F, -3.5F);

		gunModel[50].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 71
		gunModel[50].setRotationPoint(56F, -7F, -3.5F);

		gunModel[51].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 72
		gunModel[51].setRotationPoint(50F, -7F, -3.5F);

		gunModel[52].addShapeBox(0F, 0F, 0F, 7, 2, 5, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F); // Box 75
		gunModel[52].setRotationPoint(33F, -22.5F, -2.5F);

		gunModel[53].addBox(0F, 0F, 0F, 14, 5, 6, 0F); // Box 76
		gunModel[53].setRotationPoint(12F, -16F, -2F);

		gunModel[54].addBox(0F, 0F, 0F, 14, 2, 2, 0F); // Box 77
		gunModel[54].setRotationPoint(12F, -16F, -4F);

		gunModel[55].addBox(0F, 0F, 0F, 29, 1, 5, 0F); // Box 78
		gunModel[55].setRotationPoint(9F, -22.5F, -2.5F);

		gunModel[56].addBox(0F, 0F, 0F, 15, 1, 6, 0F); // Box 80
		gunModel[56].setRotationPoint(-6F, -21F, -3F);

		gunModel[57].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 81
		gunModel[57].setRotationPoint(-6F, -24.5F, -3.5F);

		gunModel[58].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 82
		gunModel[58].setRotationPoint(0F, -24.5F, -3.5F);

		gunModel[59].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 83
		gunModel[59].setRotationPoint(6F, -24.5F, -3.5F);

		gunModel[60].addShapeBox(0F, 0F, 0F, 1, 6, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 85
		gunModel[60].setRotationPoint(59F, -10.5F, -5F);

		gunModel[61].addBox(0F, 0F, 0F, 3, 10, 8, 0F); // Box 89
		gunModel[61].setRotationPoint(-8F, -16F, -4F);

		gunModel[62].addShapeBox(0F, 0F, 0F, 30, 30, 8, 0F, 0F, 0F, 0F, -25F, 0F, 0F, -25F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, -25F, 0F, -25F, -25F, 0F, -25F, -25F, 0.4F, 0F, -25F, 0.4F); // Box 90
		gunModel[62].setRotationPoint(6.5F, -11.5F, -4.2F);

		gunModel[63].addShapeBox(0F, 0F, 0F, 4, 2, 1, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 91
		gunModel[63].setRotationPoint(-2.5F, -8.5F, -4F);

		gunModel[64].addShapeBox(0F, 0F, 0F, 4, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 92
		gunModel[64].setRotationPoint(-2.5F, -6.5F, -4F);

		gunModel[65].addBox(0F, 0F, 0F, 10, 1, 1, 0F); // Box 93
		gunModel[65].setRotationPoint(13F, -11F, 3.3F);

		gunModel[66].addBox(0F, 0F, 0F, 4, 3, 1, 0F); // Box 94
		gunModel[66].setRotationPoint(13F, -10F, 3.3F);

		gunModel[67].addShapeBox(0F, 0F, 0F, 1, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 116
		gunModel[67].setRotationPoint(1F, 8F, -3.5F);

		gunModel[68].addShapeBox(0F, 0F, 0F, 8, 2, 7, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F); // Box 117
		gunModel[68].setRotationPoint(-7F, 8F, -3.5F);


		defaultBarrelModel = new ModelRendererTurbo[3];
		defaultBarrelModel[0] = new ModelRendererTurbo(this, 199, 2, textureX, textureY); // Box 61
		defaultBarrelModel[1] = new ModelRendererTurbo(this, 199, 10, textureX, textureY); // Box 62
		defaultBarrelModel[2] = new ModelRendererTurbo(this, 199, 2, textureX, textureY); // Box 63

		defaultBarrelModel[0].addShapeBox(0F, 0F, 0F, 10, 2, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F); // Box 61
		defaultBarrelModel[0].setRotationPoint(80F, -12F, -2.5F);

		defaultBarrelModel[1].addBox(0F, 0F, 0F, 10, 2, 5, 0F); // Box 62
		defaultBarrelModel[1].setRotationPoint(80F, -14F, -2.5F);

		defaultBarrelModel[2].addShapeBox(0F, 0F, 0F, 10, 2, 5, 0F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 63
		defaultBarrelModel[2].setRotationPoint(80F, -16F, -2.5F);


		defaultScopeModel = new ModelRendererTurbo[4];
		defaultScopeModel[0] = new ModelRendererTurbo(this, 1, 11, textureX, textureY); // Box 95
		defaultScopeModel[1] = new ModelRendererTurbo(this, 22, 12, textureX, textureY); // Box 96
		defaultScopeModel[2] = new ModelRendererTurbo(this, 35, 14, textureX, textureY); // Box 101
		defaultScopeModel[3] = new ModelRendererTurbo(this, 22, 12, textureX, textureY); // Box 102

		defaultScopeModel[0].addBox(0F, 0F, 0F, 5, 2, 5, 0F); // Box 95
		defaultScopeModel[0].setRotationPoint(32F, -24.5F, -2.5F);

		defaultScopeModel[1].addShapeBox(0F, 0F, 0F, 5, 5, 1, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 96
		defaultScopeModel[1].setRotationPoint(32F, -29.5F, -2.5F);

		defaultScopeModel[2].addBox(0F, 0F, 0F, 3, 3, 1, 0F); // Box 101
		defaultScopeModel[2].setRotationPoint(33F, -27.5F, -0.5F);

		defaultScopeModel[3].addShapeBox(0F, 0F, 0F, 5, 5, 1, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 102
		defaultScopeModel[3].setRotationPoint(32F, -29.5F, 1.5F);


		defaultStockModel = new ModelRendererTurbo[9];
		defaultStockModel[0] = new ModelRendererTurbo(this, 247, 10, textureX, textureY); // stock2
		defaultStockModel[1] = new ModelRendererTurbo(this, 270, 20, textureX, textureY); // Box 68
		defaultStockModel[2] = new ModelRendererTurbo(this, 301, 10, textureX, textureY); // Box 73
		defaultStockModel[3] = new ModelRendererTurbo(this, 293, 23, textureX, textureY); // Box 74
		defaultStockModel[4] = new ModelRendererTurbo(this, 247, 36, textureX, textureY); // Box 75
		defaultStockModel[5] = new ModelRendererTurbo(this, 247, 1, textureX, textureY); // Box 90
		defaultStockModel[6] = new ModelRendererTurbo(this, 282, 10, textureX, textureY); // Box 91
		defaultStockModel[7] = new ModelRendererTurbo(this, 270, 43, textureX, textureY); // Box 92
		defaultStockModel[8] = new ModelRendererTurbo(this, 247, 18, textureX, textureY); // Box 93

		defaultStockModel[0].addShapeBox(0F, 0F, 0F, 12, 2, 5, 0F, 0F, -5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -5F, 0F, 0F, 5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 5F, 0F); // stock2
		defaultStockModel[0].setRotationPoint(-34F, -9F, -2.5F);

		defaultStockModel[1].addShapeBox(0F, 0F, 0F, 5, 9, 6, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 68
		defaultStockModel[1].setRotationPoint(-15F, -16F, -3F);

		defaultStockModel[2].addBox(0F, 0F, 0F, 7, 2, 5, 0F); // Box 73
		defaultStockModel[2].setRotationPoint(-22F, -9F, -2.5F);

		defaultStockModel[3].addShapeBox(0F, 0F, 0F, 4, 7, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F); // Box 74
		defaultStockModel[3].setRotationPoint(-38F, -2F, -2.5F);

		defaultStockModel[4].addShapeBox(0F, 0F, 0F, 4, 10, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F); // Box 75
		defaultStockModel[4].setRotationPoint(-42F, -15F, -3.5F);

		defaultStockModel[5].addBox(0F, 0F, 0F, 23, 2, 6, 0F); // Box 90
		defaultStockModel[5].setRotationPoint(-38F, -15F, -3F);

		defaultStockModel[6].addBox(0F, 0F, 0F, 4, 2, 5, 0F); // Box 91
		defaultStockModel[6].setRotationPoint(-38F, -4F, -2.5F);

		defaultStockModel[7].addBox(0F, 0F, 0F, 2, 4, 6, 0F); // Box 92
		defaultStockModel[7].setRotationPoint(-22F, -13F, -3F);

		defaultStockModel[8].addShapeBox(0F, 0F, 0F, 4, 10, 7, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 93
		defaultStockModel[8].setRotationPoint(-42F, -5F, -3.5F);


		ammoModel = new ModelRendererTurbo[28];
		ammoModel[0] = new ModelRendererTurbo(this, 55, 111, textureX, textureY); // Box 25
		ammoModel[1] = new ModelRendererTurbo(this, 1, 126, textureX, textureY); // Box 26
		ammoModel[2] = new ModelRendererTurbo(this, 1, 111, textureX, textureY); // Box 27
		ammoModel[3] = new ModelRendererTurbo(this, 1, 126, textureX, textureY); // Box 28
		ammoModel[4] = new ModelRendererTurbo(this, 16, 126, textureX, textureY); // Box 86
		ammoModel[5] = new ModelRendererTurbo(this, 16, 132, textureX, textureY); // Box 87
		ammoModel[6] = new ModelRendererTurbo(this, 31, 128, textureX, textureY); // Box 94
		ammoModel[7] = new ModelRendererTurbo(this, 1, 96, textureX, textureY); // Box 95
		ammoModel[8] = new ModelRendererTurbo(this, 35, 96, textureX, textureY); // Box 96
		ammoModel[9] = new ModelRendererTurbo(this, 1, 126, textureX, textureY); // Box 97
		ammoModel[10] = new ModelRendererTurbo(this, 1, 126, textureX, textureY); // Box 98
		ammoModel[11] = new ModelRendererTurbo(this, 52, 96, textureX, textureY); // Box 99
		ammoModel[12] = new ModelRendererTurbo(this, 99, 133, textureX, textureY); // Box 100
		ammoModel[13] = new ModelRendererTurbo(this, 73, 96, textureX, textureY); // Box 101
		ammoModel[14] = new ModelRendererTurbo(this, 1, 126, textureX, textureY); // Box 102
		ammoModel[15] = new ModelRendererTurbo(this, 28, 111, textureX, textureY); // Box 103
		ammoModel[16] = new ModelRendererTurbo(this, 1, 126, textureX, textureY); // Box 104
		ammoModel[17] = new ModelRendererTurbo(this, 18, 96, textureX, textureY); // Box 105
		ammoModel[18] = new ModelRendererTurbo(this, 147, 136, textureX, textureY); // Box 106
		ammoModel[19] = new ModelRendererTurbo(this, 74, 127, textureX, textureY); // Box 107
		ammoModel[20] = new ModelRendererTurbo(this, 120, 136, textureX, textureY); // Box 108
		ammoModel[21] = new ModelRendererTurbo(this, 74, 127, textureX, textureY); // Box 109
		ammoModel[22] = new ModelRendererTurbo(this, 82, 113, textureX, textureY); // Box 110
		ammoModel[23] = new ModelRendererTurbo(this, 99, 122, textureX, textureY); // Box 111
		ammoModel[24] = new ModelRendererTurbo(this, 140, 123, textureX, textureY); // Box 112
		ammoModel[25] = new ModelRendererTurbo(this, 140, 123, textureX, textureY); // Box 113
		ammoModel[26] = new ModelRendererTurbo(this, 140, 123, textureX, textureY); // Box 114
		ammoModel[27] = new ModelRendererTurbo(this, 140, 123, textureX, textureY); // Box 115

		ammoModel[0].addBox(1F, 0F, 0F, 6, 7, 7, 0F); // Box 25
		ammoModel[0].setRotationPoint(14F, -10F, -3.5F);

		ammoModel[1].addBox(0F, 0F, 0F, 1, 7, 6, 0F); // Box 26
		ammoModel[1].setRotationPoint(14F, -10F, -3F);

		ammoModel[2].addShapeBox(1F, 0F, 0F, 6, 7, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F); // Box 27
		ammoModel[2].setRotationPoint(14F, -3F, -3.5F);

		ammoModel[3].addShapeBox(0F, 0F, 0F, 1, 7, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F); // Box 28
		ammoModel[3].setRotationPoint(14F, -3F, -3F);

		ammoModel[4].addShapeBox(0F, 0F, 0F, 7, 1, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 86
		ammoModel[4].setRotationPoint(14F, -11F, -2F);

		ammoModel[5].addShapeBox(0F, 0F, 0F, 3, 1, 4, 0F, 0F, 0F, -1F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 87
		ammoModel[5].setRotationPoint(21F, -11F, -2F);

		ammoModel[6].addShapeBox(1F, 0F, 0F, 13, 1, 8, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F); // Box 94
		ammoModel[6].setRotationPoint(11.5F, -4F, -4F);

		ammoModel[7].addBox(1F, 0F, 0F, 1, 7, 7, 0F); // Box 95
		ammoModel[7].setRotationPoint(12F, -10F, -3.5F);

		ammoModel[8].addShapeBox(1F, 0F, 0F, 1, 7, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F); // Box 96
		ammoModel[8].setRotationPoint(12F, -3F, -3.5F);

		ammoModel[9].addBox(0F, 0F, 0F, 1, 7, 6, 0F); // Box 97
		ammoModel[9].setRotationPoint(21F, -10F, -3F);

		ammoModel[10].addShapeBox(0F, 0F, 0F, 1, 7, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F); // Box 98
		ammoModel[10].setRotationPoint(21F, -3F, -3F);

		ammoModel[11].addShapeBox(1F, 0F, 0F, 3, 7, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F); // Box 99
		ammoModel[11].setRotationPoint(21F, -3F, -3.5F);

		ammoModel[12].addBox(1F, 0F, 0F, 3, 7, 7, 0F); // Box 100
		ammoModel[12].setRotationPoint(21F, -10F, -3.5F);

		ammoModel[13].addShapeBox(1F, 0F, 0F, 3, 7, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F); // Box 101
		ammoModel[13].setRotationPoint(22F, 4F, -3.5F);

		ammoModel[14].addShapeBox(0F, 0F, 0F, 1, 7, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F); // Box 102
		ammoModel[14].setRotationPoint(22F, 4F, -3F);

		ammoModel[15].addShapeBox(1F, 0F, 0F, 6, 7, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F); // Box 103
		ammoModel[15].setRotationPoint(15F, 4F, -3.5F);

		ammoModel[16].addShapeBox(0F, 0F, 0F, 1, 7, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F); // Box 104
		ammoModel[16].setRotationPoint(15F, 4F, -3F);

		ammoModel[17].addShapeBox(1F, 0F, 0F, 1, 7, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F); // Box 105
		ammoModel[17].setRotationPoint(13F, 4F, -3.5F);

		ammoModel[18].addShapeBox(1F, 0F, 0F, 3, 4, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 1.5F, -1F, 0F, 1.5F, -1F, 0F, -2F, 0F, 0F); // Box 106
		ammoModel[18].setRotationPoint(24F, 11F, -3.5F);

		ammoModel[19].addShapeBox(0F, 0F, 0F, 1, 4, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F); // Box 107
		ammoModel[19].setRotationPoint(24F, 11F, -3F);

		ammoModel[20].addShapeBox(1F, 0F, 0F, 6, 4, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F); // Box 108
		ammoModel[20].setRotationPoint(17F, 11F, -3.5F);

		ammoModel[21].addShapeBox(0F, 0F, 0F, 1, 4, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F); // Box 109
		ammoModel[21].setRotationPoint(17F, 11F, -3F);

		ammoModel[22].addShapeBox(1F, 0F, 0F, 1, 4, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F); // Box 110
		ammoModel[22].setRotationPoint(15F, 11F, -3.5F);

		ammoModel[23].addShapeBox(1F, 0F, 0F, 12, 2, 8, 0F, 0F, 0F, 0F, 0F, 2.5F, 0F, 0F, 2.5F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, 0.5F, -2.5F, 0F, 0.5F, -2.5F, 0F, -0.5F, 0F, 0F); // Box 111
		ammoModel[23].setRotationPoint(16.25F, 15F, -4F);

		ammoModel[24].addShapeBox(1F, 0F, 0F, 2, 1, 8, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 112
		ammoModel[24].setRotationPoint(16F, -3F, -4F);

		ammoModel[25].addShapeBox(1F, 0F, 0F, 2, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 113
		ammoModel[25].setRotationPoint(16F, -2F, -4F);

		ammoModel[26].addShapeBox(1F, 0F, 0F, 2, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 114
		ammoModel[26].setRotationPoint(18F, 8F, -4F);

		ammoModel[27].addShapeBox(1F, 0F, 0F, 2, 1, 8, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 115
		ammoModel[27].setRotationPoint(18F, 7F, -4F);


		slideModel = new ModelRendererTurbo[1];
		slideModel[0] = new ModelRendererTurbo(this, 141, 116, textureX, textureY); // Box 79

		slideModel[0].addBox(0F, 0F, 0F, 14, 3, 2, 0F); // Box 79
		slideModel[0].setRotationPoint(12F, -14F, -3.5F);

		barrelAttachPoint = new Vector3f(80F /16F, 13F /16F, 0F /16F);
		stockAttachPoint = new Vector3f(-10F /16F, 11F /16F, 0F /16F);
		scopeAttachPoint = new Vector3f(5F /16F, 22F /16F, 0F /16F);
		gripAttachPoint = new Vector3f(48 /16F, 8F /16F, 0F /16F);

		gunSlideDistance = 0.5F;
		animationType = EnumAnimationType.BOTTOM_CLIP;

		translateAll(0F, 0F, 0F);


		flipAll();
	}
}