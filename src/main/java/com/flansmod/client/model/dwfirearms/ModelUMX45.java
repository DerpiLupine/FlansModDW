package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelUMX45 extends ModelGun
{
	int textureX = 512;
	int textureY = 256;

	public ModelUMX45()
	{
		gunModel = new ModelRendererTurbo[42];
		gunModel[0] = new ModelRendererTurbo(this, 117, 126, textureX, textureY); // body1
		gunModel[1] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // rearSight1
		gunModel[2] = new ModelRendererTurbo(this, 45, 4, textureX, textureY); // rearSight2
		gunModel[3] = new ModelRendererTurbo(this, 30, 4, textureX, textureY); // rearSight3
		gunModel[4] = new ModelRendererTurbo(this, 1, 61, textureX, textureY); // body1
		gunModel[5] = new ModelRendererTurbo(this, 22, 62, textureX, textureY); // body1
		gunModel[6] = new ModelRendererTurbo(this, 42, 98, textureX, textureY); // Box 0
		gunModel[7] = new ModelRendererTurbo(this, 1, 10, textureX, textureY); // Box 1
		gunModel[8] = new ModelRendererTurbo(this, 1, 20, textureX, textureY); // Box 2
		gunModel[9] = new ModelRendererTurbo(this, 1, 50, textureX, textureY); // Box 4
		gunModel[10] = new ModelRendererTurbo(this, 36, 20, textureX, textureY); // Box 6
		gunModel[11] = new ModelRendererTurbo(this, 36, 10, textureX, textureY); // Box 7
		gunModel[12] = new ModelRendererTurbo(this, 55, 10, textureX, textureY); // Box 8
		gunModel[13] = new ModelRendererTurbo(this, 55, 20, textureX, textureY); // Box 9
		gunModel[14] = new ModelRendererTurbo(this, 36, 50, textureX, textureY); // Box 11
		gunModel[15] = new ModelRendererTurbo(this, 100, 126, textureX, textureY); // Box 12
		gunModel[16] = new ModelRendererTurbo(this, 198, 127, textureX, textureY); // Box 76
		gunModel[17] = new ModelRendererTurbo(this, 138, 127, textureX, textureY); // Box 77
		gunModel[18] = new ModelRendererTurbo(this, 201, 63, textureX, textureY); // Box 79
		gunModel[19] = new ModelRendererTurbo(this, 189, 127, textureX, textureY); // Box 17
		gunModel[20] = new ModelRendererTurbo(this, 172, 131, textureX, textureY); // Box 18
		gunModel[21] = new ModelRendererTurbo(this, 100, 115, textureX, textureY); // Box 21
		gunModel[22] = new ModelRendererTurbo(this, 145, 63, textureX, textureY); // Box 3
		gunModel[23] = new ModelRendererTurbo(this, 145, 74, textureX, textureY); // Box 4
		gunModel[24] = new ModelRendererTurbo(this, 1, 95, textureX, textureY); // Box 11
		gunModel[25] = new ModelRendererTurbo(this, 72, 83, textureX, textureY); // Box 12
		gunModel[26] = new ModelRendererTurbo(this, 73, 71, textureX, textureY); // Box 13
		gunModel[27] = new ModelRendererTurbo(this, 48, 72, textureX, textureY); // Box 14
		gunModel[28] = new ModelRendererTurbo(this, 48, 72, textureX, textureY); // Box 15
		gunModel[29] = new ModelRendererTurbo(this, 48, 85, textureX, textureY); // Box 17
		gunModel[30] = new ModelRendererTurbo(this, 48, 85, textureX, textureY); // Box 18
		gunModel[31] = new ModelRendererTurbo(this, 172, 122, textureX, textureY); // Box 20
		gunModel[32] = new ModelRendererTurbo(this, 161, 122, textureX, textureY); // Box 21
		gunModel[33] = new ModelRendererTurbo(this, 161, 130, textureX, textureY); // Box 22
		gunModel[34] = new ModelRendererTurbo(this, 1, 110, textureX, textureY); // Box 33
		gunModel[35] = new ModelRendererTurbo(this, 36, 110, textureX, textureY); // Box 34
		gunModel[36] = new ModelRendererTurbo(this, 1, 133, textureX, textureY); // Box 35
		gunModel[37] = new ModelRendererTurbo(this, 36, 121, textureX, textureY); // Box 36
		gunModel[38] = new ModelRendererTurbo(this, 48, 72, textureX, textureY); // Box 72
		gunModel[39] = new ModelRendererTurbo(this, 48, 72, textureX, textureY); // Box 73
		gunModel[40] = new ModelRendererTurbo(this, 48, 72, textureX, textureY); // Box 74
		gunModel[41] = new ModelRendererTurbo(this, 48, 72, textureX, textureY); // Box 75

		gunModel[0].addShapeBox(0F, 0F, 0F, 3, 4, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F); // body1
		gunModel[0].setRotationPoint(-10F, -15F, -3.5F);

		gunModel[1].addBox(0F, 0F, 0F, 8, 2, 6, 0F); // rearSight1
		gunModel[1].setRotationPoint(-9.5F, -28.5F, -3F);

		gunModel[2].addShapeBox(0F, 0F, 0F, 5, 3, 2, 0F, 0F, -0.5F, 0F, -2F, -0.5F, 0F, -2F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // rearSight2
		gunModel[2].setRotationPoint(-9.5F, -31.5F, 1F);

		gunModel[3].addShapeBox(0F, 0F, 0F, 5, 3, 2, 0F, 0F, -0.5F, 0F, -2F, -0.5F, 0F, -2F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // rearSight3
		gunModel[3].setRotationPoint(-9.5F, -31.5F, -3F);

		gunModel[4].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // body1
		gunModel[4].setRotationPoint(-4F, 6F, -3.5F);

		gunModel[5].addShapeBox(0F, 0F, 0F, 3, 1, 7, 0F, 0F, 0F, 0F, -1F, 0F, -1F, -1F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // body1
		gunModel[5].setRotationPoint(-4F, 5F, -3.5F);

		gunModel[6].addBox(0F, 0F, 0F, 12, 4, 7, 0F); // Box 0
		gunModel[6].setRotationPoint(-7F, -15F, -3.5F);

		gunModel[7].addBox(0F, 0F, 0F, 10, 2, 7, 0F); // Box 1
		gunModel[7].setRotationPoint(-8F, -11F, -3.5F);

		gunModel[8].addShapeBox(0F, 0F, 0F, 10, 14, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 6F, 0F, 0F, -6F, 0F, 0F, -6F, 0F, 0F, 6F, 0F, 0F); // Box 2
		gunModel[8].setRotationPoint(-8F, -9F, -3.5F);

		gunModel[9].addBox(0F, 0F, 0F, 10, 3, 7, 0F); // Box 4
		gunModel[9].setRotationPoint(-14F, 5F, -3.5F);

		gunModel[10].addShapeBox(0F, 0F, 0F, 2, 14, 7, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 6F, 0F, 0F, -6F, 0F, -1F, -6F, 0F, -1F, 6F, 0F, 0F); // Box 6
		gunModel[10].setRotationPoint(2F, -9F, -3.5F);

		gunModel[11].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 7
		gunModel[11].setRotationPoint(2F, -11F, -3.5F);

		gunModel[12].addShapeBox(0F, 0F, 0F, 1, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 8
		gunModel[12].setRotationPoint(-9F, -11F, -3.5F);

		gunModel[13].addShapeBox(0F, 0F, 0F, 1, 14, 7, 0F, -6F, 0F, -1F, 6F, 0F, 0F, 6F, 0F, 0F, -6F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 9
		gunModel[13].setRotationPoint(-15F, -9F, -3.5F);

		gunModel[14].addShapeBox(0F, 0F, 0F, 1, 3, 7, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 11
		gunModel[14].setRotationPoint(-15F, 5F, -3.5F);

		gunModel[15].addShapeBox(0F, 0F, 0F, 1, 4, 7, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, -2F, 0F, -1F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, -1F); // Box 12
		gunModel[15].setRotationPoint(-11F, -15F, -3.5F);

		gunModel[16].addShapeBox(0F, 0F, 0F, 2, 8, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F); // Box 76
		gunModel[16].setRotationPoint(-12F, -25F, -4F);

		gunModel[17].addShapeBox(0F, 0F, 0F, 3, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F); // Box 77
		gunModel[17].setRotationPoint(-14F, -17F, -4F);

		gunModel[18].addShapeBox(0F, 0F, 0F, 2, 2, 8, 0F, -1F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, -1F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 79
		gunModel[18].setRotationPoint(-12F, -27F, -4F);

		gunModel[19].addShapeBox(0F, 0F, 0F, 2, 8, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F); // Box 17
		gunModel[19].setRotationPoint(-12F, -25F, 2F);

		gunModel[20].addShapeBox(0F, 0F, 0F, 4, 2, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F); // Box 18
		gunModel[20].setRotationPoint(-13.5F, -19F, -2F);

		gunModel[21].addBox(0F, 0F, 0F, 16, 2, 8, 0F); // Box 21
		gunModel[21].setRotationPoint(-11F, -17F, -4F);

		gunModel[22].addShapeBox(0F, 0F, 0F, 9, 2, 8, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 3
		gunModel[22].setRotationPoint(-10F, -27F, -4F);

		gunModel[23].addBox(0F, 0F, 0F, 9, 8, 8, 0F); // Box 4
		gunModel[23].setRotationPoint(-10F, -25F, -4F);

		gunModel[24].addBox(0F, 0F, 0F, 12, 6, 8, 0F); // Box 11
		gunModel[24].setRotationPoint(5F, -17F, -4F);

		gunModel[25].addShapeBox(0F, 0F, 0F, 2, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F); // Box 12
		gunModel[25].setRotationPoint(17F, -14F, -4F);

		gunModel[26].addBox(0F, 0F, 0F, 2, 3, 8, 0F); // Box 13
		gunModel[26].setRotationPoint(17F, -17F, -4F);

		gunModel[27].addShapeBox(0F, 0F, 0F, 2, 1, 9, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 14
		gunModel[27].setRotationPoint(16F, -16F, -4.5F);

		gunModel[28].addShapeBox(0F, 0F, 0F, 2, 1, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 15
		gunModel[28].setRotationPoint(16F, -15F, -4.5F);

		gunModel[29].addShapeBox(0F, 0F, 0F, 2, 1, 8, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 17
		gunModel[29].setRotationPoint(2F, -14F, -4F);

		gunModel[30].addShapeBox(0F, 0F, 0F, 2, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 18
		gunModel[30].setRotationPoint(2F, -13F, -4F);

		gunModel[31].addBox(0F, 0F, 0F, 3, 3, 4, 0F); // Box 20
		gunModel[31].setRotationPoint(-13F, -25F, -2F);

		gunModel[32].addShapeBox(0F, 0F, 0F, 1, 3, 4, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F); // Box 21
		gunModel[32].setRotationPoint(-14F, -25F, -2F);

		gunModel[33].addShapeBox(0F, 0F, 0F, 1, 3, 4, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 22
		gunModel[33].setRotationPoint(-13F, -22F, -2F);

		gunModel[34].addShapeBox(0F, 0F, 0F, 9, 14, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 6F, 0F, 0F, -6F, 0F, 0F, -6F, 0F, 0F, 6F, 0F, 0F); // Box 33
		gunModel[34].setRotationPoint(-7.5F, -9F, -4F);

		gunModel[35].addBox(0F, 0F, 0F, 9, 2, 8, 0F); // Box 34
		gunModel[35].setRotationPoint(-7.5F, -11F, -4F);

		gunModel[36].addBox(0F, 0F, 0F, 7, 3, 8, 0F); // Box 35
		gunModel[36].setRotationPoint(-7.5F, -14F, -4F);

		gunModel[37].addShapeBox(0F, 0F, 0F, 9, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 36
		gunModel[37].setRotationPoint(-13.5F, 5F, -4F);

		gunModel[38].addShapeBox(0F, 0F, 0F, 2, 1, 9, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 72
		gunModel[38].setRotationPoint(-9F, 2F, -4.5F);

		gunModel[39].addShapeBox(0F, 0F, 0F, 2, 1, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 73
		gunModel[39].setRotationPoint(-9F, 3F, -4.5F);

		gunModel[40].addShapeBox(0F, 0F, 0F, 2, 1, 9, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 74
		gunModel[40].setRotationPoint(-4.5F, -9F, -4.5F);

		gunModel[41].addShapeBox(0F, 0F, 0F, 2, 1, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 75
		gunModel[41].setRotationPoint(-4.5F, -8F, -4.5F);


		ammoModel = new ModelRendererTurbo[2];
		ammoModel[0] = new ModelRendererTurbo(this, 1, 83, textureX, textureY); // Box 27
		ammoModel[1] = new ModelRendererTurbo(this, 1, 83, textureX, textureY); // Box 28

		ammoModel[0].addBox(-20F, -11F, -3.5F, 20, 8, 3, 0F); // Box 27
		ammoModel[0].setRotationPoint(17F, -15F, 0F);

		ammoModel[1].addBox(-20F, -11F, 0.5F, 20, 8, 3, 0F); // Box 28
		ammoModel[1].setRotationPoint(17F, -15F, 0F);


		slideModel = new ModelRendererTurbo[1];
		slideModel[0] = new ModelRendererTurbo(this, 53, 52, textureX, textureY); // Box 26

		slideModel[0].addShapeBox(4F, -11F, -3F, 10, 2, 6, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 26
		slideModel[0].setRotationPoint(17F, -15F, 0F);


		breakActionModel = new ModelRendererTurbo[27];
		breakActionModel[0] = new ModelRendererTurbo(this, 127, 13, textureX, textureY); // barrelPart1
		breakActionModel[1] = new ModelRendererTurbo(this, 127, 3, textureX, textureY); // barrelPart2
		breakActionModel[2] = new ModelRendererTurbo(this, 127, 13, textureX, textureY); // barrelPart3
		breakActionModel[3] = new ModelRendererTurbo(this, 100, 103, textureX, textureY); // bodyRail1
		breakActionModel[4] = new ModelRendererTurbo(this, 60, 3, textureX, textureY); // frontSight
		breakActionModel[5] = new ModelRendererTurbo(this, 171, 102, textureX, textureY); // slide1
		breakActionModel[6] = new ModelRendererTurbo(this, 100, 63, textureX, textureY); // Box 58
		breakActionModel[7] = new ModelRendererTurbo(this, 144, 102, textureX, textureY); // Box 72
		breakActionModel[8] = new ModelRendererTurbo(this, 125, 102, textureX, textureY); // Box 73
		breakActionModel[9] = new ModelRendererTurbo(this, 125, 102, textureX, textureY); // Box 74
		breakActionModel[10] = new ModelRendererTurbo(this, 134, 6, textureX, textureY); // Box 86
		breakActionModel[11] = new ModelRendererTurbo(this, 134, 15, textureX, textureY); // Box 87
		breakActionModel[12] = new ModelRendererTurbo(this, 134, 6, textureX, textureY); // Box 88
		breakActionModel[13] = new ModelRendererTurbo(this, 100, 80, textureX, textureY); // Box 2
		breakActionModel[14] = new ModelRendererTurbo(this, 100, 91, textureX, textureY); // Box 3
		breakActionModel[15] = new ModelRendererTurbo(this, 100, 22, textureX, textureY); // Box 38
		breakActionModel[16] = new ModelRendererTurbo(this, 100, 33, textureX, textureY); // Box 39
		breakActionModel[17] = new ModelRendererTurbo(this, 159, 52, textureX, textureY); // Box 0
		breakActionModel[18] = new ModelRendererTurbo(this, 184, 42, textureX, textureY); // Box 1
		breakActionModel[19] = new ModelRendererTurbo(this, 100, 52, textureX, textureY); // Box 2
		breakActionModel[20] = new ModelRendererTurbo(this, 192, 95, textureX, textureY); // Box 10
		breakActionModel[21] = new ModelRendererTurbo(this, 141, 42, textureX, textureY); // Box 16
		breakActionModel[22] = new ModelRendererTurbo(this, 77, 5, textureX, textureY); // Box 19
		breakActionModel[23] = new ModelRendererTurbo(this, 169, 95, textureX, textureY); // Box 23
		breakActionModel[24] = new ModelRendererTurbo(this, 180, 76, textureX, textureY); // Box 24
		breakActionModel[25] = new ModelRendererTurbo(this, 180, 63, textureX, textureY); // Box 25
		breakActionModel[26] = new ModelRendererTurbo(this, 100, 42, textureX, textureY); // Box 29

		breakActionModel[0].addShapeBox(28F, -11F, -3F, 1, 6, 2, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // barrelPart1
		breakActionModel[0].setRotationPoint(17F, -15F, 0F);

		breakActionModel[1].addBox(28F, -11F, -1F, 1, 6, 2, 0F); // barrelPart2
		breakActionModel[1].setRotationPoint(17F, -15F, 0F);

		breakActionModel[2].addShapeBox(28F, -11F, 1F, 1, 6, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F); // barrelPart3
		breakActionModel[2].setRotationPoint(17F, -15F, 0F);

		breakActionModel[3].addShapeBox(16F, -8F, -3.5F, 5, 4, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F); // bodyRail1
		breakActionModel[3].setRotationPoint(17F, -15F, 0F);

		breakActionModel[4].addShapeBox(21F, -16F, -0.5F, 5, 3, 3, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F); // frontSight
		breakActionModel[4].setRotationPoint(17F, -15F, 0F);

		breakActionModel[5].addShapeBox(14F, -8F, -4F, 2, 4, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F); // slide1
		breakActionModel[5].setRotationPoint(17F, -15F, 0F);

		breakActionModel[6].addShapeBox(14F, -12F, -4F, 14, 2, 8, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 58
		breakActionModel[6].setRotationPoint(17F, -15F, 0F);

		breakActionModel[7].addShapeBox(23F, -8F, -4F, 5, 4, 8, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 72
		breakActionModel[7].setRotationPoint(17F, -15F, 0F);

		breakActionModel[8].addShapeBox(17F, -8F, -4F, 1, 4, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F); // Box 73
		breakActionModel[8].setRotationPoint(17F, -15F, 0F);

		breakActionModel[9].addShapeBox(19F, -8F, -4F, 1, 4, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F); // Box 74
		breakActionModel[9].setRotationPoint(17F, -15F, 0F);

		breakActionModel[10].addShapeBox(28F, -4F, 1F, 1, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F); // Box 86
		breakActionModel[10].setRotationPoint(17F, -15F, 0F);

		breakActionModel[11].addBox(28F, -4F, -1F, 1, 4, 2, 0F); // Box 87
		breakActionModel[11].setRotationPoint(17F, -15F, 0F);

		breakActionModel[12].addShapeBox(28F, -4F, -2F, 1, 4, 1, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 88
		breakActionModel[12].setRotationPoint(17F, -15F, 0F);

		breakActionModel[13].addBox(14F, -10F, -4F, 14, 2, 8, 0F); // Box 2
		breakActionModel[13].setRotationPoint(17F, -15F, 0F);

		breakActionModel[14].addBox(2F, -4F, -4F, 26, 2, 8, 0F); // Box 3
		breakActionModel[14].setRotationPoint(17F, -15F, 0F);

		breakActionModel[15].addBox(-2F, -2F, -3.5F, 30, 3, 7, 0F); // Box 38
		breakActionModel[15].setRotationPoint(17F, -15F, 0F);

		breakActionModel[16].addShapeBox(-2F, 1F, -3.5F, 30, 1, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 39
		breakActionModel[16].setRotationPoint(17F, -15F, 0F);

		breakActionModel[17].addShapeBox(-18F, -12F, -4.5F, 20, 1, 9, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 0
		breakActionModel[17].setRotationPoint(17F, -15F, 0F);

		breakActionModel[18].addBox(-18F, -11F, -4.5F, 20, 8, 1, 0F); // Box 1
		breakActionModel[18].setRotationPoint(17F, -15F, 0F);

		breakActionModel[19].addShapeBox(-18F, -3F, -4.5F, 20, 1, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 2
		breakActionModel[19].setRotationPoint(17F, -15F, 0F);

		breakActionModel[20].addBox(4F, -9F, -4F, 10, 5, 1, 0F); // Box 10
		breakActionModel[20].setRotationPoint(17F, -15F, 0F);

		breakActionModel[21].addBox(-18F, -11F, 3.5F, 20, 8, 1, 0F); // Box 16
		breakActionModel[21].setRotationPoint(17F, -15F, 0F);

		breakActionModel[22].addBox(21F, -13F, -1F, 6, 2, 2, 0F); // Box 19
		breakActionModel[22].setRotationPoint(17F, -15F, 0F);

		breakActionModel[23].addBox(4F, -9F, 3F, 10, 5, 1, 0F); // Box 23
		breakActionModel[23].setRotationPoint(17F, -15F, 0F);

		breakActionModel[24].addBox(2F, -10F, -4F, 2, 6, 8, 0F); // Box 24
		breakActionModel[24].setRotationPoint(17F, -15F, 0F);

		breakActionModel[25].addShapeBox(2F, -12F, -4F, 2, 2, 8, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 25
		breakActionModel[25].setRotationPoint(17F, -15F, 0F);

		breakActionModel[26].addBox(-17F, -11F, -0.5F, 19, 8, 1, 0F); // Box 29
		breakActionModel[26].setRotationPoint(17F, -15F, 0F);

		barrelAttachPoint = new Vector3f(0F /16F, 0F /16F, 0F /16F);
		stockAttachPoint = new Vector3f(0F /16F, 0F /16F, 0F /16F);
		scopeAttachPoint = new Vector3f(0F /16F, 0F, 0F /16F);
		gripAttachPoint = new Vector3f(0 /16F, 0F /16F, 0F /16F);

		gunSlideDistance = 1F;
		animationType = EnumAnimationType.BREAK_ACTION;

		barrelBreakPoint = new Vector3f(17 /16F, 15F /16F, 0F /16F);
		breakAngle = 35F;


		translateAll(0F, 0F, 0F);


		flipAll();
	}
}