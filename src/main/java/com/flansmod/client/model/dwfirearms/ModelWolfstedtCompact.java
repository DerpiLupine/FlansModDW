package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelWolfstedtCompact extends ModelGun
{
	int textureX = 512;
	int textureY = 256;

	public ModelWolfstedtCompact()
	{
		gunModel = new ModelRendererTurbo[42];
		gunModel[0] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Import 
		gunModel[1] = new ModelRendererTurbo(this, 33, 1, textureX, textureY); // Import 
		gunModel[2] = new ModelRendererTurbo(this, 89, 1, textureX, textureY); // Import 
		gunModel[3] = new ModelRendererTurbo(this, 145, 1, textureX, textureY); // Import 
		gunModel[4] = new ModelRendererTurbo(this, 225, 1, textureX, textureY); // Import 
		gunModel[5] = new ModelRendererTurbo(this, 305, 1, textureX, textureY); // Import 
		gunModel[6] = new ModelRendererTurbo(this, 385, 1, textureX, textureY); // Import 
		gunModel[7] = new ModelRendererTurbo(this, 409, 1, textureX, textureY); // Import 
		gunModel[8] = new ModelRendererTurbo(this, 441, 1, textureX, textureY); // Import 
		gunModel[9] = new ModelRendererTurbo(this, 49, 17, textureX, textureY); // Import 
		gunModel[10] = new ModelRendererTurbo(this, 1, 33, textureX, textureY); // Import 
		gunModel[11] = new ModelRendererTurbo(this, 121, 17, textureX, textureY); // Import 
		gunModel[12] = new ModelRendererTurbo(this, 1, 17, textureX, textureY); // Import 
		gunModel[13] = new ModelRendererTurbo(this, 193, 17, textureX, textureY); // Import 
		gunModel[14] = new ModelRendererTurbo(this, 297, 17, textureX, textureY); // Import 
		gunModel[15] = new ModelRendererTurbo(this, 393, 17, textureX, textureY); // Import 
		gunModel[16] = new ModelRendererTurbo(this, 449, 1, textureX, textureY); // Import 
		gunModel[17] = new ModelRendererTurbo(this, 473, 17, textureX, textureY); // Import 
		gunModel[18] = new ModelRendererTurbo(this, 481, 1, textureX, textureY); // Import 
		gunModel[19] = new ModelRendererTurbo(this, 105, 17, textureX, textureY); // Import 
		gunModel[20] = new ModelRendererTurbo(this, 297, 33, textureX, textureY); // Import 
		gunModel[21] = new ModelRendererTurbo(this, 337, 33, textureX, textureY); // Import 
		gunModel[22] = new ModelRendererTurbo(this, 25, 41, textureX, textureY); // Import 
		gunModel[23] = new ModelRendererTurbo(this, 177, 17, textureX, textureY); // Import 
		gunModel[24] = new ModelRendererTurbo(this, 465, 17, textureX, textureY); // Import 
		gunModel[25] = new ModelRendererTurbo(this, 465, 25, textureX, textureY); // Import 
		gunModel[26] = new ModelRendererTurbo(this, 465, 21, textureX, textureY); // Import 
		gunModel[27] = new ModelRendererTurbo(this, 321, 33, textureX, textureY); // Import 
		gunModel[28] = new ModelRendererTurbo(this, 161, 41, textureX, textureY); // Import 
		gunModel[29] = new ModelRendererTurbo(this, 185, 41, textureX, textureY); // Import 
		gunModel[30] = new ModelRendererTurbo(this, 209, 41, textureX, textureY); // Import 
		gunModel[31] = new ModelRendererTurbo(this, 233, 41, textureX, textureY); // Import 
		gunModel[32] = new ModelRendererTurbo(this, 49, 41, textureX, textureY); // Import 
		gunModel[33] = new ModelRendererTurbo(this, 425, 1, textureX, textureY); // Import 
		gunModel[34] = new ModelRendererTurbo(this, 1, 17, textureX, textureY); // Import 
		gunModel[35] = new ModelRendererTurbo(this, 1, 17, textureX, textureY); // Import 
		gunModel[36] = new ModelRendererTurbo(this, 297, 1, textureX, textureY); // Import 
		gunModel[37] = new ModelRendererTurbo(this, 297, 1, textureX, textureY); // Import 
		gunModel[38] = new ModelRendererTurbo(this, 1, 17, textureX, textureY); // Import 
		gunModel[39] = new ModelRendererTurbo(this, 1, 17, textureX, textureY); // Import 
		gunModel[40] = new ModelRendererTurbo(this, 1, 17, textureX, textureY); // Import 
		gunModel[41] = new ModelRendererTurbo(this, 1, 17, textureX, textureY); // Import 

		gunModel[0].addBox(0F, 0F, 0F, 12, 3, 7, 0F); // Import 
		gunModel[0].setRotationPoint(13F, -15.5F, -3.5F);

		gunModel[1].addShapeBox(0F, 0F, 0F, 18, 3, 12, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import 
		gunModel[1].setRotationPoint(12F, -24F, -6F);

		gunModel[2].addShapeBox(0F, 0F, 0F, 18, 2, 9, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import 
		gunModel[2].setRotationPoint(12F, -26F, -4.5F);

		gunModel[3].addShapeBox(0F, 0F, 0F, 28, 2, 8, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import 
		gunModel[3].setRotationPoint(35F, -25F, -4F);

		gunModel[4].addShapeBox(0F, 0F, 0F, 28, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F); // Import 
		gunModel[4].setRotationPoint(35F, -23F, -4F);

		gunModel[5].addShapeBox(0F, 0F, 0F, 28, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Import 
		gunModel[5].setRotationPoint(35F, -19F, -4F);

		gunModel[6].addShapeBox(0F, 0F, 0F, 2, 2, 8, 0F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F); // Import 
		gunModel[6].setRotationPoint(58F, -25F, -4F);

		gunModel[7].addShapeBox(0F, 0F, 0F, 2, 3, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F); // Import 
		gunModel[7].setRotationPoint(58F, -23F, -5F);

		gunModel[8].addShapeBox(0F, 0F, 0F, 2, 2, 8, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F); // Import 
		gunModel[8].setRotationPoint(58F, -19F, -4F);

		gunModel[9].addShapeBox(0F, 0F, 0F, 20, 3, 14, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import 
		gunModel[9].setRotationPoint(-8F, -25F, -7F);

		gunModel[10].addShapeBox(0F, 0F, 0F, 10, 13, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, -3F, 1F, 0F, -3F, 1F, 0F, 3F, 0F, 0F); // Import 
		gunModel[10].setRotationPoint(-6F, -12.5F, -3F);

		gunModel[11].addBox(0F, 0F, 0F, 20, 3, 14, 0F); // Import 
		gunModel[11].setRotationPoint(-8F, -22F, -7F);

		gunModel[12].addShapeBox(0F, 0F, 0F, 20, 2, 10, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import 
		gunModel[12].setRotationPoint(-8F, -27F, -5F);

		gunModel[13].addShapeBox(0F, 0F, 0F, 43, 3, 14, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Import 
		gunModel[13].setRotationPoint(-8F, -19F, -7F);

		gunModel[14].addShapeBox(0F, 0F, 0F, 43, 2, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Import 
		gunModel[14].setRotationPoint(-8F, -16F, -5F);

		gunModel[15].addBox(0F, 0F, 0F, 23, 2, 14, 0F); // Import 
		gunModel[15].setRotationPoint(12F, -21F, -7F);

		gunModel[16].addBox(0F, 0F, 0F, 5, 1, 14, 0F); // Import 
		gunModel[16].setRotationPoint(30F, -22F, -7F);

		gunModel[17].addShapeBox(0F, 0F, 0F, 5, 3, 14, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import 
		gunModel[17].setRotationPoint(30F, -25F, -7F);

		gunModel[18].addShapeBox(0F, 0F, 0F, 5, 2, 10, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import 
		gunModel[18].setRotationPoint(30F, -27F, -5F);

		gunModel[19].addShapeBox(0F, 0F, 0F, 2, 2, 10, 0F, 0F, -0.5F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, -0.5F, -3F, 0F, 0F, -0.4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.4F); // Import 
		gunModel[19].setRotationPoint(-10F, -27F, -5F);

		gunModel[20].addShapeBox(0F, 0F, 0F, 2, 3, 14, 0F, 0F, 0F, -2.5F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F); // Import 
		gunModel[20].setRotationPoint(-10F, -25F, -7F);

		gunModel[21].addShapeBox(0F, 0F, 0F, 2, 3, 14, 0F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F); // Import 
		gunModel[21].setRotationPoint(-10F, -22F, -7F);

		gunModel[22].addShapeBox(0F, 0F, 0F, 2, 3, 14, 0F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -2.5F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2.5F); // Import 
		gunModel[22].setRotationPoint(-10F, -19F, -7F);

		gunModel[23].addShapeBox(0F, 0F, 0F, 2, 2, 10, 0F, 0F, 0F, -0.4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.4F, 0F, -0.5F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, -0.5F, -3F); // Import 
		gunModel[23].setRotationPoint(-10F, -16F, -5F);

		gunModel[24].addShapeBox(0F, 0F, 0F, 6, 2, 1, 0F, -1.75F, 0F, 0F, -1.75F, 0F, 0F, -1.75F, 0F, 0F, -1.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import 
		gunModel[24].setRotationPoint(-3F, -23.5F, -8.5F);

		gunModel[25].addShapeBox(0F, 0F, 0F, 6, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import 
		gunModel[25].setRotationPoint(-3F, -21.5F, -8.5F);

		gunModel[26].addShapeBox(0F, 0F, 0F, 6, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.75F, 0F, 0F, -1.75F, 0F, 0F, -1.75F, 0F, 0F, -1.75F, 0F, 0F); // Import 
		gunModel[26].setRotationPoint(-3F, -19.5F, -8.5F);

		gunModel[27].addShapeBox(0F, 0F, 0F, 6, 2, 1, 0F, -2F, -0.75F, 0F, -2F, -0.75F, 0F, -2F, -0.75F, 0F, -2F, -0.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import 
		gunModel[27].setRotationPoint(-3F, -25.5F, -8F);

		gunModel[28].addShapeBox(0F, 0F, 0F, 10, 2, 1, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Import 
		gunModel[28].setRotationPoint(-5F, -23.5F, -8F);

		gunModel[29].addShapeBox(0F, 0F, 0F, 8, 2, 1, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F); // Import 
		gunModel[29].setRotationPoint(-4F, -21.5F, -8F);

		gunModel[30].addShapeBox(0F, 0F, 0F, 10, 2, 1, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F); // Import 
		gunModel[30].setRotationPoint(-5F, -19.5F, -8F);

		gunModel[31].addShapeBox(0F, 0F, 0F, 6, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, -0.75F, 0F, -2F, -0.75F, 0F, -2F, -0.75F, 0F, -2F, -0.75F, 0F); // Import 
		gunModel[31].setRotationPoint(-3F, -17.5F, -8F);

		gunModel[32].addShapeBox(0F, 0F, 0F, 13, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1.75F, 0F, 0F, -1.75F, 0F, 0F, -1F, 0F, 0F); // Import 
		gunModel[32].setRotationPoint(-7F, -14.5F, -3F);

		gunModel[33].addShapeBox(0F, 0F, 0F, 6, 1, 5, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import 
		gunModel[33].setRotationPoint(-3F, -28F, -2.5F);

		gunModel[34].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import 
		gunModel[34].setRotationPoint(-1F, -25.7F, -8F);

		gunModel[35].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F); // Import 
		gunModel[35].setRotationPoint(-1F, -16.3F, -8F);

		gunModel[36].addShapeBox(0F, 0F, 0F, 1, 2, 1, 0F, 0.25F, -0.3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.25F, -0.3F, 0F, 0.25F, -0.3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.25F, -0.3F, 0F); // Import 
		gunModel[36].setRotationPoint(-5.5F, -21.7F, -8F);

		gunModel[37].addShapeBox(0F, 0F, 0F, 1, 2, 1, 0F, 0F, 0F, 0F, 0.25F, -0.3F, 0F, 0.25F, -0.3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.25F, -0.3F, 0F, 0.25F, -0.3F, 0F, 0F, 0F, 0F); // Import 
		gunModel[37].setRotationPoint(4.5F, -21.7F, -8F);

		gunModel[38].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import 
		gunModel[38].setRotationPoint(-4F, -23.7F, -8F);
		gunModel[38].rotateAngleZ = 0.6981317F;

		gunModel[39].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F); // Import 
		gunModel[39].setRotationPoint(2.2F, -17F, -8F);
		gunModel[39].rotateAngleZ = 0.78539816F;

		gunModel[40].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import 
		gunModel[40].setRotationPoint(3F, -24.8F, -8F);
		gunModel[40].rotateAngleZ = -0.78539816F;

		gunModel[41].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F, -0.3F, 0.25F, 0F); // Import 
		gunModel[41].setRotationPoint(-3.7F, -18.5F, -8F);
		gunModel[41].rotateAngleZ = -0.78539816F;


		defaultScopeModel = new ModelRendererTurbo[7];
		defaultScopeModel[0] = new ModelRendererTurbo(this, 89, 1, textureX, textureY); // Import 
		defaultScopeModel[1] = new ModelRendererTurbo(this, 137, 1, textureX, textureY); // Import 
		defaultScopeModel[2] = new ModelRendererTurbo(this, 297, 1, textureX, textureY); // Import 
		defaultScopeModel[3] = new ModelRendererTurbo(this, 217, 1, textureX, textureY); // Import 
		defaultScopeModel[4] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Import 
		defaultScopeModel[5] = new ModelRendererTurbo(this, 217, 1, textureX, textureY); // Import 
		defaultScopeModel[6] = new ModelRendererTurbo(this, 33, 1, textureX, textureY); // Import 

		defaultScopeModel[0].addShapeBox(0F, 0F, 0F, 3, 6, 1, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import 
		defaultScopeModel[0].setRotationPoint(0F, -32F, -3F);

		defaultScopeModel[1].addShapeBox(0F, 0F, 0F, 3, 6, 1, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import 
		defaultScopeModel[1].setRotationPoint(0F, -32F, 2F);

		defaultScopeModel[2].addShapeBox(0F, 0F, 0F, 1, 1, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Import 
		defaultScopeModel[2].setRotationPoint(32F, -28F, -2.5F);

		defaultScopeModel[3].addShapeBox(0F, 0F, 0F, 1, 1, 5, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import 
		defaultScopeModel[3].setRotationPoint(32F, -32F, -2.5F);

		defaultScopeModel[4].addBox(0F, 0F, 0F, 1, 3, 1, 0F); // Import 
		defaultScopeModel[4].setRotationPoint(32F, -31F, -2.5F);

		defaultScopeModel[5].addBox(0F, 0F, 0F, 1, 3, 1, 0F); // Import 
		defaultScopeModel[5].setRotationPoint(32F, -31F, 1.5F);

		defaultScopeModel[6].addShapeBox(0F, 0F, 0F, 3, 2, 1, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import 
		defaultScopeModel[6].setRotationPoint(0F, -30F, -0.5F);


		ammoModel = new ModelRendererTurbo[5];
		ammoModel[0] = new ModelRendererTurbo(this, 89, 41, textureX, textureY); // Import 
		ammoModel[1] = new ModelRendererTurbo(this, 361, 33, textureX, textureY); // Import 
		ammoModel[2] = new ModelRendererTurbo(this, 129, 41, textureX, textureY); // Import 
		ammoModel[3] = new ModelRendererTurbo(this, 377, 1, textureX, textureY); // Import 
		ammoModel[4] = new ModelRendererTurbo(this, 457, 17, textureX, textureY); // Import 

		ammoModel[0].addBox(0F, 0F, 0F, 11, 26, 5, 0F); // Import 
		ammoModel[0].setRotationPoint(13.5F, -12.5F, -2.5F);

		ammoModel[1].addBox(0F, 0F, 0F, 12, 4, 6, 0F); // Import 
		ammoModel[1].setRotationPoint(13F, -9.5F, -3F);

		ammoModel[2].addBox(0F, 0F, 0F, 12, 4, 6, 0F); // Import 
		ammoModel[2].setRotationPoint(13F, 5F, -3F);

		ammoModel[3].addShapeBox(0F, 0F, 0F, 2, 3, 4, 0F, 0F, -1F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F); // Import 
		ammoModel[3].setRotationPoint(11F, -8.5F, -2F);

		ammoModel[4].addBox(0F, 0F, 0F, 1, 10, 2, 0F); // Import 
		ammoModel[4].setRotationPoint(11F, -5.5F, -1F);


		slideModel = new ModelRendererTurbo[3];
		slideModel[0] = new ModelRendererTurbo(this, 225, 1, textureX, textureY); // Import 
		slideModel[1] = new ModelRendererTurbo(this, 401, 1, textureX, textureY); // Import 
		slideModel[2] = new ModelRendererTurbo(this, 377, 9, textureX, textureY); // Import 

		slideModel[0].addBox(0F, 0F, 0F, 1, 3, 1, 0F); // Import 
		slideModel[0].setRotationPoint(27F, -27F, 5.1F);
		slideModel[0].rotateAngleX = -0.78539816F;

		slideModel[1].addShapeBox(0F, 0F, 0F, 2, 3, 3, 0F, -0.5F, 0F, -0.75F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, -0.75F, -0.5F, 0F, -0.75F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, -0.75F); // Import 
		slideModel[1].setRotationPoint(25.5F, -29F, 5.5F);
		slideModel[1].rotateAngleX = -0.78539816F;

		slideModel[2].addShapeBox(0F, 0F, 0F, 2, 3, 3, 0F, 0F, 0F, 0F, -0.5F, 0F, -0.75F, -0.5F, 0F, -0.75F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, -0.75F, -0.5F, 0F, -0.75F, 0F, 0F, 0F); // Import 
		slideModel[2].setRotationPoint(27.5F, -29F, 5.5F);
		slideModel[2].rotateAngleX = -0.78539816F;

		barrelAttachPoint = new Vector3f(63F /16F, 21F /16F, 0F /16F);
		stockAttachPoint = new Vector3f(-10F /16F, 20.5F /16F, 0F /16F);
		gripAttachPoint = new Vector3f(35 /16F, 20.5F /16F, 0F /16F);

		gunSlideDistance = 0.5F;
		animationType = EnumAnimationType.BOTTOM_CLIP;


		translateAll(0F, 0F, 0F);


		flipAll();
	}
}
