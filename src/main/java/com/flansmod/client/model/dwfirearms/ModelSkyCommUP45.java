package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelSkyCommUP45 extends ModelGun
{
	int textureX = 512;
	int textureY = 256;

	public ModelSkyCommUP45()
	{
		gunModel = new ModelRendererTurbo[73];
		gunModel[0] = new ModelRendererTurbo(this, 1, 11, textureX, textureY); // Box 32
		gunModel[1] = new ModelRendererTurbo(this, 120, 80, textureX, textureY); // Box 33
		gunModel[2] = new ModelRendererTurbo(this, 1, 23, textureX, textureY); // Box 34
		gunModel[3] = new ModelRendererTurbo(this, 1, 35, textureX, textureY); // Box 35
		gunModel[4] = new ModelRendererTurbo(this, 38, 23, textureX, textureY); // Box 39
		gunModel[5] = new ModelRendererTurbo(this, 38, 35, textureX, textureY); // Box 40
		gunModel[6] = new ModelRendererTurbo(this, 59, 35, textureX, textureY); // Box 0
		gunModel[7] = new ModelRendererTurbo(this, 59, 25, textureX, textureY); // Box 1
		gunModel[8] = new ModelRendererTurbo(this, 1, 57, textureX, textureY); // Box 0
		gunModel[9] = new ModelRendererTurbo(this, 38, 57, textureX, textureY); // Box 2
		gunModel[10] = new ModelRendererTurbo(this, 1, 68, textureX, textureY); // Box 3
		gunModel[11] = new ModelRendererTurbo(this, 188, 79, textureX, textureY); // Box 4
		gunModel[12] = new ModelRendererTurbo(this, 120, 1, textureX, textureY); // Box 5
		gunModel[13] = new ModelRendererTurbo(this, 120, 44, textureX, textureY); // Box 6
		gunModel[14] = new ModelRendererTurbo(this, 143, 56, textureX, textureY); // Box 7
		gunModel[15] = new ModelRendererTurbo(this, 161, 93, textureX, textureY); // Box 8
		gunModel[16] = new ModelRendererTurbo(this, 120, 92, textureX, textureY); // Box 9
		gunModel[17] = new ModelRendererTurbo(this, 120, 16, textureX, textureY); // Box 19
		gunModel[18] = new ModelRendererTurbo(this, 195, 18, textureX, textureY); // Box 20
		gunModel[19] = new ModelRendererTurbo(this, 195, 18, textureX, textureY); // Box 21
		gunModel[20] = new ModelRendererTurbo(this, 177, 56, textureX, textureY); // Box 22
		gunModel[21] = new ModelRendererTurbo(this, 120, 67, textureX, textureY); // Box 23
		gunModel[22] = new ModelRendererTurbo(this, 20, 68, textureX, textureY); // Box 24
		gunModel[23] = new ModelRendererTurbo(this, 57, 57, textureX, textureY); // Box 25
		gunModel[24] = new ModelRendererTurbo(this, 187, 33, textureX, textureY); // Box 26
		gunModel[25] = new ModelRendererTurbo(this, 120, 33, textureX, textureY); // Box 27
		gunModel[26] = new ModelRendererTurbo(this, 66, 1, textureX, textureY); // Box 28
		gunModel[27] = new ModelRendererTurbo(this, 174, 70, textureX, textureY); // Box 30
		gunModel[28] = new ModelRendererTurbo(this, 65, 13, textureX, textureY); // Box 0
		gunModel[29] = new ModelRendererTurbo(this, 65, 13, textureX, textureY); // Box 1
		gunModel[30] = new ModelRendererTurbo(this, 65, 13, textureX, textureY); // Box 2
		gunModel[31] = new ModelRendererTurbo(this, 44, 13, textureX, textureY); // Box 3
		gunModel[32] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 4
		gunModel[33] = new ModelRendererTurbo(this, 44, 13, textureX, textureY); // Box 5
		gunModel[34] = new ModelRendererTurbo(this, 44, 13, textureX, textureY); // Box 6
		gunModel[35] = new ModelRendererTurbo(this, 44, 13, textureX, textureY); // Box 7
		gunModel[36] = new ModelRendererTurbo(this, 44, 13, textureX, textureY); // Box 8
		gunModel[37] = new ModelRendererTurbo(this, 44, 13, textureX, textureY); // Box 9
		gunModel[38] = new ModelRendererTurbo(this, 80, 58, textureX, textureY); // Box 10
		gunModel[39] = new ModelRendererTurbo(this, 80, 47, textureX, textureY); // Box 11
		gunModel[40] = new ModelRendererTurbo(this, 245, 44, textureX, textureY); // Box 12
		gunModel[41] = new ModelRendererTurbo(this, 195, 44, textureX, textureY); // Box 13
		gunModel[42] = new ModelRendererTurbo(this, 220, 44, textureX, textureY); // Box 14
		gunModel[43] = new ModelRendererTurbo(this, 216, 35, textureX, textureY); // Box 15
		gunModel[44] = new ModelRendererTurbo(this, 253, 35, textureX, textureY); // Box 16
		gunModel[45] = new ModelRendererTurbo(this, 253, 35, textureX, textureY); // Box 17
		gunModel[46] = new ModelRendererTurbo(this, 135, 109, textureX, textureY); // Box 18
		gunModel[47] = new ModelRendererTurbo(this, 120, 104, textureX, textureY); // Box 22
		gunModel[48] = new ModelRendererTurbo(this, 135, 109, textureX, textureY); // Box 24
		gunModel[49] = new ModelRendererTurbo(this, 156, 109, textureX, textureY); // Box 25
		gunModel[50] = new ModelRendererTurbo(this, 65, 13, textureX, textureY); // Box 52
		gunModel[51] = new ModelRendererTurbo(this, 120, 109, textureX, textureY); // Box 53
		gunModel[52] = new ModelRendererTurbo(this, 120, 109, textureX, textureY); // Box 54
		gunModel[53] = new ModelRendererTurbo(this, 39, 68, textureX, textureY); // Box 55
		gunModel[54] = new ModelRendererTurbo(this, 120, 56, textureX, textureY); // Box 68
		gunModel[55] = new ModelRendererTurbo(this, 166, 56, textureX, textureY); // Box 69
		gunModel[56] = new ModelRendererTurbo(this, 155, 79, textureX, textureY); // Box 70
		gunModel[57] = new ModelRendererTurbo(this, 188, 57, textureX, textureY); // Box 71
		gunModel[58] = new ModelRendererTurbo(this, 217, 60, textureX, textureY); // Box 72
		gunModel[59] = new ModelRendererTurbo(this, 147, 69, textureX, textureY); // Box 73
		gunModel[60] = new ModelRendererTurbo(this, 291, 22, textureX, textureY); // Box 0
		gunModel[61] = new ModelRendererTurbo(this, 270, 23, textureX, textureY); // Box 1
		gunModel[62] = new ModelRendererTurbo(this, 270, 23, textureX, textureY); // Box 2
		gunModel[63] = new ModelRendererTurbo(this, 161, 1, textureX, textureY); // Box 3
		gunModel[64] = new ModelRendererTurbo(this, 271, 3, textureX, textureY); // Box 4
		gunModel[65] = new ModelRendererTurbo(this, 99, 39, textureX, textureY); // Box 6
		gunModel[66] = new ModelRendererTurbo(this, 108, 39, textureX, textureY); // Box 7
		gunModel[67] = new ModelRendererTurbo(this, 80, 34, textureX, textureY); // Box 8
		gunModel[68] = new ModelRendererTurbo(this, 186, 1, textureX, textureY); // Box 9
		gunModel[69] = new ModelRendererTurbo(this, 186, 1, textureX, textureY); // Box 10
		gunModel[70] = new ModelRendererTurbo(this, 186, 1, textureX, textureY); // Box 11
		gunModel[71] = new ModelRendererTurbo(this, 238, 1, textureX, textureY); // Box 12
		gunModel[72] = new ModelRendererTurbo(this, 207, 2, textureX, textureY); // Box 13

		gunModel[0].addShapeBox(0F, 0F, 0F, 13, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F); // Box 32
		gunModel[0].setRotationPoint(-10F, -12F, -4F);

		gunModel[1].addBox(0F, 0F, 0F, 9, 3, 8, 0F); // Box 33
		gunModel[1].setRotationPoint(3F, -12F, -4F);

		gunModel[2].addShapeBox(0F, 0F, 0F, 10, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, -2F, 0F); // Box 34
		gunModel[2].setRotationPoint(-14F, 6F, -4F);

		gunModel[3].addShapeBox(0F, 0F, 0F, 10, 13, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 6F, 0F, 0F, -6F, 0F, 0F, -6F, 0F, 0F, 6F, 0F, 0F); // Box 35
		gunModel[3].setRotationPoint(-8F, -7F, -4F);

		gunModel[4].addShapeBox(0F, 0F, 0F, 2, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, -1.5F, -1F, 0F, -1.5F, 1F, 0F, 0F); // Box 39
		gunModel[4].setRotationPoint(-4F, 6F, -4F);

		gunModel[5].addShapeBox(0F, 0F, 0F, 2, 13, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 6F, 0F, 0F, -6F, 0F, -1.5F, -6F, 0F, -1.5F, 6F, 0F, 0F); // Box 40
		gunModel[5].setRotationPoint(2F, -7F, -4F);

		gunModel[6].addShapeBox(0F, 0F, 0F, 1, 13, 8, 0F, -6F, 0F, -1.5F, 6F, 0F, 0F, 6F, 0F, 0F, -6F, 0F, -1.5F, 0F, -0.5F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -1.5F); // Box 0
		gunModel[6].setRotationPoint(-15F, -7F, -4F);

		gunModel[7].addShapeBox(0F, 0F, 0F, 1, 1, 8, 0F, 0F, 0.5F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -1.5F); // Box 1
		gunModel[7].setRotationPoint(-15F, 6F, -4F);

		gunModel[8].addShapeBox(0F, 0F, 0F, 10, 2, 8, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 0
		gunModel[8].setRotationPoint(-8F, -9F, -4F);

		gunModel[9].addShapeBox(0F, 0F, 0F, 1, 2, 8, 0F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F); // Box 2
		gunModel[9].setRotationPoint(-9F, -9F, -4F);

		gunModel[10].addShapeBox(0F, 0F, 0F, 1, 3, 8, 0F, 2F, 0F, -1.5F, -2F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F); // Box 3
		gunModel[10].setRotationPoint(-9F, -12F, -4F);

		gunModel[11].addShapeBox(0F, 0F, 0F, 3, 3, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F); // Box 4
		gunModel[11].setRotationPoint(-4F, -15F, -4.5F);

		gunModel[12].addBox(0F, 0F, 0F, 11, 5, 9, 0F); // Box 5
		gunModel[12].setRotationPoint(-11F, -20F, -4.5F);

		gunModel[13].addBox(0F, 0F, 0F, 29, 3, 8, 0F); // Box 6
		gunModel[13].setRotationPoint(-3F, -15F, -4F);

		gunModel[14].addShapeBox(0F, 0F, 0F, 9, 8, 2, 0F, -8F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -8F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 7
		gunModel[14].setRotationPoint(-11F, -28F, -4F);

		gunModel[15].addBox(0F, 0F, 0F, 25, 2, 8, 0F); // Box 8
		gunModel[15].setRotationPoint(1F, -22F, -4F);

		gunModel[16].addShapeBox(0F, 0F, 0F, 12, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F); // Box 9
		gunModel[16].setRotationPoint(14F, -9F, -4F);

		gunModel[17].addBox(0F, 0F, 0F, 26, 5, 11, 0F); // Box 19
		gunModel[17].setRotationPoint(26F, -20F, -5.5F);

		gunModel[18].addShapeBox(0F, 0F, 0F, 26, 3, 11, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 20
		gunModel[18].setRotationPoint(26F, -23F, -5.5F);

		gunModel[19].addShapeBox(0F, 0F, 0F, 26, 3, 11, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 21
		gunModel[19].setRotationPoint(26F, -15F, -5.5F);

		gunModel[20].addBox(0F, 0F, 0F, 3, 8, 2, 0F); // Box 22
		gunModel[20].setRotationPoint(-2F, -28F, -4F);

		gunModel[21].addBox(0F, 0F, 0F, 5, 3, 8, 0F); // Box 23
		gunModel[21].setRotationPoint(34F, -24F, -4F);

		gunModel[22].addShapeBox(0F, 0F, 0F, 1, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F); // Box 24
		gunModel[22].setRotationPoint(12F, -12F, -4F);

		gunModel[23].addShapeBox(0F, 0F, 0F, 2, 2, 8, 0F, -1F, 0F, 0F, 1F, 0F, -1.5F, 1F, 0F, -1.5F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F); // Box 25
		gunModel[23].setRotationPoint(2F, -9F, -4F);

		gunModel[24].addShapeBox(0F, 0F, 0F, 6, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, -2F, 0.5F, 0F, -2F, 0.5F, 0F, 0F, 0.5F, 0F); // Box 26
		gunModel[24].setRotationPoint(26F, -11.5F, -4F);

		gunModel[25].addShapeBox(0F, 0F, 0F, 25, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F); // Box 27
		gunModel[25].setRotationPoint(26F, -14F, -4F);

		gunModel[26].addBox(0F, 0F, 0F, 21, 2, 7, 0F); // Box 28
		gunModel[26].setRotationPoint(30F, -11.5F, -3.5F);

		gunModel[27].addShapeBox(0F, 0F, 0F, 5, 3, 5, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 30
		gunModel[27].setRotationPoint(34F, -28F, -2.5F);

		gunModel[28].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 0
		gunModel[28].setRotationPoint(30F, -9.5F, -3.5F);

		gunModel[29].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 1
		gunModel[29].setRotationPoint(36F, -9.5F, -3.5F);

		gunModel[30].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 2
		gunModel[30].setRotationPoint(42F, -9.5F, -3.5F);

		gunModel[31].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 3
		gunModel[31].setRotationPoint(1F, -26F, -3.5F);

		gunModel[32].addBox(0F, 0F, 0F, 25, 2, 7, 0F); // Box 4
		gunModel[32].setRotationPoint(1F, -24F, -3.5F);

		gunModel[33].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 5
		gunModel[33].setRotationPoint(7F, -26F, -3.5F);

		gunModel[34].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 6
		gunModel[34].setRotationPoint(13F, -26F, -3.5F);

		gunModel[35].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 7
		gunModel[35].setRotationPoint(19F, -26F, -3.5F);

		gunModel[36].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 8
		gunModel[36].setRotationPoint(25F, -26F, -3.5F);

		gunModel[37].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 9
		gunModel[37].setRotationPoint(31F, -26F, -3.5F);

		gunModel[38].addBox(0F, 0F, 0F, 8, 2, 7, 0F); // Box 10
		gunModel[38].setRotationPoint(26F, -24F, -3.5F);

		gunModel[39].addBox(0F, 0F, 0F, 8, 2, 8, 0F); // Box 11
		gunModel[39].setRotationPoint(26F, -22F, -4F);

		gunModel[40].addBox(0F, 0F, 0F, 3, 3, 9, 0F); // Box 12
		gunModel[40].setRotationPoint(52F, -19F, -4.5F);

		gunModel[41].addShapeBox(0F, 0F, 0F, 3, 3, 9, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 13
		gunModel[41].setRotationPoint(52F, -22F, -4.5F);

		gunModel[42].addShapeBox(0F, 0F, 0F, 3, 3, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 14
		gunModel[42].setRotationPoint(52F, -16F, -4.5F);

		gunModel[43].addBox(0F, 0F, 0F, 12, 2, 6, 0F); // Box 15
		gunModel[43].setRotationPoint(55F, -18.5F, -3F);

		gunModel[44].addShapeBox(0F, 0F, 0F, 12, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 16
		gunModel[44].setRotationPoint(55F, -16.5F, -3F);

		gunModel[45].addShapeBox(0F, 0F, 0F, 12, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 17
		gunModel[45].setRotationPoint(55F, -20.5F, -3F);

		gunModel[46].addShapeBox(0F, 0F, 0F, 6, 2, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 18
		gunModel[46].setRotationPoint(43F, -27F, -2F);

		gunModel[47].addBox(0F, 0F, 0F, 15, 2, 2, 0F); // Box 22
		gunModel[47].setRotationPoint(37F, -26F, -1F);

		gunModel[48].addShapeBox(0F, 0F, 0F, 6, 2, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 24
		gunModel[48].setRotationPoint(43F, -25F, -2F);

		gunModel[49].addShapeBox(0F, 0F, 0F, 2, 4, 2, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 25
		gunModel[49].setRotationPoint(52F, -26F, -1F);

		gunModel[50].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 52
		gunModel[50].setRotationPoint(48F, -9.5F, -3.5F);

		gunModel[51].addShapeBox(0F, 0F, 0F, 3, 2, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 53
		gunModel[51].setRotationPoint(37F, -25F, -2F);

		gunModel[52].addShapeBox(0F, 0F, 0F, 3, 2, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 54
		gunModel[52].setRotationPoint(37F, -27F, -2F);

		gunModel[53].addShapeBox(0F, 0F, 0F, 12, 3, 8, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 55
		gunModel[53].setRotationPoint(14F, -12F, -4F);

		gunModel[54].addShapeBox(0F, 0F, 0F, 9, 8, 2, 0F, -8F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -8F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 68
		gunModel[54].setRotationPoint(-11F, -28F, 2F);

		gunModel[55].addBox(0F, 0F, 0F, 3, 8, 2, 0F); // Box 69
		gunModel[55].setRotationPoint(-2F, -28F, 2F);

		gunModel[56].addBox(0F, 0F, 0F, 7, 3, 9, 0F); // Box 70
		gunModel[56].setRotationPoint(-11F, -15F, -4.5F);

		gunModel[57].addShapeBox(0F, 0F, 0F, 10, 5, 4, 0F, -5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 71
		gunModel[57].setRotationPoint(-9F, -26F, -2F);

		gunModel[58].addBox(0F, 0F, 0F, 10, 1, 4, 0F); // Box 72
		gunModel[58].setRotationPoint(-9F, -21F, -2F);

		gunModel[59].addShapeBox(0F, 0F, 0F, 5, 1, 8, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 73
		gunModel[59].setRotationPoint(34F, -25F, -4F);

		gunModel[60].addBox(0F, 0F, 0F, 3, 3, 7, 0F); // Box 0
		gunModel[60].setRotationPoint(56F, -19F, -3.5F);

		gunModel[61].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 1
		gunModel[61].setRotationPoint(56F, -16F, -3.5F);

		gunModel[62].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 2
		gunModel[62].setRotationPoint(56F, -21F, -3.5F);

		gunModel[63].addBox(0F, 0F, 0F, 3, 5, 9, 0F); // Box 3
		gunModel[63].setRotationPoint(23F, -20F, -4.5F);

		gunModel[64].addBox(0F, 0F, 0F, 9, 5, 7, 0F); // Box 4
		gunModel[64].setRotationPoint(14F, -20F, -2.5F);

		gunModel[65].addBox(0F, 0F, 0F, 1, 4, 3, 0F); // Box 6
		gunModel[65].setRotationPoint(14F, -20F, -5F);

		gunModel[66].addBox(0F, 0F, 0F, 1, 4, 3, 0F); // Box 7
		gunModel[66].setRotationPoint(22F, -20F, -5F);

		gunModel[67].addBox(0F, 0F, 0F, 9, 1, 3, 0F); // Box 8
		gunModel[67].setRotationPoint(14F, -16F, -5F);

		gunModel[68].addBox(0F, 0F, 0F, 1, 5, 9, 0F); // Box 9
		gunModel[68].setRotationPoint(1F, -20F, -4.5F);

		gunModel[69].addBox(0F, 0F, 0F, 1, 5, 9, 0F); // Box 10
		gunModel[69].setRotationPoint(3F, -20F, -4.5F);

		gunModel[70].addBox(0F, 0F, 0F, 1, 5, 9, 0F); // Box 11
		gunModel[70].setRotationPoint(5F, -20F, -4.5F);

		gunModel[71].addBox(0F, 0F, 0F, 7, 5, 9, 0F); // Box 12
		gunModel[71].setRotationPoint(7F, -20F, -4.5F);

		gunModel[72].addBox(0F, 0F, 0F, 7, 5, 8, 0F); // Box 13
		gunModel[72].setRotationPoint(0F, -20F, -4F);


		defaultScopeModel = new ModelRendererTurbo[7];
		defaultScopeModel[0] = new ModelRendererTurbo(this, 80, 29, textureX, textureY); // Box 14
		defaultScopeModel[1] = new ModelRendererTurbo(this, 96, 29, textureX, textureY); // Box 15
		defaultScopeModel[2] = new ModelRendererTurbo(this, 87, 28, textureX, textureY); // Box 16
		defaultScopeModel[3] = new ModelRendererTurbo(this, 87, 28, textureX, textureY); // Box 18
		defaultScopeModel[4] = new ModelRendererTurbo(this, 96, 29, textureX, textureY); // Box 19
		defaultScopeModel[5] = new ModelRendererTurbo(this, 87, 28, textureX, textureY); // Box 20
		defaultScopeModel[6] = new ModelRendererTurbo(this, 87, 28, textureX, textureY); // Box 21

		defaultScopeModel[0].addBox(0F, 0F, 0F, 2, 3, 1, 0F); // Box 14
		defaultScopeModel[0].setRotationPoint(34.5F, -31F, -0.5F);

		defaultScopeModel[1].addBox(0F, 0F, 0F, 2, 1, 3, 0F); // Box 15
		defaultScopeModel[1].setRotationPoint(34.5F, -29F, -1.5F);

		defaultScopeModel[2].addShapeBox(0F, 0F, 0F, 2, 3, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // Box 16
		defaultScopeModel[2].setRotationPoint(34.5F, -31F, -3F);

		defaultScopeModel[3].addShapeBox(0F, 0F, 0F, 2, 3, 2, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 18
		defaultScopeModel[3].setRotationPoint(34.5F, -34F, -3F);

		defaultScopeModel[4].addBox(0F, 0F, 0F, 2, 1, 3, 0F); // Box 19
		defaultScopeModel[4].setRotationPoint(34.5F, -34F, -1.5F);

		defaultScopeModel[5].addShapeBox(0F, 0F, 0F, 2, 3, 2, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 20
		defaultScopeModel[5].setRotationPoint(34.5F, -34F, 1F);

		defaultScopeModel[6].addShapeBox(0F, 0F, 0F, 2, 3, 2, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F); // Box 21
		defaultScopeModel[6].setRotationPoint(34.5F, -31F, 1F);


		defaultStockModel = new ModelRendererTurbo[18];
		defaultStockModel[0] = new ModelRendererTurbo(this, 181, 163, textureX, textureY); // Box 25
		defaultStockModel[1] = new ModelRendererTurbo(this, 202, 127, textureX, textureY); // Box 26
		defaultStockModel[2] = new ModelRendererTurbo(this, 120, 127, textureX, textureY); // Box 27
		defaultStockModel[3] = new ModelRendererTurbo(this, 179, 127, textureX, textureY); // Box 28
		defaultStockModel[4] = new ModelRendererTurbo(this, 145, 158, textureX, textureY); // Box 29
		defaultStockModel[5] = new ModelRendererTurbo(this, 120, 116, textureX, textureY); // Box 30
		defaultStockModel[6] = new ModelRendererTurbo(this, 197, 117, textureX, textureY); // Box 31
		defaultStockModel[7] = new ModelRendererTurbo(this, 202, 140, textureX, textureY); // Box 32
		defaultStockModel[8] = new ModelRendererTurbo(this, 120, 138, textureX, textureY); // Box 33
		defaultStockModel[9] = new ModelRendererTurbo(this, 161, 141, textureX, textureY); // Box 34
		defaultStockModel[10] = new ModelRendererTurbo(this, 120, 151, textureX, textureY); // Box 35
		defaultStockModel[11] = new ModelRendererTurbo(this, 120, 179, textureX, textureY); // Box 36
		defaultStockModel[12] = new ModelRendererTurbo(this, 120, 179, textureX, textureY); // Box 37
		defaultStockModel[13] = new ModelRendererTurbo(this, 164, 160, textureX, textureY); // Box 38
		defaultStockModel[14] = new ModelRendererTurbo(this, 202, 167, textureX, textureY); // Box 39
		defaultStockModel[15] = new ModelRendererTurbo(this, 181, 154, textureX, textureY); // Box 40
		defaultStockModel[16] = new ModelRendererTurbo(this, 202, 159, textureX, textureY); // Box 41
		defaultStockModel[17] = new ModelRendererTurbo(this, 164, 152, textureX, textureY); // Box 42

		defaultStockModel[0].addBox(0F, 0F, 0F, 3, 8, 7, 0F); // Box 25
		defaultStockModel[0].setRotationPoint(-12F, -20.5F, -3.5F);

		defaultStockModel[1].addBox(0F, 0F, 0F, 5, 3, 7, 0F); // Box 26
		defaultStockModel[1].setRotationPoint(-17F, -15.5F, -3.5F);

		defaultStockModel[2].addShapeBox(0F, 0F, 0F, 22, 3, 7, 0F, 0F, 0F, 0F, 0F, 13F, 0F, 0F, 13F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -13F, 0F, 0F, -13F, 0F, 0F, 0F, 0F); // Box 27
		defaultStockModel[2].setRotationPoint(-39F, -2.5F, -3.5F);

		defaultStockModel[3].addBox(0F, 0F, 0F, 4, 3, 7, 0F); // Box 28
		defaultStockModel[3].setRotationPoint(-43F, -2.5F, -3.5F);

		defaultStockModel[4].addBox(0F, 0F, 0F, 2, 13, 7, 0F); // Box 29
		defaultStockModel[4].setRotationPoint(-43F, -15.5F, -3.5F);

		defaultStockModel[5].addBox(0F, 0F, 0F, 31, 3, 7, 0F); // Box 30
		defaultStockModel[5].setRotationPoint(-43F, -18.5F, -3.5F);

		defaultStockModel[6].addShapeBox(0F, 0F, 0F, 31, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 31
		defaultStockModel[6].setRotationPoint(-43F, -20.5F, -3.5F);

		defaultStockModel[7].addShapeBox(0F, 0F, 0F, 12, 2, 8, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 32
		defaultStockModel[7].setRotationPoint(-37F, -21F, -4F);

		defaultStockModel[8].addBox(0F, 0F, 0F, 12, 4, 8, 0F); // Box 33
		defaultStockModel[8].setRotationPoint(-37F, -19F, -4F);

		defaultStockModel[9].addShapeBox(0F, 0F, 0F, 12, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 34
		defaultStockModel[9].setRotationPoint(-37F, -15F, -4F);

		defaultStockModel[10].addBox(0F, 0F, 0F, 4, 19, 8, 0F); // Box 35
		defaultStockModel[10].setRotationPoint(-47F, -19F, -4F);

		defaultStockModel[11].addShapeBox(0F, 0F, 0F, 4, 2, 8, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 36
		defaultStockModel[11].setRotationPoint(-47F, -21F, -4F);

		defaultStockModel[12].addShapeBox(0F, 0F, 0F, 4, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 37
		defaultStockModel[12].setRotationPoint(-47F, 0F, -4F);

		defaultStockModel[13].addShapeBox(0F, 0F, 0F, 2, 12, 6, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 38
		defaultStockModel[13].setRotationPoint(-49F, -19F, -3F);

		defaultStockModel[14].addShapeBox(0F, 0F, 0F, 1, 5, 6, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 39
		defaultStockModel[14].setRotationPoint(-48F, -5F, -3F);

		defaultStockModel[15].addShapeBox(0F, 0F, 0F, 2, 2, 6, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, -1F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -1F); // Box 40
		defaultStockModel[15].setRotationPoint(-49F, -7F, -3F);

		defaultStockModel[16].addShapeBox(0F, 0F, 0F, 1, 1, 6, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1.5F); // Box 41
		defaultStockModel[16].setRotationPoint(-48F, 0F, -3F);

		defaultStockModel[17].addShapeBox(0F, 0F, 0F, 2, 1, 6, 0F, 0F, 0F, -1.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1.5F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 42
		defaultStockModel[17].setRotationPoint(-49F, -20F, -3F);


		ammoModel = new ModelRendererTurbo[16];
		ammoModel[0] = new ModelRendererTurbo(this, 18, 80, textureX, textureY); // Box 56
		ammoModel[1] = new ModelRendererTurbo(this, 1, 80, textureX, textureY); // Box 57
		ammoModel[2] = new ModelRendererTurbo(this, 18, 90, textureX, textureY); // Box 58
		ammoModel[3] = new ModelRendererTurbo(this, 18, 90, textureX, textureY); // Box 59
		ammoModel[4] = new ModelRendererTurbo(this, 18, 100, textureX, textureY); // Box 60
		ammoModel[5] = new ModelRendererTurbo(this, 49, 80, textureX, textureY); // Box 61
		ammoModel[6] = new ModelRendererTurbo(this, 1, 112, textureX, textureY); // Box 62
		ammoModel[7] = new ModelRendererTurbo(this, 49, 97, textureX, textureY); // Box 64
		ammoModel[8] = new ModelRendererTurbo(this, 49, 107, textureX, textureY); // Box 65
		ammoModel[9] = new ModelRendererTurbo(this, 20, 121, textureX, textureY); // Box 66
		ammoModel[10] = new ModelRendererTurbo(this, 59, 121, textureX, textureY); // Box 67
		ammoModel[11] = new ModelRendererTurbo(this, 66, 111, textureX, textureY); // Box 22
		ammoModel[12] = new ModelRendererTurbo(this, 85, 111, textureX, textureY); // Box 23
		ammoModel[13] = new ModelRendererTurbo(this, 1, 133, textureX, textureY); // Box 24
		ammoModel[14] = new ModelRendererTurbo(this, 1, 139, textureX, textureY); // Box 113
		ammoModel[15] = new ModelRendererTurbo(this, 24, 139, textureX, textureY); // Box 114

		ammoModel[0].addBox(0F, 3F, -3.5F, 8, 2, 7, 0F); // Box 56
		ammoModel[0].setRotationPoint(15F, -9F, 0F);
		ammoModel[0].rotateAngleZ = 0.31415927F;

		ammoModel[1].addBox(8F, 3F, -3F, 2, 25, 6, 0F); // Box 57
		ammoModel[1].setRotationPoint(15F, -9F, 0F);
		ammoModel[1].rotateAngleZ = 0.31415927F;

		ammoModel[2].addShapeBox(0F, 5F, -3.5F, 4, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F); // Box 58
		ammoModel[2].setRotationPoint(15F, -9F, 0F);
		ammoModel[2].rotateAngleZ = 0.31415927F;

		ammoModel[3].addShapeBox(0F, 17F, -3.5F, 4, 2, 7, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 59
		ammoModel[3].setRotationPoint(15F, -9F, 0F);
		ammoModel[3].rotateAngleZ = 0.31415927F;

		ammoModel[4].addBox(3F, 7F, -3.5F, 5, 10, 7, 0F); // Box 60
		ammoModel[4].setRotationPoint(15F, -9F, 0F);
		ammoModel[4].rotateAngleZ = 0.31415927F;

		ammoModel[5].addBox(0F, 19F, -3.5F, 8, 9, 7, 0F); // Box 61
		ammoModel[5].setRotationPoint(15F, -9F, 0F);
		ammoModel[5].rotateAngleZ = 0.31415927F;

		ammoModel[6].addBox(0.5F, 5F, -3F, 3, 14, 6, 0F); // Box 62
		ammoModel[6].setRotationPoint(15F, -9F, 0F);
		ammoModel[6].rotateAngleZ = 0.31415927F;

		ammoModel[7].addShapeBox(0F, 1F, -3.5F, 8, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 64
		ammoModel[7].setRotationPoint(15F, -9F, 0F);
		ammoModel[7].rotateAngleZ = 0.31415927F;

		ammoModel[8].addShapeBox(8F, 1F, -3F, 2, 2, 6, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 65
		ammoModel[8].setRotationPoint(15F, -9F, 0F);
		ammoModel[8].rotateAngleZ = 0.31415927F;

		ammoModel[9].addBox(-0.5F, 28F, -4F, 11, 3, 8, 0F); // Box 66
		ammoModel[9].setRotationPoint(15F, -9F, 0F);
		ammoModel[9].rotateAngleZ = 0.31415927F;

		ammoModel[10].addShapeBox(0.5F, 25F, -4F, 7, 3, 8, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 67
		ammoModel[10].setRotationPoint(15F, -9F, 0F);
		ammoModel[10].rotateAngleZ = 0.31415927F;

		ammoModel[11].addShapeBox(1F, 0F, -1.5F, 6, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 22
		ammoModel[11].setRotationPoint(15F, -9F, 0F);
		ammoModel[11].rotateAngleZ = 0.31415927F;

		ammoModel[12].addShapeBox(7F, 0F, -1.5F, 2, 1, 3, 0F, 0F, 0F, -1F, -1F, 0F, -1.25F, -1F, 0F, -1.25F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 23
		ammoModel[12].setRotationPoint(15F, -9F, 0F);
		ammoModel[12].rotateAngleZ = 0.31415927F;

		ammoModel[13].addBox(0.5F, 31F, -2F, 9, 1, 4, 0F); // Box 24
		ammoModel[13].setRotationPoint(15F, -9F, 0F);
		ammoModel[13].rotateAngleZ = 0.31415927F;

		ammoModel[14].addBox(4F, 17F, -3.5F, 4, 2, 7, 0F); // Box 113
		ammoModel[14].setRotationPoint(15F, -9F, 0F);
		ammoModel[14].rotateAngleZ = 0.31415927F;

		ammoModel[15].addBox(4F, 5F, -3.5F, 4, 2, 7, 0F); // Box 114
		ammoModel[15].setRotationPoint(15F, -9F, 0F);
		ammoModel[15].rotateAngleZ = 0.31415927F;


		slideModel = new ModelRendererTurbo[1];
		slideModel[0] = new ModelRendererTurbo(this, 80, 40, textureX, textureY); // Box 5

		slideModel[0].addBox(0F, 0F, 0F, 7, 4, 2, 0F); // Box 5
		slideModel[0].setRotationPoint(15F, -20F, -4F);

		barrelAttachPoint = new Vector3f(68F /16F, 14.5F /16F, 0F /16F);
		stockAttachPoint = new Vector3f(-11F /16F, 16F /16F, 0F /16F);
		scopeAttachPoint = new Vector3f(18F /16F, 23F /16F, 0F /16F);
		gripAttachPoint = new Vector3f(42 /16F, 9F /16F, 0F /16F);

		gunSlideDistance = 0.25F;
		animationType = EnumAnimationType.BOTTOM_CLIP;

		flipAll();

		translateAll(0F, 0F, 0F);		
	}
}