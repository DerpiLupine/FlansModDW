package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelCRM15 extends ModelGun
{
	int textureX = 512;
	int textureY = 256;

	public ModelCRM15()
	{
		gunModel = new ModelRendererTurbo[25];
		gunModel[0] = new ModelRendererTurbo(this, 80, 1, textureX, textureY); // barrel1
		gunModel[1] = new ModelRendererTurbo(this, 80, 1, textureX, textureY); // barrel1-2
		gunModel[2] = new ModelRendererTurbo(this, 80, 10, textureX, textureY); // barrel2
		gunModel[3] = new ModelRendererTurbo(this, 80, 46, textureX, textureY); // body2
		gunModel[4] = new ModelRendererTurbo(this, 80, 25, textureX, textureY); // Box 0
		gunModel[5] = new ModelRendererTurbo(this, 80, 59, textureX, textureY); // Box 1
		gunModel[6] = new ModelRendererTurbo(this, 52, 36, textureX, textureY); // Box 23
		gunModel[7] = new ModelRendererTurbo(this, 52, 36, textureX, textureY); // Box 24
		gunModel[8] = new ModelRendererTurbo(this, 80, 107, textureX, textureY); // Box 24
		gunModel[9] = new ModelRendererTurbo(this, 52, 36, textureX, textureY); // Box 25
		gunModel[10] = new ModelRendererTurbo(this, 52, 36, textureX, textureY); // Box 26
		gunModel[11] = new ModelRendererTurbo(this, 151, 90, textureX, textureY); // foreRailConnector
		gunModel[12] = new ModelRendererTurbo(this, 80, 162, textureX, textureY); // stock7
		gunModel[13] = new ModelRendererTurbo(this, 52, 36, textureX, textureY); // Box 46
		gunModel[14] = new ModelRendererTurbo(this, 1, 65, textureX, textureY); // grip1
		gunModel[15] = new ModelRendererTurbo(this, 1, 93, textureX, textureY); // grip2
		gunModel[16] = new ModelRendererTurbo(this, 1, 36, textureX, textureY); // gripConnector
		gunModel[17] = new ModelRendererTurbo(this, 1, 49, textureX, textureY); // gripConnector2
		gunModel[18] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // ironSightBase1
		gunModel[19] = new ModelRendererTurbo(this, 1, 26, textureX, textureY); // rail
		gunModel[20] = new ModelRendererTurbo(this, 52, 36, textureX, textureY); // railBlock
		gunModel[21] = new ModelRendererTurbo(this, 1, 167, textureX, textureY); // Box 53
		gunModel[22] = new ModelRendererTurbo(this, 115, 123, textureX, textureY); // Box 54
		gunModel[23] = new ModelRendererTurbo(this, 1, 186, textureX, textureY); // Box 55
		gunModel[24] = new ModelRendererTurbo(this, 80, 123, textureX, textureY); // Box 56

		gunModel[0].addShapeBox(0F, 0F, 0F, 40, 2, 6, 0F,0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // barrel1
		gunModel[0].setRotationPoint(32F, -22F, -3F);

		gunModel[1].addShapeBox(0F, 0F, 0F, 40, 2, 6, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // barrel1-2
		gunModel[1].setRotationPoint(32F, -18F, -3F);

		gunModel[2].addBox(0F, 0F, 0F, 40, 2, 6, 0F); // barrel2
		gunModel[2].setRotationPoint(32F, -20F, -3F);

		gunModel[3].addShapeBox(0F, 0F, 0F, 45, 2, 10, 0F,0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body2
		gunModel[3].setRotationPoint(-16F, -25F, -5F);

		gunModel[4].addBox(0F, 0F, 0F, 45, 10, 10, 0F); // Box 0
		gunModel[4].setRotationPoint(-16F, -23F, -5F);

		gunModel[5].addBox(0F, 0F, 0F, 31, 3, 10, 0F); // Box 1
		gunModel[5].setRotationPoint(-2F, -13F, -5F);

		gunModel[6].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 23
		gunModel[6].setRotationPoint(1F, -29F, -3.5F);

		gunModel[7].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 24
		gunModel[7].setRotationPoint(13F, -29F, -3.5F);

		gunModel[8].addShapeBox(0F, 0F, 0F, 18, 5, 10, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F); // Box 24
		gunModel[8].setRotationPoint(8F, -10F, -5F);

		gunModel[9].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 25
		gunModel[9].setRotationPoint(7F, -29F, -3.5F);

		gunModel[10].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 26
		gunModel[10].setRotationPoint(19F, -29F, -3.5F);

		gunModel[11].addBox(0F, 0F, 0F, 2, 10, 6, 0F); // foreRailConnector
		gunModel[11].setRotationPoint(29F, -23F, -3F);

		gunModel[12].addShapeBox(0F, 0F, 0F, 28, 5, 1, 0F,0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // stock7
		gunModel[12].setRotationPoint(-46F, -21F, -4F);

		gunModel[13].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 46
		gunModel[13].setRotationPoint(25F, -29F, -3.5F);

		gunModel[14].addShapeBox(0F, 0F, 0F, 12, 18, 9, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, -2F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 4F, -2F, 0F); // grip1
		gunModel[14].setRotationPoint(-14F, -11F, -4.5F);

		gunModel[15].addBox(0F, 0F, 0F, 13, 2, 9, 0F); // grip2
		gunModel[15].setRotationPoint(-18F, 5F, -4.5F);
		gunModel[15].rotateAngleZ = -0.17453293F;

		gunModel[16].addShapeBox(0F, 0F, 0F, 12, 2, 10, 0F,2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // gripConnector
		gunModel[16].setRotationPoint(-14F, -13F, -5F);

		gunModel[17].addBox(0F, 0F, 0F, 7, 5, 10, 0F); // gripConnector2
		gunModel[17].setRotationPoint(-9F, -11F, -5F);

		gunModel[18].addBox(0F, 0F, 0F, 7, 4, 9, 0F); // ironSightBase1
		gunModel[18].setRotationPoint(-12F, -28F, -4.5F);

		gunModel[19].addBox(0F, 0F, 0F, 33, 2, 7, 0F); // rail
		gunModel[19].setRotationPoint(-5F, -27F, -3.5F);

		gunModel[20].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // railBlock
		gunModel[20].setRotationPoint(-5F, -29F, -3.5F);

		gunModel[21].addShapeBox(0F, 0F, 0F, 17, 7, 11, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F); // Box 53
		gunModel[21].setRotationPoint(8.5F, -13F, -5.5F);

		gunModel[22].addBox(0F, 0F, 0F, 8, 2, 1, 0F); // Box 54
		gunModel[22].setRotationPoint(-1F, -13F, 4.5F);

		gunModel[23].addBox(0F, 0F, 0F, 21, 2, 11, 0F); // Box 55
		gunModel[23].setRotationPoint(-12F, -23F, -5.5F);

		gunModel[24].addBox(0F, 0F, 0F, 16, 2, 1, 0F); // Box 56
		gunModel[24].setRotationPoint(9F, -23F, 4.5F);


		defaultScopeModel = new ModelRendererTurbo[9];
		defaultScopeModel[0] = new ModelRendererTurbo(this, 49, 3, textureX, textureY); // Box 42
		defaultScopeModel[1] = new ModelRendererTurbo(this, 49, 3, textureX, textureY); // Box 43
		defaultScopeModel[2] = new ModelRendererTurbo(this, 49, 11, textureX, textureY); // Box 44
		defaultScopeModel[3] = new ModelRendererTurbo(this, 56, 6, textureX, textureY); // Box 45
		defaultScopeModel[4] = new ModelRendererTurbo(this, 1, 5, textureX, textureY); // ironBolt1
		defaultScopeModel[5] = new ModelRendererTurbo(this, 1, 5, textureX, textureY); // ironBolt1-2
		defaultScopeModel[6] = new ModelRendererTurbo(this, 34, 17, textureX, textureY); // ironSight1
		defaultScopeModel[7] = new ModelRendererTurbo(this, 34, 17, textureX, textureY); // ironSight1-2
		defaultScopeModel[8] = new ModelRendererTurbo(this, 1, 15, textureX, textureY); // ironSightBase2

		defaultScopeModel[0].addBox(0F, 0F, 0F, 2, 6, 1, 0F); // Box 42
		defaultScopeModel[0].setRotationPoint(56F, -34F, 2F);

		defaultScopeModel[1].addBox(0F, 0F, 0F, 2, 6, 1, 0F); // Box 43
		defaultScopeModel[1].setRotationPoint(56F, -34F, -3F);

		defaultScopeModel[2].addBox(0F, 0F, 0F, 2, 8, 6, 0F); // Box 44
		defaultScopeModel[2].setRotationPoint(56F, -28F, -3F);

		defaultScopeModel[3].addBox(0F, 0F, 0F, 2, 3, 1, 0F); // Box 45
		defaultScopeModel[3].setRotationPoint(56F, -31F, -0.5F);

		defaultScopeModel[4].addBox(0F, 0F, 0F, 3, 3, 1, 0F); // ironBolt1
		defaultScopeModel[4].setRotationPoint(-9F, -32F, 4F);

		defaultScopeModel[5].addBox(0F, 0F, 0F, 3, 3, 1, 0F); // ironBolt1-2
		defaultScopeModel[5].setRotationPoint(-9F, -32F, -5F);

		defaultScopeModel[6].addBox(0F, 0F, 0F, 5, 6, 2, 0F); // ironSight1
		defaultScopeModel[6].setRotationPoint(-10F, -36F, 2F);

		defaultScopeModel[7].addBox(0F, 0F, 0F, 5, 6, 2, 0F); // ironSight1-2
		defaultScopeModel[7].setRotationPoint(-10F, -36F, -4F);

		defaultScopeModel[8].addBox(0F, 0F, 0F, 7, 2, 8, 0F); // ironSightBase2
		defaultScopeModel[8].setRotationPoint(-12F, -30F, -4F);


		defaultStockModel = new ModelRendererTurbo[7];
		defaultStockModel[0] = new ModelRendererTurbo(this, 80, 169, textureX, textureY); // stock6
		defaultStockModel[1] = new ModelRendererTurbo(this, 80, 145, textureX, textureY); // stock1
		defaultStockModel[2] = new ModelRendererTurbo(this, 165, 146, textureX, textureY); // stock2
		defaultStockModel[3] = new ModelRendererTurbo(this, 144, 137, textureX, textureY); // stock4
		defaultStockModel[4] = new ModelRendererTurbo(this, 97, 175, textureX, textureY); // stock5
		defaultStockModel[5] = new ModelRendererTurbo(this, 80, 136, textureX, textureY); // stock3
		defaultStockModel[6] = new ModelRendererTurbo(this, 80, 162, textureX, textureY); // stock7-2

		defaultStockModel[0].addBox(0F, 0F, 0F, 2, 10, 6, 0F); // stock6
		defaultStockModel[0].setRotationPoint(-18F, -23F, -3F);

		defaultStockModel[1].addShapeBox(0F, 0F, 0F, 22, 10, 6, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 6F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 6F, 0F); // stock1
		defaultStockModel[1].setRotationPoint(-40F, -22F, -3F);

		defaultStockModel[2].addShapeBox(0F, 0F, 0F, 6, 16, 6, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 6F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 6F, 0F); // stock2
		defaultStockModel[2].setRotationPoint(-46F, -22F, -3F);

		defaultStockModel[3].addBox(0F, 0F, 0F, 2, 23, 8, 0F); // stock4
		defaultStockModel[3].setRotationPoint(-48F, -22F, -4F);

		defaultStockModel[4].addShapeBox(0F, 0F, 0F, 2, 2, 8, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // stock5
		defaultStockModel[4].setRotationPoint(-48F, -24F, -4F);

		defaultStockModel[5].addShapeBox(0F, 0F, 0F, 28, 2, 6, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // stock3
		defaultStockModel[5].setRotationPoint(-46F, -24F, -3F);

		defaultStockModel[6].addShapeBox(0F, 0F, 0F, 28, 5, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F); // stock7-2
		defaultStockModel[6].setRotationPoint(-46F, -21F, 3F);


		defaultGripModel = new ModelRendererTurbo[9];
		defaultGripModel[0] = new ModelRendererTurbo(this, 80, 73, textureX, textureY); // foreRail2
		defaultGripModel[1] = new ModelRendererTurbo(this, 80, 94, textureX, textureY); // foreRail1
		defaultGripModel[2] = new ModelRendererTurbo(this, 80, 94, textureX, textureY); // foreRail1-2
		defaultGripModel[3] = new ModelRendererTurbo(this, 176, 117, textureX, textureY); // Box 47
		defaultGripModel[4] = new ModelRendererTurbo(this, 201, 78, textureX, textureY); // Box 48
		defaultGripModel[5] = new ModelRendererTurbo(this, 201, 98, textureX, textureY); // Box 50
		defaultGripModel[6] = new ModelRendererTurbo(this, 176, 68, textureX, textureY); // Box 51
		defaultGripModel[7] = new ModelRendererTurbo(this, 176, 96, textureX, textureY); // Box 52
		defaultGripModel[8] = new ModelRendererTurbo(this, 205, 127, textureX, textureY); // Box 54

		defaultGripModel[0].addBox(0F, 0F, 0F, 25, 10, 10, 0F); // foreRail2
		defaultGripModel[0].setRotationPoint(31F, -23F, -5F);

		defaultGripModel[1].addShapeBox(0F, 0F, 0F, 25, 2, 10, 0F,0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // foreRail1
		defaultGripModel[1].setRotationPoint(31F, -25F, -5F);

		defaultGripModel[2].addShapeBox(0F, 0F, 0F, 25, 2, 10, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // foreRail1-2
		defaultGripModel[2].setRotationPoint(31F, -13F, -5F);

		defaultGripModel[3].addBox(0F, 0F, 0F, 3, 6, 22, 0F); // Box 47
		defaultGripModel[3].setRotationPoint(51F, -13F, -5F);

		defaultGripModel[4].addBox(0F, 0F, 0F, 3, 16, 1, 0F); // Box 48
		defaultGripModel[4].setRotationPoint(51F, -35F, 16F);

		defaultGripModel[5].addBox(0F, 0F, 0F, 3, 6, 12, 0F); // Box 50
		defaultGripModel[5].setRotationPoint(51F, -19F, 5F);

		defaultGripModel[6].addBox(0F, 0F, 0F, 1, 16, 11, 0F); // Box 51
		defaultGripModel[6].setRotationPoint(52F, -35F, 5F);

		defaultGripModel[7].addBox(0F, 0F, 0F, 1, 9, 11, 0F); // Box 52
		defaultGripModel[7].setRotationPoint(52F, -35F, -6F);

		defaultGripModel[8].addBox(0F, 0F, 0F, 8, 5, 6, 0F); // Box 54
		defaultGripModel[8].setRotationPoint(43F, -13.5F, -3F);


		ammoModel = new ModelRendererTurbo[6];
		ammoModel[0] = new ModelRendererTurbo(this, 1, 105, textureX, textureY); // clip1
		ammoModel[1] = new ModelRendererTurbo(this, 1, 136, textureX, textureY); // clip2
		ammoModel[2] = new ModelRendererTurbo(this, 1, 148, textureX, textureY); // clip3-1
		ammoModel[3] = new ModelRendererTurbo(this, 1, 148, textureX, textureY); // clip3
		ammoModel[4] = new ModelRendererTurbo(this, 1, 159, textureX, textureY); // bullet
		ammoModel[5] = new ModelRendererTurbo(this, 34, 159, textureX, textureY); // bulletTip

		ammoModel[0].addBox(0F, 0F, 0F, 16, 22, 8, 0F); // clip1
		ammoModel[0].setRotationPoint(9F, -6F, -4F);
		ammoModel[0].rotateAngleZ = 0.08726646F;

		ammoModel[1].addBox(0F, 0F, 0F, 18, 2, 9, 0F); // clip2
		ammoModel[1].setRotationPoint(10F, 16F, -4.5F);
		ammoModel[1].rotateAngleZ = 0.08726646F;

		ammoModel[2].addBox(0F, 0F, 0F, 9, 1, 9, 0F); // clip3-1
		ammoModel[2].setRotationPoint(18F, 11F, -4.5F);
		ammoModel[2].rotateAngleZ = 0.08726646F;

		ammoModel[3].addBox(0F, 0F, 0F, 9, 1, 9, 0F); // clip3
		ammoModel[3].setRotationPoint(17F, 2F, -4.5F);
		ammoModel[3].rotateAngleZ = 0.08726646F;

		ammoModel[4].addShapeBox(0F, 0F, 0F, 10, 1, 6, 0F,0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // bullet
		ammoModel[4].setRotationPoint(10F, -7F, -3F);
		ammoModel[4].rotateAngleZ = 0.08726646F;

		ammoModel[5].addShapeBox(0F, 0F, 0F, 4, 1, 6, 0F,0F, 0F, -1.5F, 0F, -0.5F, -2.5F, 0F, -0.5F, -2.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // bulletTip
		ammoModel[5].setRotationPoint(20F, -7.8F, -3F);
		ammoModel[5].rotateAngleZ = 0.08726646F;


		slideModel = new ModelRendererTurbo[1];
		slideModel[0] = new ModelRendererTurbo(this, 66, 19, textureX, textureY); // slider

		slideModel[0].addBox(0F, 0F, 0F, 2, 2, 4, 0F); // slider
		slideModel[0].setRotationPoint(25F, -23F, -9F);



		barrelAttachPoint = new Vector3f(0F /16F, 0F /16F, 0F /16F);
		stockAttachPoint = new Vector3f(0F /16F, 0F /16F, 0F /16F);
		scopeAttachPoint = new Vector3f(0F /16F, 0F /16F, 0F /16F);
		gripAttachPoint = new Vector3f(0 /16F, 0F /16F, 0F /16F);

		gunSlideDistance = 0.8F;
		animationType = EnumAnimationType.BOTTOM_CLIP;


		translateAll(0F, 0F, 0F);


		flipAll();
	}
}