package com.flansmod.client.model.dwfirearms; //Path where the model is located

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelIronhoof extends ModelGun //Same as Filename
{
	int textureX = 512;
	int textureY = 256;

	public ModelIronhoof() //Same as Filename
	{
		gunModel = new ModelRendererTurbo[76];
		gunModel[0] = new ModelRendererTurbo(this, 157, 121, textureX, textureY); // body1
		gunModel[1] = new ModelRendererTurbo(this, 181, 105, textureX, textureY); // body6
		gunModel[2] = new ModelRendererTurbo(this, 1, 76, textureX, textureY); // foreRail1
		gunModel[3] = new ModelRendererTurbo(this, 191, 62, textureX, textureY); // body12
		gunModel[4] = new ModelRendererTurbo(this, 93, 9, textureX, textureY); // body17
		gunModel[5] = new ModelRendererTurbo(this, 112, 121, textureX, textureY); // Import GU,body4
		gunModel[6] = new ModelRendererTurbo(this, 112, 29, textureX, textureY); // Import GU,body9
		gunModel[7] = new ModelRendererTurbo(this, 112, 15, textureX, textureY); // railPart3
		gunModel[8] = new ModelRendererTurbo(this, 1, 2, textureX, textureY); // Import GU,tacRailEnd1
		gunModel[9] = new ModelRendererTurbo(this, 32, 10, textureX, textureY); // ironSightPart1
		gunModel[10] = new ModelRendererTurbo(this, 32, 1, textureX, textureY); // ironSightPart4
		gunModel[11] = new ModelRendererTurbo(this, 232, 68, textureX, textureY); // Box 39
		gunModel[12] = new ModelRendererTurbo(this, 112, 104, textureX, textureY); // Box 1
		gunModel[13] = new ModelRendererTurbo(this, 232, 68, textureX, textureY); // Box 2
		gunModel[14] = new ModelRendererTurbo(this, 68, 78, textureX, textureY); // Box 3
		gunModel[15] = new ModelRendererTurbo(this, 182, 89, textureX, textureY); // Box 11
		gunModel[16] = new ModelRendererTurbo(this, 169, 88, textureX, textureY); // Box 12
		gunModel[17] = new ModelRendererTurbo(this, 182, 89, textureX, textureY); // Box 13
		gunModel[18] = new ModelRendererTurbo(this, 1, 63, textureX, textureY); // Box 14
		gunModel[19] = new ModelRendererTurbo(this, 112, 53, textureX, textureY); // Box 16
		gunModel[20] = new ModelRendererTurbo(this, 153, 89, textureX, textureY); // Box 22
		gunModel[21] = new ModelRendererTurbo(this, 160, 88, textureX, textureY); // Box 23
		gunModel[22] = new ModelRendererTurbo(this, 153, 89, textureX, textureY); // Box 24
		gunModel[23] = new ModelRendererTurbo(this, 195, 42, textureX, textureY); // Box 26
		gunModel[24] = new ModelRendererTurbo(this, 112, 42, textureX, textureY); // Box 27
		gunModel[25] = new ModelRendererTurbo(this, 168, 63, textureX, textureY); // Box 29
		gunModel[26] = new ModelRendererTurbo(this, 149, 63, textureX, textureY); // Box 30
		gunModel[27] = new ModelRendererTurbo(this, 149, 63, textureX, textureY); // Box 31
		gunModel[28] = new ModelRendererTurbo(this, 149, 63, textureX, textureY); // Box 32
		gunModel[29] = new ModelRendererTurbo(this, 112, 63, textureX, textureY); // Box 33
		gunModel[30] = new ModelRendererTurbo(this, 112, 76, textureX, textureY); // Box 34
		gunModel[31] = new ModelRendererTurbo(this, 193, 88, textureX, textureY); // Box 84
		gunModel[32] = new ModelRendererTurbo(this, 193, 88, textureX, textureY); // Box 85
		gunModel[33] = new ModelRendererTurbo(this, 193, 88, textureX, textureY); // Box 86
		gunModel[34] = new ModelRendererTurbo(this, 279, 26, textureX, textureY); // Box 0
		gunModel[35] = new ModelRendererTurbo(this, 112, 1, textureX, textureY); // Box 104
		gunModel[36] = new ModelRendererTurbo(this, 112, 87, textureX, textureY); // Box 127
		gunModel[37] = new ModelRendererTurbo(this, 254, 122, textureX, textureY); // Box 135
		gunModel[38] = new ModelRendererTurbo(this, 252, 107, textureX, textureY); // Box 136
		gunModel[39] = new ModelRendererTurbo(this, 98, 6, textureX, textureY); // Box 137
		gunModel[40] = new ModelRendererTurbo(this, 93, 6, textureX, textureY); // Box 138
		gunModel[41] = new ModelRendererTurbo(this, 202, 123, textureX, textureY); // Box 154
		gunModel[42] = new ModelRendererTurbo(this, 277, 123, textureX, textureY); // Box 156
		gunModel[43] = new ModelRendererTurbo(this, 254, 122, textureX, textureY); // Box 157
		gunModel[44] = new ModelRendererTurbo(this, 277, 123, textureX, textureY); // Box 158
		gunModel[45] = new ModelRendererTurbo(this, 213, 3, textureX, textureY); // Box 159
		gunModel[46] = new ModelRendererTurbo(this, 273, 15, textureX, textureY); // Box 160
		gunModel[47] = new ModelRendererTurbo(this, 282, 4, textureX, textureY); // Box 162
		gunModel[48] = new ModelRendererTurbo(this, 64, 13, textureX, textureY); // Box 2
		gunModel[49] = new ModelRendererTurbo(this, 64, 9, textureX, textureY); // Box 3
		gunModel[50] = new ModelRendererTurbo(this, 64, 9, textureX, textureY); // Box 4
		gunModel[51] = new ModelRendererTurbo(this, 64, 13, textureX, textureY); // Box 5
		gunModel[52] = new ModelRendererTurbo(this, 64, 2, textureX, textureY); // Box 6
		gunModel[53] = new ModelRendererTurbo(this, 221, 79, textureX, textureY); // Box 0
		gunModel[54] = new ModelRendererTurbo(this, 210, 88, textureX, textureY); // Box 1
		gunModel[55] = new ModelRendererTurbo(this, 210, 81, textureX, textureY); // Box 2
		gunModel[56] = new ModelRendererTurbo(this, 225, 90, textureX, textureY); // Box 3
		gunModel[57] = new ModelRendererTurbo(this, 1, 40, textureX, textureY); // Box 35
		gunModel[58] = new ModelRendererTurbo(this, 1, 32, textureX, textureY); // Box 36
		gunModel[59] = new ModelRendererTurbo(this, 1, 48, textureX, textureY); // Box 37
		gunModel[60] = new ModelRendererTurbo(this, 72, 48, textureX, textureY); // Box 38
		gunModel[61] = new ModelRendererTurbo(this, 85, 51, textureX, textureY); // Box 39
		gunModel[62] = new ModelRendererTurbo(this, 236, 83, textureX, textureY); // Box 42
		gunModel[63] = new ModelRendererTurbo(this, 236, 90, textureX, textureY); // Box 43
		gunModel[64] = new ModelRendererTurbo(this, 62, 40, textureX, textureY); // Box 44
		gunModel[65] = new ModelRendererTurbo(this, 62, 32, textureX, textureY); // Box 45
		gunModel[66] = new ModelRendererTurbo(this, 59, 95, textureX, textureY); // Box 32
		gunModel[67] = new ModelRendererTurbo(this, 59, 109, textureX, textureY); // Box 33
		gunModel[68] = new ModelRendererTurbo(this, 1, 95, textureX, textureY); // Box 34
		gunModel[69] = new ModelRendererTurbo(this, 1, 107, textureX, textureY); // Box 35
		gunModel[70] = new ModelRendererTurbo(this, 1, 121, textureX, textureY); // Box 36
		gunModel[71] = new ModelRendererTurbo(this, 1, 137, textureX, textureY); // Box 38
		gunModel[72] = new ModelRendererTurbo(this, 38, 95, textureX, textureY); // Box 39
		gunModel[73] = new ModelRendererTurbo(this, 38, 107, textureX, textureY); // Box 40
		gunModel[74] = new ModelRendererTurbo(this, 38, 121, textureX, textureY); // Box 41
		gunModel[75] = new ModelRendererTurbo(this, 38, 137, textureX, textureY); // Box 42

		gunModel[0].addShapeBox(0F, 0F, 0F, 12, 3, 10, 0F, -2F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, -2F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body1
		gunModel[0].setRotationPoint(-7F, -25F, -5F);

		gunModel[1].addShapeBox(0F, 0F, 0F, 25, 4, 10, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body6
		gunModel[1].setRotationPoint(-7F, -20F, -5F);

		gunModel[2].addBox(0F, 0F, 0F, 23, 8, 10, 0F); // foreRail1
		gunModel[2].setRotationPoint(61F, -20F, -5F);

		gunModel[3].addShapeBox(0F, 0F, 0F, 12, 4, 8, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body12
		gunModel[3].setRotationPoint(16F, -20F, -4F);

		gunModel[4].addBox(0F, 0F, 0F, 8, 3, 1, 0F); // body17
		gunModel[4].setRotationPoint(0F, -15.5F, 4.5F);

		gunModel[5].addBox(0F, 0F, 0F, 12, 2, 10, 0F); // Import GU,body4
		gunModel[5].setRotationPoint(-7F, -22F, -5F);

		gunModel[6].addBox(0F, 0F, 0F, 75, 1, 10, 0F); // Import GU,body9
		gunModel[6].setRotationPoint(9F, -21F, -5F);

		gunModel[7].addBox(0F, 0F, 0F, 70, 3, 10, 0F); // railPart3
		gunModel[7].setRotationPoint(14F, -24F, -5F);

		gunModel[8].addBox(0F, 0F, 0F, 4, 7, 11, 0F); // Import GU,tacRailEnd1
		gunModel[8].setRotationPoint(5F, -27F, -5.5F);

		gunModel[9].addShapeBox(0F, 0F, 0F, 8, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F); // ironSightPart1
		gunModel[9].setRotationPoint(-3F, -27F, -4F);

		gunModel[10].addBox(0F, 0F, 0F, 6, 2, 6, 0F); // ironSightPart4
		gunModel[10].setRotationPoint(2F, -28.5F, -3F);

		gunModel[11].addShapeBox(0F, 0F, 0F, 17, 6, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F); // Box 39
		gunModel[11].setRotationPoint(64F, -19F, 5F);

		gunModel[12].addShapeBox(0F, 0F, 0F, 22, 6, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 1
		gunModel[12].setRotationPoint(-7F, -16F, -5F);

		gunModel[13].addShapeBox(0F, 0F, 0F, 17, 6, 1, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 2
		gunModel[13].setRotationPoint(64F, -19F, -6F);

		gunModel[14].addShapeBox(0F, 0F, 0F, 3, 6, 10, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F); // Box 3
		gunModel[14].setRotationPoint(34F, -16F, -5F);

		gunModel[15].addShapeBox(0F, 0F, 0F, 3, 7, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F); // Box 11
		gunModel[15].setRotationPoint(84F, -25F, 1.5F);

		gunModel[16].addBox(0F, 0F, 0F, 3, 7, 3, 0F); // Box 12
		gunModel[16].setRotationPoint(84F, -25F, -1.5F);

		gunModel[17].addShapeBox(0F, 0F, 0F, 3, 7, 2, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 13
		gunModel[17].setRotationPoint(84F, -25F, -3.5F);

		gunModel[18].addShapeBox(0F, 0F, 0F, 23, 2, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 14
		gunModel[18].setRotationPoint(61F, -12F, -5F);

		gunModel[19].addShapeBox(0F, 0F, 0F, 25, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 16
		gunModel[19].setRotationPoint(36F, -12F, -4F);

		gunModel[20].addShapeBox(0F, 0F, 0F, 1, 6, 2, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 22
		gunModel[20].setRotationPoint(83.5F, -17F, -3F);

		gunModel[21].addBox(0F, 0F, 0F, 1, 6, 2, 0F); // Box 23
		gunModel[21].setRotationPoint(83.5F, -17F, -1F);

		gunModel[22].addShapeBox(0F, 0F, 0F, 1, 6, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F); // Box 24
		gunModel[22].setRotationPoint(83.5F, -17F, 1F);

		gunModel[23].addShapeBox(0F, 0F, 0F, 33, 1, 8, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 26
		gunModel[23].setRotationPoint(28F, -20F, -4F);

		gunModel[24].addShapeBox(0F, 0F, 0F, 33, 2, 8, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 27
		gunModel[24].setRotationPoint(28F, -18F, -4F);

		gunModel[25].addShapeBox(0F, 0F, 0F, 3, 4, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F); // Box 29
		gunModel[25].setRotationPoint(58F, -16F, -4F);

		gunModel[26].addShapeBox(0F, 0F, 0F, 1, 4, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F); // Box 30
		gunModel[26].setRotationPoint(55F, -16F, -4F);

		gunModel[27].addShapeBox(0F, 0F, 0F, 1, 4, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F); // Box 31
		gunModel[27].setRotationPoint(52F, -16F, -4F);

		gunModel[28].addShapeBox(0F, 0F, 0F, 1, 4, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F); // Box 32
		gunModel[28].setRotationPoint(49F, -16F, -4F);

		gunModel[29].addShapeBox(0F, 0F, 0F, 9, 4, 8, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 33
		gunModel[29].setRotationPoint(36F, -16F, -4F);

		gunModel[30].addBox(0F, 0F, 0F, 35, 2, 7, 0F); // Box 34
		gunModel[30].setRotationPoint(26F, -20F, -3.5F);

		gunModel[31].addShapeBox(0F, 0F, 0F, 5, 7, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 84
		gunModel[31].setRotationPoint(9F, -26.5F, 1.5F);

		gunModel[32].addBox(0F, 0F, 0F, 5, 7, 3, 0F); // Box 85
		gunModel[32].setRotationPoint(9F, -26.5F, -1.5F);

		gunModel[33].addShapeBox(0F, 0F, 0F, 5, 7, 3, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 86
		gunModel[33].setRotationPoint(9F, -26.5F, -4.5F);

		gunModel[34].addShapeBox(0F, 0F, 0F, 1, 32, 40, 0F, 0F, -16F, 0F, 0F, -16F, 0F, 0F, -16F, -20F, 0F, -16F, -20F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -20F, 0F, 0F, -20F); // Box 0
		gunModel[34].setRotationPoint(6F, -52F, -5.5F);

		gunModel[35].addShapeBox(0F, 0F, 0F, 40, 3, 10, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F); // Box 104
		gunModel[35].setRotationPoint(14F, -27F, -5F);

		gunModel[36].addBox(0F, 0F, 0F, 13, 4, 7, 0F); // Box 127
		gunModel[36].setRotationPoint(45F, -16F, -3.5F);

		gunModel[37].addShapeBox(0F, 0F, 0F, 1, 2, 10, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, -1F, 0F, -1.5F, -1F, 0F, 0F, 0F); // Box 135
		gunModel[37].setRotationPoint(15F, -12F, -5F);

		gunModel[38].addBox(0F, 0F, 0F, 19, 3, 10, 0F); // Box 136
		gunModel[38].setRotationPoint(15F, -16F, -5F);

		gunModel[39].addBox(0F, 0F, 0F, 1, 1, 1, 0F); // Box 137
		gunModel[39].setRotationPoint(9F, -15.5F, 4.2F);

		gunModel[40].addBox(0F, 0F, 0F, 1, 1, 1, 0F); // Box 138
		gunModel[40].setRotationPoint(8.5F, -13.5F, 4.2F);

		gunModel[41].addShapeBox(0F, 0F, 0F, 17, 1, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 154
		gunModel[41].setRotationPoint(16F, -13F, -5F);

		gunModel[42].addShapeBox(0F, 0F, 0F, 1, 1, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 156
		gunModel[42].setRotationPoint(15F, -13F, -5F);

		gunModel[43].addShapeBox(0F, 0F, 0F, 1, 2, 10, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -1.5F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, -1F); // Box 157
		gunModel[43].setRotationPoint(33F, -12F, -5F);

		gunModel[44].addShapeBox(0F, 0F, 0F, 1, 1, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 158
		gunModel[44].setRotationPoint(33F, -13F, -5F);

		gunModel[45].addShapeBox(0F, 0F, 0F, 25, 2, 9, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0.5F, -0.5F, 2F, 0.5F, -0.5F, 2F, 0.5F, -0.5F, 0F, 0.5F, -0.5F); // Box 159
		gunModel[45].setRotationPoint(54F, -27F, -4.5F);

		gunModel[46].addShapeBox(0F, 0F, 0F, 5, 3, 10, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F); // Box 160
		gunModel[46].setRotationPoint(79F, -27F, -5F);

		gunModel[47].addShapeBox(0F, 0F, 0F, 27, 1, 9, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -2F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, -2F, 0F, 0.5F); // Box 162
		gunModel[47].setRotationPoint(54F, -25F, -4.5F);

		gunModel[48].addShapeBox(0F, 0F, 0F, 5, 1, 1, 0F, -2F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 2
		gunModel[48].setRotationPoint(79F, -31F, -2.5F);

		gunModel[49].addShapeBox(0F, 0F, 0F, 8, 2, 1, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 3
		gunModel[49].setRotationPoint(76F, -30F, -2.5F);

		gunModel[50].addShapeBox(0F, 0F, 0F, 8, 2, 1, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 4
		gunModel[50].setRotationPoint(76F, -30F, 1.5F);

		gunModel[51].addShapeBox(0F, 0F, 0F, 5, 1, 1, 0F, -2F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 5
		gunModel[51].setRotationPoint(79F, -31F, 1.5F);

		gunModel[52].addBox(0F, 0F, 0F, 9, 1, 5, 0F); // Box 6
		gunModel[52].setRotationPoint(75F, -28F, -2.5F);

		gunModel[53].addShapeBox(0F, 0F, 0F, 1, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 0
		gunModel[53].setRotationPoint(14F, -6F, -3F);

		gunModel[54].addBox(0F, 0F, 0F, 1, 4, 6, 0F); // Box 1
		gunModel[54].setRotationPoint(14F, -10F, -3F);

		gunModel[55].addShapeBox(0F, 0F, 0F, 1, 2, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F); // Box 2
		gunModel[55].setRotationPoint(14F, -4F, -2F);

		gunModel[56].addBox(0F, 0F, 0F, 1, 4, 4, 0F); // Box 3
		gunModel[56].setRotationPoint(13F, -2F, -2F);

		gunModel[57].addShapeBox(0F, 0F, 0F, 25, 1, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 35
		gunModel[57].setRotationPoint(39F, -4F, -2.5F);

		gunModel[58].addBox(0F, 0F, 0F, 25, 2, 5, 0F); // Box 36
		gunModel[58].setRotationPoint(39F, -6F, -2.5F);

		gunModel[59].addShapeBox(0F, 0F, 0F, 32, 5, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -5F, 0F, 0F, -5F, 0F, 0F, 0F, 0F, 0F); // Box 37
		gunModel[59].setRotationPoint(37F, -9F, -1.5F);

		gunModel[60].addShapeBox(0F, 0F, 0F, 3, 5, 3, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 38
		gunModel[60].setRotationPoint(35F, -11F, -1.5F);

		gunModel[61].addShapeBox(0F, 0F, 0F, 2, 2, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -1F, 0F); // Box 39
		gunModel[61].setRotationPoint(35F, -6F, -1.5F);

		gunModel[62].addShapeBox(0F, 0F, 0F, 16, 1, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 42
		gunModel[62].setRotationPoint(68F, -7F, -2.5F);

		gunModel[63].addBox(0F, 0F, 0F, 16, 3, 5, 0F); // Box 43
		gunModel[63].setRotationPoint(68F, -10F, -2.5F);

		gunModel[64].addShapeBox(0F, 0F, 0F, 4, 1, 5, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -3F, -1F, 0F, -3F, -1F, 0F, 0F, -1F); // Box 44
		gunModel[64].setRotationPoint(64F, -4F, -2.5F);

		gunModel[65].addShapeBox(0F, 0F, 0F, 4, 2, 5, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F); // Box 45
		gunModel[65].setRotationPoint(64F, -6F, -2.5F);

		gunModel[66].addShapeBox(0F, 0F, 0F, 12, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F); // Box 32
		gunModel[66].setRotationPoint(-7F, -10F, -4F);

		gunModel[67].addBox(0F, 0F, 0F, 6, 3, 8, 0F); // Box 33
		gunModel[67].setRotationPoint(5F, -10F, -4F);

		gunModel[68].addShapeBox(0F, 0F, 0F, 10, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 34
		gunModel[68].setRotationPoint(-4F, -7F, -4F);

		gunModel[69].addShapeBox(0F, 0F, 0F, 10, 5, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F); // Box 35
		gunModel[69].setRotationPoint(-4F, -4F, -4F);

		gunModel[70].addShapeBox(0F, 0F, 0F, 10, 7, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, -3F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 3F, -3F, 0F); // Box 36
		gunModel[70].setRotationPoint(-6F, 1F, -4F);

		gunModel[71].addShapeBox(0F, 0F, 0F, 10, 2, 8, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F); // Box 38
		gunModel[71].setRotationPoint(-9F, 8F, -4F);

		gunModel[72].addShapeBox(0F, 0F, 0F, 2, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F); // Box 39
		gunModel[72].setRotationPoint(6F, -7F, -4F);

		gunModel[73].addShapeBox(0F, 0F, 0F, 2, 5, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, -1.5F, -2F, 0F, -1.5F, 2F, 0F, 0F); // Box 40
		gunModel[73].setRotationPoint(6F, -4F, -4F);

		gunModel[74].addShapeBox(0F, 0F, 0F, 2, 7, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, -1.5F, -3F, 0F, -1.5F, 3F, 0F, 0F); // Box 41
		gunModel[74].setRotationPoint(4F, 1F, -4F);

		gunModel[75].addShapeBox(0F, 0F, 0F, 2, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F); // Box 42
		gunModel[75].setRotationPoint(1F, 8F, -4F);


		defaultStockModel = new ModelRendererTurbo[12];
		defaultStockModel[0] = new ModelRendererTurbo(this, 1, 165, textureX, textureY); // Box 8
		defaultStockModel[1] = new ModelRendererTurbo(this, 1, 204, textureX, textureY); // Box 15
		defaultStockModel[2] = new ModelRendererTurbo(this, 1, 183, textureX, textureY); // Box 16
		defaultStockModel[3] = new ModelRendererTurbo(this, 1, 177, textureX, textureY); // Box 111
		defaultStockModel[4] = new ModelRendererTurbo(this, 1, 177, textureX, textureY); // Box 112
		defaultStockModel[5] = new ModelRendererTurbo(this, 1, 177, textureX, textureY); // Box 128
		defaultStockModel[6] = new ModelRendererTurbo(this, 1, 165, textureX, textureY); // Box 129
		defaultStockModel[7] = new ModelRendererTurbo(this, 1, 177, textureX, textureY); // Box 130
		defaultStockModel[8] = new ModelRendererTurbo(this, 1, 204, textureX, textureY); // Box 131
		defaultStockModel[9] = new ModelRendererTurbo(this, 36, 187, textureX, textureY); // Box 132
		defaultStockModel[10] = new ModelRendererTurbo(this, 36, 205, textureX, textureY); // Box 133
		defaultStockModel[11] = new ModelRendererTurbo(this, 67, 205, textureX, textureY); // Box 134

		defaultStockModel[0].addBox(0F, 0F, 0F, 34, 8, 3, 0F); // Box 8
		defaultStockModel[0].setRotationPoint(-41F, -20F, -4.5F);

		defaultStockModel[1].addShapeBox(0F, 0F, 0F, 7, 2, 10, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 15
		defaultStockModel[1].setRotationPoint(-48F, -23F, -5F);

		defaultStockModel[2].addShapeBox(0F, 0F, 0F, 7, 10, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 16
		defaultStockModel[2].setRotationPoint(-48F, -21F, -5F);

		defaultStockModel[3].addShapeBox(0F, 0F, 0F, 34, 2, 3, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 111
		defaultStockModel[3].setRotationPoint(-41F, -22F, -4.5F);

		defaultStockModel[4].addShapeBox(0F, 0F, 0F, 34, 2, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 112
		defaultStockModel[4].setRotationPoint(-41F, -12F, -4.5F);

		defaultStockModel[5].addShapeBox(0F, 0F, 0F, 34, 2, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 128
		defaultStockModel[5].setRotationPoint(-41F, -12F, 1.5F);

		defaultStockModel[6].addBox(0F, 0F, 0F, 34, 8, 3, 0F); // Box 129
		defaultStockModel[6].setRotationPoint(-41F, -20F, 1.5F);

		defaultStockModel[7].addShapeBox(0F, 0F, 0F, 34, 2, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 130
		defaultStockModel[7].setRotationPoint(-41F, -22F, 1.5F);

		defaultStockModel[8].addShapeBox(0F, 0F, 0F, 7, 2, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 131
		defaultStockModel[8].setRotationPoint(-48F, -11F, -5F);

		defaultStockModel[9].addShapeBox(0F, 0F, 0F, 7, 8, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 132
		defaultStockModel[9].setRotationPoint(-48F, -9F, -4F);

		defaultStockModel[10].addShapeBox(0F, 0F, 0F, 7, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, -3F, 0F, -1F, -3F, 0F, -1F, 0F, 0F, -1F); // Box 133
		defaultStockModel[10].setRotationPoint(-48F, -1F, -4F);

		defaultStockModel[11].addShapeBox(0F, 0F, 0F, 4, 6, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 0F, 0F); // Box 134
		defaultStockModel[11].setRotationPoint(-41F, -10F, -2.5F);


		ammoModel = new ModelRendererTurbo[12];
		ammoModel[0] = new ModelRendererTurbo(this, 185, 137, textureX, textureY); // Box 141
		ammoModel[1] = new ModelRendererTurbo(this, 184, 172, textureX, textureY); // Box 142
		ammoModel[2] = new ModelRendererTurbo(this, 184, 195, textureX, textureY); // Box 143
		ammoModel[3] = new ModelRendererTurbo(this, 236, 138, textureX, textureY); // Box 146
		ammoModel[4] = new ModelRendererTurbo(this, 236, 172, textureX, textureY); // Box 147
		ammoModel[5] = new ModelRendererTurbo(this, 236, 194, textureX, textureY); // Box 148
		ammoModel[6] = new ModelRendererTurbo(this, 293, 172, textureX, textureY); // Box 149
		ammoModel[7] = new ModelRendererTurbo(this, 293, 149, textureX, textureY); // Box 150
		ammoModel[8] = new ModelRendererTurbo(this, 112, 137, textureX, textureY); // Box 151
		ammoModel[9] = new ModelRendererTurbo(this, 112, 172, textureX, textureY); // Box 152
		ammoModel[10] = new ModelRendererTurbo(this, 112, 195, textureX, textureY); // Box 153
		ammoModel[11] = new ModelRendererTurbo(this, 293, 186, textureX, textureY); // Box 0

		ammoModel[0].addBox(0F, 0F, 0F, 1, 10, 24, 0F); // Box 141
		ammoModel[0].setRotationPoint(15.5F, -5.5F, -12F);

		ammoModel[1].addShapeBox(0F, 0F, 0F, 1, 4, 18, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 142
		ammoModel[1].setRotationPoint(15.5F, 4.5F, -9F);

		ammoModel[2].addShapeBox(0F, 0F, 0F, 1, 3, 10, 0F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 143
		ammoModel[2].setRotationPoint(15.5F, 8.5F, -5F);

		ammoModel[3].addBox(0F, 0F, 0F, 5, 10, 23, 0F); // Box 146
		ammoModel[3].setRotationPoint(16.5F, -6F, -11.5F);

		ammoModel[4].addShapeBox(0F, 0F, 0F, 5, 4, 17, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 147
		ammoModel[4].setRotationPoint(16.5F, 4F, -8.5F);

		ammoModel[5].addShapeBox(0F, 0F, 0F, 5, 3, 9, 0F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 148
		ammoModel[5].setRotationPoint(16.5F, 8F, -4.5F);

		ammoModel[6].addShapeBox(0F, 0F, 0F, 18, 3, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, 4F); // Box 149
		ammoModel[6].setRotationPoint(15.5F, -12.5F, -5F);

		ammoModel[7].addShapeBox(0F, 0F, 0F, 18, 4, 18, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 3F); // Box 150
		ammoModel[7].setRotationPoint(15.5F, -9.5F, -9F);

		ammoModel[8].addBox(0F, 0F, 0F, 12, 10, 24, 0F); // Box 151
		ammoModel[8].setRotationPoint(21.5F, -5.5F, -12F);

		ammoModel[9].addShapeBox(0F, 0F, 0F, 12, 4, 18, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 152
		ammoModel[9].setRotationPoint(21.5F, 4.5F, -9F);

		ammoModel[10].addShapeBox(0F, 0F, 0F, 12, 3, 10, 0F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 153
		ammoModel[10].setRotationPoint(21.5F, 8.5F, -5F);

		ammoModel[11].addShapeBox(0F, 0F, 0F, 16, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 0
		ammoModel[11].setRotationPoint(16.5F, -14.5F, -3F);

		stockAttachPoint = new Vector3f(-7F /16F, 16F /16F, 0F /16F);

		gunSlideDistance = 0.8F;
		animationType = EnumAnimationType.BOTTOM_CLIP;

		translateAll(0F, 0F, 0F);

		flipAll();
	}
}