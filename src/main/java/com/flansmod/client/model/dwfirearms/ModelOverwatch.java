package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelOverwatch extends ModelGun
{
	int textureX = 512;
	int textureY = 256;

	public ModelOverwatch()
	{
		gunModel = new ModelRendererTurbo[47];
		gunModel[0] = new ModelRendererTurbo(this, 56, 61, textureX, textureY); // Import barrelFront
		gunModel[1] = new ModelRendererTurbo(this, 56, 61, textureX, textureY); // Import barrelFront2
		gunModel[2] = new ModelRendererTurbo(this, 104, 61, textureX, textureY); // Import barrelFront3
		gunModel[3] = new ModelRendererTurbo(this, 104, 61, textureX, textureY); // Import barrelFront3-2
		gunModel[4] = new ModelRendererTurbo(this, 81, 61, textureX, textureY); // Import barrelFront4
		gunModel[5] = new ModelRendererTurbo(this, 56, 61, textureX, textureY); // Import barrelFront5
		gunModel[6] = new ModelRendererTurbo(this, 102, 73, textureX, textureY); // Import barrelFront6
		gunModel[7] = new ModelRendererTurbo(this, 56, 28, textureX, textureY); // Import barrelRail
		gunModel[8] = new ModelRendererTurbo(this, 112, 46, textureX, textureY); // Import body1
		gunModel[9] = new ModelRendererTurbo(this, 56, 82, textureX, textureY); // Import body2
		gunModel[10] = new ModelRendererTurbo(this, 56, 71, textureX, textureY); // Import body3
		gunModel[11] = new ModelRendererTurbo(this, 83, 98, textureX, textureY); // Import body4
		gunModel[12] = new ModelRendererTurbo(this, 83, 98, textureX, textureY); // Import body4-2
		gunModel[13] = new ModelRendererTurbo(this, 83, 91, textureX, textureY); // Import body5
		gunModel[14] = new ModelRendererTurbo(this, 83, 91, textureX, textureY); // Import body5-2
		gunModel[15] = new ModelRendererTurbo(this, 56, 45, textureX, textureY); // Import body6
		gunModel[16] = new ModelRendererTurbo(this, 56, 53, textureX, textureY); // Import body7
		gunModel[17] = new ModelRendererTurbo(this, 83, 81, textureX, textureY); // Import body8
		gunModel[18] = new ModelRendererTurbo(this, 79, 71, textureX, textureY); // Import body9
		gunModel[19] = new ModelRendererTurbo(this, 118, 32, textureX, textureY); // Import brassPart1
		gunModel[20] = new ModelRendererTurbo(this, 118, 32, textureX, textureY); // Import brassPart2
		gunModel[21] = new ModelRendererTurbo(this, 56, 32, textureX, textureY); // Import brassPipe1
		gunModel[22] = new ModelRendererTurbo(this, 56, 32, textureX, textureY); // Import brassPipe1-2
		gunModel[23] = new ModelRendererTurbo(this, 56, 32, textureX, textureY); // Import brassPipe1-3
		gunModel[24] = new ModelRendererTurbo(this, 56, 32, textureX, textureY); // Import brassPipe1-4
		gunModel[25] = new ModelRendererTurbo(this, 1, 88, textureX, textureY); // Import canister1
		gunModel[26] = new ModelRendererTurbo(this, 1, 61, textureX, textureY); // Import gasCanister2
		gunModel[27] = new ModelRendererTurbo(this, 1, 70, textureX, textureY); // Import gasCanister3
		gunModel[28] = new ModelRendererTurbo(this, 1, 79, textureX, textureY); // Import gasCanister4
		gunModel[29] = new ModelRendererTurbo(this, 56, 36, textureX, textureY); // Import crane1
		gunModel[30] = new ModelRendererTurbo(this, 56, 36, textureX, textureY); // Import crane1-2
		gunModel[31] = new ModelRendererTurbo(this, 56, 36, textureX, textureY); // Import crane1-3
		gunModel[32] = new ModelRendererTurbo(this, 73, 71, textureX, textureY); // Import hammer1
		gunModel[33] = new ModelRendererTurbo(this, 103, 36, textureX, textureY); // Import hammer2
		gunModel[34] = new ModelRendererTurbo(this, 36, 36, textureX, textureY); // Import ironSight1
		gunModel[35] = new ModelRendererTurbo(this, 36, 36, textureX, textureY); // Import ironSight1-2
		gunModel[36] = new ModelRendererTurbo(this, 36, 32, textureX, textureY); // Import ironSight2
		gunModel[37] = new ModelRendererTurbo(this, 56, 19, textureX, textureY); // Import mainBarrelBottom
		gunModel[38] = new ModelRendererTurbo(this, 56, 10, textureX, textureY); // Import mainBarrelMiddle
		gunModel[39] = new ModelRendererTurbo(this, 56, 1, textureX, textureY); // Import mainBarrelTop
		gunModel[40] = new ModelRendererTurbo(this, 32, 61, textureX, textureY); // Import meter1
		gunModel[41] = new ModelRendererTurbo(this, 32, 71, textureX, textureY); // Import meter1-2
		gunModel[42] = new ModelRendererTurbo(this, 32, 81, textureX, textureY); // Import meter2
		gunModel[43] = new ModelRendererTurbo(this, 1, 97, textureX, textureY); // Import pipe1
		gunModel[44] = new ModelRendererTurbo(this, 1, 97, textureX, textureY); // Import pipe1-2
		gunModel[45] = new ModelRendererTurbo(this, 23, 88, textureX, textureY); // Import pipe2
		gunModel[46] = new ModelRendererTurbo(this, 12, 88, textureX, textureY); // Import pipe3

		gunModel[0].addShapeBox(0F, 0F, 0F, 5, 2, 7, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import barrelFront
		gunModel[0].setRotationPoint(17F, -24F, -3.5F);

		gunModel[1].addBox(0F, 0F, 0F, 5, 2, 7, 0F); // Import barrelFront2
		gunModel[1].setRotationPoint(17F, -22F, -3.5F);

		gunModel[2].addShapeBox(0F, 0F, 0F, 4, 1, 7, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F); // Import barrelFront3
		gunModel[2].setRotationPoint(18F, -19.5F, -3.5F);

		gunModel[3].addShapeBox(0F, 0F, 0F, 4, 1, 7, 0F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F); // Import barrelFront3-2
		gunModel[3].setRotationPoint(18F, -17.5F, -3.5F);

		gunModel[4].addBox(0F, 0F, 0F, 4, 2, 7, 0F); // Import barrelFront4
		gunModel[4].setRotationPoint(18F, -16F, -3.5F);

		gunModel[5].addShapeBox(0F, 0F, 0F, 5, 2, 7, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Import barrelFront5
		gunModel[5].setRotationPoint(17F, -13.5F, -3.5F);

		gunModel[6].addShapeBox(0F, 0F, 0F, 5, 1, 6, 0F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F); // Import barrelFront6
		gunModel[6].setRotationPoint(17F, -12.5F, -3F);

		gunModel[7].addBox(0F, 0F, 0F, 35, 1, 2, 0F); // Import barrelRail
		gunModel[7].setRotationPoint(22F, -25F, -1F);

		gunModel[8].addBox(0F, 0F, 0F, 1, 6, 8, 0F); // Import body1
		gunModel[8].setRotationPoint(17F, -20F, -4F);

		gunModel[9].addBox(0F, 0F, 0F, 7, 15, 6, 0F); // Import body2
		gunModel[9].setRotationPoint(-2F, -23F, -3F);

		gunModel[10].addBox(0F, 0F, 0F, 5, 4, 6, 0F); // Import body3
		gunModel[10].setRotationPoint(-7F, -16F, -3F);

		gunModel[11].addShapeBox(0F, 0F, 0F, 2, 3, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F); // Import body4
		gunModel[11].setRotationPoint(-4F, -19F, 1F);

		gunModel[12].addShapeBox(0F, 0F, 0F, 2, 3, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F); // Import body4-2
		gunModel[12].setRotationPoint(-4F, -19F, -3F);

		gunModel[13].addShapeBox(0F, 0F, 0F, 1, 4, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F); // Import body5
		gunModel[13].setRotationPoint(-3F, -23F, 1F);

		gunModel[14].addShapeBox(0F, 0F, 0F, 1, 4, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F); // Import body5-2
		gunModel[14].setRotationPoint(-3F, -23F, -3F);

		gunModel[15].addShapeBox(0F, 0F, 0F, 25, 1, 6, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import body6
		gunModel[15].setRotationPoint(-3F, -25F, -3F);

		gunModel[16].addBox(0F, 0F, 0F, 20, 1, 6, 0F); // Import body7
		gunModel[16].setRotationPoint(-3F, -24F, -3F);

		gunModel[17].addBox(0F, 0F, 0F, 12, 3, 6, 0F); // Import body8
		gunModel[17].setRotationPoint(5F, -11F, -3F);

		gunModel[18].addShapeBox(0F, 0F, 0F, 5, 3, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F); // Import body9
		gunModel[18].setRotationPoint(17F, -11F, -3F);

		gunModel[19].addBox(0F, 0F, 0F, 3, 2, 7, 0F); // Import brassPart1
		gunModel[19].setRotationPoint(26F, -21F, -3.5F);

		gunModel[20].addBox(0F, 0F, 0F, 3, 2, 7, 0F); // Import brassPart2
		gunModel[20].setRotationPoint(39F, -21F, -3.5F);

		gunModel[21].addShapeBox(0F, 0F, 0F, 30, 1, 2, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import brassPipe1
		gunModel[21].setRotationPoint(18F, -19F, -4F);

		gunModel[22].addShapeBox(0F, 0F, 0F, 30, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Import brassPipe1-2
		gunModel[22].setRotationPoint(18F, -18F, -4F);

		gunModel[23].addShapeBox(0F, 0F, 0F, 30, 1, 2, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import brassPipe1-3
		gunModel[23].setRotationPoint(18F, -19F, 2F);

		gunModel[24].addShapeBox(0F, 0F, 0F, 30, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Import brassPipe1-4
		gunModel[24].setRotationPoint(18F, -18F, 2F);

		gunModel[25].addBox(0F, 0F, 0F, 3, 6, 2, 0F); // Import canister1
		gunModel[25].setRotationPoint(-2F, -25F, -4.5F);

		gunModel[26].addShapeBox(0F, 0F, 0F, 9, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Import gasCanister2
		gunModel[26].setRotationPoint(-5F, -21F, -9F);

		gunModel[27].addBox(0F, 0F, 0F, 9, 2, 6, 0F); // Import gasCanister3
		gunModel[27].setRotationPoint(-5F, -23F, -9F);

		gunModel[28].addShapeBox(0F, 0F, 0F, 9, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import gasCanister4
		gunModel[28].setRotationPoint(-5F, -25F, -9F);

		gunModel[29].addShapeBox(0F, 0F, 0F, 10, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import crane1
		gunModel[29].setRotationPoint(22F, -18F, -3F);

		gunModel[30].addBox(0F, 0F, 0F, 10, 2, 6, 0F); // Import crane1-2
		gunModel[30].setRotationPoint(22F, -16F, -3F);

		gunModel[31].addShapeBox(0F, 0F, 0F, 10, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Import crane1-3
		gunModel[31].setRotationPoint(22F, -14F, -3F);

		gunModel[32].addBox(0F, 0F, 0F, 2, 3, 2, 0F); // Import hammer1
		gunModel[32].setRotationPoint(-4F, -19F, -1F);

		gunModel[33].addShapeBox(0F, 0F, 0F, 5, 3, 2, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F, -3F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, -3F, 0F); // Import hammer2
		gunModel[33].setRotationPoint(-7F, -20F, -1F);

		gunModel[34].addBox(0F, 0F, 0F, 4, 2, 2, 0F); // Import ironSight1
		gunModel[34].setRotationPoint(-2.9F, -26F, 0.5F);

		gunModel[35].addBox(0F, 0F, 0F, 4, 2, 2, 0F); // Import ironSight1-2
		gunModel[35].setRotationPoint(-2.9F, -26F, -2.5F);

		gunModel[36].addShapeBox(0F, 0F, 0F, 4, 2, 1, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import ironSight2
		gunModel[36].setRotationPoint(52F, -27F, -0.5F);

		gunModel[37].addShapeBox(0F, 0F, 0F, 35, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Import mainBarrelBottom
		gunModel[37].setRotationPoint(22F, -20F, -3F);

		gunModel[38].addBox(0F, 0F, 0F, 35, 2, 6, 0F); // Import mainBarrelMiddle
		gunModel[38].setRotationPoint(22F, -22F, -3F);

		gunModel[39].addShapeBox(0F, 0F, 0F, 35, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import mainBarrelTop
		gunModel[39].setRotationPoint(22F, -24F, -3F);

		gunModel[40].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Import meter1
		gunModel[40].setRotationPoint(0F, -27F, -10F);

		gunModel[41].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import meter1-2
		gunModel[41].setRotationPoint(0F, -32F, -10F);

		gunModel[42].addBox(0F, 0F, 0F, 2, 3, 7, 0F); // Import meter2
		gunModel[42].setRotationPoint(0F, -30F, -10F);

		gunModel[43].addShapeBox(0F, 0F, 0F, 15, 1, 2, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import pipe1
		gunModel[43].setRotationPoint(3.5F, -23F, -7F);

		gunModel[44].addShapeBox(0F, 0F, 0F, 15, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Import pipe1-2
		gunModel[44].setRotationPoint(3.5F, -22F, -7F);

		gunModel[45].addBox(0F, 0F, 0F, 2, 2, 4, 0F); // Import pipe2
		gunModel[45].setRotationPoint(18.5F, -23F, -7F);

		gunModel[46].addBox(0F, 0F, 0F, 4, 4, 1, 0F); // Import pipe3
		gunModel[46].setRotationPoint(17.5F, -24F, -4F);


		defaultScopeModel = new ModelRendererTurbo[41];
		defaultScopeModel[0] = new ModelRendererTurbo(this, 163, 1, textureX, textureY); // Import scopeBase2-2
		defaultScopeModel[1] = new ModelRendererTurbo(this, 169, 8, textureX, textureY); // Import brassScope1
		defaultScopeModel[2] = new ModelRendererTurbo(this, 169, 8, textureX, textureY); // Import brassScope1-2
		defaultScopeModel[3] = new ModelRendererTurbo(this, 169, 20, textureX, textureY); // Import brassScope2
		defaultScopeModel[4] = new ModelRendererTurbo(this, 169, 20, textureX, textureY); // Import brassScope2-2
		defaultScopeModel[5] = new ModelRendererTurbo(this, 169, 20, textureX, textureY); // Import brassScope2-3
		defaultScopeModel[6] = new ModelRendererTurbo(this, 169, 20, textureX, textureY); // Import brassScope2-4
		defaultScopeModel[7] = new ModelRendererTurbo(this, 169, 14, textureX, textureY); // Import brassScope3
		defaultScopeModel[8] = new ModelRendererTurbo(this, 169, 14, textureX, textureY); // Import brassScope3-2
		defaultScopeModel[9] = new ModelRendererTurbo(this, 144, 44, textureX, textureY); // Import knob1
		defaultScopeModel[10] = new ModelRendererTurbo(this, 144, 44, textureX, textureY); // Import knob2
		defaultScopeModel[11] = new ModelRendererTurbo(this, 144, 44, textureX, textureY); // Import knob3
		defaultScopeModel[12] = new ModelRendererTurbo(this, 144, 1, textureX, textureY); // Import scopeBase1
		defaultScopeModel[13] = new ModelRendererTurbo(this, 163, 1, textureX, textureY); // Import scopeBase2
		defaultScopeModel[14] = new ModelRendererTurbo(this, 169, 26, textureX, textureY); // Import scopeFront1
		defaultScopeModel[15] = new ModelRendererTurbo(this, 169, 26, textureX, textureY); // Import scopeFront1-2
		defaultScopeModel[16] = new ModelRendererTurbo(this, 169, 32, textureX, textureY); // Import scopeFront2
		defaultScopeModel[17] = new ModelRendererTurbo(this, 169, 32, textureX, textureY); // Import scopeFront2-2
		defaultScopeModel[18] = new ModelRendererTurbo(this, 169, 38, textureX, textureY); // Import scopeFront3
		defaultScopeModel[19] = new ModelRendererTurbo(this, 169, 38, textureX, textureY); // Import scopeFront3-2
		defaultScopeModel[20] = new ModelRendererTurbo(this, 169, 38, textureX, textureY); // Import scopeFront3-3
		defaultScopeModel[21] = new ModelRendererTurbo(this, 169, 38, textureX, textureY); // Import scopeFront3-4
		defaultScopeModel[22] = new ModelRendererTurbo(this, 144, 8, textureX, textureY); // Import scopeMain1
		defaultScopeModel[23] = new ModelRendererTurbo(this, 144, 8, textureX, textureY); // Import scopeMain1-2
		defaultScopeModel[24] = new ModelRendererTurbo(this, 144, 14, textureX, textureY); // Import scopeMain2
		defaultScopeModel[25] = new ModelRendererTurbo(this, 144, 14, textureX, textureY); // Import scopeMain2-2
		defaultScopeModel[26] = new ModelRendererTurbo(this, 144, 20, textureX, textureY); // Import scopeMain3
		defaultScopeModel[27] = new ModelRendererTurbo(this, 144, 20, textureX, textureY); // Import scopeMain3-2
		defaultScopeModel[28] = new ModelRendererTurbo(this, 144, 20, textureX, textureY); // Import scopeMain3-3
		defaultScopeModel[29] = new ModelRendererTurbo(this, 144, 20, textureX, textureY); // Import scopeMain3-4
		defaultScopeModel[30] = new ModelRendererTurbo(this, 144, 26, textureX, textureY); // Import scopeMain4
		defaultScopeModel[31] = new ModelRendererTurbo(this, 144, 26, textureX, textureY); // Import scopeMain4-2
		defaultScopeModel[32] = new ModelRendererTurbo(this, 144, 32, textureX, textureY); // Import scopeMain5
		defaultScopeModel[33] = new ModelRendererTurbo(this, 144, 32, textureX, textureY); // Import scopeMain5-2
		defaultScopeModel[34] = new ModelRendererTurbo(this, 144, 38, textureX, textureY); // Import scopeMain6
		defaultScopeModel[35] = new ModelRendererTurbo(this, 144, 38, textureX, textureY); // Import scopeMain6-2
		defaultScopeModel[36] = new ModelRendererTurbo(this, 144, 38, textureX, textureY); // Import scopeMain6-3
		defaultScopeModel[37] = new ModelRendererTurbo(this, 144, 38, textureX, textureY); // Import scopeMain6-4
		defaultScopeModel[38] = new ModelRendererTurbo(this, 184, 8, textureX, textureY); // Import scopeSight1
		defaultScopeModel[39] = new ModelRendererTurbo(this, 184, 8, textureX, textureY); // Import scopeSight1-2
		defaultScopeModel[40] = new ModelRendererTurbo(this, 184, 13, textureX, textureY); // Import scopeSight2-1

		defaultScopeModel[0].addBox(0F, 0F, 0F, 6, 2, 1, 0F); // Import scopeBase2-2
		defaultScopeModel[0].setRotationPoint(11F, -27F, -3F);

		defaultScopeModel[1].addShapeBox(0F, 0F, 0F, 3, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F); // Import brassScope1
		defaultScopeModel[1].setRotationPoint(4F, -37F, -2F);

		defaultScopeModel[2].addShapeBox(0F, 0F, 0F, 3, 1, 4, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import brassScope1-2
		defaultScopeModel[2].setRotationPoint(4F, -28F, -2F);

		defaultScopeModel[3].addShapeBox(0F, 0F, 0F, 3, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F); // Import brassScope2
		defaultScopeModel[3].setRotationPoint(4F, -37F, -2.2F);
		defaultScopeModel[3].rotateAngleX = -0.78539816F;

		defaultScopeModel[4].addShapeBox(0F, 0F, 0F, 3, 4, 1, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import brassScope2-2
		defaultScopeModel[4].setRotationPoint(4F, -36.3F, 1.3F);
		defaultScopeModel[4].rotateAngleX = 0.78539816F;

		defaultScopeModel[5].addShapeBox(0F, 0F, 0F, 3, 4, 1, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import brassScope2-3
		defaultScopeModel[5].setRotationPoint(4F, -30.7F, 4.3F);
		defaultScopeModel[5].rotateAngleX = -0.78539816F;

		defaultScopeModel[6].addShapeBox(0F, 0F, 0F, 3, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F); // Import brassScope2-4
		defaultScopeModel[6].setRotationPoint(4F, -29.8F, -4.7F);
		defaultScopeModel[6].rotateAngleX = 0.78539816F;

		defaultScopeModel[7].addShapeBox(0F, 0F, 0F, 6, 4, 1, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import brassScope3
		defaultScopeModel[7].setRotationPoint(4F, -34F, 4F);

		defaultScopeModel[8].addShapeBox(0F, 0F, 0F, 6, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F); // Import brassScope3-2
		defaultScopeModel[8].setRotationPoint(4F, -34F, -5F);

		defaultScopeModel[9].addShapeBox(0F, 0F, 0F, 2, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F); // Import knob1
		defaultScopeModel[9].setRotationPoint(11F, -39F, -3F);

		defaultScopeModel[10].addBox(0F, 0F, 0F, 2, 2, 6, 0F); // Import knob2
		defaultScopeModel[10].setRotationPoint(13F, -39F, -3F);

		defaultScopeModel[11].addShapeBox(0F, 0F, 0F, 2, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F); // Import knob3
		defaultScopeModel[11].setRotationPoint(15F, -39F, -3F);

		defaultScopeModel[12].addBox(0F, 0F, 0F, 7, 2, 4, 0F); // Import scopeBase1
		defaultScopeModel[12].setRotationPoint(10.5F, -27F, -2F);

		defaultScopeModel[13].addBox(0F, 0F, 0F, 6, 2, 1, 0F); // Import scopeBase2
		defaultScopeModel[13].setRotationPoint(11F, -27F, 2F);

		defaultScopeModel[14].addShapeBox(0F, 0F, 0F, 2, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F); // Import scopeFront1
		defaultScopeModel[14].setRotationPoint(26F, -38F, -2F);

		defaultScopeModel[15].addShapeBox(0F, 0F, 0F, 2, 1, 4, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import scopeFront1-2
		defaultScopeModel[15].setRotationPoint(26F, -27.2F, -2F);

		defaultScopeModel[16].addShapeBox(0F, 0F, 0F, 2, 4, 1, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import scopeFront2
		defaultScopeModel[16].setRotationPoint(26F, -34F, 4.85F);

		defaultScopeModel[17].addShapeBox(0F, 0F, 0F, 2, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F); // Import scopeFront2-2
		defaultScopeModel[17].setRotationPoint(26F, -34F, -5.8F);

		defaultScopeModel[18].addShapeBox(0F, 0F, 0F, 2, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F); // Import scopeFront3
		defaultScopeModel[18].setRotationPoint(26F, -37F, -3F);
		defaultScopeModel[18].rotateAngleX = -0.78539816F;

		defaultScopeModel[19].addShapeBox(0F, 0F, 0F, 2, 4, 1, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import scopeFront3-2
		defaultScopeModel[19].setRotationPoint(26F, -36.3F, 2.3F);
		defaultScopeModel[19].rotateAngleX = 0.78539816F;

		defaultScopeModel[20].addShapeBox(0F, 0F, 0F, 2, 4, 1, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import scopeFront3-3
		defaultScopeModel[20].setRotationPoint(26F, -29.7F, 4.15F);
		defaultScopeModel[20].rotateAngleX = -0.78539816F;

		defaultScopeModel[21].addShapeBox(0F, 0F, 0F, 2, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F); // Import scopeFront3-4
		defaultScopeModel[21].setRotationPoint(26F, -30F, -5.7F);
		defaultScopeModel[21].rotateAngleX = 0.78539816F;

		defaultScopeModel[22].addShapeBox(0F, 0F, 0F, 8, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F); // Import scopeMain1
		defaultScopeModel[22].setRotationPoint(10F, -38F, -2F);

		defaultScopeModel[23].addShapeBox(0F, 0F, 0F, 8, 1, 4, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import scopeMain1-2
		defaultScopeModel[23].setRotationPoint(10F, -27.2F, -2F);

		defaultScopeModel[24].addShapeBox(0F, 0F, 0F, 8, 4, 1, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import scopeMain2
		defaultScopeModel[24].setRotationPoint(10F, -34F, 4.85F);

		defaultScopeModel[25].addShapeBox(0F, 0F, 0F, 8, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F); // Import scopeMain2-2
		defaultScopeModel[25].setRotationPoint(10F, -34F, -5.8F);

		defaultScopeModel[26].addShapeBox(0F, 0F, 0F, 8, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F); // Import scopeMain3
		defaultScopeModel[26].setRotationPoint(10F, -37F, -3F);
		defaultScopeModel[26].rotateAngleX = -0.78539816F;

		defaultScopeModel[27].addShapeBox(0F, 0F, 0F, 8, 4, 1, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import scopeMain3-2
		defaultScopeModel[27].setRotationPoint(10F, -36.3F, 2.3F);
		defaultScopeModel[27].rotateAngleX = 0.78539816F;

		defaultScopeModel[28].addShapeBox(0F, 0F, 0F, 8, 4, 1, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import scopeMain3-3
		defaultScopeModel[28].setRotationPoint(10F, -29.7F, 4.15F);
		defaultScopeModel[28].rotateAngleX = -0.78539816F;

		defaultScopeModel[29].addShapeBox(0F, 0F, 0F, 8, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F); // Import scopeMain3-4
		defaultScopeModel[29].setRotationPoint(10F, -30F, -5.7F);
		defaultScopeModel[29].rotateAngleX = 0.78539816F;

		defaultScopeModel[30].addShapeBox(0F, 0F, 0F, 8, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F); // Import scopeMain4
		defaultScopeModel[30].setRotationPoint(18F, -37F, -2F);

		defaultScopeModel[31].addShapeBox(0F, 0F, 0F, 8, 1, 4, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import scopeMain4-2
		defaultScopeModel[31].setRotationPoint(18F, -28F, -2F);

		defaultScopeModel[32].addShapeBox(0F, 0F, 0F, 8, 4, 1, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import scopeMain5
		defaultScopeModel[32].setRotationPoint(18F, -34F, 4F);

		defaultScopeModel[33].addShapeBox(0F, 0F, 0F, 8, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F); // Import scopeMain5-2
		defaultScopeModel[33].setRotationPoint(18F, -34F, -5F);

		defaultScopeModel[34].addShapeBox(0F, 0F, 0F, 8, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F); // Import scopeMain6
		defaultScopeModel[34].setRotationPoint(18F, -37F, -2.2F);
		defaultScopeModel[34].rotateAngleX = -0.78539816F;

		defaultScopeModel[35].addShapeBox(0F, 0F, 0F, 8, 4, 1, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import scopeMain6-2
		defaultScopeModel[35].setRotationPoint(18F, -36.3F, 1.3F);
		defaultScopeModel[35].rotateAngleX = 0.78539816F;

		defaultScopeModel[36].addShapeBox(0F, 0F, 0F, 8, 4, 1, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import scopeMain6-3
		defaultScopeModel[36].setRotationPoint(18F, -30.7F, 4.3F);
		defaultScopeModel[36].rotateAngleX = -0.78539816F;

		defaultScopeModel[37].addShapeBox(0F, 0F, 0F, 8, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F); // Import scopeMain6-4
		defaultScopeModel[37].setRotationPoint(18F, -29.8F, -4.7F);
		defaultScopeModel[37].rotateAngleX = 0.78539816F;

		defaultScopeModel[38].addBox(0F, 0F, 0F, 1, 1, 3, 0F); // Import scopeSight1
		defaultScopeModel[38].setRotationPoint(5F, -32.5F, 1F);

		defaultScopeModel[39].addBox(0F, 0F, 0F, 1, 1, 3, 0F); // Import scopeSight1-2
		defaultScopeModel[39].setRotationPoint(5F, -32.5F, -4F);

		defaultScopeModel[40].addBox(0F, 0F, 0F, 1, 3, 1, 0F); // Import scopeSight2-1
		defaultScopeModel[40].setRotationPoint(5F, -31F, -0.5F);


		defaultGripModel = new ModelRendererTurbo[8];
		defaultGripModel[0] = new ModelRendererTurbo(this, 33, 100, textureX, textureY); // Import grip1
		defaultGripModel[1] = new ModelRendererTurbo(this, 1, 146, textureX, textureY); // Import grip2
		defaultGripModel[2] = new ModelRendererTurbo(this, 1, 125, textureX, textureY); // Import grip3
		defaultGripModel[3] = new ModelRendererTurbo(this, 1, 136, textureX, textureY); // Import grip4
		defaultGripModel[4] = new ModelRendererTurbo(this, 1, 104, textureX, textureY); // Import grip5
		defaultGripModel[5] = new ModelRendererTurbo(this, 1, 181, textureX, textureY); // Import gripPlate1
		defaultGripModel[6] = new ModelRendererTurbo(this, 1, 193, textureX, textureY); // Import gripPlate2
		defaultGripModel[7] = new ModelRendererTurbo(this, 1, 160, textureX, textureY); // Import gripPlate3

		defaultGripModel[0].addBox(0F, 0F, 0F, 4, 3, 7, 0F); // Import grip1
		defaultGripModel[0].setRotationPoint(-7.5F, -15F, -3.5F);

		defaultGripModel[1].addShapeBox(0F, 0F, 0F, 10, 4, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F); // Import grip2
		defaultGripModel[1].setRotationPoint(-7.5F, -12F, -3.5F);

		defaultGripModel[2].addShapeBox(0F, 0F, 0F, 14, 3, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 2F, 0F, 0F); // Import grip3
		defaultGripModel[2].setRotationPoint(-11.5F, -8F, -3.5F);

		defaultGripModel[3].addShapeBox(0F, 0F, 0F, 13, 2, 7, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F); // Import grip4
		defaultGripModel[3].setRotationPoint(-14.5F, -5F, -3.5F);

		defaultGripModel[4].addShapeBox(0F, 0F, 0F, 12, 13, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F); // Import grip5
		defaultGripModel[4].setRotationPoint(-14.5F, -3F, -3.5F);

		defaultGripModel[5].addShapeBox(0F, 0F, 0F, 13, 3, 8, 0F, 0F, 0F, 0F, -13F, 0F, 0F, -13F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 2F, 0F, 0F); // Import gripPlate1
		defaultGripModel[5].setRotationPoint(-11F, -8F, -4F);

		defaultGripModel[6].addShapeBox(0F, 0F, 0F, 12, 2, 8, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F); // Import gripPlate2
		defaultGripModel[6].setRotationPoint(-14F, -5F, -4F);

		defaultGripModel[7].addShapeBox(0F, 0F, 0F, 11, 12, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F); // Import gripPlate3
		defaultGripModel[7].setRotationPoint(-14F, -3F, -4F);


		ammoModel = new ModelRendererTurbo[18];
		ammoModel[0] = new ModelRendererTurbo(this, 1, 46, textureX, textureY); // Import bullet1
		ammoModel[1] = new ModelRendererTurbo(this, 1, 46, textureX, textureY); // Import bullet1-2
		ammoModel[2] = new ModelRendererTurbo(this, 1, 46, textureX, textureY); // Import bullet1-3
		ammoModel[3] = new ModelRendererTurbo(this, 26, 46, textureX, textureY); // Import bullet2
		ammoModel[4] = new ModelRendererTurbo(this, 26, 46, textureX, textureY); // Import bullet2-2
		ammoModel[5] = new ModelRendererTurbo(this, 26, 46, textureX, textureY); // Import bullet2-3
		ammoModel[6] = new ModelRendererTurbo(this, 1, 51, textureX, textureY); // Import bullet3
		ammoModel[7] = new ModelRendererTurbo(this, 1, 51, textureX, textureY); // Import bullet3-2
		ammoModel[8] = new ModelRendererTurbo(this, 1, 51, textureX, textureY); // Import bullet3-3
		ammoModel[9] = new ModelRendererTurbo(this, 26, 51, textureX, textureY); // Import bullet4
		ammoModel[10] = new ModelRendererTurbo(this, 26, 51, textureX, textureY); // Import bullet4-2
		ammoModel[11] = new ModelRendererTurbo(this, 26, 51, textureX, textureY); // Import bullet4-3
		ammoModel[12] = new ModelRendererTurbo(this, 1, 56, textureX, textureY); // Import bullet5
		ammoModel[13] = new ModelRendererTurbo(this, 1, 56, textureX, textureY); // Import bullet5-2
		ammoModel[14] = new ModelRendererTurbo(this, 1, 56, textureX, textureY); // Import bullet5-3
		ammoModel[15] = new ModelRendererTurbo(this, 26, 56, textureX, textureY); // Import bullet6
		ammoModel[16] = new ModelRendererTurbo(this, 26, 56, textureX, textureY); // Import bullet6-2
		ammoModel[17] = new ModelRendererTurbo(this, 26, 56, textureX, textureY); // Import bullet6-3

		ammoModel[0].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import bullet1
		ammoModel[0].setRotationPoint(5.3F, -22.5F, -1.5F);

		ammoModel[1].addBox(0F, 0F, 0F, 9, 1, 3, 0F); // Import bullet1-2
		ammoModel[1].setRotationPoint(5.3F, -21.5F, -1.5F);

		ammoModel[2].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Import bullet1-3
		ammoModel[2].setRotationPoint(5.3F, -20.5F, -1.5F);

		ammoModel[3].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import bullet2
		ammoModel[3].setRotationPoint(5.3F, -20.5F, -5F);

		ammoModel[4].addBox(0F, 0F, 0F, 9, 1, 3, 0F); // Import bullet2-2
		ammoModel[4].setRotationPoint(5.3F, -19.5F, -5F);

		ammoModel[5].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Import bullet2-3
		ammoModel[5].setRotationPoint(5.3F, -18.5F, -5F);

		ammoModel[6].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import bullet3
		ammoModel[6].setRotationPoint(5.3F, -16.5F, -5F);

		ammoModel[7].addBox(0F, 0F, 0F, 9, 1, 3, 0F); // Import bullet3-2
		ammoModel[7].setRotationPoint(5.3F, -15.5F, -5F);

		ammoModel[8].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Import bullet3-3
		ammoModel[8].setRotationPoint(5.3F, -14.5F, -5F);

		ammoModel[9].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import bullet4
		ammoModel[9].setRotationPoint(5.3F, -14.5F, -1.5F);

		ammoModel[10].addBox(0F, 0F, 0F, 9, 1, 3, 0F); // Import bullet4-2
		ammoModel[10].setRotationPoint(5.3F, -13.5F, -1.5F);

		ammoModel[11].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Import bullet4-3
		ammoModel[11].setRotationPoint(5.3F, -12.5F, -1.5F);

		ammoModel[12].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import bullet5
		ammoModel[12].setRotationPoint(5.3F, -16.5F, 2F);

		ammoModel[13].addBox(0F, 0F, 0F, 9, 1, 3, 0F); // Import bullet5-2
		ammoModel[13].setRotationPoint(5.3F, -15.5F, 2F);

		ammoModel[14].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Import bullet5-3
		ammoModel[14].setRotationPoint(5.3F, -14.5F, 2F);

		ammoModel[15].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import bullet6
		ammoModel[15].setRotationPoint(5.3F, -20.5F, 2F);

		ammoModel[16].addBox(0F, 0F, 0F, 9, 1, 3, 0F); // Import bullet6-2
		ammoModel[16].setRotationPoint(5.3F, -19.5F, 2F);

		ammoModel[17].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Import bullet6-3
		ammoModel[17].setRotationPoint(5.3F, -18.5F, 2F);


		revolverBarrelModel = new ModelRendererTurbo[5];
		revolverBarrelModel[0] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Import canister1
		revolverBarrelModel[1] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Import canister1-2
		revolverBarrelModel[2] = new ModelRendererTurbo(this, 1, 14, textureX, textureY); // Import canister2
		revolverBarrelModel[3] = new ModelRendererTurbo(this, 1, 14, textureX, textureY); // Import canister2-2
		revolverBarrelModel[4] = new ModelRendererTurbo(this, 1, 29, textureX, textureY); // Import canister3

		revolverBarrelModel[0].addShapeBox(0F, 0F, 0F, 11, 2, 10, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import canister1
		revolverBarrelModel[0].setRotationPoint(5.5F, -23F, -5F);

		revolverBarrelModel[1].addShapeBox(0F, 0F, 0F, 11, 2, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Import canister1-2
		revolverBarrelModel[1].setRotationPoint(5.5F, -13F, -5F);

		revolverBarrelModel[2].addShapeBox(0F, 0F, 0F, 11, 2, 12, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import canister2
		revolverBarrelModel[2].setRotationPoint(5.5F, -21F, -6F);

		revolverBarrelModel[3].addShapeBox(0F, 0F, 0F, 11, 2, 12, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Import canister2-2
		revolverBarrelModel[3].setRotationPoint(5.5F, -15F, -6F);

		revolverBarrelModel[4].addBox(0F, 0F, 0F, 11, 4, 12, 0F); // Import canister3
		revolverBarrelModel[4].setRotationPoint(5.5F, -19F, -6F);

		barrelAttachPoint = new Vector3f(57F /16F, 21F /16F, 0F /16F);
		scopeAttachPoint = new Vector3f(14F /16F, 25F /16F, 0F /16F);

		gunSlideDistance = 0F;
		animationType = EnumAnimationType.REVOLVER;

		revolverFlipAngle = -20F;


		translateAll(0F, 0F, 0F);


		flipAll();
	}
}