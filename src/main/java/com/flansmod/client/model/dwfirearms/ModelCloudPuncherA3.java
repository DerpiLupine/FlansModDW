package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelCloudPuncherA3 extends ModelGun
{
	int textureX = 512;
	int textureY = 256;

	public ModelCloudPuncherA3()
	{
		gunModel = new ModelRendererTurbo[108];
		gunModel[0] = new ModelRendererTurbo(this, 211, 15, textureX, textureY); // barrelLeft
		gunModel[1] = new ModelRendererTurbo(this, 211, 15, textureX, textureY); // barrelMiddle
		gunModel[2] = new ModelRendererTurbo(this, 232, 20, textureX, textureY); // barrelPart1
		gunModel[3] = new ModelRendererTurbo(this, 211, 15, textureX, textureY); // barrelRight
		gunModel[4] = new ModelRendererTurbo(this, 104, 99, textureX, textureY); // body1
		gunModel[5] = new ModelRendererTurbo(this, 144, 73, textureX, textureY); // body15
		gunModel[6] = new ModelRendererTurbo(this, 176, 117, textureX, textureY); // body2
		gunModel[7] = new ModelRendererTurbo(this, 104, 33, textureX, textureY); // body3
		gunModel[8] = new ModelRendererTurbo(this, 174, 54, textureX, textureY); // body4
		gunModel[9] = new ModelRendererTurbo(this, 104, 84, textureX, textureY); // body5
		gunModel[10] = new ModelRendererTurbo(this, 203, 43, textureX, textureY); // body6
		gunModel[11] = new ModelRendererTurbo(this, 203, 55, textureX, textureY); // body7
		gunModel[12] = new ModelRendererTurbo(this, 22, 1, textureX, textureY); // railPart1
		gunModel[13] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // railPart2
		gunModel[14] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // railPart3
		gunModel[15] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // railPart4
		gunModel[16] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // railPart5
		gunModel[17] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // railPart6
		gunModel[18] = new ModelRendererTurbo(this, 244, 85, textureX, textureY); // Box 0
		gunModel[19] = new ModelRendererTurbo(this, 236, 55, textureX, textureY); // Box 0
		gunModel[20] = new ModelRendererTurbo(this, 174, 43, textureX, textureY); // Box 1
		gunModel[21] = new ModelRendererTurbo(this, 104, 24, textureX, textureY); // Box 2
		gunModel[22] = new ModelRendererTurbo(this, 104, 55, textureX, textureY); // Box 3
		gunModel[23] = new ModelRendererTurbo(this, 104, 43, textureX, textureY); // Box 4
		gunModel[24] = new ModelRendererTurbo(this, 143, 43, textureX, textureY); // Box 5
		gunModel[25] = new ModelRendererTurbo(this, 143, 55, textureX, textureY); // Box 6
		gunModel[26] = new ModelRendererTurbo(this, 104, 114, textureX, textureY); // Box 1
		gunModel[27] = new ModelRendererTurbo(this, 125, 117, textureX, textureY); // Box 2
		gunModel[28] = new ModelRendererTurbo(this, 180, 131, textureX, textureY); // Box 3
		gunModel[29] = new ModelRendererTurbo(this, 125, 130, textureX, textureY); // Box 4
		gunModel[30] = new ModelRendererTurbo(this, 104, 130, textureX, textureY); // Box 5
		gunModel[31] = new ModelRendererTurbo(this, 229, 131, textureX, textureY); // Box 7
		gunModel[32] = new ModelRendererTurbo(this, 232, 20, textureX, textureY); // Box 8
		gunModel[33] = new ModelRendererTurbo(this, 232, 20, textureX, textureY); // Box 11
		gunModel[34] = new ModelRendererTurbo(this, 232, 20, textureX, textureY); // Box 12
		gunModel[35] = new ModelRendererTurbo(this, 232, 20, textureX, textureY); // Box 13
		gunModel[36] = new ModelRendererTurbo(this, 232, 20, textureX, textureY); // Box 14
		gunModel[37] = new ModelRendererTurbo(this, 232, 20, textureX, textureY); // Box 15
		gunModel[38] = new ModelRendererTurbo(this, 232, 20, textureX, textureY); // Box 16
		gunModel[39] = new ModelRendererTurbo(this, 161, 13, textureX, textureY); // Box 17
		gunModel[40] = new ModelRendererTurbo(this, 178, 12, textureX, textureY); // Box 18
		gunModel[41] = new ModelRendererTurbo(this, 161, 13, textureX, textureY); // Box 19
		gunModel[42] = new ModelRendererTurbo(this, 214, 1, textureX, textureY); // Box 22
		gunModel[43] = new ModelRendererTurbo(this, 214, 1, textureX, textureY); // Box 23
		gunModel[44] = new ModelRendererTurbo(this, 159, 1, textureX, textureY); // Box 26
		gunModel[45] = new ModelRendererTurbo(this, 104, 12, textureX, textureY); // Box 26
		gunModel[46] = new ModelRendererTurbo(this, 104, 1, textureX, textureY); // Box 26
		gunModel[47] = new ModelRendererTurbo(this, 39, 68, textureX, textureY); // Box 27
		gunModel[48] = new ModelRendererTurbo(this, 80, 58, textureX, textureY); // Box 28
		gunModel[49] = new ModelRendererTurbo(this, 41, 78, textureX, textureY); // Box 29
		gunModel[50] = new ModelRendererTurbo(this, 82, 73, textureX, textureY); // Box 30
		gunModel[51] = new ModelRendererTurbo(this, 1, 11, textureX, textureY); // Box 31
		gunModel[52] = new ModelRendererTurbo(this, 1, 11, textureX, textureY); // Box 32
		gunModel[53] = new ModelRendererTurbo(this, 91, 71, textureX, textureY); // Box 33
		gunModel[54] = new ModelRendererTurbo(this, 63, 60, textureX, textureY); // Box 34
		gunModel[55] = new ModelRendererTurbo(this, 63, 56, textureX, textureY); // Box 35
		gunModel[56] = new ModelRendererTurbo(this, 82, 69, textureX, textureY); // Box 36
		gunModel[57] = new ModelRendererTurbo(this, 84, 3, textureX, textureY); // Box 26
		gunModel[58] = new ModelRendererTurbo(this, 204, 15, textureX, textureY); // Box 38
		gunModel[59] = new ModelRendererTurbo(this, 197, 15, textureX, textureY); // Box 39
		gunModel[60] = new ModelRendererTurbo(this, 204, 15, textureX, textureY); // Box 40
		gunModel[61] = new ModelRendererTurbo(this, 173, 85, textureX, textureY); // Box 43
		gunModel[62] = new ModelRendererTurbo(this, 261, 54, textureX, textureY); // Box 44
		gunModel[63] = new ModelRendererTurbo(this, 242, 46, textureX, textureY); // Box 45
		gunModel[64] = new ModelRendererTurbo(this, 242, 50, textureX, textureY); // Box 46
		gunModel[65] = new ModelRendererTurbo(this, 261, 61, textureX, textureY); // Box 47
		gunModel[66] = new ModelRendererTurbo(this, 216, 103, textureX, textureY); // Box 48
		gunModel[67] = new ModelRendererTurbo(this, 242, 24, textureX, textureY); // Box 51
		gunModel[68] = new ModelRendererTurbo(this, 242, 24, textureX, textureY); // Box 52
		gunModel[69] = new ModelRendererTurbo(this, 219, 24, textureX, textureY); // Box 53
		gunModel[70] = new ModelRendererTurbo(this, 219, 24, textureX, textureY); // Box 54
		gunModel[71] = new ModelRendererTurbo(this, 242, 37, textureX, textureY); // Box 55
		gunModel[72] = new ModelRendererTurbo(this, 259, 39, textureX, textureY); // Box 56
		gunModel[73] = new ModelRendererTurbo(this, 219, 37, textureX, textureY); // Box 57
		gunModel[74] = new ModelRendererTurbo(this, 259, 35, textureX, textureY); // Box 58
		gunModel[75] = new ModelRendererTurbo(this, 1, 55, textureX, textureY); // Box 11
		gunModel[76] = new ModelRendererTurbo(this, 1, 15, textureX, textureY); // Box 11
		gunModel[77] = new ModelRendererTurbo(this, 1, 26, textureX, textureY); // Box 11
		gunModel[78] = new ModelRendererTurbo(this, 1, 39, textureX, textureY); // Box 11
		gunModel[79] = new ModelRendererTurbo(this, 32, 55, textureX, textureY); // Box 11
		gunModel[80] = new ModelRendererTurbo(this, 34, 13, textureX, textureY); // Box 11
		gunModel[81] = new ModelRendererTurbo(this, 34, 26, textureX, textureY); // Box 11
		gunModel[82] = new ModelRendererTurbo(this, 34, 39, textureX, textureY); // Box 11
		gunModel[83] = new ModelRendererTurbo(this, 1, 75, textureX, textureY); // Box 11
		gunModel[84] = new ModelRendererTurbo(this, 53, 28, textureX, textureY); // Box 11
		gunModel[85] = new ModelRendererTurbo(this, 70, 42, textureX, textureY); // Box 11
		gunModel[86] = new ModelRendererTurbo(this, 53, 42, textureX, textureY); // Box 11
		gunModel[87] = new ModelRendererTurbo(this, 70, 29, textureX, textureY); // Box 11
		gunModel[88] = new ModelRendererTurbo(this, 1, 65, textureX, textureY); // Box 11
		gunModel[89] = new ModelRendererTurbo(this, 22, 65, textureX, textureY); // Box 60
		gunModel[90] = new ModelRendererTurbo(this, 20, 79, textureX, textureY); // Box 65
		gunModel[91] = new ModelRendererTurbo(this, 87, 19, textureX, textureY); // Box 66
		gunModel[92] = new ModelRendererTurbo(this, 94, 19, textureX, textureY); // Box 67
		gunModel[93] = new ModelRendererTurbo(this, 129, 70, textureX, textureY); // Box 76
		gunModel[94] = new ModelRendererTurbo(this, 153, 68, textureX, textureY); // Box 77
		gunModel[95] = new ModelRendererTurbo(this, 172, 68, textureX, textureY); // Box 78
		gunModel[96] = new ModelRendererTurbo(this, 104, 68, textureX, textureY); // Box 79
		gunModel[97] = new ModelRendererTurbo(this, 104, 76, textureX, textureY); // Box 80
		gunModel[98] = new ModelRendererTurbo(this, 184, 74, textureX, textureY); // Box 81
		gunModel[99] = new ModelRendererTurbo(this, 175, 74, textureX, textureY); // Box 82
		gunModel[100] = new ModelRendererTurbo(this, 153, 73, textureX, textureY); // Box 84
		gunModel[101] = new ModelRendererTurbo(this, 164, 73, textureX, textureY); // Box 85
		gunModel[102] = new ModelRendererTurbo(this, 194, 79, textureX, textureY); // Box 86
		gunModel[103] = new ModelRendererTurbo(this, 153, 77, textureX, textureY); // Box 87
		gunModel[104] = new ModelRendererTurbo(this, 30, 207, textureX, textureY); // Box 99
		gunModel[105] = new ModelRendererTurbo(this, 1, 85, textureX, textureY); // Box 100
		gunModel[106] = new ModelRendererTurbo(this, 149, 102, textureX, textureY); // Box 102
		gunModel[107] = new ModelRendererTurbo(this, 231, 70, textureX, textureY); // Box 103

		gunModel[0].addShapeBox(0F, 0F, 0F, 8, 6, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F); // barrelLeft
		gunModel[0].setRotationPoint(54F, -24F, 1F);

		gunModel[1].addBox(0F, 0F, 0F, 8, 6, 2, 0F); // barrelMiddle
		gunModel[1].setRotationPoint(54F, -24F, -1F);

		gunModel[2].addShapeBox(0F, 0F, 0F, 6, 1, 2, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // barrelPart1
		gunModel[2].setRotationPoint(54F, -24F, -4F);

		gunModel[3].addShapeBox(0F, 0F, 0F, 8, 6, 2, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // barrelRight
		gunModel[3].setRotationPoint(54F, -24F, -3F);

		gunModel[4].addBox(0F, 0F, 0F, 13, 5, 9, 0F); // body1
		gunModel[4].setRotationPoint(-11F, -21F, -4.5F);

		gunModel[5].addBox(0F, 0F, 0F, 1, 7, 3, 0F); // body15
		gunModel[5].setRotationPoint(7.5F, -19F, 4.5F);

		gunModel[6].addShapeBox(0F, 0F, 0F, 6, 4, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F); // body2
		gunModel[6].setRotationPoint(48F, -16F, -4F);

		gunModel[7].addShapeBox(0F, 0F, 0F, 50, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body3
		gunModel[7].setRotationPoint(-1F, -24F, -3.5F);

		gunModel[8].addShapeBox(0F, 0F, 0F, 5, 4, 9, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body4
		gunModel[8].setRotationPoint(49F, -26F, -4.5F);

		gunModel[9].addBox(0F, 0F, 0F, 25, 5, 9, 0F); // body5
		gunModel[9].setRotationPoint(29F, -21F, -4.5F);

		gunModel[10].addBox(0F, 0F, 0F, 10, 1, 9, 0F); // body6
		gunModel[10].setRotationPoint(-11F, -22F, -4.5F);

		gunModel[11].addShapeBox(0F, 0F, 0F, 7, 3, 9, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body7
		gunModel[11].setRotationPoint(-8F, -25F, -4.5F);

		gunModel[12].addBox(0F, 0F, 0F, 27, 2, 7, 0F); // railPart1
		gunModel[12].setRotationPoint(0F, -27F, -3.5F);

		gunModel[13].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // railPart2
		gunModel[13].setRotationPoint(0F, -29F, -3.5F);

		gunModel[14].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // railPart3
		gunModel[14].setRotationPoint(6F, -29F, -3.5F);

		gunModel[15].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // railPart4
		gunModel[15].setRotationPoint(12F, -29F, -3.5F);

		gunModel[16].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // railPart5
		gunModel[16].setRotationPoint(18F, -29F, -3.5F);

		gunModel[17].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // railPart6
		gunModel[17].setRotationPoint(24F, -29F, -3.5F);

		gunModel[18].addBox(0F, 0F, 0F, 24, 4, 9, 0F); // Box 0
		gunModel[18].setRotationPoint(5F, -16F, -4.5F);

		gunModel[19].addShapeBox(0F, 0F, 0F, 3, 3, 9, 0F, -2F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, -2F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 0
		gunModel[19].setRotationPoint(-11F, -25F, -4.5F);

		gunModel[20].addBox(0F, 0F, 0F, 5, 1, 9, 0F); // Box 1
		gunModel[20].setRotationPoint(49F, -22F, -4.5F);

		gunModel[21].addBox(0F, 0F, 0F, 50, 1, 7, 0F); // Box 2
		gunModel[21].setRotationPoint(-1F, -22F, -3.5F);

		gunModel[22].addShapeBox(0F, 0F, 0F, 10, 3, 9, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 3
		gunModel[22].setRotationPoint(0F, -25F, -4.5F);

		gunModel[23].addBox(0F, 0F, 0F, 10, 1, 9, 0F); // Box 4
		gunModel[23].setRotationPoint(0F, -22F, -4.5F);

		gunModel[24].addBox(0F, 0F, 0F, 6, 1, 9, 0F); // Box 5
		gunModel[24].setRotationPoint(23F, -22F, -4.5F);

		gunModel[25].addShapeBox(0F, 0F, 0F, 6, 3, 9, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, -2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F); // Box 6
		gunModel[25].setRotationPoint(21F, -25F, -4.5F);

		gunModel[26].addBox(0F, 0F, 0F, 2, 7, 8, 0F); // Box 1
		gunModel[26].setRotationPoint(29F, -16F, -4F);

		gunModel[27].addBox(0F, 0F, 0F, 17, 4, 8, 0F); // Box 2
		gunModel[27].setRotationPoint(31F, -16F, -4F);

		gunModel[28].addShapeBox(0F, 0F, 0F, 17, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 3
		gunModel[28].setRotationPoint(31F, -9.5F, -3.5F);

		gunModel[29].addBox(0F, 0F, 0F, 20, 3, 7, 0F); // Box 4
		gunModel[29].setRotationPoint(31F, -12.5F, -3.5F);

		gunModel[30].addShapeBox(0F, 0F, 0F, 2, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 5
		gunModel[30].setRotationPoint(29F, -9F, -4F);

		gunModel[31].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, -2F, 0F, -2F, -2F, 0F, -2F, 0F, 0F, -2F); // Box 7
		gunModel[31].setRotationPoint(48F, -9.5F, -3.5F);

		gunModel[32].addShapeBox(0F, 0F, 0F, 6, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 8
		gunModel[32].setRotationPoint(54F, -23F, -4F);

		gunModel[33].addShapeBox(0F, 0F, 0F, 6, 1, 2, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 11
		gunModel[33].setRotationPoint(54F, -20F, -4F);

		gunModel[34].addShapeBox(0F, 0F, 0F, 6, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 12
		gunModel[34].setRotationPoint(54F, -19F, -4F);

		gunModel[35].addShapeBox(0F, 0F, 0F, 6, 1, 2, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 13
		gunModel[35].setRotationPoint(54F, -20F, 2F);

		gunModel[36].addShapeBox(0F, 0F, 0F, 6, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 14
		gunModel[36].setRotationPoint(54F, -19F, 2F);

		gunModel[37].addShapeBox(0F, 0F, 0F, 6, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 15
		gunModel[37].setRotationPoint(54F, -23F, 2F);

		gunModel[38].addShapeBox(0F, 0F, 0F, 6, 1, 2, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 16
		gunModel[38].setRotationPoint(54F, -24F, 2F);

		gunModel[39].addShapeBox(0F, 0F, 0F, 6, 8, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0.5F, 0F, -2F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0.5F, 0F, -2F, 0.5F); // Box 17
		gunModel[39].setRotationPoint(60F, -25F, 1.5F);

		gunModel[40].addBox(0F, 0F, 0F, 6, 8, 3, 0F); // Box 18
		gunModel[40].setRotationPoint(60F, -25F, -1.5F);

		gunModel[41].addShapeBox(0F, 0F, 0F, 6, 8, 2, 0F, 0F, -2F, 0.5F, 0F, -2F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0.5F, 0F, -2F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 19
		gunModel[41].setRotationPoint(60F, -25F, -3.5F);

		gunModel[42].addShapeBox(0F, 0F, 0F, 2, 4, 8, 0F, 0F, -0.5F, -2.5F, 0F, -0.5F, -2.5F, 0F, -0.5F, -2.5F, 0F, -0.5F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 22
		gunModel[42].setRotationPoint(66F, -25F, -4F);

		gunModel[43].addShapeBox(0F, 0F, 0F, 2, 4, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -2.5F, 0F, -0.5F, -2.5F, 0F, -0.5F, -2.5F, 0F, -0.5F, -2.5F); // Box 23
		gunModel[43].setRotationPoint(66F, -21F, -4F);

		gunModel[44].addShapeBox(0F, 0F, 0F, 25, 8, 2, 0F, 0F, -2F, 0.5F, 0F, -2F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0.5F, 0F, -2F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 26
		gunModel[44].setRotationPoint(68F, -25F, -3.5F);

		gunModel[45].addBox(0F, 0F, 0F, 25, 8, 3, 0F); // Box 26
		gunModel[45].setRotationPoint(68F, -25F, -1.5F);

		gunModel[46].addShapeBox(0F, 0F, 0F, 25, 8, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0.5F, 0F, -2F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0.5F, 0F, -2F, 0.5F); // Box 26
		gunModel[46].setRotationPoint(68F, -25F, 1.5F);

		gunModel[47].addBox(0F, 0F, 0F, 17, 5, 4, 0F); // Box 27
		gunModel[47].setRotationPoint(51F, -17F, -2F);

		gunModel[48].addShapeBox(0F, 0F, 0F, 5, 2, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F); // Box 28
		gunModel[48].setRotationPoint(63F, -12F, -2F);

		gunModel[49].addShapeBox(0F, 0F, 0F, 22, 3, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 29
		gunModel[49].setRotationPoint(68F, -17F, -1.5F);

		gunModel[50].addBox(0F, 0F, 0F, 2, 2, 2, 0F); // Box 30
		gunModel[50].setRotationPoint(68F, -14F, -1F);

		gunModel[51].addShapeBox(0F, 0F, 0F, 15, 1, 2, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 31
		gunModel[51].setRotationPoint(70F, -13.5F, -1F);

		gunModel[52].addShapeBox(0F, 0F, 0F, 15, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 32
		gunModel[52].setRotationPoint(70F, -12.5F, -1F);

		gunModel[53].addShapeBox(0F, 0F, 0F, 3, 3, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -2F, 0F, -0.5F, -2F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 33
		gunModel[53].setRotationPoint(90F, -17F, -1.5F);

		gunModel[54].addBox(0F, 0F, 0F, 6, 2, 2, 0F); // Box 34
		gunModel[54].setRotationPoint(85F, -14F, -1F);

		gunModel[55].addShapeBox(0F, 0F, 0F, 6, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 35
		gunModel[55].setRotationPoint(85F, -12F, -1F);

		gunModel[56].addShapeBox(0F, 0F, 0F, 2, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 36
		gunModel[56].setRotationPoint(68F, -12F, -1F);

		gunModel[57].addShapeBox(0F, 0F, 0F, 6, 2, 2, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 26
		gunModel[57].setRotationPoint(87F, -27F, -1F);

		gunModel[58].addShapeBox(0F, 0F, 0F, 1, 6, 2, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 38
		gunModel[58].setRotationPoint(93F, -24F, -3F);

		gunModel[59].addBox(0F, 0F, 0F, 1, 6, 2, 0F); // Box 39
		gunModel[59].setRotationPoint(93F, -24F, -1F);

		gunModel[60].addShapeBox(0F, 0F, 0F, 1, 6, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F); // Box 40
		gunModel[60].setRotationPoint(93F, -24F, 1F);

		gunModel[61].addBox(0F, 0F, 0F, 27, 5, 8, 0F); // Box 43
		gunModel[61].setRotationPoint(2F, -21F, -3.5F);

		gunModel[62].addBox(0F, 0F, 0F, 7, 5, 1, 0F); // Box 44
		gunModel[62].setRotationPoint(2F, -21F, -4.5F);

		gunModel[63].addShapeBox(0F, 0F, 0F, 12, 1, 2, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 45
		gunModel[63].setRotationPoint(9F, -21F, -5.5F);

		gunModel[64].addShapeBox(0F, 0F, 0F, 12, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 46
		gunModel[64].setRotationPoint(9F, -17F, -5.5F);

		gunModel[65].addBox(0F, 0F, 0F, 8, 5, 1, 0F); // Box 47
		gunModel[65].setRotationPoint(21F, -21F, -4.5F);

		gunModel[66].addShapeBox(0F, 0F, 0F, 27, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 48
		gunModel[66].setRotationPoint(2F, -9F, -3.5F);

		gunModel[67].addShapeBox(0F, 0F, 0F, 3, 2, 8, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 51
		gunModel[67].setRotationPoint(30F, -25F, -4F);

		gunModel[68].addBox(0F, 0F, 0F, 3, 2, 8, 0F); // Box 52
		gunModel[68].setRotationPoint(30F, -23F, -4F);

		gunModel[69].addBox(0F, 0F, 0F, 3, 2, 8, 0F); // Box 53
		gunModel[69].setRotationPoint(41F, -23F, -4F);

		gunModel[70].addShapeBox(0F, 0F, 0F, 3, 2, 8, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 54
		gunModel[70].setRotationPoint(41F, -25F, -4F);

		gunModel[71].addShapeBox(0F, 0F, 0F, 5, 2, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 55
		gunModel[71].setRotationPoint(33F, -25F, 1F);

		gunModel[72].addBox(0F, 0F, 0F, 8, 2, 1, 0F); // Box 56
		gunModel[72].setRotationPoint(33F, -23F, -4F);

		gunModel[73].addShapeBox(0F, 0F, 0F, 8, 2, 3, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 57
		gunModel[73].setRotationPoint(33F, -25F, -4F);

		gunModel[74].addBox(0F, 0F, 0F, 5, 2, 1, 0F); // Box 58
		gunModel[74].setRotationPoint(33F, -23F, 3F);

		gunModel[75].addBox(0F, 0F, 0F, 8, 2, 7, 0F); // Box 11
		gunModel[75].setRotationPoint(-6F, -9F, -3.5F);

		gunModel[76].addShapeBox(0F, 0F, 0F, 9, 3, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 11
		gunModel[76].setRotationPoint(-7F, -7F, -3.5F);

		gunModel[77].addShapeBox(0F, 0F, 0F, 9, 5, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 4F, 0F, 0F); // Box 11
		gunModel[77].setRotationPoint(-7F, -4F, -3.5F);

		gunModel[78].addShapeBox(0F, 0F, 0F, 9, 8, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, -3F, 0F, -5F, 0F, 0F, -5F, 0F, 0F, 4F, -3F, 0F); // Box 11
		gunModel[78].setRotationPoint(-11F, 1F, -3.5F);

		gunModel[79].addShapeBox(0F, 0F, 0F, 8, 2, 7, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F); // Box 11
		gunModel[79].setRotationPoint(-15F, 9F, -3.5F);

		gunModel[80].addShapeBox(0F, 0F, 0F, 2, 5, 7, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F); // Box 11
		gunModel[80].setRotationPoint(2F, -9F, -3.5F);

		gunModel[81].addShapeBox(0F, 0F, 0F, 2, 5, 7, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 4F, 0F, 0F, -4F, 0F, -1.5F, -4F, 0F, -1.5F, 4F, 0F, 0F); // Box 11
		gunModel[81].setRotationPoint(2F, -4F, -3.5F);

		gunModel[82].addShapeBox(0F, 0F, 0F, 2, 8, 7, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 5F, 0F, 0F, -5F, 0F, -1.5F, -5F, 0F, -1.5F, 5F, 0F, 0F); // Box 11
		gunModel[82].setRotationPoint(-2F, 1F, -3.5F);

		gunModel[83].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F); // Box 11
		gunModel[83].setRotationPoint(-7F, 9F, -3.5F);

		gunModel[84].addShapeBox(0F, 0F, 0F, 1, 3, 7, 0F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F); // Box 11
		gunModel[84].setRotationPoint(-8F, -7F, -3.5F);

		gunModel[85].addShapeBox(0F, 0F, 0F, 1, 5, 7, 0F, -4F, 0F, -1.5F, 4F, 0F, 0F, 4F, 0F, 0F, -4F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F); // Box 11
		gunModel[85].setRotationPoint(-12F, -4F, -3.5F);

		gunModel[86].addShapeBox(0F, 0F, 0F, 1, 5, 7, 0F, -4F, 0F, -1.5F, 4F, 0F, 0F, 4F, 0F, 0F, -4F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F); // Box 11
		gunModel[86].setRotationPoint(-16F, 1F, -3.5F);

		gunModel[87].addShapeBox(0F, 0F, 0F, 1, 2, 7, 0F, 2F, 0F, -1.5F, -2F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F); // Box 11
		gunModel[87].setRotationPoint(-8F, -9F, -3.5F);

		gunModel[88].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F); // Box 11
		gunModel[88].setRotationPoint(-9F, -9F, -3.5F);

		gunModel[89].addShapeBox(0F, 0F, 0F, 1, 2, 7, 0F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F); // Box 60
		gunModel[89].setRotationPoint(-16F, 6F, -3.5F);

		gunModel[90].addBox(0F, 0F, 0F, 6, 1, 4, 0F); // Box 65
		gunModel[90].setRotationPoint(54F, -18F, -2F);

		gunModel[91].addShapeBox(0F, 0F, 0F, 2, 5, 1, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 66
		gunModel[91].setRotationPoint(21F, -21F, -5.5F);

		gunModel[92].addShapeBox(0F, 0F, 0F, 2, 5, 1, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 67
		gunModel[92].setRotationPoint(7F, -21F, -5.5F);

		gunModel[93].addBox(0F, 0F, 0F, 1, 7, 6, 0F); // Box 76
		gunModel[93].setRotationPoint(23.5F, -19F, 4.5F);

		gunModel[94].addBox(0F, 0F, 0F, 6, 1, 3, 0F); // Box 77
		gunModel[94].setRotationPoint(8.5F, -19F, 4.5F);

		gunModel[95].addBox(0F, 0F, 0F, 6, 1, 3, 0F); // Box 78
		gunModel[95].setRotationPoint(8.5F, -13F, 4.5F);

		gunModel[96].addBox(0F, 0F, 0F, 6, 1, 6, 0F); // Box 79
		gunModel[96].setRotationPoint(17.5F, -19F, 4.5F);

		gunModel[97].addBox(0F, 0F, 0F, 6, 1, 6, 0F); // Box 80
		gunModel[97].setRotationPoint(17.5F, -13F, 4.5F);

		gunModel[98].addShapeBox(0F, 0F, 0F, 3, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 0F); // Box 81
		gunModel[98].setRotationPoint(14.5F, -13F, 6.5F);

		gunModel[99].addShapeBox(0F, 0F, 0F, 3, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 0F); // Box 82
		gunModel[99].setRotationPoint(14.5F, -19F, 6.5F);

		gunModel[100].addBox(0F, 0F, 0F, 3, 1, 2, 0F); // Box 84
		gunModel[100].setRotationPoint(14.5F, -19F, 4.5F);

		gunModel[101].addBox(0F, 0F, 0F, 3, 1, 2, 0F); // Box 85
		gunModel[101].setRotationPoint(14.5F, -13F, 4.5F);

		gunModel[102].addShapeBox(0F, 0F, 0F, 17, 3, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F); // Box 86
		gunModel[102].setRotationPoint(7.5F, -12F, 6.5F);

		gunModel[103].addShapeBox(0F, 0F, 0F, 17, 3, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // Box 87
		gunModel[103].setRotationPoint(7.5F, -12F, 4F);

		gunModel[104].addShapeBox(0F, 0F, 0F, 2, 1, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F); // Box 99
		gunModel[104].setRotationPoint(-11F, -9F, -3.5F);

		gunModel[105].addBox(0F, 0F, 0F, 15, 7, 8, 0F); // Box 100
		gunModel[105].setRotationPoint(-11F, -16F, -4F);

		gunModel[106].addBox(0F, 0F, 0F, 25, 3, 8, 0F); // Box 102
		gunModel[106].setRotationPoint(4F, -12F, -4F);

		gunModel[107].addShapeBox(0F, 0F, 0F, 3, 4, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F); // Box 103
		gunModel[107].setRotationPoint(2F, -16F, -4.5F);

		defaultScopeModel = new ModelRendererTurbo[47];
		defaultScopeModel[0] = new ModelRendererTurbo(this, 362, 58, textureX, textureY); // scope
		defaultScopeModel[1] = new ModelRendererTurbo(this, 321, 56, textureX, textureY); // scope
		defaultScopeModel[2] = new ModelRendererTurbo(this, 280, 54, textureX, textureY); // scope
		defaultScopeModel[3] = new ModelRendererTurbo(this, 321, 56, textureX, textureY); // scope
		defaultScopeModel[4] = new ModelRendererTurbo(this, 362, 58, textureX, textureY); // scope
		defaultScopeModel[5] = new ModelRendererTurbo(this, 354, 84, textureX, textureY); // scope
		defaultScopeModel[6] = new ModelRendererTurbo(this, 397, 86, textureX, textureY); // scope
		defaultScopeModel[7] = new ModelRendererTurbo(this, 354, 84, textureX, textureY); // scope
		defaultScopeModel[8] = new ModelRendererTurbo(this, 311, 82, textureX, textureY); // scope
		defaultScopeModel[9] = new ModelRendererTurbo(this, 397, 86, textureX, textureY); // scope
		defaultScopeModel[10] = new ModelRendererTurbo(this, 333, 25, textureX, textureY); // scope
		defaultScopeModel[11] = new ModelRendererTurbo(this, 280, 24, textureX, textureY); // scope
		defaultScopeModel[12] = new ModelRendererTurbo(this, 333, 25, textureX, textureY); // scope
		defaultScopeModel[13] = new ModelRendererTurbo(this, 280, 40, textureX, textureY); // scope
		defaultScopeModel[14] = new ModelRendererTurbo(this, 280, 40, textureX, textureY); // scope
		defaultScopeModel[15] = new ModelRendererTurbo(this, 280, 40, textureX, textureY); // scope
		defaultScopeModel[16] = new ModelRendererTurbo(this, 337, 44, textureX, textureY); // scope
		defaultScopeModel[17] = new ModelRendererTurbo(this, 337, 44, textureX, textureY); // scope
		defaultScopeModel[18] = new ModelRendererTurbo(this, 337, 44, textureX, textureY); // scope
		defaultScopeModel[19] = new ModelRendererTurbo(this, 326, 44, textureX, textureY); // scope
		defaultScopeModel[20] = new ModelRendererTurbo(this, 326, 44, textureX, textureY); // scope
		defaultScopeModel[21] = new ModelRendererTurbo(this, 326, 44, textureX, textureY); // scope
		defaultScopeModel[22] = new ModelRendererTurbo(this, 303, 40, textureX, textureY); // scope
		defaultScopeModel[23] = new ModelRendererTurbo(this, 303, 40, textureX, textureY); // scope
		defaultScopeModel[24] = new ModelRendererTurbo(this, 303, 40, textureX, textureY); // scope
		defaultScopeModel[25] = new ModelRendererTurbo(this, 280, 40, textureX, textureY); // scope
		defaultScopeModel[26] = new ModelRendererTurbo(this, 280, 40, textureX, textureY); // scope
		defaultScopeModel[27] = new ModelRendererTurbo(this, 280, 40, textureX, textureY); // scope
		defaultScopeModel[28] = new ModelRendererTurbo(this, 280, 1, textureX, textureY); // scope
		defaultScopeModel[29] = new ModelRendererTurbo(this, 341, 10, textureX, textureY); // scope
		defaultScopeModel[30] = new ModelRendererTurbo(this, 341, 10, textureX, textureY); // scope
		defaultScopeModel[31] = new ModelRendererTurbo(this, 402, 15, textureX, textureY); // scope
		defaultScopeModel[32] = new ModelRendererTurbo(this, 402, 30, textureX, textureY); // scope
		defaultScopeModel[33] = new ModelRendererTurbo(this, 402, 35, textureX, textureY); // scope
		defaultScopeModel[34] = new ModelRendererTurbo(this, 402, 25, textureX, textureY); // scope
		defaultScopeModel[35] = new ModelRendererTurbo(this, 360, 109, textureX, textureY); // scope
		defaultScopeModel[36] = new ModelRendererTurbo(this, 360, 99, textureX, textureY); // scope
		defaultScopeModel[37] = new ModelRendererTurbo(this, 425, 26, textureX, textureY); // scope
		defaultScopeModel[38] = new ModelRendererTurbo(this, 425, 26, textureX, textureY); // scope
		defaultScopeModel[39] = new ModelRendererTurbo(this, 311, 111, textureX, textureY); // scope
		defaultScopeModel[40] = new ModelRendererTurbo(this, 311, 121, textureX, textureY); // scope
		defaultScopeModel[41] = new ModelRendererTurbo(this, 402, 8, textureX, textureY); // scope
		defaultScopeModel[42] = new ModelRendererTurbo(this, 402, 1, textureX, textureY); // scope
		defaultScopeModel[43] = new ModelRendererTurbo(this, 311, 99, textureX, textureY); // scope
		defaultScopeModel[44] = new ModelRendererTurbo(this, 403, 109, textureX, textureY); // scope
		defaultScopeModel[45] = new ModelRendererTurbo(this, 403, 99, textureX, textureY); // scope
		defaultScopeModel[46] = new ModelRendererTurbo(this, 354, 40, textureX, textureY); // Box 167

		defaultScopeModel[0].addShapeBox(14F, -7F, -5F, 8, 2, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // scope
		defaultScopeModel[0].setRotationPoint(15F, -26F, 0F);

		defaultScopeModel[1].addShapeBox(14F, -9F, -6F, 8, 2, 12, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // scope
		defaultScopeModel[1].setRotationPoint(15F, -26F, 0F);

		defaultScopeModel[2].addBox(14F, -13F, -6F, 8, 4, 12, 0F); // scope
		defaultScopeModel[2].setRotationPoint(15F, -26F, 0F);

		defaultScopeModel[3].addShapeBox(14F, -15F, -6F, 8, 2, 12, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope
		defaultScopeModel[3].setRotationPoint(15F, -26F, 0F);

		defaultScopeModel[4].addShapeBox(14F, -17F, -5F, 8, 2, 10, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope
		defaultScopeModel[4].setRotationPoint(15F, -26F, 0F);

		defaultScopeModel[5].addShapeBox(-19F, -15F, -6F, 9, 2, 12, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope
		defaultScopeModel[5].setRotationPoint(15F, -26F, 0F);

		defaultScopeModel[6].addShapeBox(-19F, -7F, -5F, 9, 2, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // scope
		defaultScopeModel[6].setRotationPoint(15F, -26F, 0F);

		defaultScopeModel[7].addShapeBox(-19F, -9F, -6F, 9, 2, 12, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // scope
		defaultScopeModel[7].setRotationPoint(15F, -26F, 0F);

		defaultScopeModel[8].addBox(-19F, -13F, -6F, 9, 4, 12, 0F); // scope
		defaultScopeModel[8].setRotationPoint(15F, -26F, 0F);

		defaultScopeModel[9].addShapeBox(-19F, -17F, -5F, 9, 2, 10, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope
		defaultScopeModel[9].setRotationPoint(15F, -26F, 0F);

		defaultScopeModel[10].addShapeBox(9F, -16F, -5.5F, 15, 3, 11, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope
		defaultScopeModel[10].setRotationPoint(15F, -26F, 0F);

		defaultScopeModel[11].addBox(9F, -13F, -5.5F, 15, 4, 11, 0F); // scope
		defaultScopeModel[11].setRotationPoint(15F, -26F, 0F);

		defaultScopeModel[12].addShapeBox(9F, -9F, -5.5F, 15, 3, 11, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // scope
		defaultScopeModel[12].setRotationPoint(15F, -26F, 0F);

		defaultScopeModel[13].addShapeBox(-19.2F, -15.5F, -5F, 1, 3, 10, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope
		defaultScopeModel[13].setRotationPoint(15F, -26F, 0F);

		defaultScopeModel[14].addBox(-19.2F, -12.5F, -5F, 1, 3, 10, 0F); // scope
		defaultScopeModel[14].setRotationPoint(15F, -26F, 0F);

		defaultScopeModel[15].addShapeBox(-19.2F, -9.5F, -5F, 1, 3, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // scope
		defaultScopeModel[15].setRotationPoint(15F, -26F, 0F);

		defaultScopeModel[16].addShapeBox(-3F, -19F, -3F, 2, 3, 6, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F); // scope
		defaultScopeModel[16].setRotationPoint(15F, -26F, 0F);

		defaultScopeModel[17].addShapeBox(1F, -19F, -3F, 2, 3, 6, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F); // scope
		defaultScopeModel[17].setRotationPoint(15F, -26F, 0F);

		defaultScopeModel[18].addBox(-1F, -19F, -3F, 2, 3, 6, 0F); // scope
		defaultScopeModel[18].setRotationPoint(15F, -26F, 0F);

		defaultScopeModel[19].addShapeBox(15F, -14F, -8F, 2, 6, 3, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F); // scope
		defaultScopeModel[19].setRotationPoint(15F, -26F, 0F);

		defaultScopeModel[20].addBox(17F, -14F, -8F, 2, 6, 3, 0F); // scope
		defaultScopeModel[20].setRotationPoint(15F, -26F, 0F);

		defaultScopeModel[21].addShapeBox(19F, -14F, -8F, 2, 6, 3, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F); // scope
		defaultScopeModel[21].setRotationPoint(15F, -26F, 0F);

		defaultScopeModel[22].addShapeBox(-10.1F, -15.5F, -5F, 1, 3, 10, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope
		defaultScopeModel[22].setRotationPoint(15F, -26F, 0F);

		defaultScopeModel[23].addBox(-10.1F, -12.5F, -5F, 1, 3, 10, 0F); // scope
		defaultScopeModel[23].setRotationPoint(15F, -26F, 0F);

		defaultScopeModel[24].addShapeBox(-10.1F, -9.5F, -5F, 1, 3, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // scope
		defaultScopeModel[24].setRotationPoint(15F, -26F, 0F);

		defaultScopeModel[25].addShapeBox(23.2F, -15.5F, -5F, 1, 3, 10, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope
		defaultScopeModel[25].setRotationPoint(15F, -26F, 0F);

		defaultScopeModel[26].addBox(23.2F, -12.5F, -5F, 1, 3, 10, 0F); // scope
		defaultScopeModel[26].setRotationPoint(15F, -26F, 0F);

		defaultScopeModel[27].addShapeBox(23.2F, -9.5F, -5F, 1, 3, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // scope
		defaultScopeModel[27].setRotationPoint(15F, -26F, 0F);

		defaultScopeModel[28].addBox(-9F, -16F, -6F, 18, 10, 12, 0F); // scope
		defaultScopeModel[28].setRotationPoint(15F, -26F, 0F);

		defaultScopeModel[29].addShapeBox(-9F, -17F, -6F, 18, 1, 12, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope
		defaultScopeModel[29].setRotationPoint(15F, -26F, 0F);

		defaultScopeModel[30].addShapeBox(-9F, -6F, -6F, 18, 1, 12, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // scope
		defaultScopeModel[30].setRotationPoint(15F, -26F, 0F);

		defaultScopeModel[31].addShapeBox(-8F, -16F, 6F, 8, 4, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F); // scope
		defaultScopeModel[31].setRotationPoint(15F, -26F, 0F);

		defaultScopeModel[32].addShapeBox(-8F, -12F, 6F, 8, 2, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F); // scope
		defaultScopeModel[32].setRotationPoint(15F, -26F, 0F);

		defaultScopeModel[33].addBox(-8F, -10F, 5.5F, 8, 4, 1, 0F); // scope
		defaultScopeModel[33].setRotationPoint(15F, -26F, 0F);

		defaultScopeModel[34].addShapeBox(-8F, -17F, 5F, 8, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope
		defaultScopeModel[34].setRotationPoint(15F, -26F, 0F);

		defaultScopeModel[35].addShapeBox(-18.5F, -20F, -3.5F, 14, 1, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope
		defaultScopeModel[35].setRotationPoint(15F, -26F, 0F);

		defaultScopeModel[36].addBox(-18.5F, -19F, -3.5F, 14, 2, 7, 0F); // scope
		defaultScopeModel[36].setRotationPoint(15F, -26F, 0F);

		defaultScopeModel[37].addShapeBox(-4.5F, -18F, 1F, 1, 1, 2, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope
		defaultScopeModel[37].setRotationPoint(15F, -26F, 0F);

		defaultScopeModel[38].addShapeBox(-4.5F, -18F, -3F, 1, 1, 2, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope
		defaultScopeModel[38].setRotationPoint(15F, -26F, 0F);

		defaultScopeModel[39].addShapeBox(-8F, -4F, -4F, 16, 1, 8, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope
		defaultScopeModel[39].setRotationPoint(15F, -26F, 0F);

		defaultScopeModel[40].addBox(-6F, -5F, -3F, 12, 1, 6, 0F); // scope
		defaultScopeModel[40].setRotationPoint(15F, -26F, 0F);

		defaultScopeModel[41].addShapeBox(-9F, -11F, -6.5F, 12, 5, 1, 0F, -1F, 0F, 0F, 2F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, -1F, -1F, 0F, -0.5F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope
		defaultScopeModel[41].setRotationPoint(15F, -26F, 0F);

		defaultScopeModel[42].addShapeBox(-9F, -16F, -6.5F, 15, 5, 1, 0F, -1F, -1F, 0F, -1F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope
		defaultScopeModel[42].setRotationPoint(15F, -26F, 0F);

		defaultScopeModel[43].addBox(-8F, -3F, -4F, 16, 3, 8, 0F); // scope
		defaultScopeModel[43].setRotationPoint(15F, -26F, 0F);

		defaultScopeModel[44].addShapeBox(-17.5F, -17F, -4F, 3, 1, 8, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope
		defaultScopeModel[44].setRotationPoint(15F, -26F, 0F);

		defaultScopeModel[45].addBox(-17.5F, -16F, -4F, 3, 1, 8, 0F); // scope
		defaultScopeModel[45].setRotationPoint(15F, -26F, 0F);

		defaultScopeModel[46].addShapeBox(-18.5F, -19F, -3.5F, 1, 6, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -5F, 0F, 0F, -5F, 0F, -5F, 0F, 0F, -5F, 0F, 0F, -5F, -5F, 0F, -5F, -5F); // Box 167
		defaultScopeModel[46].setRotationPoint(14.8F, -26F, 1F);


		defaultStockModel = new ModelRendererTurbo[42];
		defaultStockModel[0] = new ModelRendererTurbo(this, 38, 114, textureX, textureY); // Box 11
		defaultStockModel[1] = new ModelRendererTurbo(this, 1, 115, textureX, textureY); // Box 11
		defaultStockModel[2] = new ModelRendererTurbo(this, 157, 159, textureX, textureY); // Box 11
		defaultStockModel[3] = new ModelRendererTurbo(this, 47, 103, textureX, textureY); // Box 11
		defaultStockModel[4] = new ModelRendererTurbo(this, 72, 104, textureX, textureY); // Box 11
		defaultStockModel[5] = new ModelRendererTurbo(this, 1, 133, textureX, textureY); // Box 11
		defaultStockModel[6] = new ModelRendererTurbo(this, 1, 125, textureX, textureY); // Box 11
		defaultStockModel[7] = new ModelRendererTurbo(this, 1, 195, textureX, textureY); // Box 11
		defaultStockModel[8] = new ModelRendererTurbo(this, 48, 85, textureX, textureY); // Box 11
		defaultStockModel[9] = new ModelRendererTurbo(this, 30, 144, textureX, textureY); // Box 11
		defaultStockModel[10] = new ModelRendererTurbo(this, 26, 180, textureX, textureY); // Box 11
		defaultStockModel[11] = new ModelRendererTurbo(this, 30, 161, textureX, textureY); // Box 11
		defaultStockModel[12] = new ModelRendererTurbo(this, 30, 102, textureX, textureY); // Box 11
		defaultStockModel[13] = new ModelRendererTurbo(this, 50, 195, textureX, textureY); // Box 11
		defaultStockModel[14] = new ModelRendererTurbo(this, 70, 14, textureX, textureY); // Box 11
		defaultStockModel[15] = new ModelRendererTurbo(this, 53, 14, textureX, textureY); // Box 11
		defaultStockModel[16] = new ModelRendererTurbo(this, 1, 207, textureX, textureY); // Box 11
		defaultStockModel[17] = new ModelRendererTurbo(this, 35, 216, textureX, textureY); // Box 11
		defaultStockModel[18] = new ModelRendererTurbo(this, 35, 216, textureX, textureY); // Box 11
		defaultStockModel[19] = new ModelRendererTurbo(this, 157, 143, textureX, textureY); // Box 11
		defaultStockModel[20] = new ModelRendererTurbo(this, 34, 232, textureX, textureY); // Box 11
		defaultStockModel[21] = new ModelRendererTurbo(this, 1, 142, textureX, textureY); // Box 11
		defaultStockModel[22] = new ModelRendererTurbo(this, 1, 101, textureX, textureY); // Box 11
		defaultStockModel[23] = new ModelRendererTurbo(this, 1, 167, textureX, textureY); // Box 11
		defaultStockModel[24] = new ModelRendererTurbo(this, 1, 101, textureX, textureY); // Box 11
		defaultStockModel[25] = new ModelRendererTurbo(this, 1, 184, textureX, textureY); // Box 11
		defaultStockModel[26] = new ModelRendererTurbo(this, 35, 216, textureX, textureY); // Box 11
		defaultStockModel[27] = new ModelRendererTurbo(this, 57, 180, textureX, textureY); // Box 11
		defaultStockModel[28] = new ModelRendererTurbo(this, 1, 216, textureX, textureY); // Box 11
		defaultStockModel[29] = new ModelRendererTurbo(this, 1, 230, textureX, textureY); // Box 11
		defaultStockModel[30] = new ModelRendererTurbo(this, 1, 216, textureX, textureY); // Box 11
		defaultStockModel[31] = new ModelRendererTurbo(this, 104, 155, textureX, textureY); // Box 11
		defaultStockModel[32] = new ModelRendererTurbo(this, 117, 164, textureX, textureY); // Box 11
		defaultStockModel[33] = new ModelRendererTurbo(this, 130, 153, textureX, textureY); // Box 11
		defaultStockModel[34] = new ModelRendererTurbo(this, 117, 169, textureX, textureY); // Box 11
		defaultStockModel[35] = new ModelRendererTurbo(this, 117, 155, textureX, textureY); // Box 11
		defaultStockModel[36] = new ModelRendererTurbo(this, 117, 143, textureX, textureY); // Box 11
		defaultStockModel[37] = new ModelRendererTurbo(this, 104, 143, textureX, textureY); // Box 11
		defaultStockModel[38] = new ModelRendererTurbo(this, 130, 143, textureX, textureY); // Box 11
		defaultStockModel[39] = new ModelRendererTurbo(this, 104, 164, textureX, textureY); // Box 11
		defaultStockModel[40] = new ModelRendererTurbo(this, 130, 161, textureX, textureY); // Box 11
		defaultStockModel[41] = new ModelRendererTurbo(this, 186, 159, textureX, textureY); // Box 11

		defaultStockModel[0].addBox(0F, 0F, 0F, 10, 2, 8, 0F); // Box 11
		defaultStockModel[0].setRotationPoint(-44F, -20F, -4F);

		defaultStockModel[1].addShapeBox(0F, 0F, 0F, 10, 1, 8, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 11
		defaultStockModel[1].setRotationPoint(-44F, -21F, -4F);

		defaultStockModel[2].addBox(0F, 0F, 0F, 5, 6, 9, 0F); // Box 11
		defaultStockModel[2].setRotationPoint(-44F, -18F, -4.5F);

		defaultStockModel[3].addShapeBox(0F, 0F, 0F, 4, 2, 8, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 11
		defaultStockModel[3].setRotationPoint(-34F, -20F, -4F);

		defaultStockModel[4].addShapeBox(0F, 0F, 0F, 2, 1, 8, 0F, 0F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 11
		defaultStockModel[4].setRotationPoint(-34F, -21F, -4F);

		defaultStockModel[5].addBox(0F, 0F, 0F, 21, 2, 6, 0F); // Box 11
		defaultStockModel[5].setRotationPoint(-33F, -19.5F, -3F);

		defaultStockModel[6].addShapeBox(0F, 0F, 0F, 21, 1, 6, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 11
		defaultStockModel[6].setRotationPoint(-33F, -20.5F, -3F);

		defaultStockModel[7].addBox(0F, 0F, 0F, 16, 3, 8, 0F); // Box 11
		defaultStockModel[7].setRotationPoint(-39F, -12F, -4F);

		defaultStockModel[8].addBox(0F, 0F, 0F, 12, 7, 8, 0F); // Box 11
		defaultStockModel[8].setRotationPoint(-23F, -16F, -4F);

		defaultStockModel[9].addBox(0F, 0F, 0F, 10, 8, 8, 0F); // Box 11
		defaultStockModel[9].setRotationPoint(-44F, -9F, -4F);

		defaultStockModel[10].addShapeBox(0F, 0F, 0F, 7, 6, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -6F, 0F, 0F, -6F, 0F, 0F, 0F, 0F, 0F); // Box 11
		defaultStockModel[10].setRotationPoint(-41F, -1F, -4F);

		defaultStockModel[11].addBox(0F, 0F, 0F, 12, 8, 7, 0F); // Box 11
		defaultStockModel[11].setRotationPoint(-34F, -9F, -3.5F);

		defaultStockModel[12].addShapeBox(0F, 0F, 0F, 1, 4, 7, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 11
		defaultStockModel[12].setRotationPoint(-22F, -9F, -3.5F);

		defaultStockModel[13].addBox(0F, 0F, 0F, 1, 4, 7, 0F); // Box 11
		defaultStockModel[13].setRotationPoint(-22F, -5F, -3.5F);

		defaultStockModel[14].addShapeBox(0F, 0F, 0F, 1, 4, 7, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F); // Box 11
		defaultStockModel[14].setRotationPoint(-21F, -5F, -3.5F);

		defaultStockModel[15].addShapeBox(0F, 0F, 0F, 1, 4, 7, 0F, -3F, 0F, 0F, 3F, 0F, -1.5F, 3F, 0F, -1.5F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F); // Box 11
		defaultStockModel[15].setRotationPoint(-21F, -9F, -3.5F);

		defaultStockModel[16].addShapeBox(0F, 0F, 0F, 7, 1, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F); // Box 11
		defaultStockModel[16].setRotationPoint(-18F, -9F, -3.5F);

		defaultStockModel[17].addBox(0F, 0F, 0F, 1, 6, 9, 0F); // Box 11
		defaultStockModel[17].setRotationPoint(-36F, -18F, -4.5F);

		defaultStockModel[18].addBox(0F, 0F, 0F, 1, 6, 9, 0F); // Box 11
		defaultStockModel[18].setRotationPoint(-34F, -18F, -4.5F);

		defaultStockModel[19].addBox(0F, 0F, 0F, 16, 6, 9, 0F); // Box 11
		defaultStockModel[19].setRotationPoint(-32F, -18F, -4.5F);

		defaultStockModel[20].addBox(0F, 0F, 0F, 7, 6, 8, 0F); // Box 11
		defaultStockModel[20].setRotationPoint(-39F, -18F, -4F);

		defaultStockModel[21].addBox(0F, 0F, 0F, 4, 14, 10, 0F); // Box 11
		defaultStockModel[21].setRotationPoint(-48F, -19F, -5F);

		defaultStockModel[22].addShapeBox(0F, 0F, 0F, 4, 2, 10, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 11
		defaultStockModel[22].setRotationPoint(-48F, -21F, -5F);

		defaultStockModel[23].addBox(0F, 0F, 0F, 4, 8, 8, 0F); // Box 11
		defaultStockModel[23].setRotationPoint(-48F, -3F, -4F);

		defaultStockModel[24].addShapeBox(0F, 0F, 0F, 4, 2, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 11
		defaultStockModel[24].setRotationPoint(-48F, -5F, -5F);

		defaultStockModel[25].addShapeBox(0F, 0F, 0F, 4, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 11
		defaultStockModel[25].setRotationPoint(-48F, 5F, -4F);

		defaultStockModel[26].addBox(0F, 0F, 0F, 1, 6, 9, 0F); // Box 11
		defaultStockModel[26].setRotationPoint(-38F, -18F, -4.5F);

		defaultStockModel[27].addBox(0F, 0F, 0F, 3, 6, 8, 0F); // Box 11
		defaultStockModel[27].setRotationPoint(-44F, -1F, -4F);

		defaultStockModel[28].addShapeBox(0F, 0F, 0F, 5, 2, 11, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 11
		defaultStockModel[28].setRotationPoint(-44F, -12F, -5.5F);

		defaultStockModel[29].addBox(0F, 0F, 0F, 5, 5, 11, 0F); // Box 11
		defaultStockModel[29].setRotationPoint(-44F, -10F, -5.5F);

		defaultStockModel[30].addShapeBox(0F, 0F, 0F, 5, 2, 11, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 11
		defaultStockModel[30].setRotationPoint(-44F, -5F, -5.5F);

		defaultStockModel[31].addBox(0F, 0F, 0F, 5, 7, 1, 0F); // Box 11
		defaultStockModel[31].setRotationPoint(-29F, -18F, -5F);

		defaultStockModel[32].addShapeBox(0F, 0F, 0F, 5, 3, 1, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 11
		defaultStockModel[32].setRotationPoint(-29F, -21F, -5F);

		defaultStockModel[33].addBox(0F, 0F, 0F, 5, 1, 6, 0F); // Box 11
		defaultStockModel[33].setRotationPoint(-29F, -21F, -3F);

		defaultStockModel[34].addShapeBox(0F, 0F, 0F, 5, 3, 1, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 11
		defaultStockModel[34].setRotationPoint(-29F, -21F, 4F);

		defaultStockModel[35].addBox(0F, 0F, 0F, 5, 7, 1, 0F); // Box 11
		defaultStockModel[35].setRotationPoint(-29F, -18F, 4F);

		defaultStockModel[36].addShapeBox(0F, 0F, 0F, 5, 10, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F, 1F); // Box 11
		defaultStockModel[36].setRotationPoint(-29F, -11F, -5F);

		defaultStockModel[37].addShapeBox(0F, 0F, 0F, 5, 10, 1, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 11
		defaultStockModel[37].setRotationPoint(-29F, -11F, 3F);

		defaultStockModel[38].addShapeBox(0F, 0F, 0F, 5, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 11
		defaultStockModel[38].setRotationPoint(-29F, -1F, -4F);

		defaultStockModel[39].addBox(0F, 0F, 0F, 5, 8, 1, 0F); // Box 11
		defaultStockModel[39].setRotationPoint(-29F, -9F, -6F);

		defaultStockModel[40].addShapeBox(0F, 0F, 0F, 5, 2, 1, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 11
		defaultStockModel[40].setRotationPoint(-29F, -11F, -6F);

		defaultStockModel[41].addShapeBox(0F, 0F, 0F, 5, 6, 9, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 0F, 0F); // Box 11
		defaultStockModel[41].setRotationPoint(-16F, -18F, -4.5F);


		ammoModel = new ModelRendererTurbo[11];
		ammoModel[0] = new ModelRendererTurbo(this, 154, 178, textureX, textureY); // bullet
		ammoModel[1] = new ModelRendererTurbo(this, 104, 176, textureX, textureY); // clip1
		ammoModel[2] = new ModelRendererTurbo(this, 104, 201, textureX, textureY); // Box 73
		ammoModel[3] = new ModelRendererTurbo(this, 143, 201, textureX, textureY); // Box 75
		ammoModel[4] = new ModelRendererTurbo(this, 143, 178, textureX, textureY); // Box 83
		ammoModel[5] = new ModelRendererTurbo(this, 143, 184, textureX, textureY); // Box 92
		ammoModel[6] = new ModelRendererTurbo(this, 161, 176, textureX, textureY); // Box 94
		ammoModel[7] = new ModelRendererTurbo(this, 182, 202, textureX, textureY); // Box 95
		ammoModel[8] = new ModelRendererTurbo(this, 157, 189, textureX, textureY); // Box 96
		ammoModel[9] = new ModelRendererTurbo(this, 150, 189, textureX, textureY); // Box 97
		ammoModel[10] = new ModelRendererTurbo(this, 143, 189, textureX, textureY); // Box 98

		ammoModel[0].addShapeBox(0F, 0F, 0F, 10, 4, 1, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // bullet
		ammoModel[0].setRotationPoint(9F, -17F, 3F);

		ammoModel[1].addBox(0F, 0F, 0F, 10, 6, 18, 0F); // clip1
		ammoModel[1].setRotationPoint(13F, -18F, 4F);

		ammoModel[2].addShapeBox(0F, 0F, 0F, 1, 6, 18, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F); // Box 73
		ammoModel[2].setRotationPoint(8F, -18F, 4F);

		ammoModel[3].addShapeBox(0F, 0F, 0F, 1, 6, 18, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F); // Box 75
		ammoModel[3].setRotationPoint(23F, -18F, 4F);

		ammoModel[4].addShapeBox(0F, 0F, 0F, 4, 4, 1, 0F, 0F, -1F, 0F, 0F, -1.5F, -0.5F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1.5F, -0.5F, 0F, -1F, 0F, 0F, 0F, 0F); // Box 83
		ammoModel[4].setRotationPoint(19F, -17F, 3F);

		ammoModel[5].addBox(0F, 0F, 0F, 14, 3, 1, 0F); // Box 92
		ammoModel[5].setRotationPoint(9F, -16.5F, 22F);

		ammoModel[6].addBox(0F, 0F, 0F, 3, 6, 18, 0F); // Box 94
		ammoModel[6].setRotationPoint(9F, -18F, 4F);

		ammoModel[7].addBox(0F, 0F, 0F, 1, 5, 18, 0F); // Box 95
		ammoModel[7].setRotationPoint(12F, -17.5F, 4F);

		ammoModel[8].addShapeBox(0F, 0F, 0F, 7, 3, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 96
		ammoModel[8].setRotationPoint(10F, -16.5F, 24F);

		ammoModel[9].addShapeBox(0F, 0F, 0F, 2, 3, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F); // Box 97
		ammoModel[9].setRotationPoint(9F, -16.5F, 23F);

		ammoModel[10].addShapeBox(0F, 0F, 0F, 2, 3, 1, 0F, -1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 98
		ammoModel[10].setRotationPoint(15F, -16.5F, 23F);


		slideModel = new ModelRendererTurbo[6];
		slideModel[0] = new ModelRendererTurbo(this, 87, 49, textureX, textureY); // Box 70
		slideModel[1] = new ModelRendererTurbo(this, 87, 49, textureX, textureY); // Box 70
		slideModel[2] = new ModelRendererTurbo(this, 87, 49, textureX, textureY); // Box 70
		slideModel[3] = new ModelRendererTurbo(this, 81, 42, textureX, textureY); // Box 70
		slideModel[4] = new ModelRendererTurbo(this, 81, 28, textureX, textureY); // Box 70
		slideModel[5] = new ModelRendererTurbo(this, 81, 42, textureX, textureY); // Box 70

		slideModel[0].addShapeBox(0F, 0F, 0F, 3, 1, 3, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 70
		slideModel[0].setRotationPoint(18F, -20F, -6.5F);

		slideModel[1].addShapeBox(0F, 0F, 0F, 3, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 70
		slideModel[1].setRotationPoint(18F, -18F, -6.5F);

		slideModel[2].addBox(0F, 0F, 0F, 3, 1, 3, 0F); // Box 70
		slideModel[2].setRotationPoint(18F, -19F, -6.5F);

		slideModel[3].addShapeBox(0F, 0F, 0F, 4, 1, 5, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 70
		slideModel[3].setRotationPoint(17.5F, -20.5F, -11.5F);

		slideModel[4].addBox(0F, 0F, 0F, 4, 2, 5, 0F); // Box 70
		slideModel[4].setRotationPoint(17.5F, -19.5F, -11.5F);

		slideModel[5].addShapeBox(0F, 0F, 0F, 4, 1, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 70
		slideModel[5].setRotationPoint(17.5F, -17.5F, -11.5F);

		stockAttachPoint = new Vector3f(-18F /16F, 11F /16F, 0F /16F);
		scopeAttachPoint = new Vector3f(12F /16F, 20F /16F, 0F /16F);

		gunSlideDistance = 0F;
		animationType = EnumAnimationType.CUSTOM;

		/* ----Start of Reload Block---- */
		/* Reload Name: Amended Side Clip */
		rotateClipHorizontal = 120F;
		rotateClipVertical = 60F;
		translateClip = new Vector3f(0.5F, 0F, 0F);
		/* ----End of Reload Block---- */

		flipAll();

		translateAll(0F, 0F, 0F);
	}
}