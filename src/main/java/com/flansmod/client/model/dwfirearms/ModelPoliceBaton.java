package com.flansmod.client.model.dwfirearms; //Path where the model is located

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelPoliceBaton extends ModelGun
{
	int textureX = 512;
	int textureY = 256;

	public ModelPoliceBaton()
	{
		gunModel = new ModelRendererTurbo[18];
		gunModel[0] = new ModelRendererTurbo(this, 24, 1, textureX, textureY); // Box 22
		gunModel[1] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 23
		gunModel[2] = new ModelRendererTurbo(this, 1, 82, textureX, textureY); // Box 38
		gunModel[3] = new ModelRendererTurbo(this, 24, 82, textureX, textureY); // Box 40
		gunModel[4] = new ModelRendererTurbo(this, 1, 82, textureX, textureY); // Box 0
		gunModel[5] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 1
		gunModel[6] = new ModelRendererTurbo(this, 20, 70, textureX, textureY); // Box 2
		gunModel[7] = new ModelRendererTurbo(this, 1, 70, textureX, textureY); // Box 3
		gunModel[8] = new ModelRendererTurbo(this, 1, 70, textureX, textureY); // Box 4
		gunModel[9] = new ModelRendererTurbo(this, 16, 39, textureX, textureY); // Box 5
		gunModel[10] = new ModelRendererTurbo(this, 16, 39, textureX, textureY); // Box 6
		gunModel[11] = new ModelRendererTurbo(this, 1, 39, textureX, textureY); // Box 7
		gunModel[12] = new ModelRendererTurbo(this, 29, 41, textureX, textureY); // Box 8
		gunModel[13] = new ModelRendererTurbo(this, 29, 41, textureX, textureY); // Box 9
		gunModel[14] = new ModelRendererTurbo(this, 29, 41, textureX, textureY); // Box 10
		gunModel[15] = new ModelRendererTurbo(this, 41, 68, textureX, textureY); // Box 11
		gunModel[16] = new ModelRendererTurbo(this, 41, 68, textureX, textureY); // Box 12
		gunModel[17] = new ModelRendererTurbo(this, 41, 75, textureX, textureY); // Box 13

		gunModel[0].addShapeBox(-8F, -22F, -4F, 2, 29, 8, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F); // Box 22
		gunModel[0].setRotationPoint(9F, 5F, 0F);
		gunModel[0].rotateAngleZ = 0.08726646F;

		gunModel[1].addShapeBox(-11F, -22F, -4F, 3, 29, 8, 0F, 0F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2.5F); // Box 23
		gunModel[1].setRotationPoint(9F, 5F, 0F);
		gunModel[1].rotateAngleZ = 0.08726646F;

		gunModel[2].addShapeBox(-11F, 7F, -4F, 3, 1, 8, 0F, 0F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2.5F, -1F, 0F, -3F, 0F, 0F, -1F, 0F, 0F, -1F, -1F, 0F, -3F); // Box 38
		gunModel[2].setRotationPoint(9F, 5F, 0F);
		gunModel[2].rotateAngleZ = 0.08726646F;

		gunModel[3].addShapeBox(-8F, 7F, -4F, 3, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 40
		gunModel[3].setRotationPoint(9F, 5F, 0F);
		gunModel[3].rotateAngleZ = 0.08726646F;

		gunModel[4].addShapeBox(-5F, 7F, -4F, 3, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, -1F, -1F, 0F, -3F, -1F, 0F, -3F, 0F, 0F, -1F); // Box 0
		gunModel[4].setRotationPoint(9F, 5F, 0F);
		gunModel[4].rotateAngleZ = 0.08726646F;

		gunModel[5].addShapeBox(-5F, -22F, -4F, 3, 29, 8, 0F, 0F, 0F, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, 0F); // Box 1
		gunModel[5].setRotationPoint(9F, 5F, 0F);
		gunModel[5].rotateAngleZ = 0.08726646F;

		gunModel[6].addBox(-8F, -26F, -3.5F, 3, 4, 7, 0F); // Box 2
		gunModel[6].setRotationPoint(9F, 5F, 0F);
		gunModel[6].rotateAngleZ = 0.08726646F;

		gunModel[7].addShapeBox(-10F, -26F, -3.5F, 2, 4, 7, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F); // Box 3
		gunModel[7].setRotationPoint(9F, 5F, 0F);
		gunModel[7].rotateAngleZ = 0.08726646F;

		gunModel[8].addShapeBox(-5F, -26F, -3.5F, 2, 4, 7, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F); // Box 4
		gunModel[8].setRotationPoint(9F, 5F, 0F);
		gunModel[8].rotateAngleZ = 0.08726646F;

		gunModel[9].addShapeBox(-8.5F, -51F, -2.5F, 1, 25, 5, 0F, 0.5F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, -1.5F, 0.5F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, -1.5F); // Box 5
		gunModel[9].setRotationPoint(9F, 5F, 0F);
		gunModel[9].rotateAngleZ = 0.08726646F;

		gunModel[10].addShapeBox(-5.5F, -51F, -2.5F, 1, 25, 5, 0F, 0F, 0F, 0F, 0.5F, 0F, -1.5F, 0.5F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, -1.5F, 0.5F, 0F, -1.5F, 0F, 0F, 0F); // Box 6
		gunModel[10].setRotationPoint(9F, 5F, 0F);
		gunModel[10].rotateAngleZ = 0.08726646F;

		gunModel[11].addBox(-7.5F, -51F, -2.5F, 2, 25, 5, 0F); // Box 7
		gunModel[11].setRotationPoint(9F, 5F, 0F);
		gunModel[11].rotateAngleZ = 0.08726646F;

		gunModel[12].addBox(-7F, -76F, -1.5F, 1, 25, 3, 0F); // Box 8
		gunModel[12].setRotationPoint(9F, 5F, 0F);
		gunModel[12].rotateAngleZ = 0.08726646F;

		gunModel[13].addShapeBox(-6F, -76F, -1.5F, 1, 25, 3, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 9
		gunModel[13].setRotationPoint(9F, 5F, 0F);
		gunModel[13].rotateAngleZ = 0.08726646F;

		gunModel[14].addShapeBox(-8F, -76F, -1.5F, 1, 25, 3, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 10
		gunModel[14].setRotationPoint(9F, 5F, 0F);
		gunModel[14].rotateAngleZ = 0.08726646F;

		gunModel[15].addShapeBox(-8.5F, -78F, -2F, 1, 2, 4, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 11
		gunModel[15].setRotationPoint(9F, 5F, 0F);
		gunModel[15].rotateAngleZ = 0.08726646F;

		gunModel[16].addShapeBox(-5.5F, -78F, -2F, 1, 2, 4, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 12
		gunModel[16].setRotationPoint(9F, 5F, 0F);
		gunModel[16].rotateAngleZ = 0.08726646F;

		gunModel[17].addBox(-7.5F, -78F, -2F, 2, 2, 4, 0F); // Box 13
		gunModel[17].setRotationPoint(9F, 5F, 0F);
		gunModel[17].rotateAngleZ = 0.08726646F;

		translateAll(0F, 0F, 0F);


		flipAll();
	}
}