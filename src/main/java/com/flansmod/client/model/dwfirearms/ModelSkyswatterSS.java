package com.flansmod.client.model.dwfirearms; //Path where the model is located

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelSkyswatterSS extends ModelGun //Same as Filename
{
	int textureX = 512;
	int textureY = 256;

	public ModelSkyswatterSS() //Same as Filename
	{
		gunModel = new ModelRendererTurbo[91];
		gunModel[0] = new ModelRendererTurbo(this, 104, 164, textureX, textureY); // Box 36
		gunModel[1] = new ModelRendererTurbo(this, 104, 164, textureX, textureY); // Box 36
		gunModel[2] = new ModelRendererTurbo(this, 104, 164, textureX, textureY); // Box 36
		gunModel[3] = new ModelRendererTurbo(this, 104, 164, textureX, textureY); // Box 36
		gunModel[4] = new ModelRendererTurbo(this, 104, 144, textureX, textureY); // Box 36
		gunModel[5] = new ModelRendererTurbo(this, 104, 144, textureX, textureY); // Box 36
		gunModel[6] = new ModelRendererTurbo(this, 104, 144, textureX, textureY); // Box 36
		gunModel[7] = new ModelRendererTurbo(this, 104, 144, textureX, textureY); // Box 36
		gunModel[8] = new ModelRendererTurbo(this, 104, 121, textureX, textureY); // Box 36
		gunModel[9] = new ModelRendererTurbo(this, 104, 121, textureX, textureY); // Box 36
		gunModel[10] = new ModelRendererTurbo(this, 199, 123, textureX, textureY); // Box 36
		gunModel[11] = new ModelRendererTurbo(this, 234, 123, textureX, textureY); // Box 36
		gunModel[12] = new ModelRendererTurbo(this, 234, 114, textureX, textureY); // Box 36
		gunModel[13] = new ModelRendererTurbo(this, 199, 114, textureX, textureY); // Box 36
		gunModel[14] = new ModelRendererTurbo(this, 54, 76, textureX, textureY); // Box 36
		gunModel[15] = new ModelRendererTurbo(this, 147, 164, textureX, textureY); // Box 36
		gunModel[16] = new ModelRendererTurbo(this, 147, 164, textureX, textureY); // Box 36
		gunModel[17] = new ModelRendererTurbo(this, 147, 144, textureX, textureY); // Box 36
		gunModel[18] = new ModelRendererTurbo(this, 147, 144, textureX, textureY); // Box 36
		gunModel[19] = new ModelRendererTurbo(this, 147, 121, textureX, textureY); // Box 36
		gunModel[20] = new ModelRendererTurbo(this, 32, 87, textureX, textureY); // Box 36
		gunModel[21] = new ModelRendererTurbo(this, 32, 87, textureX, textureY); // Box 36
		gunModel[22] = new ModelRendererTurbo(this, 32, 87, textureX, textureY); // Box 36
		gunModel[23] = new ModelRendererTurbo(this, 291, 20, textureX, textureY); // Box 36
		gunModel[24] = new ModelRendererTurbo(this, 305, 72, textureX, textureY); // Box 36
		gunModel[25] = new ModelRendererTurbo(this, 104, 38, textureX, textureY); // Box 36
		gunModel[26] = new ModelRendererTurbo(this, 104, 20, textureX, textureY); // Box 36
		gunModel[27] = new ModelRendererTurbo(this, 104, 38, textureX, textureY); // Box 36
		gunModel[28] = new ModelRendererTurbo(this, 104, 1, textureX, textureY); // Box 36
		gunModel[29] = new ModelRendererTurbo(this, 104, 20, textureX, textureY); // Box 36
		gunModel[30] = new ModelRendererTurbo(this, 1, 116, textureX, textureY); // Box 36
		gunModel[31] = new ModelRendererTurbo(this, 24, 116, textureX, textureY); // Box 36
		gunModel[32] = new ModelRendererTurbo(this, 1, 96, textureX, textureY); // Box 36
		gunModel[33] = new ModelRendererTurbo(this, 1, 85, textureX, textureY); // Box 36
		gunModel[34] = new ModelRendererTurbo(this, 1, 106, textureX, textureY); // Box 36
		gunModel[35] = new ModelRendererTurbo(this, 28, 76, textureX, textureY); // Box 36
		gunModel[36] = new ModelRendererTurbo(this, 1, 76, textureX, textureY); // Box 36
		gunModel[37] = new ModelRendererTurbo(this, 237, 14, textureX, textureY); // Box 36
		gunModel[38] = new ModelRendererTurbo(this, 237, 8, textureX, textureY); // Box 36
		gunModel[39] = new ModelRendererTurbo(this, 237, 1, textureX, textureY); // Box 36
		gunModel[40] = new ModelRendererTurbo(this, 54, 99, textureX, textureY); // Box 36
		gunModel[41] = new ModelRendererTurbo(this, 54, 88, textureX, textureY); // Box 36
		gunModel[42] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 36
		gunModel[43] = new ModelRendererTurbo(this, 1, 17, textureX, textureY); // Box 36
		gunModel[44] = new ModelRendererTurbo(this, 1, 45, textureX, textureY); // Box 36
		gunModel[45] = new ModelRendererTurbo(this, 1, 65, textureX, textureY); // Box 36
		gunModel[46] = new ModelRendererTurbo(this, 38, 17, textureX, textureY); // Box 36
		gunModel[47] = new ModelRendererTurbo(this, 38, 45, textureX, textureY); // Box 36
		gunModel[48] = new ModelRendererTurbo(this, 199, 159, textureX, textureY); // Box 73
		gunModel[49] = new ModelRendererTurbo(this, 51, 161, textureX, textureY); // Box 74
		gunModel[50] = new ModelRendererTurbo(this, 28, 158, textureX, textureY); // Box 75
		gunModel[51] = new ModelRendererTurbo(this, 1, 146, textureX, textureY); // Box 76
		gunModel[52] = new ModelRendererTurbo(this, 1, 132, textureX, textureY); // Box 77
		gunModel[53] = new ModelRendererTurbo(this, 1, 157, textureX, textureY); // Box 78
		gunModel[54] = new ModelRendererTurbo(this, 199, 134, textureX, textureY); // Box 79
		gunModel[55] = new ModelRendererTurbo(this, 36, 65, textureX, textureY); // Box 80
		gunModel[56] = new ModelRendererTurbo(this, 220, 170, textureX, textureY); // Box 81
		gunModel[57] = new ModelRendererTurbo(this, 228, 134, textureX, textureY); // Box 82
		gunModel[58] = new ModelRendererTurbo(this, 232, 159, textureX, textureY); // Box 83
		gunModel[59] = new ModelRendererTurbo(this, 199, 170, textureX, textureY); // Box 84
		gunModel[60] = new ModelRendererTurbo(this, 65, 30, textureX, textureY); // Box 85
		gunModel[61] = new ModelRendererTurbo(this, 286, 1, textureX, textureY); // Box 0
		gunModel[62] = new ModelRendererTurbo(this, 317, 2, textureX, textureY); // Box 1
		gunModel[63] = new ModelRendererTurbo(this, 348, 7, textureX, textureY); // Box 2
		gunModel[64] = new ModelRendererTurbo(this, 317, 2, textureX, textureY); // Box 3
		gunModel[65] = new ModelRendererTurbo(this, 348, 7, textureX, textureY); // Box 4
		gunModel[66] = new ModelRendererTurbo(this, 247, 138, textureX, textureY); // Box 5
		gunModel[67] = new ModelRendererTurbo(this, 284, 145, textureX, textureY); // Box 7
		gunModel[68] = new ModelRendererTurbo(this, 284, 145, textureX, textureY); // Box 10
		gunModel[69] = new ModelRendererTurbo(this, 323, 144, textureX, textureY); // Box 11
		gunModel[70] = new ModelRendererTurbo(this, 323, 144, textureX, textureY); // Box 12
		gunModel[71] = new ModelRendererTurbo(this, 257, 110, textureX, textureY); // Box 13
		gunModel[72] = new ModelRendererTurbo(this, 257, 110, textureX, textureY); // Box 14
		gunModel[73] = new ModelRendererTurbo(this, 257, 121, textureX, textureY); // Box 15
		gunModel[74] = new ModelRendererTurbo(this, 43, 76, textureX, textureY); // Box 16
		gunModel[75] = new ModelRendererTurbo(this, 70, 1, textureX, textureY); // Box 17
		gunModel[76] = new ModelRendererTurbo(this, 291, 39, textureX, textureY); // Box 0
		gunModel[77] = new ModelRendererTurbo(this, 208, 20, textureX, textureY); // Box 1
		gunModel[78] = new ModelRendererTurbo(this, 208, 39, textureX, textureY); // Box 2
		gunModel[79] = new ModelRendererTurbo(this, 234, 91, textureX, textureY); // Box 3
		gunModel[80] = new ModelRendererTurbo(this, 343, 110, textureX, textureY); // Box 4
		gunModel[81] = new ModelRendererTurbo(this, 306, 114, textureX, textureY); // Box 5
		gunModel[82] = new ModelRendererTurbo(this, 208, 68, textureX, textureY); // Box 6
		gunModel[83] = new ModelRendererTurbo(this, 104, 88, textureX, textureY); // Box 7
		gunModel[84] = new ModelRendererTurbo(this, 104, 70, textureX, textureY); // Box 8
		gunModel[85] = new ModelRendererTurbo(this, 104, 51, textureX, textureY); // Box 9
		gunModel[86] = new ModelRendererTurbo(this, 132, 121, textureX, textureY); // Box 10
		gunModel[87] = new ModelRendererTurbo(this, 143, 101, textureX, textureY); // Box 11
		gunModel[88] = new ModelRendererTurbo(this, 104, 101, textureX, textureY); // Box 12
		gunModel[89] = new ModelRendererTurbo(this, 59, 45, textureX, textureY); // Box 15
		gunModel[90] = new ModelRendererTurbo(this, 65, 62, textureX, textureY); // Box 16

		gunModel[0].addShapeBox(0F, 0F, 0F, 5, 2, 12, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 36
		gunModel[0].setRotationPoint(47F, -30F, -7F);

		gunModel[1].addShapeBox(0F, 0F, 0F, 5, 2, 12, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 36
		gunModel[1].setRotationPoint(22F, -16F, -7F);

		gunModel[2].addShapeBox(0F, 0F, 0F, 5, 2, 12, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 36
		gunModel[2].setRotationPoint(22F, -30F, -7F);

		gunModel[3].addShapeBox(0F, 0F, 0F, 5, 2, 12, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 36
		gunModel[3].setRotationPoint(47F, -16F, -7F);

		gunModel[4].addShapeBox(0F, 0F, 0F, 5, 3, 16, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 36
		gunModel[4].setRotationPoint(22F, -19F, -9F);

		gunModel[5].addShapeBox(0F, 0F, 0F, 5, 3, 16, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 36
		gunModel[5].setRotationPoint(22F, -28F, -9F);

		gunModel[6].addShapeBox(0F, 0F, 0F, 5, 3, 16, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 36
		gunModel[6].setRotationPoint(47F, -28F, -9F);

		gunModel[7].addShapeBox(0F, 0F, 0F, 5, 3, 16, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 36
		gunModel[7].setRotationPoint(47F, -19F, -9F);

		gunModel[8].addBox(0F, 0F, 0F, 5, 6, 16, 0F); // Box 36
		gunModel[8].setRotationPoint(47F, -25F, -9F);

		gunModel[9].addBox(0F, 0F, 0F, 5, 6, 16, 0F); // Box 36
		gunModel[9].setRotationPoint(22F, -25F, -9F);

		gunModel[10].addBox(0F, 0F, 0F, 5, 3, 6, 0F); // Box 36
		gunModel[10].setRotationPoint(22F, -14F, -4F);

		gunModel[11].addBox(0F, 0F, 0F, 5, 3, 6, 0F); // Box 36
		gunModel[11].setRotationPoint(47F, -14F, -4F);

		gunModel[12].addShapeBox(0F, 0F, 0F, 5, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 36
		gunModel[12].setRotationPoint(47F, -11F, -4F);

		gunModel[13].addShapeBox(0F, 0F, 0F, 5, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 36
		gunModel[13].setRotationPoint(22F, -11F, -4F);

		gunModel[14].addBox(0F, 0F, 0F, 15, 3, 8, 0F); // Box 36
		gunModel[14].setRotationPoint(-4F, -30.5F, -5F);

		gunModel[15].addShapeBox(0F, 0F, 0F, 5, 2, 12, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 36
		gunModel[15].setRotationPoint(-73F, -30F, -7F);

		gunModel[16].addShapeBox(0F, 0F, 0F, 5, 2, 12, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 36
		gunModel[16].setRotationPoint(-73F, -16F, -7F);

		gunModel[17].addShapeBox(0F, 0F, 0F, 5, 3, 16, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 36
		gunModel[17].setRotationPoint(-73F, -28F, -9F);

		gunModel[18].addShapeBox(0F, 0F, 0F, 5, 3, 16, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 36
		gunModel[18].setRotationPoint(-73F, -19F, -9F);

		gunModel[19].addBox(0F, 0F, 0F, 5, 6, 16, 0F); // Box 36
		gunModel[19].setRotationPoint(-73F, -25F, -9F);

		gunModel[20].addShapeBox(0F, 0F, 0F, 3, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 36
		gunModel[20].setRotationPoint(7F, -32.5F, 7.5F);

		gunModel[21].addBox(0F, 0F, 0F, 3, 2, 6, 0F); // Box 36
		gunModel[21].setRotationPoint(7F, -30.5F, 7.5F);

		gunModel[22].addShapeBox(0F, 0F, 0F, 3, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 36
		gunModel[22].setRotationPoint(7F, -28.5F, 7.5F);

		gunModel[23].addShapeBox(0F, 0F, 0F, 28, 2, 16, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 36
		gunModel[23].setRotationPoint(-11F, -30F, -9F);

		gunModel[24].addShapeBox(0F, 0F, 0F, 28, 2, 16, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 36
		gunModel[24].setRotationPoint(-11F, -16F, -9F);

		gunModel[25].addShapeBox(0F, 0F, 0F, 36, 2, 10, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 36
		gunModel[25].setRotationPoint(17F, -29F, -6F);

		gunModel[26].addShapeBox(0F, 0F, 0F, 36, 3, 14, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 36
		gunModel[26].setRotationPoint(17F, -27F, -8F);

		gunModel[27].addShapeBox(0F, 0F, 0F, 36, 2, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 36
		gunModel[27].setRotationPoint(17F, -17F, -6F);

		gunModel[28].addBox(0F, 0F, 0F, 36, 4, 14, 0F); // Box 36
		gunModel[28].setRotationPoint(17F, -24F, -8F);

		gunModel[29].addShapeBox(0F, 0F, 0F, 36, 3, 14, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 36
		gunModel[29].setRotationPoint(17F, -20F, -8F);

		gunModel[30].addBox(0F, 0F, 0F, 4, 8, 7, 0F); // Box 36
		gunModel[30].setRotationPoint(3F, -31.5F, 7F);

		gunModel[31].addShapeBox(0F, 0F, 0F, 4, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 36
		gunModel[31].setRotationPoint(3F, -33.5F, 7F);

		gunModel[32].addShapeBox(0F, 0F, 0F, 12, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 36
		gunModel[32].setRotationPoint(-5F, -23.5F, 7F);

		gunModel[33].addBox(0F, 0F, 0F, 8, 3, 7, 0F); // Box 36
		gunModel[33].setRotationPoint(-5F, -26.5F, 7F);

		gunModel[34].addShapeBox(0F, 0F, 0F, 8, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 36
		gunModel[34].setRotationPoint(-5F, -28.5F, 7F);

		gunModel[35].addShapeBox(0F, 0F, 0F, 4, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 36
		gunModel[35].setRotationPoint(3F, -33.5F, 3F);

		gunModel[36].addBox(0F, 0F, 0F, 10, 5, 2, 0F); // Box 36
		gunModel[36].setRotationPoint(-4F, -24F, 7F);

		gunModel[37].addShapeBox(0F, 0F, 0F, 20, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 36
		gunModel[37].setRotationPoint(27F, -11F, -3F);

		gunModel[38].addShapeBox(0F, 0F, 0F, 20, 1, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 36
		gunModel[38].setRotationPoint(27F, -14F, -3F);

		gunModel[39].addBox(0F, 0F, 0F, 20, 2, 4, 0F); // Box 36
		gunModel[39].setRotationPoint(27F, -13F, -3F);

		gunModel[40].addBox(0F, 0F, 0F, 16, 3, 7, 0F); // Box 36
		gunModel[40].setRotationPoint(-26F, -14F, -4.5F);

		gunModel[41].addBox(0F, 0F, 0F, 16, 2, 8, 0F); // Box 36
		gunModel[41].setRotationPoint(-26F, -11F, -5F);

		gunModel[42].addBox(0F, 0F, 0F, 26, 7, 8, 0F); // Box 36
		gunModel[42].setRotationPoint(-10F, -15F, -5F);

		gunModel[43].addShapeBox(0F, 0F, 0F, 10, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 36
		gunModel[43].setRotationPoint(-10F, -8F, -5F);

		gunModel[44].addShapeBox(0F, 0F, 0F, 10, 11, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 6F, 0F, 0F, -6F, 0F, 0F, -6F, 0F, 0F, 6F, 0F, 0F); // Box 36
		gunModel[44].setRotationPoint(-10F, -6F, -5F);

		gunModel[45].addShapeBox(0F, 0F, 0F, 9, 2, 8, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F); // Box 36
		gunModel[45].setRotationPoint(-17F, 7F, -5F);

		gunModel[46].addShapeBox(0F, 0F, 0F, 2, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F); // Box 36
		gunModel[46].setRotationPoint(0F, -8F, -5F);

		gunModel[47].addShapeBox(0F, 0F, 0F, 2, 11, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 6F, 0F, 0F, -6F, 0F, -1.5F, -6F, 0F, -1.5F, 6F, 0F, 0F); // Box 36
		gunModel[47].setRotationPoint(0F, -6F, -5F);

		gunModel[48].addShapeBox(0F, 0F, 0F, 8, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F); // Box 73
		gunModel[48].setRotationPoint(13F, -8F, -5F);

		gunModel[49].addShapeBox(0F, 0F, 0F, 4, 2, 8, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F); // Box 74
		gunModel[49].setRotationPoint(-30F, -8F, -5F);

		gunModel[50].addShapeBox(0F, 0F, 0F, 4, 6, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F); // Box 75
		gunModel[50].setRotationPoint(-30F, -14F, -4.5F);

		gunModel[51].addBox(0F, 0F, 0F, 38, 2, 8, 0F); // Box 76
		gunModel[51].setRotationPoint(-68F, -8F, -5F);

		gunModel[52].addBox(0F, 0F, 0F, 33, 6, 7, 0F); // Box 77
		gunModel[52].setRotationPoint(-63F, -14F, -4.5F);

		gunModel[53].addBox(0F, 0F, 0F, 5, 6, 8, 0F); // Box 78
		gunModel[53].setRotationPoint(-68F, -14F, -5F);

		gunModel[54].addBox(0F, 0F, 0F, 6, 16, 8, 0F); // Box 79
		gunModel[54].setRotationPoint(13F, -6F, -5F);

		gunModel[55].addShapeBox(0F, 0F, 0F, 6, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F); // Box 80
		gunModel[55].setRotationPoint(13F, 10F, -5F);

		gunModel[56].addShapeBox(0F, 0F, 0F, 1, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, -1F, -2F, 0F, -1F, 2F, 0F, 0F); // Box 81
		gunModel[56].setRotationPoint(19F, 10F, -5F);

		gunModel[57].addShapeBox(0F, 0F, 0F, 1, 16, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 82
		gunModel[57].setRotationPoint(19F, -6F, -5F);

		gunModel[58].addShapeBox(0F, 0F, 0F, 1, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, -1F, -2F, 0F, -1F, 2F, 0F, 0F); // Box 83
		gunModel[58].setRotationPoint(21F, -8F, -5F);

		gunModel[59].addShapeBox(0F, 0F, 0F, 2, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F); // Box 84
		gunModel[59].setRotationPoint(-8F, 10F, -5F);

		gunModel[60].addBox(0F, 0F, 0F, 6, 6, 8, 0F); // Box 85
		gunModel[60].setRotationPoint(16F, -14F, -5F);

		gunModel[61].addBox(0F, 0F, 0F, 1, 4, 14, 0F); // Box 0
		gunModel[61].setRotationPoint(-74F, -24F, -8F);

		gunModel[62].addShapeBox(0F, 0F, 0F, 1, 3, 14, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 1
		gunModel[62].setRotationPoint(-74F, -20F, -8F);

		gunModel[63].addShapeBox(0F, 0F, 0F, 1, 2, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 2
		gunModel[63].setRotationPoint(-74F, -17F, -6F);

		gunModel[64].addShapeBox(0F, 0F, 0F, 1, 3, 14, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 3
		gunModel[64].setRotationPoint(-74F, -27F, -8F);

		gunModel[65].addShapeBox(0F, 0F, 0F, 1, 2, 10, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 4
		gunModel[65].setRotationPoint(-74F, -29F, -6F);

		gunModel[66].addBox(0F, 0F, 0F, 8, 10, 10, 0F); // Box 5
		gunModel[66].setRotationPoint(-63F, -16.5F, -6F);

		gunModel[67].addBox(-3.5F, -1.5F, 0F, 7, 1, 12, 0F); // Box 7
		gunModel[67].setRotationPoint(-59F, -10.5F, -7F);
		gunModel[67].rotateAngleZ = 0.78539816F;

		gunModel[68].addBox(-3.5F, 0.5F, 0F, 7, 1, 12, 0F); // Box 10
		gunModel[68].setRotationPoint(-59F, -10.5F, -7F);
		gunModel[68].rotateAngleZ = 0.78539816F;

		gunModel[69].addShapeBox(-3.5F, -3.5F, 0F, 7, 2, 12, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 11
		gunModel[69].setRotationPoint(-59F, -10.5F, -7F);
		gunModel[69].rotateAngleZ = 0.78539816F;

		gunModel[70].addShapeBox(-3.5F, 1.5F, 0F, 7, 2, 12, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F); // Box 12
		gunModel[70].setRotationPoint(-59F, -10.5F, -7F);
		gunModel[70].rotateAngleZ = 0.78539816F;

		gunModel[71].addShapeBox(0F, 0F, 0F, 15, 1, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 13
		gunModel[71].setRotationPoint(-4F, -31.5F, -6F);

		gunModel[72].addShapeBox(0F, 0F, 0F, 15, 1, 9, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 14
		gunModel[72].setRotationPoint(-4F, -34.5F, -6F);

		gunModel[73].addBox(0F, 0F, 0F, 15, 2, 9, 0F); // Box 15
		gunModel[73].setRotationPoint(-4F, -33.5F, -6F);

		gunModel[74].addBox(0F, 0F, 0F, 6, 2, 1, 0F); // Box 16
		gunModel[74].setRotationPoint(-3F, -33.5F, 3F);

		gunModel[75].addShapeBox(0F, 0F, 0F, 1, 20, 8, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 17
		gunModel[75].setRotationPoint(12F, -8F, -5F);

		gunModel[76].addBox(0F, 0F, 0F, 28, 12, 16, 0F); // Box 0
		gunModel[76].setRotationPoint(-11F, -28F, -9F);

		gunModel[77].addShapeBox(0F, 0F, 0F, 25, 2, 16, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 1
		gunModel[77].setRotationPoint(-68F, -30F, -9F);

		gunModel[78].addBox(0F, 0F, 0F, 25, 12, 16, 0F); // Box 2
		gunModel[78].setRotationPoint(-68F, -28F, -9F);

		gunModel[79].addShapeBox(0F, 0F, 0F, 57, 2, 16, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 3
		gunModel[79].setRotationPoint(-68F, -16F, -9F);

		gunModel[80].addShapeBox(0F, 0F, 0F, 5, 6, 16, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 4
		gunModel[80].setRotationPoint(-43F, -28F, -9F);

		gunModel[81].addShapeBox(0F, 0F, 0F, 2, 2, 16, 0F, 0F, 0F, -2F, -1F, 0F, -2F, -1F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 5
		gunModel[81].setRotationPoint(-43F, -30F, -9F);

		gunModel[82].addBox(0F, 0F, 0F, 32, 6, 16, 0F); // Box 6
		gunModel[82].setRotationPoint(-43F, -22F, -9F);

		gunModel[83].addShapeBox(0F, 0F, 0F, 28, 2, 10, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 7
		gunModel[83].setRotationPoint(-42F, -29F, -6F);

		gunModel[84].addShapeBox(0F, 0F, 0F, 28, 3, 14, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 8
		gunModel[84].setRotationPoint(-42F, -27F, -8F);

		gunModel[85].addBox(0F, 0F, 0F, 28, 4, 14, 0F); // Box 9
		gunModel[85].setRotationPoint(-42F, -24F, -8F);

		gunModel[86].addShapeBox(0F, 0F, 0F, 3, 2, 12, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 10
		gunModel[86].setRotationPoint(-14F, -30F, -7F);

		gunModel[87].addShapeBox(0F, 0F, 0F, 3, 3, 16, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 11
		gunModel[87].setRotationPoint(-14F, -28F, -9F);

		gunModel[88].addBox(0F, 0F, 0F, 3, 3, 16, 0F); // Box 12
		gunModel[88].setRotationPoint(-14F, -25F, -9F);

		gunModel[89].addShapeBox(0F, 0F, 0F, 10, 5, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, -3F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 1F, -3F, 0F); // Box 15
		gunModel[89].setRotationPoint(-16F, 5F, -5F);

		gunModel[90].addShapeBox(0F, 0F, 0F, 2, 5, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, -1.5F, -2F, 0F, -1.5F, 2F, 0F, 0F); // Box 16
		gunModel[90].setRotationPoint(-6F, 5F, -5F);

		animationType = EnumAnimationType.GENERIC;

		translateAll(0F, 0F, 0F);

		flipAll();
	}
}