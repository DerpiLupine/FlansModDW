package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.ModelAttachment;
import com.flansmod.client.tmt.ModelRendererTurbo;

import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelABTRoundFrontrail extends ModelAttachment
{
	int textureX = 512;
	int textureY = 256;

	public ModelABTRoundFrontrail()
	{
		attachmentModel = new ModelRendererTurbo[13];
		attachmentModel[0] = new ModelRendererTurbo(this, 1, 166, textureX, textureY); // Box 57
		attachmentModel[1] = new ModelRendererTurbo(this, 1, 166, textureX, textureY); // Box 58
		attachmentModel[2] = new ModelRendererTurbo(this, 1, 184, textureX, textureY); // Box 59
		attachmentModel[3] = new ModelRendererTurbo(this, 1, 166, textureX, textureY); // Box 60
		attachmentModel[4] = new ModelRendererTurbo(this, 1, 184, textureX, textureY); // Box 61
		attachmentModel[5] = new ModelRendererTurbo(this, 1, 235, textureX, textureY); // grip3
		attachmentModel[6] = new ModelRendererTurbo(this, 1, 197, textureX, textureY); // grip2
		attachmentModel[7] = new ModelRendererTurbo(this, 1, 216, textureX, textureY); // grip
		attachmentModel[8] = new ModelRendererTurbo(this, 70, 184, textureX, textureY); // Box 65
		attachmentModel[9] = new ModelRendererTurbo(this, 70, 168, textureX, textureY); // Box 66
		attachmentModel[10] = new ModelRendererTurbo(this, 70, 168, textureX, textureY); // Box 67
		attachmentModel[11] = new ModelRendererTurbo(this, 70, 168, textureX, textureY); // Box 68
		attachmentModel[12] = new ModelRendererTurbo(this, 70, 184, textureX, textureY); // Box 69

		attachmentModel[0].addShapeBox(0F, -4.5F, -7F, 20, 3, 14, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 57
		attachmentModel[0].setRotationPoint(0F, 0F, 0F);

		attachmentModel[1].addBox(0F, -1.5F, -7F, 20, 3, 14, 0F); // Box 58
		attachmentModel[1].setRotationPoint(0F, 0F, 0F);

		attachmentModel[2].addShapeBox(0F, -6.5F, -5F, 20, 2, 10, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 59
		attachmentModel[2].setRotationPoint(0F, 0F, 0F);

		attachmentModel[3].addShapeBox(0F, 1.5F, -7F, 20, 3, 14, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 60
		attachmentModel[3].setRotationPoint(0F, 0F, 0F);

		attachmentModel[4].addShapeBox(0F, 4.5F, -5F, 20, 2, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F); // Box 61
		attachmentModel[4].setRotationPoint(0F, 0F, 0F);

		attachmentModel[5].addShapeBox(1F, 5F, -5.5F, 18, 2, 11, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F); // grip3
		attachmentModel[5].setRotationPoint(0F, 0F, 0F);

		attachmentModel[6].addShapeBox(1F, 2F, -7.5F, 18, 3, 15, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // grip2
		attachmentModel[6].setRotationPoint(0F, 0F, 0F);

		attachmentModel[7].addBox(1F, -1F, -7.5F, 18, 3, 15, 0F); // grip
		attachmentModel[7].setRotationPoint(0F, 0F, 0F);

		attachmentModel[8].addShapeBox(20F, -5.5F, -4F, 3, 1, 8, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 65
		attachmentModel[8].setRotationPoint(0F, 0F, 0F);

		attachmentModel[9].addShapeBox(20F, -4.5F, -6F, 3, 3, 12, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 66
		attachmentModel[9].setRotationPoint(0F, 0F, 0F);

		attachmentModel[10].addBox(20F, -1.5F, -6F, 3, 3, 12, 0F); // Box 67
		attachmentModel[10].setRotationPoint(0F, 0F, 0F);

		attachmentModel[11].addShapeBox(20F, 1.5F, -6F, 3, 3, 12, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 68
		attachmentModel[11].setRotationPoint(0F, 0F, 0F);

		attachmentModel[12].addShapeBox(20F, 4.5F, -4F, 3, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F); // Box 69
		attachmentModel[12].setRotationPoint(0F, 0F, 0F);

		renderOffset = 0F;

		//Based off the Ambircon SteamRifle positioning.

		flipAll();
	}
}

