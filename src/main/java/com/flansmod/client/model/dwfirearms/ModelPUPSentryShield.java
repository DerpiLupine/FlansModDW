package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.ModelAAGun;
import com.flansmod.client.tmt.ModelRendererTurbo;

public class ModelPUPSentryShield extends ModelAAGun 
{
	public ModelPUPSentryShield()
    {
		int textureX = 256;
		int textureY = 128;
		
        baseModel = new ModelRendererTurbo[16];
		baseModel[0] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // stand
		baseModel[1] = new ModelRendererTurbo(this, 1, 8, textureX, textureY); // stump
		baseModel[2] = new ModelRendererTurbo(this, 1, 8, textureX, textureY); // stump
		baseModel[3] = new ModelRendererTurbo(this, 1, 8, textureX, textureY); // stump
		baseModel[4] = new ModelRendererTurbo(this, 1, 8, textureX, textureY); // stump
		baseModel[5] = new ModelRendererTurbo(this, 12, 17, textureX, textureY); // shield1
		baseModel[6] = new ModelRendererTurbo(this, 27, 19, textureX, textureY); // shield2
		baseModel[7] = new ModelRendererTurbo(this, 1, 19, textureX, textureY); // shield3
		baseModel[8] = new ModelRendererTurbo(this, 1, 38, textureX, textureY); // Box 4
		baseModel[9] = new ModelRendererTurbo(this, 1, 61, textureX, textureY); // Box 6
		baseModel[10] = new ModelRendererTurbo(this, 36, 52, textureX, textureY); // Box 7
		baseModel[11] = new ModelRendererTurbo(this, 36, 68, textureX, textureY); // Box 8
		baseModel[12] = new ModelRendererTurbo(this, 34, 45, textureX, textureY); // platingBack
		baseModel[13] = new ModelRendererTurbo(this, 34, 45, textureX, textureY); // platingBack
		baseModel[14] = new ModelRendererTurbo(this, 39, 52, textureX, textureY); // stabiliser
		baseModel[15] = new ModelRendererTurbo(this, 39, 52, textureX, textureY); // stabiliser

		baseModel[0].addBox(-5F, -2.5F, -7F, 12, 1, 14, 0F); // stand
		baseModel[0].setRotationPoint(0F, 0F, 0F);

		baseModel[1].addBox(5F, -3F, 5F, 3, 3, 3, 0F); // stump
		baseModel[1].setRotationPoint(0F, 0F, 0F);

		baseModel[2].addBox(-6F, -3F, -8F, 3, 3, 3, 0F); // stump
		baseModel[2].setRotationPoint(0F, 0F, 0F);

		baseModel[3].addBox(-6F, -3F, 5F, 3, 3, 3, 0F); // stump
		baseModel[3].setRotationPoint(0F, 0F, 0F);

		baseModel[4].addBox(5F, -3F, -8F, 3, 3, 3, 0F); // stump
		baseModel[4].setRotationPoint(0F, 0F, 0F);

		baseModel[5].addBox(5F, -24F, -3F, 1, 14, 6, 0F); // shield1
		baseModel[5].setRotationPoint(0F, 0F, 0F);

		baseModel[6].addShapeBox(5F, -24F, 3F, 1, 14, 4, 0F,0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F); // shield2
		baseModel[6].setRotationPoint(0F, 0F, 0F);

		baseModel[7].addShapeBox(3F, -24F, -7F, 1, 14, 4, 0F,0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F); // shield3
		baseModel[7].setRotationPoint(0F, 0F, 0F);

		baseModel[8].addShapeBox(4.5F, -10F, -7F, 2, 8, 14, 0F,0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F); // Box 4
		baseModel[8].setRotationPoint(0F, 0F, 0F);

		baseModel[9].addBox(1.5F, -10F, -7F, 3, 8, 14, 0F); // Box 6
		baseModel[9].setRotationPoint(0F, 0F, 0F);

		baseModel[10].addBox(2F, -25F, -7F, 2, 1, 14, 0F); // Box 7
		baseModel[10].setRotationPoint(0F, 0F, 0F);

		baseModel[11].addShapeBox(4F, -25F, -7F, 2, 1, 14, 0F,0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F); // Box 8
		baseModel[11].setRotationPoint(0F, 0F, 0F);

		baseModel[12].addBox(2F, -24F, -7F, 1, 14, 1, 0F); // platingBack
		baseModel[12].setRotationPoint(0F, 0F, 0F);

		baseModel[13].addBox(2F, -24F, 6F, 1, 14, 1, 0F); // platingBack
		baseModel[13].setRotationPoint(0F, 0F, 0F);

		baseModel[14].addShapeBox(-2.5F, -8F, -6F, 2, 6, 2, 0F,-4F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // stabiliser
		baseModel[14].setRotationPoint(0F, 0F, 0F);

		baseModel[15].addShapeBox(-2.5F, -8F, 4F, 2, 6, 2, 0F,-4F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // stabiliser
		baseModel[15].setRotationPoint(0F, 0F, 0F);


		ammoModel = new ModelRendererTurbo[0][0];

		barrelX = 0;
		barrelY = 0;
		barrelZ = 0;


		flipAll();
    }
}
