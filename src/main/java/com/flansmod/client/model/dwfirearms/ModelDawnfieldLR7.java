package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelDawnfieldLR7 extends ModelGun
{
	int textureX = 512;
	int textureY = 256;

	public ModelDawnfieldLR7()
	{
		gunModel = new ModelRendererTurbo[79];
		gunModel[0] = new ModelRendererTurbo(this, 48, 12, textureX, textureY); // barrelLeft
		gunModel[1] = new ModelRendererTurbo(this, 59, 12, textureX, textureY); // barrelMiddle
		gunModel[2] = new ModelRendererTurbo(this, 48, 12, textureX, textureY); // barrelRight
		gunModel[3] = new ModelRendererTurbo(this, 201, 8, textureX, textureY); // body10
		gunModel[4] = new ModelRendererTurbo(this, 100, 1, textureX, textureY); // body2
		gunModel[5] = new ModelRendererTurbo(this, 100, 56, textureX, textureY); // body6
		gunModel[6] = new ModelRendererTurbo(this, 177, 125, textureX, textureY); // foreRail1
		gunModel[7] = new ModelRendererTurbo(this, 177, 106, textureX, textureY); // foreRail2
		gunModel[8] = new ModelRendererTurbo(this, 100, 159, textureX, textureY); // foreRail3
		gunModel[9] = new ModelRendererTurbo(this, 100, 138, textureX, textureY); // foreRail4
		gunModel[10] = new ModelRendererTurbo(this, 100, 148, textureX, textureY); // foreRail5
		gunModel[11] = new ModelRendererTurbo(this, 1, 122, textureX, textureY); // railPart0
		gunModel[12] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // railPart11
		gunModel[13] = new ModelRendererTurbo(this, 1, 112, textureX, textureY); // railPart7
		gunModel[14] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // railPart2
		gunModel[15] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // railPart3
		gunModel[16] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // railPart4
		gunModel[17] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // railPart5
		gunModel[18] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // railPart6
		gunModel[19] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // railPart10
		gunModel[20] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // railPart9
		gunModel[21] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // railPart8
		gunModel[22] = new ModelRendererTurbo(this, 177, 150, textureX, textureY); // Box 0
		gunModel[23] = new ModelRendererTurbo(this, 100, 18, textureX, textureY); // Box 1
		gunModel[24] = new ModelRendererTurbo(this, 100, 32, textureX, textureY); // Box 2
		gunModel[25] = new ModelRendererTurbo(this, 100, 81, textureX, textureY); // Box 3
		gunModel[26] = new ModelRendererTurbo(this, 100, 43, textureX, textureY); // Box 4
		gunModel[27] = new ModelRendererTurbo(this, 100, 128, textureX, textureY); // Box 5
		gunModel[28] = new ModelRendererTurbo(this, 177, 133, textureX, textureY); // Box 6
		gunModel[29] = new ModelRendererTurbo(this, 100, 117, textureX, textureY); // Box 7
		gunModel[30] = new ModelRendererTurbo(this, 177, 159, textureX, textureY); // Box 8
		gunModel[31] = new ModelRendererTurbo(this, 151, 81, textureX, textureY); // Box 11
		gunModel[32] = new ModelRendererTurbo(this, 177, 117, textureX, textureY); // Box 12
		gunModel[33] = new ModelRendererTurbo(this, 1, 89, textureX, textureY); // Box 48
		gunModel[34] = new ModelRendererTurbo(this, 55, 89, textureX, textureY); // Box 49
		gunModel[35] = new ModelRendererTurbo(this, 135, 97, textureX, textureY); // Box 1
		gunModel[36] = new ModelRendererTurbo(this, 135, 107, textureX, textureY); // Box 2
		gunModel[37] = new ModelRendererTurbo(this, 100, 97, textureX, textureY); // Box 6
		gunModel[38] = new ModelRendererTurbo(this, 100, 107, textureX, textureY); // Box 7
		gunModel[39] = new ModelRendererTurbo(this, 180, 5, textureX, textureY); // Box 8
		gunModel[40] = new ModelRendererTurbo(this, 145, 70, textureX, textureY); // Box 9
		gunModel[41] = new ModelRendererTurbo(this, 167, 31, textureX, textureY); // Box 10
		gunModel[42] = new ModelRendererTurbo(this, 167, 20, textureX, textureY); // Box 11
		gunModel[43] = new ModelRendererTurbo(this, 159, 5, textureX, textureY); // Box 12
		gunModel[44] = new ModelRendererTurbo(this, 20, 102, textureX, textureY); // Box 13
		gunModel[45] = new ModelRendererTurbo(this, 1, 54, textureX, textureY); // Box 14
		gunModel[46] = new ModelRendererTurbo(this, 1, 78, textureX, textureY); // Box 15
		gunModel[47] = new ModelRendererTurbo(this, 57, 54, textureX, textureY); // Box 16
		gunModel[48] = new ModelRendererTurbo(this, 57, 78, textureX, textureY); // Box 17
		gunModel[49] = new ModelRendererTurbo(this, 38, 78, textureX, textureY); // Box 18
		gunModel[50] = new ModelRendererTurbo(this, 38, 54, textureX, textureY); // Box 19
		gunModel[51] = new ModelRendererTurbo(this, 100, 70, textureX, textureY); // Box 31
		gunModel[52] = new ModelRendererTurbo(this, 167, 52, textureX, textureY); // Box 0
		gunModel[53] = new ModelRendererTurbo(this, 167, 49, textureX, textureY); // Box 1
		gunModel[54] = new ModelRendererTurbo(this, 167, 45, textureX, textureY); // Box 2
		gunModel[55] = new ModelRendererTurbo(this, 1, 45, textureX, textureY); // Box 5
		gunModel[56] = new ModelRendererTurbo(this, 1, 35, textureX, textureY); // Box 6
		gunModel[57] = new ModelRendererTurbo(this, 46, 34, textureX, textureY); // Box 7
		gunModel[58] = new ModelRendererTurbo(this, 46, 44, textureX, textureY); // Box 8
		gunModel[59] = new ModelRendererTurbo(this, 46, 44, textureX, textureY); // Box 9
		gunModel[60] = new ModelRendererTurbo(this, 46, 34, textureX, textureY); // Box 10
		gunModel[61] = new ModelRendererTurbo(this, 1, 31, textureX, textureY); // Box 11
		gunModel[62] = new ModelRendererTurbo(this, 1, 31, textureX, textureY); // Box 12
		gunModel[63] = new ModelRendererTurbo(this, 20, 24, textureX, textureY); // Box 13
		gunModel[64] = new ModelRendererTurbo(this, 11, 27, textureX, textureY); // Box 14
		gunModel[65] = new ModelRendererTurbo(this, 18, 27, textureX, textureY); // Box 15
		gunModel[66] = new ModelRendererTurbo(this, 6, 21, textureX, textureY); // Box 16
		gunModel[67] = new ModelRendererTurbo(this, 1, 21, textureX, textureY); // Box 17
		gunModel[68] = new ModelRendererTurbo(this, 6, 25, textureX, textureY); // Box 18
		gunModel[69] = new ModelRendererTurbo(this, 11, 24, textureX, textureY); // Box 19
		gunModel[70] = new ModelRendererTurbo(this, 20, 21, textureX, textureY); // Box 20
		gunModel[71] = new ModelRendererTurbo(this, 11, 21, textureX, textureY); // Box 21
		gunModel[72] = new ModelRendererTurbo(this, 27, 21, textureX, textureY); // Box 22
		gunModel[73] = new ModelRendererTurbo(this, 56, 21, textureX, textureY); // Box 23
		gunModel[74] = new ModelRendererTurbo(this, 44, 21, textureX, textureY); // Box 24
		gunModel[75] = new ModelRendererTurbo(this, 51, 21, textureX, textureY); // Box 25
		gunModel[76] = new ModelRendererTurbo(this, 39, 102, textureX, textureY); // Box 26
		gunModel[77] = new ModelRendererTurbo(this, 1, 101, textureX, textureY); // Box 27
		gunModel[78] = new ModelRendererTurbo(this, 36, 90, textureX, textureY); // Box 28

		gunModel[0].addShapeBox(0F, 0F, 0F, 3, 6, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F); // barrelLeft
		gunModel[0].setRotationPoint(85.5F, -21F, 1F);

		gunModel[1].addBox(0F, 0F, 0F, 3, 6, 2, 0F); // barrelMiddle
		gunModel[1].setRotationPoint(85.5F, -21F, -1F);

		gunModel[2].addShapeBox(0F, 0F, 0F, 3, 6, 2, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // barrelRight
		gunModel[2].setRotationPoint(85.5F, -21F, -3F);

		gunModel[3].addShapeBox(0F, 0F, 0F, 24, 8, 1, 0F, 0F, 0F, 0F, -12F, 0F, 0F, -12F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, -12F, -4F, 0F, -12F, -4F, 0F, 0F, -4F, 0F); // body10
		gunModel[3].setRotationPoint(59F, -13F, -3.2F);

		gunModel[4].addBox(0F, 0F, 0F, 21, 8, 8, 0F); // body2
		gunModel[4].setRotationPoint(-7F, -16F, -4F);

		gunModel[5].addBox(0F, 0F, 0F, 25, 5, 8, 0F); // body6
		gunModel[5].setRotationPoint(33F, -16F, -4F);

		gunModel[6].addShapeBox(0F, 0F, 0F, 30, 1, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // foreRail1
		gunModel[6].setRotationPoint(58F, -9F, -3F);

		gunModel[7].addBox(0F, 0F, 0F, 30, 4, 6, 0F); // foreRail2
		gunModel[7].setRotationPoint(58F, -13F, -3F);

		gunModel[8].addShapeBox(0F, 0F, 0F, 30, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // foreRail3
		gunModel[8].setRotationPoint(58F, -16F, -4F);

		gunModel[9].addBox(0F, 0F, 0F, 30, 1, 8, 0F); // foreRail4
		gunModel[9].setRotationPoint(58F, -17F, -4F);

		gunModel[10].addShapeBox(0F, 0F, 0F, 30, 2, 8, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // foreRail5
		gunModel[10].setRotationPoint(58F, -23F, -4F);

		gunModel[11].addBox(0F, 0F, 0F, 27, 2, 7, 0F); // railPart0
		gunModel[11].setRotationPoint(13F, -24F, -3.5F);

		gunModel[12].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // railPart11
		gunModel[12].setRotationPoint(53F, -6F, -3.5F);

		gunModel[13].addBox(0F, 0F, 0F, 21, 2, 7, 0F); // railPart7
		gunModel[13].setRotationPoint(35F, -8F, -3.5F);

		gunModel[14].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // railPart2
		gunModel[14].setRotationPoint(13F, -26F, -3.5F);

		gunModel[15].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // railPart3
		gunModel[15].setRotationPoint(19F, -26F, -3.5F);

		gunModel[16].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // railPart4
		gunModel[16].setRotationPoint(25F, -26F, -3.5F);

		gunModel[17].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // railPart5
		gunModel[17].setRotationPoint(31F, -26F, -3.5F);

		gunModel[18].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // railPart6
		gunModel[18].setRotationPoint(37F, -26F, -3.5F);

		gunModel[19].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // railPart10
		gunModel[19].setRotationPoint(47F, -6F, -3.5F);

		gunModel[20].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // railPart9
		gunModel[20].setRotationPoint(41F, -6F, -3.5F);

		gunModel[21].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // railPart8
		gunModel[21].setRotationPoint(35F, -6F, -3.5F);

		gunModel[22].addShapeBox(0F, 0F, 0F, 26, 1, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 0
		gunModel[22].setRotationPoint(32F, -8F, -3.5F);

		gunModel[23].addBox(0F, 0F, 0F, 25, 5, 8, 0F); // Box 1
		gunModel[23].setRotationPoint(33F, -21F, -4F);

		gunModel[24].addShapeBox(0F, 0F, 0F, 25, 2, 8, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 2
		gunModel[24].setRotationPoint(33F, -23F, -4F);

		gunModel[25].addBox(0F, 0F, 0F, 18, 8, 7, 0F); // Box 3
		gunModel[25].setRotationPoint(14F, -8F, -3.5F);

		gunModel[26].addBox(0F, 0F, 0F, 25, 5, 7, 0F); // Box 4
		gunModel[26].setRotationPoint(8F, -21F, -4F);

		gunModel[27].addBox(0F, 0F, 0F, 30, 1, 8, 0F); // Box 5
		gunModel[27].setRotationPoint(58F, -21F, -4F);

		gunModel[28].addShapeBox(0F, 0F, 0F, 10, 2, 8, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 6
		gunModel[28].setRotationPoint(78F, -19F, -4F);

		gunModel[29].addBox(0F, 0F, 0F, 28, 3, 7, 0F); // Box 7
		gunModel[29].setRotationPoint(58F, -20F, -3.5F);

		gunModel[30].addShapeBox(0F, 0F, 0F, 25, 2, 8, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 8
		gunModel[30].setRotationPoint(8F, -23F, -4F);

		gunModel[31].addShapeBox(0F, 0F, 0F, 1, 8, 7, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F); // Box 11
		gunModel[31].setRotationPoint(32F, -8F, -3.5F);

		gunModel[32].addShapeBox(0F, 0F, 0F, 30, 1, 6, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 12
		gunModel[32].setRotationPoint(58F, -14F, -3F);

		gunModel[33].addBox(0F, 0F, 0F, 9, 3, 8, 0F); // Box 48
		gunModel[33].setRotationPoint(-4F, -8F, -4F);

		gunModel[34].addShapeBox(0F, 0F, 0F, 1, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 49
		gunModel[34].setRotationPoint(5F, -8F, -4F);

		gunModel[35].addShapeBox(0F, 0F, 0F, 5, 2, 7, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 1
		gunModel[35].setRotationPoint(-7F, -21F, -3.5F);

		gunModel[36].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, -2F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, -2F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 2
		gunModel[36].setRotationPoint(-5F, -23F, -3.5F);

		gunModel[37].addBox(0F, 0F, 0F, 10, 2, 7, 0F); // Box 6
		gunModel[37].setRotationPoint(-2F, -21F, -3.5F);

		gunModel[38].addShapeBox(0F, 0F, 0F, 10, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 7
		gunModel[38].setRotationPoint(-2F, -23F, -3.5F);

		gunModel[39].addShapeBox(0F, 0F, 0F, 2, 4, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F); // Box 8
		gunModel[39].setRotationPoint(14F, -12F, -4F);

		gunModel[40].addBox(0F, 0F, 0F, 19, 3, 7, 0F); // Box 9
		gunModel[40].setRotationPoint(14F, -11F, -3.5F);

		gunModel[41].addBox(0F, 0F, 0F, 17, 5, 8, 0F); // Box 10
		gunModel[41].setRotationPoint(16F, -16F, -4F);

		gunModel[42].addBox(0F, 0F, 0F, 25, 3, 7, 0F); // Box 11
		gunModel[42].setRotationPoint(33F, -11F, -3.5F);

		gunModel[43].addBox(0F, 0F, 0F, 2, 4, 8, 0F); // Box 12
		gunModel[43].setRotationPoint(14F, -16F, -4F);

		gunModel[44].addShapeBox(0F, 0F, 0F, 1, 1, 8, 0F, 1F, 0F, -1F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 13
		gunModel[44].setRotationPoint(-6F, -8F, -4F);

		gunModel[45].addShapeBox(0F, 0F, 0F, 10, 15, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 8F, -3F, 0F, -8F, 0F, 0F, -8F, 0F, 0F, 8F, -3F, 0F); // Box 14
		gunModel[45].setRotationPoint(-5F, -5F, -4F);

		gunModel[46].addShapeBox(0F, 0F, 0F, 10, 2, 8, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F); // Box 15
		gunModel[46].setRotationPoint(-13F, 10F, -4F);

		gunModel[47].addShapeBox(0F, 0F, 0F, 1, 12, 8, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 8F, 0F, -1F, -8F, 0F, 0F, -8F, 0F, 0F, 8F, 0F, -1F); // Box 16
		gunModel[47].setRotationPoint(-6F, -5F, -4F);

		gunModel[48].addShapeBox(0F, 0F, 0F, 1, 2, 8, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -0.5F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -1F); // Box 17
		gunModel[48].setRotationPoint(-14F, 7F, -4F);

		gunModel[49].addShapeBox(0F, 0F, 0F, 1, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 18
		gunModel[49].setRotationPoint(-3F, 10F, -4F);

		gunModel[50].addShapeBox(0F, 0F, 0F, 1, 15, 8, 0F, -8F, 0F, 0F, 8F, 0F, -1F, 8F, 0F, -1F, -8F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 19
		gunModel[50].setRotationPoint(-3F, -5F, -4F);

		gunModel[51].addBox(0F, 0F, 0F, 15, 3, 7, 0F); // Box 31
		gunModel[51].setRotationPoint(-7F, -19F, -3.5F);

		gunModel[52].addBox(0F, 0F, 0F, 10, 5, 1, 0F); // Box 0
		gunModel[52].setRotationPoint(8F, -21F, 3F);

		gunModel[53].addBox(0F, 0F, 0F, 15, 1, 1, 0F); // Box 1
		gunModel[53].setRotationPoint(18F, -21F, 3F);

		gunModel[54].addBox(0F, 0F, 0F, 15, 2, 1, 0F); // Box 2
		gunModel[54].setRotationPoint(18F, -18F, 3F);

		gunModel[55].addShapeBox(0F, 0F, 0F, 15, 1, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 5
		gunModel[55].setRotationPoint(49F, -26F, -3.5F);

		gunModel[56].addBox(0F, 0F, 0F, 15, 2, 7, 0F); // Box 6
		gunModel[56].setRotationPoint(49F, -25F, -3.5F);

		gunModel[57].addShapeBox(0F, 0F, 0F, 3, 1, 8, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 7
		gunModel[57].setRotationPoint(50F, -23F, -4F);

		gunModel[58].addBox(0F, 0F, 0F, 3, 1, 8, 0F); // Box 8
		gunModel[58].setRotationPoint(50F, -22F, -4F);

		gunModel[59].addBox(0F, 0F, 0F, 3, 1, 8, 0F); // Box 9
		gunModel[59].setRotationPoint(60F, -22F, -4F);

		gunModel[60].addShapeBox(0F, 0F, 0F, 3, 1, 8, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 10
		gunModel[60].setRotationPoint(60F, -23F, -4F);

		gunModel[61].addShapeBox(0F, 0F, 0F, 3, 1, 2, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 11
		gunModel[61].setRotationPoint(46F, -25F, -2.5F);

		gunModel[62].addShapeBox(0F, 0F, 0F, 3, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 12
		gunModel[62].setRotationPoint(46F, -24F, -2.5F);

		gunModel[63].addBox(0F, 0F, 0F, 2, 1, 1, 0F); // Box 13
		gunModel[63].setRotationPoint(44F, -24.5F, -2F);

		gunModel[64].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F, 0F, -0.5F, 0F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, -0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0.5F, 0F); // Box 14
		gunModel[64].setRotationPoint(42F, -24.5F, -4F);

		gunModel[65].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F, 0F, -2F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, -1F, 0F, 2F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, -1F); // Box 15
		gunModel[65].setRotationPoint(40F, -24F, -4F);

		gunModel[66].addShapeBox(0F, 0F, 0F, 1, 2, 1, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 16
		gunModel[66].setRotationPoint(39F, -22F, -5F);

		gunModel[67].addShapeBox(0F, 0F, 0F, 1, 8, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F); // Box 17
		gunModel[67].setRotationPoint(39F, -20F, -5F);

		gunModel[68].addShapeBox(0F, 0F, 0F, 1, 3, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, -1F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 2F, -1F, 0F); // Box 18
		gunModel[68].setRotationPoint(38F, -12F, -5F);

		gunModel[69].addBox(0F, 0F, 0F, 3, 1, 1, 0F); // Box 19
		gunModel[69].setRotationPoint(33F, -10F, -5F);

		gunModel[70].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F); // Box 20
		gunModel[70].setRotationPoint(31F, -10F, -5F);

		gunModel[71].addBox(0F, 0F, 0F, 3, 1, 1, 0F); // Box 21
		gunModel[71].setRotationPoint(28F, -10F, -4.5F);

		gunModel[72].addBox(0F, 0F, 0F, 7, 5, 1, 0F); // Box 22
		gunModel[72].setRotationPoint(18F, -11F, -4.5F);

		gunModel[73].addShapeBox(0F, 0F, 0F, 1, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F); // Box 23
		gunModel[73].setRotationPoint(25F, -8F, -4.5F);

		gunModel[74].addBox(0F, 0F, 0F, 2, 4, 1, 0F); // Box 24
		gunModel[74].setRotationPoint(26F, -11F, -4.5F);

		gunModel[75].addBox(0F, 0F, 0F, 1, 3, 1, 0F); // Box 25
		gunModel[75].setRotationPoint(25F, -11F, -4.5F);

		gunModel[76].addShapeBox(0F, 0F, 0F, 2, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F); // Box 26
		gunModel[76].setRotationPoint(-6F, -8F, -4F);

		gunModel[77].addShapeBox(0F, 0F, 0F, 1, 2, 8, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 27
		gunModel[77].setRotationPoint(-6F, -7F, -4F);

		gunModel[78].addBox(0F, 0F, 0F, 1, 2, 8, 0F); // Box 28
		gunModel[78].setRotationPoint(-5F, -7F, -4F);


		defaultScopeModel = new ModelRendererTurbo[21];
		defaultScopeModel[0] = new ModelRendererTurbo(this, 39, 1, textureX, textureY); // Box 35
		defaultScopeModel[1] = new ModelRendererTurbo(this, 58, 1, textureX, textureY); // Box 35
		defaultScopeModel[2] = new ModelRendererTurbo(this, 37, 16, textureX, textureY); // Box 35
		defaultScopeModel[3] = new ModelRendererTurbo(this, 2, 11, textureX, textureY); // Box 35
		defaultScopeModel[4] = new ModelRendererTurbo(this, 2, 11, textureX, textureY); // Box 35
		defaultScopeModel[5] = new ModelRendererTurbo(this, 39, 1, textureX, textureY); // Box 35
		defaultScopeModel[6] = new ModelRendererTurbo(this, 58, 1, textureX, textureY); // Box 35
		defaultScopeModel[7] = new ModelRendererTurbo(this, 2, 15, textureX, textureY); // Box 35
		defaultScopeModel[8] = new ModelRendererTurbo(this, 1, 11, textureX, textureY); // Box 35
		defaultScopeModel[9] = new ModelRendererTurbo(this, 32, 14, textureX, textureY); // Box 35
		defaultScopeModel[10] = new ModelRendererTurbo(this, 32, 14, textureX, textureY); // Box 35
		defaultScopeModel[11] = new ModelRendererTurbo(this, 14, 15, textureX, textureY); // Box 35
		defaultScopeModel[12] = new ModelRendererTurbo(this, 14, 15, textureX, textureY); // Box 35
		defaultScopeModel[13] = new ModelRendererTurbo(this, 22, 3, textureX, textureY); // Box 35
		defaultScopeModel[14] = new ModelRendererTurbo(this, 22, 3, textureX, textureY); // Box 35
		defaultScopeModel[15] = new ModelRendererTurbo(this, 2, 15, textureX, textureY); // Box 35
		defaultScopeModel[16] = new ModelRendererTurbo(this, 2, 15, textureX, textureY); // Box 35
		defaultScopeModel[17] = new ModelRendererTurbo(this, 22, 3, textureX, textureY); // Box 35
		defaultScopeModel[18] = new ModelRendererTurbo(this, 22, 3, textureX, textureY); // Box 35
		defaultScopeModel[19] = new ModelRendererTurbo(this, 2, 15, textureX, textureY); // Box 35
		defaultScopeModel[20] = new ModelRendererTurbo(this, 22, 14, textureX, textureY); // Box 35

		defaultScopeModel[0].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 35
		defaultScopeModel[0].setRotationPoint(10.5F, -26F, -3.5F);

		defaultScopeModel[1].addBox(0F, 0F, 0F, 4, 2, 7, 0F); // Box 35
		defaultScopeModel[1].setRotationPoint(8.5F, -24F, -3.5F);

		defaultScopeModel[2].addBox(0F, 0F, 0F, 2, 1, 3, 0F); // Box 35
		defaultScopeModel[2].setRotationPoint(8.5F, -28F, -1.5F);

		defaultScopeModel[3].addBox(0F, 0F, 0F, 2, 2, 1, 0F); // Box 35
		defaultScopeModel[3].setRotationPoint(8.5F, -30F, -1.5F);

		defaultScopeModel[4].addBox(0F, 0F, 0F, 2, 2, 1, 0F); // Box 35
		defaultScopeModel[4].setRotationPoint(8.5F, -30F, 0.5F);

		defaultScopeModel[5].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 35
		defaultScopeModel[5].setRotationPoint(84.5F, -26F, -3.5F);

		defaultScopeModel[6].addBox(0F, 0F, 0F, 4, 2, 7, 0F); // Box 35
		defaultScopeModel[6].setRotationPoint(82.5F, -24F, -3.5F);

		defaultScopeModel[7].addBox(0F, 0F, 0F, 2, 2, 1, 0F); // Box 35
		defaultScopeModel[7].setRotationPoint(82.5F, -26F, -2.5F);

		defaultScopeModel[8].addShapeBox(0F, 0F, 0F, 2, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 35
		defaultScopeModel[8].setRotationPoint(82.5F, -27F, -4F);

		defaultScopeModel[9].addBox(0F, 0F, 0F, 2, 3, 1, 0F); // Box 35
		defaultScopeModel[9].setRotationPoint(82.5F, -30F, -4F);

		defaultScopeModel[10].addBox(0F, 0F, 0F, 2, 3, 1, 0F); // Box 35
		defaultScopeModel[10].setRotationPoint(82.5F, -30F, 3F);

		defaultScopeModel[11].addShapeBox(0F, 0F, 0F, 2, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 35
		defaultScopeModel[11].setRotationPoint(82.5F, -32F, 3F);

		defaultScopeModel[12].addShapeBox(0F, 0F, 0F, 2, 2, 1, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 35
		defaultScopeModel[12].setRotationPoint(82.5F, -32F, -4F);

		defaultScopeModel[13].addShapeBox(0F, 0F, 0F, 2, 1, 6, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 35
		defaultScopeModel[13].setRotationPoint(82.5F, -26F, -3F);

		defaultScopeModel[14].addShapeBox(0F, 0F, 0F, 2, 1, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 35
		defaultScopeModel[14].setRotationPoint(82.5F, -25F, -3F);

		defaultScopeModel[15].addBox(0F, 0F, 0F, 2, 2, 1, 0F); // Box 35
		defaultScopeModel[15].setRotationPoint(82.5F, -26F, 1.5F);

		defaultScopeModel[16].addBox(0F, 0F, 0F, 2, 2, 1, 0F); // Box 35
		defaultScopeModel[16].setRotationPoint(8.5F, -26F, -2.5F);

		defaultScopeModel[17].addShapeBox(0F, 0F, 0F, 2, 1, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 35
		defaultScopeModel[17].setRotationPoint(8.5F, -25F, -3F);

		defaultScopeModel[18].addShapeBox(0F, 0F, 0F, 2, 1, 6, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 35
		defaultScopeModel[18].setRotationPoint(8.5F, -26F, -3F);

		defaultScopeModel[19].addBox(0F, 0F, 0F, 2, 2, 1, 0F); // Box 35
		defaultScopeModel[19].setRotationPoint(8.5F, -26F, 1.5F);

		defaultScopeModel[20].addBox(0F, 0F, 0F, 2, 1, 5, 0F); // Box 35
		defaultScopeModel[20].setRotationPoint(8.5F, -27F, -2.5F);


		defaultStockModel = new ModelRendererTurbo[18];
		defaultStockModel[0] = new ModelRendererTurbo(this, 48, 144, textureX, textureY); // Box 69
		defaultStockModel[1] = new ModelRendererTurbo(this, 1, 143, textureX, textureY); // Box 69
		defaultStockModel[2] = new ModelRendererTurbo(this, 1, 171, textureX, textureY); // Box 69
		defaultStockModel[3] = new ModelRendererTurbo(this, 1, 159, textureX, textureY); // Box 69
		defaultStockModel[4] = new ModelRendererTurbo(this, 65, 146, textureX, textureY); // Box 69
		defaultStockModel[5] = new ModelRendererTurbo(this, 65, 146, textureX, textureY); // Box 69
		defaultStockModel[6] = new ModelRendererTurbo(this, 60, 156, textureX, textureY); // Box 69
		defaultStockModel[7] = new ModelRendererTurbo(this, 93, 205, textureX, textureY); // Box 69
		defaultStockModel[8] = new ModelRendererTurbo(this, 1, 206, textureX, textureY); // Box 69
		defaultStockModel[9] = new ModelRendererTurbo(this, 1, 195, textureX, textureY); // Box 69
		defaultStockModel[10] = new ModelRendererTurbo(this, 1, 183, textureX, textureY); // Box 69
		defaultStockModel[11] = new ModelRendererTurbo(this, 48, 187, textureX, textureY); // Box 69
		defaultStockModel[12] = new ModelRendererTurbo(this, 102, 217, textureX, textureY); // Box 69
		defaultStockModel[13] = new ModelRendererTurbo(this, 73, 221, textureX, textureY); // Box 69
		defaultStockModel[14] = new ModelRendererTurbo(this, 48, 211, textureX, textureY); // Box 69
		defaultStockModel[15] = new ModelRendererTurbo(this, 73, 209, textureX, textureY); // Box 69
		defaultStockModel[16] = new ModelRendererTurbo(this, 48, 199, textureX, textureY); // Box 69
		defaultStockModel[17] = new ModelRendererTurbo(this, 48, 175, textureX, textureY); // Box 69

		defaultStockModel[0].addBox(-1F, -3.5F, -3.5F, 1, 7, 7, 0F); // Box 69
		defaultStockModel[0].setRotationPoint(-7F, -13.5F, 0F);

		defaultStockModel[1].addBox(-15F, -1.5F, -4.5F, 14, 6, 9, 0F); // Box 69
		defaultStockModel[1].setRotationPoint(-7F, -13.5F, 0F);

		defaultStockModel[2].addShapeBox(-15F, 4.5F, -4.5F, 14, 2, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 69
		defaultStockModel[2].setRotationPoint(-7F, -13.5F, 0F);

		defaultStockModel[3].addShapeBox(-35F, -6.5F, -4.5F, 20, 2, 9, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 69
		defaultStockModel[3].setRotationPoint(-7F, -13.5F, 0F);

		defaultStockModel[4].addShapeBox(-1F, -5.5F, -3.5F, 1, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 69
		defaultStockModel[4].setRotationPoint(-7F, -13.5F, 0F);

		defaultStockModel[5].addShapeBox(-1F, 3.5F, -3.5F, 1, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 69
		defaultStockModel[5].setRotationPoint(-7F, -13.5F, 0F);

		defaultStockModel[6].addShapeBox(-21F, -4.5F, -4.5F, 6, 9, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 10F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 10F, 0F); // Box 69
		defaultStockModel[6].setRotationPoint(-7F, -13.5F, 0F);

		defaultStockModel[7].addShapeBox(-21F, 4.5F, -4.5F, 6, 2, 9, 0F, 0F, -10F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -10F, 0F, 0F, 10F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 10F, -2F); // Box 69
		defaultStockModel[7].setRotationPoint(-7F, -13.5F, 0F);

		defaultStockModel[8].addBox(-35F, -4.5F, -4.5F, 14, 19, 9, 0F); // Box 69
		defaultStockModel[8].setRotationPoint(-7F, -13.5F, 0F);

		defaultStockModel[9].addShapeBox(-15F, -6F, -4F, 14, 2, 8, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 69
		defaultStockModel[9].setRotationPoint(-7F, -13.5F, 0F);

		defaultStockModel[10].addBox(-15F, -4F, -4F, 14, 3, 8, 0F); // Box 69
		defaultStockModel[10].setRotationPoint(-7F, -13.5F, 0F);

		defaultStockModel[11].addShapeBox(-35F, 14.5F, -4.5F, 14, 2, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 69
		defaultStockModel[11].setRotationPoint(-7F, -13.5F, 0F);

		defaultStockModel[12].addBox(-37F, -6F, -5F, 4, 7, 10, 0F); // Box 69
		defaultStockModel[12].setRotationPoint(-7F, -13.5F, 0F);

		defaultStockModel[13].addShapeBox(-37F, 1F, -5F, 4, 3, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F); // Box 69
		defaultStockModel[13].setRotationPoint(-7F, -13.5F, 0F);

		defaultStockModel[14].addBox(-37F, 4F, -5F, 2, 13, 10, 0F); // Box 69
		defaultStockModel[14].setRotationPoint(-7F, -13.5F, 0F);

		defaultStockModel[15].addShapeBox(-37F, -7F, -5F, 4, 1, 10, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 69
		defaultStockModel[15].setRotationPoint(-7F, -13.5F, 0F);

		defaultStockModel[16].addShapeBox(-37F, 17F, -5F, 2, 1, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 69
		defaultStockModel[16].setRotationPoint(-7F, -13.5F, 0F);

		defaultStockModel[17].addBox(-35F, 14.5F, -3.5F, 10, 4, 7, 0F); // Box 69
		defaultStockModel[17].setRotationPoint(-7F, -13.5F, 0F);


		ammoModel = new ModelRendererTurbo[14];
		ammoModel[0] = new ModelRendererTurbo(this, 217, 48, textureX, textureY); // Box 20
		ammoModel[1] = new ModelRendererTurbo(this, 234, 48, textureX, textureY); // Box 21
		ammoModel[2] = new ModelRendererTurbo(this, 200, 48, textureX, textureY); // Box 22
		ammoModel[3] = new ModelRendererTurbo(this, 282, 49, textureX, textureY); // Box 23
		ammoModel[4] = new ModelRendererTurbo(this, 269, 49, textureX, textureY); // Box 24
		ammoModel[5] = new ModelRendererTurbo(this, 247, 79, textureX, textureY); // Box 27
		ammoModel[6] = new ModelRendererTurbo(this, 200, 78, textureX, textureY); // Box 28
		ammoModel[7] = new ModelRendererTurbo(this, 200, 87, textureX, textureY); // Box 29
		ammoModel[8] = new ModelRendererTurbo(this, 229, 87, textureX, textureY); // Box 30
		ammoModel[9] = new ModelRendererTurbo(this, 200, 70, textureX, textureY); // Box 45
		ammoModel[10] = new ModelRendererTurbo(this, 269, 71, textureX, textureY); // Box 46
		ammoModel[11] = new ModelRendererTurbo(this, 234, 70, textureX, textureY); // Box 48
		ammoModel[12] = new ModelRendererTurbo(this, 282, 71, textureX, textureY); // Box 49
		ammoModel[13] = new ModelRendererTurbo(this, 217, 70, textureX, textureY); // Box 50

		ammoModel[0].addBox(0F, 0F, 0F, 2, 15, 6, 0F); // Box 20
		ammoModel[0].setRotationPoint(15F, -7F, -3F);

		ammoModel[1].addBox(0F, 0F, 0F, 11, 15, 6, 0F); // Box 21
		ammoModel[1].setRotationPoint(18F, -7F, -3F);

		ammoModel[2].addBox(0F, 0F, 0F, 2, 15, 6, 0F); // Box 22
		ammoModel[2].setRotationPoint(30F, -7F, -3F);

		ammoModel[3].addBox(0F, 0F, 0F, 1, 15, 5, 0F); // Box 23
		ammoModel[3].setRotationPoint(17F, -7F, -2.5F);

		ammoModel[4].addBox(0F, 0F, 0F, 1, 15, 5, 0F); // Box 24
		ammoModel[4].setRotationPoint(29F, -7F, -2.5F);

		ammoModel[5].addBox(0F, 0F, 0F, 17, 1, 6, 0F); // Box 27
		ammoModel[5].setRotationPoint(15F, -8F, -3F);

		ammoModel[6].addShapeBox(0F, 0F, 0F, 17, 2, 6, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 28
		ammoModel[6].setRotationPoint(15F, -10F, -3F);

		ammoModel[7].addShapeBox(0F, 0F, 0F, 11, 1, 3, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 29
		ammoModel[7].setRotationPoint(16F, -11F, -1.5F);

		ammoModel[8].addShapeBox(0F, 0F, 0F, 4, 1, 3, 0F, 0F, 0F, -0.5F, 0F, -0.5F, -1F, 0F, -0.5F, -1F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F); // Box 30
		ammoModel[8].setRotationPoint(27F, -11F, -1.5F);

		ammoModel[9].addShapeBox(0F, 0F, 0F, 2, 1, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F, 0F, 0F, -1F); // Box 45
		ammoModel[9].setRotationPoint(30F, 8F, -3F);

		ammoModel[10].addShapeBox(0F, 0F, 0F, 1, 1, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 46
		ammoModel[10].setRotationPoint(29F, 8F, -2.5F);

		ammoModel[11].addShapeBox(0F, 0F, 0F, 11, 1, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 48
		ammoModel[11].setRotationPoint(18F, 8F, -3F);

		ammoModel[12].addShapeBox(0F, 0F, 0F, 1, 1, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 49
		ammoModel[12].setRotationPoint(17F, 8F, -2.5F);

		ammoModel[13].addShapeBox(0F, 0F, 0F, 2, 1, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, -1F, 0F, -1F); // Box 50
		ammoModel[13].setRotationPoint(15F, 8F, -3F);


		pumpModel = new ModelRendererTurbo[2];
		pumpModel[0] = new ModelRendererTurbo(this, 61, 35, textureX, textureY); // Box 3
		pumpModel[1] = new ModelRendererTurbo(this, 61, 35, textureX, textureY); // Box 4

		pumpModel[0].addShapeBox(0F, 0F, 0F, 2, 1, 5, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 3
		pumpModel[0].setRotationPoint(31F, -20F, 3F);

		pumpModel[1].addShapeBox(0F, 0F, 0F, 2, 1, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 4
		pumpModel[1].setRotationPoint(31F, -19F, 3F);

		stockAttachPoint = new Vector3f(-13F /16F, 9F /16F, 0F /16F);
		scopeAttachPoint = new Vector3f(22F /16F, 20F /16F, 0F /16F);
		gripAttachPoint = new Vector3f(51 /16F, 1F /16F, 0F /16F);

		gunSlideDistance = 0F;
		animationType = EnumAnimationType.BOTTOM_CLIP;


		translateAll(0F, 0F, 0F);


		flipAll();
	}
}