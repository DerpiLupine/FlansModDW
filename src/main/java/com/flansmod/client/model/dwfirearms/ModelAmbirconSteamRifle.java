package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelAmbirconSteamRifle extends ModelGun
{
	int textureX = 512;
	int textureY = 256;

	public ModelAmbirconSteamRifle()
	{
		gunModel = new ModelRendererTurbo[49];
		gunModel[0] = new ModelRendererTurbo(this, 1, 35, textureX, textureY); // Import backBarrel1
		gunModel[1] = new ModelRendererTurbo(this, 1, 35, textureX, textureY); // Import backBarrel1-2
		gunModel[2] = new ModelRendererTurbo(this, 1, 24, textureX, textureY); // Import backBarrel2
		gunModel[3] = new ModelRendererTurbo(this, 24, 35, textureX, textureY); // Import backBarrel3
		gunModel[4] = new ModelRendererTurbo(this, 24, 35, textureX, textureY); // Import backBarrel3-2
		gunModel[5] = new ModelRendererTurbo(this, 24, 22, textureX, textureY); // Import backBarrel4
		gunModel[6] = new ModelRendererTurbo(this, 43, 25, textureX, textureY); // Import backBarrel5
		gunModel[7] = new ModelRendererTurbo(this, 43, 25, textureX, textureY); // Import backBarrel5-2
		gunModel[8] = new ModelRendererTurbo(this, 43, 25, textureX, textureY); // Import backBarrel5-3
		gunModel[9] = new ModelRendererTurbo(this, 96, 84, textureX, textureY); // Import body1
		gunModel[10] = new ModelRendererTurbo(this, 101, 45, textureX, textureY); // Import body10
		gunModel[11] = new ModelRendererTurbo(this, 101, 45, textureX, textureY); // Import body10-2
		gunModel[12] = new ModelRendererTurbo(this, 96, 45, textureX, textureY); // Import body11
		gunModel[13] = new ModelRendererTurbo(this, 96, 45, textureX, textureY); // Import body11-2
		gunModel[14] = new ModelRendererTurbo(this, 128, 32, textureX, textureY); // Import body12
		gunModel[15] = new ModelRendererTurbo(this, 121, 42, textureX, textureY); // Import body13
		gunModel[16] = new ModelRendererTurbo(this, 121, 42, textureX, textureY); // Import body13-2
		gunModel[17] = new ModelRendererTurbo(this, 96, 72, textureX, textureY); // Import body14
		gunModel[18] = new ModelRendererTurbo(this, 159, 32, textureX, textureY); // Import body15
		gunModel[19] = new ModelRendererTurbo(this, 154, 117, textureX, textureY); // Import body2
		gunModel[20] = new ModelRendererTurbo(this, 96, 97, textureX, textureY); // Import body3
		gunModel[21] = new ModelRendererTurbo(this, 154, 97, textureX, textureY); // Import body4
		gunModel[22] = new ModelRendererTurbo(this, 139, 97, textureX, textureY); // Import body5
		gunModel[23] = new ModelRendererTurbo(this, 96, 51, textureX, textureY); // Import body6
		gunModel[24] = new ModelRendererTurbo(this, 144, 38, textureX, textureY); // Import body7
		gunModel[25] = new ModelRendererTurbo(this, 119, 38, textureX, textureY); // Import body8
		gunModel[26] = new ModelRendererTurbo(this, 154, 109, textureX, textureY); // Import body9
		gunModel[27] = new ModelRendererTurbo(this, 96, 32, textureX, textureY); // Import bodyMeterScreen
		gunModel[28] = new ModelRendererTurbo(this, 1, 185, textureX, textureY); // Import grip1
		gunModel[29] = new ModelRendererTurbo(this, 35, 167, textureX, textureY); // Import grip2
		gunModel[30] = new ModelRendererTurbo(this, 1, 167, textureX, textureY); // Import grip3
		gunModel[31] = new ModelRendererTurbo(this, 35, 178, textureX, textureY); // Import grip4
		gunModel[32] = new ModelRendererTurbo(this, 96, 22, textureX, textureY); // Import mainBarrelBottom
		gunModel[33] = new ModelRendererTurbo(this, 96, 11, textureX, textureY); // Import mainBarrelMiddle
		gunModel[34] = new ModelRendererTurbo(this, 96, 1, textureX, textureY); // Import mainBarrelTop
		gunModel[35] = new ModelRendererTurbo(this, 44, 9, textureX, textureY); // Import rail1
		gunModel[36] = new ModelRendererTurbo(this, 44, 1, textureX, textureY); // Import rail2
		gunModel[37] = new ModelRendererTurbo(this, 44, 15, textureX, textureY); // Import rail3
		gunModel[38] = new ModelRendererTurbo(this, 44, 15, textureX, textureY); // Import rail3-2
		gunModel[39] = new ModelRendererTurbo(this, 72, 48, textureX, textureY); // Import ringPart1-1
		gunModel[40] = new ModelRendererTurbo(this, 72, 48, textureX, textureY); // Import ringPart1-2
		gunModel[41] = new ModelRendererTurbo(this, 72, 48, textureX, textureY); // Import ringPart1-3
		gunModel[42] = new ModelRendererTurbo(this, 72, 48, textureX, textureY); // Import ringPart1-4
		gunModel[43] = new ModelRendererTurbo(this, 26, 45, textureX, textureY); // Import ringPart2-1
		gunModel[44] = new ModelRendererTurbo(this, 26, 45, textureX, textureY); // Import ringPart2-2
		gunModel[45] = new ModelRendererTurbo(this, 121, 32, textureX, textureY); // Import steamPipe1-1
		gunModel[46] = new ModelRendererTurbo(this, 121, 32, textureX, textureY); // Import steamPipe1-2
		gunModel[47] = new ModelRendererTurbo(this, 121, 32, textureX, textureY); // Import steamPipe1-3
		gunModel[48] = new ModelRendererTurbo(this, 121, 32, textureX, textureY); // Import steamPipe1-4

		gunModel[0].addShapeBox(0F, 0F, 0F, 4, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Import backBarrel1
		gunModel[0].setRotationPoint(-16F, -17F, -3.5F);

		gunModel[1].addShapeBox(0F, 0F, 0F, 4, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import backBarrel1-2
		gunModel[1].setRotationPoint(-16F, -22F, -3.5F);

		gunModel[2].addBox(0F, 0F, 0F, 4, 3, 7, 0F); // Import backBarrel2
		gunModel[2].setRotationPoint(-16F, -20F, -3.5F);

		gunModel[3].addShapeBox(0F, 0F, 0F, 5, 1, 8, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F); // Import backBarrel3
		gunModel[3].setRotationPoint(-21F, -16F, -4F);

		gunModel[4].addShapeBox(0F, 0F, 0F, 5, 1, 8, 0F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F); // Import backBarrel3-2
		gunModel[4].setRotationPoint(-21F, -22F, -4F);

		gunModel[5].addBox(0F, 0F, 0F, 5, 4, 8, 0F); // Import backBarrel4
		gunModel[5].setRotationPoint(-21F, -20.5F, -4F);

		gunModel[6].addShapeBox(0F, 0F, 0F, 1, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Import backBarrel5
		gunModel[6].setRotationPoint(-18F, -23.5F, -1.5F);

		gunModel[7].addShapeBox(0F, 0F, 0F, 1, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import backBarrel5-2
		gunModel[7].setRotationPoint(-19F, -23.5F, -1.5F);

		gunModel[8].addShapeBox(0F, 0F, 0F, 1, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Import backBarrel5-3
		gunModel[8].setRotationPoint(-20F, -23.5F, -1.5F);

		gunModel[9].addShapeBox(0F, 0F, 0F, 32, 2, 10, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import body1
		gunModel[9].setRotationPoint(-12F, -23F, -5F);

		gunModel[10].addShapeBox(0F, 0F, 0F, 6, 1, 1, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import body10
		gunModel[10].setRotationPoint(-2F, -18F, 5F);

		gunModel[11].addShapeBox(0F, 0F, 0F, 6, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Import body10-2
		gunModel[11].setRotationPoint(-2F, -13F, 5F);

		gunModel[12].addBox(0F, 0F, 0F, 1, 4, 1, 0F); // Import body11
		gunModel[12].setRotationPoint(3F, -17F, 5F);

		gunModel[13].addBox(0F, 0F, 0F, 1, 4, 1, 0F); // Import body11-2
		gunModel[13].setRotationPoint(-2F, -17F, 5F);

		gunModel[14].addBox(0F, 0F, 0F, 7, 4, 1, 0F); // Import body12
		gunModel[14].setRotationPoint(-2F, -20F, -6F);

		gunModel[15].addBox(0F, 0F, 0F, 2, 3, 1, 0F); // Import body13
		gunModel[15].setRotationPoint(-1F, -20F, -7F);

		gunModel[16].addBox(0F, 0F, 0F, 2, 3, 1, 0F); // Import body13-2
		gunModel[16].setRotationPoint(2F, -20F, -7F);

		gunModel[17].addShapeBox(0F, 0F, 0F, 34, 1, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Import body14
		gunModel[17].setRotationPoint(-12F, -8F, -5F);

		gunModel[18].addBox(0F, 0F, 0F, 16, 3, 8, 0F); // Import body15
		gunModel[18].setRotationPoint(4F, -7F, -4F);

		gunModel[19].addBox(0F, 0F, 0F, 12, 1, 3, 0F); // Import body2
		gunModel[19].setRotationPoint(6F, -21F, -5F);

		gunModel[20].addBox(0F, 0F, 0F, 18, 13, 3, 0F); // Import body3
		gunModel[20].setRotationPoint(-12F, -21F, -5F);

		gunModel[21].addBox(0F, 0F, 0F, 12, 8, 3, 0F); // Import body4
		gunModel[21].setRotationPoint(6F, -16F, -5F);

		gunModel[22].addBox(0F, 0F, 0F, 4, 13, 3, 0F); // Import body5
		gunModel[22].setRotationPoint(18F, -21F, -5F);

		gunModel[23].addBox(0F, 0F, 0F, 34, 13, 7, 0F); // Import body6
		gunModel[23].setRotationPoint(-12F, -21F, -2F);

		gunModel[24].addBox(0F, 0F, 0F, 2, 1, 10, 0F); // Import body7
		gunModel[24].setRotationPoint(20F, -22F, -5F);

		gunModel[25].addShapeBox(0F, 0F, 0F, 2, 2, 10, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import body8
		gunModel[25].setRotationPoint(20F, -24F, -5F);

		gunModel[26].addBox(0F, 0F, 0F, 14, 6, 1, 0F); // Import body9
		gunModel[26].setRotationPoint(6F, -19F, 4.5F);

		gunModel[27].addShapeBox(0F, 0F, 0F, 11, 11, 1, 0F, 0F, 0F, 0F, -7F, 0F, 0F, -7F, 0F, 0F, 0F, 0F, 0F, 0F, -7F, 0F, -7F, -7F, 0F, -7F, -7F, 0F, 0F, -7F, 0F); // Import bodyMeterScreen
		gunModel[27].setRotationPoint(-1F, -17F, 4.7F);

		gunModel[28].addShapeBox(0F, 0F, 0F, 4, 4, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F); // Import grip1
		gunModel[28].setRotationPoint(-4F, -7F, -3F);

		gunModel[29].addShapeBox(0F, 0F, 0F, 8, 4, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F); // Import grip2
		gunModel[29].setRotationPoint(-12F, -7F, -3F);

		gunModel[30].addShapeBox(0F, 0F, 0F, 10, 11, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 2F, 0F, -2F, 2F, 0F, 2F, 0F, 0F); // Import grip3
		gunModel[30].setRotationPoint(-15F, -3F, -3F);

		gunModel[31].addShapeBox(0F, 0F, 0F, 9, 1, 5, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F); // Import grip4
		gunModel[31].setRotationPoint(-16.5F, 8F, -2.5F);

		gunModel[32].addShapeBox(0F, 0F, 0F, 60, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Import mainBarrelBottom
		gunModel[32].setRotationPoint(22F, -17F, -3.5F);

		gunModel[33].addBox(0F, 0F, 0F, 60, 3, 7, 0F); // Import mainBarrelMiddle
		gunModel[33].setRotationPoint(22F, -20F, -3.5F);

		gunModel[34].addShapeBox(0F, 0F, 0F, 60, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import mainBarrelTop
		gunModel[34].setRotationPoint(22F, -22F, -3.5F);

		gunModel[35].addBox(0F, 0F, 0F, 18, 1, 4, 0F); // Import rail1
		gunModel[35].setRotationPoint(0F, -24F, -2F);

		gunModel[36].addBox(0F, 0F, 0F, 18, 1, 6, 0F); // Import rail2
		gunModel[36].setRotationPoint(0F, -25F, -3F);

		gunModel[37].addShapeBox(0F, 0F, 0F, 18, 1, 1, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import rail3
		gunModel[37].setRotationPoint(0F, -26F, 2F);

		gunModel[38].addShapeBox(0F, 0F, 0F, 18, 1, 1, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import rail3-2
		gunModel[38].setRotationPoint(0F, -26F, -3F);

		gunModel[39].addShapeBox(0F, 0F, 0F, 2, 1, 8, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F); // Import ringPart1-1
		gunModel[39].setRotationPoint(72F, -16F, -4F);

		gunModel[40].addShapeBox(0F, 0F, 0F, 2, 1, 8, 0F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F); // Import ringPart1-2
		gunModel[40].setRotationPoint(72F, -22F, -4F);

		gunModel[41].addShapeBox(0F, 0F, 0F, 2, 1, 8, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F); // Import ringPart1-3
		gunModel[41].setRotationPoint(77F, -16F, -4F);

		gunModel[42].addShapeBox(0F, 0F, 0F, 2, 1, 8, 0F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F); // Import ringPart1-4
		gunModel[42].setRotationPoint(77F, -22F, -4F);

		gunModel[43].addBox(0F, 0F, 0F, 2, 4, 8, 0F); // Import ringPart2-1
		gunModel[43].setRotationPoint(72F, -20.5F, -4F);

		gunModel[44].addBox(0F, 0F, 0F, 2, 4, 8, 0F); // Import ringPart2-2
		gunModel[44].setRotationPoint(77F, -20.5F, -4F);

		gunModel[45].addShapeBox(0F, 0F, 0F, 2, 8, 1, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import steamPipe1-1
		gunModel[45].setRotationPoint(2F, -28F, -7F);

		gunModel[46].addShapeBox(0F, 0F, 0F, 2, 8, 1, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import steamPipe1-2
		gunModel[46].setRotationPoint(-1F, -28F, -7F);

		gunModel[47].addShapeBox(0F, 0F, 0F, 2, 8, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Import steamPipe1-3
		gunModel[47].setRotationPoint(2F, -28F, -6F);

		gunModel[48].addShapeBox(0F, 0F, 0F, 2, 8, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Import steamPipe1-4
		gunModel[48].setRotationPoint(-1F, -28F, -6F);


		defaultScopeModel = new ModelRendererTurbo[7];
		defaultScopeModel[0] = new ModelRendererTurbo(this, 1, 10, textureX, textureY); // Import ironSight1
		defaultScopeModel[1] = new ModelRendererTurbo(this, 30, 5, textureX, textureY); // Import ironSight1-2
		defaultScopeModel[2] = new ModelRendererTurbo(this, 15, 1, textureX, textureY); // Import ironSight2
		defaultScopeModel[3] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Import ironSight3-1
		defaultScopeModel[4] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Import ironSight3-2
		defaultScopeModel[5] = new ModelRendererTurbo(this, 12, 9, textureX, textureY); // Import ironSight4
		defaultScopeModel[6] = new ModelRendererTurbo(this, 6, 1, textureX, textureY); // Import ironSight5

		defaultScopeModel[0].addBox(0F, 0F, 0F, 1, 2, 4, 0F); // Import ironSight1
		defaultScopeModel[0].setRotationPoint(-8F, -25F, -2F);

		defaultScopeModel[1].addShapeBox(0F, 0F, 0F, 1, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F); // Import ironSight1-2
		defaultScopeModel[1].setRotationPoint(-8F, -32F, -1F);

		defaultScopeModel[2].addBox(0F, 0F, 0F, 2, 2, 5, 0F); // Import ironSight2
		defaultScopeModel[2].setRotationPoint(-8.5F, -27.5F, -2.5F);

		defaultScopeModel[3].addBox(0F, 0F, 0F, 1, 6, 1, 0F); // Import ironSight3-1
		defaultScopeModel[3].setRotationPoint(-8F, -31F, -2F);

		defaultScopeModel[4].addBox(0F, 0F, 0F, 1, 6, 1, 0F); // Import ironSight3-2
		defaultScopeModel[4].setRotationPoint(-8F, -31F, 1F);

		defaultScopeModel[5].addShapeBox(0F, 0F, 0F, 8, 2, 5, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import ironSight4
		defaultScopeModel[5].setRotationPoint(-9F, -24.5F, -2.5F);

		defaultScopeModel[6].addShapeBox(0F, 0F, 0F, 2, 5, 2, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import ironSight5
		defaultScopeModel[6].setRotationPoint(77F, -27.5F, -1F);


		defaultStockModel = new ModelRendererTurbo[9];
		defaultStockModel[0] = new ModelRendererTurbo(this, 1, 111, textureX, textureY); // Import stock1
		defaultStockModel[1] = new ModelRendererTurbo(this, 68, 139, textureX, textureY); // Import stock10
		defaultStockModel[2] = new ModelRendererTurbo(this, 41, 138, textureX, textureY); // Import stock2
		defaultStockModel[3] = new ModelRendererTurbo(this, 1, 94, textureX, textureY); // Import stock3
		defaultStockModel[4] = new ModelRendererTurbo(this, 1, 124, textureX, textureY); // Import stock5
		defaultStockModel[5] = new ModelRendererTurbo(this, 41, 152, textureX, textureY); // Import stock6
		defaultStockModel[6] = new ModelRendererTurbo(this, 26, 144, textureX, textureY); // Import stock7
		defaultStockModel[7] = new ModelRendererTurbo(this, 1, 139, textureX, textureY); // Import stock8
		defaultStockModel[8] = new ModelRendererTurbo(this, 70, 153, textureX, textureY); // Import stock9

		defaultStockModel[0].addBox(0F, 0F, 0F, 20, 4, 8, 0F); // Import stock1
		defaultStockModel[0].setRotationPoint(-41F, -18F, -4F);

		defaultStockModel[1].addShapeBox(0F, 0F, 0F, 2, 2, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Import stock10
		defaultStockModel[1].setRotationPoint(-44F, -1F, -5F);

		defaultStockModel[2].addBox(0F, 0F, 0F, 2, 2, 11, 0F); // Import stock2
		defaultStockModel[2].setRotationPoint(-13F, -12F, -5.5F);

		defaultStockModel[3].addShapeBox(0F, 0F, 0F, 29, 6, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 5F, 0F); // Import stock3
		defaultStockModel[3].setRotationPoint(-41F, -14F, -5F);

		defaultStockModel[4].addShapeBox(0F, 0F, 0F, 20, 3, 10, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import stock5
		defaultStockModel[4].setRotationPoint(-41F, -21F, -5F);

		defaultStockModel[5].addBox(0F, 0F, 0F, 3, 3, 11, 0F); // Import stock6
		defaultStockModel[5].setRotationPoint(-38F, -8F, -5.5F);

		defaultStockModel[6].addBox(0F, 0F, 0F, 1, 16, 6, 0F); // Import stock7
		defaultStockModel[6].setRotationPoint(-42F, -18F, -3F);

		defaultStockModel[7].addBox(0F, 0F, 0F, 2, 17, 10, 0F); // Import stock8
		defaultStockModel[7].setRotationPoint(-44F, -18F, -5F);

		defaultStockModel[8].addShapeBox(0F, 0F, 0F, 2, 3, 10, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import stock9
		defaultStockModel[8].setRotationPoint(-44F, -21F, -5F);


		defaultGripModel = new ModelRendererTurbo[11];
		defaultGripModel[0] = new ModelRendererTurbo(this, 1, 71, textureX, textureY); // Import boiler1
		defaultGripModel[1] = new ModelRendererTurbo(this, 1, 71, textureX, textureY); // Import boiler1-2
		defaultGripModel[2] = new ModelRendererTurbo(this, 1, 58, textureX, textureY); // Import boiler2
		defaultGripModel[3] = new ModelRendererTurbo(this, 2, 84, textureX, textureY); // Import boiler3
		defaultGripModel[4] = new ModelRendererTurbo(this, 2, 84, textureX, textureY); // Import boiler3-2
		defaultGripModel[5] = new ModelRendererTurbo(this, 45, 83, textureX, textureY); // Import boiler4
		defaultGripModel[6] = new ModelRendererTurbo(this, 21, 87, textureX, textureY); // Import boiler5
		defaultGripModel[7] = new ModelRendererTurbo(this, 47, 48, textureX, textureY); // Import ringPart3
		defaultGripModel[8] = new ModelRendererTurbo(this, 47, 48, textureX, textureY); // Import ringPart3-2
		defaultGripModel[9] = new ModelRendererTurbo(this, 47, 48, textureX, textureY); // Import ringPart3-3
		defaultGripModel[10] = new ModelRendererTurbo(this, 1, 45, textureX, textureY); // Import ringPart4

		defaultGripModel[0].addShapeBox(0F, 0F, 0F, 16, 3, 9, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import boiler1
		defaultGripModel[0].setRotationPoint(30F, -14F, -4.5F);

		defaultGripModel[1].addShapeBox(0F, 0F, 0F, 16, 3, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Import boiler1-2
		defaultGripModel[1].setRotationPoint(30F, -8F, -4.5F);

		defaultGripModel[2].addBox(0F, 0F, 0F, 16, 3, 9, 0F); // Import boiler2
		defaultGripModel[2].setRotationPoint(30F, -11F, -4.5F);

		defaultGripModel[3].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Import boiler3
		defaultGripModel[3].setRotationPoint(28F, -8F, -3.5F);

		defaultGripModel[4].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import boiler3-2
		defaultGripModel[4].setRotationPoint(28F, -13F, -3.5F);

		defaultGripModel[5].addBox(0F, 0F, 0F, 2, 3, 7, 0F); // Import boiler4
		defaultGripModel[5].setRotationPoint(28F, -11F, -3.5F);

		defaultGripModel[6].addBox(0F, 0F, 0F, 6, 3, 3, 0F); // Import boiler5
		defaultGripModel[6].setRotationPoint(22F, -11F, -1.5F);

		defaultGripModel[7].addShapeBox(0F, 0F, 0F, 4, 1, 8, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F); // Import ringPart3
		defaultGripModel[7].setRotationPoint(36F, -16F, -4F);

		defaultGripModel[8].addShapeBox(0F, 0F, 0F, 4, 1, 8, 0F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F); // Import ringPart3-2
		defaultGripModel[8].setRotationPoint(36F, -22F, -4F);

		defaultGripModel[9].addShapeBox(0F, 0F, 0F, 4, 1, 8, 0F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F); // Import ringPart3-3
		defaultGripModel[9].setRotationPoint(36F, -14F, -4F);

		defaultGripModel[10].addBox(0F, 0F, 0F, 4, 4, 8, 0F); // Import ringPart4
		defaultGripModel[10].setRotationPoint(36F, -20.5F, -4F);


		ammoModel = new ModelRendererTurbo[10];
		ammoModel[0] = new ModelRendererTurbo(this, 126, 136, textureX, textureY); // Import ammo1
		ammoModel[1] = new ModelRendererTurbo(this, 96, 137, textureX, textureY); // Import ammo2
		ammoModel[2] = new ModelRendererTurbo(this, 96, 137, textureX, textureY); // Import ammo2-2
		ammoModel[3] = new ModelRendererTurbo(this, 109, 136, textureX, textureY); // Import ammo3
		ammoModel[4] = new ModelRendererTurbo(this, 109, 136, textureX, textureY); // Import ammo3-2
		ammoModel[5] = new ModelRendererTurbo(this, 96, 125, textureX, textureY); // Import ammo4
		ammoModel[6] = new ModelRendererTurbo(this, 96, 125, textureX, textureY); // Import ammo4-2
		ammoModel[7] = new ModelRendererTurbo(this, 96, 125, textureX, textureY); // Import ammo4-3
		ammoModel[8] = new ModelRendererTurbo(this, 109, 120, textureX, textureY); // Import bullet
		ammoModel[9] = new ModelRendererTurbo(this, 96, 120, textureX, textureY); // Import bullet2

		ammoModel[0].addBox(0F, 0F, 0F, 8, 20, 6, 0F); // Import ammo1
		ammoModel[0].setRotationPoint(5F, -6F, -3F);

		ammoModel[1].addBox(0F, 0F, 0F, 1, 20, 5, 0F); // Import ammo2
		ammoModel[1].setRotationPoint(13F, -6F, -2.5F);

		ammoModel[2].addBox(0F, 0F, 0F, 1, 20, 5, 0F); // Import ammo2-2
		ammoModel[2].setRotationPoint(16F, -6F, -2.5F);

		ammoModel[3].addBox(0F, 0F, 0F, 2, 20, 6, 0F); // Import ammo3
		ammoModel[3].setRotationPoint(14F, -6F, -3F);

		ammoModel[4].addBox(0F, 0F, 0F, 2, 20, 6, 0F); // Import ammo3-2
		ammoModel[4].setRotationPoint(17F, -6F, -3F);

		ammoModel[5].addBox(0F, 0F, 0F, 15, 3, 7, 0F); // Import ammo4
		ammoModel[5].setRotationPoint(4.5F, -1F, -3.5F);

		ammoModel[6].addShapeBox(0F, 0F, 0F, 15, 3, 7, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F); // Import ammo4-2
		ammoModel[6].setRotationPoint(4.5F, 1F, -3.5F);

		ammoModel[7].addShapeBox(0F, 0F, 0F, 15, 3, 7, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F); // Import ammo4-3
		ammoModel[7].setRotationPoint(4.5F, 6F, -3.5F);

		ammoModel[8].addShapeBox(0F, 0F, 0F, 10, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import bullet
		ammoModel[8].setRotationPoint(5.5F, -7F, -1.5F);

		ammoModel[9].addShapeBox(0F, 0F, 0F, 3, 1, 3, 0F, 0F, 0F, -1F, 0F, -0.7F, -1F, 0F, -0.7F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import bullet2
		ammoModel[9].setRotationPoint(15.5F, -7F, -1.5F);


		slideModel = new ModelRendererTurbo[1];
		slideModel[0] = new ModelRendererTurbo(this, 1, 17, textureX, textureY); // Import bolt

		slideModel[0].addShapeBox(0F, 0F, 0F, 12, 4, 2, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import bolt
		slideModel[0].setRotationPoint(6F, -20F, -4.5F);

		barrelAttachPoint = new Vector3f(82F /16F, 18.5F /16F, 0F /16F);
		scopeAttachPoint = new Vector3f(10F /16F, 23F /16F, 0F /16F);
		gripAttachPoint = new Vector3f(22 /16F, 16F /16F, 0F /16F);

		gunSlideDistance = 0.5F;
		animationType = EnumAnimationType.BOTTOM_CLIP;


		translateAll(0F, 0F, 0F);


		flipAll();
	}
}
