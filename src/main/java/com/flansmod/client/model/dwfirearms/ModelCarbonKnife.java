package com.flansmod.client.model.dwfirearms; //Path where the model is located

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelCarbonKnife extends ModelGun
{
	int textureX = 512;
	int textureY = 256;

	public ModelCarbonKnife()
	{
		gunModel = new ModelRendererTurbo[23];
		gunModel[0] = new ModelRendererTurbo(this, 1, 56, textureX, textureY); // Box 1
		gunModel[1] = new ModelRendererTurbo(this, 54, 56, textureX, textureY); // Box 2
		gunModel[2] = new ModelRendererTurbo(this, 54, 56, textureX, textureY); // Box 3
		gunModel[3] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 4
		gunModel[4] = new ModelRendererTurbo(this, 26, 1, textureX, textureY); // Box 5
		gunModel[5] = new ModelRendererTurbo(this, 1, 68, textureX, textureY); // Box 22
		gunModel[6] = new ModelRendererTurbo(this, 32, 68, textureX, textureY); // Box 23
		gunModel[7] = new ModelRendererTurbo(this, 49, 96, textureX, textureY); // Box 24
		gunModel[8] = new ModelRendererTurbo(this, 49, 78, textureX, textureY); // Box 27
		gunModel[9] = new ModelRendererTurbo(this, 49, 87, textureX, textureY); // Box 28
		gunModel[10] = new ModelRendererTurbo(this, 49, 87, textureX, textureY); // Box 29
		gunModel[11] = new ModelRendererTurbo(this, 49, 68, textureX, textureY); // Box 30
		gunModel[12] = new ModelRendererTurbo(this, 108, 60, textureX, textureY); // Box 38
		gunModel[13] = new ModelRendererTurbo(this, 108, 60, textureX, textureY); // Box 39
		gunModel[14] = new ModelRendererTurbo(this, 75, 60, textureX, textureY); // Box 40
		gunModel[15] = new ModelRendererTurbo(this, 49, 87, textureX, textureY); // Box 0
		gunModel[16] = new ModelRendererTurbo(this, 49, 87, textureX, textureY); // Box 1
		gunModel[17] = new ModelRendererTurbo(this, 49, 78, textureX, textureY); // Box 2
		gunModel[18] = new ModelRendererTurbo(this, 49, 87, textureX, textureY); // Box 3
		gunModel[19] = new ModelRendererTurbo(this, 49, 68, textureX, textureY); // Box 4
		gunModel[20] = new ModelRendererTurbo(this, 49, 87, textureX, textureY); // Box 5
		gunModel[21] = new ModelRendererTurbo(this, 49, 78, textureX, textureY); // Box 6
		gunModel[22] = new ModelRendererTurbo(this, 49, 68, textureX, textureY); // Box 7

		gunModel[0].addBox(-15F, -25F, -4F, 18, 3, 8, 0F); // Box 1
		gunModel[0].setRotationPoint(5F, 11F, 0F);
		gunModel[0].rotateAngleZ = 0.12217305F;

		gunModel[1].addShapeBox(-17F, -25F, -4F, 2, 3, 8, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 2
		gunModel[1].setRotationPoint(5F, 11F, 0F);
		gunModel[1].rotateAngleZ = 0.12217305F;

		gunModel[2].addShapeBox(3F, -25F, -4F, 2, 3, 8, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 3
		gunModel[2].setRotationPoint(5F, 11F, 0F);
		gunModel[2].rotateAngleZ = 0.12217305F;

		gunModel[3].addBox(-13F, -75F, -2F, 8, 50, 4, 0F); // Box 4
		gunModel[3].setRotationPoint(5F, 11F, 0F);
		gunModel[3].rotateAngleZ = 0.12217305F;

		gunModel[4].addShapeBox(-5F, -75F, -2F, 6, 50, 4, 0F, 0F, 0F, 0F, 0F, -8F, -1.5F, 0F, -8F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F); // Box 5
		gunModel[4].setRotationPoint(5F, 11F, 0F);
		gunModel[4].rotateAngleZ = 0.12217305F;

		gunModel[5].addShapeBox(-11F, -22F, -3F, 9, 29, 6, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F); // Box 22
		gunModel[5].setRotationPoint(5F, 11F, 0F);
		gunModel[5].rotateAngleZ = 0.12217305F;

		gunModel[6].addShapeBox(-13F, -22F, -3F, 2, 29, 6, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 23
		gunModel[6].setRotationPoint(5F, 11F, 0F);
		gunModel[6].rotateAngleZ = 0.12217305F;

		gunModel[7].addShapeBox(-1F, -22F, -3F, 2, 2, 6, 0F, 0F, 0F, 0F, 1F, 0F, -1F, 1F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 24
		gunModel[7].setRotationPoint(5F, 11F, 0F);
		gunModel[7].rotateAngleZ = 0.12217305F;

		gunModel[8].addShapeBox(-1F, -13F, -3F, 2, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 27
		gunModel[8].setRotationPoint(5F, 11F, 0F);
		gunModel[8].rotateAngleZ = 0.12217305F;

		gunModel[9].addShapeBox(-1F, -20F, -3F, 2, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -1F, -1F, 0F, -1F, 0F, 0F, 0F); // Box 28
		gunModel[9].setRotationPoint(5F, 11F, 0F);
		gunModel[9].rotateAngleZ = 0.12217305F;

		gunModel[10].addShapeBox(-1F, -15F, -3F, 2, 2, 6, 0F, 0F, 0F, 0F, -1F, 0F, -1F, -1F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 29
		gunModel[10].setRotationPoint(5F, 11F, 0F);
		gunModel[10].rotateAngleZ = 0.12217305F;

		gunModel[11].addShapeBox(-1F, -18F, -3F, 1, 3, 6, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 30
		gunModel[11].setRotationPoint(5F, 11F, 0F);
		gunModel[11].rotateAngleZ = 0.12217305F;

		gunModel[12].addShapeBox(-13F, 7F, -3F, 2, 1, 6, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -2F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -2F); // Box 38
		gunModel[12].setRotationPoint(5F, 11F, 0F);
		gunModel[12].rotateAngleZ = 0.12217305F;

		gunModel[13].addShapeBox(-1F, 7F, -3F, 2, 1, 6, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -1F); // Box 39
		gunModel[13].setRotationPoint(5F, 11F, 0F);
		gunModel[13].rotateAngleZ = 0.12217305F;

		gunModel[14].addShapeBox(-11F, 7F, -3F, 10, 1, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 40
		gunModel[14].setRotationPoint(5F, 11F, 0F);
		gunModel[14].rotateAngleZ = 0.12217305F;

		gunModel[15].addShapeBox(-1F, -11F, -3F, 2, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -1F, -1F, 0F, -1F, 0F, 0F, 0F); // Box 0
		gunModel[15].setRotationPoint(5F, 11F, 0F);
		gunModel[15].rotateAngleZ = 0.12217305F;

		gunModel[16].addShapeBox(-1F, -6F, -3F, 2, 2, 6, 0F, 0F, 0F, 0F, -1F, 0F, -1F, -1F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 1
		gunModel[16].setRotationPoint(5F, 11F, 0F);
		gunModel[16].rotateAngleZ = 0.12217305F;

		gunModel[17].addShapeBox(-1F, -4F, -3F, 2, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 2
		gunModel[17].setRotationPoint(5F, 11F, 0F);
		gunModel[17].rotateAngleZ = 0.12217305F;

		gunModel[18].addShapeBox(-1F, -2F, -3F, 2, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -1F, -1F, 0F, -1F, 0F, 0F, 0F); // Box 3
		gunModel[18].setRotationPoint(5F, 11F, 0F);
		gunModel[18].rotateAngleZ = 0.12217305F;

		gunModel[19].addShapeBox(-1F, -9F, -3F, 1, 3, 6, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 4
		gunModel[19].setRotationPoint(5F, 11F, 0F);
		gunModel[19].rotateAngleZ = 0.12217305F;

		gunModel[20].addShapeBox(-1F, 3F, -3F, 2, 2, 6, 0F, 0F, 0F, 0F, -1F, 0F, -1F, -1F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 5
		gunModel[20].setRotationPoint(5F, 11F, 0F);
		gunModel[20].rotateAngleZ = 0.12217305F;

		gunModel[21].addShapeBox(-1F, 5F, -3F, 2, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 6
		gunModel[21].setRotationPoint(5F, 11F, 0F);
		gunModel[21].rotateAngleZ = 0.12217305F;

		gunModel[22].addShapeBox(-1F, 0F, -3F, 1, 3, 6, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 7
		gunModel[22].setRotationPoint(5F, 11F, 0F);
		gunModel[22].rotateAngleZ = 0.12217305F;

		translateAll(0F, 0F, 0F);


		flipAll();
	}
}