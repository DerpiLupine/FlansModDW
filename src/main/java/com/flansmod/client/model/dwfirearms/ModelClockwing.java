package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelClockwing extends ModelGun
{
	int textureX = 512;
	int textureY = 256;

	public ModelClockwing()
	{
		gunModel = new ModelRendererTurbo[41];
		gunModel[0] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Import barrelLeft
		gunModel[1] = new ModelRendererTurbo(this, 1, 11, textureX, textureY); // Import barrelMiddle
		gunModel[2] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Import barrelRight
		gunModel[3] = new ModelRendererTurbo(this, 151, 1, textureX, textureY); // Import body1
		gunModel[4] = new ModelRendererTurbo(this, 125, 16, textureX, textureY); // Import body10
		gunModel[5] = new ModelRendererTurbo(this, 125, 21, textureX, textureY); // Import body12
		gunModel[6] = new ModelRendererTurbo(this, 71, 62, textureX, textureY); // Import body2
		gunModel[7] = new ModelRendererTurbo(this, 101, 109, textureX, textureY); // Import body3
		gunModel[8] = new ModelRendererTurbo(this, 105, 93, textureX, textureY); // Import body4
		gunModel[9] = new ModelRendererTurbo(this, 72, 109, textureX, textureY); // Import body5
		gunModel[10] = new ModelRendererTurbo(this, 72, 95, textureX, textureY); // Import body6
		gunModel[11] = new ModelRendererTurbo(this, 105, 75, textureX, textureY); // Import body7
		gunModel[12] = new ModelRendererTurbo(this, 72, 75, textureX, textureY); // Import body8
		gunModel[13] = new ModelRendererTurbo(this, 101, 28, textureX, textureY); // Import body9
		gunModel[14] = new ModelRendererTurbo(this, 72, 28, textureX, textureY); // Import bodyGrip10
		gunModel[15] = new ModelRendererTurbo(this, 72, 16, textureX, textureY); // Import boltPart
		gunModel[16] = new ModelRendererTurbo(this, 72, 46, textureX, textureY); // Import boltPart3
		gunModel[17] = new ModelRendererTurbo(this, 72, 1, textureX, textureY); // Import boltPart4
		gunModel[18] = new ModelRendererTurbo(this, 125, 26, textureX, textureY); // Import body11
		gunModel[19] = new ModelRendererTurbo(this, 47, 85, textureX, textureY); // Import connectorPart1
		gunModel[20] = new ModelRendererTurbo(this, 1, 92, textureX, textureY); // Import connectorPart2
		gunModel[21] = new ModelRendererTurbo(this, 47, 72, textureX, textureY); // Import connectorPart3
		gunModel[22] = new ModelRendererTurbo(this, 28, 22, textureX, textureY); // Import gasBlockLeft
		gunModel[23] = new ModelRendererTurbo(this, 28, 22, textureX, textureY); // Import gasBlockMiddle
		gunModel[24] = new ModelRendererTurbo(this, 28, 22, textureX, textureY); // Import gasBlockRight
		gunModel[25] = new ModelRendererTurbo(this, 37, 76, textureX, textureY); // Import ironSight1
		gunModel[26] = new ModelRendererTurbo(this, 37, 76, textureX, textureY); // Import ironSight2
		gunModel[27] = new ModelRendererTurbo(this, 1, 34, textureX, textureY); // Import sideCanister1
		gunModel[28] = new ModelRendererTurbo(this, 1, 34, textureX, textureY); // Import sideCanister2
		gunModel[29] = new ModelRendererTurbo(this, 14, 75, textureX, textureY); // Import sideCanister3
		gunModel[30] = new ModelRendererTurbo(this, 30, 31, textureX, textureY); // Import steamPipeCanister1
		gunModel[31] = new ModelRendererTurbo(this, 30, 31, textureX, textureY); // Import steamPipeCanister2
		gunModel[32] = new ModelRendererTurbo(this, 39, 31, textureX, textureY); // Import steamPipeTop
		gunModel[33] = new ModelRendererTurbo(this, 39, 31, textureX, textureY); // Import steamPipeTop2
		gunModel[34] = new ModelRendererTurbo(this, 74, 123, textureX, textureY); // Import wingPart1
		gunModel[35] = new ModelRendererTurbo(this, 74, 127, textureX, textureY); // Import wingPart2
		gunModel[36] = new ModelRendererTurbo(this, 74, 131, textureX, textureY); // Import wingPart3
		gunModel[37] = new ModelRendererTurbo(this, 74, 135, textureX, textureY); // Import wingPart4
		gunModel[38] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Import Box69
		gunModel[39] = new ModelRendererTurbo(this, 1, 11, textureX, textureY); // Import Box70
		gunModel[40] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Import Box71

		gunModel[0].addShapeBox(0F, 0F, 0F, 25, 7, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F); // Import barrelLeft
		gunModel[0].setRotationPoint(55F, -24.5F, 1.5F);

		gunModel[1].addBox(0F, 0F, 0F, 25, 7, 3, 0F); // Import barrelMiddle
		gunModel[1].setRotationPoint(55F, -24.5F, -1.5F);

		gunModel[2].addShapeBox(0F, 0F, 0F, 25, 7, 2, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import barrelRight
		gunModel[2].setRotationPoint(55F, -24.5F, -3.5F);

		gunModel[3].addBox(0F, 0F, 0F, 34, 12, 10, 0F); // Import body1
		gunModel[3].setRotationPoint(-10F, -22F, -5F);

		gunModel[4].addShapeBox(0F, 0F, 0F, 8, 2, 2, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import body10
		gunModel[4].setRotationPoint(-5F, -18F, -6.5F);

		gunModel[5].addShapeBox(0F, 0F, 0F, 8, 2, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F); // Import body12
		gunModel[5].setRotationPoint(-5F, -12F, -6.5F);

		gunModel[6].addShapeBox(0F, 0F, 0F, 26, 2, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Import body2
		gunModel[6].setRotationPoint(-2F, -10F, -5F);

		gunModel[7].addShapeBox(0F, 0F, 0F, 6, 3, 10, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import body3
		gunModel[7].setRotationPoint(-10F, -25F, -5F);

		gunModel[8].addShapeBox(0F, 0F, 0F, 4, 4, 11, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import body4
		gunModel[8].setRotationPoint(20F, -26F, -5.5F);

		gunModel[9].addShapeBox(0F, 0F, 0F, 4, 3, 10, 0F, -2F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, -2F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import body5
		gunModel[9].setRotationPoint(-14F, -25F, -5F);

		gunModel[10].addShapeBox(0F, 0F, 0F, 6, 3, 10, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import body6
		gunModel[10].setRotationPoint(-16F, -22F, -5F);

		gunModel[11].addShapeBox(0F, 0F, 0F, 7, 7, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, -5F, -2F, 0F, -5F, -2F, 0F, 0F, -2F); // Import body7
		gunModel[11].setRotationPoint(-9F, -10F, -5F);

		gunModel[12].addBox(0F, 0F, 0F, 6, 9, 10, 0F); // Import body8
		gunModel[12].setRotationPoint(-16F, -19F, -5F);

		gunModel[13].addShapeBox(0F, 0F, 0F, 3, 7, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -5F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, -5F, -2F); // Import body9
		gunModel[13].setRotationPoint(-16F, -10F, -5F);

		gunModel[14].addShapeBox(0F, 0F, 0F, 4, 7, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Import bodyGrip10
		gunModel[14].setRotationPoint(-13F, -10F, -5F);

		gunModel[15].addShapeBox(0F, 0F, 0F, 18, 3, 8, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import boltPart
		gunModel[15].setRotationPoint(2F, -25F, -4F);

		gunModel[16].addShapeBox(0F, 0F, 0F, 6, 4, 11, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import boltPart3
		gunModel[16].setRotationPoint(-4F, -26F, -5.5F);

		gunModel[17].addBox(0F, 0F, 0F, 28, 3, 11, 0F); // Import boltPart4
		gunModel[17].setRotationPoint(-4F, -22.05F, -5.5F);

		gunModel[18].addBox(0F, 0F, 0F, 8, 4, 2, 0F); // Import body11
		gunModel[18].setRotationPoint(-5F, -16F, -6.5F);

		gunModel[19].addShapeBox(0F, 0F, 0F, 1, 3, 10, 0F, 0F, 0.25F, -1.75F, 0F, 0.25F, -1.75F, 0F, 0.25F, -1.75F, 0F, 0.25F, -1.75F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F); // Import connectorPart1
		gunModel[19].setRotationPoint(24F, -25F, -5F);

		gunModel[20].addShapeBox(0F, 0F, 0F, 1, 12, 10, 0F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F); // Import connectorPart2
		gunModel[20].setRotationPoint(24F, -22F, -5F);

		gunModel[21].addShapeBox(0F, 0F, 0F, 1, 2, 10, 0F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0.25F, -1.75F, 0F, 0.25F, -1.75F, 0F, 0.25F, -1.75F, 0F, 0.25F, -1.75F); // Import connectorPart3
		gunModel[21].setRotationPoint(24F, -10F, -5F);

		gunModel[22].addShapeBox(0F, 0F, 0F, 8, 6, 2, 0F, 0F, 0F, 0F, 8F, 0F, 0F, 8F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 8F, 0F, 0F, 8F, -2F, 0F, 0F, -2F, 0F); // Import gasBlockLeft
		gunModel[22].setRotationPoint(54F, -17F, 1F);

		gunModel[23].addShapeBox(0F, 0F, 0F, 8, 6, 2, 0F, 0F, 0F, 0F, 8F, 0F, 0F, 8F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 8F, 0F, 0F, 8F, 0F, 0F, 0F, 0F, 0F); // Import gasBlockMiddle
		gunModel[23].setRotationPoint(54F, -17F, -1F);

		gunModel[24].addShapeBox(0F, 0F, 0F, 8, 6, 2, 0F, 0F, -2F, 0F, 8F, -2F, 0F, 8F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 8F, -2F, 0F, 8F, 0F, 0F, 0F, 0F, 0F); // Import gasBlockRight
		gunModel[24].setRotationPoint(54F, -17F, -3F);

		gunModel[25].addShapeBox(0F, 0F, 0F, 4, 3, 2, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import ironSight1
		gunModel[25].setRotationPoint(-3F, -29F, -3F);

		gunModel[26].addShapeBox(0F, 0F, 0F, 4, 3, 2, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import ironSight2
		gunModel[26].setRotationPoint(-3F, -29F, 1F);

		gunModel[27].addShapeBox(0F, 0F, 0F, 12, 8, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F); // Import sideCanister1
		gunModel[27].setRotationPoint(-7F, -18F, 7F);

		gunModel[28].addBox(0F, 0F, 0F, 12, 8, 2, 0F); // Import sideCanister2
		gunModel[28].setRotationPoint(-7F, -18F, 5F);

		gunModel[29].addBox(0F, 0F, 0F, 3, 3, 3, 0F); // Import sideCanister3
		gunModel[29].setRotationPoint(4.5F, -15.5F, 5.5F);

		gunModel[30].addShapeBox(0F, 0F, 0F, 1, 12, 3, 0F, 0F, 0F, 0F, 0.2F, 0F, -1F, 0.2F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0.2F, 0F, -1F, 0.2F, 0F, -1F, 0F, 0F, 0F); // Import steamPipeCanister1
		gunModel[30].setRotationPoint(6.2F, -27.5F, 5.5F);

		gunModel[31].addShapeBox(0F, 0F, 0F, 1, 12, 3, 0F, 0.2F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0.2F, 0F, -1F, 0.2F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0.2F, 0F, -1F); // Import steamPipeCanister2
		gunModel[31].setRotationPoint(5.2F, -27.5F, 5.5F);

		gunModel[32].addShapeBox(0F, 0F, 0F, 1, 4, 4, 0F, 0F, 0F, 0F, 0.7F, 0F, -1F, 0.7F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0.7F, 0F, -1F, 0.7F, 0F, -1F, 0F, 0F, 0F); // Import steamPipeTop
		gunModel[32].setRotationPoint(6.2F, -31.5F, 5F);

		gunModel[33].addShapeBox(0F, 0F, 0F, 1, 4, 4, 0F, 0.7F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0.7F, 0F, -1F, 0.7F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0.7F, 0F, -1F); // Import steamPipeTop2
		gunModel[33].setRotationPoint(5.2F, -31.5F, 5F);

		gunModel[34].addShapeBox(0F, 0F, 0F, 20, 2, 1, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, -4F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 2F, 0F); // Import wingPart1
		gunModel[34].setRotationPoint(-23F, -17F, -6F);

		gunModel[35].addShapeBox(0F, 0F, 0F, 18, 2, 1, 0F, 0F, -4.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4.5F, 0F, -4F, 4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 4F, 0F); // Import wingPart2
		gunModel[35].setRotationPoint(-21F, -16F, -6F);

		gunModel[36].addShapeBox(0F, 0F, 0F, 14, 2, 1, 0F, 0F, -6F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -6F, 0F, -4F, 4.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 4.5F, 0F); // Import wingPart3
		gunModel[36].setRotationPoint(-17F, -15F, -6F);

		gunModel[37].addShapeBox(0F, 0F, 0F, 10, 2, 1, 0F, 0F, -7F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -7F, 0F, -4F, 4.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 4.5F, 0F); // Import wingPart4
		gunModel[37].setRotationPoint(-13F, -14F, -6F);

		gunModel[38].addShapeBox(0F, 0F, 0F, 25, 7, 2, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import Box69
		gunModel[38].setRotationPoint(80F, -24.5F, -3.5F);

		gunModel[39].addBox(0F, 0F, 0F, 25, 7, 3, 0F); // Import Box70
		gunModel[39].setRotationPoint(80F, -24.5F, -1.5F);

		gunModel[40].addShapeBox(0F, 0F, 0F, 25, 7, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F); // Import Box71
		gunModel[40].setRotationPoint(80F, -24.5F, 1.5F);


		defaultBarrelModel = new ModelRendererTurbo[3];
		defaultBarrelModel[0] = new ModelRendererTurbo(this, 1, 22, textureX, textureY); // Import barrelMuzzleLeft
		defaultBarrelModel[1] = new ModelRendererTurbo(this, 1, 22, textureX, textureY); // Import barrelMuzzleMiddle
		defaultBarrelModel[2] = new ModelRendererTurbo(this, 1, 22, textureX, textureY); // Import barrelMuzzleRight

		defaultBarrelModel[0].addShapeBox(0F, 0F, 0F, 10, 8, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, -0.5F, 0F, -2F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, -0.5F, 0F, -2F, -0.5F); // Import barrelMuzzleLeft
		defaultBarrelModel[0].setRotationPoint(98F, -25F, 1.5F);

		defaultBarrelModel[1].addBox(0F, 0F, 0F, 10, 8, 3, 0F); // Import barrelMuzzleMiddle
		defaultBarrelModel[1].setRotationPoint(98F, -25F, -1.5F);

		defaultBarrelModel[2].addShapeBox(0F, 0F, 0F, 10, 8, 3, 0F, 0F, -2F, -0.5F, 0F, -2F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, -0.5F, 0F, -2F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Import barrelMuzzleRight
		defaultBarrelModel[2].setRotationPoint(98F, -25F, -4.5F);


		defaultStockModel = new ModelRendererTurbo[11];
		defaultStockModel[0] = new ModelRendererTurbo(this, 1, 115, textureX, textureY); // Import stockPart1
		defaultStockModel[1] = new ModelRendererTurbo(this, 1, 203, textureX, textureY); // Import stockPart10
		defaultStockModel[2] = new ModelRendererTurbo(this, 48, 160, textureX, textureY); // Import stockPart11
		defaultStockModel[3] = new ModelRendererTurbo(this, 1, 135, textureX, textureY); // Import stockPart2
		defaultStockModel[4] = new ModelRendererTurbo(this, 1, 148, textureX, textureY); // Import stockPart3
		defaultStockModel[5] = new ModelRendererTurbo(this, 32, 204, textureX, textureY); // Import stockPart4
		defaultStockModel[6] = new ModelRendererTurbo(this, 28, 190, textureX, textureY); // Import stockPart5
		defaultStockModel[7] = new ModelRendererTurbo(this, 1, 177, textureX, textureY); // Import stockPart6
		defaultStockModel[8] = new ModelRendererTurbo(this, 55, 190, textureX, textureY); // Import stockPart7
		defaultStockModel[9] = new ModelRendererTurbo(this, 28, 177, textureX, textureY); // Import stockPart8
		defaultStockModel[10] = new ModelRendererTurbo(this, 1, 160, textureX, textureY); // Import stockPart9

		defaultStockModel[0].addShapeBox(0F, 0F, 0F, 26, 9, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 6F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 6F, 0F); // Import stockPart1
		defaultStockModel[0].setRotationPoint(-42F, -19F, -5F);

		defaultStockModel[1].addShapeBox(0F, 0F, 0F, 4, 8, 11, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F); // Import stockPart10
		defaultStockModel[1].setRotationPoint(-33F, -14.5F, -5.5F);

		defaultStockModel[2].addShapeBox(0F, 0F, 0F, 4, 2, 11, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, 0.8F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0.8F, -2F); // Import stockPart11
		defaultStockModel[2].setRotationPoint(-33F, -6.5F, -5.5F);

		defaultStockModel[3].addShapeBox(0F, 0F, 0F, 26, 2, 10, 0F, 0F, -6F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -6F, 0F, 0F, 6F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 6F, -2F); // Import stockPart2
		defaultStockModel[3].setRotationPoint(-42F, -10F, -5F);

		defaultStockModel[4].addShapeBox(0F, 0F, 0F, 27, 1, 10, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import stockPart3
		defaultStockModel[4].setRotationPoint(-42F, -20F, -5F);

		defaultStockModel[5].addShapeBox(0F, 0F, 0F, 3, 1, 10, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import stockPart4
		defaultStockModel[5].setRotationPoint(-46F, -20F, -5F);

		defaultStockModel[6].addShapeBox(0F, 0F, 0F, 3, 2, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Import stockPart5
		defaultStockModel[6].setRotationPoint(-46F, -4F, -5F);

		defaultStockModel[7].addShapeBox(0F, 0F, 0F, 3, 15, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import stockPart6
		defaultStockModel[7].setRotationPoint(-46F, -19F, -5F);

		defaultStockModel[8].addShapeBox(0F, 0F, 0F, 1, 16, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import stockPart7
		defaultStockModel[8].setRotationPoint(-43F, -18.5F, -3.5F);

		defaultStockModel[9].addShapeBox(0F, 0F, 0F, 12, 1, 11, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import stockPart8
		defaultStockModel[9].setRotationPoint(-37F, -20.5F, -5.5F);

		defaultStockModel[10].addBox(0F, 0F, 0F, 12, 5, 11, 0F); // Import stockPart9
		defaultStockModel[10].setRotationPoint(-37F, -19.5F, -5.5F);


		defaultGripModel = new ModelRendererTurbo[10];
		defaultGripModel[0] = new ModelRendererTurbo(this, 1, 45, textureX, textureY); // Import beltStrap1
		defaultGripModel[1] = new ModelRendererTurbo(this, 30, 48, textureX, textureY); // Import beltStrap2
		defaultGripModel[2] = new ModelRendererTurbo(this, 1, 59, textureX, textureY); // Import beltStrap3
		defaultGripModel[3] = new ModelRendererTurbo(this, 24, 85, textureX, textureY); // Import connectorPartFront1
		defaultGripModel[4] = new ModelRendererTurbo(this, 24, 72, textureX, textureY); // Import connectorPartFront3
		defaultGripModel[5] = new ModelRendererTurbo(this, 1, 72, textureX, textureY); // Import connectPartFront2
		defaultGripModel[6] = new ModelRendererTurbo(this, 151, 24, textureX, textureY); // Import foreRail1
		defaultGripModel[7] = new ModelRendererTurbo(this, 151, 61, textureX, textureY); // Import foreRail2
		defaultGripModel[8] = new ModelRendererTurbo(this, 151, 47, textureX, textureY); // Import foreRail3
		defaultGripModel[9] = new ModelRendererTurbo(this, 20, 49, textureX, textureY); // Import ironSight3

		defaultGripModel[0].addShapeBox(0F, 0F, 0F, 4, 3, 10, 0F, 0F, 0.25F, -1.75F, 0F, 0.25F, -1.75F, 0F, 0.25F, -1.75F, 0F, 0.25F, -1.75F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F); // Import beltStrap1
		defaultGripModel[0].setRotationPoint(28F, -25F, -5F);

		defaultGripModel[1].addShapeBox(0F, 0F, 0F, 4, 12, 10, 0F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F); // Import beltStrap2
		defaultGripModel[1].setRotationPoint(28F, -22F, -5F);

		defaultGripModel[2].addShapeBox(0F, 0F, 0F, 4, 2, 10, 0F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0.25F, -1.75F, 0F, 0F, -1.75F, 0F, 0F, -1.75F, 0F, 0.25F, -1.75F); // Import beltStrap3
		defaultGripModel[2].setRotationPoint(28F, -10F, -5F);

		defaultGripModel[3].addShapeBox(0F, 0F, 0F, 1, 3, 10, 0F, 0F, 0.25F, -1.75F, 0F, 0.25F, -1.75F, 0F, 0.25F, -1.75F, 0F, 0.25F, -1.75F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F); // Import connectorPartFront1
		defaultGripModel[3].setRotationPoint(54F, -25F, -5F);

		defaultGripModel[4].addShapeBox(0F, 0F, 0F, 1, 2, 10, 0F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0.25F, -1.75F, 0F, 0.25F, -1.75F, 0F, 0.25F, -1.75F, 0F, 0.25F, -1.75F); // Import connectorPartFront3
		defaultGripModel[4].setRotationPoint(54F, -13F, -5F);

		defaultGripModel[5].addShapeBox(0F, 0F, 0F, 1, 9, 10, 0F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F); // Import connectPartFront2
		defaultGripModel[5].setRotationPoint(54F, -22F, -5F);

		defaultGripModel[6].addShapeBox(0F, 0F, 0F, 29, 12, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F); // Import foreRail1
		defaultGripModel[6].setRotationPoint(25F, -22F, -5F);

		defaultGripModel[7].addShapeBox(0F, 0F, 0F, 29, 2, 10, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, -3F, -2F, 0F, -3F, -2F, 0F, 0F, -2F); // Import foreRail2
		defaultGripModel[7].setRotationPoint(25F, -10F, -5F);

		defaultGripModel[8].addShapeBox(0F, 0F, 0F, 29, 3, 10, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import foreRail3
		defaultGripModel[8].setRotationPoint(25F, -25F, -5F);

		defaultGripModel[9].addBox(0F, 0F, 0F, 4, 2, 3, 0F); // Import ironSight3
		defaultGripModel[9].setRotationPoint(50F, -27F, -1.5F);


		ammoModel = new ModelRendererTurbo[3];
		ammoModel[0] = new ModelRendererTurbo(this, 24, 110, textureX, textureY); // Import ammoPart1
		ammoModel[1] = new ModelRendererTurbo(this, 24, 110, textureX, textureY); // Import ammoPart3
		ammoModel[2] = new ModelRendererTurbo(this, 24, 110, textureX, textureY); // Import ammoPart2

		ammoModel[0].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import ammoPart1
		ammoModel[0].setRotationPoint(3F, -24F, -2F);

		ammoModel[1].addShapeBox(0F, 0F, 0F, 9, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Import ammoPart3
		ammoModel[1].setRotationPoint(3F, -22F, -2F);

		ammoModel[2].addBox(0F, 0F, 0F, 9, 1, 3, 0F); // Import ammoPart2
		ammoModel[2].setRotationPoint(3F, -23F, -2F);


		pumpModel = new ModelRendererTurbo[4];
		pumpModel[0] = new ModelRendererTurbo(this, 107, 46, textureX, textureY); // Import boltPart5
		pumpModel[1] = new ModelRendererTurbo(this, 39, 40, textureX, textureY); // Import pumpBolt1
		pumpModel[2] = new ModelRendererTurbo(this, 39, 40, textureX, textureY); // Import pumpBolt2
		pumpModel[3] = new ModelRendererTurbo(this, 49, 50, textureX, textureY); // Import pumpBolt3

		pumpModel[0].addShapeBox(0F, 0F, 0F, 6, 3, 9, 0F, 0F, 0.3F, -2.5F, 0F, 0.3F, -2.5F, 0F, 0.3F, -2.5F, 0F, 0.3F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import boltPart5
		pumpModel[0].setRotationPoint(14F, -25F, -4.5F);

		pumpModel[1].addShapeBox(0F, 0F, 0F, 1, 3, 4, 0F, 0F, 0F, 0F, 0.2F, -1F, 0F, 0.2F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.2F, -1F, 0F, 0.2F, -1F, 0F, 0F, 0F, 0F); // Import pumpBolt1
		pumpModel[1].setRotationPoint(16F, -29F, -7F);
		pumpModel[1].rotateAngleX = -0.6981317F;

		pumpModel[2].addShapeBox(0F, 0F, 0F, 1, 3, 4, 0F, 0.2F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.2F, -1F, 0F, 0.2F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.2F, -1F, 0F); // Import pumpBolt2
		pumpModel[2].setRotationPoint(15F, -29F, -7F);
		pumpModel[2].rotateAngleX = -0.6981317F;

		pumpModel[3].addBox(0F, 0F, 0F, 1, 1, 5, 0F); // Import pumpBolt3
		pumpModel[3].setRotationPoint(15.5F, -27F, -6F);
		pumpModel[3].rotateAngleX = -0.6981317F;

		barrelAttachPoint = new Vector3f(98F /16F, 21F /16F, 0F /16F);
		stockAttachPoint = new Vector3f(-16F /16F, 15F /16F, 0F /16F);
		gripAttachPoint = new Vector3f(25 /16F, 16F /16F, 0F /16F);

		gunSlideDistance = 0.5F;
		animationType = EnumAnimationType.RIFLE;

		//Pump parameters
		pumpDelayAfterReload = 115;
		pumpDelay = 10;
		pumpTime = 8;
		gripIsOnPump = false;
		pumpHandleDistance = 0.5F;
		//boltDistance = 0.75F;

		//Reload parameters
		numBulletsInReloadAnimation = 4;
		tiltGunTime = 0.250F;
		unloadClipTime = 0.0F;
		loadClipTime = 0.500F;
		untiltGunTime = 0.250F;


		translateAll(0F, 0F, 0F);


		flipAll();
	}
}
