package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelGS5 extends ModelGun
{
	int textureX = 512;
	int textureY = 256;

	public ModelGS5()	
	{
		gunModel = new ModelRendererTurbo[68];
		gunModel[0] = new ModelRendererTurbo(this, 140, 1, textureX, textureY); // barrelPart1
		gunModel[1] = new ModelRendererTurbo(this, 140, 10, textureX, textureY); // barrelPart2
		gunModel[2] = new ModelRendererTurbo(this, 140, 1, textureX, textureY); // barrelPart3
		gunModel[3] = new ModelRendererTurbo(this, 158, 127, textureX, textureY); // body1
		gunModel[4] = new ModelRendererTurbo(this, 1, 61, textureX, textureY); // body1
		gunModel[5] = new ModelRendererTurbo(this, 22, 62, textureX, textureY); // body1
		gunModel[6] = new ModelRendererTurbo(this, 81, 79, textureX, textureY); // Box 0
		gunModel[7] = new ModelRendererTurbo(this, 1, 10, textureX, textureY); // Box 1
		gunModel[8] = new ModelRendererTurbo(this, 1, 20, textureX, textureY); // Box 2
		gunModel[9] = new ModelRendererTurbo(this, 1, 33, textureX, textureY); // Box 3
		gunModel[10] = new ModelRendererTurbo(this, 1, 50, textureX, textureY); // Box 4
		gunModel[11] = new ModelRendererTurbo(this, 36, 33, textureX, textureY); // Box 5
		gunModel[12] = new ModelRendererTurbo(this, 36, 20, textureX, textureY); // Box 6
		gunModel[13] = new ModelRendererTurbo(this, 36, 10, textureX, textureY); // Box 7
		gunModel[14] = new ModelRendererTurbo(this, 55, 10, textureX, textureY); // Box 8
		gunModel[15] = new ModelRendererTurbo(this, 55, 20, textureX, textureY); // Box 9
		gunModel[16] = new ModelRendererTurbo(this, 55, 33, textureX, textureY); // Box 10
		gunModel[17] = new ModelRendererTurbo(this, 36, 50, textureX, textureY); // Box 11
		gunModel[18] = new ModelRendererTurbo(this, 141, 127, textureX, textureY); // Box 12
		gunModel[19] = new ModelRendererTurbo(this, 81, 68, textureX, textureY); // Box 14
		gunModel[20] = new ModelRendererTurbo(this, 81, 91, textureX, textureY); // Box 15
		gunModel[21] = new ModelRendererTurbo(this, 1, 101, textureX, textureY); // Box 16
		gunModel[22] = new ModelRendererTurbo(this, 1, 72, textureX, textureY); // Box 17
		gunModel[23] = new ModelRendererTurbo(this, 1, 86, textureX, textureY); // Box 18
		gunModel[24] = new ModelRendererTurbo(this, 1, 72, textureX, textureY); // Box 19
		gunModel[25] = new ModelRendererTurbo(this, 54, 88, textureX, textureY); // Box 20
		gunModel[26] = new ModelRendererTurbo(this, 54, 77, textureX, textureY); // Box 21
		gunModel[27] = new ModelRendererTurbo(this, 54, 77, textureX, textureY); // Box 22
		gunModel[28] = new ModelRendererTurbo(this, 43, 142, textureX, textureY); // Box 23
		gunModel[29] = new ModelRendererTurbo(this, 58, 134, textureX, textureY); // Box 24
		gunModel[30] = new ModelRendererTurbo(this, 1, 141, textureX, textureY); // Box 25
		gunModel[31] = new ModelRendererTurbo(this, 43, 134, textureX, textureY); // Box 26
		gunModel[32] = new ModelRendererTurbo(this, 58, 142, textureX, textureY); // Box 27
		gunModel[33] = new ModelRendererTurbo(this, 22, 141, textureX, textureY); // Box 28
		gunModel[34] = new ModelRendererTurbo(this, 1, 117, textureX, textureY); // Box 29
		gunModel[35] = new ModelRendererTurbo(this, 1, 133, textureX, textureY); // Box 30
		gunModel[36] = new ModelRendererTurbo(this, 1, 137, textureX, textureY); // Box 31
		gunModel[37] = new ModelRendererTurbo(this, 1, 137, textureX, textureY); // Box 32
		gunModel[38] = new ModelRendererTurbo(this, 1, 133, textureX, textureY); // Box 33
		gunModel[39] = new ModelRendererTurbo(this, 50, 120, textureX, textureY); // Box 34
		gunModel[40] = new ModelRendererTurbo(this, 50, 109, textureX, textureY); // Box 35
		gunModel[41] = new ModelRendererTurbo(this, 50, 109, textureX, textureY); // Box 36
		gunModel[42] = new ModelRendererTurbo(this, 58, 134, textureX, textureY); // Box 40
		gunModel[43] = new ModelRendererTurbo(this, 43, 134, textureX, textureY); // Box 41
		gunModel[44] = new ModelRendererTurbo(this, 22, 141, textureX, textureY); // Box 42
		gunModel[45] = new ModelRendererTurbo(this, 58, 142, textureX, textureY); // Box 43
		gunModel[46] = new ModelRendererTurbo(this, 43, 142, textureX, textureY); // Box 44
		gunModel[47] = new ModelRendererTurbo(this, 1, 141, textureX, textureY); // Box 45
		gunModel[48] = new ModelRendererTurbo(this, 38, 101, textureX, textureY); // Box 49
		gunModel[49] = new ModelRendererTurbo(this, 96, 100, textureX, textureY); // Box 50
		gunModel[50] = new ModelRendererTurbo(this, 113, 58, textureX, textureY); // Box 51
		gunModel[51] = new ModelRendererTurbo(this, 80, 58, textureX, textureY); // Box 52
		gunModel[52] = new ModelRendererTurbo(this, 141, 117, textureX, textureY); // Box 53
		gunModel[53] = new ModelRendererTurbo(this, 81, 99, textureX, textureY); // Box 54
		gunModel[54] = new ModelRendererTurbo(this, 38, 101, textureX, textureY); // Box 55
		gunModel[55] = new ModelRendererTurbo(this, 212, 63, textureX, textureY); // Box 63
		gunModel[56] = new ModelRendererTurbo(this, 212, 63, textureX, textureY); // Box 64
		gunModel[57] = new ModelRendererTurbo(this, 38, 107, textureX, textureY); // Box 66
		gunModel[58] = new ModelRendererTurbo(this, 38, 107, textureX, textureY); // Box 69
		gunModel[59] = new ModelRendererTurbo(this, 39, 121, textureX, textureY); // Box 70
		gunModel[60] = new ModelRendererTurbo(this, 39, 121, textureX, textureY); // Box 71
		gunModel[61] = new ModelRendererTurbo(this, 179, 129, textureX, textureY); // Box 77
		gunModel[62] = new ModelRendererTurbo(this, 185, 6, textureX, textureY); // Box 86
		gunModel[63] = new ModelRendererTurbo(this, 185, 12, textureX, textureY); // Box 87
		gunModel[64] = new ModelRendererTurbo(this, 185, 6, textureX, textureY); // Box 88
		gunModel[65] = new ModelRendererTurbo(this, 80, 5, textureX, textureY); // Box 94
		gunModel[66] = new ModelRendererTurbo(this, 89, 1, textureX, textureY); // Box 95
		gunModel[67] = new ModelRendererTurbo(this, 80, 1, textureX, textureY); // Box 96

		gunModel[0].addShapeBox(0F, 0F, 0F, 20, 6, 2, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // barrelPart1
		gunModel[0].setRotationPoint(26F, -26F, -3F);

		gunModel[1].addBox(0F, 0F, 0F, 20, 6, 2, 0F); // barrelPart2
		gunModel[1].setRotationPoint(26F, -26F, -1F);

		gunModel[2].addShapeBox(0F, 0F, 0F, 20, 6, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F); // barrelPart3
		gunModel[2].setRotationPoint(26F, -26F, 1F);

		gunModel[3].addShapeBox(0F, 0F, 0F, 3, 4, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F); // body1
		gunModel[3].setRotationPoint(-10F, -15F, -3.5F);

		gunModel[4].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // body1
		gunModel[4].setRotationPoint(-3F, 6F, -3.5F);

		gunModel[5].addShapeBox(0F, 0F, 0F, 3, 1, 7, 0F, 0F, 0F, 0F, -1F, 0F, -1F, -1F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // body1
		gunModel[5].setRotationPoint(-3F, 5F, -3.5F);

		gunModel[6].addBox(0F, 0F, 0F, 25, 4, 7, 0F); // Box 0
		gunModel[6].setRotationPoint(-7F, -15F, -3.5F);

		gunModel[7].addBox(0F, 0F, 0F, 10, 2, 7, 0F); // Box 1
		gunModel[7].setRotationPoint(-8F, -11F, -3.5F);

		gunModel[8].addShapeBox(0F, 0F, 0F, 10, 5, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 3F, 0F, 0F); // Box 2
		gunModel[8].setRotationPoint(-8F, -9F, -3.5F);

		gunModel[9].addShapeBox(0F, 0F, 0F, 10, 9, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F); // Box 3
		gunModel[9].setRotationPoint(-11F, -4F, -3.5F);

		gunModel[10].addBox(0F, 0F, 0F, 10, 3, 7, 0F); // Box 4
		gunModel[10].setRotationPoint(-13F, 5F, -3.5F);

		gunModel[11].addShapeBox(0F, 0F, 0F, 2, 9, 7, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, -1F, -2F, 0F, -1F, 2F, 0F, 0F); // Box 5
		gunModel[11].setRotationPoint(-1F, -4F, -3.5F);

		gunModel[12].addShapeBox(0F, 0F, 0F, 2, 5, 7, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, -1F, -3F, 0F, -1F, 3F, 0F, 0F); // Box 6
		gunModel[12].setRotationPoint(2F, -9F, -3.5F);

		gunModel[13].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 7
		gunModel[13].setRotationPoint(2F, -11F, -3.5F);

		gunModel[14].addShapeBox(0F, 0F, 0F, 1, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 8
		gunModel[14].setRotationPoint(-9F, -11F, -3.5F);

		gunModel[15].addShapeBox(0F, 0F, 0F, 1, 5, 7, 0F, -3F, 0F, -1F, 3F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 9
		gunModel[15].setRotationPoint(-12F, -9F, -3.5F);

		gunModel[16].addShapeBox(0F, 0F, 0F, 1, 9, 7, 0F, -2F, 0F, -1F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 10
		gunModel[16].setRotationPoint(-14F, -4F, -3.5F);

		gunModel[17].addShapeBox(0F, 0F, 0F, 1, 3, 7, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 11
		gunModel[17].setRotationPoint(-14F, 5F, -3.5F);

		gunModel[18].addShapeBox(0F, 0F, 0F, 1, 4, 7, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, -2F, 0F, -1F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, -1F); // Box 12
		gunModel[18].setRotationPoint(-11F, -15F, -3.5F);

		gunModel[19].addBox(0F, 0F, 0F, 22, 3, 7, 0F); // Box 14
		gunModel[19].setRotationPoint(19F, -14F, -3.5F);

		gunModel[20].addBox(0F, 0F, 0F, 23, 1, 6, 0F); // Box 15
		gunModel[20].setRotationPoint(18F, -15F, -3F);

		gunModel[21].addShapeBox(0F, 0F, 0F, 12, 3, 12, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 16
		gunModel[21].setRotationPoint(26F, -11F, -6F);

		gunModel[22].addShapeBox(0F, 0F, 0F, 16, 3, 10, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 17
		gunModel[22].setRotationPoint(24F, -9.5F, -5F);

		gunModel[23].addBox(0F, 0F, 0F, 16, 4, 10, 0F); // Box 18
		gunModel[23].setRotationPoint(24F, -6.5F, -5F);

		gunModel[24].addShapeBox(0F, 0F, 0F, 16, 3, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F); // Box 19
		gunModel[24].setRotationPoint(24F, -2.5F, -5F);

		gunModel[25].addBox(0F, 0F, 0F, 5, 4, 8, 0F); // Box 20
		gunModel[25].setRotationPoint(19F, -6.5F, -4F);

		gunModel[26].addShapeBox(0F, 0F, 0F, 5, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 21
		gunModel[26].setRotationPoint(19F, -2.5F, -4F);

		gunModel[27].addShapeBox(0F, 0F, 0F, 5, 2, 8, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 22
		gunModel[27].setRotationPoint(19F, -8.5F, -4F);

		gunModel[28].addShapeBox(0F, 0F, 0F, 3, 3, 4, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F); // Box 23
		gunModel[28].setRotationPoint(23F, -11F, -6F);

		gunModel[29].addShapeBox(0F, 0F, 0F, 3, 3, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 24
		gunModel[29].setRotationPoint(23F, -11F, 2F);

		gunModel[30].addBox(0F, 0F, 0F, 3, 1, 7, 0F); // Box 25
		gunModel[30].setRotationPoint(23F, -11F, -3.5F);

		gunModel[31].addShapeBox(0F, 0F, 0F, 3, 3, 4, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F); // Box 26
		gunModel[31].setRotationPoint(23F, -1F, 2F);

		gunModel[32].addShapeBox(0F, 0F, 0F, 3, 3, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 27
		gunModel[32].setRotationPoint(23F, -1F, -6F);

		gunModel[33].addBox(0F, 0F, 0F, 3, 1, 7, 0F); // Box 28
		gunModel[33].setRotationPoint(23F, 1F, -3.5F);

		gunModel[34].addShapeBox(0F, 0F, 0F, 12, 3, 12, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F); // Box 29
		gunModel[34].setRotationPoint(26F, -1F, -6F);

		gunModel[35].addShapeBox(0F, 0F, 0F, 18, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 30
		gunModel[35].setRotationPoint(23F, -8F, -6F);

		gunModel[36].addShapeBox(0F, 0F, 0F, 18, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 31
		gunModel[36].setRotationPoint(23F, -2F, -6F);

		gunModel[37].addShapeBox(0F, 0F, 0F, 18, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 32
		gunModel[37].setRotationPoint(23F, -2F, 4.5F);

		gunModel[38].addShapeBox(0F, 0F, 0F, 18, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 33
		gunModel[38].setRotationPoint(23F, -8F, 4.5F);

		gunModel[39].addBox(0F, 0F, 0F, 3, 4, 8, 0F); // Box 34
		gunModel[39].setRotationPoint(41F, -6.5F, -4F);

		gunModel[40].addShapeBox(0F, 0F, 0F, 3, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 35
		gunModel[40].setRotationPoint(41F, -2.5F, -4F);

		gunModel[41].addShapeBox(0F, 0F, 0F, 3, 2, 8, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 36
		gunModel[41].setRotationPoint(41F, -8.5F, -4F);

		gunModel[42].addShapeBox(0F, 0F, 0F, 3, 3, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 40
		gunModel[42].setRotationPoint(38F, -11F, 2F);

		gunModel[43].addShapeBox(0F, 0F, 0F, 3, 3, 4, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F); // Box 41
		gunModel[43].setRotationPoint(38F, -1F, 2F);

		gunModel[44].addBox(0F, 0F, 0F, 3, 1, 7, 0F); // Box 42
		gunModel[44].setRotationPoint(38F, 1F, -3.5F);

		gunModel[45].addShapeBox(0F, 0F, 0F, 3, 3, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 43
		gunModel[45].setRotationPoint(38F, -1F, -6F);

		gunModel[46].addShapeBox(0F, 0F, 0F, 3, 3, 4, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F); // Box 44
		gunModel[46].setRotationPoint(38F, -11F, -6F);

		gunModel[47].addBox(0F, 0F, 0F, 3, 1, 7, 0F); // Box 45
		gunModel[47].setRotationPoint(38F, -11F, -3.5F);

		gunModel[48].addBox(0F, 0F, 0F, 3, 4, 1, 0F); // Box 49
		gunModel[48].setRotationPoint(41F, -6.5F, -5F);

		gunModel[49].addShapeBox(0F, 0F, 0F, 1, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F); // Box 50
		gunModel[49].setRotationPoint(18F, -11F, -3F);

		gunModel[50].addShapeBox(0F, 0F, 0F, 7, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F); // Box 51
		gunModel[50].setRotationPoint(19F, -11F, -3.5F);

		gunModel[51].addShapeBox(0F, 0F, 0F, 9, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F); // Box 52
		gunModel[51].setRotationPoint(9F, -11F, -3.5F);

		gunModel[52].addBox(0F, 0F, 0F, 52, 2, 7, 0F); // Box 53
		gunModel[52].setRotationPoint(-11F, -17F, -3.5F);

		gunModel[53].addBox(0F, 0F, 0F, 1, 3, 6, 0F); // Box 54
		gunModel[53].setRotationPoint(18F, -14F, -3F);

		gunModel[54].addBox(0F, 0F, 0F, 3, 4, 1, 0F); // Box 55
		gunModel[54].setRotationPoint(41F, -6.5F, 4F);

		gunModel[55].addBox(0F, 0F, 0F, 10, 2, 5, 0F); // Box 63
		gunModel[55].setRotationPoint(12F, -24.5F, -3.5F);

		gunModel[56].addShapeBox(0F, 0F, 0F, 10, 2, 5, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 64
		gunModel[56].setRotationPoint(12F, -26.5F, -3.5F);

		gunModel[57].addBox(0F, 0F, 0F, 3, 1, 4, 0F); // Box 66
		gunModel[57].setRotationPoint(41F, -9.5F, -2F);

		gunModel[58].addBox(0F, 0F, 0F, 3, 1, 4, 0F); // Box 69
		gunModel[58].setRotationPoint(41F, -0.5F, -2F);

		gunModel[59].addShapeBox(0F, 0F, 0F, 5, 2, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 70
		gunModel[59].setRotationPoint(40F, -6.5F, -2F);

		gunModel[60].addShapeBox(0F, 0F, 0F, 5, 2, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 71
		gunModel[60].setRotationPoint(40F, -4.5F, -2F);

		gunModel[61].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F); // Box 77
		gunModel[61].setRotationPoint(-14F, -17F, -3.5F);

		gunModel[62].addShapeBox(0F, 0F, 0F, 10, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F); // Box 86
		gunModel[62].setRotationPoint(32F, -19F, 1F);

		gunModel[63].addBox(0F, 0F, 0F, 10, 4, 2, 0F); // Box 87
		gunModel[63].setRotationPoint(32F, -19F, -1F);

		gunModel[64].addShapeBox(0F, 0F, 0F, 10, 4, 1, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 88
		gunModel[64].setRotationPoint(32F, -19F, -2F);

		gunModel[65].addBox(0F, 0F, 0F, 12, 3, 1, 0F); // Box 94
		gunModel[65].setRotationPoint(4F, -15F, 3F);

		gunModel[66].addBox(0F, 0F, 0F, 2, 2, 1, 0F); // Box 95
		gunModel[66].setRotationPoint(14F, -14.5F, 3.5F);

		gunModel[67].addShapeBox(0F, 0F, 0F, 3, 2, 1, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F); // Box 96
		gunModel[67].setRotationPoint(11F, -14.5F, 3.5F);


		ammoModel = new ModelRendererTurbo[10];
		ammoModel[0] = new ModelRendererTurbo(this, 107, 34, textureX, textureY); // ammoPart1
		ammoModel[1] = new ModelRendererTurbo(this, 80, 33, textureX, textureY); // ammoPart2
		ammoModel[2] = new ModelRendererTurbo(this, 80, 43, textureX, textureY); // ammoPart4
		ammoModel[3] = new ModelRendererTurbo(this, 99, 43, textureX, textureY); // ammoPart5
		ammoModel[4] = new ModelRendererTurbo(this, 80, 10, textureX, textureY); // Box 59
		ammoModel[5] = new ModelRendererTurbo(this, 107, 11, textureX, textureY); // Box 60
		ammoModel[6] = new ModelRendererTurbo(this, 99, 43, textureX, textureY); // Box 13
		ammoModel[7] = new ModelRendererTurbo(this, 115, 48, textureX, textureY); // Box 99
		ammoModel[8] = new ModelRendererTurbo(this, 80, 48, textureX, textureY); // Box 100
		ammoModel[9] = new ModelRendererTurbo(this, 117, 38, textureX, textureY); // Box 101

		ammoModel[0].addShapeBox(0F, 0F, 0F, 3, 3, 5, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F); // ammoPart1
		ammoModel[0].setRotationPoint(-1F, -9.5F, -2.5F);

		ammoModel[1].addShapeBox(0F, 0F, 0F, 7, 3, 6, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F); // ammoPart2
		ammoModel[1].setRotationPoint(-8F, -10F, -3F);

		ammoModel[2].addShapeBox(0F, 0F, 0F, 6, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // ammoPart4
		ammoModel[2].setRotationPoint(-7F, -11F, -1.5F);

		ammoModel[3].addShapeBox(0F, 0F, 0F, 2, 1, 3, 0F, 0F, 0F, -1F, 0F, -0.5F, -1F, 0F, -0.5F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F); // ammoPart5
		ammoModel[3].setRotationPoint(-1F, -11F, -1.5F);

		ammoModel[4].addShapeBox(0F, 3F, 0F, 7, 16, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 4F, 0F, 0F); // Box 59
		ammoModel[4].setRotationPoint(-9F, -10F, -3F);

		ammoModel[5].addShapeBox(0F, 2F, 0F, 3, 16, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 4F, 0F, 0F); // Box 60
		ammoModel[5].setRotationPoint(-2F, -8.5F, -2.5F);

		ammoModel[6].addShapeBox(0F, 0F, 0F, 2, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F); // Box 13
		ammoModel[6].setRotationPoint(-1F, -10F, -1.5F);

		ammoModel[7].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 99
		ammoModel[7].setRotationPoint(-3F, 8.5F, -3.5F);

		ammoModel[8].addBox(0F, 0F, 0F, 10, 2, 7, 0F); // Box 100
		ammoModel[8].setRotationPoint(-13F, 8.5F, -3.5F);

		ammoModel[9].addShapeBox(0F, 0F, 0F, 1, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 101
		ammoModel[9].setRotationPoint(-14F, 8.5F, -3.5F);


		slideModel = new ModelRendererTurbo[25];
		slideModel[0] = new ModelRendererTurbo(this, 141, 101, textureX, textureY); // bodyRail1
		slideModel[1] = new ModelRendererTurbo(this, 60, 3, textureX, textureY); // frontSight
		slideModel[2] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // rearSight1
		slideModel[3] = new ModelRendererTurbo(this, 45, 4, textureX, textureY); // rearSight2
		slideModel[4] = new ModelRendererTurbo(this, 30, 4, textureX, textureY); // rearSight3
		slideModel[5] = new ModelRendererTurbo(this, 208, 100, textureX, textureY); // slide1
		slideModel[6] = new ModelRendererTurbo(this, 141, 71, textureX, textureY); // slide2
		slideModel[7] = new ModelRendererTurbo(this, 204, 33, textureX, textureY); // slide3
		slideModel[8] = new ModelRendererTurbo(this, 141, 61, textureX, textureY); // Box 36
		slideModel[9] = new ModelRendererTurbo(this, 140, 20, textureX, textureY); // Box 37
		slideModel[10] = new ModelRendererTurbo(this, 141, 83, textureX, textureY); // Box 42
		slideModel[11] = new ModelRendererTurbo(this, 171, 33, textureX, textureY); // Box 56
		slideModel[12] = new ModelRendererTurbo(this, 180, 83, textureX, textureY); // Box 57
		slideModel[13] = new ModelRendererTurbo(this, 140, 51, textureX, textureY); // Box 58
		slideModel[14] = new ModelRendererTurbo(this, 212, 57, textureX, textureY); // Box 62
		slideModel[15] = new ModelRendererTurbo(this, 189, 100, textureX, textureY); // Box 72
		slideModel[16] = new ModelRendererTurbo(this, 170, 100, textureX, textureY); // Box 73
		slideModel[17] = new ModelRendererTurbo(this, 170, 100, textureX, textureY); // Box 74
		slideModel[18] = new ModelRendererTurbo(this, 170, 100, textureX, textureY); // Box 75
		slideModel[19] = new ModelRendererTurbo(this, 227, 100, textureX, textureY); // Box 76
		slideModel[20] = new ModelRendererTurbo(this, 193, 51, textureX, textureY); // Box 79
		slideModel[21] = new ModelRendererTurbo(this, 168, 71, textureX, textureY); // Box 83
		slideModel[22] = new ModelRendererTurbo(this, 195, 19, textureX, textureY); // Box 84
		slideModel[23] = new ModelRendererTurbo(this, 140, 33, textureX, textureY); // Box 89
		slideModel[24] = new ModelRendererTurbo(this, 237, 37, textureX, textureY); // Box 102

		slideModel[0].addShapeBox(0F, 0F, 0F, 7, 8, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F); // bodyRail1
		slideModel[0].setRotationPoint(-9F, -25F, -3.5F);

		slideModel[1].addShapeBox(0F, 0F, 0F, 5, 3, 3, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F); // frontSight
		slideModel[1].setRotationPoint(35F, -31F, -0.5F);

		slideModel[2].addBox(0F, 0F, 0F, 8, 2, 6, 0F); // rearSight1
		slideModel[2].setRotationPoint(-10F, -28F, -3F);

		slideModel[3].addShapeBox(0F, 0F, 0F, 5, 3, 2, 0F, 0F, -0.5F, 0F, -2F, -0.5F, 0F, -2F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // rearSight2
		slideModel[3].setRotationPoint(-10F, -31F, 1F);

		slideModel[4].addShapeBox(0F, 0F, 0F, 5, 3, 2, 0F, 0F, -0.5F, 0F, -2F, -0.5F, 0F, -2F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // rearSight3
		slideModel[4].setRotationPoint(-10F, -31F, -3F);

		slideModel[5].addShapeBox(0F, 0F, 0F, 1, 8, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F); // slide1
		slideModel[5].setRotationPoint(-10F, -25F, -4F);

		slideModel[6].addBox(0F, 0F, 0F, 10, 8, 3, 0F); // slide2
		slideModel[6].setRotationPoint(12F, -25F, 1F);

		slideModel[7].addShapeBox(0F, 0F, 0F, 6, 7, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -5F, 0F, 0F); // slide3
		slideModel[7].setRotationPoint(28F, -26F, -5F);

		slideModel[8].addShapeBox(0F, 0F, 0F, 22, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 36
		slideModel[8].setRotationPoint(-10F, -27F, -3.5F);

		slideModel[9].addShapeBox(0F, 0F, 0F, 17, 2, 10, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 37
		slideModel[9].setRotationPoint(23F, -28F, -5F);

		slideModel[10].addBox(0F, 0F, 0F, 11, 8, 8, 0F); // Box 42
		slideModel[10].setRotationPoint(1F, -25F, -4F);

		slideModel[11].addBox(0F, 0F, 0F, 6, 7, 10, 0F); // Box 56
		slideModel[11].setRotationPoint(34F, -26F, -5F);

		slideModel[12].addBox(0F, 0F, 0F, 19, 8, 8, 0F); // Box 57
		slideModel[12].setRotationPoint(22F, -25F, -4F);

		slideModel[13].addShapeBox(0F, 0F, 0F, 19, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 58
		slideModel[13].setRotationPoint(22F, -27F, -3.5F);

		slideModel[14].addShapeBox(0F, 0F, 0F, 10, 2, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 62
		slideModel[14].setRotationPoint(12F, -27F, 0.5F);

		slideModel[15].addShapeBox(0F, 0F, 0F, 1, 8, 8, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 72
		slideModel[15].setRotationPoint(0F, -25F, -4F);

		slideModel[16].addShapeBox(0F, 0F, 0F, 1, 8, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F); // Box 73
		slideModel[16].setRotationPoint(-8F, -25F, -4F);

		slideModel[17].addShapeBox(0F, 0F, 0F, 1, 8, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F); // Box 74
		slideModel[17].setRotationPoint(-6F, -25F, -4F);

		slideModel[18].addShapeBox(0F, 0F, 0F, 1, 8, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F); // Box 75
		slideModel[18].setRotationPoint(-4F, -25F, -4F);

		slideModel[19].addShapeBox(0F, 0F, 0F, 2, 8, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F); // Box 76
		slideModel[19].setRotationPoint(-12F, -25F, -4F);

		slideModel[20].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, -1F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, -1F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 79
		slideModel[20].setRotationPoint(-12F, -27F, -3.5F);

		slideModel[21].addBox(0F, 0F, 0F, 10, 6, 5, 0F); // Box 83
		slideModel[21].setRotationPoint(12F, -23F, -4F);

		slideModel[22].addShapeBox(0F, 0F, 0F, 5, 3, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, -1F, 2F, 0F, -1F, 2F, 0F, -1F, -2F, 0F, -1F); // Box 84
		slideModel[22].setRotationPoint(28F, -19F, -5F);

		slideModel[23].addShapeBox(0F, 0F, 0F, 5, 7, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -5F, 0F, 0F, 5F, 0F, 0F, 5F, 0F, 0F, -5F, 0F, 0F); // Box 89
		slideModel[23].setRotationPoint(23F, -26F, -5F);

		slideModel[24].addShapeBox(0F, 0F, 0F, 7, 3, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, -2F, 0F, -1F); // Box 102
		slideModel[24].setRotationPoint(33F, -19F, -5F);

		barrelAttachPoint = new Vector3f(46F /16F, 23F /16F, 0F /16F);

		gunSlideDistance = 0.8F;
		animationType = EnumAnimationType.CUSTOM;

		/* ----Start of Reload Block---- */
		/* Reload Name: Alternate Pistol Clip */
		rotateGunHorizontal = 60F;
		translateGun = new Vector3f(0.15F, 0.25F, 0F);

		rotateClipVertical = 5F;
		translateClip = new Vector3f(0F, -3F, 0F);
		/* ----End of Reload Block---- */

		flipAll();
		translateAll(0F, 0F, 0F);

		///<summary>
		///Owner = "DerpiWolf"
		///</summary>
	}
}