package com.flansmod.client.model.dwfirearms; //Path where the model is located

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelMFP5C extends ModelGun //Same as Filename
{
	int textureX = 512;
	int textureY = 256;

	public ModelMFP5C() //Same as Filename
	{
		gunModel = new ModelRendererTurbo[43];
		gunModel[0] = new ModelRendererTurbo(this, 137, 59, textureX, textureY); // Box 1
		gunModel[1] = new ModelRendererTurbo(this, 129, 73, textureX, textureY); // Box 2
		gunModel[2] = new ModelRendererTurbo(this, 158, 73, textureX, textureY); // Box 4
		gunModel[3] = new ModelRendererTurbo(this, 99, 71, textureX, textureY); // Box 5
		gunModel[4] = new ModelRendererTurbo(this, 100, 49, textureX, textureY); // Box 6
		gunModel[5] = new ModelRendererTurbo(this, 100, 31, textureX, textureY); // Box 9
		gunModel[6] = new ModelRendererTurbo(this, 98, 106, textureX, textureY); // Box 10
		gunModel[7] = new ModelRendererTurbo(this, 168, 88, textureX, textureY); // Box 11
		gunModel[8] = new ModelRendererTurbo(this, 153, 90, textureX, textureY); // Box 12
		gunModel[9] = new ModelRendererTurbo(this, 164, 61, textureX, textureY); // Box 13
		gunModel[10] = new ModelRendererTurbo(this, 98, 99, textureX, textureY); // Box 14
		gunModel[11] = new ModelRendererTurbo(this, 134, 87, textureX, textureY); // Box 15
		gunModel[12] = new ModelRendererTurbo(this, 183, 84, textureX, textureY); // Box 16
		gunModel[13] = new ModelRendererTurbo(this, 119, 89, textureX, textureY); // Box 17
		gunModel[14] = new ModelRendererTurbo(this, 199, 62, textureX, textureY); // Box 18
		gunModel[15] = new ModelRendererTurbo(this, 98, 86, textureX, textureY); // Box 19
		gunModel[16] = new ModelRendererTurbo(this, 1, 32, textureX, textureY); // Box 21
		gunModel[17] = new ModelRendererTurbo(this, 49, 34, textureX, textureY); // Box 22
		gunModel[18] = new ModelRendererTurbo(this, 30, 32, textureX, textureY); // Box 23
		gunModel[19] = new ModelRendererTurbo(this, 1, 53, textureX, textureY); // Box 24
		gunModel[20] = new ModelRendererTurbo(this, 47, 54, textureX, textureY); // Box 25
		gunModel[21] = new ModelRendererTurbo(this, 47, 54, textureX, textureY); // Box 26
		gunModel[22] = new ModelRendererTurbo(this, 30, 53, textureX, textureY); // Box 27
		gunModel[23] = new ModelRendererTurbo(this, 100, 59, textureX, textureY); // Box 28
		gunModel[24] = new ModelRendererTurbo(this, 100, 1, textureX, textureY); // Box 28
		gunModel[25] = new ModelRendererTurbo(this, 100, 8, textureX, textureY); // Box 29
		gunModel[26] = new ModelRendererTurbo(this, 100, 1, textureX, textureY); // Box 31
		gunModel[27] = new ModelRendererTurbo(this, 100, 16, textureX, textureY); // Box 38
		gunModel[28] = new ModelRendererTurbo(this, 100, 23, textureX, textureY); // Box 39
		gunModel[29] = new ModelRendererTurbo(this, 100, 16, textureX, textureY); // Box 40
		gunModel[30] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 72
		gunModel[31] = new ModelRendererTurbo(this, 58, 1, textureX, textureY); // Box 73
		gunModel[32] = new ModelRendererTurbo(this, 58, 1, textureX, textureY); // Box 74
		gunModel[33] = new ModelRendererTurbo(this, 58, 1, textureX, textureY); // Box 75
		gunModel[34] = new ModelRendererTurbo(this, 58, 1, textureX, textureY); // Box 76
		gunModel[35] = new ModelRendererTurbo(this, 105, 99, textureX, textureY); // Box 93
		gunModel[36] = new ModelRendererTurbo(this, 128, 98, textureX, textureY); // Box 94
		gunModel[37] = new ModelRendererTurbo(this, 128, 102, textureX, textureY); // Box 95
		gunModel[38] = new ModelRendererTurbo(this, 129, 106, textureX, textureY); // Box 96
		gunModel[39] = new ModelRendererTurbo(this, 156, 110, textureX, textureY); // Box 97
		gunModel[40] = new ModelRendererTurbo(this, 156, 106, textureX, textureY); // Box 98
		gunModel[41] = new ModelRendererTurbo(this, 33, 21, textureX, textureY); // Box 0
		gunModel[42] = new ModelRendererTurbo(this, 33, 17, textureX, textureY); // Box 1

		gunModel[0].addShapeBox(0F, 0F, 0F, 5, 3, 8, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 1
		gunModel[0].setRotationPoint(-13F, -22F, -4F);

		gunModel[1].addShapeBox(0F, 0F, 0F, 5, 2, 10, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 2
		gunModel[1].setRotationPoint(-13F, -19F, -5F);

		gunModel[2].addShapeBox(0F, 0F, 0F, 5, 2, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F); // Box 4
		gunModel[2].setRotationPoint(-13F, -13F, -5F);

		gunModel[3].addBox(0F, 0F, 0F, 5, 4, 10, 0F); // Box 5
		gunModel[3].setRotationPoint(-13F, -17F, -5F);

		gunModel[4].addShapeBox(0F, 0F, 0F, 35, 2, 7, 0F, 0F, 0.5F, -1F, 0F, 0.5F, -1F, 0F, 0.5F, -1F, 0F, 0.5F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 6
		gunModel[4].setRotationPoint(-8F, -21F, -3.5F);

		gunModel[5].addBox(0F, 0F, 0F, 35, 10, 7, 0F); // Box 9
		gunModel[5].setRotationPoint(-8F, -19F, -3.5F);

		gunModel[6].addShapeBox(0F, 0F, 0F, 14, 5, 1, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 10
		gunModel[6].setRotationPoint(-8F, -17.5F, -4.5F);

		gunModel[7].addBox(0F, 0F, 0F, 1, 3, 6, 0F); // Box 11
		gunModel[7].setRotationPoint(-14F, -12F, -3F);

		gunModel[8].addShapeBox(0F, 0F, 0F, 1, 1, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 12
		gunModel[8].setRotationPoint(-14F, -9F, -3F);

		gunModel[9].addBox(0F, 0F, 0F, 5, 2, 7, 0F); // Box 13
		gunModel[9].setRotationPoint(-13F, -11F, -3.5F);

		gunModel[10].addShapeBox(0F, 0F, 0F, 2, 5, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F); // Box 14
		gunModel[10].setRotationPoint(-8F, -17.5F, 3.5F);

		gunModel[11].addShapeBox(0F, 0F, 0F, 1, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 15
		gunModel[11].setRotationPoint(-14F, -14F, -4F);

		gunModel[12].addShapeBox(0F, 0F, 0F, 1, 5, 8, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 16
		gunModel[12].setRotationPoint(-14F, -19F, -4F);

		gunModel[13].addShapeBox(0F, 0F, 0F, 1, 2, 6, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 17
		gunModel[13].setRotationPoint(-14F, -21F, -3F);

		gunModel[14].addBox(0F, 0F, 0F, 21, 4, 7, 0F); // Box 18
		gunModel[14].setRotationPoint(-5F, -9F, -3.5F);

		gunModel[15].addShapeBox(0F, 0F, 0F, 3, 4, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2.5F, 0F, 0F); // Box 19
		gunModel[15].setRotationPoint(-8F, -9F, -3.5F);

		gunModel[16].addShapeBox(0F, 0F, 0F, 7, 13, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, -2F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 4F, -2F, 0F); // Box 21
		gunModel[16].setRotationPoint(-4.5F, -5F, -3.5F);

		gunModel[17].addShapeBox(0F, 0F, 0F, 1, 11, 7, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 4F, 0F, -1F, -4F, 0F, 0F, -4F, 0F, 0F, 4F, 0F, -1F); // Box 22
		gunModel[17].setRotationPoint(-5.5F, -5F, -3.5F);

		gunModel[18].addShapeBox(0F, 0F, 0F, 2, 13, 7, 0F, -4F, 0F, 0F, 4F, 0F, -1F, 4F, 0F, -1F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 23
		gunModel[18].setRotationPoint(-1.5F, -5F, -3.5F);

		gunModel[19].addShapeBox(0F, 0F, 0F, 7, 2, 7, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F); // Box 24
		gunModel[19].setRotationPoint(-8.5F, 6F, -3.5F);

		gunModel[20].addShapeBox(0F, 0F, 0F, 3, 1, 7, 0F, 0F, 0F, 0F, -1F, 0F, -1F, -1F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 25
		gunModel[20].setRotationPoint(-1.5F, 8F, -3.5F);

		gunModel[21].addShapeBox(0F, 0F, 0F, 3, 1, 7, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 26
		gunModel[21].setRotationPoint(-1.5F, 9F, -3.5F);

		gunModel[22].addShapeBox(0F, 0F, 0F, 1, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -1F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, -1F); // Box 27
		gunModel[22].setRotationPoint(-9.5F, 6F, -3.5F);

		gunModel[23].addShapeBox(0F, 0F, 0F, 11, 4, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F); // Box 28
		gunModel[23].setRotationPoint(16F, -9F, -3.5F);

		gunModel[24].addShapeBox(0F, 0F, 0F, 30, 1, 5, 0F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 28
		gunModel[24].setRotationPoint(27F, -17F, -2.5F);

		gunModel[25].addBox(0F, 0F, 0F, 30, 2, 5, 0F); // Box 29
		gunModel[25].setRotationPoint(27F, -16F, -2.5F);

		gunModel[26].addShapeBox(0F, 0F, 0F, 30, 1, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F); // Box 31
		gunModel[26].setRotationPoint(27F, -14F, -2.5F);

		gunModel[27].addShapeBox(0F, 0F, 0F, 26, 1, 5, 0F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 38
		gunModel[27].setRotationPoint(27F, -21F, -2.5F);

		gunModel[28].addBox(0F, 0F, 0F, 26, 2, 5, 0F); // Box 39
		gunModel[28].setRotationPoint(27F, -20F, -2.5F);

		gunModel[29].addShapeBox(0F, 0F, 0F, 26, 1, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F); // Box 40
		gunModel[29].setRotationPoint(27F, -18F, -2.5F);

		gunModel[30].addBox(0F, 0F, 0F, 21, 3, 7, 0F); // Box 72
		gunModel[30].setRotationPoint(1F, -22.5F, -3.5F);

		gunModel[31].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 73
		gunModel[31].setRotationPoint(1F, -24.5F, -3.5F);

		gunModel[32].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 74
		gunModel[32].setRotationPoint(7F, -24.5F, -3.5F);

		gunModel[33].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 75
		gunModel[33].setRotationPoint(13F, -24.5F, -3.5F);

		gunModel[34].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 76
		gunModel[34].setRotationPoint(19F, -24.5F, -3.5F);

		gunModel[35].addShapeBox(0F, 0F, 0F, 10, 5, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F); // Box 93
		gunModel[35].setRotationPoint(17F, -17.5F, 3.5F);

		gunModel[36].addShapeBox(0F, 0F, 0F, 23, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 94
		gunModel[36].setRotationPoint(-6F, -17.5F, 3.5F);

		gunModel[37].addShapeBox(0F, 0F, 0F, 23, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F); // Box 95
		gunModel[37].setRotationPoint(-6F, -14.5F, 3.5F);

		gunModel[38].addShapeBox(0F, 0F, 0F, 12, 5, 1, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 96
		gunModel[38].setRotationPoint(15F, -17.5F, -4.5F);

		gunModel[39].addShapeBox(0F, 0F, 0F, 9, 2, 1, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F); // Box 97
		gunModel[39].setRotationPoint(6F, -17.5F, -4.5F);

		gunModel[40].addShapeBox(0F, 0F, 0F, 9, 2, 1, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 98
		gunModel[40].setRotationPoint(6F, -14.5F, -4.5F);

		gunModel[41].addShapeBox(0F, 0F, 0F, 4, 2, 1, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 0
		gunModel[41].setRotationPoint(1F, -10F, 3F);

		gunModel[42].addShapeBox(0F, 0F, 0F, 4, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 1
		gunModel[42].setRotationPoint(1F, -8F, 3F);


		defaultScopeModel = new ModelRendererTurbo[13];
		defaultScopeModel[0] = new ModelRendererTurbo(this, 13, 23, textureX, textureY); // Box 42
		defaultScopeModel[1] = new ModelRendererTurbo(this, 13, 20, textureX, textureY); // Box 43
		defaultScopeModel[2] = new ModelRendererTurbo(this, 1, 12, textureX, textureY); // Box 44
		defaultScopeModel[3] = new ModelRendererTurbo(this, 13, 27, textureX, textureY); // Box 78
		defaultScopeModel[4] = new ModelRendererTurbo(this, 8, 29, textureX, textureY); // Box 79
		defaultScopeModel[5] = new ModelRendererTurbo(this, 8, 20, textureX, textureY); // Box 80
		defaultScopeModel[6] = new ModelRendererTurbo(this, 8, 23, textureX, textureY); // Box 81
		defaultScopeModel[7] = new ModelRendererTurbo(this, 1, 25, textureX, textureY); // Box 82
		defaultScopeModel[8] = new ModelRendererTurbo(this, 1, 20, textureX, textureY); // Box 83
		defaultScopeModel[9] = new ModelRendererTurbo(this, 1, 12, textureX, textureY); // Box 84
		defaultScopeModel[10] = new ModelRendererTurbo(this, 22, 12, textureX, textureY); // Box 85
		defaultScopeModel[11] = new ModelRendererTurbo(this, 22, 12, textureX, textureY); // Box 86
		defaultScopeModel[12] = new ModelRendererTurbo(this, 8, 26, textureX, textureY); // Box 87

		defaultScopeModel[0].addBox(0F, 0F, 0F, 1, 2, 1, 0F); // Box 42
		defaultScopeModel[0].setRotationPoint(52F, -26F, -2.5F);

		defaultScopeModel[1].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, 1.5F, 0F, 0.5F, 1.5F); // Box 43
		defaultScopeModel[1].setRotationPoint(52F, -24F, -2.5F);

		defaultScopeModel[2].addShapeBox(0F, 0F, 0F, 5, 2, 5, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 44
		defaultScopeModel[2].setRotationPoint(-8F, -25.5F, -2.5F);

		defaultScopeModel[3].addBox(0F, 0F, 0F, 1, 2, 1, 0F); // Box 78
		defaultScopeModel[3].setRotationPoint(52F, -26F, 1.5F);

		defaultScopeModel[4].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 1.5F, 0F, 0.5F, 1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F); // Box 79
		defaultScopeModel[4].setRotationPoint(52F, -24F, 1.5F);

		defaultScopeModel[5].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F, 0F, 0.5F, 1.5F, 0F, 0.5F, 1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 80
		defaultScopeModel[5].setRotationPoint(52F, -27F, 1.5F);

		defaultScopeModel[6].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, 1.5F, 0F, 0.5F, 1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 81
		defaultScopeModel[6].setRotationPoint(52F, -27F, -2.5F);

		defaultScopeModel[7].addBox(0F, 0F, 0F, 1, 1, 2, 0F); // Box 82
		defaultScopeModel[7].setRotationPoint(52F, -27.5F, -1F);

		defaultScopeModel[8].addBox(0F, 0F, 0F, 1, 2, 2, 0F); // Box 83
		defaultScopeModel[8].setRotationPoint(52F, -23.5F, -1F);

		defaultScopeModel[9].addBox(0F, 0F, 0F, 5, 2, 5, 0F); // Box 84
		defaultScopeModel[9].setRotationPoint(-8F, -23.5F, -2.5F);

		defaultScopeModel[10].addBox(0F, 0F, 0F, 3, 3, 2, 0F); // Box 85
		defaultScopeModel[10].setRotationPoint(-7F, -26.5F, -2.5F);

		defaultScopeModel[11].addBox(0F, 0F, 0F, 3, 3, 2, 0F); // Box 86
		defaultScopeModel[11].setRotationPoint(-7F, -26.5F, 0.5F);

		defaultScopeModel[12].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 87
		defaultScopeModel[12].setRotationPoint(52F, -24.5F, -0.5F);


		defaultStockModel = new ModelRendererTurbo[10];
		defaultStockModel[0] = new ModelRendererTurbo(this, 98, 128, textureX, textureY); // Box 88
		defaultStockModel[1] = new ModelRendererTurbo(this, 119, 127, textureX, textureY); // Box 89
		defaultStockModel[2] = new ModelRendererTurbo(this, 119, 113, textureX, textureY); // Box 90
		defaultStockModel[3] = new ModelRendererTurbo(this, 98, 113, textureX, textureY); // Box 91
		defaultStockModel[4] = new ModelRendererTurbo(this, 98, 137, textureX, textureY); // Box 92
		defaultStockModel[5] = new ModelRendererTurbo(this, 144, 114, textureX, textureY); // Box 108
		defaultStockModel[6] = new ModelRendererTurbo(this, 169, 122, textureX, textureY); // Box 109
		defaultStockModel[7] = new ModelRendererTurbo(this, 144, 126, textureX, textureY); // Box 110
		defaultStockModel[8] = new ModelRendererTurbo(this, 169, 114, textureX, textureY); // Box 111
		defaultStockModel[9] = new ModelRendererTurbo(this, 169, 128, textureX, textureY); // Box 112

		defaultStockModel[0].addShapeBox(0F, 0F, 0F, 4, 2, 6, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 88
		defaultStockModel[0].setRotationPoint(-24F, -21F, -3F);

		defaultStockModel[1].addShapeBox(0F, 0F, 0F, 4, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 89
		defaultStockModel[1].setRotationPoint(-24F, -14F, -4F);

		defaultStockModel[2].addShapeBox(0F, 0F, 0F, 4, 5, 8, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 90
		defaultStockModel[2].setRotationPoint(-24F, -19F, -4F);

		defaultStockModel[3].addBox(0F, 0F, 0F, 4, 8, 6, 0F); // Box 91
		defaultStockModel[3].setRotationPoint(-24F, -12F, -3F);

		defaultStockModel[4].addShapeBox(0F, 0F, 0F, 4, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 92
		defaultStockModel[4].setRotationPoint(-24F, -4F, -3F);

		defaultStockModel[5].addShapeBox(0F, 0F, 0F, 6, 5, 6, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 108
		defaultStockModel[5].setRotationPoint(-20F, -19F, -3F);

		defaultStockModel[6].addShapeBox(0F, 0F, 0F, 6, 1, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 109
		defaultStockModel[6].setRotationPoint(-20F, -20F, -2F);

		defaultStockModel[7].addShapeBox(0F, 0F, 0F, 6, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 110
		defaultStockModel[7].setRotationPoint(-20F, -14F, -3F);

		defaultStockModel[8].addShapeBox(0F, 0F, 0F, 6, 3, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F); // Box 111
		defaultStockModel[8].setRotationPoint(-20F, -12F, -2F);

		defaultStockModel[9].addShapeBox(0F, 0F, 0F, 3, 6, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F); // Box 112
		defaultStockModel[9].setRotationPoint(-21F, -9F, -2F);


		defaultGripModel = new ModelRendererTurbo[2];
		defaultGripModel[0] = new ModelRendererTurbo(this, 185, 49, textureX, textureY); // Box 36
		defaultGripModel[1] = new ModelRendererTurbo(this, 185, 31, textureX, textureY); // Box 36

		defaultGripModel[0].addShapeBox(0F, 0F, 0F, 25, 2, 10, 0F, 0F, 0F, 0F, 0F, 2F, -1F, 0F, 2F, -1F, 0F, 0F, 0F, 0F, 0.5F, -1F, 0F, -1.5F, -2F, 0F, -1.5F, -2F, 0F, 0.5F, -1F); // Box 36
		defaultGripModel[0].setRotationPoint(27F, -11F, -5F);

		defaultGripModel[1].addShapeBox(0F, 0F, 0F, 25, 7, 10, 0F, 0F, 0.5F, -1F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -1F, 0F, 0F, 0F, 0F, -2F, -1F, 0F, -2F, -1F, 0F, 0F, 0F); // Box 36
		defaultGripModel[1].setRotationPoint(27F, -18F, -5F);


		ammoModel = new ModelRendererTurbo[12];
		ammoModel[0] = new ModelRendererTurbo(this, 1, 63, textureX, textureY); // Box 27
		ammoModel[1] = new ModelRendererTurbo(this, 30, 82, textureX, textureY); // Box 45
		ammoModel[2] = new ModelRendererTurbo(this, 30, 77, textureX, textureY); // Box 46
		ammoModel[3] = new ModelRendererTurbo(this, 26, 63, textureX, textureY); // Box 49
		ammoModel[4] = new ModelRendererTurbo(this, 39, 64, textureX, textureY); // Box 50
		ammoModel[5] = new ModelRendererTurbo(this, 1, 63, textureX, textureY); // Box 51
		ammoModel[6] = new ModelRendererTurbo(this, 26, 63, textureX, textureY); // Box 52
		ammoModel[7] = new ModelRendererTurbo(this, 39, 64, textureX, textureY); // Box 53
		ammoModel[8] = new ModelRendererTurbo(this, 1, 63, textureX, textureY); // Box 66
		ammoModel[9] = new ModelRendererTurbo(this, 50, 65, textureX, textureY); // Box 69
		ammoModel[10] = new ModelRendererTurbo(this, 63, 66, textureX, textureY); // Box 70
		ammoModel[11] = new ModelRendererTurbo(this, 1, 77, textureX, textureY); // Box 71

		ammoModel[0].addBox(0F, 0F, 0F, 7, 8, 5, 0F); // Box 27
		ammoModel[0].setRotationPoint(17F, -11F, -2.5F);

		ammoModel[1].addShapeBox(0F, 0F, 0F, 5, 1, 3, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 45
		ammoModel[1].setRotationPoint(18F, -12F, -1.5F);

		ammoModel[2].addShapeBox(0F, 0F, 0F, 2, 1, 3, 0F, 0F, 0F, -0.5F, 0F, -0.5F, -1F, 0F, -0.5F, -1F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F); // Box 46
		ammoModel[2].setRotationPoint(23F, -12F, -1.5F);

		ammoModel[3].addBox(0F, 0F, 0F, 1, 8, 5, 0F); // Box 49
		ammoModel[3].setRotationPoint(25F, -11F, -2.5F);

		ammoModel[4].addBox(0F, 0F, 0F, 1, 8, 4, 0F); // Box 50
		ammoModel[4].setRotationPoint(24F, -11F, -2F);

		ammoModel[5].addShapeBox(0F, 0F, 0F, 7, 8, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F); // Box 51
		ammoModel[5].setRotationPoint(17F, -3F, -2.5F);

		ammoModel[6].addShapeBox(0F, 0F, 0F, 1, 8, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F); // Box 52
		ammoModel[6].setRotationPoint(25F, -3F, -2.5F);

		ammoModel[7].addShapeBox(0F, 0F, 0F, 1, 8, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F); // Box 53
		ammoModel[7].setRotationPoint(24F, -3F, -2F);

		ammoModel[8].addShapeBox(0F, 0F, 0F, 7, 8, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, -3F, 0F, 0F); // Box 66
		ammoModel[8].setRotationPoint(19F, 5F, -2.5F);

		ammoModel[9].addShapeBox(0F, 0F, 0F, 1, 6, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, 0F); // Box 69
		ammoModel[9].setRotationPoint(27F, 5F, -2.5F);

		ammoModel[10].addShapeBox(0F, 0F, 0F, 1, 6, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, 0F); // Box 70
		ammoModel[10].setRotationPoint(26F, 5F, -2F);

		ammoModel[11].addBox(0F, 0F, 0F, 9, 4, 5, 0F); // Box 71
		ammoModel[11].setRotationPoint(22F, 13F, -2.5F);
		ammoModel[11].rotateAngleZ = 0.45378561F;


		slideModel = new ModelRendererTurbo[3];
		slideModel[0] = new ModelRendererTurbo(this, 33, 12, textureX, textureY); // Box 99
		slideModel[1] = new ModelRendererTurbo(this, 67, 11, textureX, textureY); // Box 100
		slideModel[2] = new ModelRendererTurbo(this, 67, 11, textureX, textureY); // Box 101

		slideModel[0].addShapeBox(0F, 0F, 0F, 9, 3, 1, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 99
		slideModel[0].setRotationPoint(6F, -16.5F, -4F);

		slideModel[1].addShapeBox(0F, 0F, 0F, 2, 1, 4, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 100
		slideModel[1].setRotationPoint(13F, -16F, -8F);

		slideModel[2].addShapeBox(0F, 0F, 0F, 2, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 101
		slideModel[2].setRotationPoint(13F, -15F, -8F);


		pumpModel = new ModelRendererTurbo[4];
		pumpModel[0] = new ModelRendererTurbo(this, 54, 17, textureX, textureY); // Box 102
		pumpModel[1] = new ModelRendererTurbo(this, 54, 17, textureX, textureY); // Box 103
		pumpModel[2] = new ModelRendererTurbo(this, 54, 12, textureX, textureY); // Box 106
		pumpModel[3] = new ModelRendererTurbo(this, 54, 12, textureX, textureY); // Box 107

		pumpModel[0].addShapeBox(0F, 0F, 0F, 2, 1, 4, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 102
		pumpModel[0].setRotationPoint(48F, -19F, 1F);
		pumpModel[0].rotateAngleX = 0.61086524F;

		pumpModel[1].addShapeBox(0F, 1F, 0F, 2, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 103
		pumpModel[1].setRotationPoint(48F, -19F, 1F);
		pumpModel[1].rotateAngleX = 0.61086524F;

		pumpModel[2].addShapeBox(-0.5F, 0F, 4F, 3, 1, 3, 0F, -1F, 0.5F, 0F, -1F, 0.5F, 0F, -1F, 0.5F, 0F, -1F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 106
		pumpModel[2].setRotationPoint(48F, -19F, 1F);
		pumpModel[2].rotateAngleX = 0.61086524F;

		pumpModel[3].addShapeBox(-0.5F, 1F, 4F, 3, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0.5F, 0F, -1F, 0.5F, 0F, -1F, 0.5F, 0F, -1F, 0.5F, 0F); // Box 107
		pumpModel[3].setRotationPoint(48F, -19F, 1F);
		pumpModel[3].rotateAngleX = 0.61086524F;

		barrelAttachPoint = new Vector3f(57F /16F, 15F /16F, 0F /16F);
		stockAttachPoint = new Vector3f(-14F /16F, 15F /16F, 0F /16F);
		scopeAttachPoint = new Vector3f(13F /16F, 22F /16F, 0F /16F);

		gunSlideDistance = 0.5F;
		animationType = EnumAnimationType.BOTTOM_CLIP;

		pumpTime = 10;
		pumpDelayAfterReload = 60;
		pumpHandleDistance = 1F;

		translateAll(0F, 0F, 0F);


		flipAll();
	}
}