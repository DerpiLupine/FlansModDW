package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.ModelAttachment;
import com.flansmod.client.tmt.ModelRendererTurbo;

import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelABTHoloSight extends ModelAttachment
{
	int textureX = 512;
	int textureY = 256;

	public ModelABTHoloSight()
	{
		attachmentModel = new ModelRendererTurbo[6];
		attachmentModel[0] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 0
		attachmentModel[1] = new ModelRendererTurbo(this, 40, 4, textureX, textureY); // Box 1
		attachmentModel[2] = new ModelRendererTurbo(this, 40, 4, textureX, textureY); // Box 2
		attachmentModel[3] = new ModelRendererTurbo(this, 30, 14, textureX, textureY); // Box 3
		attachmentModel[4] = new ModelRendererTurbo(this, 1, 12, textureX, textureY); // Box 4
		attachmentModel[5] = new ModelRendererTurbo(this, 33, 4, textureX, textureY); // Box 5

		attachmentModel[0].addBox(-6F, -3F, -3.5F, 12, 3, 7, 0F); // Box 0
		attachmentModel[0].setRotationPoint(0F, 0F, 0F);

		attachmentModel[1].addShapeBox(-6F, -11F, -3.5F, 7, 6, 1, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 1
		attachmentModel[1].setRotationPoint(0F, 0F, 0F);

		attachmentModel[2].addShapeBox(-6F, -11F, 2.5F, 7, 6, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 2
		attachmentModel[2].setRotationPoint(0F, 0F, 0F);

		attachmentModel[3].addBox(-6F, -11F, -2.5F, 7, 1, 5, 0F); // Box 3
		attachmentModel[3].setRotationPoint(0F, 0F, 0F);

		attachmentModel[4].addBox(-6F, -5F, -3.5F, 7, 2, 7, 0F); // Box 4
		attachmentModel[4].setRotationPoint(0F, 0F, 0F);

		attachmentModel[5].addBox(-4F, -7F, -0.5F, 1, 2, 1, 0F); // Box 5
		attachmentModel[5].setRotationPoint(0F, 0F, 0F);

		renderOffset = 0F;

		//Based off the Ambircon SteamRifle positioning.

		flipAll();
	}
}