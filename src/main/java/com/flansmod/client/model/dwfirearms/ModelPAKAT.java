package com.flansmod.client.model.dwfirearms; //Path where the model is located

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelPAKAT extends ModelGun //Same as Filename
{
	int textureX = 512;
	int textureY = 256;

	public ModelPAKAT() //Same as Filename
	{
		gunModel = new ModelRendererTurbo[62];
		gunModel[0] = new ModelRendererTurbo(this, 80, 22, textureX, textureY); // Box 1
		gunModel[1] = new ModelRendererTurbo(this, 80, 92, textureX, textureY); // Box 2
		gunModel[2] = new ModelRendererTurbo(this, 105, 103, textureX, textureY); // Box 3
		gunModel[3] = new ModelRendererTurbo(this, 80, 54, textureX, textureY); // Box 4
		gunModel[4] = new ModelRendererTurbo(this, 149, 80, textureX, textureY); // Box 5
		gunModel[5] = new ModelRendererTurbo(this, 1, 96, textureX, textureY); // Box 7
		gunModel[6] = new ModelRendererTurbo(this, 1, 76, textureX, textureY); // Box 8
		gunModel[7] = new ModelRendererTurbo(this, 30, 76, textureX, textureY); // Box 9
		gunModel[8] = new ModelRendererTurbo(this, 34, 98, textureX, textureY); // Box 10
		gunModel[9] = new ModelRendererTurbo(this, 80, 80, textureX, textureY); // Box 11
		gunModel[10] = new ModelRendererTurbo(this, 80, 65, textureX, textureY); // Box 12
		gunModel[11] = new ModelRendererTurbo(this, 80, 131, textureX, textureY); // Box 13
		gunModel[12] = new ModelRendererTurbo(this, 80, 146, textureX, textureY); // Box 14
		gunModel[13] = new ModelRendererTurbo(this, 80, 146, textureX, textureY); // Box 16
		gunModel[14] = new ModelRendererTurbo(this, 132, 107, textureX, textureY); // Box 17
		gunModel[15] = new ModelRendererTurbo(this, 1, 11, textureX, textureY); // Box 18
		gunModel[16] = new ModelRendererTurbo(this, 157, 109, textureX, textureY); // Box 19
		gunModel[17] = new ModelRendererTurbo(this, 157, 109, textureX, textureY); // Box 20
		gunModel[18] = new ModelRendererTurbo(this, 80, 120, textureX, textureY); // Box 21
		gunModel[19] = new ModelRendererTurbo(this, 118, 122, textureX, textureY); // Box 22
		gunModel[20] = new ModelRendererTurbo(this, 80, 197, textureX, textureY); // Box 23
		gunModel[21] = new ModelRendererTurbo(this, 80, 158, textureX, textureY); // Box 24
		gunModel[22] = new ModelRendererTurbo(this, 80, 175, textureX, textureY); // Box 25
		gunModel[23] = new ModelRendererTurbo(this, 80, 186, textureX, textureY); // Box 26
		gunModel[24] = new ModelRendererTurbo(this, 107, 135, textureX, textureY); // Box 27
		gunModel[25] = new ModelRendererTurbo(this, 107, 148, textureX, textureY); // Box 28
		gunModel[26] = new ModelRendererTurbo(this, 107, 148, textureX, textureY); // Box 29
		gunModel[27] = new ModelRendererTurbo(this, 157, 158, textureX, textureY); // Box 30
		gunModel[28] = new ModelRendererTurbo(this, 158, 11, textureX, textureY); // Box 31
		gunModel[29] = new ModelRendererTurbo(this, 158, 1, textureX, textureY); // Box 32
		gunModel[30] = new ModelRendererTurbo(this, 158, 1, textureX, textureY); // Box 33
		gunModel[31] = new ModelRendererTurbo(this, 80, 1, textureX, textureY); // Box 34
		gunModel[32] = new ModelRendererTurbo(this, 80, 1, textureX, textureY); // Box 35
		gunModel[33] = new ModelRendererTurbo(this, 80, 1, textureX, textureY); // Box 36
		gunModel[34] = new ModelRendererTurbo(this, 177, 9, textureX, textureY); // Box 40
		gunModel[35] = new ModelRendererTurbo(this, 177, 15, textureX, textureY); // Box 41
		gunModel[36] = new ModelRendererTurbo(this, 177, 9, textureX, textureY); // Box 43
		gunModel[37] = new ModelRendererTurbo(this, 1, 22, textureX, textureY); // Box 44
		gunModel[38] = new ModelRendererTurbo(this, 149, 69, textureX, textureY); // Box 50
		gunModel[39] = new ModelRendererTurbo(this, 149, 73, textureX, textureY); // Box 51
		gunModel[40] = new ModelRendererTurbo(this, 149, 30, textureX, textureY); // Box 52
		gunModel[41] = new ModelRendererTurbo(this, 80, 41, textureX, textureY); // Box 53
		gunModel[42] = new ModelRendererTurbo(this, 135, 42, textureX, textureY); // Box 54
		gunModel[43] = new ModelRendererTurbo(this, 180, 24, textureX, textureY); // Box 55
		gunModel[44] = new ModelRendererTurbo(this, 80, 175, textureX, textureY); // Box 57
		gunModel[45] = new ModelRendererTurbo(this, 80, 102, textureX, textureY); // Box 58
		gunModel[46] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 59
		gunModel[47] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 60
		gunModel[48] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 61
		gunModel[49] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 62
		gunModel[50] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 63
		gunModel[51] = new ModelRendererTurbo(this, 107, 127, textureX, textureY); // Box 65
		gunModel[52] = new ModelRendererTurbo(this, 107, 127, textureX, textureY); // Box 66
		gunModel[53] = new ModelRendererTurbo(this, 23, 1, textureX, textureY); // Box 78
		gunModel[54] = new ModelRendererTurbo(this, 28, 1, textureX, textureY); // Box 79
		gunModel[55] = new ModelRendererTurbo(this, 28, 1, textureX, textureY); // Box 81
		gunModel[56] = new ModelRendererTurbo(this, 124, 137, textureX, textureY); // Box 98
		gunModel[57] = new ModelRendererTurbo(this, 124, 137, textureX, textureY); // Box 99
		gunModel[58] = new ModelRendererTurbo(this, 124, 137, textureX, textureY); // Box 100
		gunModel[59] = new ModelRendererTurbo(this, 124, 137, textureX, textureY); // Box 101
		gunModel[60] = new ModelRendererTurbo(this, 124, 137, textureX, textureY); // Box 102
		gunModel[61] = new ModelRendererTurbo(this, 124, 137, textureX, textureY); // Box 103

		gunModel[0].addBox(0F, 0F, 0F, 26, 10, 8, 0F); // Box 1
		gunModel[0].setRotationPoint(-11F, -21.5F, -4F);

		gunModel[1].addShapeBox(0F, 0F, 0F, 20, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F); // Box 2
		gunModel[1].setRotationPoint(-16F, -11.5F, -4F);

		gunModel[2].addShapeBox(0F, 0F, 0F, 5, 8, 8, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 3
		gunModel[2].setRotationPoint(-16F, -19.5F, -4F);

		gunModel[3].addBox(0F, 0F, 0F, 47, 3, 7, 0F); // Box 4
		gunModel[3].setRotationPoint(-13F, -22.5F, -3.5F);

		gunModel[4].addShapeBox(0F, 0F, 0F, 17, 4, 8, 0F, 0F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 5
		gunModel[4].setRotationPoint(13F, -11.5F, -4F);

		gunModel[5].addShapeBox(0F, 0F, 0F, 10, 4, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F); // Box 7
		gunModel[5].setRotationPoint(-10F, -10.5F, -3F);

		gunModel[6].addShapeBox(0F, 0F, 0F, 8, 13, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, -2F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 4F, -2F, 0F); // Box 8
		gunModel[6].setRotationPoint(-8F, -6.5F, -3F);

		gunModel[7].addShapeBox(0F, 0F, 0F, 1, 13, 6, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 4F, 0F, 0F, -4F, 0F, -1F, -4F, 0F, -1F, 4F, 0F, 0F); // Box 9
		gunModel[7].setRotationPoint(0F, -6.5F, -3F);

		gunModel[8].addShapeBox(0F, 0F, 0F, 1, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 10
		gunModel[8].setRotationPoint(0F, -8.5F, -3F);

		gunModel[9].addShapeBox(0F, 0F, 0F, 24, 1, 10, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 11
		gunModel[9].setRotationPoint(-9F, -20.5F, -5F);

		gunModel[10].addBox(0F, 0F, 0F, 24, 4, 10, 0F); // Box 12
		gunModel[10].setRotationPoint(-9F, -19.5F, -5F);

		gunModel[11].addBox(0F, 0F, 0F, 4, 5, 9, 0F); // Box 13
		gunModel[11].setRotationPoint(34F, -17.5F, -4.5F);

		gunModel[12].addShapeBox(0F, 0F, 0F, 4, 2, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 14
		gunModel[12].setRotationPoint(34F, -12.5F, -4.5F);

		gunModel[13].addShapeBox(0F, 0F, 0F, 4, 2, 9, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 16
		gunModel[13].setRotationPoint(34F, -19.5F, -4.5F);

		gunModel[14].addBox(0F, 0F, 0F, 4, 4, 8, 0F); // Box 17
		gunModel[14].setRotationPoint(34F, -21.5F, -4F);

		gunModel[15].addBox(0F, 0F, 0F, 6, 3, 7, 0F); // Box 18
		gunModel[15].setRotationPoint(32F, -24.5F, -3.5F);

		gunModel[16].addShapeBox(0F, 0F, 0F, 2, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F); // Box 19
		gunModel[16].setRotationPoint(11F, -11.5F, -4F);

		gunModel[17].addShapeBox(0F, 0F, 0F, 2, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F); // Box 20
		gunModel[17].setRotationPoint(4F, -11.5F, -4F);

		gunModel[18].addBox(0F, 0F, 0F, 5, 2, 8, 0F); // Box 21
		gunModel[18].setRotationPoint(6F, -11.5F, -4F);

		gunModel[19].addBox(0F, 0F, 0F, 1, 4, 4, 0F); // Box 22
		gunModel[19].setRotationPoint(38F, -21.5F, -2F);

		gunModel[20].addShapeBox(0F, 0F, 0F, 30, 1, 6, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 23
		gunModel[20].setRotationPoint(39F, -22.5F, -3F);

		gunModel[21].addShapeBox(0F, 0F, 0F, 30, 8, 8, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 24
		gunModel[21].setRotationPoint(39F, -21.5F, -4F);

		gunModel[22].addShapeBox(0F, 0F, 0F, 30, 1, 9, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 25
		gunModel[22].setRotationPoint(39F, -13.5F, -4.5F);

		gunModel[23].addBox(0F, 0F, 0F, 30, 1, 9, 0F); // Box 26
		gunModel[23].setRotationPoint(39F, -12.5F, -4.5F);

		gunModel[24].addBox(0F, 0F, 0F, 1, 3, 7, 0F); // Box 27
		gunModel[24].setRotationPoint(38F, -16.5F, -3.5F);

		gunModel[25].addShapeBox(0F, 0F, 0F, 1, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 28
		gunModel[25].setRotationPoint(38F, -18.5F, -3.5F);

		gunModel[26].addShapeBox(0F, 0F, 0F, 1, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 29
		gunModel[26].setRotationPoint(38F, -13.5F, -3.5F);

		gunModel[27].addShapeBox(0F, 0F, 0F, 15, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.75F, 0F, -2F, 0.75F, 0F, -2F, 0.75F, 0F, 0F, 0.75F, 0F); // Box 30
		gunModel[27].setRotationPoint(39.5F, -15.5F, -4F);

		gunModel[28].addBox(0F, 0F, 0F, 2, 3, 7, 0F); // Box 31
		gunModel[28].setRotationPoint(69F, -16.5F, -3.5F);

		gunModel[29].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 32
		gunModel[29].setRotationPoint(69F, -13.5F, -3.5F);

		gunModel[30].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 33
		gunModel[30].setRotationPoint(69F, -18.5F, -3.5F);

		gunModel[31].addBox(0F, 0F, 0F, 24, 2, 6, 0F); // Box 34
		gunModel[31].setRotationPoint(71F, -16F, -3F);

		gunModel[32].addShapeBox(0F, 0F, 0F, 24, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 35
		gunModel[32].setRotationPoint(71F, -18F, -3F);

		gunModel[33].addShapeBox(0F, 0F, 0F, 24, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 36
		gunModel[33].setRotationPoint(71F, -14F, -3F);

		gunModel[34].addShapeBox(0F, 0F, 0F, 4, 1, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 40
		gunModel[34].setRotationPoint(69F, -22F, -2F);

		gunModel[35].addBox(0F, 0F, 0F, 4, 2, 4, 0F); // Box 41
		gunModel[35].setRotationPoint(69F, -21F, -2F);

		gunModel[36].addShapeBox(0F, 0F, 0F, 4, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 43
		gunModel[36].setRotationPoint(69F, -19F, -2F);

		gunModel[37].addBox(0F, 0F, 0F, 6, 1, 4, 0F); // Box 44
		gunModel[37].setRotationPoint(63F, -23.5F, -2F);

		gunModel[38].addShapeBox(0F, 0F, 0F, 13, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 50
		gunModel[38].setRotationPoint(15F, -20.5F, 3F);

		gunModel[39].addBox(0F, 0F, 0F, 13, 4, 2, 0F); // Box 51
		gunModel[39].setRotationPoint(15F, -19.5F, 3F);

		gunModel[40].addBox(0F, 0F, 0F, 19, 2, 8, 0F); // Box 52
		gunModel[40].setRotationPoint(15F, -21.5F, -4F);

		gunModel[41].addBox(0F, 0F, 0F, 19, 4, 8, 0F); // Box 53
		gunModel[41].setRotationPoint(15F, -15.5F, -4F);

		gunModel[42].addBox(0F, 0F, 0F, 19, 4, 7, 0F); // Box 54
		gunModel[42].setRotationPoint(15F, -19.5F, -3F);

		gunModel[43].addBox(0F, 0F, 0F, 5, 4, 1, 0F); // Box 55
		gunModel[43].setRotationPoint(29F, -19.5F, -4F);

		gunModel[44].addShapeBox(0F, 0F, 0F, 30, 1, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 57
		gunModel[44].setRotationPoint(39F, -11.5F, -4.5F);

		gunModel[45].addShapeBox(0F, 0F, 0F, 5, 10, 7, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 58
		gunModel[45].setRotationPoint(-18F, -20.5F, -3.5F);

		gunModel[46].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 59
		gunModel[46].setRotationPoint(26F, -24.5F, -3.5F);

		gunModel[47].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 60
		gunModel[47].setRotationPoint(20F, -24.5F, -3.5F);

		gunModel[48].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 61
		gunModel[48].setRotationPoint(8F, -24.5F, -3.5F);

		gunModel[49].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 62
		gunModel[49].setRotationPoint(14F, -24.5F, -3.5F);

		gunModel[50].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 63
		gunModel[50].setRotationPoint(2F, -24.5F, -3.5F);

		gunModel[51].addShapeBox(0F, 0F, 0F, 4, 2, 1, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 65
		gunModel[51].setRotationPoint(-4F, -14.5F, 3.5F);

		gunModel[52].addShapeBox(0F, 0F, 0F, 4, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 66
		gunModel[52].setRotationPoint(-4F, -12.5F, 3.5F);

		gunModel[53].addShapeBox(0F, 0F, 0F, 1, 4, 1, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 78
		gunModel[53].setRotationPoint(25F, -23.5F, 4F);

		gunModel[54].addShapeBox(0F, 1F, 0F, 10, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 79
		gunModel[54].setRotationPoint(16.2F, -24.5F, 7F);
		gunModel[54].rotateAngleX = -0.61086524F;

		gunModel[55].addShapeBox(0F, 0F, 0F, 10, 1, 2, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 81
		gunModel[55].setRotationPoint(16.2F, -24.5F, 7F);
		gunModel[55].rotateAngleX = -0.61086524F;

		gunModel[56].addShapeBox(0F, 0F, 0F, 2, 1, 9, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 98
		gunModel[56].setRotationPoint(2.5F, -10.5F, -4.5F);

		gunModel[57].addShapeBox(0F, 0F, 0F, 2, 1, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 99
		gunModel[57].setRotationPoint(2.5F, -9.5F, -4.5F);

		gunModel[58].addShapeBox(0F, 0F, 0F, 2, 1, 9, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 100
		gunModel[58].setRotationPoint(-15.5F, -13.5F, -4.5F);

		gunModel[59].addShapeBox(0F, 0F, 0F, 2, 1, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 101
		gunModel[59].setRotationPoint(-15.5F, -12.5F, -4.5F);

		gunModel[60].addShapeBox(0F, 0F, 0F, 2, 1, 9, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 102
		gunModel[60].setRotationPoint(-15.5F, -19.5F, -4.5F);

		gunModel[61].addShapeBox(0F, 0F, 0F, 2, 1, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 103
		gunModel[61].setRotationPoint(-15.5F, -18.5F, -4.5F);


		defaultBarrelModel = new ModelRendererTurbo[3];
		defaultBarrelModel[0] = new ModelRendererTurbo(this, 119, 12, textureX, textureY); // Box 37
		defaultBarrelModel[1] = new ModelRendererTurbo(this, 80, 11, textureX, textureY); // Box 38
		defaultBarrelModel[2] = new ModelRendererTurbo(this, 119, 12, textureX, textureY); // Box 39

		defaultBarrelModel[0].addShapeBox(0F, 0F, 0F, 12, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 37
		defaultBarrelModel[0].setRotationPoint(95F, -18.5F, -3.5F);

		defaultBarrelModel[1].addBox(0F, 0F, 0F, 12, 3, 7, 0F); // Box 38
		defaultBarrelModel[1].setRotationPoint(95F, -16.5F, -3.5F);

		defaultBarrelModel[2].addShapeBox(0F, 0F, 0F, 12, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 39
		defaultBarrelModel[2].setRotationPoint(95F, -13.5F, -3.5F);


		defaultScopeModel = new ModelRendererTurbo[11];
		defaultScopeModel[0] = new ModelRendererTurbo(this, 28, 8, textureX, textureY); // Box 46
		defaultScopeModel[1] = new ModelRendererTurbo(this, 28, 8, textureX, textureY); // Box 47
		defaultScopeModel[2] = new ModelRendererTurbo(this, 28, 5, textureX, textureY); // Box 48
		defaultScopeModel[3] = new ModelRendererTurbo(this, 28, 5, textureX, textureY); // Box 49
		defaultScopeModel[4] = new ModelRendererTurbo(this, 28, 18, textureX, textureY); // Box 64
		defaultScopeModel[5] = new ModelRendererTurbo(this, 43, 8, textureX, textureY); // Box 91
		defaultScopeModel[6] = new ModelRendererTurbo(this, 43, 5, textureX, textureY); // Box 92
		defaultScopeModel[7] = new ModelRendererTurbo(this, 43, 8, textureX, textureY); // Box 93
		defaultScopeModel[8] = new ModelRendererTurbo(this, 43, 5, textureX, textureY); // Box 94
		defaultScopeModel[9] = new ModelRendererTurbo(this, 124, 148, textureX, textureY); // Box 95
		defaultScopeModel[10] = new ModelRendererTurbo(this, 124, 148, textureX, textureY); // Box 97

		defaultScopeModel[0].addBox(0F, 0F, 0F, 6, 3, 1, 0F); // Box 46
		defaultScopeModel[0].setRotationPoint(63F, -26.5F, -2F);

		defaultScopeModel[1].addBox(0F, 0F, 0F, 6, 3, 1, 0F); // Box 47
		defaultScopeModel[1].setRotationPoint(63F, -26.5F, 1F);

		defaultScopeModel[2].addShapeBox(0F, 0F, 0F, 6, 1, 1, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 48
		defaultScopeModel[2].setRotationPoint(63F, -27.5F, 1F);

		defaultScopeModel[3].addShapeBox(0F, 0F, 0F, 6, 1, 1, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 49
		defaultScopeModel[3].setRotationPoint(63F, -27.5F, -2F);

		defaultScopeModel[4].addShapeBox(0F, 0F, 0F, 7, 2, 7, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 64
		defaultScopeModel[4].setRotationPoint(-7F, -24.5F, -3.5F);

		defaultScopeModel[5].addBox(0F, 0F, 0F, 5, 4, 1, 0F); // Box 91
		defaultScopeModel[5].setRotationPoint(-5F, -28.5F, 2F);

		defaultScopeModel[6].addShapeBox(0F, 0F, 0F, 5, 1, 1, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 92
		defaultScopeModel[6].setRotationPoint(-5F, -29.5F, 2F);

		defaultScopeModel[7].addBox(0F, 0F, 0F, 5, 4, 1, 0F); // Box 93
		defaultScopeModel[7].setRotationPoint(-5F, -28.5F, -3F);

		defaultScopeModel[8].addShapeBox(0F, 0F, 0F, 5, 1, 1, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 94
		defaultScopeModel[8].setRotationPoint(-5F, -29.5F, -3F);

		defaultScopeModel[9].addShapeBox(0F, 0F, 0F, 2, 1, 8, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 95
		defaultScopeModel[9].setRotationPoint(-3.5F, -24F, -4F);

		defaultScopeModel[10].addShapeBox(0F, 0F, 0F, 2, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 97
		defaultScopeModel[10].setRotationPoint(-3.5F, -23F, -4F);


		defaultStockModel = new ModelRendererTurbo[9];
		defaultStockModel[0] = new ModelRendererTurbo(this, 1, 154, textureX, textureY); // Box 82
		defaultStockModel[1] = new ModelRendererTurbo(this, 1, 116, textureX, textureY); // Box 83
		defaultStockModel[2] = new ModelRendererTurbo(this, 1, 107, textureX, textureY); // Box 84
		defaultStockModel[3] = new ModelRendererTurbo(this, 39, 145, textureX, textureY); // Box 85
		defaultStockModel[4] = new ModelRendererTurbo(this, 26, 160, textureX, textureY); // Box 86
		defaultStockModel[5] = new ModelRendererTurbo(this, 39, 132, textureX, textureY); // Box 87
		defaultStockModel[6] = new ModelRendererTurbo(this, 1, 133, textureX, textureY); // Box 88
		defaultStockModel[7] = new ModelRendererTurbo(this, 19, 154, textureX, textureY); // Box 89
		defaultStockModel[8] = new ModelRendererTurbo(this, 26, 132, textureX, textureY); // Box 90

		defaultStockModel[0].addShapeBox(0F, 0F, 0F, 5, 7, 7, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F); // Box 82
		defaultStockModel[0].setRotationPoint(-23F, -17.5F, -3.5F);

		defaultStockModel[1].addShapeBox(0F, 0F, 0F, 25, 8, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 10F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 10F, 0F); // Box 83
		defaultStockModel[1].setRotationPoint(-48F, -17.5F, -3.5F);

		defaultStockModel[2].addShapeBox(0F, 0F, 0F, 25, 1, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 84
		defaultStockModel[2].setRotationPoint(-48F, -18.5F, -3.5F);

		defaultStockModel[3].addShapeBox(0F, 0F, 0F, 5, 1, 7, 0F, 0F, -2F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, -2F, -1F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F); // Box 85
		defaultStockModel[3].setRotationPoint(-23F, -20.5F, -3.5F);

		defaultStockModel[4].addShapeBox(0F, 0F, 0F, 6, 1, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 86
		defaultStockModel[4].setRotationPoint(-55F, -18.5F, -3.5F);

		defaultStockModel[5].addShapeBox(0F, 0F, 0F, 6, 5, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F); // Box 87
		defaultStockModel[5].setRotationPoint(-55F, -17.5F, -3.5F);

		defaultStockModel[6].addBox(0F, 0F, 0F, 5, 13, 7, 0F); // Box 88
		defaultStockModel[6].setRotationPoint(-54F, -12.5F, -3.5F);

		defaultStockModel[7].addShapeBox(0F, 0F, 0F, 1, 1, 5, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 89
		defaultStockModel[7].setRotationPoint(-49F, -17.5F, -2.5F);

		defaultStockModel[8].addBox(0F, 0F, 0F, 1, 16, 5, 0F); // Box 90
		defaultStockModel[8].setRotationPoint(-49F, -16.5F, -2.5F);


		ammoModel = new ModelRendererTurbo[12];
		ammoModel[0] = new ModelRendererTurbo(this, 1, 47, textureX, textureY); // Box 6
		ammoModel[1] = new ModelRendererTurbo(this, 24, 47, textureX, textureY); // Box 67
		ammoModel[2] = new ModelRendererTurbo(this, 24, 47, textureX, textureY); // Box 68
		ammoModel[3] = new ModelRendererTurbo(this, 43, 47, textureX, textureY); // Box 69
		ammoModel[4] = new ModelRendererTurbo(this, 58, 48, textureX, textureY); // Box 70
		ammoModel[5] = new ModelRendererTurbo(this, 58, 48, textureX, textureY); // Box 71
		ammoModel[6] = new ModelRendererTurbo(this, 58, 48, textureX, textureY); // Box 72
		ammoModel[7] = new ModelRendererTurbo(this, 1, 38, textureX, textureY); // Box 73
		ammoModel[8] = new ModelRendererTurbo(this, 1, 28, textureX, textureY); // Box 74
		ammoModel[9] = new ModelRendererTurbo(this, 1, 28, textureX, textureY); // Box 75
		ammoModel[10] = new ModelRendererTurbo(this, 44, 41, textureX, textureY); // Box 76
		ammoModel[11] = new ModelRendererTurbo(this, 58, 35, textureX, textureY); // Box 77

		ammoModel[0].addBox(0F, 0F, 0F, 5, 22, 6, 0F); // Box 6
		ammoModel[0].setRotationPoint(14F, -12.5F, -3F);

		ammoModel[1].addBox(0F, 0F, 0F, 3, 22, 6, 0F); // Box 67
		ammoModel[1].setRotationPoint(20F, -12.5F, -3F);

		ammoModel[2].addBox(0F, 0F, 0F, 3, 22, 6, 0F); // Box 68
		ammoModel[2].setRotationPoint(24F, -12.5F, -3F);

		ammoModel[3].addBox(0F, 0F, 0F, 1, 22, 6, 0F); // Box 69
		ammoModel[3].setRotationPoint(28F, -12.5F, -3F);

		ammoModel[4].addBox(0F, 0F, 0F, 1, 22, 5, 0F); // Box 70
		ammoModel[4].setRotationPoint(19F, -12.5F, -2.5F);

		ammoModel[5].addBox(0F, 0F, 0F, 1, 22, 5, 0F); // Box 71
		ammoModel[5].setRotationPoint(23F, -12.5F, -2.5F);

		ammoModel[6].addBox(0F, 0F, 0F, 1, 22, 5, 0F); // Box 72
		ammoModel[6].setRotationPoint(27F, -12.5F, -2.5F);

		ammoModel[7].addBox(0F, 0F, 0F, 15, 2, 6, 0F); // Box 73
		ammoModel[7].setRotationPoint(14F, 9.5F, -3F);

		ammoModel[8].addBox(0F, 0F, 0F, 16, 2, 7, 0F); // Box 74
		ammoModel[8].setRotationPoint(13.5F, 5.5F, -3.5F);

		ammoModel[9].addBox(0F, 0F, 0F, 16, 2, 7, 0F); // Box 75
		ammoModel[9].setRotationPoint(13.5F, 2.5F, -3.5F);

		ammoModel[10].addShapeBox(0F, 0F, 0F, 10, 1, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 76
		ammoModel[10].setRotationPoint(15F, -13.5F, -2F);

		ammoModel[11].addShapeBox(0F, 0F, 0F, 3, 1, 4, 0F, 0F, 0F, -1F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 77
		ammoModel[11].setRotationPoint(25F, -13.5F, -2F);


		slideModel = new ModelRendererTurbo[1];
		slideModel[0] = new ModelRendererTurbo(this, 149, 24, textureX, textureY); // Box 56

		slideModel[0].addShapeBox(0F, 0F, 0F, 14, 4, 1, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 56
		slideModel[0].setRotationPoint(15F, -19.5F, -3.5F);

		barrelAttachPoint = new Vector3f(95F /16F, 15F /16F, 0F /16F);
		stockAttachPoint = new Vector3f(-18F /16F, 15F /16F, 0F /16F);
		scopeAttachPoint = new Vector3f(14F /16F, 22F /16F, 0F /16F);

		gunSlideDistance = 0.8F;
		animationType = EnumAnimationType.BOTTOM_CLIP;

		translateAll(0F, 0F, 0F);


		flipAll();
	}
}