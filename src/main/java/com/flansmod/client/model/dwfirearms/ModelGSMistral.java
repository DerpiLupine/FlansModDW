package com.flansmod.client.model.dwfirearms; //Path where the model is located

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelGSMistral extends ModelGun //Same as Filename
{
	int textureX = 512;
	int textureY = 256;

	public ModelGSMistral() //Same as Filename
	{
		gunModel = new ModelRendererTurbo[55];
		gunModel[0] = new ModelRendererTurbo(this, 210, 1, textureX, textureY); // barrelPart1
		gunModel[1] = new ModelRendererTurbo(this, 210, 10, textureX, textureY); // barrelPart2
		gunModel[2] = new ModelRendererTurbo(this, 210, 1, textureX, textureY); // barrelPart3
		gunModel[3] = new ModelRendererTurbo(this, 129, 127, textureX, textureY); // body1
		gunModel[4] = new ModelRendererTurbo(this, 35, 1, textureX, textureY); // frontSight
		gunModel[5] = new ModelRendererTurbo(this, 1, 61, textureX, textureY); // body1
		gunModel[6] = new ModelRendererTurbo(this, 22, 62, textureX, textureY); // body1
		gunModel[7] = new ModelRendererTurbo(this, 113, 88, textureX, textureY); // Box 0
		gunModel[8] = new ModelRendererTurbo(this, 1, 10, textureX, textureY); // Box 1
		gunModel[9] = new ModelRendererTurbo(this, 1, 20, textureX, textureY); // Box 2
		gunModel[10] = new ModelRendererTurbo(this, 1, 33, textureX, textureY); // Box 3
		gunModel[11] = new ModelRendererTurbo(this, 1, 50, textureX, textureY); // Box 4
		gunModel[12] = new ModelRendererTurbo(this, 40, 33, textureX, textureY); // Box 5
		gunModel[13] = new ModelRendererTurbo(this, 40, 20, textureX, textureY); // Box 6
		gunModel[14] = new ModelRendererTurbo(this, 40, 10, textureX, textureY); // Box 7
		gunModel[15] = new ModelRendererTurbo(this, 59, 10, textureX, textureY); // Box 8
		gunModel[16] = new ModelRendererTurbo(this, 59, 20, textureX, textureY); // Box 9
		gunModel[17] = new ModelRendererTurbo(this, 59, 33, textureX, textureY); // Box 10
		gunModel[18] = new ModelRendererTurbo(this, 40, 50, textureX, textureY); // Box 11
		gunModel[19] = new ModelRendererTurbo(this, 112, 127, textureX, textureY); // Box 12
		gunModel[20] = new ModelRendererTurbo(this, 112, 149, textureX, textureY); // Box 14
		gunModel[21] = new ModelRendererTurbo(this, 176, 32, textureX, textureY); // Box 15
		gunModel[22] = new ModelRendererTurbo(this, 49, 75, textureX, textureY); // Box 50
		gunModel[23] = new ModelRendererTurbo(this, 214, 127, textureX, textureY); // Box 51
		gunModel[24] = new ModelRendererTurbo(this, 177, 129, textureX, textureY); // Box 52
		gunModel[25] = new ModelRendererTurbo(this, 112, 19, textureX, textureY); // Box 53
		gunModel[26] = new ModelRendererTurbo(this, 49, 84, textureX, textureY); // Box 54
		gunModel[27] = new ModelRendererTurbo(this, 214, 40, textureX, textureY); // Box 57
		gunModel[28] = new ModelRendererTurbo(this, 112, 29, textureX, textureY); // Box 58
		gunModel[29] = new ModelRendererTurbo(this, 150, 129, textureX, textureY); // Box 77
		gunModel[30] = new ModelRendererTurbo(this, 185, 6, textureX, textureY); // Box 86
		gunModel[31] = new ModelRendererTurbo(this, 185, 12, textureX, textureY); // Box 87
		gunModel[32] = new ModelRendererTurbo(this, 185, 6, textureX, textureY); // Box 88
		gunModel[33] = new ModelRendererTurbo(this, 80, 5, textureX, textureY); // Box 94
		gunModel[34] = new ModelRendererTurbo(this, 89, 1, textureX, textureY); // Box 95
		gunModel[35] = new ModelRendererTurbo(this, 80, 1, textureX, textureY); // Box 96
		gunModel[36] = new ModelRendererTurbo(this, 214, 137, textureX, textureY); // Box 2
		gunModel[37] = new ModelRendererTurbo(this, 214, 137, textureX, textureY); // Box 3
		gunModel[38] = new ModelRendererTurbo(this, 214, 137, textureX, textureY); // Box 4
		gunModel[39] = new ModelRendererTurbo(this, 214, 137, textureX, textureY); // Box 5
		gunModel[40] = new ModelRendererTurbo(this, 218, 147, textureX, textureY); // Box 7
		gunModel[41] = new ModelRendererTurbo(this, 152, 3, textureX, textureY); // Box 5
		gunModel[42] = new ModelRendererTurbo(this, 152, 3, textureX, textureY); // Box 6
		gunModel[43] = new ModelRendererTurbo(this, 143, 1, textureX, textureY); // Box 9
		gunModel[44] = new ModelRendererTurbo(this, 130, 1, textureX, textureY); // Box 10
		gunModel[45] = new ModelRendererTurbo(this, 169, 118, textureX, textureY); // Box 0
		gunModel[46] = new ModelRendererTurbo(this, 155, 29, textureX, textureY); // Box 3
		gunModel[47] = new ModelRendererTurbo(this, 112, 66, textureX, textureY); // Box 8
		gunModel[48] = new ModelRendererTurbo(this, 112, 74, textureX, textureY); // Box 9
		gunModel[49] = new ModelRendererTurbo(this, 193, 40, textureX, textureY); // Box 15
		gunModel[50] = new ModelRendererTurbo(this, 168, 40, textureX, textureY); // Box 16
		gunModel[51] = new ModelRendererTurbo(this, 143, 40, textureX, textureY); // Box 17
		gunModel[52] = new ModelRendererTurbo(this, 112, 40, textureX, textureY); // Box 18
		gunModel[53] = new ModelRendererTurbo(this, 52, 3, textureX, textureY); // Box 15
		gunModel[54] = new ModelRendererTurbo(this, 112, 139, textureX, textureY); // Box 86

		gunModel[0].addShapeBox(0F, 0F, 0F, 16, 6, 2, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // barrelPart1
		gunModel[0].setRotationPoint(31F, -23.5F, -3F);

		gunModel[1].addBox(0F, 0F, 0F, 16, 6, 2, 0F); // barrelPart2
		gunModel[1].setRotationPoint(31F, -23.5F, -1F);

		gunModel[2].addShapeBox(0F, 0F, 0F, 16, 6, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F); // barrelPart3
		gunModel[2].setRotationPoint(31F, -23.5F, 1F);

		gunModel[3].addShapeBox(0F, 0F, 0F, 3, 4, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F); // body1
		gunModel[3].setRotationPoint(-10F, -14F, -3.5F);

		gunModel[4].addShapeBox(0F, 0F, 0F, 5, 2, 3, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, -2F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F); // frontSight
		gunModel[4].setRotationPoint(40F, -28F, -0.5F);

		gunModel[5].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // body1
		gunModel[5].setRotationPoint(-1F, 7F, -3.5F);

		gunModel[6].addShapeBox(0F, 0F, 0F, 3, 1, 7, 0F, 0F, 0F, 0F, -1F, 0F, -1F, -1F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // body1
		gunModel[6].setRotationPoint(-1F, 6F, -3.5F);

		gunModel[7].addBox(0F, 0F, 0F, 25, 4, 7, 0F); // Box 0
		gunModel[7].setRotationPoint(-7F, -14F, -3.5F);

		gunModel[8].addBox(0F, 0F, 0F, 12, 2, 7, 0F); // Box 1
		gunModel[8].setRotationPoint(-8F, -10F, -3.5F);

		gunModel[9].addShapeBox(0F, 0F, 0F, 12, 5, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 3F, 0F, 0F); // Box 2
		gunModel[9].setRotationPoint(-8F, -8F, -3.5F);

		gunModel[10].addShapeBox(0F, 0F, 0F, 12, 9, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F); // Box 3
		gunModel[10].setRotationPoint(-11F, -3F, -3.5F);

		gunModel[11].addBox(0F, 0F, 0F, 12, 3, 7, 0F); // Box 4
		gunModel[11].setRotationPoint(-13F, 6F, -3.5F);

		gunModel[12].addShapeBox(0F, 0F, 0F, 2, 9, 7, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, -1F, -2F, 0F, -1F, 2F, 0F, 0F); // Box 5
		gunModel[12].setRotationPoint(1F, -3F, -3.5F);

		gunModel[13].addShapeBox(0F, 0F, 0F, 2, 5, 7, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, -1F, -3F, 0F, -1F, 3F, 0F, 0F); // Box 6
		gunModel[13].setRotationPoint(4F, -8F, -3.5F);

		gunModel[14].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 7
		gunModel[14].setRotationPoint(4F, -10F, -3.5F);

		gunModel[15].addShapeBox(0F, 0F, 0F, 1, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 8
		gunModel[15].setRotationPoint(-9F, -10F, -3.5F);

		gunModel[16].addShapeBox(0F, 0F, 0F, 1, 5, 7, 0F, -3F, 0F, -1F, 3F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 9
		gunModel[16].setRotationPoint(-12F, -8F, -3.5F);

		gunModel[17].addShapeBox(0F, 0F, 0F, 1, 9, 7, 0F, -2F, 0F, -1F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 10
		gunModel[17].setRotationPoint(-14F, -3F, -3.5F);

		gunModel[18].addShapeBox(0F, 0F, 0F, 1, 3, 7, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 11
		gunModel[18].setRotationPoint(-14F, 6F, -3.5F);

		gunModel[19].addShapeBox(0F, 0F, 0F, 1, 4, 7, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, -2F, 0F, -1F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, -1F); // Box 12
		gunModel[19].setRotationPoint(-11F, -14F, -3.5F);

		gunModel[20].addBox(0F, 0F, 0F, 25, 3, 7, 0F); // Box 14
		gunModel[20].setRotationPoint(19F, -13F, -3.5F);

		gunModel[21].addBox(0F, 0F, 0F, 27, 1, 6, 0F); // Box 15
		gunModel[21].setRotationPoint(18F, -14F, -3F);

		gunModel[22].addShapeBox(0F, 0F, 0F, 1, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F); // Box 50
		gunModel[22].setRotationPoint(18F, -10F, -3F);

		gunModel[23].addShapeBox(0F, 0F, 0F, 4, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F); // Box 51
		gunModel[23].setRotationPoint(19F, -10F, -3.5F);

		gunModel[24].addShapeBox(0F, 0F, 0F, 7, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F); // Box 52
		gunModel[24].setRotationPoint(11F, -10F, -3.5F);

		gunModel[25].addBox(0F, 0F, 0F, 56, 2, 7, 0F); // Box 53
		gunModel[25].setRotationPoint(-11F, -16F, -3.5F);

		gunModel[26].addBox(0F, 0F, 0F, 1, 3, 6, 0F); // Box 54
		gunModel[26].setRotationPoint(18F, -13F, -3F);

		gunModel[27].addShapeBox(0F, 0F, 0F, 4, 7, 8, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 57
		gunModel[27].setRotationPoint(32F, -24F, -4F);

		gunModel[28].addShapeBox(0F, 0F, 0F, 13, 2, 8, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 58
		gunModel[28].setRotationPoint(30F, -26F, -4F);

		gunModel[29].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F); // Box 77
		gunModel[29].setRotationPoint(-14F, -16F, -3.5F);

		gunModel[30].addShapeBox(0F, 0F, 0F, 9, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F); // Box 86
		gunModel[30].setRotationPoint(37F, -16F, 1F);

		gunModel[31].addBox(0F, 0F, 0F, 9, 4, 2, 0F); // Box 87
		gunModel[31].setRotationPoint(37F, -16F, -1F);

		gunModel[32].addShapeBox(0F, 0F, 0F, 9, 4, 1, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 88
		gunModel[32].setRotationPoint(37F, -16F, -2F);

		gunModel[33].addBox(0F, 0F, 0F, 12, 3, 1, 0F); // Box 94
		gunModel[33].setRotationPoint(4F, -14F, 3F);

		gunModel[34].addBox(0F, 0F, 0F, 2, 2, 1, 0F); // Box 95
		gunModel[34].setRotationPoint(14F, -13.5F, 3.5F);

		gunModel[35].addShapeBox(0F, 0F, 0F, 3, 2, 1, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F); // Box 96
		gunModel[35].setRotationPoint(11F, -13.5F, 3.5F);

		gunModel[36].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 2
		gunModel[36].setRotationPoint(23F, -8F, -3.5F);

		gunModel[37].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 3
		gunModel[37].setRotationPoint(29F, -8F, -3.5F);

		gunModel[38].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 4
		gunModel[38].setRotationPoint(41F, -8F, -3.5F);

		gunModel[39].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 5
		gunModel[39].setRotationPoint(35F, -8F, -3.5F);

		gunModel[40].addShapeBox(0F, 0F, 0F, 1, 5, 7, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, 0F, 0F); // Box 7
		gunModel[40].setRotationPoint(44F, -13F, -3.5F);

		gunModel[41].addShapeBox(0F, 0F, 0F, 3, 1, 4, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 5
		gunModel[41].setRotationPoint(-13.5F, -20F, -2F);

		gunModel[42].addShapeBox(0F, 0F, 0F, 3, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F); // Box 6
		gunModel[42].setRotationPoint(-13.5F, -19F, -2F);

		gunModel[43].addShapeBox(0F, 0F, 0F, 1, 4, 3, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F); // Box 9
		gunModel[43].setRotationPoint(-14F, -24F, -1.5F);

		gunModel[44].addBox(0F, 0F, 0F, 3, 4, 3, 0F); // Box 10
		gunModel[44].setRotationPoint(-13F, -24F, -1.5F);

		gunModel[45].addBox(0F, 0F, 0F, 27, 1, 7, 0F); // Box 0
		gunModel[45].setRotationPoint(18F, -17F, -3.5F);

		gunModel[46].addShapeBox(0F, 0F, 0F, 2, 2, 8, 0F, 0F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 3
		gunModel[46].setRotationPoint(43F, -26F, -4F);

		gunModel[47].addShapeBox(0F, 0F, 0F, 10, 2, 5, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 8
		gunModel[47].setRotationPoint(10F, -25.5F, -3F);

		gunModel[48].addBox(0F, 0F, 0F, 10, 8, 5, 0F); // Box 9
		gunModel[48].setRotationPoint(10F, -23.5F, -3F);

		gunModel[49].addBox(0F, 0F, 0F, 2, 7, 8, 0F); // Box 15
		gunModel[49].setRotationPoint(30F, -24F, -4F);

		gunModel[50].addShapeBox(0F, 0F, 0F, 4, 7, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F); // Box 16
		gunModel[50].setRotationPoint(37F, -24F, -4F);

		gunModel[51].addBox(0F, 0F, 0F, 4, 7, 8, 0F); // Box 17
		gunModel[51].setRotationPoint(41F, -24F, -4F);

		gunModel[52].addShapeBox(0F, 0F, 0F, 7, 7, 8, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F); // Box 18
		gunModel[52].setRotationPoint(33F, -24F, -4F);

		gunModel[53].addBox(0F, 0F, 0F, 1, 2, 1, 0F); // Box 15
		gunModel[53].setRotationPoint(44F, -26F, -0.5F);

		gunModel[54].addBox(0F, 0F, 0F, 21, 2, 7, 0F); // Box 86
		gunModel[54].setRotationPoint(23F, -10F, -3.5F);


		ammoModel = new ModelRendererTurbo[10];
		ammoModel[0] = new ModelRendererTurbo(this, 32, 95, textureX, textureY); // ammoPart1
		ammoModel[1] = new ModelRendererTurbo(this, 1, 94, textureX, textureY); // ammoPart2
		ammoModel[2] = new ModelRendererTurbo(this, 1, 104, textureX, textureY); // ammoPart4
		ammoModel[3] = new ModelRendererTurbo(this, 24, 104, textureX, textureY); // ammoPart5
		ammoModel[4] = new ModelRendererTurbo(this, 1, 71, textureX, textureY); // Box 59
		ammoModel[5] = new ModelRendererTurbo(this, 32, 72, textureX, textureY); // Box 60
		ammoModel[6] = new ModelRendererTurbo(this, 24, 104, textureX, textureY); // Box 13
		ammoModel[7] = new ModelRendererTurbo(this, 40, 109, textureX, textureY); // Box 99
		ammoModel[8] = new ModelRendererTurbo(this, 1, 109, textureX, textureY); // Box 100
		ammoModel[9] = new ModelRendererTurbo(this, 59, 109, textureX, textureY); // Box 101

		ammoModel[0].addShapeBox(0F, 0F, 0F, 3, 3, 5, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F); // ammoPart1
		ammoModel[0].setRotationPoint(1F, -8.5F, -2.5F);

		ammoModel[1].addShapeBox(0F, 0F, 0F, 9, 3, 6, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F); // ammoPart2
		ammoModel[1].setRotationPoint(-8F, -9F, -3F);

		ammoModel[2].addShapeBox(0F, 0F, 0F, 8, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // ammoPart4
		ammoModel[2].setRotationPoint(-7F, -10F, -1.5F);

		ammoModel[3].addShapeBox(0F, 0F, 0F, 2, 1, 3, 0F, 0F, 0F, -1F, 0F, -0.5F, -1F, 0F, -0.5F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F); // ammoPart5
		ammoModel[3].setRotationPoint(1F, -10F, -1.5F);

		ammoModel[4].addShapeBox(0F, 3F, 0F, 9, 16, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 4F, 0F, 0F); // Box 59
		ammoModel[4].setRotationPoint(-9F, -9F, -3F);

		ammoModel[5].addShapeBox(0F, 2F, 0F, 3, 16, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 4F, 0F, 0F); // Box 60
		ammoModel[5].setRotationPoint(0F, -7.5F, -2.5F);

		ammoModel[6].addShapeBox(0F, 0F, 0F, 2, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F); // Box 13
		ammoModel[6].setRotationPoint(1F, -9F, -1.5F);

		ammoModel[7].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 99
		ammoModel[7].setRotationPoint(-1F, 9.5F, -3.5F);

		ammoModel[8].addBox(0F, 0F, 0F, 12, 2, 7, 0F); // Box 100
		ammoModel[8].setRotationPoint(-13F, 9.5F, -3.5F);

		ammoModel[9].addShapeBox(0F, 0F, 0F, 1, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 101
		ammoModel[9].setRotationPoint(-14F, 9.5F, -3.5F);


		slideModel = new ModelRendererTurbo[22];
		slideModel[0] = new ModelRendererTurbo(this, 112, 101, textureX, textureY); // bodyRail1
		slideModel[1] = new ModelRendererTurbo(this, 179, 100, textureX, textureY); // slide1
		slideModel[2] = new ModelRendererTurbo(this, 131, 56, textureX, textureY); // Box 36
		slideModel[3] = new ModelRendererTurbo(this, 198, 100, textureX, textureY); // Box 42
		slideModel[4] = new ModelRendererTurbo(this, 160, 100, textureX, textureY); // Box 72
		slideModel[5] = new ModelRendererTurbo(this, 141, 100, textureX, textureY); // Box 73
		slideModel[6] = new ModelRendererTurbo(this, 141, 100, textureX, textureY); // Box 74
		slideModel[7] = new ModelRendererTurbo(this, 141, 100, textureX, textureY); // Box 75
		slideModel[8] = new ModelRendererTurbo(this, 121, 1, textureX, textureY); // Box 76
		slideModel[9] = new ModelRendererTurbo(this, 112, 56, textureX, textureY); // Box 79
		slideModel[10] = new ModelRendererTurbo(this, 112, 1, textureX, textureY); // Box 3
		slideModel[11] = new ModelRendererTurbo(this, 130, 9, textureX, textureY); // Box 4
		slideModel[12] = new ModelRendererTurbo(this, 177, 139, textureX, textureY); // Box 6
		slideModel[13] = new ModelRendererTurbo(this, 177, 150, textureX, textureY); // Box 7
		slideModel[14] = new ModelRendererTurbo(this, 186, 56, textureX, textureY); // Box 10
		slideModel[15] = new ModelRendererTurbo(this, 186, 66, textureX, textureY); // Box 11
		slideModel[16] = new ModelRendererTurbo(this, 143, 66, textureX, textureY); // Box 12
		slideModel[17] = new ModelRendererTurbo(this, 143, 71, textureX, textureY); // Box 13
		slideModel[18] = new ModelRendererTurbo(this, 112, 117, textureX, textureY); // Box 14
		slideModel[19] = new ModelRendererTurbo(this, 18, 1, textureX, textureY); // Box 0
		slideModel[20] = new ModelRendererTurbo(this, 80, 10, textureX, textureY); // Box 2
		slideModel[21] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 3

		slideModel[0].addShapeBox(0F, 0F, 0F, 7, 8, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F); // bodyRail1
		slideModel[0].setRotationPoint(-9F, -24F, -3.5F);

		slideModel[1].addShapeBox(0F, 0F, 0F, 1, 8, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F); // slide1
		slideModel[1].setRotationPoint(-10F, -24F, -4F);

		slideModel[2].addShapeBox(0F, 0F, 0F, 20, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 36
		slideModel[2].setRotationPoint(-10F, -26F, -3.5F);

		slideModel[3].addBox(0F, 0F, 0F, 9, 8, 8, 0F); // Box 42
		slideModel[3].setRotationPoint(1F, -24F, -4F);

		slideModel[4].addShapeBox(0F, 0F, 0F, 1, 8, 8, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 72
		slideModel[4].setRotationPoint(0F, -24F, -4F);

		slideModel[5].addShapeBox(0F, 0F, 0F, 1, 8, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F); // Box 73
		slideModel[5].setRotationPoint(-8F, -24F, -4F);

		slideModel[6].addShapeBox(0F, 0F, 0F, 1, 8, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F); // Box 74
		slideModel[6].setRotationPoint(-6F, -24F, -4F);

		slideModel[7].addShapeBox(0F, 0F, 0F, 1, 8, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F); // Box 75
		slideModel[7].setRotationPoint(-4F, -24F, -4F);

		slideModel[8].addShapeBox(0F, 0F, 0F, 2, 8, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F); // Box 76
		slideModel[8].setRotationPoint(-12F, -24F, -4F);

		slideModel[9].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, -1F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, -1F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 79
		slideModel[9].setRotationPoint(-12F, -26F, -3.5F);

		slideModel[10].addShapeBox(0F, 0F, 0F, 2, 8, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F); // Box 3
		slideModel[10].setRotationPoint(-12F, -24F, 2F);

		slideModel[11].addShapeBox(0F, 0F, 0F, 3, 2, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F); // Box 4
		slideModel[11].setRotationPoint(-13.5F, -18F, -2F);

		slideModel[12].addBox(0F, 0F, 0F, 8, 2, 8, 0F); // Box 6
		slideModel[12].setRotationPoint(10F, -18F, -4F);

		slideModel[13].addBox(0F, 0F, 0F, 12, 1, 8, 0F); // Box 7
		slideModel[13].setRotationPoint(18F, -18F, -4F);

		slideModel[14].addShapeBox(0F, 0F, 0F, 10, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 10
		slideModel[14].setRotationPoint(20F, -26F, -3.5F);

		slideModel[15].addBox(0F, 0F, 0F, 10, 5, 7, 0F); // Box 11
		slideModel[15].setRotationPoint(20F, -24F, -3.5F);

		slideModel[16].addShapeBox(0F, 0F, 0F, 10, 2, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 12
		slideModel[16].setRotationPoint(10F, -26F, 1.5F);

		slideModel[17].addBox(0F, 0F, 0F, 10, 6, 2, 0F); // Box 13
		slideModel[17].setRotationPoint(10F, -24F, 1.5F);

		slideModel[18].addShapeBox(0F, 0F, 0F, 20, 1, 8, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 14
		slideModel[18].setRotationPoint(10F, -19F, -4F);

		slideModel[19].addShapeBox(0F, 0F, 0F, 6, 2, 2, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 0
		slideModel[19].setRotationPoint(-9F, -28.5F, 1F);

		slideModel[20].addBox(0F, 0F, 0F, 6, 3, 6, 0F); // Box 2
		slideModel[20].setRotationPoint(-9F, -26.5F, -3F);

		slideModel[21].addShapeBox(0F, 0F, 0F, 6, 2, 2, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 3
		slideModel[21].setRotationPoint(-9F, -28.5F, -3F);

		barrelAttachPoint = new Vector3f(47F /16F, 23F /16F, 0F /16F);
		scopeAttachPoint = new Vector3f(32F /16F, 27F /16F, 0F /16F);
		gripAttachPoint = new Vector3f(34 /16F, 9F /16F, 0F /16F);

		gunSlideDistance = 1.25F;
		animationType = EnumAnimationType.CUSTOM;

		/* ----Start of Reload Block---- */
		/* Reload Name: Alternate Pistol Clip */
		rotateGunVertical = 10F;
		translateGun = new Vector3f(0.15F, 0.25F, 0F);

		rotateClipVertical = 5F;
		translateClip = new Vector3f(0F, -3F, 0F);
		/* ----End of Reload Block---- */

		flipAll();

		translateAll(0F, 0F, 0F);
	}
}