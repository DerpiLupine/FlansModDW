package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelSF2012Petyr extends ModelGun
{
	int textureX = 512;
	int textureY = 256;

	public ModelSF2012Petyr()
	{
		gunModel = new ModelRendererTurbo[40];
		gunModel[0] = new ModelRendererTurbo(this, 72, 29, textureX, textureY); // body1
		gunModel[1] = new ModelRendererTurbo(this, 72, 50, textureX, textureY); // body10
		gunModel[2] = new ModelRendererTurbo(this, 105, 140, textureX, textureY); // body11
		gunModel[3] = new ModelRendererTurbo(this, 105, 146, textureX, textureY); // body12
		gunModel[4] = new ModelRendererTurbo(this, 72, 85, textureX, textureY); // body2
		gunModel[5] = new ModelRendererTurbo(this, 72, 100, textureX, textureY); // body3
		gunModel[6] = new ModelRendererTurbo(this, 72, 115, textureX, textureY); // body4
		gunModel[7] = new ModelRendererTurbo(this, 72, 126, textureX, textureY); // body5
		gunModel[8] = new ModelRendererTurbo(this, 72, 138, textureX, textureY); // body6
		gunModel[9] = new ModelRendererTurbo(this, 118, 143, textureX, textureY); // body7
		gunModel[10] = new ModelRendererTurbo(this, 72, 72, textureX, textureY); // body8
		gunModel[11] = new ModelRendererTurbo(this, 72, 61, textureX, textureY); // body9
		gunModel[12] = new ModelRendererTurbo(this, 1, 61, textureX, textureY); // grip1
		gunModel[13] = new ModelRendererTurbo(this, 1, 33, textureX, textureY); // grip2
		gunModel[14] = new ModelRendererTurbo(this, 1, 75, textureX, textureY); // grip3
		gunModel[15] = new ModelRendererTurbo(this, 144, 103, textureX, textureY); // handGuard2
		gunModel[16] = new ModelRendererTurbo(this, 1, 23, textureX, textureY); // railBase
		gunModel[17] = new ModelRendererTurbo(this, 72, 20, textureX, textureY); // mainBarrelBottom
		gunModel[18] = new ModelRendererTurbo(this, 72, 11, textureX, textureY); // mainBarrelMiddle
		gunModel[19] = new ModelRendererTurbo(this, 72, 2, textureX, textureY); // mainBarrelTop
		gunModel[20] = new ModelRendererTurbo(this, 36, 56, textureX, textureY); // grip4
		gunModel[21] = new ModelRendererTurbo(this, 144, 135, textureX, textureY); // rail1
		gunModel[22] = new ModelRendererTurbo(this, 44, 41, textureX, textureY); // clipPart
		gunModel[23] = new ModelRendererTurbo(this, 144, 85, textureX, textureY); // handGuard3
		gunModel[24] = new ModelRendererTurbo(this, 144, 135, textureX, textureY); // rail2
		gunModel[25] = new ModelRendererTurbo(this, 144, 135, textureX, textureY); // rail3
		gunModel[26] = new ModelRendererTurbo(this, 144, 135, textureX, textureY); // rail4
		gunModel[27] = new ModelRendererTurbo(this, 144, 123, textureX, textureY); // handGuard1
		gunModel[28] = new ModelRendererTurbo(this, 153, 11, textureX, textureY); // Box 21
		gunModel[29] = new ModelRendererTurbo(this, 153, 21, textureX, textureY); // Box 22
		gunModel[30] = new ModelRendererTurbo(this, 153, 11, textureX, textureY); // Box 23
		gunModel[31] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // ironsightRearBase
		gunModel[32] = new ModelRendererTurbo(this, 22, 1, textureX, textureY); // Box 25
		gunModel[33] = new ModelRendererTurbo(this, 22, 1, textureX, textureY); // Box 26
		gunModel[34] = new ModelRendererTurbo(this, 135, 2, textureX, textureY); // gasBlock1
		gunModel[35] = new ModelRendererTurbo(this, 135, 11, textureX, textureY); // gasBlock2
		gunModel[36] = new ModelRendererTurbo(this, 135, 11, textureX, textureY); // gasBlock2-2
		gunModel[37] = new ModelRendererTurbo(this, 12, 13, textureX, textureY); // ironSight1
		gunModel[38] = new ModelRendererTurbo(this, 12, 20, textureX, textureY); // ironSight2
		gunModel[39] = new ModelRendererTurbo(this, 1, 13, textureX, textureY); // ironSight3

		gunModel[0].addBox(0F, 0F, 0F, 25, 10, 10, 0F); // body1
		gunModel[0].setRotationPoint(-10F, -15F, -5F);

		gunModel[1].addShapeBox(0F, 0F, 0F, 43, 2, 8, 0F, -1F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, -1F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body10
		gunModel[1].setRotationPoint(-8F, -21F, -4F);

		gunModel[2].addShapeBox(0F, 0F, 0F, 3, 2, 3, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body11
		gunModel[2].setRotationPoint(-9F, -20F, -1.5F);

		gunModel[3].addBox(0F, 0F, 0F, 12, 3, 1, 0F); // body12
		gunModel[3].setRotationPoint(14F, -14F, 4.5F);

		gunModel[4].addBox(0F, 0F, 0F, 20, 5, 9, 0F); // body2
		gunModel[4].setRotationPoint(15F, -10F, -4.5F);

		gunModel[5].addBox(0F, 0F, 0F, 20, 4, 10, 0F); // body3
		gunModel[5].setRotationPoint(15F, -14F, -5F);

		gunModel[6].addBox(0F, 0F, 0F, 20, 1, 9, 0F); // body4
		gunModel[6].setRotationPoint(15F, -15F, -4F);

		gunModel[7].addShapeBox(0F, 0F, 0F, 14, 2, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body5
		gunModel[7].setRotationPoint(15F, -17F, -4F);

		gunModel[8].addShapeBox(0F, 0F, 0F, 6, 2, 10, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body6
		gunModel[8].setRotationPoint(29F, -17F, -5F);

		gunModel[9].addBox(0F, 0F, 0F, 6, 1, 1, 0F); // body7
		gunModel[9].setRotationPoint(29F, -15F, -5F);

		gunModel[10].addShapeBox(0F, 0F, 0F, 25, 2, 10, 0F, -1F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, -1F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body8
		gunModel[10].setRotationPoint(-10F, -17F, -5F);

		gunModel[11].addShapeBox(0F, 0F, 0F, 44, 2, 8, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body9
		gunModel[11].setRotationPoint(-9F, -19F, -4F);

		gunModel[12].addShapeBox(0F, 0F, 0F, 12, 3, 10, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F); // grip1
		gunModel[12].setRotationPoint(-9F, -5F, -5F);

		gunModel[13].addShapeBox(0F, 0F, 0F, 11, 12, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 4F, 0F, 0F); // grip2
		gunModel[13].setRotationPoint(-8F, -2F, -5F);

		gunModel[14].addShapeBox(0F, 0F, 0F, 11, 2, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F); // grip3
		gunModel[14].setRotationPoint(-12F, 10F, -5F);

		gunModel[15].addBox(0F, 0F, 0F, 25, 12, 7, 0F); // handGuard2
		gunModel[15].setRotationPoint(35F, -18.5F, -3.5F);

		gunModel[16].addBox(0F, 0F, 0F, 28, 2, 7, 0F); // railBase
		gunModel[16].setRotationPoint(14F, -22F, -3.5F);

		gunModel[17].addShapeBox(0F, 0F, 0F, 22, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // mainBarrelBottom
		gunModel[17].setRotationPoint(60F, -9F, -3F);

		gunModel[18].addBox(0F, 0F, 0F, 22, 2, 6, 0F); // mainBarrelMiddle
		gunModel[18].setRotationPoint(60F, -11F, -3F);

		gunModel[19].addShapeBox(0F, 0F, 0F, 22, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // mainBarrelTop
		gunModel[19].setRotationPoint(60F, -13F, -3F);

		gunModel[20].addShapeBox(0F, 0F, 0F, 11, 2, 10, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F); // grip4
		gunModel[20].setRotationPoint(-12F, 12F, -5F);

		gunModel[21].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // rail1
		gunModel[21].setRotationPoint(14F, -24F, -3.5F);

		gunModel[22].addBox(0F, 0F, 0F, 2, 5, 9, 0F); // clipPart
		gunModel[22].setRotationPoint(13F, -5F, -4.5F);

		gunModel[23].addShapeBox(0F, 0F, 0F, 25, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // handGuard3
		gunModel[23].setRotationPoint(35F, -6.5F, -3.5F);

		gunModel[24].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // rail2
		gunModel[24].setRotationPoint(20F, -24F, -3.5F);

		gunModel[25].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // rail3
		gunModel[25].setRotationPoint(26F, -24F, -3.5F);

		gunModel[26].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // rail4
		gunModel[26].setRotationPoint(32F, -24F, -3.5F);

		gunModel[27].addShapeBox(0F, 0F, 0F, 17, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // handGuard1
		gunModel[27].setRotationPoint(35F, -20.5F, -3.5F);

		gunModel[28].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 21
		gunModel[28].setRotationPoint(76F, -13.5F, -3.5F);

		gunModel[29].addBox(0F, 0F, 0F, 3, 3, 7, 0F); // Box 22
		gunModel[29].setRotationPoint(76F, -11.5F, -3.5F);

		gunModel[30].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 23
		gunModel[30].setRotationPoint(76F, -8.5F, -3.5F);

		gunModel[31].addBox(0F, 0F, 0F, 6, 3, 8, 0F); // ironsightRearBase
		gunModel[31].setRotationPoint(36F, -24F, -4F);

		gunModel[32].addBox(0F, 0F, 0F, 6, 1, 3, 0F); // Box 25
		gunModel[32].setRotationPoint(36F, -25F, -4F);

		gunModel[33].addBox(0F, 0F, 0F, 6, 1, 3, 0F); // Box 26
		gunModel[33].setRotationPoint(36F, -25F, 1F);

		gunModel[34].addShapeBox(0F, 0F, 0F, 10, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // gasBlock1
		gunModel[34].setRotationPoint(52F, -20F, -3F);

		gunModel[35].addBox(0F, 0F, 0F, 2, 2, 6, 0F); // gasBlock2
		gunModel[35].setRotationPoint(60F, -18F, -3F);

		gunModel[36].addShapeBox(0F, 0F, 0F, 2, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // gasBlock2-2
		gunModel[36].setRotationPoint(60F, -16F, -3F);

		gunModel[37].addBox(0F, 0F, 0F, 3, 5, 1, 0F); // ironSight1
		gunModel[37].setRotationPoint(62F, -26F, -0.5F);

		gunModel[38].addShapeBox(0F, 0F, 0F, 3, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // ironSight2
		gunModel[38].setRotationPoint(62F, -21F, -0.5F);

		gunModel[39].addBox(0F, 0F, 0F, 3, 7, 2, 0F); // ironSight3
		gunModel[39].setRotationPoint(62F, -20F, -1F);


		defaultStockModel = new ModelRendererTurbo[19];
		defaultStockModel[0] = new ModelRendererTurbo(this, 72, 167, textureX, textureY); // Box 0
		defaultStockModel[1] = new ModelRendererTurbo(this, 122, 169, textureX, textureY); // Box 52
		defaultStockModel[2] = new ModelRendererTurbo(this, 206, 154, textureX, textureY); // Box 53
		defaultStockModel[3] = new ModelRendererTurbo(this, 206, 170, textureX, textureY); // Box 54
		defaultStockModel[4] = new ModelRendererTurbo(this, 122, 194, textureX, textureY); // Box 55
		defaultStockModel[5] = new ModelRendererTurbo(this, 206, 180, textureX, textureY); // Box 56
		defaultStockModel[6] = new ModelRendererTurbo(this, 122, 184, textureX, textureY); // Box 57
		defaultStockModel[7] = new ModelRendererTurbo(this, 95, 184, textureX, textureY); // Box 59
		defaultStockModel[8] = new ModelRendererTurbo(this, 95, 200, textureX, textureY); // Box 60
		defaultStockModel[9] = new ModelRendererTurbo(this, 72, 205, textureX, textureY); // Box 61
		defaultStockModel[10] = new ModelRendererTurbo(this, 72, 151, textureX, textureY); // Box 62
		defaultStockModel[11] = new ModelRendererTurbo(this, 139, 167, textureX, textureY); // Box 64
		defaultStockModel[12] = new ModelRendererTurbo(this, 72, 195, textureX, textureY); // Box 65
		defaultStockModel[13] = new ModelRendererTurbo(this, 139, 154, textureX, textureY); // Box 66
		defaultStockModel[14] = new ModelRendererTurbo(this, 139, 177, textureX, textureY); // Box 67
		defaultStockModel[15] = new ModelRendererTurbo(this, 139, 194, textureX, textureY); // Box 68
		defaultStockModel[16] = new ModelRendererTurbo(this, 95, 210, textureX, textureY); // Box 0
		defaultStockModel[17] = new ModelRendererTurbo(this, 95, 167, textureX, textureY); // Box 1
		defaultStockModel[18] = new ModelRendererTurbo(this, 206, 191, textureX, textureY); // Box 2

		defaultStockModel[0].addBox(0F, 0F, 0F, 3, 19, 8, 0F); // Box 0
		defaultStockModel[0].setRotationPoint(-52F, -14F, -4F);

		defaultStockModel[1].addBox(0F, 0F, 0F, 1, 7, 7, 0F); // Box 52
		defaultStockModel[1].setRotationPoint(-11F, -14F, -3.5F);

		defaultStockModel[2].addShapeBox(0F, 0F, 0F, 10, 7, 8, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, 4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F); // Box 53
		defaultStockModel[2].setRotationPoint(-21F, -14F, -4F);

		defaultStockModel[3].addShapeBox(0F, 0F, 0F, 10, 1, 8, 0F, 0F, -4F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, -4F, -1F, 0F, 4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F); // Box 54
		defaultStockModel[3].setRotationPoint(-21F, -15F, -4F);

		defaultStockModel[4].addShapeBox(0F, 0F, 0F, 1, 1, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 55
		defaultStockModel[4].setRotationPoint(-11F, -15F, -3.5F);

		defaultStockModel[5].addShapeBox(0F, 0F, 0F, 10, 2, 8, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -4F, -1F, 0F, -4F, -1F, 0F, 0F, -1F); // Box 56
		defaultStockModel[5].setRotationPoint(-21F, -3F, -4F);

		defaultStockModel[6].addShapeBox(0F, 0F, 0F, 1, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 57
		defaultStockModel[6].setRotationPoint(-11F, -7F, -3.5F);

		defaultStockModel[7].addShapeBox(0F, 0F, 0F, 3, 7, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F); // Box 59
		defaultStockModel[7].setRotationPoint(-24F, -10F, -4F);

		defaultStockModel[8].addShapeBox(0F, 0F, 0F, 3, 1, 8, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 60
		defaultStockModel[8].setRotationPoint(-24F, -11F, -4F);

		defaultStockModel[9].addShapeBox(0F, 0F, 0F, 3, 1, 8, 0F, 0F, 0F, -1F, 0F, -4F, -1F, 0F, -4F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, 0F, 0F); // Box 61
		defaultStockModel[9].setRotationPoint(-27F, -15F, -4F);

		defaultStockModel[10].addShapeBox(0F, 0F, 0F, 25, 7, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 62
		defaultStockModel[10].setRotationPoint(-49F, -10F, -4F);

		defaultStockModel[11].addShapeBox(0F, 0F, 0F, 22, 1, 8, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 64
		defaultStockModel[11].setRotationPoint(-49F, -15F, -4F);

		defaultStockModel[12].addShapeBox(0F, 0F, 0F, 3, 1, 8, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 65
		defaultStockModel[12].setRotationPoint(-52F, -15F, -4F);

		defaultStockModel[13].addShapeBox(0F, 0F, 0F, 25, 4, 8, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 66
		defaultStockModel[13].setRotationPoint(-49F, -14F, -4F);

		defaultStockModel[14].addShapeBox(0F, 0F, 0F, 20, 8, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -7F, 0F, 0F, -7F, 0F, 0F, 0F, 0F); // Box 67
		defaultStockModel[14].setRotationPoint(-44F, -3F, -4F);

		defaultStockModel[15].addShapeBox(0F, 0F, 0F, 20, 2, 8, 0F, 0F, 0F, 0F, 0F, 7F, 0F, 0F, 7F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -7F, -1F, 0F, -7F, -1F, 0F, 0F, -1F); // Box 68
		defaultStockModel[15].setRotationPoint(-44F, 5F, -4F);

		defaultStockModel[16].addShapeBox(0F, 0F, 0F, 3, 2, 8, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, 0F, -1F); // Box 0
		defaultStockModel[16].setRotationPoint(-24F, -2F, -4F);

		defaultStockModel[17].addBox(0F, 0F, 0F, 5, 8, 8, 0F); // Box 1
		defaultStockModel[17].setRotationPoint(-49F, -3F, -4F);

		defaultStockModel[18].addShapeBox(0F, 0F, 0F, 8, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 2
		defaultStockModel[18].setRotationPoint(-52F, 5F, -4F);


		ammoModel = new ModelRendererTurbo[3];
		ammoModel[0] = new ModelRendererTurbo(this, 1, 134, textureX, textureY); // bullet
		ammoModel[1] = new ModelRendererTurbo(this, 1, 122, textureX, textureY); // clip1
		ammoModel[2] = new ModelRendererTurbo(this, 1, 88, textureX, textureY); // clip2

		ammoModel[0].addShapeBox(0F, 0F, 0F, 12, 1, 6, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // bullet
		ammoModel[0].setRotationPoint(16F, -6F, -3F);

		ammoModel[1].addBox(0F, 0F, 0F, 14, 3, 8, 0F); // clip1
		ammoModel[1].setRotationPoint(15F, -5F, -4F);

		ammoModel[2].addShapeBox(0F, 0F, 0F, 14, 25, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 3F, -3F, 0F, 3F, -3F, 0F, -3F, 0F, 0F); // clip2
		ammoModel[2].setRotationPoint(15F, -2F, -4F);


		slideModel = new ModelRendererTurbo[2];
		slideModel[0] = new ModelRendererTurbo(this, 48, 10, textureX, textureY); // bolt1
		slideModel[1] = new ModelRendererTurbo(this, 48, 5, textureX, textureY); // bolt2

		slideModel[0].addBox(0F, 0F, 0F, 1, 3, 3, 0F); // bolt1
		slideModel[0].setRotationPoint(28F, -17F, -7F);

		slideModel[1].addBox(0F, 0F, 0F, 3, 3, 1, 0F); // bolt2
		slideModel[1].setRotationPoint(25F, -17F, -4.5F);

		barrelAttachPoint = new Vector3f(82F /16F, 10F /16F, 0F /16F);
		stockAttachPoint = new Vector3f(-10F /16F, 10F /16F, 0F /16F);
		scopeAttachPoint = new Vector3f(24F /16F, 22F /16F, 0F /16F);

		gunSlideDistance = 1F;
		animationType = EnumAnimationType.BOTTOM_CLIP;


		translateAll(0F, 0F, 0F);


		flipAll();
	}
}