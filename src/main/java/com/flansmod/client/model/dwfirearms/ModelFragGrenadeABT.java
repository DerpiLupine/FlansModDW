package com.flansmod.client.model.dwfirearms;

import net.minecraft.client.model.ModelBase;
import net.minecraft.entity.Entity;

import com.flansmod.client.tmt.ModelRendererTurbo;

public class ModelFragGrenadeABT extends ModelBase 
{
	public ModelRendererTurbo[] grenadeModel;

	int textureX = 64;
	int textureY = 32;

	public ModelFragGrenadeABT()
	{
		grenadeModel = new ModelRendererTurbo[15];
		grenadeModel[0] = new ModelRendererTurbo(this, 34, 11, textureX, textureY); // framework
		grenadeModel[1] = new ModelRendererTurbo(this, 20, 1, textureX, textureY); // Box 1
		grenadeModel[2] = new ModelRendererTurbo(this, 20, 17, textureX, textureY); // Box 2
		grenadeModel[3] = new ModelRendererTurbo(this, 20, 9, textureX, textureY); // Box 3
		grenadeModel[4] = new ModelRendererTurbo(this, 29, 1, textureX, textureY); // Box 2
		grenadeModel[5] = new ModelRendererTurbo(this, 29, 1, textureX, textureY); // Box 3
		grenadeModel[6] = new ModelRendererTurbo(this, 29, 1, textureX, textureY); // Box 4
		grenadeModel[7] = new ModelRendererTurbo(this, 29, 1, textureX, textureY); // Box 5
		grenadeModel[8] = new ModelRendererTurbo(this, 29, 1, textureX, textureY); // Box 6
		grenadeModel[9] = new ModelRendererTurbo(this, 29, 1, textureX, textureY); // Box 7
		grenadeModel[10] = new ModelRendererTurbo(this, 29, 4, textureX, textureY); // Box 1
		grenadeModel[11] = new ModelRendererTurbo(this, 29, 4, textureX, textureY); // Box 2
		grenadeModel[12] = new ModelRendererTurbo(this, 29, 4, textureX, textureY); // Box 3
		grenadeModel[13] = new ModelRendererTurbo(this, 29, 7, textureX, textureY); // Box 4
		grenadeModel[14] = new ModelRendererTurbo(this, 29, 11, textureX, textureY); // Box 5

		grenadeModel[0].addBox(-0.5F, -3F, -0.5F, 1, 2, 1, 0F); // framework
		grenadeModel[0].setRotationPoint(0F, 0F, 0F);

		grenadeModel[1].addShapeBox(-1.5F, -1F, -1.5F, 3, 6, 1, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 1
		grenadeModel[1].setRotationPoint(0F, 0F, 0F);

		grenadeModel[2].addShapeBox(-1.5F, -1F, 0.5F, 3, 6, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 2
		grenadeModel[2].setRotationPoint(0F, 0F, 0F);

		grenadeModel[3].addBox(-1.5F, -1F, -0.5F, 3, 6, 1, 0F); // Box 3
		grenadeModel[3].setRotationPoint(0F, 0F, 0F);

		grenadeModel[4].addShapeBox(-1.5F, -1.7F, -0.5F, 3, 1, 1, 0F, 0.25F, -0.5F, 0F, 0.25F, -0.5F, 0F, 0.25F, -0.5F, 0F, 0.25F, -0.5F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0F); // Box 2
		grenadeModel[4].setRotationPoint(0F, 0F, 0F);

		grenadeModel[5].addShapeBox(-1.5F, -1.7F, 0.5F, 3, 1, 1, 0F, 0.25F, -0.5F, 0F, 0.25F, -0.5F, 0F, -0.8F, -0.5F, 0.25F, -0.8F, -0.5F, 0.25F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, -0.8F, 0F, 0.25F, -0.8F, 0F, 0.25F); // Box 3
		grenadeModel[5].setRotationPoint(0F, 0F, 0F);

		grenadeModel[6].addShapeBox(-1.5F, -1.7F, -1.5F, 3, 1, 1, 0F, -0.8F, -0.5F, 0.25F, -0.8F, -0.5F, 0.25F, 0.25F, -0.5F, 0F, 0.25F, -0.5F, 0F, -0.8F, 0F, 0.25F, -0.8F, 0F, 0.25F, 0.25F, 0F, 0F, 0.25F, 0F, 0F); // Box 4
		grenadeModel[6].setRotationPoint(0F, 0F, 0F);

		grenadeModel[7].addShapeBox(-1.5F, 4.2F, 0.5F, 3, 1, 1, 0F, 0.25F, -0.5F, 0F, 0.25F, -0.5F, 0F, -0.8F, -0.5F, 0.25F, -0.8F, -0.5F, 0.25F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, -0.8F, 0F, 0.25F, -0.8F, 0F, 0.25F); // Box 5
		grenadeModel[7].setRotationPoint(0F, 0F, 0F);

		grenadeModel[8].addShapeBox(-1.5F, 4.2F, -0.5F, 3, 1, 1, 0F, 0.25F, -0.5F, 0F, 0.25F, -0.5F, 0F, 0.25F, -0.5F, 0F, 0.25F, -0.5F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0F); // Box 6
		grenadeModel[8].setRotationPoint(0F, 0F, 0F);

		grenadeModel[9].addShapeBox(-1.5F, 4.2F, -1.5F, 3, 1, 1, 0F, -0.8F, -0.5F, 0.25F, -0.8F, -0.5F, 0.25F, 0.25F, -0.5F, 0F, 0.25F, -0.5F, 0F, -0.8F, 0F, 0.25F, -0.8F, 0F, 0.25F, 0.25F, 0F, 0F, 0.25F, 0F, 0F); // Box 7
		grenadeModel[9].setRotationPoint(0F, 0F, 0F);

		grenadeModel[10].addShapeBox(-1.5F, -0.7F, 0.5F, 3, 1, 1, 0F, 0.25F, -0.5F, 0F, 0.25F, -0.5F, 0F, -0.8F, -0.5F, 0.25F, -0.8F, -0.5F, 0.25F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, -0.8F, 0F, 0.25F, -0.8F, 0F, 0.25F); // Box 1
		grenadeModel[10].setRotationPoint(0F, 0F, 0F);

		grenadeModel[11].addShapeBox(-1.5F, -0.7F, -0.5F, 3, 1, 1, 0F, 0.25F, -0.5F, 0F, 0.25F, -0.5F, 0F, 0.25F, -0.5F, 0F, 0.25F, -0.5F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0F); // Box 2
		grenadeModel[11].setRotationPoint(0F, 0F, 0F);

		grenadeModel[12].addShapeBox(-1.5F, -0.7F, -1.5F, 3, 1, 1, 0F, -0.8F, -0.5F, 0.25F, -0.8F, -0.5F, 0.25F, 0.25F, -0.5F, 0F, 0.25F, -0.5F, 0F, -0.8F, 0F, 0.25F, -0.8F, 0F, 0.25F, 0.25F, 0F, 0F, 0.25F, 0F, 0F); // Box 3
		grenadeModel[12].setRotationPoint(0F, 0F, 0F);

		grenadeModel[13].addShapeBox(-0.5F, -2.5F, 0.5F, 1, 1, 2, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 4
		grenadeModel[13].setRotationPoint(0F, 0F, 0F);

		grenadeModel[14].addShapeBox(-0.5F, -2F, 1.5F, 1, 4, 1, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 5
		grenadeModel[14].setRotationPoint(0F, 0F, 0F);

		for(int i = 0; i < 15; i++)
			grenadeModel[i].rotateAngleZ = 3.14159265F;
	}

	@Override
	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
	{
		for(ModelRendererTurbo mineModelBit : grenadeModel)
			mineModelBit.render(f5);
	}
}