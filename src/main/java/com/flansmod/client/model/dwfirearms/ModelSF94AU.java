package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelSF94AU extends ModelGun
{
	int textureX = 512;
	int textureY = 256;

	public ModelSF94AU()
	{
		gunModel = new ModelRendererTurbo[38];
		gunModel[0] = new ModelRendererTurbo(this, 72, 32, textureX, textureY); // body1
		gunModel[1] = new ModelRendererTurbo(this, 72, 53, textureX, textureY); // body10
		gunModel[2] = new ModelRendererTurbo(this, 99, 143, textureX, textureY); // body11
		gunModel[3] = new ModelRendererTurbo(this, 99, 149, textureX, textureY); // body12
		gunModel[4] = new ModelRendererTurbo(this, 72, 88, textureX, textureY); // body2
		gunModel[5] = new ModelRendererTurbo(this, 72, 103, textureX, textureY); // body3
		gunModel[6] = new ModelRendererTurbo(this, 72, 118, textureX, textureY); // body4
		gunModel[7] = new ModelRendererTurbo(this, 72, 129, textureX, textureY); // body5
		gunModel[8] = new ModelRendererTurbo(this, 72, 141, textureX, textureY); // body6
		gunModel[9] = new ModelRendererTurbo(this, 104, 126, textureX, textureY); // body7
		gunModel[10] = new ModelRendererTurbo(this, 72, 75, textureX, textureY); // body8
		gunModel[11] = new ModelRendererTurbo(this, 72, 64, textureX, textureY); // body9
		gunModel[12] = new ModelRendererTurbo(this, 126, 144, textureX, textureY); // Box 5
		gunModel[13] = new ModelRendererTurbo(this, 126, 144, textureX, textureY); // Box 71
		gunModel[14] = new ModelRendererTurbo(this, 126, 144, textureX, textureY); // Box 72
		gunModel[15] = new ModelRendererTurbo(this, 126, 144, textureX, textureY); // Box 73
		gunModel[16] = new ModelRendererTurbo(this, 72, 88, textureX, textureY); // clipPart
		gunModel[17] = new ModelRendererTurbo(this, 115, 2, textureX, textureY); // gasBlock1
		gunModel[18] = new ModelRendererTurbo(this, 115, 2, textureX, textureY); // gasBlock1-2
		gunModel[19] = new ModelRendererTurbo(this, 115, 2, textureX, textureY); // gasBlock1-3
		gunModel[20] = new ModelRendererTurbo(this, 142, 27, textureX, textureY); // gasBlockConnector
		gunModel[21] = new ModelRendererTurbo(this, 115, 11, textureX, textureY); // gasBlockConnector2
		gunModel[22] = new ModelRendererTurbo(this, 115, 21, textureX, textureY); // gasBlockConnector2-2
		gunModel[23] = new ModelRendererTurbo(this, 115, 11, textureX, textureY); // gasBlockConnector2-3
		gunModel[24] = new ModelRendererTurbo(this, 1, 63, textureX, textureY); // grip1
		gunModel[25] = new ModelRendererTurbo(this, 1, 35, textureX, textureY); // grip2
		gunModel[26] = new ModelRendererTurbo(this, 1, 77, textureX, textureY); // grip3
		gunModel[27] = new ModelRendererTurbo(this, 1, 77, textureX, textureY); // grip4
		gunModel[28] = new ModelRendererTurbo(this, 144, 127, textureX, textureY); // handGuard1
		gunModel[29] = new ModelRendererTurbo(this, 183, 131, textureX, textureY); // handGuard2
		gunModel[30] = new ModelRendererTurbo(this, 144, 88, textureX, textureY); // handGuard3
		gunModel[31] = new ModelRendererTurbo(this, 144, 117, textureX, textureY); // handGuard4
		gunModel[32] = new ModelRendererTurbo(this, 144, 106, textureX, textureY); // handGuard5
		gunModel[33] = new ModelRendererTurbo(this, 144, 106, textureX, textureY); // handGuard5-2
		gunModel[34] = new ModelRendererTurbo(this, 72, 20, textureX, textureY); // mainBarrelBottom
		gunModel[35] = new ModelRendererTurbo(this, 72, 11, textureX, textureY); // mainBarrelMiddle
		gunModel[36] = new ModelRendererTurbo(this, 72, 2, textureX, textureY); // mainBarrelTop
		gunModel[37] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // scopeBase

		gunModel[0].addBox(0F, 0F, 0F, 22, 10, 10, 0F); // body1
		gunModel[0].setRotationPoint(-10F, -16F, -5F);

		gunModel[1].addShapeBox(0F, 0F, 0F, 34, 2, 8, 0F, -1F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, -1F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body10
		gunModel[1].setRotationPoint(-8F, -22F, -4F);

		gunModel[2].addShapeBox(0F, 0F, 0F, 3, 2, 3, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body11
		gunModel[2].setRotationPoint(-9F, -21F, -1.5F);

		gunModel[3].addBox(0F, 0F, 0F, 12, 3, 1, 0F); // body12
		gunModel[3].setRotationPoint(8F, -15F, 4.5F);

		gunModel[4].addBox(0F, 0F, 0F, 17, 5, 9, 0F); // body2
		gunModel[4].setRotationPoint(12F, -11F, -4.5F);

		gunModel[5].addBox(0F, 0F, 0F, 17, 4, 10, 0F); // body3
		gunModel[5].setRotationPoint(12F, -15F, -5F);

		gunModel[6].addBox(0F, 0F, 0F, 11, 1, 9, 0F); // body4
		gunModel[6].setRotationPoint(12F, -16F, -4F);

		gunModel[7].addShapeBox(0F, 0F, 0F, 11, 2, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body5
		gunModel[7].setRotationPoint(12F, -18F, -4F);

		gunModel[8].addShapeBox(0F, 0F, 0F, 3, 2, 10, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body6
		gunModel[8].setRotationPoint(23F, -18F, -5F);

		gunModel[9].addBox(0F, 0F, 0F, 6, 1, 10, 0F); // body7
		gunModel[9].setRotationPoint(23F, -16F, -5F);

		gunModel[10].addShapeBox(0F, 0F, 0F, 22, 2, 10, 0F, -1F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, -1F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body8
		gunModel[10].setRotationPoint(-10F, -18F, -5F);

		gunModel[11].addShapeBox(0F, 0F, 0F, 35, 2, 8, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body9
		gunModel[11].setRotationPoint(-9F, -20F, -4F);

		gunModel[12].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 5
		gunModel[12].setRotationPoint(47F, -6F, -3.5F);

		gunModel[13].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 71
		gunModel[13].setRotationPoint(29F, -6F, -3.5F);

		gunModel[14].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 72
		gunModel[14].setRotationPoint(35F, -6F, -3.5F);

		gunModel[15].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 73
		gunModel[15].setRotationPoint(41F, -6F, -3.5F);

		gunModel[16].addBox(0F, 0F, 0F, 3, 5, 9, 0F); // clipPart
		gunModel[16].setRotationPoint(11F, -6F, -4.5F);

		gunModel[17].addShapeBox(0F, 0F, 0F, 6, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // gasBlock1
		gunModel[17].setRotationPoint(50F, -21F, -3F);

		gunModel[18].addBox(0F, 0F, 0F, 6, 2, 6, 0F); // gasBlock1-2
		gunModel[18].setRotationPoint(50F, -19F, -3F);

		gunModel[19].addShapeBox(0F, 0F, 0F, 6, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // gasBlock1-3
		gunModel[19].setRotationPoint(50F, -17F, -3F);

		gunModel[20].addBox(0F, 0F, 0F, 4, 1, 3, 0F); // gasBlockConnector
		gunModel[20].setRotationPoint(52F, -15F, -1.5F);

		gunModel[21].addShapeBox(0F, 0F, 0F, 6, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // gasBlockConnector2
		gunModel[21].setRotationPoint(52F, -14F, -3.5F);

		gunModel[22].addBox(0F, 0F, 0F, 6, 3, 7, 0F); // gasBlockConnector2-2
		gunModel[22].setRotationPoint(52F, -12F, -3.5F);

		gunModel[23].addShapeBox(0F, 0F, 0F, 6, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // gasBlockConnector2-3
		gunModel[23].setRotationPoint(52F, -9F, -3.5F);

		gunModel[24].addShapeBox(0F, 0F, 0F, 12, 3, 10, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F); // grip1
		gunModel[24].setRotationPoint(-9F, -6F, -5F);

		gunModel[25].addShapeBox(0F, 0F, 0F, 11, 12, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 4F, 0F, 0F); // grip2
		gunModel[25].setRotationPoint(-8F, -3F, -5F);

		gunModel[26].addShapeBox(0F, 0F, 0F, 11, 2, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F); // grip3
		gunModel[26].setRotationPoint(-12F, 9F, -5F);

		gunModel[27].addShapeBox(0F, 0F, 0F, 11, 2, 10, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F); // grip4
		gunModel[27].setRotationPoint(-12F, 11F, -5F);

		gunModel[28].addBox(0F, 0F, 0F, 12, 5, 7, 0F); // handGuard1
		gunModel[28].setRotationPoint(38F, -21F, -3.5F);

		gunModel[29].addShapeBox(0F, 0F, 0F, 12, 1, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // handGuard2
		gunModel[29].setRotationPoint(38F, -22F, -3.5F);

		gunModel[30].addBox(0F, 0F, 0F, 21, 9, 8, 0F); // handGuard3
		gunModel[30].setRotationPoint(29F, -16F, -4F);

		gunModel[31].addShapeBox(0F, 0F, 0F, 21, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // handGuard4
		gunModel[31].setRotationPoint(29F, -7F, -4F);

		gunModel[32].addBox(0F, 0F, 0F, 19, 1, 9, 0F); // handGuard5
		gunModel[32].setRotationPoint(30F, -10.5F, -4.5F);

		gunModel[33].addBox(0F, 0F, 0F, 19, 1, 9, 0F); // handGuard5-2
		gunModel[33].setRotationPoint(30F, -13.5F, -4.5F);

		gunModel[34].addShapeBox(0F, 0F, 0F, 15, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // mainBarrelBottom
		gunModel[34].setRotationPoint(50F, -9.5F, -3F);

		gunModel[35].addBox(0F, 0F, 0F, 15, 2, 6, 0F); // mainBarrelMiddle
		gunModel[35].setRotationPoint(50F, -11.5F, -3F);

		gunModel[36].addShapeBox(0F, 0F, 0F, 15, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // mainBarrelTop
		gunModel[36].setRotationPoint(50F, -13.5F, -3F);

		gunModel[37].addBox(0F, 0F, 0F, 12, 5, 9, 0F); // scopeBase
		gunModel[37].setRotationPoint(26F, -21F, -4.5F);


		defaultScopeModel = new ModelRendererTurbo[7];
		defaultScopeModel[0] = new ModelRendererTurbo(this, 44, 1, textureX, textureY); // ironSight1
		defaultScopeModel[1] = new ModelRendererTurbo(this, 44, 1, textureX, textureY); // ironSight1-2
		defaultScopeModel[2] = new ModelRendererTurbo(this, 53, 11, textureX, textureY); // ironSight2
		defaultScopeModel[3] = new ModelRendererTurbo(this, 53, 1, textureX, textureY); // ironSight3
		defaultScopeModel[4] = new ModelRendererTurbo(this, 1, 28, textureX, textureY); // ironSight4
		defaultScopeModel[5] = new ModelRendererTurbo(this, 1, 16, textureX, textureY); // ironSightBase
		defaultScopeModel[6] = new ModelRendererTurbo(this, 53, 1, textureX, textureY); // ironSightBase1-2

		defaultScopeModel[0].addBox(0F, 0F, 0F, 3, 10, 1, 0F); // ironSight1
		defaultScopeModel[0].setRotationPoint(52F, -29F, 2F);

		defaultScopeModel[1].addBox(0F, 0F, 0F, 3, 10, 1, 0F); // ironSight1-2
		defaultScopeModel[1].setRotationPoint(52F, -29F, -3F);

		defaultScopeModel[2].addBox(0F, 0F, 0F, 2, 2, 1, 0F); // ironSight2
		defaultScopeModel[2].setRotationPoint(35F, -26F, -0.5F);

		defaultScopeModel[3].addBox(0F, 0F, 0F, 3, 5, 4, 0F); // ironSight3
		defaultScopeModel[3].setRotationPoint(52F, -25F, -2F);

		defaultScopeModel[4].addBox(0F, 0F, 0F, 9, 1, 5, 0F); // ironSight4
		defaultScopeModel[4].setRotationPoint(26F, -24F, -2.5F);

		defaultScopeModel[5].addShapeBox(0F, 0F, 0F, 12, 2, 9, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // ironSightBase
		defaultScopeModel[5].setRotationPoint(26F, -23F, -4.5F);

		defaultScopeModel[6].addBox(0F, 0F, 0F, 3, 2, 4, 0F); // ironSightBase1-2
		defaultScopeModel[6].setRotationPoint(35F, -24.5F, -2F);


		defaultStockModel = new ModelRendererTurbo[4];
		defaultStockModel[0] = new ModelRendererTurbo(this, 72, 154, textureX, textureY); // stock1
		defaultStockModel[1] = new ModelRendererTurbo(this, 72, 184, textureX, textureY); // stock2
		defaultStockModel[2] = new ModelRendererTurbo(this, 147, 158, textureX, textureY); // stock3
		defaultStockModel[3] = new ModelRendererTurbo(this, 147, 184, textureX, textureY); // stock4

		defaultStockModel[0].addShapeBox(0F, 0F, 0F, 28, 20, 9, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, -12F, 0F, 0F, -12F, 0F, 0F, 0F, 0F); // stock1
		defaultStockModel[0].setRotationPoint(-38F, -14F, -4.5F);

		defaultStockModel[1].addShapeBox(0F, 0F, 0F, 28, 2, 9, 0F, 0F, 0F, -2F, 0F, 4F, -2F, 0F, 4F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 0F, 0F); // stock2
		defaultStockModel[1].setRotationPoint(-38F, -12F, -4.5F);

		defaultStockModel[2].addBox(0F, 0F, 0F, 2, 16, 9, 0F); // stock3
		defaultStockModel[2].setRotationPoint(-40F, -10F, -4.5F);

		defaultStockModel[3].addShapeBox(0F, 0F, 0F, 2, 2, 9, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // stock4
		defaultStockModel[3].setRotationPoint(-40F, -12F, -4.5F);


		ammoModel = new ModelRendererTurbo[10];
		ammoModel[0] = new ModelRendererTurbo(this, 12, 165, textureX, textureY); // bullet
		ammoModel[1] = new ModelRendererTurbo(this, 1, 165, textureX, textureY); // bulletTip
		ammoModel[2] = new ModelRendererTurbo(this, 1, 152, textureX, textureY); // clip1
		ammoModel[3] = new ModelRendererTurbo(this, 1, 134, textureX, textureY); // clip2
		ammoModel[4] = new ModelRendererTurbo(this, 1, 113, textureX, textureY); // clip3
		ammoModel[5] = new ModelRendererTurbo(this, 1, 90, textureX, textureY); // clip4
		ammoModel[6] = new ModelRendererTurbo(this, 30, 153, textureX, textureY); // clipSmall5
		ammoModel[7] = new ModelRendererTurbo(this, 30, 135, textureX, textureY); // clipSmall6
		ammoModel[8] = new ModelRendererTurbo(this, 30, 117, textureX, textureY); // clipSmall7
		ammoModel[9] = new ModelRendererTurbo(this, 30, 92, textureX, textureY); // clipSmall8

		ammoModel[0].addShapeBox(0F, 0F, 0F, 6, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // bullet
		ammoModel[0].setRotationPoint(15F, -7F, -1.5F);

		ammoModel[1].addShapeBox(0F, 0F, 0F, 2, 1, 3, 0F, 0F, 0F, -1F, 0F, -0.5F, -1F, 0F, -0.5F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F); // bulletTip
		ammoModel[1].setRotationPoint(21F, -7F, -1.5F);

		ammoModel[2].addBox(0F, 0F, 0F, 8, 6, 6, 0F); // clip1
		ammoModel[2].setRotationPoint(14F, -6F, -3F);

		ammoModel[3].addBox(0F, 0F, 0F, 8, 9, 6, 0F); // clip2
		ammoModel[3].setRotationPoint(14F, -1F, -3F);
		ammoModel[3].rotateAngleZ = 0.10471976F;

		ammoModel[4].addBox(0F, 0F, 0F, 8, 12, 6, 0F); // clip3
		ammoModel[4].setRotationPoint(15F, 8F, -3F);
		ammoModel[4].rotateAngleZ = 0.33161256F;

		ammoModel[5].addBox(0F, 0F, 0F, 8, 14, 6, 0F); // clip4
		ammoModel[5].setRotationPoint(19F, 19.5F, -3F);
		ammoModel[5].rotateAngleZ = 0.66322512F;

		ammoModel[6].addBox(0F, 0F, 0F, 2, 6, 5, 0F); // clipSmall5
		ammoModel[6].setRotationPoint(22F, -6F, -2.5F);

		ammoModel[7].addBox(0F, 0F, 0F, 2, 9, 5, 0F); // clipSmall6
		ammoModel[7].setRotationPoint(22F, -2F, -2.5F);
		ammoModel[7].rotateAngleZ = 0.10471976F;

		ammoModel[8].addBox(0F, 0F, 0F, 2, 9, 5, 0F); // clipSmall7
		ammoModel[8].setRotationPoint(23F, 7F, -2.5F);
		ammoModel[8].rotateAngleZ = 0.33161256F;

		ammoModel[9].addBox(0F, 0F, 0F, 2, 13, 5, 0F); // clipSmall8
		ammoModel[9].setRotationPoint(26F, 15.5F, -2.5F);
		ammoModel[9].rotateAngleZ = 0.66322512F;


		slideModel = new ModelRendererTurbo[2];
		slideModel[0] = new ModelRendererTurbo(this, 44, 18, textureX, textureY); // bolt1
		slideModel[1] = new ModelRendererTurbo(this, 44, 13, textureX, textureY); // bolt2

		slideModel[0].addBox(0F, 0F, 0F, 1, 3, 3, 0F); // bolt1
		slideModel[0].setRotationPoint(22F, -18F, -7F);

		slideModel[1].addBox(0F, 0F, 0F, 3, 3, 1, 0F); // bolt2
		slideModel[1].setRotationPoint(19F, -18F, -4.5F);

		barrelAttachPoint = new Vector3f(65F /16F, 10.5F /16F, 0F /16F);
		stockAttachPoint = new Vector3f(-10F /16F, 11F /16F, 0F /16F);
		scopeAttachPoint = new Vector3f(32F /16F, 21F /16F, 0F /16F);
		gripAttachPoint = new Vector3f(40 /16F, 7F /16F, 0F /16F);

		gunSlideDistance = 0.8F;
		animationType = EnumAnimationType.BOTTOM_CLIP;


		translateAll(0F, 0F, 0F);


		flipAll();
	}
}