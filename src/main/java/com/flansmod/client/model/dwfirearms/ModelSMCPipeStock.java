package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.ModelAttachment;
import com.flansmod.client.tmt.ModelRendererTurbo;

import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelSMCPipeStock extends ModelAttachment
{
	int textureX = 512;
	int textureY = 256;

	public ModelSMCPipeStock()
	{
		attachmentModel = new ModelRendererTurbo[19];
		attachmentModel[0] = new ModelRendererTurbo(this, 38, 146, textureX, textureY); // Box 23
		attachmentModel[1] = new ModelRendererTurbo(this, 38, 157, textureX, textureY); // Box 23
		attachmentModel[2] = new ModelRendererTurbo(this, 38, 157, textureX, textureY); // Box 23
		attachmentModel[3] = new ModelRendererTurbo(this, 1, 146, textureX, textureY); // Box 23
		attachmentModel[4] = new ModelRendererTurbo(this, 1, 155, textureX, textureY); // Box 23
		attachmentModel[5] = new ModelRendererTurbo(this, 1, 146, textureX, textureY); // Box 23
		attachmentModel[6] = new ModelRendererTurbo(this, 73, 157, textureX, textureY); // Box 23
		attachmentModel[7] = new ModelRendererTurbo(this, 73, 146, textureX, textureY); // Box 23
		attachmentModel[8] = new ModelRendererTurbo(this, 73, 157, textureX, textureY); // Box 23
		attachmentModel[9] = new ModelRendererTurbo(this, 1, 119, textureX, textureY); // Box 23
		attachmentModel[10] = new ModelRendererTurbo(this, 1, 136, textureX, textureY); // Box 23
		attachmentModel[11] = new ModelRendererTurbo(this, 65, 109, textureX, textureY); // Box 23
		attachmentModel[12] = new ModelRendererTurbo(this, 38, 134, textureX, textureY); // Box 23
		attachmentModel[13] = new ModelRendererTurbo(this, 1, 106, textureX, textureY); // Box 23
		attachmentModel[14] = new ModelRendererTurbo(this, 65, 122, textureX, textureY); // Box 23
		attachmentModel[15] = new ModelRendererTurbo(this, 38, 113, textureX, textureY); // Box 23
		attachmentModel[16] = new ModelRendererTurbo(this, 38, 103, textureX, textureY); // Box 23
		attachmentModel[17] = new ModelRendererTurbo(this, 18, 164, textureX, textureY); // Box 23
		attachmentModel[18] = new ModelRendererTurbo(this, 1, 164, textureX, textureY); // Box 23

		attachmentModel[0].addBox(-10F, -1.5F, -3.5F, 10, 3, 7, 0F); // Box 23
		attachmentModel[0].setRotationPoint(0F, 0F, 0F);

		attachmentModel[1].addShapeBox(-10F, -3.5F, -3.5F, 10, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 23
		attachmentModel[1].setRotationPoint(0F, 0F, 0F);

		attachmentModel[2].addShapeBox(-10F, 1.5F, -3.5F, 10, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 23
		attachmentModel[2].setRotationPoint(0F, 0F, 0F);

		attachmentModel[3].addShapeBox(-22F, 1F, -3F, 12, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 23
		attachmentModel[3].setRotationPoint(0F, 0F, 0F);

		attachmentModel[4].addBox(-22F, -1F, -3F, 12, 2, 6, 0F); // Box 23
		attachmentModel[4].setRotationPoint(0F, 0F, 0F);

		attachmentModel[5].addShapeBox(-22F, -3F, -3F, 12, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 23
		attachmentModel[5].setRotationPoint(0F, 0F, 0F);

		attachmentModel[6].addShapeBox(-25F, -3.5F, -3.5F, 3, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 23
		attachmentModel[6].setRotationPoint(0F, 0F, 0F);

		attachmentModel[7].addBox(-25F, -1.5F, -3.5F, 3, 3, 7, 0F); // Box 23
		attachmentModel[7].setRotationPoint(0F, 0F, 0F);

		attachmentModel[8].addShapeBox(-25F, 1.5F, -3.5F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 23
		attachmentModel[8].setRotationPoint(0F, 0F, 0F);

		attachmentModel[9].addBox(-35F, -3F, -4F, 10, 6, 8, 0F); // Box 23
		attachmentModel[9].setRotationPoint(0F, 0F, 0F);

		attachmentModel[10].addShapeBox(-35F, -4F, -4F, 10, 1, 8, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 23
		attachmentModel[10].setRotationPoint(0F, 0F, 0F);

		attachmentModel[11].addShapeBox(-26F, 3F, -4F, 1, 4, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F); // Box 23
		attachmentModel[11].setRotationPoint(0F, 0F, 0F);

		attachmentModel[12].addBox(-35F, 7F, -4F, 13, 3, 8, 0F); // Box 23
		attachmentModel[12].setRotationPoint(0F, 0F, 0F);

		attachmentModel[13].addBox(-35F, 3F, -4F, 9, 4, 8, 0F); // Box 23
		attachmentModel[13].setRotationPoint(0F, 0F, 0F);

		attachmentModel[14].addShapeBox(-30F, 10F, -4F, 1, 3, 8, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 23
		attachmentModel[14].setRotationPoint(0F, 0F, 0F);

		attachmentModel[15].addBox(-35F, 10F, -4F, 5, 11, 8, 0F); // Box 23
		attachmentModel[15].setRotationPoint(0F, 0F, 0F);

		attachmentModel[16].addShapeBox(-35F, 21F, -4F, 5, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 23
		attachmentModel[16].setRotationPoint(0F, 0F, 0F);

		attachmentModel[17].addShapeBox(-37F, -3F, -3F, 2, 12, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F); // Box 23
		attachmentModel[17].setRotationPoint(0F, 0F, 0F);

		attachmentModel[18].addShapeBox(-37F, 9F, -3F, 2, 12, 6, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 23
		attachmentModel[18].setRotationPoint(0F, 0F, 0F);

		renderOffset = 0F;

		flipAll();
	}
}