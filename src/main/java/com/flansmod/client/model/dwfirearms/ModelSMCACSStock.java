package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.ModelAttachment;
import com.flansmod.client.tmt.ModelRendererTurbo;

import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelSMCACSStock extends ModelAttachment
{
	int textureX = 512;
	int textureY = 256;

	public ModelSMCACSStock()
	{
		attachmentModel = new ModelRendererTurbo[21];
		attachmentModel[0] = new ModelRendererTurbo(this, 1, 92, textureX, textureY); // Box 30
		attachmentModel[1] = new ModelRendererTurbo(this, 65, 153, textureX, textureY); // Box 30
		attachmentModel[2] = new ModelRendererTurbo(this, 48, 107, textureX, textureY); // Box 30
		attachmentModel[3] = new ModelRendererTurbo(this, 48, 100, textureX, textureY); // Box 30
		attachmentModel[4] = new ModelRendererTurbo(this, 67, 121, textureX, textureY); // Box 30
		attachmentModel[5] = new ModelRendererTurbo(this, 48, 137, textureX, textureY); // Box 30
		attachmentModel[6] = new ModelRendererTurbo(this, 1, 165, textureX, textureY); // Box 30
		attachmentModel[7] = new ModelRendererTurbo(this, 1, 85, textureX, textureY); // Box 30
		attachmentModel[8] = new ModelRendererTurbo(this, 1, 178, textureX, textureY); // Box 30
		attachmentModel[9] = new ModelRendererTurbo(this, 20, 145, textureX, textureY); // Box 30
		attachmentModel[10] = new ModelRendererTurbo(this, 1, 130, textureX, textureY); // Box 30
		attachmentModel[11] = new ModelRendererTurbo(this, 28, 156, textureX, textureY); // Box 30
		attachmentModel[12] = new ModelRendererTurbo(this, 1, 145, textureX, textureY); // Box 30
		attachmentModel[13] = new ModelRendererTurbo(this, 25, 131, textureX, textureY); // Box 30
		attachmentModel[14] = new ModelRendererTurbo(this, 16, 132, textureX, textureY); // Box 30
		attachmentModel[15] = new ModelRendererTurbo(this, 1, 125, textureX, textureY); // Box 30
		attachmentModel[16] = new ModelRendererTurbo(this, 25, 140, textureX, textureY); // Box 30
		attachmentModel[17] = new ModelRendererTurbo(this, 48, 123, textureX, textureY); // Box 30
		attachmentModel[18] = new ModelRendererTurbo(this, 1, 156, textureX, textureY); // Box 30
		attachmentModel[19] = new ModelRendererTurbo(this, 1, 114, textureX, textureY); // Box 30
		attachmentModel[20] = new ModelRendererTurbo(this, 1, 100, textureX, textureY); // Box 30

		attachmentModel[0].addBox(-31F, 3.5F, -2.5F, 25, 2, 5, 0F); // Box 30
		attachmentModel[0].setRotationPoint(0F, 0F, 0F);

		attachmentModel[1].addShapeBox(-35F, -5.5F, -4.5F, 4, 2, 9, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 30
		attachmentModel[1].setRotationPoint(0F, 0F, 0F);

		attachmentModel[2].addBox(-35F, 3.5F, -3.5F, 4, 8, 7, 0F); // Box 30
		attachmentModel[2].setRotationPoint(0F, 0F, 0F);

		attachmentModel[3].addShapeBox(-35F, 19.5F, -2.5F, 12, 1, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 30
		attachmentModel[3].setRotationPoint(0F, 0F, 0F);

		attachmentModel[4].addBox(-35F, -3.5F, -4.5F, 4, 6, 9, 0F); // Box 30
		attachmentModel[4].setRotationPoint(0F, 0F, 0F);

		attachmentModel[5].addShapeBox(-35F, 2.5F, -3.5F, 4, 1, 7, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 30
		attachmentModel[5].setRotationPoint(0F, 0F, 0F);

		attachmentModel[6].addBox(-31F, -2.5F, -3.5F, 31, 5, 7, 0F); // Box 30
		attachmentModel[6].setRotationPoint(0F, 0F, 0F);

		attachmentModel[7].addShapeBox(-31F, 2.5F, -2.5F, 31, 1, 5, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 30
		attachmentModel[7].setRotationPoint(0F, 0F, 0F);

		attachmentModel[8].addShapeBox(-31F, -4.5F, -3.5F, 31, 2, 7, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 30
		attachmentModel[8].setRotationPoint(0F, 0F, 0F);

		attachmentModel[9].addBox(-31F, 14.5F, -2.5F, 8, 5, 5, 0F); // Box 30
		attachmentModel[9].setRotationPoint(0F, 0F, 0F);

		attachmentModel[10].addBox(-31F, 5.5F, -2.5F, 2, 9, 5, 0F); // Box 30
		attachmentModel[10].setRotationPoint(0F, 0F, 0F);

		attachmentModel[11].addShapeBox(-23F, 14.5F, -2.5F, 13, 3, 5, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 0F, 0F); // Box 30
		attachmentModel[11].setRotationPoint(0F, 0F, 0F);

		attachmentModel[12].addBox(-10F, 5.5F, -2.5F, 4, 5, 5, 0F); // Box 30
		attachmentModel[12].setRotationPoint(0F, 0F, 0F);

		attachmentModel[13].addShapeBox(-10F, 10.5F, -2.5F, 4, 3, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F); // Box 30
		attachmentModel[13].setRotationPoint(0F, 0F, 0F);

		attachmentModel[14].addBox(-29F, 5.5F, -1.5F, 1, 9, 3, 0F); // Box 30
		attachmentModel[14].setRotationPoint(0F, 0F, 0F);

		attachmentModel[15].addBox(-28F, 5.5F, -1.5F, 18, 1, 3, 0F); // Box 30
		attachmentModel[15].setRotationPoint(0F, 0F, 0F);

		attachmentModel[16].addBox(-28F, 13.5F, -1.5F, 8, 1, 3, 0F); // Box 30
		attachmentModel[16].setRotationPoint(0F, 0F, 0F);

		attachmentModel[17].addBox(-35F, 11.5F, -2.5F, 4, 8, 5, 0F); // Box 30
		attachmentModel[17].setRotationPoint(0F, 0F, 0F);

		attachmentModel[18].addBox(-6F, 3F, -2F, 6, 3, 4, 0F); // Box 30
		attachmentModel[18].setRotationPoint(0F, 0F, 0F);

		attachmentModel[19].addShapeBox(-23F, -5.5F, -4F, 15, 2, 8, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 30
		attachmentModel[19].setRotationPoint(0F, 0F, 0F);

		attachmentModel[20].addBox(-23F, -3.5F, -4F, 15, 5, 8, 0F); // Box 30
		attachmentModel[20].setRotationPoint(0F, 0F, 0F);

		renderOffset = 0F;

		flipAll();
	}
}