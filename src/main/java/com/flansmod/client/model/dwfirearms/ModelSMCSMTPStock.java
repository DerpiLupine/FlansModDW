package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.ModelAttachment;
import com.flansmod.client.tmt.ModelRendererTurbo;

import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelSMCSMTPStock extends ModelAttachment
{
	int textureX = 512;
	int textureY = 256;

	public ModelSMCSMTPStock()
	{
		attachmentModel = new ModelRendererTurbo[28];
		attachmentModel[0] = new ModelRendererTurbo(this, 1, 141, textureX, textureY); // stock
		attachmentModel[1] = new ModelRendererTurbo(this, 1, 124, textureX, textureY); // stock
		attachmentModel[2] = new ModelRendererTurbo(this, 1, 155, textureX, textureY); // stock
		attachmentModel[3] = new ModelRendererTurbo(this, 54, 122, textureX, textureY); // stock
		attachmentModel[4] = new ModelRendererTurbo(this, 54, 141, textureX, textureY); // stock
		attachmentModel[5] = new ModelRendererTurbo(this, 54, 156, textureX, textureY); // stock
		attachmentModel[6] = new ModelRendererTurbo(this, 91, 141, textureX, textureY); // stock
		attachmentModel[7] = new ModelRendererTurbo(this, 69, 171, textureX, textureY); // stock
		attachmentModel[8] = new ModelRendererTurbo(this, 48, 170, textureX, textureY); // stock
		attachmentModel[9] = new ModelRendererTurbo(this, 48, 205, textureX, textureY); // stock
		attachmentModel[10] = new ModelRendererTurbo(this, 1, 176, textureX, textureY); // stock
		attachmentModel[11] = new ModelRendererTurbo(this, 1, 165, textureX, textureY); // stock
		attachmentModel[12] = new ModelRendererTurbo(this, 91, 128, textureX, textureY); // stock
		attachmentModel[13] = new ModelRendererTurbo(this, 1, 165, textureX, textureY); // stock
		attachmentModel[14] = new ModelRendererTurbo(this, 80, 193, textureX, textureY); // stock
		attachmentModel[15] = new ModelRendererTurbo(this, 48, 198, textureX, textureY); // stock
		attachmentModel[16] = new ModelRendererTurbo(this, 48, 187, textureX, textureY); // stock
		attachmentModel[17] = new ModelRendererTurbo(this, 48, 205, textureX, textureY); // stock
		attachmentModel[18] = new ModelRendererTurbo(this, 63, 205, textureX, textureY); // stock
		attachmentModel[19] = new ModelRendererTurbo(this, 63, 205, textureX, textureY); // stock
		attachmentModel[20] = new ModelRendererTurbo(this, 76, 206, textureX, textureY); // stock
		attachmentModel[21] = new ModelRendererTurbo(this, 1, 189, textureX, textureY); // stock
		attachmentModel[22] = new ModelRendererTurbo(this, 1, 189, textureX, textureY); // stock
		attachmentModel[23] = new ModelRendererTurbo(this, 1, 189, textureX, textureY); // stock
		attachmentModel[24] = new ModelRendererTurbo(this, 1, 205, textureX, textureY); // stock
		attachmentModel[25] = new ModelRendererTurbo(this, 1, 198, textureX, textureY); // stock
		attachmentModel[26] = new ModelRendererTurbo(this, 1, 205, textureX, textureY); // stock
		attachmentModel[27] = new ModelRendererTurbo(this, 95, 189, textureX, textureY); // stock

		attachmentModel[0].addShapeBox(-32F, -1F, -5.5F, 15, 2, 11, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // stock
		attachmentModel[0].setRotationPoint(0F, 0F, 0F);

		attachmentModel[1].addShapeBox(-32F, -6F, -5.5F, 15, 5, 11, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // stock
		attachmentModel[1].setRotationPoint(0F, 0F, 0F);

		attachmentModel[2].addShapeBox(-32F, -7F, -4F, 15, 1, 8, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // stock
		attachmentModel[2].setRotationPoint(0F, 0F, 0F);

		attachmentModel[3].addShapeBox(-38F, -6.5F, -6F, 6, 6, 12, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // stock
		attachmentModel[3].setRotationPoint(0F, 0F, 0F);

		attachmentModel[4].addShapeBox(-38F, -0.5F, -6F, 6, 2, 12, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // stock
		attachmentModel[4].setRotationPoint(0F, 0F, 0F);

		attachmentModel[5].addShapeBox(-38F, -7.5F, -4.5F, 6, 1, 9, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // stock
		attachmentModel[5].setRotationPoint(0F, 0F, 0F);

		attachmentModel[6].addBox(-38F, 1.5F, -5F, 3, 16, 10, 0F); // stock
		attachmentModel[6].setRotationPoint(0F, 0F, 0F);

		attachmentModel[7].addShapeBox(-32F, 1F, -4.5F, 1, 6, 9, 0F, 0F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // stock
		attachmentModel[7].setRotationPoint(0F, 0F, 0F);

		attachmentModel[8].addBox(-32F, 7F, -4.5F, 1, 7, 9, 0F); // stock
		attachmentModel[8].setRotationPoint(0F, 0F, 0F);

		attachmentModel[9].addShapeBox(-23F, 6F, -2.5F, 2, 4, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F); // stock
		attachmentModel[9].setRotationPoint(0F, 0F, 0F);

		attachmentModel[10].addShapeBox(-32F, 14F, -4.5F, 1, 3, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F); // stock
		attachmentModel[10].setRotationPoint(0F, 0F, 0F);

		attachmentModel[11].addBox(-35F, 17F, -4.5F, 6, 1, 9, 0F); // stock
		attachmentModel[11].setRotationPoint(0F, 0F, 0F);

		attachmentModel[12].addShapeBox(-38F, 17.5F, -5F, 3, 2, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // stock
		attachmentModel[12].setRotationPoint(0F, 0F, 0F);

		attachmentModel[13].addShapeBox(-35F, 18F, -4.5F, 6, 1, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // stock
		attachmentModel[13].setRotationPoint(0F, 0F, 0F);

		attachmentModel[14].addBox(-31F, 11F, -2.5F, 2, 6, 5, 0F); // stock
		attachmentModel[14].setRotationPoint(0F, 0F, 0F);

		attachmentModel[15].addShapeBox(-31F, 10F, -2.5F, 10, 1, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F); // stock
		attachmentModel[15].setRotationPoint(0F, 0F, 0F);

		attachmentModel[16].addBox(-31F, 1F, -2.5F, 13, 5, 5, 0F); // stock
		attachmentModel[16].setRotationPoint(0F, 0F, 0F);

		attachmentModel[17].addShapeBox(-31F, 6F, -2.5F, 2, 4, 5, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // stock
		attachmentModel[17].setRotationPoint(0F, 0F, 0F);

		attachmentModel[18].addShapeBox(-27F, 6F, -2.5F, 1, 4, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F); // stock
		attachmentModel[18].setRotationPoint(0F, 0F, 0F);

		attachmentModel[19].addShapeBox(-25F, 6F, -2.5F, 1, 4, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F); // stock
		attachmentModel[19].setRotationPoint(0F, 0F, 0F);

		attachmentModel[20].addShapeBox(-28F, 6F, -2F, 5, 4, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F); // stock
		attachmentModel[20].setRotationPoint(0F, 0F, 0F);

		attachmentModel[21].addShapeBox(-17F, -5.5F, -3F, 17, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // stock
		attachmentModel[21].setRotationPoint(0F, 0F, 0F);

		attachmentModel[22].addBox(-17F, -3.5F, -3F, 17, 2, 6, 0F); // stock
		attachmentModel[22].setRotationPoint(0F, 0F, 0F);

		attachmentModel[23].addShapeBox(-17F, -1.5F, -3F, 17, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // stock
		attachmentModel[23].setRotationPoint(0F, 0F, 0F);

		attachmentModel[24].addShapeBox(-18F, 4.5F, -2F, 18, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // stock
		attachmentModel[24].setRotationPoint(0F, 0F, 0F);

		attachmentModel[25].addBox(-18F, 2.5F, -2F, 18, 2, 4, 0F); // stock
		attachmentModel[25].setRotationPoint(0F, 0F, 0F);

		attachmentModel[26].addShapeBox(-18F, 1.5F, -2F, 18, 1, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // stock
		attachmentModel[26].setRotationPoint(0F, 0F, 0F);

		attachmentModel[27].addBox(-35F, 1F, -4.5F, 3, 16, 9, 0F); // stock
		attachmentModel[27].setRotationPoint(0F, 0F, 0F);
		
		renderOffset = 0F;

		flipAll();
	}
}