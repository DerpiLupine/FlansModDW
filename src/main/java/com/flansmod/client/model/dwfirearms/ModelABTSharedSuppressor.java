package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.ModelAttachment;
import com.flansmod.client.tmt.ModelRendererTurbo;

import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelABTSharedSuppressor extends ModelAttachment
{
	int textureX = 512;
	int textureY = 256;

	public ModelABTSharedSuppressor()
	{
		attachmentModel = new ModelRendererTurbo[8];
		attachmentModel[0] = new ModelRendererTurbo(this, 1, 11, textureX, textureY); // Box 0
		attachmentModel[1] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 1
		attachmentModel[2] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 3
		attachmentModel[3] = new ModelRendererTurbo(this, 60, 1, textureX, textureY); // Box 4
		attachmentModel[4] = new ModelRendererTurbo(this, 60, 12, textureX, textureY); // Box 5
		attachmentModel[5] = new ModelRendererTurbo(this, 60, 1, textureX, textureY); // Box 6
		attachmentModel[6] = new ModelRendererTurbo(this, 1, 22, textureX, textureY); // Box 7
		attachmentModel[7] = new ModelRendererTurbo(this, 1, 22, textureX, textureY); // Box 8

		attachmentModel[0].addBox(0F, -1.5F, -3.5F, 22, 3, 7, 0F); // Box 0
		attachmentModel[0].setRotationPoint(0F, 0F, 0F);

		attachmentModel[1].addShapeBox(0F, 1.5F, -3.5F, 22, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 1
		attachmentModel[1].setRotationPoint(0F, 0F, 0F);

		attachmentModel[2].addShapeBox(0F, -3.5F, -3.5F, 22, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 3
		attachmentModel[2].setRotationPoint(0F, 0F, 0F);

		attachmentModel[3].addShapeBox(1F, 2F, -4F, 2, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 4
		attachmentModel[3].setRotationPoint(0F, 0F, 0F);

		attachmentModel[4].addBox(1F, -2F, -4F, 2, 4, 8, 0F); // Box 5
		attachmentModel[4].setRotationPoint(0F, 0F, 0F);

		attachmentModel[5].addShapeBox(1F, -4F, -4F, 2, 2, 8, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 6
		attachmentModel[5].setRotationPoint(0F, 0F, 0F);

		attachmentModel[6].addShapeBox(4F, 2F, -4F, 15, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 7
		attachmentModel[6].setRotationPoint(0F, 0F, 0F);

		attachmentModel[7].addBox(4F, 0F, -4F, 15, 2, 8, 0F); // Box 8
		attachmentModel[7].setRotationPoint(0F, 0F, 0F);

		renderOffset = 0F;

		//0 Positioning.

		flipAll();
	}
}