package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.ModelAttachment;
import com.flansmod.client.tmt.ModelRendererTurbo;

import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelSMCCompactHoloSight extends ModelAttachment
{
	int textureX = 512;
	int textureY = 256;

	public ModelSMCCompactHoloSight()
	{
		attachmentModel = new ModelRendererTurbo[17];
		attachmentModel[0] = new ModelRendererTurbo(this, 1, 31, textureX, textureY); // scope
		attachmentModel[1] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // scope
		attachmentModel[2] = new ModelRendererTurbo(this, 53, 17, textureX, textureY); // Box 1
		attachmentModel[3] = new ModelRendererTurbo(this, 54, 1, textureX, textureY); // Box 6
		attachmentModel[4] = new ModelRendererTurbo(this, 32, 17, textureX, textureY); // Box 8
		attachmentModel[5] = new ModelRendererTurbo(this, 20, 35, textureX, textureY); // Box 10
		attachmentModel[6] = new ModelRendererTurbo(this, 54, 30, textureX, textureY); // Box 11
		attachmentModel[7] = new ModelRendererTurbo(this, 20, 25, textureX, textureY); // Box 12
		attachmentModel[8] = new ModelRendererTurbo(this, 20, 32, textureX, textureY); // Box 13
		attachmentModel[9] = new ModelRendererTurbo(this, 20, 29, textureX, textureY); // Box 14
		attachmentModel[10] = new ModelRendererTurbo(this, 1, 15, textureX, textureY); // Box 17
		attachmentModel[11] = new ModelRendererTurbo(this, 81, 4, textureX, textureY); // Box 19
		attachmentModel[12] = new ModelRendererTurbo(this, 31, 30, textureX, textureY); // Box 0
		attachmentModel[13] = new ModelRendererTurbo(this, 1, 42, textureX, textureY); // Box 1
		attachmentModel[14] = new ModelRendererTurbo(this, 1, 42, textureX, textureY); // Box 3
		attachmentModel[15] = new ModelRendererTurbo(this, 28, 41, textureX, textureY); // Box 4
		attachmentModel[16] = new ModelRendererTurbo(this, 28, 41, textureX, textureY); // Box 5

		attachmentModel[0].addShapeBox(8F, -4F, -4F, 1, 1, 8, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope
		attachmentModel[0].setRotationPoint(0F, 0F, 0F);

		attachmentModel[1].addBox(-9F, -3F, -4F, 18, 3, 8, 0F); // scope
		attachmentModel[1].setRotationPoint(0F, 0F, 0F);

		attachmentModel[2].addShapeBox(-8F, -13F, -4F, 9, 6, 1, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 1
		attachmentModel[2].setRotationPoint(0F, 0F, 0F);

		attachmentModel[3].addBox(-8F, -7F, -4F, 9, 4, 8, 0F); // Box 6
		attachmentModel[3].setRotationPoint(0F, 0F, 0F);

		attachmentModel[4].addShapeBox(-8F, -13F, 3F, 9, 6, 1, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 8
		attachmentModel[4].setRotationPoint(0F, 0F, 0F);

		attachmentModel[5].addShapeBox(-5F, -12F, -5F, 4, 4, 1, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 10
		attachmentModel[5].setRotationPoint(0F, 0F, 0F);

		attachmentModel[6].addBox(1F, -5F, -4F, 5, 2, 8, 0F); // Box 11
		attachmentModel[6].setRotationPoint(0F, 0F, 0F);

		attachmentModel[7].addShapeBox(1F, -6F, 2F, 6, 1, 2, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 12
		attachmentModel[7].setRotationPoint(0F, 0F, 0F);

		attachmentModel[8].addShapeBox(3.5F, -4.5F, 4F, 2, 1, 1, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 13
		attachmentModel[8].setRotationPoint(0F, 0F, 0F);

		attachmentModel[9].addShapeBox(3.5F, -3.5F, 4F, 2, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 14
		attachmentModel[9].setRotationPoint(0F, 0F, 0F);

		attachmentModel[10].addShapeBox(-6F, -14F, -4F, 7, 1, 8, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 17
		attachmentModel[10].setRotationPoint(0F, 0F, 0F);

		attachmentModel[11].addShapeBox(-4F, -13F, -3F, 1, 18, 18, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -12F, 0F, 0F, -12F, 0F, -12F, 0F, 0F, -12F, 0F, 0F, -12F, -12F, 0F, -12F, -12F); // Box 19
		attachmentModel[11].setRotationPoint(0F, 0F, 0F);

		attachmentModel[12].addShapeBox(6F, -5F, -4F, 3, 2, 8, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 0
		attachmentModel[12].setRotationPoint(0F, 0F, 0F);

		attachmentModel[13].addShapeBox(-11F, -4.5F, -4.5F, 4, 2, 9, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 1
		attachmentModel[13].setRotationPoint(0F, 0F, 0F);

		attachmentModel[14].addShapeBox(-11F, -2.5F, -4.5F, 4, 2, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 3
		attachmentModel[14].setRotationPoint(0F, 0F, 0F);

		attachmentModel[15].addShapeBox(-11F, -4.5F, -5F, 4, 2, 10, 0F, -1.15F, -0.25F, 0F, -1.15F, -0.25F, 0F, -1.15F, -0.25F, 0F, -1.15F, -0.25F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F); // Box 4
		attachmentModel[15].setRotationPoint(0F, 0F, 0F);

		attachmentModel[16].addShapeBox(-11F, -2.5F, -5F, 4, 2, 10, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -1.15F, -0.25F, 0F, -1.15F, -0.25F, 0F, -1.15F, -0.25F, 0F, -1.15F, -0.25F, 0F); // Box 5
		attachmentModel[16].setRotationPoint(0F, 0F, 0F);

		renderOffset = 0F;

		flipAll();
	}
}