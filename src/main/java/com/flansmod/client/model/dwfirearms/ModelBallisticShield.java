package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelBallisticShield extends ModelGun
{
	int textureX = 512;
	int textureY = 256;

	public ModelBallisticShield()
	{
		gunModel = new ModelRendererTurbo[12];
		gunModel[0] = new ModelRendererTurbo(this, 1, 118, textureX, textureY); // Box 0
		gunModel[1] = new ModelRendererTurbo(this, 46, 71, textureX, textureY); // Box 2
		gunModel[2] = new ModelRendererTurbo(this, 46, 71, textureX, textureY); // Box 3
		gunModel[3] = new ModelRendererTurbo(this, 55, 64, textureX, textureY); // Box 4
		gunModel[4] = new ModelRendererTurbo(this, 55, 64, textureX, textureY); // Box 5
		gunModel[5] = new ModelRendererTurbo(this, 55, 87, textureX, textureY); // Box 6
		gunModel[6] = new ModelRendererTurbo(this, 55, 99, textureX, textureY); // Box 7
		gunModel[7] = new ModelRendererTurbo(this, 1, 66, textureX, textureY); // Box 0
		gunModel[8] = new ModelRendererTurbo(this, 36, 119, textureX, textureY); // Box 1
		gunModel[9] = new ModelRendererTurbo(this, 62, 99, textureX, textureY); // Box 2
		gunModel[10] = new ModelRendererTurbo(this, 62, 99, textureX, textureY); // Box 3
		gunModel[11] = new ModelRendererTurbo(this, 55, 99, textureX, textureY); // Box 4

		gunModel[0].addBox(0F, 0F, 0F, 1, 7, 16, 0F); // Box 0
		gunModel[0].setRotationPoint(2F, -14F, -8F);

		gunModel[1].addShapeBox(0F, 0F, 0F, 2, 44, 2, 0F,0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 2
		gunModel[1].setRotationPoint(1.5F, -18F, -12F);

		gunModel[2].addShapeBox(0F, 0F, 0F, 2, 44, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F); // Box 3
		gunModel[2].setRotationPoint(1.5F, -18F, 10F);

		gunModel[3].addBox(0F, 0F, 0F, 2, 2, 20, 0F); // Box 4
		gunModel[3].setRotationPoint(1.5F, -18F, -10F);

		gunModel[4].addBox(0F, 0F, 0F, 2, 2, 20, 0F); // Box 5
		gunModel[4].setRotationPoint(1.5F, 24F, -10F);

		gunModel[5].addBox(0F, 0F, 0F, 1, 9, 22, 0F); // Box 6
		gunModel[5].setRotationPoint(1F, -5F, -11F);

		gunModel[6].addBox(0F, 0F, 0F, 1, 7, 2, 0F); // Box 7
		gunModel[6].setRotationPoint(0F, -4F, -8F);

		gunModel[7].addBox(0F, 0F, 0F, 2, 31, 20, 0F); // Box 0
		gunModel[7].setRotationPoint(1.5F, -7F, -10F);

		gunModel[8].addBox(0F, 0F, 0F, 2, 2, 20, 0F); // Box 1
		gunModel[8].setRotationPoint(1.5F, -16F, -10F);

		gunModel[9].addBox(0F, 0F, 0F, 2, 7, 2, 0F); // Box 2
		gunModel[9].setRotationPoint(1.5F, -14F, -10F);

		gunModel[10].addBox(0F, 0F, 0F, 2, 7, 2, 0F); // Box 3
		gunModel[10].setRotationPoint(1.5F, -14F, 8F);

		gunModel[11].addBox(0F, 0F, 0F, 1, 7, 2, 0F); // Box 4
		gunModel[11].setRotationPoint(0F, -4F, 6F);

		translateAll(0F, 0F, 0F);

		//THIS IS A SHIELD.
		//Owner:"DerpiWolf"


		flipAll();
	}
}