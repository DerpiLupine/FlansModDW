package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.ModelAttachment;
import com.flansmod.client.tmt.ModelRendererTurbo;

import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelLPMPistolGrip extends ModelAttachment 
{
	int textureX = 512;
	int textureY = 256;

	public ModelLPMPistolGrip()
	{
		attachmentModel = new ModelRendererTurbo[16];
		attachmentModel[0] = new ModelRendererTurbo(this, 40, 191, textureX, textureY); // pistol2
		attachmentModel[1] = new ModelRendererTurbo(this, 1, 226, textureX, textureY); // pistol5
		attachmentModel[2] = new ModelRendererTurbo(this, 1, 239, textureX, textureY); // pistol6
		attachmentModel[3] = new ModelRendererTurbo(this, 1, 239, textureX, textureY); // pistol6
		attachmentModel[4] = new ModelRendererTurbo(this, 45, 228, textureX, textureY); // pistol7
		attachmentModel[5] = new ModelRendererTurbo(this, 45, 228, textureX, textureY); // pistol7
		attachmentModel[6] = new ModelRendererTurbo(this, 45, 228, textureX, textureY); // pistol7
		attachmentModel[7] = new ModelRendererTurbo(this, 1, 210, textureX, textureY); // pistol3
		attachmentModel[8] = new ModelRendererTurbo(this, 1, 190, textureX, textureY); // pistol1
		attachmentModel[9] = new ModelRendererTurbo(this, 47, 212, textureX, textureY); // pistol8
		attachmentModel[10] = new ModelRendererTurbo(this, 80, 210, textureX, textureY); // grip1
		attachmentModel[11] = new ModelRendererTurbo(this, 80, 222, textureX, textureY); // grip3
		attachmentModel[12] = new ModelRendererTurbo(this, 115, 214, textureX, textureY); // grip4
		attachmentModel[13] = new ModelRendererTurbo(this, 80, 194, textureX, textureY); // grip2
		attachmentModel[14] = new ModelRendererTurbo(this, 22, 227, textureX, textureY); // pistol9
		attachmentModel[15] = new ModelRendererTurbo(this, 24, 211, textureX, textureY); // pistol4

		attachmentModel[0].addShapeBox(-7F, 0F, -4.5F, 10, 9, 9, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // pistol2
		attachmentModel[0].setRotationPoint(0F, 0F, 0F);

		attachmentModel[1].addShapeBox(3F, 14F, -4.5F, 1, 3, 9, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // pistol5
		attachmentModel[1].setRotationPoint(0F, 0F, 0F);

		attachmentModel[2].addShapeBox(3F, 18F, -4.5F, 2, 1, 9, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // pistol6
		attachmentModel[2].setRotationPoint(0F, 0F, 0F);

		attachmentModel[3].addShapeBox(3F, 17F, -4.5F, 2, 1, 9, 0F, 0F, 0F, 0F, -1F, 0F, -1F, -1F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // pistol6
		attachmentModel[3].setRotationPoint(0F, 0F, 0F);

		attachmentModel[4].addShapeBox(3F, 6F, -4.5F, 3, 1, 9, 0F, 0F, 0F, 0F, -1F, 0F, -1F, -1F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // pistol7
		attachmentModel[4].setRotationPoint(0F, 0F, 0F);

		attachmentModel[5].addShapeBox(3F, 7F, -4.5F, 3, 1, 9, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // pistol7
		attachmentModel[5].setRotationPoint(0F, 0F, 0F);

		attachmentModel[6].addShapeBox(3F, 8F, -4.5F, 3, 1, 9, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -1F, -1F, 0F, -1F, 0F, 0F, 0F); // pistol7
		attachmentModel[6].setRotationPoint(0F, 0F, 0F);

		attachmentModel[7].addShapeBox(3F, 0F, -4.5F, 2, 6, 9, 0F, 0F, 0F, 0F, 1F, 0F, -1F, 1F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // pistol3
		attachmentModel[7].setRotationPoint(0F, 0F, 0F);

		attachmentModel[8].addBox(-7F, 9F, -4.5F, 10, 10, 9, 0F); // pistol1
		attachmentModel[8].setRotationPoint(0F, 0F, 0F);

		attachmentModel[9].addShapeBox(-7F, 0F, -4.5F, 1, 4, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F); // pistol8
		attachmentModel[9].setRotationPoint(0F, 0F, 0F);

		attachmentModel[10].addShapeBox(-9.2F, 15F, -4.2F, 10, 2, 9, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0F, 0F, 0F, 0F, 0F); // grip1
		attachmentModel[10].setRotationPoint(0F, 0F, 0F);

		attachmentModel[11].addShapeBox(-7.2F, 4F, -4.2F, 8, 5, 9, 0F, -0.5F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0F, 0F, 0F, 0F, 0F); // grip3
		attachmentModel[11].setRotationPoint(0F, 0F, 0F);

		attachmentModel[12].addShapeBox(0.8F, 4F, -4.2F, 1, 13, 9, 0F, 0F, 0F, 0.4F, 0F, -1F, 0.4F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0.4F, 0F, -1F, 0.4F, 0F, -1F, 0F, 0F, 0F, 0F); // grip4
		attachmentModel[12].setRotationPoint(0F, 0F, 0F);

		attachmentModel[13].addShapeBox(-9.2F, 9F, -4.2F, 10, 6, 9, 0F, -2F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0F, 0F, 0F, 0F, 0F); // grip2
		attachmentModel[13].setRotationPoint(0F, 0F, 0F);

		attachmentModel[14].addBox(-9F, 17F, -4.5F, 2, 2, 9, 0F); // pistol9
		attachmentModel[14].setRotationPoint(0F, 0F, 0F);

		attachmentModel[15].addShapeBox(3F, 9F, -4.5F, 2, 5, 9, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -1F, -1F, 0F, -1F, 0F, 0F, 0F); // pistol4
		attachmentModel[15].setRotationPoint(0F, 0F, 0F);

		renderOffset = 0F;

		flipAll();
	}
}