package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelRoselupHighwind extends ModelGun
{
	int textureX = 512;
	int textureY = 256;

	public ModelRoselupHighwind()
	{
		gunModel = new ModelRendererTurbo[33];
		gunModel[0] = new ModelRendererTurbo(this, 96, 88, textureX, textureY); // Import body1
		gunModel[1] = new ModelRendererTurbo(this, 191, 115, textureX, textureY); // Import body2
		gunModel[2] = new ModelRendererTurbo(this, 96, 101, textureX, textureY); // Import body3
		gunModel[3] = new ModelRendererTurbo(this, 160, 107, textureX, textureY); // Import body4
		gunModel[4] = new ModelRendererTurbo(this, 143, 101, textureX, textureY); // Import body5
		gunModel[5] = new ModelRendererTurbo(this, 96, 65, textureX, textureY); // Import body6
		gunModel[6] = new ModelRendererTurbo(this, 96, 33, textureX, textureY); // Import lowerBarrelTop
		gunModel[7] = new ModelRendererTurbo(this, 96, 1, textureX, textureY); // Import mainBarrelTop
		gunModel[8] = new ModelRendererTurbo(this, 96, 11, textureX, textureY); // Import mainBarrelMiddle
		gunModel[9] = new ModelRendererTurbo(this, 96, 22, textureX, textureY); // Import mainBarrelBottom
		gunModel[10] = new ModelRendererTurbo(this, 96, 54, textureX, textureY); // Import lowerBarrelBottom
		gunModel[11] = new ModelRendererTurbo(this, 96, 43, textureX, textureY); // Import lowerBarrelMiddle
		gunModel[12] = new ModelRendererTurbo(this, 47, 46, textureX, textureY); // Import canister1
		gunModel[13] = new ModelRendererTurbo(this, 26, 24, textureX, textureY); // Import canister2
		gunModel[14] = new ModelRendererTurbo(this, 26, 15, textureX, textureY); // Import canister3
		gunModel[15] = new ModelRendererTurbo(this, 26, 33, textureX, textureY); // Import canister4
		gunModel[16] = new ModelRendererTurbo(this, 64, 24, textureX, textureY); // Import meter1
		gunModel[17] = new ModelRendererTurbo(this, 64, 34, textureX, textureY); // Import meter1-2
		gunModel[18] = new ModelRendererTurbo(this, 64, 44, textureX, textureY); // Import meter2
		gunModel[19] = new ModelRendererTurbo(this, 21, 10, textureX, textureY); // Import pipe1
		gunModel[20] = new ModelRendererTurbo(this, 1, 15, textureX, textureY); // Import pipe2
		gunModel[21] = new ModelRendererTurbo(this, 14, 16, textureX, textureY); // Import pipe3
		gunModel[22] = new ModelRendererTurbo(this, 77, 9, textureX, textureY); // Import railPart1
		gunModel[23] = new ModelRendererTurbo(this, 46, 3, textureX, textureY); // Import railPart2
		gunModel[24] = new ModelRendererTurbo(this, 1, 22, textureX, textureY); // Import ringPart1
		gunModel[25] = new ModelRendererTurbo(this, 1, 22, textureX, textureY); // Import ringPart1-2
		gunModel[26] = new ModelRendererTurbo(this, 1, 22, textureX, textureY); // Import ringPart1-3
		gunModel[27] = new ModelRendererTurbo(this, 1, 22, textureX, textureY); // Import ringPart1-4
		gunModel[28] = new ModelRendererTurbo(this, 1, 42, textureX, textureY); // Import ringPart2
		gunModel[29] = new ModelRendererTurbo(this, 1, 42, textureX, textureY); // Import ringPart2-2
		gunModel[30] = new ModelRendererTurbo(this, 1, 32, textureX, textureY); // Import ringPart3
		gunModel[31] = new ModelRendererTurbo(this, 1, 32, textureX, textureY); // Import ringPart3-2
		gunModel[32] = new ModelRendererTurbo(this, 22, 42, textureX, textureY); // Import ringPart4

		gunModel[0].addShapeBox(0F, 0F, 0F, 37, 2, 10, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import body1
		gunModel[0].setRotationPoint(-12F, -24F, -5F);

		gunModel[1].addBox(0F, 0F, 0F, 12, 1, 3, 0F); // Import body2
		gunModel[1].setRotationPoint(8F, -22F, -5F);

		gunModel[2].addBox(0F, 0F, 0F, 20, 15, 3, 0F); // Import body3
		gunModel[2].setRotationPoint(-12F, -22F, -5F);

		gunModel[3].addBox(0F, 0F, 0F, 12, 9, 3, 0F); // Import body4
		gunModel[3].setRotationPoint(8F, -16F, -5F);

		gunModel[4].addBox(0F, 0F, 0F, 5, 15, 3, 0F); // Import body5
		gunModel[4].setRotationPoint(20F, -22F, -5F);

		gunModel[5].addBox(0F, 0F, 0F, 37, 15, 7, 0F); // Import body6
		gunModel[5].setRotationPoint(-12F, -22F, -2F);

		gunModel[6].addShapeBox(0F, 0F, 0F, 55, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import lowerBarrelTop
		gunModel[6].setRotationPoint(25F, -15F, -3.5F);

		gunModel[7].addShapeBox(0F, 0F, 0F, 55, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import mainBarrelTop
		gunModel[7].setRotationPoint(25F, -23F, -3.5F);

		gunModel[8].addBox(0F, 0F, 0F, 55, 3, 7, 0F); // Import mainBarrelMiddle
		gunModel[8].setRotationPoint(25F, -21F, -3.5F);

		gunModel[9].addShapeBox(0F, 0F, 0F, 55, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Import mainBarrelBottom
		gunModel[9].setRotationPoint(25F, -18F, -3.5F);

		gunModel[10].addShapeBox(0F, 0F, 0F, 55, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Import lowerBarrelBottom
		gunModel[10].setRotationPoint(25F, -10F, -3.5F);

		gunModel[11].addBox(0F, 0F, 0F, 55, 3, 7, 0F); // Import lowerBarrelMiddle
		gunModel[11].setRotationPoint(25F, -13F, -3.5F);

		gunModel[12].addBox(0F, 0F, 0F, 6, 6, 2, 0F); // Import canister1
		gunModel[12].setRotationPoint(-9F, -22F, -6.5F);

		gunModel[13].addShapeBox(0F, 0F, 0F, 10, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Import canister2
		gunModel[13].setRotationPoint(-10F, -18F, -11F);

		gunModel[14].addBox(0F, 0F, 0F, 10, 2, 6, 0F); // Import canister3
		gunModel[14].setRotationPoint(-10F, -20F, -11F);

		gunModel[15].addShapeBox(0F, 0F, 0F, 10, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import canister4
		gunModel[15].setRotationPoint(-10F, -22F, -11F);

		gunModel[16].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Import meter1
		gunModel[16].setRotationPoint(-8F, -24F, -10F);

		gunModel[17].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import meter1-2
		gunModel[17].setRotationPoint(-8F, -29F, -10F);

		gunModel[18].addBox(0F, 0F, 0F, 2, 3, 7, 0F); // Import meter2
		gunModel[18].setRotationPoint(-8F, -27F, -10F);

		gunModel[19].addBox(0F, 0F, 0F, 3, 2, 2, 0F); // Import pipe1
		gunModel[19].setRotationPoint(0F, -20F, -9F);

		gunModel[20].addBox(0F, 0F, 0F, 2, 2, 4, 0F); // Import pipe2
		gunModel[20].setRotationPoint(3F, -20F, -9F);

		gunModel[21].addBox(0F, 0F, 0F, 4, 4, 1, 0F); // Import pipe3
		gunModel[21].setRotationPoint(2F, -21F, -5.5F);

		gunModel[22].addShapeBox(0F, 0F, 0F, 4, 2, 4, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F); // Import railPart1
		gunModel[22].setRotationPoint(-9F, -26F, -2F);

		gunModel[23].addBox(0F, 0F, 0F, 20, 1, 4, 0F); // Import railPart2
		gunModel[23].setRotationPoint(-6F, -26F, -2F);

		gunModel[24].addShapeBox(0F, 0F, 0F, 2, 1, 8, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F); // Import ringPart1
		gunModel[24].setRotationPoint(75F, -9F, -4F);

		gunModel[25].addShapeBox(0F, 0F, 0F, 2, 1, 8, 0F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F); // Import ringPart1-2
		gunModel[25].setRotationPoint(75F, -15F, -4F);

		gunModel[26].addShapeBox(0F, 0F, 0F, 2, 1, 8, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F); // Import ringPart1-3
		gunModel[26].setRotationPoint(75F, -17F, -4F);

		gunModel[27].addShapeBox(0F, 0F, 0F, 2, 1, 8, 0F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F); // Import ringPart1-4
		gunModel[27].setRotationPoint(75F, -23F, -4F);

		gunModel[28].addBox(0F, 0F, 0F, 2, 4, 8, 0F); // Import ringPart2
		gunModel[28].setRotationPoint(75F, -13.5F, -4F);

		gunModel[29].addBox(0F, 0F, 0F, 2, 4, 8, 0F); // Import ringPart2-2
		gunModel[29].setRotationPoint(75F, -21.5F, -4F);

		gunModel[30].addShapeBox(0F, 0F, 0F, 4, 1, 8, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F); // Import ringPart3
		gunModel[30].setRotationPoint(25F, -17F, -4F);

		gunModel[31].addShapeBox(0F, 0F, 0F, 4, 1, 8, 0F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F); // Import ringPart3-2
		gunModel[31].setRotationPoint(25F, -23F, -4F);

		gunModel[32].addBox(0F, 0F, 0F, 4, 4, 8, 0F); // Import ringPart4
		gunModel[32].setRotationPoint(25F, -21.5F, -4F);


		defaultScopeModel = new ModelRendererTurbo[6];
		defaultScopeModel[0] = new ModelRendererTurbo(this, 10, 8, textureX, textureY); // Import ironSight1
		defaultScopeModel[1] = new ModelRendererTurbo(this, 6, 1, textureX, textureY); // Import ironSight2
		defaultScopeModel[2] = new ModelRendererTurbo(this, 6, 1, textureX, textureY); // Import ironSight2-2
		defaultScopeModel[3] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Import ironSight3-1
		defaultScopeModel[4] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Import ironSight3-2
		defaultScopeModel[5] = new ModelRendererTurbo(this, 1, 7, textureX, textureY); // Import ironSight4

		defaultScopeModel[0].addBox(0F, 0F, 0F, 1, 2, 4, 0F); // Import ironSight1
		defaultScopeModel[0].setRotationPoint(-4F, -28F, -2F);

		defaultScopeModel[1].addShapeBox(0F, 0F, 0F, 1, 1, 4, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import ironSight2
		defaultScopeModel[1].setRotationPoint(-4F, -29F, -2F);

		defaultScopeModel[2].addShapeBox(0F, 0F, 0F, 1, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F); // Import ironSight2-2
		defaultScopeModel[2].setRotationPoint(-4F, -34F, -2F);

		defaultScopeModel[3].addBox(0F, 0F, 0F, 1, 4, 1, 0F); // Import ironSight3-1
		defaultScopeModel[3].setRotationPoint(-4F, -33F, -3F);

		defaultScopeModel[4].addBox(0F, 0F, 0F, 1, 4, 1, 0F); // Import ironSight3-2
		defaultScopeModel[4].setRotationPoint(-4F, -33F, 2F);

		defaultScopeModel[5].addShapeBox(0F, 0F, 0F, 2, 5, 2, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import ironSight4
		defaultScopeModel[5].setRotationPoint(75F, -28.5F, -1F);


		defaultStockModel = new ModelRendererTurbo[12];
		defaultStockModel[0] = new ModelRendererTurbo(this, 47, 133, textureX, textureY); // Import stock1
		defaultStockModel[1] = new ModelRendererTurbo(this, 70, 151, textureX, textureY); // Import stock10
		defaultStockModel[2] = new ModelRendererTurbo(this, 1, 133, textureX, textureY); // Import stock2
		defaultStockModel[3] = new ModelRendererTurbo(this, 1, 94, textureX, textureY); // Import stock3
		defaultStockModel[4] = new ModelRendererTurbo(this, 41, 146, textureX, textureY); // Import stock4
		defaultStockModel[5] = new ModelRendererTurbo(this, 1, 118, textureX, textureY); // Import stock5
		defaultStockModel[6] = new ModelRendererTurbo(this, 41, 163, textureX, textureY); // Import stock6
		defaultStockModel[7] = new ModelRendererTurbo(this, 26, 155, textureX, textureY); // Import stock7
		defaultStockModel[8] = new ModelRendererTurbo(this, 1, 149, textureX, textureY); // Import stock8
		defaultStockModel[9] = new ModelRendererTurbo(this, 70, 164, textureX, textureY); // Import stock9
		defaultStockModel[10] = new ModelRendererTurbo(this, 41, 163, textureX, textureY); // Import Box10
		defaultStockModel[11] = new ModelRendererTurbo(this, 41, 163, textureX, textureY); // Import Box11

		defaultStockModel[0].addShapeBox(0F, 0F, 0F, 12, 2, 10, 0F, 0F, -10F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, -10F, -2F, 0F, 10F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 10F, 0F); // Import stock1
		defaultStockModel[0].setRotationPoint(-24F, -24F, -5F);

		defaultStockModel[1].addShapeBox(0F, 0F, 0F, 2, 2, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Import stock10
		defaultStockModel[1].setRotationPoint(-56F, 5F, -5F);

		defaultStockModel[2].addShapeBox(0F, 0F, 0F, 12, 5, 10, 0F, 0F, 0F, 0F, 0F, 10F, 0F, 0F, 10F, 0F, 0F, 0F, 0F, 0F, 8F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 8F, 0F); // Import stock2
		defaultStockModel[2].setRotationPoint(-24F, -12F, -5F);

		defaultStockModel[3].addShapeBox(0F, 0F, 0F, 29, 13, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 5F, 0F); // Import stock3
		defaultStockModel[3].setRotationPoint(-53F, -12F, -5F);

		defaultStockModel[4].addShapeBox(0F, 0F, 0F, 6, 4, 10, 0F, 0F, 0F, -2F, 0F, -2F, -2F, 0F, -2F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import stock4
		defaultStockModel[4].setRotationPoint(-30F, -16F, -5F);

		defaultStockModel[5].addShapeBox(0F, 0F, 0F, 23, 4, 10, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import stock5
		defaultStockModel[5].setRotationPoint(-53F, -16F, -5F);

		defaultStockModel[6].addBox(0F, 0F, 0F, 3, 3, 11, 0F); // Import stock6
		defaultStockModel[6].setRotationPoint(-50F, -2F, -5.5F);

		defaultStockModel[7].addBox(0F, 0F, 0F, 1, 16, 6, 0F); // Import stock7
		defaultStockModel[7].setRotationPoint(-54F, -13F, -3F);

		defaultStockModel[8].addBox(0F, 0F, 0F, 2, 18, 10, 0F); // Import stock8
		defaultStockModel[8].setRotationPoint(-56F, -13F, -5F);

		defaultStockModel[9].addShapeBox(0F, 0F, 0F, 2, 3, 10, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import stock9
		defaultStockModel[9].setRotationPoint(-56F, -16F, -5F);

		defaultStockModel[10].addBox(0F, 0F, 0F, 3, 3, 11, 0F); // Import Box10
		defaultStockModel[10].setRotationPoint(-13.5F, -12F, -5.5F);

		defaultStockModel[11].addBox(0F, 0F, 0F, 3, 3, 11, 0F); // Import Box11
		defaultStockModel[11].setRotationPoint(-13.5F, -19F, -5.5F);


		ammoModel = new ModelRendererTurbo[6];
		ammoModel[0] = new ModelRendererTurbo(this, 149, 134, textureX, textureY); // Import ammo1
		ammoModel[1] = new ModelRendererTurbo(this, 149, 120, textureX, textureY); // Import ammo2
		ammoModel[2] = new ModelRendererTurbo(this, 96, 153, textureX, textureY); // Import ammo3
		ammoModel[3] = new ModelRendererTurbo(this, 96, 120, textureX, textureY); // Import ammo4
		ammoModel[4] = new ModelRendererTurbo(this, 96, 153, textureX, textureY); // Import ammo5
		ammoModel[5] = new ModelRendererTurbo(this, 149, 120, textureX, textureY); // Import ammo6

		ammoModel[0].addBox(0F, 0F, 0F, 14, 1, 8, 0F); // Import ammo1
		ammoModel[0].setRotationPoint(7F, -7F, -4F);

		ammoModel[1].addShapeBox(0F, 0F, 0F, 14, 3, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, 4F); // Import ammo2
		ammoModel[1].setRotationPoint(7F, -6F, -5F);

		ammoModel[2].addShapeBox(0F, 0F, 0F, 14, 5, 18, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 3F); // Import ammo3
		ammoModel[2].setRotationPoint(7F, -3F, -9F);

		ammoModel[3].addBox(0F, 0F, 0F, 14, 8, 24, 0F); // Import ammo4
		ammoModel[3].setRotationPoint(7F, 2F, -12F);

		ammoModel[4].addShapeBox(0F, 0F, 0F, 14, 5, 18, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import ammo5
		ammoModel[4].setRotationPoint(7F, 10F, -9F);

		ammoModel[5].addShapeBox(0F, 0F, 0F, 14, 3, 10, 0F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import ammo6
		ammoModel[5].setRotationPoint(7F, 15F, -5F);


		slideModel = new ModelRendererTurbo[1];
		slideModel[0] = new ModelRendererTurbo(this, 17, 1, textureX, textureY); // Import bolt

		slideModel[0].addShapeBox(0F, 0F, 0F, 12, 5, 2, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import bolt
		slideModel[0].setRotationPoint(8F, -21F, -4.5F);


		pumpModel = new ModelRendererTurbo[3];
		pumpModel[0] = new ModelRendererTurbo(this, 1, 82, textureX, textureY); // Import pump1
		pumpModel[1] = new ModelRendererTurbo(this, 1, 70, textureX, textureY); // Import pump1-2
		pumpModel[2] = new ModelRendererTurbo(this, 1, 55, textureX, textureY); // Import pump2

		pumpModel[0].addShapeBox(0F, 0F, 0F, 22, 2, 9, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import pump1
		pumpModel[0].setRotationPoint(51F, -16F, -4.5F);

		pumpModel[1].addShapeBox(0F, 0F, 0F, 22, 2, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Import pump1-2
		pumpModel[1].setRotationPoint(51F, -9F, -4.5F);

		pumpModel[2].addBox(0F, 0F, 0F, 22, 5, 9, 0F); // Import pump2
		pumpModel[2].setRotationPoint(51F, -14F, -4.5F);

		scopeAttachPoint = new Vector3f(5F /16F, 24F /16F, 0F /16F);

		gunSlideDistance = 0.5F;
		animationType = EnumAnimationType.BOTTOM_CLIP;

		pumpDelay = 10;
		pumpTime = 7;
		pumpDelayAfterReload = 118;
		pumpHandleDistance = 1.2F;

		translateAll(0F, 0F, 0F);


		flipAll();
	}
}
