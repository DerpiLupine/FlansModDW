package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.ModelAttachment;
import com.flansmod.client.tmt.ModelRendererTurbo;

import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelABTLanternFrontrail extends ModelAttachment
{
	int textureX = 512;
	int textureY = 256;

	public ModelABTLanternFrontrail()
	{	
		attachmentModel = new ModelRendererTurbo[21];
		attachmentModel[0] = new ModelRendererTurbo(this, 110, 111, textureX, textureY); // bodyRail1
		attachmentModel[1] = new ModelRendererTurbo(this, 110, 132, textureX, textureY); // bodyRail3
		attachmentModel[2] = new ModelRendererTurbo(this, 110, 145, textureX, textureY); // Box 19
		attachmentModel[3] = new ModelRendererTurbo(this, 110, 145, textureX, textureY); // Box 20
		attachmentModel[4] = new ModelRendererTurbo(this, 151, 157, textureX, textureY); // clip
		attachmentModel[5] = new ModelRendererTurbo(this, 151, 148, textureX, textureY); // gasCanister
		attachmentModel[6] = new ModelRendererTurbo(this, 151, 148, textureX, textureY); // gasCanister
		attachmentModel[7] = new ModelRendererTurbo(this, 151, 148, textureX, textureY); // gasCanister
		attachmentModel[8] = new ModelRendererTurbo(this, 143, 199, textureX, textureY); // lantern
		attachmentModel[9] = new ModelRendererTurbo(this, 110, 182, textureX, textureY); // lantern2
		attachmentModel[10] = new ModelRendererTurbo(this, 110, 170, textureX, textureY); // lantern3
		attachmentModel[11] = new ModelRendererTurbo(this, 110, 195, textureX, textureY); // lantern4
		attachmentModel[12] = new ModelRendererTurbo(this, 110, 148, textureX, textureY); // lantern5
		attachmentModel[13] = new ModelRendererTurbo(this, 176, 145, textureX, textureY); // light1
		attachmentModel[14] = new ModelRendererTurbo(this, 176, 154, textureX, textureY); // light2
		attachmentModel[15] = new ModelRendererTurbo(this, 151, 168, textureX, textureY); // steamPipe
		attachmentModel[16] = new ModelRendererTurbo(this, 151, 173, textureX, textureY); // steamPipe2
		attachmentModel[17] = new ModelRendererTurbo(this, 151, 173, textureX, textureY); // steamPipe2
		attachmentModel[18] = new ModelRendererTurbo(this, 158, 181, textureX, textureY); // steamPipe3
		attachmentModel[19] = new ModelRendererTurbo(this, 158, 173, textureX, textureY); // steamPipe4
		attachmentModel[20] = new ModelRendererTurbo(this, 110, 98, textureX, textureY); // topRail

		attachmentModel[0].addBox(0F, -5F, -5F, 32, 10, 10, 0F); // bodyRail1
		attachmentModel[0].setRotationPoint(0F, 0F, 0F);

		attachmentModel[1].addShapeBox(0F, 5F, -5F, 32, 2, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // bodyRail3
		attachmentModel[1].setRotationPoint(0F, 0F, 0F);

		attachmentModel[2].addBox(1F, -7F, -4F, 30, 1, 1, 0F); // Box 19
		attachmentModel[2].setRotationPoint(0F, 0F, 0F);

		attachmentModel[3].addBox(1F, -7F, 3F, 30, 1, 1, 0F); // Box 20
		attachmentModel[3].setRotationPoint(0F, 0F, 0F);

		attachmentModel[4].addBox(6F, -3.5F, -8F, 6, 7, 3, 0F); // clip
		attachmentModel[4].setRotationPoint(0F, 0F, 0F);

		attachmentModel[5].addBox(4F, -3F, -9F, 10, 6, 2, 0F); // gasCanister
		attachmentModel[5].setRotationPoint(0F, 0F, 0F);

		attachmentModel[6].addShapeBox(4F, -3F, -7F, 10, 6, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F); // gasCanister
		attachmentModel[6].setRotationPoint(0F, 0F, 0F);

		attachmentModel[7].addShapeBox(4F, -3F, -11F, 10, 6, 2, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // gasCanister
		attachmentModel[7].setRotationPoint(0F, 0F, 0F);

		attachmentModel[8].addBox(23F, 4F, -10F, 4, 1, 4, 0F); // lantern
		attachmentModel[8].setRotationPoint(0F, 0F, 0F);

		attachmentModel[9].addShapeBox(20F, 5F, -13F, 10, 2, 10, 0F, -3F, 0F, -3F, -3F, 0F, -3F, -3F, 0F, -3F, -3F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // lantern2
		attachmentModel[9].setRotationPoint(0F, 0F, 0F);

		attachmentModel[10].addBox(20F, 7F, -13F, 10, 1, 10, 0F); // lantern3
		attachmentModel[10].setRotationPoint(0F, 0F, 0F);

		attachmentModel[11].addShapeBox(21F, 19F, -12F, 8, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F); // lantern4
		attachmentModel[11].setRotationPoint(0F, 0F, 0F);

		attachmentModel[12].addShapeBox(20F, 8F, -13F, 10, 11, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F); // lantern5
		attachmentModel[12].setRotationPoint(0F, 0F, 0F);

		attachmentModel[13].addBox(23F, 8F, -10F, 4, 4, 4, 0F); // light1
		attachmentModel[13].setRotationPoint(0F, 0F, 0F);

		attachmentModel[14].addBox(24F, 12F, -9F, 2, 2, 2, 0F); // light2
		attachmentModel[14].setRotationPoint(0F, 0F, 0F);

		attachmentModel[15].addBox(14F, -1F, -9F, 9, 2, 2, 0F); // steamPipe
		attachmentModel[15].setRotationPoint(0F, 0F, 0F);

		attachmentModel[16].addShapeBox(24F, -11F, -8F, 2, 15, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.4F, 0F, 0F, -0.4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.4F, 0F, 0F, -0.4F, 0F, 0F); // steamPipe2
		attachmentModel[16].setRotationPoint(0F, 0F, 0F);

		attachmentModel[17].addShapeBox(24F, -11F, -9F, 2, 15, 1, 0F, -0.4F, 0F, 0F, -0.4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.4F, 0F, 0F, -0.4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // steamPipe2
		attachmentModel[17].setRotationPoint(0F, 0F, 0F);

		attachmentModel[18].addBox(24F, -1F, -7F, 2, 2, 2, 0F); // steamPipe3
		attachmentModel[18].setRotationPoint(0F, 0F, 0F);

		attachmentModel[19].addBox(23F, -1.5F, -10F, 4, 3, 4, 0F); // steamPipe4
		attachmentModel[19].setRotationPoint(0F, 0F, 0F);

		attachmentModel[20].addShapeBox(0F, -7F, -5F, 32, 2, 10, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // topRail
		attachmentModel[20].setRotationPoint(0F, 0F, 0F);

		renderOffset = 0F;

		//Based off the Ambircon SteamRifle positioning.

		flipAll();
	}
}

