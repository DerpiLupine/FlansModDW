package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.ModelAttachment;
import com.flansmod.client.tmt.ModelRendererTurbo;

import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelSMCReflexSight extends ModelAttachment
{
	int textureX = 512;
	int textureY = 256;

	public ModelSMCReflexSight()
	{
		attachmentModel = new ModelRendererTurbo[39];
		attachmentModel[0] = new ModelRendererTurbo(this, 1, 100, textureX, textureY); // scope
		attachmentModel[1] = new ModelRendererTurbo(this, 1, 88, textureX, textureY); // scope
		attachmentModel[2] = new ModelRendererTurbo(this, 119, 100, textureX, textureY); // Box 19
		attachmentModel[3] = new ModelRendererTurbo(this, 1, 121, textureX, textureY); // Box 0
		attachmentModel[4] = new ModelRendererTurbo(this, 1, 110, textureX, textureY); // Box 1
		attachmentModel[5] = new ModelRendererTurbo(this, 53, 121, textureX, textureY); // Box 2
		attachmentModel[6] = new ModelRendererTurbo(this, 42, 121, textureX, textureY); // Box 3
		attachmentModel[7] = new ModelRendererTurbo(this, 1, 131, textureX, textureY); // Box 4
		attachmentModel[8] = new ModelRendererTurbo(this, 36, 130, textureX, textureY); // Box 5
		attachmentModel[9] = new ModelRendererTurbo(this, 130, 95, textureX, textureY); // Box 10
		attachmentModel[10] = new ModelRendererTurbo(this, 130, 90, textureX, textureY); // Box 14
		attachmentModel[11] = new ModelRendererTurbo(this, 96, 90, textureX, textureY); // Box 15
		attachmentModel[12] = new ModelRendererTurbo(this, 63, 100, textureX, textureY); // Box 16
		attachmentModel[13] = new ModelRendererTurbo(this, 63, 100, textureX, textureY); // Box 20
		attachmentModel[14] = new ModelRendererTurbo(this, 54, 100, textureX, textureY); // Box 21
		attachmentModel[15] = new ModelRendererTurbo(this, 46, 101, textureX, textureY); // Box 22
		attachmentModel[16] = new ModelRendererTurbo(this, 96, 95, textureX, textureY); // Box 24
		attachmentModel[17] = new ModelRendererTurbo(this, 111, 90, textureX, textureY); // Box 25
		attachmentModel[18] = new ModelRendererTurbo(this, 111, 95, textureX, textureY); // Box 26
		attachmentModel[19] = new ModelRendererTurbo(this, 81, 90, textureX, textureY); // Box 27
		attachmentModel[20] = new ModelRendererTurbo(this, 81, 95, textureX, textureY); // Box 28
		attachmentModel[21] = new ModelRendererTurbo(this, 54, 100, textureX, textureY); // Box 37
		attachmentModel[22] = new ModelRendererTurbo(this, 54, 100, textureX, textureY); // Box 38
		attachmentModel[23] = new ModelRendererTurbo(this, 46, 101, textureX, textureY); // Box 39
		attachmentModel[24] = new ModelRendererTurbo(this, 54, 100, textureX, textureY); // Box 40
		attachmentModel[25] = new ModelRendererTurbo(this, 61, 113, textureX, textureY); // Box 49
		attachmentModel[26] = new ModelRendererTurbo(this, 42, 113, textureX, textureY); // Box 50
		attachmentModel[27] = new ModelRendererTurbo(this, 72, 91, textureX, textureY); // Box 52
		attachmentModel[28] = new ModelRendererTurbo(this, 72, 91, textureX, textureY); // Box 53
		attachmentModel[29] = new ModelRendererTurbo(this, 54, 95, textureX, textureY); // Box 54
		attachmentModel[30] = new ModelRendererTurbo(this, 54, 95, textureX, textureY); // Box 55
		attachmentModel[31] = new ModelRendererTurbo(this, 65, 91, textureX, textureY); // Box 56
		attachmentModel[32] = new ModelRendererTurbo(this, 65, 91, textureX, textureY); // Box 57
		attachmentModel[33] = new ModelRendererTurbo(this, 54, 90, textureX, textureY); // Box 58
		attachmentModel[34] = new ModelRendererTurbo(this, 54, 90, textureX, textureY); // Box 59
		attachmentModel[35] = new ModelRendererTurbo(this, 80, 100, textureX, textureY); // Box 60
		attachmentModel[36] = new ModelRendererTurbo(this, 67, 104, textureX, textureY); // Box 61
		attachmentModel[37] = new ModelRendererTurbo(this, 67, 104, textureX, textureY); // Box 62
		attachmentModel[38] = new ModelRendererTurbo(this, 54, 104, textureX, textureY); // Box 63

		attachmentModel[0].addShapeBox(-9F, -4F, -4F, 18, 1, 8, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // scope
		attachmentModel[0].setRotationPoint(0F, 0F, 0F);

		attachmentModel[1].addBox(-9F, -3F, -4F, 18, 3, 8, 0F); // scope
		attachmentModel[1].setRotationPoint(0F, 0F, 0F);

		attachmentModel[2].addShapeBox(2F, -13F, -3F, 1, 18, 18, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -12F, 0F, 0F, -12F, 0F, -12F, 0F, 0F, -12F, 0F, 0F, -12F, -12F, 0F, -12F, -12F); // Box 19
		attachmentModel[2].setRotationPoint(0F, 0F, 0F);

		attachmentModel[3].addShapeBox(-6F, -14F, -4F, 9, 1, 8, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 0
		attachmentModel[3].setRotationPoint(0F, 0F, 0F);

		attachmentModel[4].addShapeBox(-9F, -7F, -4F, 12, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F); // Box 1
		attachmentModel[4].setRotationPoint(0F, 0F, 0F);

		attachmentModel[5].addShapeBox(-9F, -13F, -4F, 4, 6, 1, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 2
		attachmentModel[5].setRotationPoint(0F, 0F, 0F);

		attachmentModel[6].addShapeBox(-9F, -13F, 3F, 4, 6, 1, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 3
		attachmentModel[6].setRotationPoint(0F, 0F, 0F);

		attachmentModel[7].addBox(-9F, -5F, -2.5F, 12, 1, 5, 0F); // Box 4
		attachmentModel[7].setRotationPoint(0F, 0F, 0F);

		attachmentModel[8].addBox(-6F, -15F, -3F, 9, 1, 6, 0F); // Box 5
		attachmentModel[8].setRotationPoint(0F, 0F, 0F);

		attachmentModel[9].addBox(3F, -11.5F, -4.5F, 6, 3, 1, 0F); // Box 10
		attachmentModel[9].setRotationPoint(0F, 0F, 0F);

		attachmentModel[10].addShapeBox(3F, -8.5F, 3.5F, 6, 3, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 14
		attachmentModel[10].setRotationPoint(0F, 0F, 0F);

		attachmentModel[11].addBox(3F, -11.5F, 3.5F, 6, 3, 1, 0F); // Box 15
		attachmentModel[11].setRotationPoint(0F, 0F, 0F);

		attachmentModel[12].addBox(9F, -11.5F, -4F, 1, 3, 1, 0F); // Box 16
		attachmentModel[12].setRotationPoint(0F, 0F, 0F);

		attachmentModel[13].addBox(9F, -11.5F, 3F, 1, 3, 1, 0F); // Box 20
		attachmentModel[13].setRotationPoint(0F, 0F, 0F);

		attachmentModel[14].addShapeBox(9F, -13.5F, 0.5F, 1, 2, 1, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, 2.5F, 0F, 0F, 2.5F); // Box 21
		attachmentModel[14].setRotationPoint(0F, 0F, 0F);

		attachmentModel[15].addBox(9F, -14F, -1.5F, 1, 1, 3, 0F); // Box 22
		attachmentModel[15].setRotationPoint(0F, 0F, 0F);

		attachmentModel[16].addShapeBox(3F, -8.5F, -4.5F, 6, 3, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 3F, 0F, 0F, 3F); // Box 24
		attachmentModel[16].setRotationPoint(0F, 0F, 0F);

		attachmentModel[17].addBox(3F, -6.5F, -1.5F, 6, 1, 3, 0F); // Box 25
		attachmentModel[17].setRotationPoint(0F, 0F, 0F);

		attachmentModel[18].addBox(3F, -14.5F, -1.5F, 6, 1, 3, 0F); // Box 26
		attachmentModel[18].setRotationPoint(0F, 0F, 0F);

		attachmentModel[19].addShapeBox(3F, -14.5F, 3.5F, 6, 3, 1, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 27
		attachmentModel[19].setRotationPoint(0F, 0F, 0F);

		attachmentModel[20].addShapeBox(3F, -14.5F, -4.5F, 6, 3, 1, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 28
		attachmentModel[20].setRotationPoint(0F, 0F, 0F);

		attachmentModel[21].addShapeBox(9F, -14F, -4F, 1, 2, 1, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, 2.5F, 0F, 0F, 2.5F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F); // Box 37
		attachmentModel[21].setRotationPoint(0F, 0F, 0F);

		attachmentModel[22].addShapeBox(9F, -8F, -4F, 1, 2, 1, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, 2.5F, 0F, 0F, 2.5F); // Box 38
		attachmentModel[22].setRotationPoint(0F, 0F, 0F);

		attachmentModel[23].addBox(9F, -7F, -1.5F, 1, 1, 3, 0F); // Box 39
		attachmentModel[23].setRotationPoint(0F, 0F, 0F);

		attachmentModel[24].addShapeBox(9F, -8.5F, 0.5F, 1, 2, 1, 0F, 0F, 0F, -2.5F, 0F, 0F, -2.5F, 0F, 0F, 2.5F, 0F, 0F, 2.5F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F); // Box 40
		attachmentModel[24].setRotationPoint(0F, 0F, 0F);

		attachmentModel[25].addBox(-5F, -13F, 3F, 8, 6, 1, 0F); // Box 49
		attachmentModel[25].setRotationPoint(0F, 0F, 0F);

		attachmentModel[26].addBox(-5F, -13F, -4F, 8, 6, 1, 0F); // Box 50
		attachmentModel[26].setRotationPoint(0F, 0F, 0F);

		attachmentModel[27].addShapeBox(-9F, -5.5F, 3F, 1, 1, 2, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 52
		attachmentModel[27].setRotationPoint(0F, 0F, 0F);

		attachmentModel[28].addShapeBox(-9F, -4.5F, 3F, 1, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 53
		attachmentModel[28].setRotationPoint(0F, 0F, 0F);

		attachmentModel[29].addShapeBox(-8.5F, -5.5F, 2.5F, 10, 1, 3, 0F, 0F, 0.5F, -0.75F, 0F, 0.5F, -0.75F, 0F, 0.5F, -0.75F, 0F, 0.5F, -0.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 54
		attachmentModel[29].setRotationPoint(0F, 0F, 0F);

		attachmentModel[30].addShapeBox(-8.5F, -4.5F, 2.5F, 10, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, -0.75F, 0F, 0.5F, -0.75F, 0F, 0.5F, -0.75F, 0F, 0.5F, -0.75F); // Box 55
		attachmentModel[30].setRotationPoint(0F, 0F, 0F);

		attachmentModel[31].addShapeBox(1F, -5.5F, 3F, 1, 1, 2, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 56
		attachmentModel[31].setRotationPoint(0F, 0F, 0F);

		attachmentModel[32].addShapeBox(1F, -4.5F, 3F, 1, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 57
		attachmentModel[32].setRotationPoint(0F, 0F, 0F);

		attachmentModel[33].addShapeBox(2F, -5.5F, 2.5F, 2, 1, 3, 0F, 0F, 0.5F, -0.75F, 0F, 0.5F, -0.75F, 0F, 0.5F, -0.75F, 0F, 0.5F, -0.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 58
		attachmentModel[33].setRotationPoint(0F, 0F, 0F);

		attachmentModel[34].addShapeBox(2F, -4.5F, 2.5F, 2, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, -0.75F, 0F, 0.5F, -0.75F, 0F, 0.5F, -0.75F, 0F, 0.5F, -0.75F); // Box 59
		attachmentModel[34].setRotationPoint(0F, 0F, 0F);

		attachmentModel[35].addShapeBox(-5F, -13F, -3F, 1, 18, 18, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -12F, 0F, 0F, -12F, 0F, -12F, 0F, 0F, -12F, 0F, 0F, -12F, -12F, 0F, -12F, -12F); // Box 60
		attachmentModel[35].setRotationPoint(0F, 0F, 0F);

		attachmentModel[36].addShapeBox(-0.5F, -16F, -2F, 1, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 61
		attachmentModel[36].setRotationPoint(0F, 0F, 0F);

		attachmentModel[37].addShapeBox(-3.5F, -16F, -2F, 1, 1, 4, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 62
		attachmentModel[37].setRotationPoint(0F, 0F, 0F);

		attachmentModel[38].addBox(-2.5F, -16F, -2F, 2, 1, 4, 0F); // Box 63
		attachmentModel[38].setRotationPoint(0F, 0F, 0F);

		renderOffset = 0F;

		flipAll();
	}
}