package com.flansmod.client.model.dwfirearms; //Path where the model is located

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelWolfstedtRepeater extends ModelGun //Same as Filename
{
	int textureX = 512;
	int textureY = 256;

	public ModelWolfstedtRepeater() //Same as Filename
	{
		gunModel = new ModelRendererTurbo[35];
		gunModel[0] = new ModelRendererTurbo(this, 64, 1, textureX, textureY); // barrelPart1
		gunModel[1] = new ModelRendererTurbo(this, 64, 11, textureX, textureY); // barrelPart2
		gunModel[2] = new ModelRendererTurbo(this, 64, 1, textureX, textureY); // barrelPart3
		gunModel[3] = new ModelRendererTurbo(this, 64, 40, textureX, textureY); // Body1
		gunModel[4] = new ModelRendererTurbo(this, 1, 29, textureX, textureY); // Body10
		gunModel[5] = new ModelRendererTurbo(this, 64, 22, textureX, textureY); // Body2
		gunModel[6] = new ModelRendererTurbo(this, 64, 58, textureX, textureY); // Body3
		gunModel[7] = new ModelRendererTurbo(this, 64, 40, textureX, textureY); // Body4
		gunModel[8] = new ModelRendererTurbo(this, 64, 58, textureX, textureY); // Body5
		gunModel[9] = new ModelRendererTurbo(this, 133, 58, textureX, textureY); // bodyStockEnd1
		gunModel[10] = new ModelRendererTurbo(this, 133, 40, textureX, textureY); // bodyStockEnd2
		gunModel[11] = new ModelRendererTurbo(this, 133, 40, textureX, textureY); // bodyStockEnd3
		gunModel[12] = new ModelRendererTurbo(this, 133, 22, textureX, textureY); // bodyStockEnd4
		gunModel[13] = new ModelRendererTurbo(this, 133, 58, textureX, textureY); // bodyStockEnd5
		gunModel[14] = new ModelRendererTurbo(this, 1, 20, textureX, textureY); // gripPart1
		gunModel[15] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // ironSightPart1
		gunModel[16] = new ModelRendererTurbo(this, 32, 29, textureX, textureY); // Box 0
		gunModel[17] = new ModelRendererTurbo(this, 47, 31, textureX, textureY); // Box 1
		gunModel[18] = new ModelRendererTurbo(this, 18, 58, textureX, textureY); // Box 0
		gunModel[19] = new ModelRendererTurbo(this, 1, 48, textureX, textureY); // Box 1
		gunModel[20] = new ModelRendererTurbo(this, 32, 48, textureX, textureY); // Box 2
		gunModel[21] = new ModelRendererTurbo(this, 33, 59, textureX, textureY); // Box 3
		gunModel[22] = new ModelRendererTurbo(this, 1, 58, textureX, textureY); // Box 4
		gunModel[23] = new ModelRendererTurbo(this, 133, 71, textureX, textureY); // Box 6
		gunModel[24] = new ModelRendererTurbo(this, 133, 83, textureX, textureY); // Box 7
		gunModel[25] = new ModelRendererTurbo(this, 133, 83, textureX, textureY); // Box 8
		gunModel[26] = new ModelRendererTurbo(this, 133, 91, textureX, textureY); // Box 9
		gunModel[27] = new ModelRendererTurbo(this, 158, 91, textureX, textureY); // Box 10
		gunModel[28] = new ModelRendererTurbo(this, 1, 67, textureX, textureY); // Box 13
		gunModel[29] = new ModelRendererTurbo(this, 43, 83, textureX, textureY); // Box 15
		gunModel[30] = new ModelRendererTurbo(this, 36, 83, textureX, textureY); // Box 22
		gunModel[31] = new ModelRendererTurbo(this, 36, 83, textureX, textureY); // Box 23
		gunModel[32] = new ModelRendererTurbo(this, 33, 4, textureX, textureY); // Box 46
		gunModel[33] = new ModelRendererTurbo(this, 33, 4, textureX, textureY); // Box 47
		gunModel[34] = new ModelRendererTurbo(this, 1, 9, textureX, textureY); // Box 48

		gunModel[0].addShapeBox(0F, 0F, 0F, 25, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // barrelPart1
		gunModel[0].setRotationPoint(34F, -23F, -3.5F);

		gunModel[1].addBox(0F, 0F, 0F, 25, 3, 7, 0F); // barrelPart2
		gunModel[1].setRotationPoint(34F, -21F, -3.5F);

		gunModel[2].addShapeBox(0F, 0F, 0F, 25, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // barrelPart3
		gunModel[2].setRotationPoint(34F, -18F, -3.5F);

		gunModel[3].addShapeBox(0F, 0F, 0F, 20, 3, 14, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Body1
		gunModel[3].setRotationPoint(-11F, -24F, -7F);

		gunModel[4].addShapeBox(0F, 0F, 0F, 9, 12, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, -2F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 4F, -2F, 0F); // Body10
		gunModel[4].setRotationPoint(-6F, -11.5F, -3F);

		gunModel[5].addBox(0F, 0F, 0F, 20, 3, 14, 0F); // Body2
		gunModel[5].setRotationPoint(-11F, -21F, -7F);

		gunModel[6].addShapeBox(0F, 0F, 0F, 20, 2, 10, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Body3
		gunModel[6].setRotationPoint(-11F, -26F, -5F);

		gunModel[7].addShapeBox(0F, 0F, 0F, 20, 3, 14, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Body4
		gunModel[7].setRotationPoint(-11F, -18F, -7F);

		gunModel[8].addShapeBox(0F, 0F, 0F, 20, 2, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Body5
		gunModel[8].setRotationPoint(-11F, -15F, -5F);

		gunModel[9].addShapeBox(0F, 0F, 0F, 2, 2, 10, 0F, 0F, -0.5F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, -0.5F, -3F, 0F, 0F, -0.4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.4F); // bodyStockEnd1
		gunModel[9].setRotationPoint(-13F, -26F, -5F);

		gunModel[10].addShapeBox(0F, 0F, 0F, 2, 3, 14, 0F, 0F, 0F, -2.5F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F); // bodyStockEnd2
		gunModel[10].setRotationPoint(-13F, -24F, -7F);

		gunModel[11].addShapeBox(0F, 0F, 0F, 2, 3, 14, 0F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F); // bodyStockEnd3
		gunModel[11].setRotationPoint(-13F, -21F, -7F);

		gunModel[12].addShapeBox(0F, 0F, 0F, 2, 3, 14, 0F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -2.5F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2.5F); // bodyStockEnd4
		gunModel[12].setRotationPoint(-13F, -18F, -7F);

		gunModel[13].addShapeBox(0F, 0F, 0F, 2, 2, 10, 0F, 0F, 0F, -0.4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.4F, 0F, -0.5F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, -0.5F, -3F); // bodyStockEnd5
		gunModel[13].setRotationPoint(-13F, -15F, -5F);

		gunModel[14].addShapeBox(0F, 0F, 0F, 13, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -1F, 0F, 0F); // gripPart1
		gunModel[14].setRotationPoint(-7F, -13.5F, -3F);

		gunModel[15].addShapeBox(0F, 0F, 0F, 6, 2, 5, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // ironSightPart1
		gunModel[15].setRotationPoint(2F, -27F, -2.5F);

		gunModel[16].addShapeBox(0F, 0F, 0F, 1, 12, 6, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 4F, 0F, 0F, -4F, 0F, -1F, -4F, 0F, -1F, 4F, 0F, 0F); // Box 0
		gunModel[16].setRotationPoint(3F, -11.5F, -3F);

		gunModel[17].addShapeBox(0F, 0F, 0F, 1, 10, 6, 0F, -4F, 0F, -1F, 4F, 0F, 0F, 4F, 0F, 0F, -4F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 1
		gunModel[17].setRotationPoint(-11F, -11.5F, -3F);

		gunModel[18].addShapeBox(0F, 0F, 0F, 1, 2, 6, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, -1F, 0F, -1F, 1F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, -1F); // Box 0
		gunModel[18].setRotationPoint(-8F, -13.5F, -3F);

		gunModel[19].addShapeBox(0F, 0F, 0F, 9, 3, 6, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F); // Box 1
		gunModel[19].setRotationPoint(-10F, 0.5F, -3F);

		gunModel[20].addShapeBox(0F, 0F, 0F, 1, 3, 6, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -1F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, -1F); // Box 2
		gunModel[20].setRotationPoint(-11F, -1.5F, -3F);

		gunModel[21].addShapeBox(0F, 0F, 0F, 1, 1, 6, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, -1F, 1F, 0F, -1F, 0F, 0F, 0F); // Box 3
		gunModel[21].setRotationPoint(-1F, 0.5F, -3F);

		gunModel[22].addShapeBox(0F, 0F, 0F, 2, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 4
		gunModel[22].setRotationPoint(-1F, 1.5F, -3F);

		gunModel[23].addBox(0F, 0F, 0F, 25, 3, 8, 0F); // Box 6
		gunModel[23].setRotationPoint(9F, -21F, -3.5F);

		gunModel[24].addShapeBox(0F, 0F, 0F, 25, 3, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2.5F, 0F, 0F, 2.5F, 0F, 0F, 2.5F, 0F, 0F, 2.5F); // Box 7
		gunModel[24].setRotationPoint(9F, -24F, -2F);

		gunModel[25].addShapeBox(0F, 0F, 0F, 25, 3, 4, 0F, 0F, 0F, 2.5F, 0F, 0F, 2.5F, 0F, 0F, 2.5F, 0F, 0F, 2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 8
		gunModel[25].setRotationPoint(9F, -18F, -2F);

		gunModel[26].addBox(0F, 0F, 0F, 11, 3, 1, 0F); // Box 9
		gunModel[26].setRotationPoint(9F, -21F, -4.5F);

		gunModel[27].addBox(0F, 0F, 0F, 6, 3, 1, 0F); // Box 10
		gunModel[27].setRotationPoint(28F, -21F, -4.5F);

		gunModel[28].addBox(0F, 0F, 0F, 11, 6, 4, 0F); // Box 13
		gunModel[28].setRotationPoint(18.5F, -22.5F, 3.5F);

		gunModel[29].addShapeBox(0F, 0F, 0F, 1, 6, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 15
		gunModel[29].setRotationPoint(28.5F, -22.5F, 7.5F);

		gunModel[30].addShapeBox(0F, 0F, 0F, 2, 7, 1, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 22
		gunModel[30].setRotationPoint(17.5F, -23F, 3.5F);

		gunModel[31].addShapeBox(0F, 0F, 0F, 2, 7, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 23
		gunModel[31].setRotationPoint(17.5F, -23F, 4.5F);

		gunModel[32].addBox(0F, 0F, 0F, 3, 3, 1, 0F); // Box 46
		gunModel[32].setRotationPoint(4F, -27.5F, -3.5F);

		gunModel[33].addBox(0F, 0F, 0F, 3, 3, 1, 0F); // Box 47
		gunModel[33].setRotationPoint(4F, -27.5F, 2.5F);

		gunModel[34].addBox(0F, 0F, 0F, 12, 2, 5, 0F); // Box 48
		gunModel[34].setRotationPoint(-10F, -27F, -2.5F);


		defaultBarrelModel = new ModelRendererTurbo[6];
		defaultBarrelModel[0] = new ModelRendererTurbo(this, 64, 91, textureX, textureY); // barrelRingPart1
		defaultBarrelModel[1] = new ModelRendererTurbo(this, 64, 71, textureX, textureY); // barrelRingPart2
		defaultBarrelModel[2] = new ModelRendererTurbo(this, 64, 82, textureX, textureY); // barrelRingPart3
		defaultBarrelModel[3] = new ModelRendererTurbo(this, 129, 1, textureX, textureY); // Box 24
		defaultBarrelModel[4] = new ModelRendererTurbo(this, 129, 10, textureX, textureY); // Box 25
		defaultBarrelModel[5] = new ModelRendererTurbo(this, 129, 1, textureX, textureY); // Box 26

		defaultBarrelModel[0].addShapeBox(0F, 0F, 0F, 20, 2, 6, 0F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F); // barrelRingPart1
		defaultBarrelModel[0].setRotationPoint(37F, -23F, -3F);

		defaultBarrelModel[1].addShapeBox(0F, 0F, 0F, 20, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F); // barrelRingPart2
		defaultBarrelModel[1].setRotationPoint(37F, -21F, -4F);

		defaultBarrelModel[2].addShapeBox(0F, 0F, 0F, 20, 2, 6, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F, 0F, 0.5F, -1.5F); // barrelRingPart3
		defaultBarrelModel[2].setRotationPoint(37F, -18F, -3F);

		defaultBarrelModel[3].addShapeBox(0F, 0F, 0F, 12, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 24
		defaultBarrelModel[3].setRotationPoint(59F, -22.5F, -3F);

		defaultBarrelModel[4].addBox(0F, 0F, 0F, 12, 2, 6, 0F); // Box 25
		defaultBarrelModel[4].setRotationPoint(59F, -20.5F, -3F);

		defaultBarrelModel[5].addShapeBox(0F, 0F, 0F, 12, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 26
		defaultBarrelModel[5].setRotationPoint(59F, -18.5F, -3F);


		defaultScopeModel = new ModelRendererTurbo[10];
		defaultScopeModel[0] = new ModelRendererTurbo(this, 50, 20, textureX, textureY); // ironSightPart4
		defaultScopeModel[1] = new ModelRendererTurbo(this, 50, 20, textureX, textureY); // ironSightPart5
		defaultScopeModel[2] = new ModelRendererTurbo(this, 19, 1, textureX, textureY); // ironSightPart6
		defaultScopeModel[3] = new ModelRendererTurbo(this, 19, 1, textureX, textureY); // ironSightPart7
		defaultScopeModel[4] = new ModelRendererTurbo(this, 24, 2, textureX, textureY); // Box 11
		defaultScopeModel[5] = new ModelRendererTurbo(this, 40, 20, textureX, textureY); // Box 41
		defaultScopeModel[6] = new ModelRendererTurbo(this, 42, 20, textureX, textureY); // Box 42
		defaultScopeModel[7] = new ModelRendererTurbo(this, 40, 20, textureX, textureY); // Box 43
		defaultScopeModel[8] = new ModelRendererTurbo(this, 42, 20, textureX, textureY); // Box 44
		defaultScopeModel[9] = new ModelRendererTurbo(this, 33, 1, textureX, textureY); // Box 45

		defaultScopeModel[0].addShapeBox(0F, 0F, 0F, 1, 1, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // ironSightPart4
		defaultScopeModel[0].setRotationPoint(58F, -27F, -2.5F);

		defaultScopeModel[1].addShapeBox(0F, 0F, 0F, 1, 1, 5, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // ironSightPart5
		defaultScopeModel[1].setRotationPoint(58F, -31F, -2.5F);

		defaultScopeModel[2].addBox(0F, 0F, 0F, 1, 3, 1, 0F); // ironSightPart6
		defaultScopeModel[2].setRotationPoint(58F, -30F, -2.5F);

		defaultScopeModel[3].addBox(0F, 0F, 0F, 1, 3, 1, 0F); // ironSightPart7
		defaultScopeModel[3].setRotationPoint(58F, -30F, 1.5F);

		defaultScopeModel[4].addBox(0F, 0F, 0F, 1, 3, 3, 0F); // Box 11
		defaultScopeModel[4].setRotationPoint(58F, -26F, -1.5F);

		defaultScopeModel[5].addShapeBox(0F, 0F, 0F, 1, 1, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 41
		defaultScopeModel[5].setRotationPoint(5F, -28F, -3.5F);

		defaultScopeModel[6].addBox(0F, 0F, 0F, 1, 5, 1, 0F); // Box 42
		defaultScopeModel[6].setRotationPoint(5F, -33F, 2.5F);

		defaultScopeModel[7].addShapeBox(0F, 0F, 0F, 1, 1, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 43
		defaultScopeModel[7].setRotationPoint(5F, -34F, -3.5F);

		defaultScopeModel[8].addBox(0F, 0F, 0F, 1, 5, 1, 0F); // Box 44
		defaultScopeModel[8].setRotationPoint(5F, -33F, -3.5F);

		defaultScopeModel[9].addBox(0F, 0F, 0F, 1, 1, 1, 0F); // Box 45
		defaultScopeModel[9].setRotationPoint(58F, -28F, -0.5F);


		defaultStockModel = new ModelRendererTurbo[9];
		defaultStockModel[0] = new ModelRendererTurbo(this, 30, 142, textureX, textureY); // Box 32
		defaultStockModel[1] = new ModelRendererTurbo(this, 30, 131, textureX, textureY); // Box 33
		defaultStockModel[2] = new ModelRendererTurbo(this, 30, 142, textureX, textureY); // Box 34
		defaultStockModel[3] = new ModelRendererTurbo(this, 1, 157, textureX, textureY); // Box 35
		defaultStockModel[4] = new ModelRendererTurbo(this, 1, 131, textureX, textureY); // Box 36
		defaultStockModel[5] = new ModelRendererTurbo(this, 1, 157, textureX, textureY); // Box 37
		defaultStockModel[6] = new ModelRendererTurbo(this, 1, 121, textureX, textureY); // Box 38
		defaultStockModel[7] = new ModelRendererTurbo(this, 1, 110, textureX, textureY); // Box 39
		defaultStockModel[8] = new ModelRendererTurbo(this, 1, 121, textureX, textureY); // Box 40

		defaultStockModel[0].addShapeBox(0F, 0F, 0F, 25, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 32
		defaultStockModel[0].setRotationPoint(-44F, -20F, -3.5F);

		defaultStockModel[1].addBox(0F, 0F, 0F, 25, 3, 7, 0F); // Box 33
		defaultStockModel[1].setRotationPoint(-44F, -18F, -3.5F);

		defaultStockModel[2].addShapeBox(0F, 0F, 0F, 25, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 34
		defaultStockModel[2].setRotationPoint(-44F, -15F, -3.5F);

		defaultStockModel[3].addShapeBox(0F, 0F, 0F, 5, 3, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2.5F, 0F, 0F, 2.5F, 0F, 0F, 2.5F, 0F, 0F, 2.5F); // Box 35
		defaultStockModel[3].setRotationPoint(-49F, -21F, -2F);

		defaultStockModel[4].addBox(0F, 0F, 0F, 5, 16, 9, 0F); // Box 36
		defaultStockModel[4].setRotationPoint(-49F, -18F, -4.5F);

		defaultStockModel[5].addShapeBox(0F, 0F, 0F, 5, 3, 4, 0F, 0F, 0F, 2.5F, 0F, 0F, 2.5F, 0F, 0F, 2.5F, 0F, 0F, 2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 37
		defaultStockModel[5].setRotationPoint(-49F, -2F, -2F);

		defaultStockModel[6].addShapeBox(0F, 0F, 0F, 31, 2, 7, 0F, 0F, -16F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, -16F, -2F, 0F, 16F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 16F, 0F); // Box 38
		defaultStockModel[6].setRotationPoint(-44F, -23F, -3.5F);

		defaultStockModel[7].addShapeBox(0F, 0F, 0F, 31, 3, 7, 0F, 0F, -16F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -16F, 0F, 0F, 16F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 16F, 0F); // Box 39
		defaultStockModel[7].setRotationPoint(-44F, -21F, -3.5F);

		defaultStockModel[8].addShapeBox(0F, 0F, 0F, 31, 2, 7, 0F, 0F, -16F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -16F, 0F, 0F, 16F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 16F, -2F); // Box 40
		defaultStockModel[8].setRotationPoint(-44F, -18F, -3.5F);


		ammoModel = new ModelRendererTurbo[7];
		ammoModel[0] = new ModelRendererTurbo(this, 1, 92, textureX, textureY); // Box 14
		ammoModel[1] = new ModelRendererTurbo(this, 1, 78, textureX, textureY); // Box 16
		ammoModel[2] = new ModelRendererTurbo(this, 1, 78, textureX, textureY); // Box 17
		ammoModel[3] = new ModelRendererTurbo(this, 32, 92, textureX, textureY); // Box 18
		ammoModel[4] = new ModelRendererTurbo(this, 32, 67, textureX, textureY); // Box 19
		ammoModel[5] = new ModelRendererTurbo(this, 32, 74, textureX, textureY); // Box 20
		ammoModel[6] = new ModelRendererTurbo(this, 47, 74, textureX, textureY); // Box 21

		ammoModel[0].addBox(0F, 0F, 0F, 9, 5, 12, 0F); // Box 14
		ammoModel[0].setRotationPoint(19F, -22F, 4.5F);

		ammoModel[1].addBox(0F, 0F, 0F, 9, 5, 8, 0F); // Box 16
		ammoModel[1].setRotationPoint(19F, -22F, 16.5F);
		ammoModel[1].rotateAngleY = -0.15707963F;

		ammoModel[2].addBox(0F, 0F, 0F, 9, 5, 8, 0F); // Box 17
		ammoModel[2].setRotationPoint(20.2F, -22F, 24.2F);
		ammoModel[2].rotateAngleY = -0.38397244F;

		ammoModel[3].addBox(0F, 0F, 8F, 9, 4, 1, 0F); // Box 18
		ammoModel[3].setRotationPoint(20.2F, -21.5F, 24.2F);
		ammoModel[3].rotateAngleY = -0.38397244F;

		ammoModel[4].addBox(0F, 0F, 9F, 9, 5, 1, 0F); // Box 19
		ammoModel[4].setRotationPoint(20.2F, -22F, 24.2F);
		ammoModel[4].rotateAngleY = -0.38397244F;

		ammoModel[5].addShapeBox(0F, 0F, 0F, 6, 4, 1, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 20
		ammoModel[5].setRotationPoint(19.5F, -21.5F, 3.5F);

		ammoModel[6].addShapeBox(0F, 0F, 0F, 2, 4, 1, 0F, 0F, -1F, 0F, 0F, -1.5F, -0.5F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1.5F, -0.5F, 0F, -1F, 0F, 0F, 0F, 0F); // Box 21
		ammoModel[6].setRotationPoint(25.5F, -21.5F, 3.5F);


		slideModel = new ModelRendererTurbo[3];
		slideModel[0] = new ModelRendererTurbo(this, 36, 12, textureX, textureY); // Box 12
		slideModel[1] = new ModelRendererTurbo(this, 42, 1, textureX, textureY); // Box 49
		slideModel[2] = new ModelRendererTurbo(this, 42, 1, textureX, textureY); // Box 50

		slideModel[0].addShapeBox(0F, 0F, 0F, 8, 3, 1, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 12
		slideModel[0].setRotationPoint(20F, -21F, -4.3F);

		slideModel[1].addShapeBox(0F, 0F, 0F, 1, 2, 5, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F); // Box 49
		slideModel[1].setRotationPoint(27F, -20.5F, -8.5F);

		slideModel[2].addShapeBox(0F, 0F, 0F, 1, 2, 5, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F); // Box 50
		slideModel[2].setRotationPoint(26F, -20.5F, -8.5F);

		stockAttachPoint = new Vector3f(-13F /16F, 19.5F /16F, 0F /16F);

		gunSlideDistance = 0.8F;
		animationType = EnumAnimationType.CUSTOM;

		/* ----Start of Reload Block---- */
		/* Reload Name: Amended Side Clip */
		rotateClipHorizontal = 120F;
		rotateClipVertical = 60F;
		translateClip = new Vector3f(0.5F, 0F, 0F);
		/* ----End of Reload Block---- */

		translateAll(0F, 0F, 0F);


		flipAll();
	}
}