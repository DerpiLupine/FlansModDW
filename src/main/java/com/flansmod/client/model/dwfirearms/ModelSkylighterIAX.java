package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelSkylighterIAX extends ModelGun
{
	int textureX = 512;
	int textureY = 256;

	public ModelSkylighterIAX()
	{
		gunModel = new ModelRendererTurbo[65];
		gunModel[0] = new ModelRendererTurbo(this, 233, 3, textureX, textureY); // barrel
		gunModel[1] = new ModelRendererTurbo(this, 112, 19, textureX, textureY); // body1
		gunModel[2] = new ModelRendererTurbo(this, 147, 83, textureX, textureY); // body10
		gunModel[3] = new ModelRendererTurbo(this, 112, 51, textureX, textureY); // body11
		gunModel[4] = new ModelRendererTurbo(this, 186, 75, textureX, textureY); // body13
		gunModel[5] = new ModelRendererTurbo(this, 209, 77, textureX, textureY); // body14
		gunModel[6] = new ModelRendererTurbo(this, 112, 1, textureX, textureY); // body2
		gunModel[7] = new ModelRendererTurbo(this, 250, 34, textureX, textureY); // body3
		gunModel[8] = new ModelRendererTurbo(this, 112, 75, textureX, textureY); // body3
		gunModel[9] = new ModelRendererTurbo(this, 179, 36, textureX, textureY); // body4
		gunModel[10] = new ModelRendererTurbo(this, 112, 63, textureX, textureY); // body5
		gunModel[11] = new ModelRendererTurbo(this, 173, 63, textureX, textureY); // body6
		gunModel[12] = new ModelRendererTurbo(this, 208, 63, textureX, textureY); // body7
		gunModel[13] = new ModelRendererTurbo(this, 147, 76, textureX, textureY); // body9
		gunModel[14] = new ModelRendererTurbo(this, 40, 27, textureX, textureY); // bodyLight
		gunModel[15] = new ModelRendererTurbo(this, 40, 27, textureX, textureY); // bodyLight
		gunModel[16] = new ModelRendererTurbo(this, 1, 27, textureX, textureY); // bodyLight2
		gunModel[17] = new ModelRendererTurbo(this, 137, 169, textureX, textureY); // frontGrip1
		gunModel[18] = new ModelRendererTurbo(this, 137, 186, textureX, textureY); // frontGrip2
		gunModel[19] = new ModelRendererTurbo(this, 112, 167, textureX, textureY); // frontGrip3
		gunModel[20] = new ModelRendererTurbo(this, 112, 167, textureX, textureY); // frontGrip3
		gunModel[21] = new ModelRendererTurbo(this, 112, 185, textureX, textureY); // frontGrip4
		gunModel[22] = new ModelRendererTurbo(this, 112, 185, textureX, textureY); // frontGrip4
		gunModel[23] = new ModelRendererTurbo(this, 112, 89, textureX, textureY); // frontRail1
		gunModel[24] = new ModelRendererTurbo(this, 112, 106, textureX, textureY); // frontRail2
		gunModel[25] = new ModelRendererTurbo(this, 112, 119, textureX, textureY); // frontRail3
		gunModel[26] = new ModelRendererTurbo(this, 201, 144, textureX, textureY); // frontRail4
		gunModel[27] = new ModelRendererTurbo(this, 201, 166, textureX, textureY); // frontRail4
		gunModel[28] = new ModelRendererTurbo(this, 201, 155, textureX, textureY); // frontRail4
		gunModel[29] = new ModelRendererTurbo(this, 112, 131, textureX, textureY); // frontRail5
		gunModel[30] = new ModelRendererTurbo(this, 112, 143, textureX, textureY); // frontRail6
		gunModel[31] = new ModelRendererTurbo(this, 112, 157, textureX, textureY); // frontRail7
		gunModel[32] = new ModelRendererTurbo(this, 1, 61, textureX, textureY); // grip
		gunModel[33] = new ModelRendererTurbo(this, 1, 48, textureX, textureY); // grip1
		gunModel[34] = new ModelRendererTurbo(this, 1, 73, textureX, textureY); // grip2
		gunModel[35] = new ModelRendererTurbo(this, 65, 73, textureX, textureY); // grip3
		gunModel[36] = new ModelRendererTurbo(this, 58, 56, textureX, textureY); // grip4
		gunModel[37] = new ModelRendererTurbo(this, 40, 73, textureX, textureY); // grip5
		gunModel[38] = new ModelRendererTurbo(this, 1, 15, textureX, textureY); // railLight
		gunModel[39] = new ModelRendererTurbo(this, 1, 15, textureX, textureY); // railLight
		gunModel[40] = new ModelRendererTurbo(this, 22, 1, textureX, textureY); // railPart0
		gunModel[41] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // railPart1
		gunModel[42] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // railPart2
		gunModel[43] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // railPart3
		gunModel[44] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // railPart4
		gunModel[45] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // railPart5
		gunModel[46] = new ModelRendererTurbo(this, 68, 27, textureX, textureY); // receiverControls
		gunModel[47] = new ModelRendererTurbo(this, 1, 126, textureX, textureY); // stock
		gunModel[48] = new ModelRendererTurbo(this, 1, 104, textureX, textureY); // stock2
		gunModel[49] = new ModelRendererTurbo(this, 1, 156, textureX, textureY); // stock3
		gunModel[50] = new ModelRendererTurbo(this, 1, 139, textureX, textureY); // stock4
		gunModel[51] = new ModelRendererTurbo(this, 1, 184, textureX, textureY); // stock5
		gunModel[52] = new ModelRendererTurbo(this, 1, 201, textureX, textureY); // stock6
		gunModel[53] = new ModelRendererTurbo(this, 81, 173, textureX, textureY); // stock7
		gunModel[54] = new ModelRendererTurbo(this, 1, 214, textureX, textureY); // stock8
		gunModel[55] = new ModelRendererTurbo(this, 54, 210, textureX, textureY); // stock9
		gunModel[56] = new ModelRendererTurbo(this, 54, 210, textureX, textureY); // stock9
		gunModel[57] = new ModelRendererTurbo(this, 54, 210, textureX, textureY); // stock9
		gunModel[58] = new ModelRendererTurbo(this, 56, 162, textureX, textureY); // stockPart3
		gunModel[59] = new ModelRendererTurbo(this, 72, 141, textureX, textureY); // stockPart4
		gunModel[60] = new ModelRendererTurbo(this, 72, 141, textureX, textureY); // stockPart4-2
		gunModel[61] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 0
		gunModel[62] = new ModelRendererTurbo(this, 135, 19, textureX, textureY); // Box 72
		gunModel[63] = new ModelRendererTurbo(this, 112, 34, textureX, textureY); // Box 73
		gunModel[64] = new ModelRendererTurbo(this, 53, 17, textureX, textureY); // Box 74

		gunModel[0].addBox(0F, 0F, 0F, 1, 9, 6, 0F); // barrel
		gunModel[0].setRotationPoint(89.5F, -26.5F, -3F);

		gunModel[1].addShapeBox(0F, 0F, 0F, 1, 4, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F); // body1
		gunModel[1].setRotationPoint(-8F, -28F, -5F);

		gunModel[2].addBox(0F, 0F, 0F, 18, 4, 1, 0F); // body10
		gunModel[2].setRotationPoint(71F, -16F, 2.5F);

		gunModel[3].addBox(0F, 0F, 0F, 22, 3, 8, 0F); // body11
		gunModel[3].setRotationPoint(8F, -11F, -4F);

		gunModel[4].addBox(0F, 0F, 0F, 2, 4, 9, 0F); // body13
		gunModel[4].setRotationPoint(-19F, -27.5F, -4.5F);

		gunModel[5].addBox(0F, 0F, 0F, 1, 3, 8, 0F); // body14
		gunModel[5].setRotationPoint(-17F, -27F, -4F);

		gunModel[6].addBox(0F, 0F, 0F, 50, 7, 10, 0F); // body2
		gunModel[6].setRotationPoint(-20F, -24F, -5F);

		gunModel[7].addShapeBox(0F, 0F, 0F, 8, 6, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -7F, 0F, 0F, -7F, 0F, 0F, 0F, 0F, 0F); // body3
		gunModel[7].setRotationPoint(2F, -17F, -5F);

		gunModel[8].addBox(0F, 0F, 0F, 8, 4, 9, 0F); // body3
		gunModel[8].setRotationPoint(-16F, -27.5F, -4.5F);

		gunModel[9].addBox(0F, 0F, 0F, 27, 6, 8, 0F); // body4
		gunModel[9].setRotationPoint(3F, -17F, -4F);

		gunModel[10].addShapeBox(0F, 0F, 0F, 20, 1, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // body5
		gunModel[10].setRotationPoint(10F, -17F, -5F);

		gunModel[11].addShapeBox(0F, 0F, 0F, 7, 1, 10, 0F, 0F, -6F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -6F, 0F, 0F, 6F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 6F, -1F); // body6
		gunModel[11].setRotationPoint(3F, -17F, -5F);

		gunModel[12].addShapeBox(0F, 0F, 0F, 5, 1, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // body7
		gunModel[12].setRotationPoint(-2F, -11F, -5F);

		gunModel[13].addShapeBox(0F, 0F, 0F, 13, 5, 1, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body9
		gunModel[13].setRotationPoint(56F, -16F, -4F);

		gunModel[14].addShapeBox(0F, 0F, 0F, 7, 2, 11, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F); // bodyLight
		gunModel[14].setRotationPoint(-6F, -15F, -5.5F);

		gunModel[15].addShapeBox(0F, 0F, 0F, 7, 2, 11, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // bodyLight
		gunModel[15].setRotationPoint(-6F, -20F, -5.5F);

		gunModel[16].addBox(0F, 0F, 0F, 7, 3, 11, 0F); // bodyLight2
		gunModel[16].setRotationPoint(-6F, -18F, -5.5F);

		gunModel[17].addBox(0F, 0F, 0F, 23, 5, 8, 0F); // frontGrip1
		gunModel[17].setRotationPoint(31F, -16F, -4F);

		gunModel[18].addShapeBox(0F, 0F, 0F, 23, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // frontGrip2
		gunModel[18].setRotationPoint(31F, -11F, -4F);

		gunModel[19].addBox(0F, 0F, 0F, 1, 6, 9, 0F); // frontGrip3
		gunModel[19].setRotationPoint(30F, -16.5F, -4.5F);

		gunModel[20].addBox(0F, 0F, 0F, 1, 6, 9, 0F); // frontGrip3
		gunModel[20].setRotationPoint(54F, -16.5F, -4.5F);

		gunModel[21].addShapeBox(0F, 0F, 0F, 1, 2, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // frontGrip4
		gunModel[21].setRotationPoint(54F, -10.5F, -4.5F);

		gunModel[22].addShapeBox(0F, 0F, 0F, 1, 2, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // frontGrip4
		gunModel[22].setRotationPoint(30F, -10.5F, -4.5F);

		gunModel[23].addBox(0F, 0F, 0F, 60, 6, 8, 0F); // frontRail1
		gunModel[23].setRotationPoint(30F, -23F, -4F);

		gunModel[24].addBox(0F, 0F, 0F, 60, 2, 8, 0F); // frontRail2
		gunModel[24].setRotationPoint(30F, -27F, -4F);

		gunModel[25].addShapeBox(0F, 0F, 0F, 60, 1, 8, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // frontRail3
		gunModel[25].setRotationPoint(30F, -28F, -4F);

		gunModel[26].addBox(0F, 0F, 0F, 8, 2, 8, 0F); // frontRail4
		gunModel[26].setRotationPoint(30F, -25F, -4F);

		gunModel[27].addBox(0F, 0F, 0F, 8, 2, 8, 0F); // frontRail4
		gunModel[27].setRotationPoint(82F, -25F, -4F);

		gunModel[28].addBox(0F, 0F, 0F, 8, 2, 8, 0F); // frontRail4
		gunModel[28].setRotationPoint(56F, -25F, -4F);

		gunModel[29].addShapeBox(0F, 0F, 0F, 60, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // frontRail5
		gunModel[29].setRotationPoint(30F, -17F, -4F);

		gunModel[30].addBox(0F, 0F, 0F, 35, 5, 6, 0F); // frontRail6
		gunModel[30].setRotationPoint(55F, -16F, -3F);

		gunModel[31].addShapeBox(0F, 0F, 0F, 35, 1, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // frontRail7
		gunModel[31].setRotationPoint(55F, -11F, -3F);

		gunModel[32].addBox(0F, 0F, 0F, 18, 1, 10, 0F); // grip
		gunModel[32].setRotationPoint(-20F, -11F, -5F);

		gunModel[33].addShapeBox(0F, 0F, 0F, 16, 2, 10, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // grip1
		gunModel[33].setRotationPoint(-18F, -10F, -5F);

		gunModel[34].addShapeBox(0F, 0F, 0F, 9, 14, 10, 0F, -7F, 0F, 0F, 7F, 0F, 0F, 7F, 0F, 0F, -7F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // grip2
		gunModel[34].setRotationPoint(-22F, -8F, -5F);

		gunModel[35].addShapeBox(0F, 0F, 0F, 3, 14, 10, 0F, -7F, 0F, 0F, 7F, 0F, -2F, 7F, 0F, -2F, -7F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F); // grip3
		gunModel[35].setRotationPoint(-13F, -8F, -5F);

		gunModel[36].addShapeBox(0F, 0F, 0F, 3, 6, 10, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, -2F, -3F, 0F, -2F, 3F, 0F, 0F); // grip4
		gunModel[36].setRotationPoint(-13F, 6F, -5F);

		gunModel[37].addShapeBox(0F, 0F, 0F, 2, 14, 10, 0F, -7F, 0F, -1F, 7F, 0F, 0F, 7F, 0F, 0F, -7F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // grip5
		gunModel[37].setRotationPoint(-24F, -8F, -5F);

		gunModel[38].addBox(0F, 0F, 0F, 18, 2, 7, 0F); // railLight
		gunModel[38].setRotationPoint(38F, -25F, -3.5F);

		gunModel[39].addBox(0F, 0F, 0F, 18, 2, 7, 0F); // railLight
		gunModel[39].setRotationPoint(64F, -25F, -3.5F);

		gunModel[40].addBox(0F, 0F, 0F, 33, 2, 7, 0F); // railPart0
		gunModel[40].setRotationPoint(-3F, -30F, -3.5F);

		gunModel[41].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // railPart1
		gunModel[41].setRotationPoint(-3F, -32F, -3.5F);

		gunModel[42].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // railPart2
		gunModel[42].setRotationPoint(3F, -32F, -3.5F);

		gunModel[43].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // railPart3
		gunModel[43].setRotationPoint(9F, -32F, -3.5F);

		gunModel[44].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // railPart4
		gunModel[44].setRotationPoint(15F, -32F, -3.5F);

		gunModel[45].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // railPart5
		gunModel[45].setRotationPoint(21F, -32F, -3.5F);

		gunModel[46].addBox(0F, 0F, 0F, 18, 5, 1, 0F); // receiverControls
		gunModel[46].setRotationPoint(2F, -23F, 4.5F);

		gunModel[47].addShapeBox(0F, 0F, 0F, 35, 2, 10, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // stock
		gunModel[47].setRotationPoint(-55F, -24F, -5F);

		gunModel[48].addBox(0F, 0F, 0F, 35, 11, 10, 0F); // stock2
		gunModel[48].setRotationPoint(-55F, -22F, -5F);

		gunModel[49].addBox(0F, 0F, 0F, 16, 17, 10, 0F); // stock3
		gunModel[49].setRotationPoint(-55F, -11F, -5F);

		gunModel[50].addShapeBox(0F, 0F, 0F, 25, 6, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -5F, 0F); // stock4
		gunModel[50].setRotationPoint(-55F, 6F, -5F);

		gunModel[51].addShapeBox(0F, 0F, 0F, 17, 6, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F); // stock5
		gunModel[51].setRotationPoint(-30F, 6F, -5F);

		gunModel[52].addShapeBox(0F, 0F, 0F, 20, 2, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // stock6
		gunModel[52].setRotationPoint(-39F, -11F, -5F);

		gunModel[53].addShapeBox(0F, 0F, 0F, 2, 17, 10, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // stock7
		gunModel[53].setRotationPoint(-39F, -11F, -5F);

		gunModel[54].addShapeBox(0F, 0F, 0F, 16, 2, 10, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // stock8
		gunModel[54].setRotationPoint(-39F, 4F, -5F);

		gunModel[55].addShapeBox(0F, 0F, 0F, 5, 6, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -4F, -1F, 0F, -4F, -1F, 0F, 0F, -1F); // stock9
		gunModel[55].setRotationPoint(-39F, -11F, -5F);

		gunModel[56].addShapeBox(0F, 0F, 0F, 5, 6, 10, 0F, 0F, -4F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, -4F, -1F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F); // stock9
		gunModel[56].setRotationPoint(-25F, 0F, -5F);

		gunModel[57].addShapeBox(0F, 0F, 0F, 5, 6, 10, 0F, 0F, 0F, -1F, 0F, -4F, -1F, 0F, -4F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // stock9
		gunModel[57].setRotationPoint(-39F, 0F, -5F);

		gunModel[58].addBox(0F, 0F, 0F, 2, 28, 10, 0F); // stockPart3
		gunModel[58].setRotationPoint(-57F, -22F, -5F);

		gunModel[59].addBox(0F, 0F, 0F, 3, 3, 11, 0F); // stockPart4
		gunModel[59].setRotationPoint(-53.5F, -11F, -5.5F);

		gunModel[60].addBox(0F, 0F, 0F, 3, 3, 11, 0F); // stockPart4-2
		gunModel[60].setRotationPoint(-53.5F, -20F, -5.5F);

		gunModel[61].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 0
		gunModel[61].setRotationPoint(27F, -32F, -3.5F);

		gunModel[62].addBox(0F, 0F, 0F, 37, 4, 10, 0F); // Box 72
		gunModel[62].setRotationPoint(-7F, -28F, -5F);

		gunModel[63].addBox(0F, 0F, 0F, 22, 6, 10, 0F); // Box 73
		gunModel[63].setRotationPoint(-20F, -17F, -5F);

		gunModel[64].addShapeBox(0F, 0F, 0F, 9, 6, 1, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, -3F, -3F, 0F, -3F, -3F, 0F, 0F, -3F, 0F); // Box 74
		gunModel[64].setRotationPoint(13F, -22F, 4.7F);


		ammoModel = new ModelRendererTurbo[10];
		ammoModel[0] = new ModelRendererTurbo(this, 177, 221, textureX, textureY); // clip4
		ammoModel[1] = new ModelRendererTurbo(this, 112, 208, textureX, textureY); // clip1
		ammoModel[2] = new ModelRendererTurbo(this, 164, 209, textureX, textureY); // clip3
		ammoModel[3] = new ModelRendererTurbo(this, 147, 208, textureX, textureY); // clip2
		ammoModel[4] = new ModelRendererTurbo(this, 164, 209, textureX, textureY); // clip3
		ammoModel[5] = new ModelRendererTurbo(this, 147, 208, textureX, textureY); // clip2
		ammoModel[6] = new ModelRendererTurbo(this, 147, 208, textureX, textureY); // clip2
		ammoModel[7] = new ModelRendererTurbo(this, 164, 209, textureX, textureY); // clip3
		ammoModel[8] = new ModelRendererTurbo(this, 177, 215, textureX, textureY); // Box 25
		ammoModel[9] = new ModelRendererTurbo(this, 177, 221, textureX, textureY); // clip4

		ammoModel[0].addShapeBox(0F, 0F, 0F, 20, 1, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // clip4
		ammoModel[0].setRotationPoint(9F, 0F, -3F);

		ammoModel[1].addBox(0F, 0F, 0F, 11, 14, 6, 0F); // clip1
		ammoModel[1].setRotationPoint(9F, -15F, -3F);

		ammoModel[2].addBox(0F, 0F, 0F, 1, 14, 5, 0F); // clip3
		ammoModel[2].setRotationPoint(20F, -15F, -2.5F);

		ammoModel[3].addBox(0F, 0F, 0F, 2, 14, 6, 0F); // clip2
		ammoModel[3].setRotationPoint(21F, -15F, -3F);

		ammoModel[4].addBox(0F, 0F, 0F, 1, 14, 5, 0F); // clip3
		ammoModel[4].setRotationPoint(23F, -15F, -2.5F);

		ammoModel[5].addBox(0F, 0F, 0F, 2, 14, 6, 0F); // clip2
		ammoModel[5].setRotationPoint(24F, -15F, -3F);

		ammoModel[6].addBox(0F, 0F, 0F, 2, 14, 6, 0F); // clip2
		ammoModel[6].setRotationPoint(27F, -15F, -3F);

		ammoModel[7].addBox(0F, 0F, 0F, 1, 14, 5, 0F); // clip3
		ammoModel[7].setRotationPoint(26F, -15F, -2.5F);

		ammoModel[8].addBox(0F, 0F, 0F, 18, 1, 4, 0F); // Box 25
		ammoModel[8].setRotationPoint(10F, -15.5F, -2F);

		ammoModel[9].addBox(0F, 0F, 0F, 20, 1, 6, 0F); // clip4
		ammoModel[9].setRotationPoint(9F, -1F, -3F);

		scopeAttachPoint = new Vector3f(15F /16F, 21F /16F, 0F /16F);

		gunSlideDistance = 0.8F;
		animationType = EnumAnimationType.CUSTOM;

		/* ----Start of Reload Block---- */
		/* Reload Name: DropMag Pistol Clip */
		rotateGunHorizontal = 60F;
		translateGun = new Vector3f(0.15F, 0.25F, 0F);

		translateClip = new Vector3f(0F, -4F, 0F);
		/* ----End of Reload Block---- */

		translateAll(0F, 0F, 0F);

		flipAll();
	}
}