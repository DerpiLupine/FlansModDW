package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelSF370B extends ModelGun
{
	int textureX = 512;
	int textureY = 256;

	public ModelSF370B()
	{
		gunModel = new ModelRendererTurbo[50];
		gunModel[0] = new ModelRendererTurbo(this, 72, 29, textureX, textureY); // body1
		gunModel[1] = new ModelRendererTurbo(this, 72, 50, textureX, textureY); // body10
		gunModel[2] = new ModelRendererTurbo(this, 105, 130, textureX, textureY); // body11
		gunModel[3] = new ModelRendererTurbo(this, 105, 136, textureX, textureY); // body12
		gunModel[4] = new ModelRendererTurbo(this, 72, 85, textureX, textureY); // body2
		gunModel[5] = new ModelRendererTurbo(this, 72, 100, textureX, textureY); // body3
		gunModel[6] = new ModelRendererTurbo(this, 72, 116, textureX, textureY); // body5
		gunModel[7] = new ModelRendererTurbo(this, 72, 128, textureX, textureY); // body6
		gunModel[8] = new ModelRendererTurbo(this, 72, 72, textureX, textureY); // body8
		gunModel[9] = new ModelRendererTurbo(this, 72, 61, textureX, textureY); // body9
		gunModel[10] = new ModelRendererTurbo(this, 157, 29, textureX, textureY); // muzzle1
		gunModel[11] = new ModelRendererTurbo(this, 157, 39, textureX, textureY); // muzzle2
		gunModel[12] = new ModelRendererTurbo(this, 157, 29, textureX, textureY); // muzzle1-2
		gunModel[13] = new ModelRendererTurbo(this, 72, 1, textureX, textureY); // gasBlock1
		gunModel[14] = new ModelRendererTurbo(this, 72, 10, textureX, textureY); // gasBlock2
		gunModel[15] = new ModelRendererTurbo(this, 72, 19, textureX, textureY); // gasBlock2-2
		gunModel[16] = new ModelRendererTurbo(this, 1, 81, textureX, textureY); // grip1
		gunModel[17] = new ModelRendererTurbo(this, 2, 54, textureX, textureY); // grip2
		gunModel[18] = new ModelRendererTurbo(this, 38, 33, textureX, textureY); // rail1
		gunModel[19] = new ModelRendererTurbo(this, 38, 33, textureX, textureY); // rail2
		gunModel[20] = new ModelRendererTurbo(this, 38, 33, textureX, textureY); // rail3
		gunModel[21] = new ModelRendererTurbo(this, 38, 33, textureX, textureY); // rail4
		gunModel[22] = new ModelRendererTurbo(this, 1, 23, textureX, textureY); // railBase
		gunModel[23] = new ModelRendererTurbo(this, 1, 13, textureX, textureY); // Box 18
		gunModel[24] = new ModelRendererTurbo(this, 6, 17, textureX, textureY); // Box 19
		gunModel[25] = new ModelRendererTurbo(this, 6, 17, textureX, textureY); // Box 22
		gunModel[26] = new ModelRendererTurbo(this, 6, 17, textureX, textureY); // Box 23
		gunModel[27] = new ModelRendererTurbo(this, 157, 29, textureX, textureY); // Box 57
		gunModel[28] = new ModelRendererTurbo(this, 157, 39, textureX, textureY); // Box 58
		gunModel[29] = new ModelRendererTurbo(this, 157, 29, textureX, textureY); // Box 59
		gunModel[30] = new ModelRendererTurbo(this, 157, 29, textureX, textureY); // Box 60
		gunModel[31] = new ModelRendererTurbo(this, 157, 39, textureX, textureY); // Box 61
		gunModel[32] = new ModelRendererTurbo(this, 157, 29, textureX, textureY); // Box 62
		gunModel[33] = new ModelRendererTurbo(this, 2, 34, textureX, textureY); // Box 0
		gunModel[34] = new ModelRendererTurbo(this, 2, 68, textureX, textureY); // Box 1
		gunModel[35] = new ModelRendererTurbo(this, 173, 80, textureX, textureY); // Box 6
		gunModel[36] = new ModelRendererTurbo(this, 143, 72, textureX, textureY); // Box 115
		gunModel[37] = new ModelRendererTurbo(this, 143, 94, textureX, textureY); // Box 116
		gunModel[38] = new ModelRendererTurbo(this, 173, 73, textureX, textureY); // Box 117
		gunModel[39] = new ModelRendererTurbo(this, 152, 87, textureX, textureY); // Box 119
		gunModel[40] = new ModelRendererTurbo(this, 152, 94, textureX, textureY); // Box 120
		gunModel[41] = new ModelRendererTurbo(this, 152, 72, textureX, textureY); // Box 121
		gunModel[42] = new ModelRendererTurbo(this, 152, 80, textureX, textureY); // Box 122
		gunModel[43] = new ModelRendererTurbo(this, 143, 94, textureX, textureY); // Box 123
		gunModel[44] = new ModelRendererTurbo(this, 143, 90, textureX, textureY); // Box 124
		gunModel[45] = new ModelRendererTurbo(this, 143, 72, textureX, textureY); // Box 125
		gunModel[46] = new ModelRendererTurbo(this, 152, 94, textureX, textureY); // Box 126
		gunModel[47] = new ModelRendererTurbo(this, 152, 87, textureX, textureY); // Box 127
		gunModel[48] = new ModelRendererTurbo(this, 152, 72, textureX, textureY); // Box 128
		gunModel[49] = new ModelRendererTurbo(this, 152, 80, textureX, textureY); // Box 129

		gunModel[0].addBox(0F, 0F, 0F, 25, 10, 10, 0F); // body1
		gunModel[0].setRotationPoint(-10F, -16F, -5F);

		gunModel[1].addShapeBox(0F, 0F, 0F, 43, 2, 8, 0F, -1F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, -1F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body10
		gunModel[1].setRotationPoint(-8F, -22F, -4F);

		gunModel[2].addShapeBox(0F, 0F, 0F, 3, 2, 3, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body11
		gunModel[2].setRotationPoint(-9F, -21F, -1.5F);

		gunModel[3].addBox(0F, 0F, 0F, 12, 3, 1, 0F); // body12
		gunModel[3].setRotationPoint(8F, -15F, 4.5F);

		gunModel[4].addBox(0F, 0F, 0F, 20, 5, 9, 0F); // body2
		gunModel[4].setRotationPoint(15F, -11F, -4.5F);

		gunModel[5].addBox(0F, 0F, 0F, 20, 5, 10, 0F); // body3
		gunModel[5].setRotationPoint(15F, -16F, -5F);

		gunModel[6].addShapeBox(0F, 0F, 0F, 14, 2, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body5
		gunModel[6].setRotationPoint(15F, -18F, -4F);

		gunModel[7].addShapeBox(0F, 0F, 0F, 6, 2, 10, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body6
		gunModel[7].setRotationPoint(29F, -18F, -5F);

		gunModel[8].addShapeBox(0F, 0F, 0F, 25, 2, 10, 0F, -1F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, -1F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body8
		gunModel[8].setRotationPoint(-10F, -18F, -5F);

		gunModel[9].addShapeBox(0F, 0F, 0F, 44, 2, 8, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body9
		gunModel[9].setRotationPoint(-9F, -20F, -4F);

		gunModel[10].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // muzzle1
		gunModel[10].setRotationPoint(78F, -20.5F, -3.5F);

		gunModel[11].addBox(0F, 0F, 0F, 3, 3, 7, 0F); // muzzle2
		gunModel[11].setRotationPoint(78F, -18.5F, -3.5F);

		gunModel[12].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // muzzle1-2
		gunModel[12].setRotationPoint(78F, -15.5F, -3.5F);

		gunModel[13].addShapeBox(0F, 0F, 0F, 60, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // gasBlock1
		gunModel[13].setRotationPoint(35F, -20F, -3F);

		gunModel[14].addBox(0F, 0F, 0F, 60, 2, 6, 0F); // gasBlock2
		gunModel[14].setRotationPoint(35F, -18F, -3F);

		gunModel[15].addShapeBox(0F, 0F, 0F, 60, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // gasBlock2-2
		gunModel[15].setRotationPoint(35F, -16F, -3F);

		gunModel[16].addShapeBox(0F, 0F, 0F, 13, 3, 9, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F); // grip1
		gunModel[16].setRotationPoint(-8F, -6F, -4.5F);

		gunModel[17].addShapeBox(0F, 0F, 0F, 12, 4, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F); // grip2
		gunModel[17].setRotationPoint(-7F, -3F, -4.5F);

		gunModel[18].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // rail1
		gunModel[18].setRotationPoint(22F, -25F, -3.5F);

		gunModel[19].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // rail2
		gunModel[19].setRotationPoint(28F, -25F, -3.5F);

		gunModel[20].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // rail3
		gunModel[20].setRotationPoint(34F, -25F, -3.5F);

		gunModel[21].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // rail4
		gunModel[21].setRotationPoint(40F, -25F, -3.5F);

		gunModel[22].addBox(0F, 0F, 0F, 28, 2, 7, 0F); // railBase
		gunModel[22].setRotationPoint(22F, -23F, -3.5F);

		gunModel[23].addBox(0F, 0F, 0F, 1, 7, 1, 0F); // Box 18
		gunModel[23].setRotationPoint(15F, -23F, -7F);
		gunModel[23].rotateAngleX = 0.34906585F;

		gunModel[24].addShapeBox(0F, -1.5F, -1.5F, 12, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 19
		gunModel[24].setRotationPoint(4.1F, -24.5F, -6.8F);
		gunModel[24].rotateAngleX = 0.34906585F;

		gunModel[25].addBox(0F, -0.5F, -1.5F, 12, 1, 3, 0F); // Box 22
		gunModel[25].setRotationPoint(4.1F, -24.5F, -6.8F);
		gunModel[25].rotateAngleX = 0.34906585F;

		gunModel[26].addShapeBox(0F, 0.5F, -1.5F, 12, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 23
		gunModel[26].setRotationPoint(4.1F, -24.5F, -6.8F);
		gunModel[26].rotateAngleX = 0.34906585F;

		gunModel[27].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 57
		gunModel[27].setRotationPoint(84F, -20.5F, -3.5F);

		gunModel[28].addBox(0F, 0F, 0F, 3, 3, 7, 0F); // Box 58
		gunModel[28].setRotationPoint(84F, -18.5F, -3.5F);

		gunModel[29].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 59
		gunModel[29].setRotationPoint(84F, -15.5F, -3.5F);

		gunModel[30].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 60
		gunModel[30].setRotationPoint(90F, -20.5F, -3.5F);

		gunModel[31].addBox(0F, 0F, 0F, 3, 3, 7, 0F); // Box 61
		gunModel[31].setRotationPoint(90F, -18.5F, -3.5F);

		gunModel[32].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 62
		gunModel[32].setRotationPoint(90F, -15.5F, -3.5F);

		gunModel[33].addShapeBox(0F, 0F, 0F, 12, 10, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, -2F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 3F, -2F, 0F); // Box 0
		gunModel[33].setRotationPoint(-9F, 1F, -4.5F);

		gunModel[34].addShapeBox(0F, 0F, 0F, 12, 3, 9, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F); // Box 1
		gunModel[34].setRotationPoint(-12F, 11F, -4.5F);

		gunModel[35].addBox(0F, 0F, 0F, 3, 1, 3, 0F); // Box 6
		gunModel[35].setRotationPoint(59.5F, -8F, -1.5F);

		gunModel[36].addShapeBox(0F, 0F, 0F, 2, 15, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 5F, 0F, 0F, 5F, 0F, 0F, -5F, 0F, 0F, -5F); // Box 115
		gunModel[36].setRotationPoint(60F, -5.5F, -2.5F);

		gunModel[37].addShapeBox(0F, 0F, 0F, 2, 4, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 1.5F, 0F, 0F, 1.5F, 1F, 0F, -1.5F, 1F, 0F, -1.5F); // Box 116
		gunModel[37].setRotationPoint(60F, -5.5F, -3.5F);

		gunModel[38].addBox(0F, 0F, 0F, 5, 3, 3, 0F); // Box 117
		gunModel[38].setRotationPoint(58.5F, -7F, -1.5F);

		gunModel[39].addShapeBox(0F, 0F, 0F, 2, 4, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 1.5F, 0F, 0F, 1.5F, 1F, 0F, -1.5F, 1F, 0F, -1.5F); // Box 119
		gunModel[39].setRotationPoint(60F, 5.5F, -7F);

		gunModel[40].addShapeBox(0F, 0F, 0F, 2, 4, 2, 0F, 1F, 0F, -1.5F, 1F, 0F, -1.5F, 0F, 0F, 1.5F, 0F, 0F, 1.5F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 120
		gunModel[40].setRotationPoint(60F, 5.5F, -6.5F);

		gunModel[41].addBox(0F, 0F, 0F, 5, 2, 5, 0F); // Box 121
		gunModel[41].setRotationPoint(58.5F, 9.5F, -9F);

		gunModel[42].addShapeBox(0F, 0F, 0F, 5, 1, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F); // Box 122
		gunModel[42].setRotationPoint(58.5F, 11.5F, -9F);

		gunModel[43].addShapeBox(0F, 0F, 0F, 2, 4, 2, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, -1.5F, 1F, 0F, -1.5F, 0F, 0F, 1.5F, 0F, 0F, 1.5F); // Box 123
		gunModel[43].setRotationPoint(60F, -5.5F, 1.5F);

		gunModel[44].addShapeBox(0F, 0F, 0F, 2, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 124
		gunModel[44].setRotationPoint(60F, -6.5F, 1.5F);

		gunModel[45].addShapeBox(0F, 0F, 0F, 2, 15, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -5F, 0F, 0F, -5F, 0F, 0F, 5F, 0F, 0F, 5F); // Box 125
		gunModel[45].setRotationPoint(60F, -5.5F, 0.5F);

		gunModel[46].addShapeBox(0F, 0F, 0F, 2, 4, 2, 0F, 0F, 0F, 1.5F, 0F, 0F, 1.5F, 1F, 0F, -1.5F, 1F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F); // Box 126
		gunModel[46].setRotationPoint(60F, 5.5F, 4.5F);

		gunModel[47].addShapeBox(0F, 0F, 0F, 2, 4, 2, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, -1.5F, 1F, 0F, -1.5F, 0F, 0F, 1.5F, 0F, 0F, 1.5F); // Box 127
		gunModel[47].setRotationPoint(60F, 5.5F, 5F);

		gunModel[48].addBox(0F, 0F, 0F, 5, 2, 5, 0F); // Box 128
		gunModel[48].setRotationPoint(58.5F, 9.5F, 4F);

		gunModel[49].addShapeBox(0F, 0F, 0F, 5, 1, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F); // Box 129
		gunModel[49].setRotationPoint(58.5F, 11.5F, 4F);


		defaultScopeModel = new ModelRendererTurbo[3];
		defaultScopeModel[0] = new ModelRendererTurbo(this, 32, 7, textureX, textureY); // ironSight1
		defaultScopeModel[1] = new ModelRendererTurbo(this, 32, 7, textureX, textureY); // ironSight1-2
		defaultScopeModel[2] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // ironsightRearBase

		defaultScopeModel[0].addBox(0F, 0F, 0F, 6, 1, 3, 0F); // ironSight1
		defaultScopeModel[0].setRotationPoint(44F, -26F, 1F);

		defaultScopeModel[1].addBox(0F, 0F, 0F, 6, 1, 3, 0F); // ironSight1-2
		defaultScopeModel[1].setRotationPoint(44F, -26F, -4F);

		defaultScopeModel[2].addBox(0F, 0F, 0F, 6, 3, 8, 0F); // ironsightRearBase
		defaultScopeModel[2].setRotationPoint(44F, -25F, -4F);


		defaultStockModel = new ModelRendererTurbo[19];
		defaultStockModel[0] = new ModelRendererTurbo(this, 1, 117, textureX, textureY); // Box 0
		defaultStockModel[1] = new ModelRendererTurbo(this, 51, 119, textureX, textureY); // Box 52
		defaultStockModel[2] = new ModelRendererTurbo(this, 58, 196, textureX, textureY); // Box 53
		defaultStockModel[3] = new ModelRendererTurbo(this, 58, 212, textureX, textureY); // Box 54
		defaultStockModel[4] = new ModelRendererTurbo(this, 51, 144, textureX, textureY); // Box 55
		defaultStockModel[5] = new ModelRendererTurbo(this, 60, 157, textureX, textureY); // Box 56
		defaultStockModel[6] = new ModelRendererTurbo(this, 51, 134, textureX, textureY); // Box 57
		defaultStockModel[7] = new ModelRendererTurbo(this, 24, 134, textureX, textureY); // Box 59
		defaultStockModel[8] = new ModelRendererTurbo(this, 24, 150, textureX, textureY); // Box 60
		defaultStockModel[9] = new ModelRendererTurbo(this, 1, 155, textureX, textureY); // Box 61
		defaultStockModel[10] = new ModelRendererTurbo(this, 1, 101, textureX, textureY); // Box 62
		defaultStockModel[11] = new ModelRendererTurbo(this, 1, 184, textureX, textureY); // Box 64
		defaultStockModel[12] = new ModelRendererTurbo(this, 1, 145, textureX, textureY); // Box 65
		defaultStockModel[13] = new ModelRendererTurbo(this, 1, 171, textureX, textureY); // Box 66
		defaultStockModel[14] = new ModelRendererTurbo(this, 1, 194, textureX, textureY); // Box 67
		defaultStockModel[15] = new ModelRendererTurbo(this, 1, 211, textureX, textureY); // Box 68
		defaultStockModel[16] = new ModelRendererTurbo(this, 24, 160, textureX, textureY); // Box 0
		defaultStockModel[17] = new ModelRendererTurbo(this, 24, 117, textureX, textureY); // Box 1
		defaultStockModel[18] = new ModelRendererTurbo(this, 60, 168, textureX, textureY); // Box 2

		defaultStockModel[0].addBox(0F, 0F, 0F, 3, 19, 8, 0F); // Box 0
		defaultStockModel[0].setRotationPoint(-52F, -15F, -4F);

		defaultStockModel[1].addBox(0F, 0F, 0F, 1, 7, 7, 0F); // Box 52
		defaultStockModel[1].setRotationPoint(-11F, -15F, -3.5F);

		defaultStockModel[2].addShapeBox(0F, 0F, 0F, 10, 7, 8, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, 4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F); // Box 53
		defaultStockModel[2].setRotationPoint(-21F, -15F, -4F);

		defaultStockModel[3].addShapeBox(0F, 0F, 0F, 10, 1, 8, 0F, 0F, -4F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, -4F, -1F, 0F, 4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F); // Box 54
		defaultStockModel[3].setRotationPoint(-21F, -16F, -4F);

		defaultStockModel[4].addShapeBox(0F, 0F, 0F, 1, 1, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 55
		defaultStockModel[4].setRotationPoint(-11F, -16F, -3.5F);

		defaultStockModel[5].addShapeBox(0F, 0F, 0F, 10, 2, 8, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -4F, -1F, 0F, -4F, -1F, 0F, 0F, -1F); // Box 56
		defaultStockModel[5].setRotationPoint(-21F, -4F, -4F);

		defaultStockModel[6].addShapeBox(0F, 0F, 0F, 1, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 57
		defaultStockModel[6].setRotationPoint(-11F, -8F, -3.5F);

		defaultStockModel[7].addShapeBox(0F, 0F, 0F, 3, 7, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F); // Box 59
		defaultStockModel[7].setRotationPoint(-24F, -11F, -4F);

		defaultStockModel[8].addShapeBox(0F, 0F, 0F, 3, 1, 8, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 60
		defaultStockModel[8].setRotationPoint(-24F, -12F, -4F);

		defaultStockModel[9].addShapeBox(0F, 0F, 0F, 3, 1, 8, 0F, 0F, 0F, -1F, 0F, -4F, -1F, 0F, -4F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, 0F, 0F); // Box 61
		defaultStockModel[9].setRotationPoint(-27F, -16F, -4F);

		defaultStockModel[10].addShapeBox(0F, 0F, 0F, 25, 7, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 62
		defaultStockModel[10].setRotationPoint(-49F, -11F, -4F);

		defaultStockModel[11].addShapeBox(0F, 0F, 0F, 22, 1, 8, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 64
		defaultStockModel[11].setRotationPoint(-49F, -16F, -4F);

		defaultStockModel[12].addShapeBox(0F, 0F, 0F, 3, 1, 8, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 65
		defaultStockModel[12].setRotationPoint(-52F, -16F, -4F);

		defaultStockModel[13].addShapeBox(0F, 0F, 0F, 25, 4, 8, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 66
		defaultStockModel[13].setRotationPoint(-49F, -15F, -4F);

		defaultStockModel[14].addShapeBox(0F, 0F, 0F, 20, 8, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -7F, 0F, 0F, -7F, 0F, 0F, 0F, 0F); // Box 67
		defaultStockModel[14].setRotationPoint(-44F, -4F, -4F);

		defaultStockModel[15].addShapeBox(0F, 0F, 0F, 20, 2, 8, 0F, 0F, 0F, 0F, 0F, 7F, 0F, 0F, 7F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -7F, -1F, 0F, -7F, -1F, 0F, 0F, -1F); // Box 68
		defaultStockModel[15].setRotationPoint(-44F, 4F, -4F);

		defaultStockModel[16].addShapeBox(0F, 0F, 0F, 3, 2, 8, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, 0F, -1F); // Box 0
		defaultStockModel[16].setRotationPoint(-24F, -3F, -4F);

		defaultStockModel[17].addBox(0F, 0F, 0F, 5, 8, 8, 0F); // Box 1
		defaultStockModel[17].setRotationPoint(-49F, -4F, -4F);

		defaultStockModel[18].addShapeBox(0F, 0F, 0F, 8, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 2
		defaultStockModel[18].setRotationPoint(-52F, 4F, -4F);


		defaultGripModel = new ModelRendererTurbo[5];
		defaultGripModel[0] = new ModelRendererTurbo(this, 192, 32, textureX, textureY); // handGuard1
		defaultGripModel[1] = new ModelRendererTurbo(this, 192, 32, textureX, textureY); // handGuard1-2
		defaultGripModel[2] = new ModelRendererTurbo(this, 192, 32, textureX, textureY); // handGuard1-3
		defaultGripModel[3] = new ModelRendererTurbo(this, 205, 1, textureX, textureY); // handGuard2
		defaultGripModel[4] = new ModelRendererTurbo(this, 205, 14, textureX, textureY); // handGuard3

		defaultGripModel[0].addShapeBox(0F, 0F, 0F, 6, 1, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // handGuard1
		defaultGripModel[0].setRotationPoint(35F, -14.5F, -3.5F);

		defaultGripModel[1].addShapeBox(0F, 0F, 0F, 6, 1, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // handGuard1-2
		defaultGripModel[1].setRotationPoint(50F, -14.5F, -3.5F);

		defaultGripModel[2].addShapeBox(0F, 0F, 0F, 6, 1, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // handGuard1-3
		defaultGripModel[2].setRotationPoint(64F, -14.5F, -3.5F);

		defaultGripModel[3].addShapeBox(0F, 0F, 0F, 35, 5, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -5F, 0F, 0F, -5F, 0F, 0F, 0F, 0F, 0F); // handGuard2
		defaultGripModel[3].setRotationPoint(35F, -13.5F, -3.5F);

		defaultGripModel[4].addShapeBox(0F, 0F, 0F, 30, 1, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F, 0F, 0F, -1F); // handGuard3
		defaultGripModel[4].setRotationPoint(35F, -8.5F, -3.5F);


		ammoModel = new ModelRendererTurbo[6];
		ammoModel[0] = new ModelRendererTurbo(this, 96, 191, textureX, textureY); // box1
		ammoModel[1] = new ModelRendererTurbo(this, 96, 144, textureX, textureY); // box2
		ammoModel[2] = new ModelRendererTurbo(this, 184, 154, textureX, textureY); // bulletTip
		ammoModel[3] = new ModelRendererTurbo(this, 96, 191, textureX, textureY); // box1-2
		ammoModel[4] = new ModelRendererTurbo(this, 155, 154, textureX, textureY); // bullet
		ammoModel[5] = new ModelRendererTurbo(this, 155, 144, textureX, textureY); // clip1

		ammoModel[0].addShapeBox(0F, 0F, 0F, 14, 2, 30, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // box1
		ammoModel[0].setRotationPoint(20F, -5F, -10F);

		ammoModel[1].addBox(0F, 0F, 0F, 14, 16, 30, 0F); // box2
		ammoModel[1].setRotationPoint(20F, -3F, -10F);

		ammoModel[2].addShapeBox(0F, 0F, 0F, 3, 1, 5, 0F, 0F, 0F, -1.5F, 0F, -0.5F, -2F, 0F, -0.5F, -2F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // bulletTip
		ammoModel[2].setRotationPoint(30F, -7F, -2.5F);

		ammoModel[3].addShapeBox(0F, 0F, 0F, 14, 2, 30, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // box1-2
		ammoModel[3].setRotationPoint(20F, 13F, -10F);

		ammoModel[4].addShapeBox(0F, 0F, 0F, 9, 1, 5, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // bullet
		ammoModel[4].setRotationPoint(21F, -7F, -2.5F);

		ammoModel[5].addBox(0F, 0F, 0F, 14, 1, 8, 0F); // clip1
		ammoModel[5].setRotationPoint(20F, -6F, -4F);


		slideModel = new ModelRendererTurbo[1];
		slideModel[0] = new ModelRendererTurbo(this, 22, 2, textureX, textureY); // bolt1

		slideModel[0].addBox(0F, 0F, 0F, 2, 2, 4, 0F); // bolt1
		slideModel[0].setRotationPoint(27F, -18F, -8F);
		

		barrelAttachPoint = new Vector3f(95F /16F, 17F /16F, 0F /16F);
		stockAttachPoint = new Vector3f(-10F /16F, 11F /16F, 0F /16F);
		scopeAttachPoint = new Vector3f(34F /16F, 22F /16F, 0F /16F);

		gunSlideDistance = 0.5F;
		animationType = EnumAnimationType.BOTTOM_CLIP;


		translateAll(0F, 0F, 0F);


		flipAll();
	}
}