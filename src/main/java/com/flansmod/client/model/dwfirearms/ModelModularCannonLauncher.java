package com.flansmod.client.model.dwfirearms; //Path where the model is located

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelModularCannonLauncher extends ModelGun //Same as Filename
{
	int textureX = 512;
	int textureY = 256;

	public ModelModularCannonLauncher() //Same as Filename
	{
		gunModel = new ModelRendererTurbo[32];
		gunModel[0] = new ModelRendererTurbo(this, 80, 82, textureX, textureY); // Box 0
		gunModel[1] = new ModelRendererTurbo(this, 80, 116, textureX, textureY); // Box 1
		gunModel[2] = new ModelRendererTurbo(this, 80, 60, textureX, textureY); // Box 2
		gunModel[3] = new ModelRendererTurbo(this, 80, 1, textureX, textureY); // Box 9
		gunModel[4] = new ModelRendererTurbo(this, 80, 1, textureX, textureY); // Box 11
		gunModel[5] = new ModelRendererTurbo(this, 80, 1, textureX, textureY); // Box 13
		gunModel[6] = new ModelRendererTurbo(this, 80, 94, textureX, textureY); // Box 23
		gunModel[7] = new ModelRendererTurbo(this, 107, 94, textureX, textureY); // Box 24
		gunModel[8] = new ModelRendererTurbo(this, 1, 61, textureX, textureY); // Box 37
		gunModel[9] = new ModelRendererTurbo(this, 1, 86, textureX, textureY); // Box 38
		gunModel[10] = new ModelRendererTurbo(this, 80, 29, textureX, textureY); // Box 39
		gunModel[11] = new ModelRendererTurbo(this, 80, 29, textureX, textureY); // Box 40
		gunModel[12] = new ModelRendererTurbo(this, 80, 18, textureX, textureY); // Box 41
		gunModel[13] = new ModelRendererTurbo(this, 80, 50, textureX, textureY); // Box 42
		gunModel[14] = new ModelRendererTurbo(this, 80, 39, textureX, textureY); // Box 43
		gunModel[15] = new ModelRendererTurbo(this, 80, 50, textureX, textureY); // Box 44
		gunModel[16] = new ModelRendererTurbo(this, 141, 1, textureX, textureY); // Box 45
		gunModel[17] = new ModelRendererTurbo(this, 141, 1, textureX, textureY); // Box 47
		gunModel[18] = new ModelRendererTurbo(this, 141, 1, textureX, textureY); // Box 48
		gunModel[19] = new ModelRendererTurbo(this, 28, 86, textureX, textureY); // Box 49
		gunModel[20] = new ModelRendererTurbo(this, 28, 86, textureX, textureY); // Box 50
		gunModel[21] = new ModelRendererTurbo(this, 28, 86, textureX, textureY); // Box 51
		gunModel[22] = new ModelRendererTurbo(this, 51, 87, textureX, textureY); // Box 52
		gunModel[23] = new ModelRendererTurbo(this, 28, 86, textureX, textureY); // Box 56
		gunModel[24] = new ModelRendererTurbo(this, 51, 87, textureX, textureY); // Box 57
		gunModel[25] = new ModelRendererTurbo(this, 28, 86, textureX, textureY); // Box 58
		gunModel[26] = new ModelRendererTurbo(this, 28, 86, textureX, textureY); // Box 59
		gunModel[27] = new ModelRendererTurbo(this, 28, 86, textureX, textureY); // Box 60
		gunModel[28] = new ModelRendererTurbo(this, 51, 87, textureX, textureY); // Box 61
		gunModel[29] = new ModelRendererTurbo(this, 28, 86, textureX, textureY); // Box 62
		gunModel[30] = new ModelRendererTurbo(this, 28, 86, textureX, textureY); // Box 63
		gunModel[31] = new ModelRendererTurbo(this, 1, 76, textureX, textureY); // Box 64

		gunModel[0].addShapeBox(0F, 0F, 0F, 28, 2, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 0
		gunModel[0].setRotationPoint(-5.5F, -12F, -4.5F);

		gunModel[1].addShapeBox(0F, 0F, 0F, 24, 2, 9, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 1
		gunModel[1].setRotationPoint(-1.5F, -26F, -4.5F);

		gunModel[2].addBox(0F, 0F, 0F, 24, 12, 9, 0F); // Box 2
		gunModel[2].setRotationPoint(-1.5F, -24F, -4.5F);

		gunModel[3].addShapeBox(0F, 0F, 0F, 18, 4, 12, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, -4F); // Box 9
		gunModel[3].setRotationPoint(66.5F, -19.5F, -6F);

		gunModel[4].addBox(0F, 0F, 0F, 18, 4, 12, 0F); // Box 11
		gunModel[4].setRotationPoint(66.5F, -23.5F, -6F);

		gunModel[5].addShapeBox(0F, 0F, 0F, 18, 4, 12, 0F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 13
		gunModel[5].setRotationPoint(66.5F, -27.5F, -6F);

		gunModel[6].addShapeBox(0F, 0F, 0F, 4, 12, 9, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 23
		gunModel[6].setRotationPoint(-5.5F, -24F, -4.5F);

		gunModel[7].addShapeBox(0F, 0F, 0F, 4, 2, 9, 0F, 0F, -2F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, -2F, -2F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F); // Box 24
		gunModel[7].setRotationPoint(-5.5F, -26F, -4.5F);

		gunModel[8].addBox(0F, 0F, 0F, 28, 5, 9, 0F); // Box 37
		gunModel[8].setRotationPoint(26.5F, -17F, -4.5F);

		gunModel[9].addShapeBox(0F, 0F, 0F, 4, 3, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 38
		gunModel[9].setRotationPoint(26.5F, -12F, -4.5F);

		gunModel[10].addShapeBox(0F, 0F, 0F, 40, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 39
		gunModel[10].setRotationPoint(22.5F, -25F, -3.5F);

		gunModel[11].addShapeBox(0F, 0F, 0F, 40, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 40
		gunModel[11].setRotationPoint(22.5F, -20F, -3.5F);

		gunModel[12].addBox(0F, 0F, 0F, 40, 3, 7, 0F); // Box 41
		gunModel[12].setRotationPoint(22.5F, -23F, -3.5F);

		gunModel[13].addShapeBox(0F, 0F, 0F, 35, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 42
		gunModel[13].setRotationPoint(22.5F, -13F, -3.5F);

		gunModel[14].addBox(0F, 0F, 0F, 35, 3, 7, 0F); // Box 43
		gunModel[14].setRotationPoint(22.5F, -16F, -3.5F);

		gunModel[15].addShapeBox(0F, 0F, 0F, 35, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 44
		gunModel[15].setRotationPoint(22.5F, -18F, -3.5F);

		gunModel[16].addShapeBox(0F, 0F, 0F, 4, 4, 12, 0F, 0F, -0.5F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -2.5F, 0F, -0.5F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -2.5F); // Box 45
		gunModel[16].setRotationPoint(62.5F, -23.5F, -6F);

		gunModel[17].addShapeBox(0F, 0F, 0F, 4, 4, 12, 0F, 0F, 0.5F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, -2.5F, 0F, -2.5F, -4.5F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, -2.5F, -4.5F); // Box 47
		gunModel[17].setRotationPoint(62.5F, -19.5F, -6F);

		gunModel[18].addShapeBox(0F, 0F, 0F, 4, 4, 12, 0F, 0F, -2.5F, -4.5F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, -2.5F, -4.5F, 0F, 0.5F, -2.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, -2.5F); // Box 48
		gunModel[18].setRotationPoint(62.5F, -27.5F, -6F);

		gunModel[19].addShapeBox(0F, 0F, 0F, 2, 3, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 49
		gunModel[19].setRotationPoint(36.5F, -12F, -4.5F);

		gunModel[20].addShapeBox(0F, 0F, 0F, 2, 3, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, -1F, -2F, 0F, -1F, -2F, 0F, 0F, -2F); // Box 50
		gunModel[20].setRotationPoint(30.5F, -12F, -4.5F);

		gunModel[21].addShapeBox(0F, 0F, 0F, 2, 3, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, -1F, -2F); // Box 51
		gunModel[21].setRotationPoint(34.5F, -12F, -4.5F);

		gunModel[22].addShapeBox(0F, 0F, 0F, 2, 2, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 52
		gunModel[22].setRotationPoint(32.5F, -12F, -4.5F);

		gunModel[23].addShapeBox(0F, 0F, 0F, 2, 3, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, -1F, -2F, 0F, -1F, -2F, 0F, 0F, -2F); // Box 56
		gunModel[23].setRotationPoint(38.5F, -12F, -4.5F);

		gunModel[24].addShapeBox(0F, 0F, 0F, 2, 2, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 57
		gunModel[24].setRotationPoint(40.5F, -12F, -4.5F);

		gunModel[25].addShapeBox(0F, 0F, 0F, 2, 3, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, -1F, -2F); // Box 58
		gunModel[25].setRotationPoint(42.5F, -12F, -4.5F);

		gunModel[26].addShapeBox(0F, 0F, 0F, 2, 3, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 59
		gunModel[26].setRotationPoint(44.5F, -12F, -4.5F);

		gunModel[27].addShapeBox(0F, 0F, 0F, 2, 3, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, -1F, -2F, 0F, -1F, -2F, 0F, 0F, -2F); // Box 60
		gunModel[27].setRotationPoint(46.5F, -12F, -4.5F);

		gunModel[28].addShapeBox(0F, 0F, 0F, 2, 2, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 61
		gunModel[28].setRotationPoint(48.5F, -12F, -4.5F);

		gunModel[29].addShapeBox(0F, 0F, 0F, 2, 3, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, -1F, -2F); // Box 62
		gunModel[29].setRotationPoint(50.5F, -12F, -4.5F);

		gunModel[30].addShapeBox(0F, 0F, 0F, 2, 3, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 63
		gunModel[30].setRotationPoint(52.5F, -12F, -4.5F);

		gunModel[31].addBox(0F, 0F, 0F, 28, 1, 8, 0F); // Box 64
		gunModel[31].setRotationPoint(26.5F, -18F, -4F);


		defaultStockModel = new ModelRendererTurbo[7];
		defaultStockModel[0] = new ModelRendererTurbo(this, 1, 146, textureX, textureY); // grip2
		defaultStockModel[1] = new ModelRendererTurbo(this, 1, 125, textureX, textureY); // grip3
		defaultStockModel[2] = new ModelRendererTurbo(this, 1, 136, textureX, textureY); // grip4
		defaultStockModel[3] = new ModelRendererTurbo(this, 1, 104, textureX, textureY); // grip5
		defaultStockModel[4] = new ModelRendererTurbo(this, 1, 181, textureX, textureY); // gripPlate1
		defaultStockModel[5] = new ModelRendererTurbo(this, 1, 193, textureX, textureY); // gripPlate2
		defaultStockModel[6] = new ModelRendererTurbo(this, 1, 160, textureX, textureY); // gripPlate3

		defaultStockModel[0].addShapeBox(0F, 0F, 0F, 10, 4, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F); // grip2
		defaultStockModel[0].setRotationPoint(-5.5F, -10F, -3.5F);

		defaultStockModel[1].addShapeBox(0F, 0F, 0F, 14, 3, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 2F, 0F, 0F); // grip3
		defaultStockModel[1].setRotationPoint(-9.5F, -6F, -3.5F);

		defaultStockModel[2].addShapeBox(0F, 0F, 0F, 13, 2, 7, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F); // grip4
		defaultStockModel[2].setRotationPoint(-12.5F, -3F, -3.5F);

		defaultStockModel[3].addShapeBox(0F, 0F, 0F, 12, 13, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F); // grip5
		defaultStockModel[3].setRotationPoint(-12.5F, -1F, -3.5F);

		defaultStockModel[4].addShapeBox(0F, 0F, 0F, 13, 3, 8, 0F, 0F, 0F, 0F, -13F, 0F, 0F, -13F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 2F, 0F, 0F); // gripPlate1
		defaultStockModel[4].setRotationPoint(-9F, -6F, -4F);

		defaultStockModel[5].addShapeBox(0F, 0F, 0F, 12, 2, 8, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F); // gripPlate2
		defaultStockModel[5].setRotationPoint(-12F, -3F, -4F);

		defaultStockModel[6].addShapeBox(0F, 0F, 0F, 11, 12, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F); // gripPlate3
		defaultStockModel[6].setRotationPoint(-12F, -1F, -4F);


		defaultGripModel = new ModelRendererTurbo[10];
		defaultGripModel[0] = new ModelRendererTurbo(this, 53, 1, textureX, textureY); // canister1
		defaultGripModel[1] = new ModelRendererTurbo(this, 1, 10, textureX, textureY); // canister2
		defaultGripModel[2] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // canister3
		defaultGripModel[3] = new ModelRendererTurbo(this, 1, 19, textureX, textureY); // canister4
		defaultGripModel[4] = new ModelRendererTurbo(this, 34, 1, textureX, textureY); // meter1
		defaultGripModel[5] = new ModelRendererTurbo(this, 34, 11, textureX, textureY); // meter1-2
		defaultGripModel[6] = new ModelRendererTurbo(this, 34, 21, textureX, textureY); // meter2
		defaultGripModel[7] = new ModelRendererTurbo(this, 53, 23, textureX, textureY); // pipe1
		defaultGripModel[8] = new ModelRendererTurbo(this, 53, 16, textureX, textureY); // pipe2
		defaultGripModel[9] = new ModelRendererTurbo(this, 53, 10, textureX, textureY); // pipe3

		defaultGripModel[0].addBox(0F, 0F, 0F, 6, 6, 2, 0F); // canister1
		defaultGripModel[0].setRotationPoint(5F, -23F, -6.5F);

		defaultGripModel[1].addShapeBox(0F, 0F, 0F, 10, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // canister2
		defaultGripModel[1].setRotationPoint(4F, -19F, -11F);

		defaultGripModel[2].addBox(0F, 0F, 0F, 10, 2, 6, 0F); // canister3
		defaultGripModel[2].setRotationPoint(4F, -21F, -11F);

		defaultGripModel[3].addShapeBox(0F, 0F, 0F, 10, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // canister4
		defaultGripModel[3].setRotationPoint(4F, -23F, -11F);

		defaultGripModel[4].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // meter1
		defaultGripModel[4].setRotationPoint(6F, -25F, -11F);

		defaultGripModel[5].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // meter1-2
		defaultGripModel[5].setRotationPoint(6F, -30F, -11F);

		defaultGripModel[6].addBox(0F, 0F, 0F, 2, 3, 7, 0F); // meter2
		defaultGripModel[6].setRotationPoint(6F, -28F, -11F);

		defaultGripModel[7].addBox(0F, 0F, 0F, 3, 2, 2, 0F); // pipe1
		defaultGripModel[7].setRotationPoint(14F, -21F, -9F);

		defaultGripModel[8].addBox(0F, 0F, 0F, 2, 2, 4, 0F); // pipe2
		defaultGripModel[8].setRotationPoint(17F, -21F, -9F);

		defaultGripModel[9].addBox(0F, 0F, 0F, 4, 4, 1, 0F); // pipe3
		defaultGripModel[9].setRotationPoint(16F, -22F, -5.5F);


		ammoModel = new ModelRendererTurbo[6];
		ammoModel[0] = new ModelRendererTurbo(this, 1, 32, textureX, textureY); // Box 25
		ammoModel[1] = new ModelRendererTurbo(this, 1, 46, textureX, textureY); // Box 26
		ammoModel[2] = new ModelRendererTurbo(this, 1, 32, textureX, textureY); // Box 27
		ammoModel[3] = new ModelRendererTurbo(this, 46, 32, textureX, textureY); // Box 65
		ammoModel[4] = new ModelRendererTurbo(this, 46, 46, textureX, textureY); // Box 66
		ammoModel[5] = new ModelRendererTurbo(this, 46, 32, textureX, textureY); // Box 68

		ammoModel[0].addShapeBox(0F, 0F, 0F, 12, 3, 10, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 25
		ammoModel[0].setRotationPoint(67.5F, -26.5F, -5F);

		ammoModel[1].addBox(0F, 0F, 0F, 12, 4, 10, 0F); // Box 26
		ammoModel[1].setRotationPoint(67.5F, -23.5F, -5F);

		ammoModel[2].addShapeBox(0F, 0F, 0F, 12, 3, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F); // Box 27
		ammoModel[2].setRotationPoint(67.5F, -19.5F, -5F);

		ammoModel[3].addShapeBox(0F, 0F, 0F, 4, 3, 10, 0F, 0F, 0F, -3F, 0F, -2.5F, -4F, 0F, -2.5F, -4F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0.5F, -3F, 0F, 0.5F, -3F, 0F, 0F, 0F); // Box 65
		ammoModel[3].setRotationPoint(79.5F, -26.5F, -5F);

		ammoModel[4].addShapeBox(0F, 0F, 0F, 4, 4, 10, 0F, 0F, 0F, 0F, 0F, -0.5F, -3F, 0F, -0.5F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -3F, 0F, -0.5F, -3F, 0F, 0F, 0F); // Box 66
		ammoModel[4].setRotationPoint(79.5F, -23.5F, -5F);

		ammoModel[5].addShapeBox(0F, 0F, 0F, 4, 3, 10, 0F, 0F, 0F, 0F, 0F, 0.5F, -3F, 0F, 0.5F, -3F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, -2.5F, -4F, 0F, -2.5F, -4F, 0F, 0F, -3F); // Box 68
		ammoModel[5].setRotationPoint(79.5F, -19.5F, -5F);

		gunSlideDistance = 0F;
		animationType = EnumAnimationType.END_LOADED;

		translateAll(0F, 0F, 0F);


		flipAll();
	}
}