package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.ModelAttachment;
import com.flansmod.client.tmt.ModelRendererTurbo;

import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelLPMSolidTacticalStock extends ModelAttachment
{
	int textureX = 512;
	int textureY = 256;

	public ModelLPMSolidTacticalStock()
	{
		attachmentModel = new ModelRendererTurbo[19];
		attachmentModel[0] = new ModelRendererTurbo(this, 2, 130, textureX, textureY); // Box 0
		attachmentModel[1] = new ModelRendererTurbo(this, 52, 132, textureX, textureY); // Box 52
		attachmentModel[2] = new ModelRendererTurbo(this, 136, 117, textureX, textureY); // Box 53
		attachmentModel[3] = new ModelRendererTurbo(this, 136, 133, textureX, textureY); // Box 54
		attachmentModel[4] = new ModelRendererTurbo(this, 52, 157, textureX, textureY); // Box 55
		attachmentModel[5] = new ModelRendererTurbo(this, 136, 143, textureX, textureY); // Box 56
		attachmentModel[6] = new ModelRendererTurbo(this, 52, 147, textureX, textureY); // Box 57
		attachmentModel[7] = new ModelRendererTurbo(this, 25, 147, textureX, textureY); // Box 59
		attachmentModel[8] = new ModelRendererTurbo(this, 25, 163, textureX, textureY); // Box 60
		attachmentModel[9] = new ModelRendererTurbo(this, 2, 168, textureX, textureY); // Box 61
		attachmentModel[10] = new ModelRendererTurbo(this, 2, 114, textureX, textureY); // Box 62
		attachmentModel[11] = new ModelRendererTurbo(this, 69, 130, textureX, textureY); // Box 64
		attachmentModel[12] = new ModelRendererTurbo(this, 2, 158, textureX, textureY); // Box 65
		attachmentModel[13] = new ModelRendererTurbo(this, 69, 117, textureX, textureY); // Box 66
		attachmentModel[14] = new ModelRendererTurbo(this, 69, 140, textureX, textureY); // Box 67
		attachmentModel[15] = new ModelRendererTurbo(this, 69, 157, textureX, textureY); // Box 68
		attachmentModel[16] = new ModelRendererTurbo(this, 25, 173, textureX, textureY); // Box 0
		attachmentModel[17] = new ModelRendererTurbo(this, 25, 130, textureX, textureY); // Box 1
		attachmentModel[18] = new ModelRendererTurbo(this, 136, 154, textureX, textureY); // Box 2

		attachmentModel[0].addBox(-42F, -4F, -4F, 3, 19, 8, 0F); // Box 0
		attachmentModel[0].setRotationPoint(0F, 0F, 0F);

		attachmentModel[1].addBox(-1F, -4F, -3.5F, 1, 7, 7, 0F); // Box 52
		attachmentModel[1].setRotationPoint(0F, 0F, 0F);

		attachmentModel[2].addShapeBox(-11F, -4F, -4F, 10, 7, 8, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, 4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F); // Box 53
		attachmentModel[2].setRotationPoint(0F, 0F, 0F);

		attachmentModel[3].addShapeBox(-11F, -5F, -4F, 10, 1, 8, 0F, 0F, -4F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, -4F, -1F, 0F, 4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F); // Box 54
		attachmentModel[3].setRotationPoint(0F, 0F, 0F);

		attachmentModel[4].addShapeBox(-1F, -5F, -3.5F, 1, 1, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 55
		attachmentModel[4].setRotationPoint(0F, 0F, 0F);

		attachmentModel[5].addShapeBox(-11F, 7F, -4F, 10, 2, 8, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -4F, -1F, 0F, -4F, -1F, 0F, 0F, -1F); // Box 56
		attachmentModel[5].setRotationPoint(0F, 0F, 0F);

		attachmentModel[6].addShapeBox(-1F, 3F, -3.5F, 1, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 57
		attachmentModel[6].setRotationPoint(0F, 0F, 0F);

		attachmentModel[7].addShapeBox(-14F, 0F, -4F, 3, 7, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F); // Box 59
		attachmentModel[7].setRotationPoint(0F, 0F, 0F);

		attachmentModel[8].addShapeBox(-14F, -1F, -4F, 3, 1, 8, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 60
		attachmentModel[8].setRotationPoint(0F, 0F, 0F);

		attachmentModel[9].addShapeBox(-17F, -5F, -4F, 3, 1, 8, 0F, 0F, 0F, -1F, 0F, -4F, -1F, 0F, -4F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, 0F, 0F); // Box 61
		attachmentModel[9].setRotationPoint(0F, 0F, 0F);

		attachmentModel[10].addShapeBox(-39F, 0F, -4F, 25, 7, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 62
		attachmentModel[10].setRotationPoint(0F, 0F, 0F);

		attachmentModel[11].addShapeBox(-39F, -5F, -4F, 22, 1, 8, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 64
		attachmentModel[11].setRotationPoint(0F, 0F, 0F);

		attachmentModel[12].addShapeBox(-42F, -5F, -4F, 3, 1, 8, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 65
		attachmentModel[12].setRotationPoint(0F, 0F, 0F);

		attachmentModel[13].addShapeBox(-39F, -4F, -4F, 25, 4, 8, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 66
		attachmentModel[13].setRotationPoint(0F, 0F, 0F);

		attachmentModel[14].addShapeBox(-34F, 7F, -4F, 20, 8, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -7F, 0F, 0F, -7F, 0F, 0F, 0F, 0F); // Box 67
		attachmentModel[14].setRotationPoint(0F, 0F, 0F);

		attachmentModel[15].addShapeBox(-34F, 15F, -4F, 20, 2, 8, 0F, 0F, 0F, 0F, 0F, 7F, 0F, 0F, 7F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -7F, -1F, 0F, -7F, -1F, 0F, 0F, -1F); // Box 68
		attachmentModel[15].setRotationPoint(0F, 0F, 0F);

		attachmentModel[16].addShapeBox(-14F, 8F, -4F, 3, 2, 8, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, 0F, -1F); // Box 0
		attachmentModel[16].setRotationPoint(0F, 0F, 0F);

		attachmentModel[17].addBox(-39F, 7F, -4F, 5, 8, 8, 0F); // Box 1
		attachmentModel[17].setRotationPoint(0F, 0F, 0F);

		attachmentModel[18].addShapeBox(-42F, 15F, -4F, 8, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 2
		attachmentModel[18].setRotationPoint(0F, 0F, 0F);

		renderOffset = 0F;

		flipAll();
	}
}