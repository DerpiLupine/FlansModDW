package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.ModelAAGun;
import com.flansmod.client.tmt.ModelRendererTurbo;

public class ModelPUPSentry extends ModelAAGun 
{
	public ModelPUPSentry()
    {
		int textureX = 256;
		int textureY = 128;
		
        baseModel = new ModelRendererTurbo[13];
		baseModel[0] = new ModelRendererTurbo(this, 44, 7, textureX, textureY); // battery
		baseModel[1] = new ModelRendererTurbo(this, 27, 38, textureX, textureY); // gear
		baseModel[2] = new ModelRendererTurbo(this, 27, 38, textureX, textureY); // gear
		baseModel[3] = new ModelRendererTurbo(this, 27, 38, textureX, textureY); // gear
		baseModel[4] = new ModelRendererTurbo(this, 1, 31, textureX, textureY); // pole
		baseModel[5] = new ModelRendererTurbo(this, 1, 31, textureX, textureY); // pole
		baseModel[6] = new ModelRendererTurbo(this, 1, 31, textureX, textureY); // pole
		baseModel[7] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // stand
		baseModel[8] = new ModelRendererTurbo(this, 10, 34, textureX, textureY); // stand2
		baseModel[9] = new ModelRendererTurbo(this, 1, 8, textureX, textureY); // stump
		baseModel[10] = new ModelRendererTurbo(this, 1, 8, textureX, textureY); // stump
		baseModel[11] = new ModelRendererTurbo(this, 1, 8, textureX, textureY); // stump
		baseModel[12] = new ModelRendererTurbo(this, 1, 8, textureX, textureY); // stump

		baseModel[0].addBox(-5F, -5F, -6F, 6, 3, 4, 0F); // battery
		baseModel[0].setRotationPoint(0F, 0F, 0F);

		baseModel[1].addBox(-3F, -15F, -1F, 6, 2, 2, 0F); // gear
		baseModel[1].setRotationPoint(0F, 0F, 0F);

		baseModel[2].addShapeBox(-3F, -15F, 1F, 6, 2, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F); // gear
		baseModel[2].setRotationPoint(0F, 0F, 0F);

		baseModel[3].addShapeBox(-3F, -15F, -3F, 6, 2, 2, 0F,-2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // gear
		baseModel[3].setRotationPoint(0F, 0F, 0F);

		baseModel[4].addBox(-1.5F, -16F, -0.5F, 3, 10, 1, 0F); // pole
		baseModel[4].setRotationPoint(0F, 0F, 0F);

		baseModel[5].addShapeBox(-1.5F, -16F, -1.5F, 3, 10, 1, 0F,-1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // pole
		baseModel[5].setRotationPoint(0F, 0F, 0F);

		baseModel[6].addShapeBox(-1.5F, -16F, 0.5F, 3, 10, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // pole
		baseModel[6].setRotationPoint(0F, 0F, 0F);

		baseModel[7].addBox(-7F, -2.5F, -7F, 14, 1, 14, 0F); // stand
		baseModel[7].setRotationPoint(0F, 0F, 0F);

		baseModel[8].addBox(-2F, -6F, -2F, 4, 4, 4, 0F); // stand2
		baseModel[8].setRotationPoint(0F, 0F, 0F);

		baseModel[9].addBox(5F, -3F, 5F, 3, 3, 3, 0F); // stump
		baseModel[9].setRotationPoint(0F, 0F, 0F);

		baseModel[10].addBox(-8F, -3F, -8F, 3, 3, 3, 0F); // stump
		baseModel[10].setRotationPoint(0F, 0F, 0F);

		baseModel[11].addBox(-8F, -3F, 5F, 3, 3, 3, 0F); // stump
		baseModel[11].setRotationPoint(0F, 0F, 0F);

		baseModel[12].addBox(5F, -3F, -8F, 3, 3, 3, 0F); // stump
		baseModel[12].setRotationPoint(0F, 0F, 0F);


		seatModel = new ModelRendererTurbo[10];
		seatModel[0] = new ModelRendererTurbo(this, 44, 33, textureX, textureY); // ammoBox
		seatModel[1] = new ModelRendererTurbo(this, 44, 33, textureX, textureY); // ammoBox
		seatModel[2] = new ModelRendererTurbo(this, 44, 44, textureX, textureY); // ammoBox2
		seatModel[3] = new ModelRendererTurbo(this, 44, 44, textureX, textureY); // ammoBox2
		seatModel[4] = new ModelRendererTurbo(this, 72, 23, textureX, textureY); // Box 21
		seatModel[5] = new ModelRendererTurbo(this, 72, 23, textureX, textureY); // Box 22
		seatModel[6] = new ModelRendererTurbo(this, 67, 41, textureX, textureY); // camera
		seatModel[7] = new ModelRendererTurbo(this, 1, 43, textureX, textureY); // seatBox
		seatModel[8] = new ModelRendererTurbo(this, 1, 17, textureX, textureY); // sentryBody
		seatModel[9] = new ModelRendererTurbo(this, 49, 22, textureX, textureY); // sentryBody2

		seatModel[0].addBox(-2F, -21F, -10F, 4, 3, 7, 0F); // ammoBox
		seatModel[0].setRotationPoint(0F, 0F, 0F);

		seatModel[1].addBox(-2F, -21F, 3F, 4, 3, 7, 0F); // ammoBox
		seatModel[1].setRotationPoint(0F, 0F, 0F);

		seatModel[2].addShapeBox(-2F, -18F, -10F, 4, 1, 7, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F); // ammoBox2
		seatModel[2].setRotationPoint(0F, 0F, 0F);

		seatModel[3].addShapeBox(-2F, -18F, 3F, 4, 1, 7, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F); // ammoBox2
		seatModel[3].setRotationPoint(0F, 0F, 0F);

		seatModel[4].addBox(-7F, -24F, 1F, 3, 5, 1, 0F); // Box 21
		seatModel[4].setRotationPoint(0F, 0F, 0F);

		seatModel[5].addBox(-7F, -24F, -2F, 3, 5, 1, 0F); // Box 22
		seatModel[5].setRotationPoint(0F, 0F, 0F);

		seatModel[6].addBox(-3F, -25.5F, -1.5F, 6, 3, 3, 0F); // camera
		seatModel[6].setRotationPoint(0F, 0F, 0F);

		seatModel[7].addBox(-4F, -17F, -4F, 8, 1, 8, 0F); // seatBox
		seatModel[7].setRotationPoint(0F, 0F, 0F);

		seatModel[8].addBox(-6F, -23F, -3F, 7, 6, 6, 0F); // sentryBody
		seatModel[8].setRotationPoint(0F, 0F, 0F);

		seatModel[9].addShapeBox(-4F, -24F, -3F, 5, 1, 6, 0F,0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // sentryBody2
		seatModel[9].setRotationPoint(0F, 0F, 0F);


		gunModel = new ModelRendererTurbo[4];
		gunModel[0] = new ModelRendererTurbo(this, 67, 48, textureX, textureY); // barrel
		gunModel[1] = new ModelRendererTurbo(this, 67, 48, textureX, textureY); // barrel
		gunModel[2] = new ModelRendererTurbo(this, 67, 31, textureX, textureY); // gun
		gunModel[3] = new ModelRendererTurbo(this, 28, 18, textureX, textureY); // sentryBody3

		gunModel[0].addBox(5F, -21F, 0.5F, 12, 2, 2, 0F); // barrel
		gunModel[0].setRotationPoint(0F, 0F, 0F);

		gunModel[1].addBox(5F, -21F, -2.5F, 12, 2, 2, 0F); // barrel
		gunModel[1].setRotationPoint(0F, 0F, 0F);

		gunModel[2].addBox(5F, -21.5F, -3F, 1, 3, 6, 0F); // gun
		gunModel[2].setRotationPoint(0F, 0F, 0F);

		gunModel[3].addBox(1F, -22F, -3F, 4, 5, 6, 0F); // sentryBody3
		gunModel[3].setRotationPoint(0F, 0F, 0F);


		ammoModel = new ModelRendererTurbo[0][0];

		barrelX = 0;
		barrelY = 0;
		barrelZ = 0;


		flipAll();
    }
}
