package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.ModelAttachment;
import com.flansmod.client.tmt.ModelRendererTurbo;

import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelABTPressureStock extends ModelAttachment
{
	int textureX = 512;
	int textureY = 256;

	public ModelABTPressureStock()
	{
		attachmentModel = new ModelRendererTurbo[31];
		attachmentModel[0] = new ModelRendererTurbo(this, 1, 130, textureX, textureY); // stock1
		attachmentModel[1] = new ModelRendererTurbo(this, 1, 173, textureX, textureY); // stock2
		attachmentModel[2] = new ModelRendererTurbo(this, 1, 162, textureX, textureY); // stock3
		attachmentModel[3] = new ModelRendererTurbo(this, 1, 148, textureX, textureY); // stock4
		attachmentModel[4] = new ModelRendererTurbo(this, 1, 50, textureX, textureY); // stockCanister1
		attachmentModel[5] = new ModelRendererTurbo(this, 28, 50, textureX, textureY); // stockCanister1
		attachmentModel[6] = new ModelRendererTurbo(this, 55, 50, textureX, textureY); // stockCanister2
		attachmentModel[7] = new ModelRendererTurbo(this, 30, 105, textureX, textureY); // stockCanister3
		attachmentModel[8] = new ModelRendererTurbo(this, 30, 105, textureX, textureY); // stockCanister3
		attachmentModel[9] = new ModelRendererTurbo(this, 1, 105, textureX, textureY); // stockCanister4
		attachmentModel[10] = new ModelRendererTurbo(this, 30, 92, textureX, textureY); // stockCanister5
		attachmentModel[11] = new ModelRendererTurbo(this, 30, 92, textureX, textureY); // stockCanister5
		attachmentModel[12] = new ModelRendererTurbo(this, 1, 92, textureX, textureY); // stockCanister6
		attachmentModel[13] = new ModelRendererTurbo(this, 57, 108, textureX, textureY); // stockCanister7
		attachmentModel[14] = new ModelRendererTurbo(this, 57, 108, textureX, textureY); // stockCanister7
		attachmentModel[15] = new ModelRendererTurbo(this, 57, 108, textureX, textureY); // stockCanister7
		attachmentModel[16] = new ModelRendererTurbo(this, 1, 124, textureX, textureY); // stockPipe1
		attachmentModel[17] = new ModelRendererTurbo(this, 1, 124, textureX, textureY); // stockPipe1
		attachmentModel[18] = new ModelRendererTurbo(this, 1, 117, textureX, textureY); // stockPipe2
		attachmentModel[19] = new ModelRendererTurbo(this, 74, 124, textureX, textureY); // stockPipe3
		attachmentModel[20] = new ModelRendererTurbo(this, 74, 124, textureX, textureY); // stockPipe3
		attachmentModel[21] = new ModelRendererTurbo(this, 74, 117, textureX, textureY); // stockPipe4
		attachmentModel[22] = new ModelRendererTurbo(this, 43, 171, textureX, textureY); // stockStrap1
		attachmentModel[23] = new ModelRendererTurbo(this, 32, 166, textureX, textureY); // stockStrap2
		attachmentModel[24] = new ModelRendererTurbo(this, 32, 166, textureX, textureY); // stockStrap2
		attachmentModel[25] = new ModelRendererTurbo(this, 32, 148, textureX, textureY); // stockStrap3
		attachmentModel[26] = new ModelRendererTurbo(this, 32, 130, textureX, textureY); // stockStrap4
		attachmentModel[27] = new ModelRendererTurbo(this, 112, 4, textureX, textureY); // turningValve1
		attachmentModel[28] = new ModelRendererTurbo(this, 121, 9, textureX, textureY); // turningValve2
		attachmentModel[29] = new ModelRendererTurbo(this, 121, 9, textureX, textureY); // turningValve2
		attachmentModel[30] = new ModelRendererTurbo(this, 112, 8, textureX, textureY); // valve

		attachmentModel[0].addBox(-34.5F, -3.5F, -4F, 3, 9, 8, 0F); // stock1
		attachmentModel[0].setRotationPoint(0F, 0F, 0F);

		attachmentModel[1].addShapeBox(-34.5F, -5.5F, -4F, 3, 2, 8, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // stock2
		attachmentModel[1].setRotationPoint(0F, 0F, 0F);

		attachmentModel[2].addShapeBox(-34.5F, 10.5F, -4F, 5, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // stock3
		attachmentModel[2].setRotationPoint(0F, 0F, 0F);

		attachmentModel[3].addShapeBox(-34.5F, 5.5F, -4F, 3, 5, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F); // stock4
		attachmentModel[3].setRotationPoint(0F, 0F, 0F);

		attachmentModel[4].addShapeBox(-7F, 16F, -5F, 3, 14, 10, 0F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F); // stockCanister1
		attachmentModel[4].setRotationPoint(0F, 0F, 0F);
		attachmentModel[4].rotateAngleZ = -1.22173048F;

		attachmentModel[5].addShapeBox(0F, 16F, -5F, 3, 14, 10, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F); // stockCanister1
		attachmentModel[5].setRotationPoint(0F, 0F, 0F);
		attachmentModel[5].rotateAngleZ = -1.22173048F;

		attachmentModel[6].addBox(-4F, 16F, -5F, 4, 14, 10, 0F); // stockCanister2
		attachmentModel[6].setRotationPoint(0F, 0F, 0F);
		attachmentModel[6].rotateAngleZ = -1.22173048F;

		attachmentModel[7].addShapeBox(-7F, 30F, -5F, 3, 1, 10, 0F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, -1F, 0F, -3F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, -1F, 0F, -3F); // stockCanister3
		attachmentModel[7].setRotationPoint(0F, 0F, 0F);
		attachmentModel[7].rotateAngleZ = -1.22173048F;

		attachmentModel[8].addShapeBox(0F, 30F, -5F, 3, 1, 10, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, -1.5F, -1F, 0F, -3F, -1F, 0F, -3F, 0F, 0F, -1.5F); // stockCanister3
		attachmentModel[8].setRotationPoint(0F, 0F, 0F);
		attachmentModel[8].rotateAngleZ = -1.22173048F;

		attachmentModel[9].addShapeBox(-4F, 30F, -5F, 4, 1, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F); // stockCanister4
		attachmentModel[9].setRotationPoint(0F, 0F, 0F);
		attachmentModel[9].rotateAngleZ = -1.22173048F;

		attachmentModel[10].addShapeBox(-7F, 14F, -5F, 3, 2, 10, 0F, -1F, 0F, -3F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, -1F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F); // stockCanister5
		attachmentModel[10].setRotationPoint(0F, 0F, 0F);
		attachmentModel[10].rotateAngleZ = -1.22173048F;

		attachmentModel[11].addShapeBox(0F, 14F, -5F, 3, 2, 10, 0F, 0F, 0F, -1.5F, -1F, 0F, -3F, -1F, 0F, -3F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F); // stockCanister5
		attachmentModel[11].setRotationPoint(0F, 0F, 0F);
		attachmentModel[11].rotateAngleZ = -1.22173048F;

		attachmentModel[12].addShapeBox(-4F, 14F, -5F, 4, 2, 10, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // stockCanister6
		attachmentModel[12].setRotationPoint(0F, 0F, 0F);
		attachmentModel[12].rotateAngleZ = -1.22173048F;

		attachmentModel[13].addShapeBox(-1F, 12.5F, -3F, 2, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F); // stockCanister7
		attachmentModel[13].setRotationPoint(0F, 0F, 0F);
		attachmentModel[13].rotateAngleZ = -1.22173048F;

		attachmentModel[14].addShapeBox(-5F, 12.5F, -3F, 2, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F); // stockCanister7
		attachmentModel[14].setRotationPoint(0F, 0F, 0F);
		attachmentModel[14].rotateAngleZ = -1.22173048F;

		attachmentModel[15].addBox(-3F, 12.5F, -3F, 2, 2, 6, 0F); // stockCanister7
		attachmentModel[15].setRotationPoint(0F, 0F, 0F);
		attachmentModel[15].rotateAngleZ = -1.22173048F;

		attachmentModel[16].addShapeBox(-32F, -4.5F, -2F, 32, 1, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // stockPipe1
		attachmentModel[16].setRotationPoint(0F, 0F, 0F);

		attachmentModel[17].addShapeBox(-32F, -1.5F, -2F, 32, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // stockPipe1
		attachmentModel[17].setRotationPoint(0F, 0F, 0F);

		attachmentModel[18].addBox(-32F, -3.5F, -2F, 32, 2, 4, 0F); // stockPipe2
		attachmentModel[18].setRotationPoint(0F, 0F, 0F);

		attachmentModel[19].addShapeBox(-13F, 3.5F, -2F, 13, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // stockPipe3
		attachmentModel[19].setRotationPoint(0F, 0F, 0F);

		attachmentModel[20].addShapeBox(-13F, 0.5F, -2F, 13, 1, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // stockPipe3
		attachmentModel[20].setRotationPoint(0F, 0F, 0F);

		attachmentModel[21].addBox(-13F, 1.5F, -2F, 13, 2, 4, 0F); // stockPipe4
		attachmentModel[21].setRotationPoint(0F, 0F, 0F);

		attachmentModel[22].addShapeBox(-29F, -5F, -2.5F, 4, 1, 5, 0F, 0F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // stockStrap1
		attachmentModel[22].setRotationPoint(0F, 0F, 0F);

		attachmentModel[23].addShapeBox(-29F, -4F, 1.5F, 4, 10, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, -0.5F, -3F, 4F, -2F, -3F, 4F, -2F, 3F, -4F, -0.5F, 3F); // stockStrap2
		attachmentModel[23].setRotationPoint(0F, 0F, 0F);

		attachmentModel[24].addShapeBox(-29F, -4F, -2.5F, 4, 10, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, -0.5F, 3F, 4F, -2F, 3F, 4F, -2F, -3F, -4F, -0.5F, -3F); // stockStrap2
		attachmentModel[24].setRotationPoint(0F, 0F, 0F);

		attachmentModel[25].addShapeBox(-25F, 3.5F, -5.5F, 4, 6, 11, 0F, 0F, -2F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -2F, 0F, -1.5F, -0.5F, 0F, 1.5F, -2F, 0F, 1.5F, -2F, 0F, -1.5F, -0.5F, 0F); // stockStrap3
		attachmentModel[25].setRotationPoint(0F, 0F, 0F);

		attachmentModel[26].addShapeBox(-23.5F, 7F, -5.5F, 4, 6, 11, 0F, 0F, -2F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -2F, 0F, -1.5F, -0.5F, -3.5F, 1.5F, -2F, -3.5F, 1.5F, -2F, -3.5F, -1.5F, -0.5F, -3.5F); // stockStrap4
		attachmentModel[26].setRotationPoint(0F, 0F, 0F);

		attachmentModel[27].addBox(-8F, 1.5F, -4F, 6, 2, 1, 0F); // turningValve1
		attachmentModel[27].setRotationPoint(0F, 0F, 0F);

		attachmentModel[28].addShapeBox(-8F, 3.5F, -4F, 6, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F); // turningValve2
		attachmentModel[28].setRotationPoint(0F, 0F, 0F);

		attachmentModel[29].addShapeBox(-8F, -0.5F, -4F, 6, 2, 1, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // turningValve2
		attachmentModel[29].setRotationPoint(0F, 0F, 0F);

		attachmentModel[30].addBox(-6F, 1.5F, -3F, 2, 2, 2, 0F); // valve
		attachmentModel[30].setRotationPoint(0F, 0F, 0F);

		renderOffset = 0F;

		flipAll();
	}
}