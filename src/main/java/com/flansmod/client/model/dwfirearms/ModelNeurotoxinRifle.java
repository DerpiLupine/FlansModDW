package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelNeurotoxinRifle extends ModelGun
{
	int textureX = 512;
	int textureY = 256;

	public ModelNeurotoxinRifle()
	{
		gunModel = new ModelRendererTurbo[106];
		gunModel[0] = new ModelRendererTurbo(this, 1, 53, textureX, textureY); // Import acidCanister
		gunModel[1] = new ModelRendererTurbo(this, 1, 53, textureX, textureY); // Import acidCanister
		gunModel[2] = new ModelRendererTurbo(this, 1, 53, textureX, textureY); // Import acidCanister
		gunModel[3] = new ModelRendererTurbo(this, 1, 53, textureX, textureY); // Import acidCanister
		gunModel[4] = new ModelRendererTurbo(this, 1, 53, textureX, textureY); // Import acidCanister
		gunModel[5] = new ModelRendererTurbo(this, 1, 53, textureX, textureY); // Import acidCanister
		gunModel[6] = new ModelRendererTurbo(this, 1, 53, textureX, textureY); // Import acidCanister
		gunModel[7] = new ModelRendererTurbo(this, 1, 53, textureX, textureY); // Import acidCanister
		gunModel[8] = new ModelRendererTurbo(this, 1, 53, textureX, textureY); // Import acidCanister
		gunModel[9] = new ModelRendererTurbo(this, 1, 53, textureX, textureY); // Import acidCanister
		gunModel[10] = new ModelRendererTurbo(this, 1, 53, textureX, textureY); // Import acidCanister
		gunModel[11] = new ModelRendererTurbo(this, 1, 53, textureX, textureY); // Import acidCanister
		gunModel[12] = new ModelRendererTurbo(this, 18, 52, textureX, textureY); // Import acidCanister2
		gunModel[13] = new ModelRendererTurbo(this, 18, 52, textureX, textureY); // Import acidCanister2
		gunModel[14] = new ModelRendererTurbo(this, 18, 52, textureX, textureY); // Import acidCanister2
		gunModel[15] = new ModelRendererTurbo(this, 18, 52, textureX, textureY); // Import acidCanister2
		gunModel[16] = new ModelRendererTurbo(this, 37, 52, textureX, textureY); // Import acidCanister3
		gunModel[17] = new ModelRendererTurbo(this, 37, 52, textureX, textureY); // Import acidCanister3
		gunModel[18] = new ModelRendererTurbo(this, 35, 24, textureX, textureY); // Import acidCanisterGlass
		gunModel[19] = new ModelRendererTurbo(this, 35, 24, textureX, textureY); // Import acidCanisterGlass
		gunModel[20] = new ModelRendererTurbo(this, 18, 24, textureX, textureY); // Import acidCanisterGlass2
		gunModel[21] = new ModelRendererTurbo(this, 18, 24, textureX, textureY); // Import acidCanisterGlass2
		gunModel[22] = new ModelRendererTurbo(this, 1, 24, textureX, textureY); // Import acidCanisterGlass3
		gunModel[23] = new ModelRendererTurbo(this, 1, 24, textureX, textureY); // Import acidCanisterGlass3
		gunModel[24] = new ModelRendererTurbo(this, 16, 40, textureX, textureY); // Import acidLiquid
		gunModel[25] = new ModelRendererTurbo(this, 16, 40, textureX, textureY); // Import acidLiquid
		gunModel[26] = new ModelRendererTurbo(this, 16, 40, textureX, textureY); // Import acidLiquid
		gunModel[27] = new ModelRendererTurbo(this, 16, 40, textureX, textureY); // Import acidLiquid
		gunModel[28] = new ModelRendererTurbo(this, 1, 40, textureX, textureY); // Import acidLiquid2
		gunModel[29] = new ModelRendererTurbo(this, 1, 40, textureX, textureY); // Import acidLiquid2
		gunModel[30] = new ModelRendererTurbo(this, 29, 43, textureX, textureY); // Import acidTank
		gunModel[31] = new ModelRendererTurbo(this, 29, 43, textureX, textureY); // Import acidTank
		gunModel[32] = new ModelRendererTurbo(this, 29, 43, textureX, textureY); // Import acidTank
		gunModel[33] = new ModelRendererTurbo(this, 58, 45, textureX, textureY); // Import acidTankHolder
		gunModel[34] = new ModelRendererTurbo(this, 81, 31, textureX, textureY); // Import ammoCasing
		gunModel[35] = new ModelRendererTurbo(this, 81, 31, textureX, textureY); // Import ammoCasing
		gunModel[36] = new ModelRendererTurbo(this, 81, 31, textureX, textureY); // Import ammoCasing
		gunModel[37] = new ModelRendererTurbo(this, 81, 31, textureX, textureY); // Import ammoCasing
		gunModel[38] = new ModelRendererTurbo(this, 81, 31, textureX, textureY); // Import ammoCasing2
		gunModel[39] = new ModelRendererTurbo(this, 81, 31, textureX, textureY); // Import ammoCasing2
		gunModel[40] = new ModelRendererTurbo(this, 96, 1, textureX, textureY); // Import barrel1
		gunModel[41] = new ModelRendererTurbo(this, 96, 10, textureX, textureY); // Import barrel2
		gunModel[42] = new ModelRendererTurbo(this, 96, 19, textureX, textureY); // Import barrel3
		gunModel[43] = new ModelRendererTurbo(this, 196, 19, textureX, textureY); // Import barrelHolder
		gunModel[44] = new ModelRendererTurbo(this, 196, 19, textureX, textureY); // Import barrelHolder
		gunModel[45] = new ModelRendererTurbo(this, 175, 19, textureX, textureY); // Import barrelHolder2
		gunModel[46] = new ModelRendererTurbo(this, 175, 19, textureX, textureY); // Import barrelHolder2
		gunModel[47] = new ModelRendererTurbo(this, 52, 37, textureX, textureY); // Import barrelRail
		gunModel[48] = new ModelRendererTurbo(this, 52, 37, textureX, textureY); // Import barrelRail
		gunModel[49] = new ModelRendererTurbo(this, 96, 61, textureX, textureY); // Import body1
		gunModel[50] = new ModelRendererTurbo(this, 96, 91, textureX, textureY); // Import body2
		gunModel[51] = new ModelRendererTurbo(this, 163, 69, textureX, textureY); // Import body3
		gunModel[52] = new ModelRendererTurbo(this, 96, 80, textureX, textureY); // Import body4
		gunModel[53] = new ModelRendererTurbo(this, 96, 44, textureX, textureY); // Import body5
		gunModel[54] = new ModelRendererTurbo(this, 133, 80, textureX, textureY); // Import body6
		gunModel[55] = new ModelRendererTurbo(this, 96, 29, textureX, textureY); // Import body7
		gunModel[56] = new ModelRendererTurbo(this, 96, 103, textureX, textureY); // Import body8
		gunModel[57] = new ModelRendererTurbo(this, 96, 114, textureX, textureY); // Import body9
		gunModel[58] = new ModelRendererTurbo(this, 24, 18, textureX, textureY); // Import electricGrid
		gunModel[59] = new ModelRendererTurbo(this, 144, 22, textureX, textureY); // Import lowerBarrel
		gunModel[60] = new ModelRendererTurbo(this, 144, 22, textureX, textureY); // Import lowerBarrel
		gunModel[61] = new ModelRendererTurbo(this, 113, 21, textureX, textureY); // Import lowerBarrel2
		gunModel[62] = new ModelRendererTurbo(this, 221, 1, textureX, textureY); // Import muzzle
		gunModel[63] = new ModelRendererTurbo(this, 221, 1, textureX, textureY); // Import muzzle
		gunModel[64] = new ModelRendererTurbo(this, 239, 11, textureX, textureY); // Import muzzle2
		gunModel[65] = new ModelRendererTurbo(this, 230, 11, textureX, textureY); // Import muzzle2
		gunModel[66] = new ModelRendererTurbo(this, 221, 11, textureX, textureY); // Import muzzle2
		gunModel[67] = new ModelRendererTurbo(this, 248, 11, textureX, textureY); // Import muzzle2
		gunModel[68] = new ModelRendererTurbo(this, 1, 61, textureX, textureY); // Import pipe1
		gunModel[69] = new ModelRendererTurbo(this, 1, 68, textureX, textureY); // Import pipe2
		gunModel[70] = new ModelRendererTurbo(this, 1, 68, textureX, textureY); // Import pipe2
		gunModel[71] = new ModelRendererTurbo(this, 45, 68, textureX, textureY); // Import pipe3
		gunModel[72] = new ModelRendererTurbo(this, 38, 68, textureX, textureY); // Import pipe3
		gunModel[73] = new ModelRendererTurbo(this, 38, 72, textureX, textureY); // Import pipe4
		gunModel[74] = new ModelRendererTurbo(this, 29, 66, textureX, textureY); // Import pipe5
		gunModel[75] = new ModelRendererTurbo(this, 29, 71, textureX, textureY); // Import pipe6
		gunModel[76] = new ModelRendererTurbo(this, 10, 67, textureX, textureY); // Import pipe7
		gunModel[77] = new ModelRendererTurbo(this, 29, 66, textureX, textureY); // Import pipe8
		gunModel[78] = new ModelRendererTurbo(this, 36, 61, textureX, textureY); // Import pipeConnector
		gunModel[79] = new ModelRendererTurbo(this, 36, 61, textureX, textureY); // Import pipeConnector
		gunModel[80] = new ModelRendererTurbo(this, 35, 9, textureX, textureY); // Import rail1
		gunModel[81] = new ModelRendererTurbo(this, 35, 1, textureX, textureY); // Import rail2
		gunModel[82] = new ModelRendererTurbo(this, 35, 15, textureX, textureY); // Import rail3
		gunModel[83] = new ModelRendererTurbo(this, 35, 15, textureX, textureY); // Import rail3-2
		gunModel[84] = new ModelRendererTurbo(this, 1, 15, textureX, textureY); // Import sparkCanister
		gunModel[85] = new ModelRendererTurbo(this, 1, 15, textureX, textureY); // Import sparkCanister
		gunModel[86] = new ModelRendererTurbo(this, 1, 15, textureX, textureY); // Import sparkCanister
		gunModel[87] = new ModelRendererTurbo(this, 1, 15, textureX, textureY); // Import sparkCanister
		gunModel[88] = new ModelRendererTurbo(this, 1, 15, textureX, textureY); // Import sparkCanister
		gunModel[89] = new ModelRendererTurbo(this, 1, 15, textureX, textureY); // Import sparkCanister
		gunModel[90] = new ModelRendererTurbo(this, 1, 15, textureX, textureY); // Import sparkCanister
		gunModel[91] = new ModelRendererTurbo(this, 1, 15, textureX, textureY); // Import sparkCanister
		gunModel[92] = new ModelRendererTurbo(this, 10, 15, textureX, textureY); // Import sparkCanister2
		gunModel[93] = new ModelRendererTurbo(this, 10, 15, textureX, textureY); // Import sparkCanister2
		gunModel[94] = new ModelRendererTurbo(this, 10, 15, textureX, textureY); // Import sparkCanister2
		gunModel[95] = new ModelRendererTurbo(this, 10, 15, textureX, textureY); // Import sparkCanister2
		gunModel[96] = new ModelRendererTurbo(this, 19, 19, textureX, textureY); // Import sparkPin
		gunModel[97] = new ModelRendererTurbo(this, 19, 19, textureX, textureY); // Import sparkPin
		gunModel[98] = new ModelRendererTurbo(this, 19, 19, textureX, textureY); // Import sparkPin
		gunModel[99] = new ModelRendererTurbo(this, 19, 19, textureX, textureY); // Import sparkPin
		gunModel[100] = new ModelRendererTurbo(this, 57, 24, textureX, textureY); // Import wire1
		gunModel[101] = new ModelRendererTurbo(this, 85, 18, textureX, textureY); // Import wire2
		gunModel[102] = new ModelRendererTurbo(this, 64, 24, textureX, textureY); // Import wire3
		gunModel[103] = new ModelRendererTurbo(this, 57, 18, textureX, textureY); // Import wire4
		gunModel[104] = new ModelRendererTurbo(this, 72, 21, textureX, textureY); // Import wire5
		gunModel[105] = new ModelRendererTurbo(this, 57, 21, textureX, textureY); // Import wire6

		gunModel[0].addShapeBox(0F, 0F, 0F, 2, 1, 6, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, -1F, -1F, 0F, -2F, -1F, 0F, -2F, 0F, 0F, -1F); // Import acidCanister
		gunModel[0].setRotationPoint(27F, -23F, 4F);

		gunModel[1].addShapeBox(0F, 0F, 0F, 2, 1, 6, 0F, -1F, 0F, -2F, 0F, 0F, -1F, 0F, 0F, -1F, -1F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F); // Import acidCanister
		gunModel[1].setRotationPoint(23F, -34F, 4F);

		gunModel[2].addShapeBox(0F, 0F, 0F, 2, 1, 6, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, -1F, 0F, -2F, 0F, 0F, -1F, 0F, 0F, -1F, -1F, 0F, -2F); // Import acidCanister
		gunModel[2].setRotationPoint(23F, -23F, 4F);

		gunModel[3].addShapeBox(0F, 0F, 0F, 2, 1, 6, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, -1F, 0F, -2F, 0F, 0F, -1F, 0F, 0F, -1F, -1F, 0F, -2F); // Import acidCanister
		gunModel[3].setRotationPoint(15F, -23F, 4F);

		gunModel[4].addShapeBox(0F, 0F, 0F, 2, 1, 6, 0F, -1F, 0F, -2F, 0F, 0F, -1F, 0F, 0F, -1F, -1F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F); // Import acidCanister
		gunModel[4].setRotationPoint(15F, -34F, 4F);

		gunModel[5].addShapeBox(0F, 0F, 0F, 2, 1, 6, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, -1F, -1F, 0F, -2F, -1F, 0F, -2F, 0F, 0F, -1F); // Import acidCanister
		gunModel[5].setRotationPoint(19F, -23F, 4F);

		gunModel[6].addShapeBox(0F, 0F, 0F, 2, 1, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Import acidCanister
		gunModel[6].setRotationPoint(17F, -23F, 4F);

		gunModel[7].addShapeBox(0F, 0F, 0F, 2, 1, 6, 0F, 0F, 0F, -1F, -1F, 0F, -2F, -1F, 0F, -2F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F); // Import acidCanister
		gunModel[7].setRotationPoint(19F, -34F, 4F);

		gunModel[8].addShapeBox(0F, 0F, 0F, 2, 1, 6, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import acidCanister
		gunModel[8].setRotationPoint(17F, -34F, 4F);

		gunModel[9].addShapeBox(0F, 0F, 0F, 2, 1, 6, 0F, 0F, 0F, -1F, -1F, 0F, -2F, -1F, 0F, -2F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F); // Import acidCanister
		gunModel[9].setRotationPoint(27F, -34F, 4F);

		gunModel[10].addShapeBox(0F, 0F, 0F, 2, 1, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Import acidCanister
		gunModel[10].setRotationPoint(25F, -23F, 4F);

		gunModel[11].addShapeBox(0F, 0F, 0F, 2, 1, 6, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import acidCanister
		gunModel[11].setRotationPoint(25F, -34F, 4F);

		gunModel[12].addShapeBox(0F, 0F, 0F, 2, 1, 7, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F); // Import acidCanister2
		gunModel[12].setRotationPoint(14.5F, -33F, 3.5F);

		gunModel[13].addShapeBox(0F, 0F, 0F, 2, 1, 7, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F); // Import acidCanister2
		gunModel[13].setRotationPoint(22.5F, -33F, 3.5F);

		gunModel[14].addShapeBox(0F, 0F, 0F, 2, 1, 7, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F); // Import acidCanister2
		gunModel[14].setRotationPoint(19.5F, -33F, 3.5F);

		gunModel[15].addShapeBox(0F, 0F, 0F, 2, 1, 7, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F); // Import acidCanister2
		gunModel[15].setRotationPoint(27.5F, -33F, 3.5F);

		gunModel[16].addBox(0F, 0F, 0F, 3, 1, 7, 0F); // Import acidCanister3
		gunModel[16].setRotationPoint(24.5F, -33F, 3.5F);

		gunModel[17].addBox(0F, 0F, 0F, 3, 1, 7, 0F); // Import acidCanister3
		gunModel[17].setRotationPoint(16.5F, -33F, 3.5F);

		gunModel[18].addShapeBox(0F, 0F, 0F, 2, 9, 6, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F); // Import acidCanisterGlass
		gunModel[18].setRotationPoint(15F, -32F, 4F);

		gunModel[19].addShapeBox(0F, 0F, 0F, 2, 9, 6, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F); // Import acidCanisterGlass
		gunModel[19].setRotationPoint(23F, -32F, 4F);

		gunModel[20].addBox(0F, 0F, 0F, 2, 9, 6, 0F); // Import acidCanisterGlass2
		gunModel[20].setRotationPoint(17F, -32F, 4F);

		gunModel[21].addBox(0F, 0F, 0F, 2, 9, 6, 0F); // Import acidCanisterGlass2
		gunModel[21].setRotationPoint(25F, -32F, 4F);

		gunModel[22].addShapeBox(0F, 0F, 0F, 2, 9, 6, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F); // Import acidCanisterGlass3
		gunModel[22].setRotationPoint(19F, -32F, 4F);

		gunModel[23].addShapeBox(0F, 0F, 0F, 2, 9, 6, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F); // Import acidCanisterGlass3
		gunModel[23].setRotationPoint(27F, -32F, 4F);

		gunModel[24].addShapeBox(0F, 0F, 0F, 1, 6, 5, 0F, 0.5F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, -1.5F, 0.5F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, -1.5F); // Import acidLiquid
		gunModel[24].setRotationPoint(24F, -29.1F, 4.5F);

		gunModel[25].addShapeBox(0F, 0F, 0F, 1, 6, 5, 0F, 0F, 0F, 0F, 0.5F, 0F, -1.5F, 0.5F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, -1.5F, 0.5F, 0F, -1.5F, 0F, 0F, 0F); // Import acidLiquid
		gunModel[25].setRotationPoint(27F, -29.1F, 4.5F);

		gunModel[26].addShapeBox(0F, 0F, 0F, 1, 6, 5, 0F, 0F, 0F, 0F, 0.5F, 0F, -1.5F, 0.5F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, -1.5F, 0.5F, 0F, -1.5F, 0F, 0F, 0F); // Import acidLiquid
		gunModel[26].setRotationPoint(19F, -29.1F, 4.5F);

		gunModel[27].addShapeBox(0F, 0F, 0F, 1, 6, 5, 0F, 0.5F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, -1.5F, 0.5F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, -1.5F); // Import acidLiquid
		gunModel[27].setRotationPoint(16F, -29.1F, 4.5F);

		gunModel[28].addBox(0F, 0F, 0F, 2, 6, 5, 0F); // Import acidLiquid2
		gunModel[28].setRotationPoint(25F, -29.1F, 4.5F);

		gunModel[29].addBox(0F, 0F, 0F, 2, 6, 5, 0F); // Import acidLiquid2
		gunModel[29].setRotationPoint(17F, -29.1F, 4.5F);

		gunModel[30].addShapeBox(0F, 0F, 0F, 12, 6, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F); // Import acidTank
		gunModel[30].setRotationPoint(0F, -18F, 8F);

		gunModel[31].addShapeBox(0F, 0F, 0F, 12, 6, 2, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import acidTank
		gunModel[31].setRotationPoint(0F, -18F, 4F);

		gunModel[32].addBox(0F, 0F, 0F, 12, 6, 2, 0F); // Import acidTank
		gunModel[32].setRotationPoint(0F, -18F, 6F);

		gunModel[33].addBox(0F, 0F, 0F, 8, 5, 1, 0F); // Import acidTankHolder
		gunModel[33].setRotationPoint(2F, -17.5F, 4F);

		gunModel[34].addShapeBox(0F, 0F, 0F, 1, 6, 2, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import ammoCasing
		gunModel[34].setRotationPoint(31F, -23.5F, -3F);

		gunModel[35].addShapeBox(0F, 0F, 0F, 1, 6, 2, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import ammoCasing
		gunModel[35].setRotationPoint(44F, -23.5F, -3F);

		gunModel[36].addShapeBox(0F, 0F, 0F, 1, 6, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F); // Import ammoCasing
		gunModel[36].setRotationPoint(44F, -23.5F, 1F);

		gunModel[37].addShapeBox(0F, 0F, 0F, 1, 6, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F); // Import ammoCasing
		gunModel[37].setRotationPoint(31F, -23.5F, 1F);

		gunModel[38].addBox(0F, 0F, 0F, 1, 6, 2, 0F); // Import ammoCasing2
		gunModel[38].setRotationPoint(31F, -23.5F, -1F);

		gunModel[39].addBox(0F, 0F, 0F, 1, 6, 2, 0F); // Import ammoCasing2
		gunModel[39].setRotationPoint(44F, -23.5F, -1F);

		gunModel[40].addShapeBox(0F, 0F, 0F, 56, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import barrel1
		gunModel[40].setRotationPoint(45F, -22F, -3F);

		gunModel[41].addBox(0F, 0F, 0F, 56, 2, 6, 0F); // Import barrel2
		gunModel[41].setRotationPoint(45F, -20F, -3F);

		gunModel[42].addShapeBox(0F, 0F, 0F, 2, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Import barrel3
		gunModel[42].setRotationPoint(99F, -18F, -3F);

		gunModel[43].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import barrelHolder
		gunModel[43].setRotationPoint(51F, -22F, -3.5F);

		gunModel[44].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import barrelHolder
		gunModel[44].setRotationPoint(94F, -22F, -3.5F);

		gunModel[45].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // Import barrelHolder2
		gunModel[45].setRotationPoint(51F, -20F, -3.5F);

		gunModel[46].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // Import barrelHolder2
		gunModel[46].setRotationPoint(94F, -20F, -3.5F);

		gunModel[47].addBox(0F, 0F, 0F, 12, 1, 1, 0F); // Import barrelRail
		gunModel[47].setRotationPoint(32F, -20F, 1F);

		gunModel[48].addBox(0F, 0F, 0F, 12, 1, 1, 0F); // Import barrelRail
		gunModel[48].setRotationPoint(32F, -20F, -2F);

		gunModel[49].addBox(0F, 0F, 0F, 25, 10, 8, 0F); // Import body1
		gunModel[49].setRotationPoint(-4F, -20F, -4F);

		gunModel[50].addShapeBox(0F, 0F, 0F, 35, 3, 8, 0F, -2F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, -2F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import body2
		gunModel[50].setRotationPoint(-4F, -23F, -4F);

		gunModel[51].addShapeBox(0F, 0F, 0F, 25, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Import body3
		gunModel[51].setRotationPoint(-4F, -10F, -4F);

		gunModel[52].addBox(0F, 0F, 0F, 10, 2, 8, 0F); // Import body4
		gunModel[52].setRotationPoint(21F, -20F, -4F);

		gunModel[53].addShapeBox(0F, 0F, 0F, 30, 8, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F); // Import body5
		gunModel[53].setRotationPoint(21F, -18F, -4F);

		gunModel[54].addShapeBox(0F, 0F, 0F, 30, 2, 8, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -2F, -1F, 0F, -2F, -1F, 0F, 0F, -1F); // Import body6
		gunModel[54].setRotationPoint(21F, -10F, -4F);

		gunModel[55].addBox(0F, 0F, 0F, 48, 6, 8, 0F); // Import body7
		gunModel[55].setRotationPoint(51F, -18F, -4F);

		gunModel[56].addShapeBox(0F, 0F, 0F, 48, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Import body8
		gunModel[56].setRotationPoint(51F, -12F, -4F);

		gunModel[57].addShapeBox(0F, 0F, 0F, 68, 1, 8, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import body9
		gunModel[57].setRotationPoint(31F, -19F, -4F);

		gunModel[58].addBox(0F, 0F, 0F, 15, 4, 1, 0F); // Import electricGrid
		gunModel[58].setRotationPoint(6F, -19F, -5F);

		gunModel[59].addShapeBox(0F, 0F, 0F, 11, 1, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import lowerBarrel
		gunModel[59].setRotationPoint(99F, -15F, -2F);

		gunModel[60].addShapeBox(0F, 0F, 0F, 11, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Import lowerBarrel
		gunModel[60].setRotationPoint(99F, -12F, -2F);

		gunModel[61].addBox(0F, 0F, 0F, 11, 2, 4, 0F); // Import lowerBarrel2
		gunModel[61].setRotationPoint(99F, -14F, -2F);

		gunModel[62].addShapeBox(0F, 0F, 0F, 11, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import muzzle
		gunModel[62].setRotationPoint(101F, -22.5F, -3.5F);

		gunModel[63].addShapeBox(0F, 0F, 0F, 11, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Import muzzle
		gunModel[63].setRotationPoint(101F, -17.5F, -3.5F);

		gunModel[64].addBox(0F, 0F, 0F, 2, 3, 2, 0F); // Import muzzle2
		gunModel[64].setRotationPoint(101F, -20.5F, -3.5F);

		gunModel[65].addBox(0F, 0F, 0F, 2, 3, 2, 0F); // Import muzzle2
		gunModel[65].setRotationPoint(110F, -20.5F, -3.5F);

		gunModel[66].addBox(0F, 0F, 0F, 2, 3, 2, 0F); // Import muzzle2
		gunModel[66].setRotationPoint(101F, -20.5F, 1.5F);

		gunModel[67].addBox(0F, 0F, 0F, 2, 3, 2, 0F); // Import muzzle2
		gunModel[67].setRotationPoint(110F, -20.5F, 1.5F);

		gunModel[68].addBox(0F, 0F, 0F, 15, 2, 2, 0F); // Import pipe1
		gunModel[68].setRotationPoint(12F, -16F, 6F);

		gunModel[69].addBox(0F, 0F, 0F, 2, 6, 2, 0F); // Import pipe2
		gunModel[69].setRotationPoint(17F, -22F, 6F);

		gunModel[70].addBox(0F, 0F, 0F, 2, 6, 2, 0F); // Import pipe2
		gunModel[70].setRotationPoint(25F, -22F, 6F);

		gunModel[71].addBox(0F, 0F, 0F, 2, 2, 1, 0F); // Import pipe3
		gunModel[71].setRotationPoint(17F, -16F, 5F);

		gunModel[72].addBox(0F, 0F, 0F, 2, 2, 1, 0F); // Import pipe3
		gunModel[72].setRotationPoint(25F, -16F, 5F);

		gunModel[73].addShapeBox(0F, 0F, 0F, 4, 2, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F); // Import pipe4
		gunModel[73].setRotationPoint(-4F, -15.5F, 6F);

		gunModel[74].addShapeBox(0F, 0F, 0F, 2, 2, 2, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import pipe5
		gunModel[74].setRotationPoint(-4F, -17.5F, 6F);

		gunModel[75].addShapeBox(0F, 0F, 0F, 2, 3, 2, 0F, 2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import pipe6
		gunModel[75].setRotationPoint(-4F, -20.5F, 4F);

		gunModel[76].addBox(0F, 0F, 0F, 2, 2, 7, 0F); // Import pipe7
		gunModel[76].setRotationPoint(-6F, -22.5F, -1F);

		gunModel[77].addBox(0F, 0F, 0F, 2, 2, 2, 0F); // Import pipe8
		gunModel[77].setRotationPoint(-4F, -22.5F, -1F);

		gunModel[78].addBox(0F, 0F, 0F, 3, 3, 1, 0F); // Import pipeConnector
		gunModel[78].setRotationPoint(16.5F, -16.5F, 4F);

		gunModel[79].addBox(0F, 0F, 0F, 3, 3, 1, 0F); // Import pipeConnector
		gunModel[79].setRotationPoint(24.5F, -16.5F, 4F);

		gunModel[80].addBox(0F, 0F, 0F, 18, 1, 4, 0F); // Import rail1
		gunModel[80].setRotationPoint(8F, -24F, -2F);

		gunModel[81].addBox(0F, 0F, 0F, 18, 1, 6, 0F); // Import rail2
		gunModel[81].setRotationPoint(8F, -25F, -3F);

		gunModel[82].addShapeBox(0F, 0F, 0F, 18, 1, 1, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import rail3
		gunModel[82].setRotationPoint(8F, -26F, 2F);

		gunModel[83].addShapeBox(0F, 0F, 0F, 18, 1, 1, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import rail3-2
		gunModel[83].setRotationPoint(8F, -26F, -3F);

		gunModel[84].addShapeBox(0F, 0F, 0F, 1, 5, 3, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Import sparkCanister
		gunModel[84].setRotationPoint(20F, -19F, -11F);
		gunModel[84].rotateAngleX = 1.04719755F;

		gunModel[85].addShapeBox(0F, 0F, 0F, 1, 5, 3, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Import sparkCanister
		gunModel[85].setRotationPoint(18F, -19F, -11F);
		gunModel[85].rotateAngleX = 1.04719755F;

		gunModel[86].addShapeBox(0F, 0F, 0F, 1, 5, 3, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Import sparkCanister
		gunModel[86].setRotationPoint(16F, -19F, -11F);
		gunModel[86].rotateAngleX = 1.04719755F;

		gunModel[87].addShapeBox(0F, 0F, 0F, 1, 5, 3, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Import sparkCanister
		gunModel[87].setRotationPoint(14F, -19F, -11F);
		gunModel[87].rotateAngleX = 1.04719755F;

		gunModel[88].addShapeBox(0F, 0F, 0F, 1, 5, 3, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Import sparkCanister
		gunModel[88].setRotationPoint(12F, -19F, -11F);
		gunModel[88].rotateAngleX = 1.04719755F;

		gunModel[89].addShapeBox(0F, 0F, 0F, 1, 5, 3, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Import sparkCanister
		gunModel[89].setRotationPoint(8F, -19F, -11F);
		gunModel[89].rotateAngleX = 1.04719755F;

		gunModel[90].addShapeBox(0F, 0F, 0F, 1, 5, 3, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Import sparkCanister
		gunModel[90].setRotationPoint(10F, -19F, -11F);
		gunModel[90].rotateAngleX = 1.04719755F;

		gunModel[91].addShapeBox(0F, 0F, 0F, 1, 5, 3, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Import sparkCanister
		gunModel[91].setRotationPoint(6F, -19F, -11F);
		gunModel[91].rotateAngleX = 1.04719755F;

		gunModel[92].addBox(0F, 0F, 0F, 1, 5, 3, 0F); // Import sparkCanister2
		gunModel[92].setRotationPoint(7F, -19F, -11F);
		gunModel[92].rotateAngleX = 1.04719755F;

		gunModel[93].addBox(0F, 0F, 0F, 1, 5, 3, 0F); // Import sparkCanister2
		gunModel[93].setRotationPoint(11F, -19F, -11F);
		gunModel[93].rotateAngleX = 1.04719755F;

		gunModel[94].addBox(0F, 0F, 0F, 1, 5, 3, 0F); // Import sparkCanister2
		gunModel[94].setRotationPoint(15F, -19F, -11F);
		gunModel[94].rotateAngleX = 1.04719755F;

		gunModel[95].addBox(0F, 0F, 0F, 1, 5, 3, 0F); // Import sparkCanister2
		gunModel[95].setRotationPoint(19F, -19F, -11F);
		gunModel[95].rotateAngleX = 1.04719755F;

		gunModel[96].addBox(0F, 0F, 0F, 1, 3, 1, 0F); // Import sparkPin
		gunModel[96].setRotationPoint(11F, -17.5F, -6.5F);
		gunModel[96].rotateAngleX = 1.04719755F;

		gunModel[97].addBox(0F, 0F, 0F, 1, 3, 1, 0F); // Import sparkPin
		gunModel[97].setRotationPoint(19F, -17.5F, -6.5F);
		gunModel[97].rotateAngleX = 1.04719755F;

		gunModel[98].addBox(0F, 0F, 0F, 1, 3, 1, 0F); // Import sparkPin
		gunModel[98].setRotationPoint(15F, -17.5F, -6.5F);
		gunModel[98].rotateAngleX = 1.04719755F;

		gunModel[99].addBox(0F, 0F, 0F, 1, 3, 1, 0F); // Import sparkPin
		gunModel[99].setRotationPoint(7F, -17.5F, -6.5F);
		gunModel[99].rotateAngleX = 1.04719755F;

		gunModel[100].addBox(0F, 0F, 0F, 1, 1, 2, 0F); // Import wire1
		gunModel[100].setRotationPoint(42F, -19.5F, -3.5F);

		gunModel[101].addShapeBox(0F, 0F, 0F, 1, 4, 1, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import wire2
		gunModel[101].setRotationPoint(42F, -19.5F, -4.5F);

		gunModel[102].addShapeBox(0F, 0F, 0F, 1, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F); // Import wire3
		gunModel[102].setRotationPoint(42F, -15.5F, -4.5F);

		gunModel[103].addShapeBox(0F, 0F, 0F, 10, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F); // Import wire4
		gunModel[103].setRotationPoint(32F, -13.5F, -4.5F);

		gunModel[104].addShapeBox(0F, 0F, 0F, 5, 1, 1, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F); // Import wire5
		gunModel[104].setRotationPoint(27F, -13.5F, -4.5F);

		gunModel[105].addBox(0F, 0F, 0F, 6, 1, 1, 0F); // Import wire6
		gunModel[105].setRotationPoint(21F, -16.5F, -4.5F);


		defaultScopeModel = new ModelRendererTurbo[6];
		defaultScopeModel[0] = new ModelRendererTurbo(this, 6, 1, textureX, textureY); // Import ironSight
		defaultScopeModel[1] = new ModelRendererTurbo(this, 6, 1, textureX, textureY); // Import ironSight
		defaultScopeModel[2] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Import ironSight2
		defaultScopeModel[3] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Import ironSight2
		defaultScopeModel[4] = new ModelRendererTurbo(this, 1, 7, textureX, textureY); // Import ironSight3
		defaultScopeModel[5] = new ModelRendererTurbo(this, 14, 8, textureX, textureY); // Import ironSightBack

		defaultScopeModel[0].addShapeBox(0F, 0F, 0F, 1, 1, 4, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import ironSight
		defaultScopeModel[0].setRotationPoint(3F, -25F, -2F);

		defaultScopeModel[1].addShapeBox(0F, 0F, 0F, 1, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F); // Import ironSight
		defaultScopeModel[1].setRotationPoint(3F, -30F, -2F);

		defaultScopeModel[2].addBox(0F, 0F, 0F, 1, 4, 1, 0F); // Import ironSight2
		defaultScopeModel[2].setRotationPoint(3F, -29F, -3F);

		defaultScopeModel[3].addBox(0F, 0F, 0F, 1, 4, 1, 0F); // Import ironSight2
		defaultScopeModel[3].setRotationPoint(3F, -29F, 2F);

		defaultScopeModel[4].addShapeBox(0F, 0F, 0F, 3, 4, 3, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F); // Import ironSight3
		defaultScopeModel[4].setRotationPoint(94F, -26.5F, -1F);

		defaultScopeModel[5].addBox(0F, 0F, 0F, 6, 2, 4, 0F); // Import ironSightBack
		defaultScopeModel[5].setRotationPoint(-1F, -24F, -2F);


		defaultStockModel = new ModelRendererTurbo[8];
		defaultStockModel[0] = new ModelRendererTurbo(this, 1, 80, textureX, textureY); // Import stock1
		defaultStockModel[1] = new ModelRendererTurbo(this, 1, 97, textureX, textureY); // Import stock2
		defaultStockModel[2] = new ModelRendererTurbo(this, 38, 120, textureX, textureY); // Import stock3
		defaultStockModel[3] = new ModelRendererTurbo(this, 38, 120, textureX, textureY); // Import stock3
		defaultStockModel[4] = new ModelRendererTurbo(this, 1, 121, textureX, textureY); // Import stock4
		defaultStockModel[5] = new ModelRendererTurbo(this, 1, 109, textureX, textureY); // Import stock5
		defaultStockModel[6] = new ModelRendererTurbo(this, 1, 138, textureX, textureY); // Import stock6
		defaultStockModel[7] = new ModelRendererTurbo(this, 22, 138, textureX, textureY); // Import stock7

		defaultStockModel[0].addShapeBox(0F, 0F, 0F, 26, 8, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 10F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 10F, 0F); // Import stock1
		defaultStockModel[0].setRotationPoint(-40F, -13F, -4F);

		defaultStockModel[1].addShapeBox(0F, 0F, 0F, 28, 3, 8, 0F, 0F, 0F, -1.5F, -3F, 0F, -1.5F, -3F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import stock2
		defaultStockModel[1].setRotationPoint(-42F, -16F, -4F);

		defaultStockModel[2].addShapeBox(0F, 0F, 0F, 10, 2, 8, 0F, 0F, 0F, 0F, 0F, 5F, 0F, 0F, 5F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -5F, -1F, 0F, -5F, -1F, 0F, 0F, -1F); // Import stock3
		defaultStockModel[2].setRotationPoint(-14F, -5F, -4F);

		defaultStockModel[3].addShapeBox(0F, 0F, 0F, 10, 2, 8, 0F, 0F, -5F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, -5F, -1.5F, 0F, 5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 5F, 0F); // Import stock3
		defaultStockModel[3].setRotationPoint(-14F, -20F, -4F);

		defaultStockModel[4].addShapeBox(0F, 0F, 0F, 10, 8, 8, 0F, 0F, 0F, 0F, 0F, 5F, 0F, 0F, 5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -5F, 0F, 0F, -5F, 0F, 0F, 0F, 0F); // Import stock4
		defaultStockModel[4].setRotationPoint(-14F, -13F, -4F);

		defaultStockModel[5].addShapeBox(0F, 0F, 0F, 26, 2, 8, 0F, 0F, -10F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -10F, 0F, 0F, 10F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 10F, -1F); // Import stock5
		defaultStockModel[5].setRotationPoint(-40F, -5F, -4F);

		defaultStockModel[6].addBox(0F, 0F, 0F, 2, 18, 8, 0F); // Import stock6
		defaultStockModel[6].setRotationPoint(-42F, -13F, -4F);

		defaultStockModel[7].addShapeBox(0F, 0F, 0F, 2, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Import stock7
		defaultStockModel[7].setRotationPoint(-42F, 5F, -4F);


		ammoModel = new ModelRendererTurbo[3];
		ammoModel[0] = new ModelRendererTurbo(this, 52, 28, textureX, textureY); // Import canister
		ammoModel[1] = new ModelRendererTurbo(this, 52, 28, textureX, textureY); // Import canister
		ammoModel[2] = new ModelRendererTurbo(this, 52, 28, textureX, textureY); // Import canister

		ammoModel[0].addBox(0F, 0F, 0F, 12, 6, 2, 0F); // Import canister
		ammoModel[0].setRotationPoint(32F, -23F, -1F);

		ammoModel[1].addShapeBox(0F, 0F, 0F, 12, 6, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F); // Import canister
		ammoModel[1].setRotationPoint(32F, -23F, 1F);

		ammoModel[2].addShapeBox(0F, 0F, 0F, 12, 6, 2, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import canister
		ammoModel[2].setRotationPoint(32F, -23F, -3F);

		stockAttachPoint = new Vector3f(-4F /16F, 15F /16F, 0F /16F);
		scopeAttachPoint = new Vector3f(17F /16F, 23F /16F, 0F /16F);

		gunSlideDistance = 0F;
		animationType = EnumAnimationType.CUSTOM;

		/* ----Start of Reload Block---- */
		/* Reload Name: Alternate Pistol Clip */
		tiltGun = -30F;
		rotateGunVertical = 10F;
		translateGun = new Vector3f(0F, -0.1F, -0.2F);
		
		rotateClipVertical = -15F;
		translateClip = new Vector3f(-6F, 0.25F, 1.5F);
		/* ----End of Reload Block---- */

		translateAll(0F, 0F, 0F);


		flipAll();
	}
}