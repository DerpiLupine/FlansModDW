package com.flansmod.client.model.dwfirearms; //Path where the model is located

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelStrikeBattleham extends ModelGun
{
	int textureX = 512;
	int textureY = 256;

	public ModelStrikeBattleham()
	{
		gunModel = new ModelRendererTurbo[39];
		gunModel[0] = new ModelRendererTurbo(this, 153, 72, textureX, textureY); // Box 22
		gunModel[1] = new ModelRendererTurbo(this, 126, 72, textureX, textureY); // Box 0
		gunModel[2] = new ModelRendererTurbo(this, 126, 72, textureX, textureY); // Box 1
		gunModel[3] = new ModelRendererTurbo(this, 159, 4, textureX, textureY); // Box 2
		gunModel[4] = new ModelRendererTurbo(this, 126, 4, textureX, textureY); // Box 3
		gunModel[5] = new ModelRendererTurbo(this, 159, 4, textureX, textureY); // Box 4
		gunModel[6] = new ModelRendererTurbo(this, 126, 36, textureX, textureY); // Box 5
		gunModel[7] = new ModelRendererTurbo(this, 153, 36, textureX, textureY); // Box 6
		gunModel[8] = new ModelRendererTurbo(this, 126, 36, textureX, textureY); // Box 7
		gunModel[9] = new ModelRendererTurbo(this, 57, 73, textureX, textureY); // Box 0
		gunModel[10] = new ModelRendererTurbo(this, 57, 73, textureX, textureY); // Box 1
		gunModel[11] = new ModelRendererTurbo(this, 57, 58, textureX, textureY); // Box 2
		gunModel[12] = new ModelRendererTurbo(this, 30, 58, textureX, textureY); // Box 3
		gunModel[13] = new ModelRendererTurbo(this, 1, 58, textureX, textureY); // Box 4
		gunModel[14] = new ModelRendererTurbo(this, 30, 58, textureX, textureY); // Box 5
		gunModel[15] = new ModelRendererTurbo(this, 57, 73, textureX, textureY); // Box 6
		gunModel[16] = new ModelRendererTurbo(this, 57, 58, textureX, textureY); // Box 7
		gunModel[17] = new ModelRendererTurbo(this, 57, 73, textureX, textureY); // Box 8
		gunModel[18] = new ModelRendererTurbo(this, 57, 73, textureX, textureY); // Box 9
		gunModel[19] = new ModelRendererTurbo(this, 57, 58, textureX, textureY); // Box 10
		gunModel[20] = new ModelRendererTurbo(this, 57, 73, textureX, textureY); // Box 11
		gunModel[21] = new ModelRendererTurbo(this, 1, 36, textureX, textureY); // Box 13
		gunModel[22] = new ModelRendererTurbo(this, 1, 36, textureX, textureY); // Box 14
		gunModel[23] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 15
		gunModel[24] = new ModelRendererTurbo(this, 1, 87, textureX, textureY); // Box 16
		gunModel[25] = new ModelRendererTurbo(this, 30, 87, textureX, textureY); // Box 17
		gunModel[26] = new ModelRendererTurbo(this, 1, 87, textureX, textureY); // Box 18
		gunModel[27] = new ModelRendererTurbo(this, 90, 73, textureX, textureY); // Box 19
		gunModel[28] = new ModelRendererTurbo(this, 90, 73, textureX, textureY); // Box 20
		gunModel[29] = new ModelRendererTurbo(this, 90, 58, textureX, textureY); // Box 21
		gunModel[30] = new ModelRendererTurbo(this, 64, 88, textureX, textureY); // Box 22
		gunModel[31] = new ModelRendererTurbo(this, 93, 88, textureX, textureY); // Box 23
		gunModel[32] = new ModelRendererTurbo(this, 64, 88, textureX, textureY); // Box 24
		gunModel[33] = new ModelRendererTurbo(this, 1, 101, textureX, textureY); // Box 25
		gunModel[34] = new ModelRendererTurbo(this, 40, 114, textureX, textureY); // Box 26
		gunModel[35] = new ModelRendererTurbo(this, 40, 114, textureX, textureY); // Box 27
		gunModel[36] = new ModelRendererTurbo(this, 40, 114, textureX, textureY); // Box 28
		gunModel[37] = new ModelRendererTurbo(this, 1, 101, textureX, textureY); // Box 29
		gunModel[38] = new ModelRendererTurbo(this, 40, 114, textureX, textureY); // Box 30

		gunModel[0].addBox(-8F, -17F, -5F, 4, 25, 10, 0F); // Box 22
		gunModel[0].setRotationPoint(6F, 15F, 0F);
		gunModel[0].rotateAngleZ = 0.08726646F;

		gunModel[1].addShapeBox(-4F, -17F, -5F, 3, 25, 10, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F); // Box 0
		gunModel[1].setRotationPoint(6F, 15F, 0F);
		gunModel[1].rotateAngleZ = 0.08726646F;

		gunModel[2].addShapeBox(-11F, -17F, -5F, 3, 25, 10, 0F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F); // Box 1
		gunModel[2].setRotationPoint(6F, 15F, 0F);
		gunModel[2].rotateAngleZ = 0.08726646F;

		gunModel[3].addShapeBox(-11.5F, -37F, -5.5F, 3, 20, 11, 0F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F); // Box 2
		gunModel[3].setRotationPoint(6F, 15F, 0F);
		gunModel[3].rotateAngleZ = 0.08726646F;

		gunModel[4].addBox(-8.5F, -37F, -5.5F, 5, 20, 11, 0F); // Box 3
		gunModel[4].setRotationPoint(6F, 15F, 0F);
		gunModel[4].rotateAngleZ = 0.08726646F;

		gunModel[5].addShapeBox(-3.5F, -37F, -5.5F, 3, 20, 11, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F); // Box 4
		gunModel[5].setRotationPoint(6F, 15F, 0F);
		gunModel[5].rotateAngleZ = 0.08726646F;

		gunModel[6].addShapeBox(-4F, -62F, -5F, 3, 25, 10, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F); // Box 5
		gunModel[6].setRotationPoint(6F, 15F, 0F);
		gunModel[6].rotateAngleZ = 0.08726646F;

		gunModel[7].addBox(-8F, -62F, -5F, 4, 25, 10, 0F); // Box 6
		gunModel[7].setRotationPoint(6F, 15F, 0F);
		gunModel[7].rotateAngleZ = 0.08726646F;

		gunModel[8].addShapeBox(-11F, -62F, -5F, 3, 25, 10, 0F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F); // Box 7
		gunModel[8].setRotationPoint(6F, 15F, 0F);
		gunModel[8].rotateAngleZ = 0.08726646F;

		gunModel[9].addShapeBox(-11.5F, -65F, -5.5F, 3, 3, 11, 0F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F); // Box 0
		gunModel[9].setRotationPoint(6F, 15F, 0F);
		gunModel[9].rotateAngleZ = 0.08726646F;

		gunModel[10].addShapeBox(-3.5F, -65F, -5.5F, 3, 3, 11, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F); // Box 1
		gunModel[10].setRotationPoint(6F, 15F, 0F);
		gunModel[10].rotateAngleZ = 0.08726646F;

		gunModel[11].addBox(-8.5F, -65F, -5.5F, 5, 3, 11, 0F); // Box 2
		gunModel[11].setRotationPoint(6F, 15F, 0F);
		gunModel[11].rotateAngleZ = 0.08726646F;

		gunModel[12].addShapeBox(-4F, -83F, -5F, 3, 18, 10, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F); // Box 3
		gunModel[12].setRotationPoint(6F, 15F, 0F);
		gunModel[12].rotateAngleZ = 0.08726646F;

		gunModel[13].addBox(-8F, -83F, -5F, 4, 18, 10, 0F); // Box 4
		gunModel[13].setRotationPoint(6F, 15F, 0F);
		gunModel[13].rotateAngleZ = 0.08726646F;

		gunModel[14].addShapeBox(-11F, -83F, -5F, 3, 18, 10, 0F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F); // Box 5
		gunModel[14].setRotationPoint(6F, 15F, 0F);
		gunModel[14].rotateAngleZ = 0.08726646F;

		gunModel[15].addShapeBox(-3.5F, -71F, -5.5F, 3, 3, 11, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F); // Box 6
		gunModel[15].setRotationPoint(6F, 15F, 0F);
		gunModel[15].rotateAngleZ = 0.08726646F;

		gunModel[16].addBox(-8.5F, -71F, -5.5F, 5, 3, 11, 0F); // Box 7
		gunModel[16].setRotationPoint(6F, 15F, 0F);
		gunModel[16].rotateAngleZ = 0.08726646F;

		gunModel[17].addShapeBox(-11.5F, -71F, -5.5F, 3, 3, 11, 0F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F); // Box 8
		gunModel[17].setRotationPoint(6F, 15F, 0F);
		gunModel[17].rotateAngleZ = 0.08726646F;

		gunModel[18].addShapeBox(-3.5F, -77F, -5.5F, 3, 3, 11, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F); // Box 9
		gunModel[18].setRotationPoint(6F, 15F, 0F);
		gunModel[18].rotateAngleZ = 0.08726646F;

		gunModel[19].addBox(-8.5F, -77F, -5.5F, 5, 3, 11, 0F); // Box 10
		gunModel[19].setRotationPoint(6F, 15F, 0F);
		gunModel[19].rotateAngleZ = 0.08726646F;

		gunModel[20].addShapeBox(-11.5F, -77F, -5.5F, 3, 3, 11, 0F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F); // Box 11
		gunModel[20].setRotationPoint(6F, 15F, 0F);
		gunModel[20].rotateAngleZ = 0.08726646F;

		gunModel[21].addShapeBox(-28F, -86F, -9F, 44, 3, 18, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 13
		gunModel[21].setRotationPoint(6F, 15F, 0F);
		gunModel[21].rotateAngleZ = 0.08726646F;

		gunModel[22].addShapeBox(-28F, -105F, -9F, 44, 3, 18, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 14
		gunModel[22].setRotationPoint(6F, 15F, 0F);
		gunModel[22].rotateAngleZ = 0.08726646F;

		gunModel[23].addShapeBox(-28F, -102F, -9F, 44, 16, 18, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 15
		gunModel[23].setRotationPoint(6F, 15F, 0F);
		gunModel[23].rotateAngleZ = 0.08726646F;

		gunModel[24].addShapeBox(-11.5F, -107F, -5.5F, 3, 2, 11, 0F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F); // Box 16
		gunModel[24].setRotationPoint(6F, 15F, 0F);
		gunModel[24].rotateAngleZ = 0.08726646F;

		gunModel[25].addBox(-8.5F, -107F, -5.5F, 5, 2, 11, 0F); // Box 17
		gunModel[25].setRotationPoint(6F, 15F, 0F);
		gunModel[25].rotateAngleZ = 0.08726646F;

		gunModel[26].addShapeBox(-3.5F, -107F, -5.5F, 3, 2, 11, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F); // Box 18
		gunModel[26].setRotationPoint(6F, 15F, 0F);
		gunModel[26].rotateAngleZ = 0.08726646F;

		gunModel[27].addShapeBox(-3.5F, 8F, -5.5F, 3, 3, 11, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F); // Box 19
		gunModel[27].setRotationPoint(6F, 15F, 0F);
		gunModel[27].rotateAngleZ = 0.08726646F;

		gunModel[28].addShapeBox(-11.5F, 8F, -5.5F, 3, 3, 11, 0F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F); // Box 20
		gunModel[28].setRotationPoint(6F, 15F, 0F);
		gunModel[28].rotateAngleZ = 0.08726646F;

		gunModel[29].addBox(-8.5F, 8F, -5.5F, 5, 3, 11, 0F); // Box 21
		gunModel[29].setRotationPoint(6F, 15F, 0F);
		gunModel[29].rotateAngleZ = 0.08726646F;

		gunModel[30].addShapeBox(-11.5F, 11F, -5.5F, 3, 2, 11, 0F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, -1F, 0F, -3F, 0F, 0F, -1F, 0F, 0F, -1F, -1F, 0F, -3F); // Box 22
		gunModel[30].setRotationPoint(6F, 15F, 0F);
		gunModel[30].rotateAngleZ = 0.08726646F;

		gunModel[31].addShapeBox(-8.5F, 11F, -5.5F, 5, 2, 11, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 23
		gunModel[31].setRotationPoint(6F, 15F, 0F);
		gunModel[31].rotateAngleZ = 0.08726646F;

		gunModel[32].addShapeBox(-3.5F, 11F, -5.5F, 3, 2, 11, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, -1F, -1F, 0F, -3F, -1F, 0F, -3F, 0F, 0F, -1F); // Box 24
		gunModel[32].setRotationPoint(6F, 15F, 0F);
		gunModel[32].rotateAngleZ = 0.08726646F;

		gunModel[33].addShapeBox(-29F, -102F, -9F, 1, 16, 18, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 25
		gunModel[33].setRotationPoint(6F, 15F, 0F);
		gunModel[33].rotateAngleZ = 0.08726646F;

		gunModel[34].addShapeBox(-29F, -86F, -9F, 1, 3, 18, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -1F, -3F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, -1F, -3F); // Box 26
		gunModel[34].setRotationPoint(6F, 15F, 0F);
		gunModel[34].rotateAngleZ = 0.08726646F;

		gunModel[35].addShapeBox(-29F, -105F, -9F, 1, 3, 18, 0F, 0F, -1F, -3F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, -1F, -3F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 27
		gunModel[35].setRotationPoint(6F, 15F, 0F);
		gunModel[35].rotateAngleZ = 0.08726646F;

		gunModel[36].addShapeBox(16F, -86F, -9F, 1, 3, 18, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, -1F, -3F, 0F, -1F, -3F, 0F, 0F, -2F); // Box 28
		gunModel[36].setRotationPoint(6F, 15F, 0F);
		gunModel[36].rotateAngleZ = 0.08726646F;

		gunModel[37].addShapeBox(16F, -102F, -9F, 1, 16, 18, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 29
		gunModel[37].setRotationPoint(6F, 15F, 0F);
		gunModel[37].rotateAngleZ = 0.08726646F;

		gunModel[38].addShapeBox(16F, -105F, -9F, 1, 3, 18, 0F, 0F, 0F, -2F, 0F, -1F, -3F, 0F, -1F, -3F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 30
		gunModel[38].setRotationPoint(6F, 15F, 0F);
		gunModel[38].rotateAngleZ = 0.08726646F;



		translateAll(0F, 0F, 0F);


		flipAll();
	}
}