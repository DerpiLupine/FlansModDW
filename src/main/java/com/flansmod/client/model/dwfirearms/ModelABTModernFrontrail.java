package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.ModelAttachment;
import com.flansmod.client.tmt.ModelRendererTurbo;

import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelABTModernFrontrail extends ModelAttachment
{
	int textureX = 512;
	int textureY = 256;

	public ModelABTModernFrontrail()
	{
		attachmentModel = new ModelRendererTurbo[6];
		attachmentModel[0] = new ModelRendererTurbo(this, 1, 121, textureX, textureY); // topBody
		attachmentModel[1] = new ModelRendererTurbo(this, 1, 121, textureX, textureY); // bottomBody
		attachmentModel[2] = new ModelRendererTurbo(this, 1, 100, textureX, textureY); // body1
		attachmentModel[3] = new ModelRendererTurbo(this, 1, 132, textureX, textureY); // grip
		attachmentModel[4] = new ModelRendererTurbo(this, 1, 149, textureX, textureY); // grip2
		attachmentModel[5] = new ModelRendererTurbo(this, 82, 109, textureX, textureY); // body2

		attachmentModel[0].addShapeBox(0F, -8F, -4F, 32, 2, 8, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // topBody
		attachmentModel[0].setRotationPoint(0F, 0F, 0F);

		attachmentModel[1].addShapeBox(0F, 6F, -4F, 32, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // bottomBody
		attachmentModel[1].setRotationPoint(0F, 0F, 0F);

		attachmentModel[2].addBox(0F, -6F, -4F, 32, 12, 8, 0F); // body1
		attachmentModel[2].setRotationPoint(0F, 0F, 0F);

		attachmentModel[3].addBox(0F, 1F, -5F, 29, 6, 10, 0F); // grip
		attachmentModel[3].setRotationPoint(0F, 0F, 0F);

		attachmentModel[4].addShapeBox(0F, 7F, -5F, 29, 2, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // grip2
		attachmentModel[4].setRotationPoint(0F, 0F, 0F);

		attachmentModel[5].addShapeBox(24F, -9F, -4F, 5, 3, 8, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body2
		attachmentModel[5].setRotationPoint(0F, 0F, 0F);

		renderOffset = 0F;

		//Based off the Ambircon SteamRifle positioning.

		flipAll();
	}
}

