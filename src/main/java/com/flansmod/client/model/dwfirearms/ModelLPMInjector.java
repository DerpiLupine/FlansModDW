package com.flansmod.client.model.dwfirearms;

import net.minecraft.client.model.ModelBase;
import net.minecraft.entity.Entity;

import com.flansmod.client.tmt.ModelRendererTurbo;

public class ModelLPMInjector extends ModelBase 
{
	public ModelRendererTurbo[] grenadeModel;

	int textureX = 32;
	int textureY = 16;

	public ModelLPMInjector()
	{
		grenadeModel = new ModelRendererTurbo[7];
		grenadeModel[0] = new ModelRendererTurbo(this, 6, 4, textureX, textureY); // lever1
		grenadeModel[1] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // buttonTop1
		grenadeModel[2] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // buttonTop2
		grenadeModel[3] = new ModelRendererTurbo(this, 6, 1, textureX, textureY); // Box 0
		grenadeModel[4] = new ModelRendererTurbo(this, 6, 1, textureX, textureY); // Box 1
		grenadeModel[5] = new ModelRendererTurbo(this, 1, 11, textureX, textureY); // Box 2
		grenadeModel[6] = new ModelRendererTurbo(this, 1, 8, textureX, textureY); // Box 3

		grenadeModel[0].addShapeBox(-0.5F, -2.5F, -0.7F, 1, 1, 1, 0F,0.1F, -0.25F, 0F,0.1F, -0.25F, 0F,0.1F, -0.25F, 0.3F,0.1F, -0.25F, 0.3F,0.1F, 0F, 0F,0.1F, 0F, 0F,0.1F, 0F, 0.3F,0.1F, 0F, 0.3F); // lever1
		grenadeModel[1].addShapeBox(-0.5F, -2.8F, -0.5F, 1, 5, 1, 0F,0F, 0F, -0.25F,-0.5F, 0F, 0F,-0.5F, 0F, 0F,0F, 0F, -0.25F,0F, 0F, -0.25F,-0.5F, 0F, 0F,-0.5F, 0F, 0F,0F, 0F, -0.25F); // buttonTop1
		grenadeModel[2].addShapeBox(-0.5F, -2.8F, -0.5F, 1, 5, 1, 0F,-0.5F, 0F, 0F,0F, 0F, -0.25F,0F, 0F, -0.25F,-0.5F, 0F, 0F,-0.5F, 0F, 0F,0F, 0F, -0.25F,0F, 0F, -0.25F,-0.5F, 0F, 0F); // buttonTop2
		grenadeModel[3].addShapeBox(-0.5F, -4F, -0.5F, 1, 1, 1, 0F,0F, 0F, -0.25F,-0.5F, 0F, 0F,-0.5F, 0F, 0F,0F, 0F, -0.25F,0F, 0F, -0.25F,-0.5F, 0F, 0F,-0.5F, 0F, 0F,0F, 0F, -0.25F); // Box 0
		grenadeModel[4].addShapeBox(-0.5F, -4F, -0.5F, 1, 1, 1, 0F,-0.5F, 0F, 0F,0F, 0F, -0.25F,0F, 0F, -0.25F,-0.5F, 0F, 0F,-0.5F, 0F, 0F,0F, 0F, -0.25F,0F, 0F, -0.25F,-0.5F, 0F, 0F); // Box 1
		grenadeModel[5].addShapeBox(-0.5F, 1.5F, -0.5F, 1, 1, 1, 0F,-0.25F, -0.25F, -0.25F,-0.25F, -0.25F, -0.25F,-0.25F, -0.25F, -0.25F,-0.25F, -0.25F, -0.25F,-0.25F, 0F, -0.25F,-0.25F, 0F, -0.25F,-0.25F, 0F, -0.25F,-0.25F, 0F, -0.25F); // Box 2
		grenadeModel[6].addShapeBox(-0.5F, -3.5F, -0.5F, 1, 1, 1, 0F,-0.25F, -0.25F, -0.25F,-0.25F, -0.25F, -0.25F,-0.25F, -0.25F, -0.25F,-0.25F, -0.25F, -0.25F,-0.25F, 0F, -0.25F,-0.25F, 0F, -0.25F,-0.25F, 0F, -0.25F,-0.25F, 0F, -0.25F); // Box 3

		for(int i = 0; i < 7; i++)
			grenadeModel[i].rotateAngleZ = 3.14159265F;
	}

	@Override
	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
	{
		for(ModelRendererTurbo mineModelBit : grenadeModel)
			mineModelBit.render(f5);
	}
}