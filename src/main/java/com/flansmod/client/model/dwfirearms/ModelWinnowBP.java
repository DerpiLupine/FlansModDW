package com.flansmod.client.model.dwfirearms; //Path where the model is located

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelWinnowBP extends ModelGun //Same as Filename
{
	int textureX = 512;
	int textureY = 256;

	public ModelWinnowBP() //Same as Filename
	{
		gunModel = new ModelRendererTurbo[37];
		gunModel[0] = new ModelRendererTurbo(this, 88, 9, textureX, textureY); // barrelMiddle
		gunModel[1] = new ModelRendererTurbo(this, 116, 50, textureX, textureY); // boltPart1
		gunModel[2] = new ModelRendererTurbo(this, 116, 50, textureX, textureY); // boltPart2
		gunModel[3] = new ModelRendererTurbo(this, 1, 72, textureX, textureY); // gripPart1
		gunModel[4] = new ModelRendererTurbo(this, 1, 62, textureX, textureY); // gripPart2
		gunModel[5] = new ModelRendererTurbo(this, 36, 62, textureX, textureY); // gripPart3
		gunModel[6] = new ModelRendererTurbo(this, 163, 9, textureX, textureY); // ironSightFixed
		gunModel[7] = new ModelRendererTurbo(this, 75, 1, textureX, textureY); // Box 12
		gunModel[8] = new ModelRendererTurbo(this, 75, 1, textureX, textureY); // Box 13
		gunModel[9] = new ModelRendererTurbo(this, 75, 1, textureX, textureY); // Box 14
		gunModel[10] = new ModelRendererTurbo(this, 75, 9, textureX, textureY); // Box 15
		gunModel[11] = new ModelRendererTurbo(this, 75, 9, textureX, textureY); // Box 16
		gunModel[12] = new ModelRendererTurbo(this, 131, 12, textureX, textureY); // Box 17
		gunModel[13] = new ModelRendererTurbo(this, 131, 5, textureX, textureY); // Box 20
		gunModel[14] = new ModelRendererTurbo(this, 131, 1, textureX, textureY); // Box 21
		gunModel[15] = new ModelRendererTurbo(this, 131, 9, textureX, textureY); // Box 22
		gunModel[16] = new ModelRendererTurbo(this, 75, 37, textureX, textureY); // Box 2
		gunModel[17] = new ModelRendererTurbo(this, 75, 28, textureX, textureY); // Box 3
		gunModel[18] = new ModelRendererTurbo(this, 1, 19, textureX, textureY); // Box 4
		gunModel[19] = new ModelRendererTurbo(this, 36, 72, textureX, textureY); // Box 56
		gunModel[20] = new ModelRendererTurbo(this, 1, 39, textureX, textureY); // Box 57
		gunModel[21] = new ModelRendererTurbo(this, 49, 40, textureX, textureY); // Box 58
		gunModel[22] = new ModelRendererTurbo(this, 34, 39, textureX, textureY); // Box 59
		gunModel[23] = new ModelRendererTurbo(this, 36, 72, textureX, textureY); // Box 60
		gunModel[24] = new ModelRendererTurbo(this, 1, 82, textureX, textureY); // Box 64
		gunModel[25] = new ModelRendererTurbo(this, 1, 82, textureX, textureY); // Box 65
		gunModel[26] = new ModelRendererTurbo(this, 22, 82, textureX, textureY); // Box 66
		gunModel[27] = new ModelRendererTurbo(this, 37, 87, textureX, textureY); // Box 67
		gunModel[28] = new ModelRendererTurbo(this, 22, 87, textureX, textureY); // Box 68
		gunModel[29] = new ModelRendererTurbo(this, 22, 82, textureX, textureY); // Box 70
		gunModel[30] = new ModelRendererTurbo(this, 37, 87, textureX, textureY); // Box 71
		gunModel[31] = new ModelRendererTurbo(this, 22, 87, textureX, textureY); // Box 72
		gunModel[32] = new ModelRendererTurbo(this, 63, 13, textureX, textureY); // Box 76
		gunModel[33] = new ModelRendererTurbo(this, 31, 91, textureX, textureY); // Box 82
		gunModel[34] = new ModelRendererTurbo(this, 22, 94, textureX, textureY); // Box 83
		gunModel[35] = new ModelRendererTurbo(this, 22, 91, textureX, textureY); // Box 85
		gunModel[36] = new ModelRendererTurbo(this, 22, 94, textureX, textureY); // Box 86

		gunModel[0].addBox(0F, 0F, 0F, 4, 6, 2, 0F); // barrelMiddle
		gunModel[0].setRotationPoint(36F, -27F, -1F);

		gunModel[1].addShapeBox(0F, 0F, 0F, 1, 3, 7, 0F, 0F, -1F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1F, 0F); // boltPart1
		gunModel[1].setRotationPoint(-6.5F, -16.5F, -3.5F);

		gunModel[2].addShapeBox(0F, 0F, 0F, 1, 3, 7, 0F, 0F, -0.5F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -0.5F, 0F); // boltPart2
		gunModel[2].setRotationPoint(-5.5F, -16.5F, -3.5F);

		gunModel[3].addShapeBox(0F, 0F, 0F, 11, 3, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // gripPart1
		gunModel[3].setRotationPoint(-7F, -16F, -3F);

		gunModel[4].addShapeBox(0F, 0F, 0F, 11, 3, 6, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // gripPart2
		gunModel[4].setRotationPoint(-9F, -13F, -3F);

		gunModel[5].addShapeBox(0F, 0F, 0F, 2, 3, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F); // gripPart3
		gunModel[5].setRotationPoint(2F, -13F, -3F);

		gunModel[6].addShapeBox(0F, 0F, 0F, 4, 4, 4, 0F, 0F, -1F, 0F, -2F, -1F, 0F, -2F, -1F, -2F, 0F, -1F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F); // ironSightFixed
		gunModel[6].setRotationPoint(36F, -31F, -1F);

		gunModel[7].addBox(0F, 0F, 0F, 25, 5, 2, 0F); // Box 12
		gunModel[7].setRotationPoint(11F, -26.5F, -1F);

		gunModel[8].addShapeBox(0F, 0F, 0F, 25, 5, 2, 0F, 0F, -1.5F, -0.5F, 0F, -1.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, -0.5F, 0F, -1.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 13
		gunModel[8].setRotationPoint(11F, -26.5F, -3F);

		gunModel[9].addShapeBox(0F, 0F, 0F, 25, 5, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, -0.5F, 0F, -1.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, -0.5F, 0F, -1.5F, -0.5F); // Box 14
		gunModel[9].setRotationPoint(11F, -26.5F, 1F);

		gunModel[10].addShapeBox(0F, 0F, 0F, 4, 6, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F); // Box 15
		gunModel[10].setRotationPoint(36F, -27F, 1F);

		gunModel[11].addShapeBox(0F, 0F, 0F, 4, 6, 2, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 16
		gunModel[11].setRotationPoint(36F, -27F, -3F);

		gunModel[12].addShapeBox(0F, 0F, 0F, 10, 1, 1, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 17
		gunModel[12].setRotationPoint(1F, -26F, 2F);

		gunModel[13].addShapeBox(0F, 0F, 0F, 10, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 20
		gunModel[13].setRotationPoint(1F, -25F, 2F);

		gunModel[14].addShapeBox(0F, 0F, 0F, 10, 2, 1, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 21
		gunModel[14].setRotationPoint(1F, -25F, -3F);

		gunModel[15].addShapeBox(0F, 0F, 0F, 10, 1, 1, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 22
		gunModel[15].setRotationPoint(1F, -26F, -3F);

		gunModel[16].addBox(0F, 0F, 0F, 30, 2, 7, 0F); // Box 2
		gunModel[16].setRotationPoint(-9F, -18F, -3.5F);

		gunModel[17].addShapeBox(0F, 0F, 0F, 30, 1, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 3
		gunModel[17].setRotationPoint(-9F, -16F, -3.5F);

		gunModel[18].addBox(0F, 0F, 0F, 30, 5, 4, 0F); // Box 4
		gunModel[18].setRotationPoint(-8F, -23F, -2F);

		gunModel[19].addShapeBox(0F, 0F, 0F, 1, 3, 6, 0F, -2F, 0F, -1F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 56
		gunModel[19].setRotationPoint(-10F, -13F, -3F);

		gunModel[20].addShapeBox(0F, 0F, 0F, 10, 16, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, -1F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 3F, -1F, 0F); // Box 57
		gunModel[20].setRotationPoint(-9F, -10F, -3F);

		gunModel[21].addShapeBox(0F, 0F, 0F, 1, 15, 6, 0F, -3F, 0F, -1F, 3F, 0F, 0F, 3F, 0F, 0F, -3F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 58
		gunModel[21].setRotationPoint(-13F, -10F, -3F);

		gunModel[22].addShapeBox(0F, 0F, 0F, 1, 16, 6, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, -1F, -2F, 0F, -1F, 2F, 0F, 0F); // Box 59
		gunModel[22].setRotationPoint(1F, -10F, -3F);

		gunModel[23].addShapeBox(0F, 0F, 0F, 1, 3, 6, 0F, 1F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 60
		gunModel[23].setRotationPoint(-8F, -16F, -3F);

		gunModel[24].addShapeBox(0F, 0F, 0F, 9, 15, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, -0.75F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 3F, -0.75F, 0F); // Box 64
		gunModel[24].setRotationPoint(-8.4F, -10F, -3.5F);

		gunModel[25].addShapeBox(0F, 0F, 0F, 9, 15, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, -0.75F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 3F, -0.75F, 0F); // Box 65
		gunModel[25].setRotationPoint(-8.4F, -10F, 2.5F);

		gunModel[26].addShapeBox(0F, 0F, 0F, 9, 3, 1, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 66
		gunModel[26].setRotationPoint(-8.4F, -13F, -3.5F);

		gunModel[27].addShapeBox(0F, 0F, 0F, 2, 2, 1, 0F, 0F, 0F, 0F, -0.6F, 0F, 0F, -0.6F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.6F, 0F, 0F, -0.6F, 0F, 0F, 0F, 0F, 0F); // Box 67
		gunModel[27].setRotationPoint(0.6F, -13F, -3.5F);

		gunModel[28].addShapeBox(0F, 0F, 0F, 6, 2, 1, 0F, 0F, 0F, 0F, -0.6F, 0F, 0F, -0.6F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.6F, 0F, 0F, -0.6F, 0F, 0F, 0F, 0F, 0F); // Box 68
		gunModel[28].setRotationPoint(-3.4F, -15F, -3.5F);

		gunModel[29].addShapeBox(0F, 0F, 0F, 9, 3, 1, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 70
		gunModel[29].setRotationPoint(-8.4F, -13F, 2.5F);

		gunModel[30].addShapeBox(0F, 0F, 0F, 2, 2, 1, 0F, 0F, 0F, 0F, -0.6F, 0F, 0F, -0.6F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.6F, 0F, 0F, -0.6F, 0F, 0F, 0F, 0F, 0F); // Box 71
		gunModel[30].setRotationPoint(0.6F, -13F, 2.5F);

		gunModel[31].addShapeBox(0F, 0F, 0F, 6, 2, 1, 0F, 0F, 0F, 0F, -0.6F, 0F, 0F, -0.6F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.6F, 0F, 0F, -0.6F, 0F, 0F, 0F, 0F, 0F); // Box 72
		gunModel[31].setRotationPoint(-3.4F, -15F, 2.5F);

		gunModel[32].addShapeBox(0F, 0F, 0F, 1, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 76
		gunModel[32].setRotationPoint(21F, -18F, -2F);

		gunModel[33].addShapeBox(0F, 0F, 0F, 2, 2, 1, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 82
		gunModel[33].setRotationPoint(3F, -17.5F, 3F);

		gunModel[34].addBox(0F, 0F, 0F, 3, 1, 1, 0F); // Box 83
		gunModel[34].setRotationPoint(0F, -17.5F, 3F);

		gunModel[35].addShapeBox(0F, 0F, 0F, 3, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -0.25F, 0F, 0F, -0.25F, 0F); // Box 85
		gunModel[35].setRotationPoint(-0.199999999999999F, -16.5F, 4F);

		gunModel[36].addBox(0F, 0F, 0F, 3, 1, 1, 0F); // Box 86
		gunModel[36].setRotationPoint(-0.199999999999999F, -16.5F, 3F);


		ammoModel = new ModelRendererTurbo[5];
		ammoModel[0] = new ModelRendererTurbo(this, 74, 47, textureX, textureY); // ammoPart1
		ammoModel[1] = new ModelRendererTurbo(this, 105, 48, textureX, textureY); // Box 25
		ammoModel[2] = new ModelRendererTurbo(this, 74, 67, textureX, textureY); // Box 26
		ammoModel[3] = new ModelRendererTurbo(this, 116, 61, textureX, textureY); // Box 31
		ammoModel[4] = new ModelRendererTurbo(this, 139, 61, textureX, textureY); // Box 32

		ammoModel[0].addBox(1.5F, 0F, -2.5F, 10, 14, 5, 0F); // ammoPart1
		ammoModel[0].setRotationPoint(-10.8F, -8F, 0F);
		ammoModel[0].rotateAngleZ = -0.12217305F;

		ammoModel[1].addShapeBox(0.5F, 0F, -2F, 1, 14, 4, 0F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F); // Box 25
		ammoModel[1].setRotationPoint(-10.8F, -8F, 0F);
		ammoModel[1].rotateAngleZ = -0.12217305F;

		ammoModel[2].addBox(0F, 14F, -3F, 12, 1, 6, 0F); // Box 26
		ammoModel[2].setRotationPoint(-10.8F, -8F, 0F);
		ammoModel[2].rotateAngleZ = -0.12217305F;

		ammoModel[3].addShapeBox(1.5F, -1F, -2F, 7, 1, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 31
		ammoModel[3].setRotationPoint(-10.8F, -8F, 0F);
		ammoModel[3].rotateAngleZ = -0.13962634F;

		ammoModel[4].addShapeBox(8.5F, -1F, -2F, 2, 1, 4, 0F, 0F, 0F, -1F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 32
		ammoModel[4].setRotationPoint(-10.8F, -8F, 0F);
		ammoModel[4].rotateAngleZ = -0.13962634F;


		slideModel = new ModelRendererTurbo[41];
		slideModel[0] = new ModelRendererTurbo(this, 75, 18, textureX, textureY); // bodyTop3
		slideModel[1] = new ModelRendererTurbo(this, 101, 9, textureX, textureY); // canisterFarEnd
		slideModel[2] = new ModelRendererTurbo(this, 101, 9, textureX, textureY); // canisterFrontEnd
		slideModel[3] = new ModelRendererTurbo(this, 110, 10, textureX, textureY); // canisterMiddle
		slideModel[4] = new ModelRendererTurbo(this, 154, 18, textureX, textureY); // ironSightPart1
		slideModel[5] = new ModelRendererTurbo(this, 154, 18, textureX, textureY); // ironSightPart2
		slideModel[6] = new ModelRendererTurbo(this, 1, 29, textureX, textureY); // Box 1
		slideModel[7] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 2
		slideModel[8] = new ModelRendererTurbo(this, 54, 1, textureX, textureY); // Box 0
		slideModel[9] = new ModelRendererTurbo(this, 52, 29, textureX, textureY); // Box 1
		slideModel[10] = new ModelRendererTurbo(this, 1, 11, textureX, textureY); // Box 1
		slideModel[11] = new ModelRendererTurbo(this, 12, 11, textureX, textureY); // Box 2
		slideModel[12] = new ModelRendererTurbo(this, 19, 12, textureX, textureY); // Box 3
		slideModel[13] = new ModelRendererTurbo(this, 12, 11, textureX, textureY); // Box 5
		slideModel[14] = new ModelRendererTurbo(this, 19, 12, textureX, textureY); // Box 6
		slideModel[15] = new ModelRendererTurbo(this, 12, 11, textureX, textureY); // Box 7
		slideModel[16] = new ModelRendererTurbo(this, 19, 12, textureX, textureY); // Box 8
		slideModel[17] = new ModelRendererTurbo(this, 19, 12, textureX, textureY); // Box 9
		slideModel[18] = new ModelRendererTurbo(this, 12, 11, textureX, textureY); // Box 10
		slideModel[19] = new ModelRendererTurbo(this, 24, 11, textureX, textureY); // Box 11
		slideModel[20] = new ModelRendererTurbo(this, 19, 12, textureX, textureY); // Box 12
		slideModel[21] = new ModelRendererTurbo(this, 19, 12, textureX, textureY); // Box 45
		slideModel[22] = new ModelRendererTurbo(this, 19, 12, textureX, textureY); // Box 46
		slideModel[23] = new ModelRendererTurbo(this, 19, 12, textureX, textureY); // Box 47
		slideModel[24] = new ModelRendererTurbo(this, 19, 12, textureX, textureY); // Box 48
		slideModel[25] = new ModelRendererTurbo(this, 19, 12, textureX, textureY); // Box 49
		slideModel[26] = new ModelRendererTurbo(this, 1, 11, textureX, textureY); // Box 50
		slideModel[27] = new ModelRendererTurbo(this, 12, 11, textureX, textureY); // Box 51
		slideModel[28] = new ModelRendererTurbo(this, 12, 11, textureX, textureY); // Box 52
		slideModel[29] = new ModelRendererTurbo(this, 12, 11, textureX, textureY); // Box 53
		slideModel[30] = new ModelRendererTurbo(this, 12, 11, textureX, textureY); // Box 54
		slideModel[31] = new ModelRendererTurbo(this, 24, 11, textureX, textureY); // Box 55
		slideModel[32] = new ModelRendererTurbo(this, 100, 19, textureX, textureY); // Box 61
		slideModel[33] = new ModelRendererTurbo(this, 154, 1, textureX, textureY); // Box 73
		slideModel[34] = new ModelRendererTurbo(this, 154, 1, textureX, textureY); // Box 74
		slideModel[35] = new ModelRendererTurbo(this, 154, 1, textureX, textureY); // Box 75
		slideModel[36] = new ModelRendererTurbo(this, 154, 9, textureX, textureY); // Box 77
		slideModel[37] = new ModelRendererTurbo(this, 154, 9, textureX, textureY); // Box 78
		slideModel[38] = new ModelRendererTurbo(this, 154, 9, textureX, textureY); // Box 79
		slideModel[39] = new ModelRendererTurbo(this, 129, 19, textureX, textureY); // Box 80
		slideModel[40] = new ModelRendererTurbo(this, 74, 75, textureX, textureY); // Box 81

		slideModel[0].addBox(0F, 0F, 0F, 6, 3, 6, 0F); // bodyTop3
		slideModel[0].setRotationPoint(-7F, -28F, -3F);

		slideModel[1].addShapeBox(0F, 0F, 0F, 2, 6, 2, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1.9F, -0.5F, 0F, -1.9F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1.9F, -0.5F, 0F, -1.9F, -0.5F); // canisterFarEnd
		slideModel[1].setRotationPoint(5F, -23.5F, 4F);

		slideModel[2].addShapeBox(0F, 0F, 0F, 2, 6, 2, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1.9F, -0.5F, 0F, -1.9F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1.9F, -0.5F, 0F, -1.9F, -0.5F); // canisterFrontEnd
		slideModel[2].setRotationPoint(15F, -23.5F, 4F);

		slideModel[3].addShapeBox(0F, 0F, 0F, 8, 5, 2, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1.5F, -1F, 0F, -1.5F, -1F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1.5F, -1F, 0F, -1.5F, -1F); // canisterMiddle
		slideModel[3].setRotationPoint(7F, -23F, 4F);

		slideModel[4].addShapeBox(0F, 0F, 0F, 4, 2, 2, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // ironSightPart1
		slideModel[4].setRotationPoint(-7F, -29.5F, -3F);

		slideModel[5].addShapeBox(0F, 0F, 0F, 4, 2, 2, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // ironSightPart2
		slideModel[5].setRotationPoint(-7F, -29.5F, 1F);

		slideModel[6].addShapeBox(0F, 0F, 0F, 18, 2, 7, 0F, -0.5F, 0F, -2F, 0.5F, 0F, -2F, 0.5F, 0F, -2F, -0.5F, 0F, -2F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F); // Box 1
		slideModel[6].setRotationPoint(-7.5F, -27F, -3.5F);

		slideModel[7].addShapeBox(0F, 0F, 0F, 19, 2, 7, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 2
		slideModel[7].setRotationPoint(-8F, -25F, -3.5F);

		slideModel[8].addBox(0F, 0F, 0F, 3, 2, 7, 0F); // Box 0
		slideModel[8].setRotationPoint(18F, -25F, -3.5F);

		slideModel[9].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 1
		slideModel[9].setRotationPoint(18F, -27F, -3.5F);

		slideModel[10].addShapeBox(0F, 0F, 0F, 3, 5, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F); // Box 1
		slideModel[10].setRotationPoint(-8F, -23F, -4F);

		slideModel[11].addShapeBox(0F, 0F, 0F, 1, 5, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F); // Box 2
		slideModel[11].setRotationPoint(-4F, -23F, -4F);

		slideModel[12].addShapeBox(0F, 0F, 0F, 1, 5, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F); // Box 3
		slideModel[12].setRotationPoint(-5F, -23F, -3.8F);

		slideModel[13].addShapeBox(0F, 0F, 0F, 1, 5, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F); // Box 5
		slideModel[13].setRotationPoint(-2F, -23F, -4F);

		slideModel[14].addShapeBox(0F, 0F, 0F, 1, 5, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F); // Box 6
		slideModel[14].setRotationPoint(-3F, -23F, -3.8F);

		slideModel[15].addShapeBox(0F, 0F, 0F, 1, 5, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F); // Box 7
		slideModel[15].setRotationPoint(2F, -23F, -4F);

		slideModel[16].addShapeBox(0F, 0F, 0F, 1, 5, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F); // Box 8
		slideModel[16].setRotationPoint(1F, -23F, -3.8F);

		slideModel[17].addShapeBox(0F, 0F, 0F, 1, 5, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F); // Box 9
		slideModel[17].setRotationPoint(-1F, -23F, -3.8F);

		slideModel[18].addShapeBox(0F, 0F, 0F, 1, 5, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F); // Box 10
		slideModel[18].setRotationPoint(0F, -23F, -4F);

		slideModel[19].addShapeBox(0F, 0F, 0F, 17, 5, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F); // Box 11
		slideModel[19].setRotationPoint(4F, -23F, -4F);

		slideModel[20].addShapeBox(0F, 0F, 0F, 1, 5, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F); // Box 12
		slideModel[20].setRotationPoint(3F, -23F, -3.8F);

		slideModel[21].addShapeBox(0F, 0F, 0F, 1, 5, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F); // Box 45
		slideModel[21].setRotationPoint(3F, -23F, 2.8F);

		slideModel[22].addShapeBox(0F, 0F, 0F, 1, 5, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F); // Box 46
		slideModel[22].setRotationPoint(-5F, -23F, 2.8F);

		slideModel[23].addShapeBox(0F, 0F, 0F, 1, 5, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F); // Box 47
		slideModel[23].setRotationPoint(-3F, -23F, 2.8F);

		slideModel[24].addShapeBox(0F, 0F, 0F, 1, 5, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F); // Box 48
		slideModel[24].setRotationPoint(-1F, -23F, 2.8F);

		slideModel[25].addShapeBox(0F, 0F, 0F, 1, 5, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F); // Box 49
		slideModel[25].setRotationPoint(1F, -23F, 2.8F);

		slideModel[26].addShapeBox(0F, 0F, 0F, 3, 5, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F); // Box 50
		slideModel[26].setRotationPoint(-8F, -23F, 2F);

		slideModel[27].addShapeBox(0F, 0F, 0F, 1, 5, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F); // Box 51
		slideModel[27].setRotationPoint(-4F, -23F, 2F);

		slideModel[28].addShapeBox(0F, 0F, 0F, 1, 5, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F); // Box 52
		slideModel[28].setRotationPoint(-2F, -23F, 2F);

		slideModel[29].addShapeBox(0F, 0F, 0F, 1, 5, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F); // Box 53
		slideModel[29].setRotationPoint(0F, -23F, 2F);

		slideModel[30].addShapeBox(0F, 0F, 0F, 1, 5, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F); // Box 54
		slideModel[30].setRotationPoint(2F, -23F, 2F);

		slideModel[31].addShapeBox(0F, 0F, 0F, 17, 5, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F); // Box 55
		slideModel[31].setRotationPoint(4F, -23F, 2F);

		slideModel[32].addBox(0F, 0F, 0F, 7, 1, 7, 0F); // Box 61
		slideModel[32].setRotationPoint(11F, -24F, -3.5F);

		slideModel[33].addShapeBox(0F, 0F, 0F, 12, 5, 2, 0F, 0F, -1.5F, -0.5F, 0F, -1.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, -0.5F, 0F, -1.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 73
		slideModel[33].setRotationPoint(-13F, -23F, -3F);

		slideModel[34].addBox(0F, 0F, 0F, 12, 5, 2, 0F); // Box 74
		slideModel[34].setRotationPoint(-13F, -23F, -1F);

		slideModel[35].addShapeBox(0F, 0F, 0F, 12, 5, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, -0.5F, 0F, -1.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, -0.5F, 0F, -1.5F, -0.5F); // Box 75
		slideModel[35].setRotationPoint(-13F, -23F, 1F);

		slideModel[36].addShapeBox(0F, 0F, 0F, 2, 6, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F); // Box 77
		slideModel[36].setRotationPoint(-12F, -23.5F, 1F);

		slideModel[37].addBox(0F, 0F, 0F, 2, 6, 2, 0F); // Box 78
		slideModel[37].setRotationPoint(-12F, -23.5F, -1F);

		slideModel[38].addShapeBox(0F, 0F, 0F, 2, 6, 2, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 79
		slideModel[38].setRotationPoint(-12F, -23.5F, -3F);

		slideModel[39].addShapeBox(0F, 0F, 0F, 7, 1, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 80
		slideModel[39].setRotationPoint(-8.8F, -18F, -3.5F);

		slideModel[40].addShapeBox(0F, 0F, 0F, 45, 9, 1, 0F, 0F, 0F, 0F, -30F, 0F, 0F, -30F, 0F, 0F, 0F, 0F, 0F, 0F, -6F, 0F, -30F, -6F, 0F, -30F, -6F, 0F, 0F, -6F, 0F); // Box 81
		slideModel[40].setRotationPoint(5F, -22F, -4.2F);

		gunSlideDistance = 0.5F;
		animationType = EnumAnimationType.CUSTOM;

		/* ----Start of Reload Block---- */
		/* Reload Name: Alternate Pistol Clip */
		rotateGunVertical = 10F;
		translateGun = new Vector3f(0.15F, 0.25F, 0F);

		rotateClipVertical = 5F;
		translateClip = new Vector3f(-0.5F, -3F, 0F);
		/* ----End of Reload Block---- */

		translateAll(0F, 0F, 0F);


		flipAll();
	}
}