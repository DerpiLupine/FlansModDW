package com.flansmod.client.model.dwfirearms; //Path where the model is located

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelWinnowMP36 extends ModelGun //Same as Filename
{
	int textureX = 512;
	int textureY = 256;

	public ModelWinnowMP36() //Same as Filename
	{
		gunModel = new ModelRendererTurbo[33];
		gunModel[0] = new ModelRendererTurbo(this, 1, 14, textureX, textureY); // body2
		gunModel[1] = new ModelRendererTurbo(this, 116, 15, textureX, textureY); // body3
		gunModel[2] = new ModelRendererTurbo(this, 116, 15, textureX, textureY); // body4
		gunModel[3] = new ModelRendererTurbo(this, 116, 15, textureX, textureY); // body5
		gunModel[4] = new ModelRendererTurbo(this, 139, 1, textureX, textureY); // body6
		gunModel[5] = new ModelRendererTurbo(this, 1, 68, textureX, textureY); // bodyMain
		gunModel[6] = new ModelRendererTurbo(this, 99, 99, textureX, textureY); // boltPart1
		gunModel[7] = new ModelRendererTurbo(this, 99, 99, textureX, textureY); // boltPart2
		gunModel[8] = new ModelRendererTurbo(this, 118, 45, textureX, textureY); // canisterFarEnd
		gunModel[9] = new ModelRendererTurbo(this, 118, 45, textureX, textureY); // canisterFrontEnd
		gunModel[10] = new ModelRendererTurbo(this, 98, 54, textureX, textureY); // canisterMiddle
		gunModel[11] = new ModelRendererTurbo(this, 98, 45, textureX, textureY); // gasBlockLeft2
		gunModel[12] = new ModelRendererTurbo(this, 111, 45, textureX, textureY); // gasBlockLeft3
		gunModel[13] = new ModelRendererTurbo(this, 98, 45, textureX, textureY); // gasBlockMiddle2
		gunModel[14] = new ModelRendererTurbo(this, 111, 45, textureX, textureY); // gasBlockMiddle3
		gunModel[15] = new ModelRendererTurbo(this, 98, 45, textureX, textureY); // gasBlockRight2
		gunModel[16] = new ModelRendererTurbo(this, 111, 45, textureX, textureY); // gasBlockRight3
		gunModel[17] = new ModelRendererTurbo(this, 1, 105, textureX, textureY); // gripPart1
		gunModel[18] = new ModelRendererTurbo(this, 1, 116, textureX, textureY); // gripPart2
		gunModel[19] = new ModelRendererTurbo(this, 34, 93, textureX, textureY); // gripPart3
		gunModel[20] = new ModelRendererTurbo(this, 1, 88, textureX, textureY); // gripPart4
		gunModel[21] = new ModelRendererTurbo(this, 34, 103, textureX, textureY); // Box 0
		gunModel[22] = new ModelRendererTurbo(this, 1, 30, textureX, textureY); // Box 3
		gunModel[23] = new ModelRendererTurbo(this, 38, 47, textureX, textureY); // Box 4
		gunModel[24] = new ModelRendererTurbo(this, 48, 68, textureX, textureY); // Box 6
		gunModel[25] = new ModelRendererTurbo(this, 74, 1, textureX, textureY); // Box 12
		gunModel[26] = new ModelRendererTurbo(this, 74, 1, textureX, textureY); // Box 13
		gunModel[27] = new ModelRendererTurbo(this, 74, 1, textureX, textureY); // Box 14
		gunModel[28] = new ModelRendererTurbo(this, 116, 12, textureX, textureY); // Box 17
		gunModel[29] = new ModelRendererTurbo(this, 116, 5, textureX, textureY); // Box 20
		gunModel[30] = new ModelRendererTurbo(this, 116, 1, textureX, textureY); // Box 21
		gunModel[31] = new ModelRendererTurbo(this, 116, 9, textureX, textureY); // Box 22
		gunModel[32] = new ModelRendererTurbo(this, 48, 78, textureX, textureY); // Box 24

		gunModel[0].addBox(0F, 0F, 0F, 28, 7, 8, 0F); // body2
		gunModel[0].setRotationPoint(-8F, -17F, -4F);

		gunModel[1].addShapeBox(0F, 0F, 0F, 16, 2, 2, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body3
		gunModel[1].setRotationPoint(1F, -14.5F, -4.8F);

		gunModel[2].addShapeBox(0F, 0F, 0F, 16, 2, 2, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -2F, -1F, 0F, -2F, 0F, 0F, 0F, 0F, 0F); // body4
		gunModel[2].setRotationPoint(1F, -12.5F, -4.8F);

		gunModel[3].addShapeBox(0F, 0F, 0F, 16, 2, 2, 0F, 0F, -1F, 0F, -2F, -1F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body5
		gunModel[3].setRotationPoint(1F, -16.5F, -4.8F);

		gunModel[4].addShapeBox(0F, 0F, 0F, 6, 6, 2, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // body6
		gunModel[4].setRotationPoint(-4F, -16.5F, -5F);

		gunModel[5].addBox(0F, 0F, 0F, 12, 2, 8, 0F); // bodyMain
		gunModel[5].setRotationPoint(8F, -10F, -4F);

		gunModel[6].addShapeBox(0F, 0F, 0F, 1, 3, 9, 0F, 0F, -1F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1F, 0F); // boltPart1
		gunModel[6].setRotationPoint(-7F, -11.5F, -4.5F);

		gunModel[7].addShapeBox(0F, 0F, 0F, 1, 3, 9, 0F, 0F, -0.5F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -0.5F, 0F); // boltPart2
		gunModel[7].setRotationPoint(-6F, -11.5F, -4.5F);

		gunModel[8].addShapeBox(0F, 0F, 0F, 2, 6, 2, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1.9F, -0.5F, 0F, -1.9F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1.9F, -0.5F, 0F, -1.9F, -0.5F); // canisterFarEnd
		gunModel[8].setRotationPoint(-5F, -17.5F, 4F);

		gunModel[9].addShapeBox(0F, 0F, 0F, 2, 6, 2, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1.9F, -0.5F, 0F, -1.9F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1.9F, -0.5F, 0F, -1.9F, -0.5F); // canisterFrontEnd
		gunModel[9].setRotationPoint(5F, -17.5F, 4F);

		gunModel[10].addShapeBox(0F, 0F, 0F, 8, 5, 2, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1.5F, -1F, 0F, -1.5F, -1F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1.5F, -1F, 0F, -1.5F, -1F); // canisterMiddle
		gunModel[10].setRotationPoint(-3F, -17F, 4F);

		gunModel[11].addShapeBox(0F, 0F, 0F, 4, 6, 2, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1.9F, -0.5F, 0F, -1.9F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1.9F, -0.5F, 0F, -1.9F, -0.5F); // gasBlockLeft2
		gunModel[11].setRotationPoint(37F, -17F, 1F);

		gunModel[12].addShapeBox(0F, 0F, 0F, 1, 6, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F); // gasBlockLeft3
		gunModel[12].setRotationPoint(41F, -17F, 1F);

		gunModel[13].addShapeBox(0F, 0F, 0F, 4, 6, 2, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F); // gasBlockMiddle2
		gunModel[13].setRotationPoint(37F, -17F, -1F);

		gunModel[14].addBox(0F, 0F, 0F, 1, 6, 2, 0F); // gasBlockMiddle3
		gunModel[14].setRotationPoint(41F, -17F, -1F);

		gunModel[15].addShapeBox(0F, 0F, 0F, 4, 6, 2, 0F, 0F, -1.9F, -0.5F, 0F, -1.9F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1.9F, -0.5F, 0F, -1.9F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F); // gasBlockRight2
		gunModel[15].setRotationPoint(37F, -17F, -3F);

		gunModel[16].addShapeBox(0F, 0F, 0F, 1, 6, 2, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // gasBlockRight3
		gunModel[16].setRotationPoint(41F, -17F, -3F);

		gunModel[17].addShapeBox(0F, 0F, 0F, 12, 2, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // gripPart1
		gunModel[17].setRotationPoint(-8F, -10F, -4F);

		gunModel[18].addShapeBox(0F, 0F, 0F, 9, 3, 6, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // gripPart2
		gunModel[18].setRotationPoint(-10F, -8F, -3F);

		gunModel[19].addShapeBox(0F, 0F, 0F, 2, 3, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F); // gripPart3
		gunModel[19].setRotationPoint(-1F, -8F, -3F);

		gunModel[20].addShapeBox(0F, 0F, 0F, 10, 10, 6, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, -1F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, -1F, 0F); // gripPart4
		gunModel[20].setRotationPoint(-11F, -5F, -3F);

		gunModel[21].addShapeBox(0F, 0F, 0F, 9, 2, 6, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, -1F, -1F, -1F, -0.5F, 0F, -1F, -0.5F, 0F, -1F, -1F, -1F, -1F); // Box 0
		gunModel[21].setRotationPoint(-11F, 4F, -3F);

		gunModel[22].addBox(0F, 0F, 0F, 17, 4, 7, 0F); // Box 3
		gunModel[22].setRotationPoint(20F, -17F, -3.5F);

		gunModel[23].addShapeBox(0F, 0F, 0F, 12, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 4
		gunModel[23].setRotationPoint(25F, -13F, -3.5F);

		gunModel[24].addBox(0F, 0F, 0F, 5, 2, 7, 0F); // Box 6
		gunModel[24].setRotationPoint(20F, -13F, -3.5F);

		gunModel[25].addBox(0F, 0F, 0F, 17, 5, 2, 0F); // Box 12
		gunModel[25].setRotationPoint(20F, -22.5F, -1F);

		gunModel[26].addShapeBox(0F, 0F, 0F, 17, 5, 2, 0F, 0F, -1.5F, -0.5F, 0F, -1.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, -0.5F, 0F, -1.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 13
		gunModel[26].setRotationPoint(20F, -22.5F, -3F);

		gunModel[27].addShapeBox(0F, 0F, 0F, 17, 5, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, -0.5F, 0F, -1.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, -0.5F, 0F, -1.5F, -0.5F); // Box 14
		gunModel[27].setRotationPoint(20F, -22.5F, 1F);

		gunModel[28].addShapeBox(0F, 0F, 0F, 10, 1, 1, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 17
		gunModel[28].setRotationPoint(10F, -22F, 2F);

		gunModel[29].addShapeBox(0F, 0F, 0F, 10, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 20
		gunModel[29].setRotationPoint(10F, -21F, 2F);

		gunModel[30].addShapeBox(0F, 0F, 0F, 10, 2, 1, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 21
		gunModel[30].setRotationPoint(10F, -21F, -3F);

		gunModel[31].addShapeBox(0F, 0F, 0F, 10, 1, 1, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 22
		gunModel[31].setRotationPoint(10F, -22F, -3F);

		gunModel[32].addShapeBox(0F, 0F, 0F, 5, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 24
		gunModel[32].setRotationPoint(20F, -11F, -3.5F);


		defaultBarrelModel = new ModelRendererTurbo[4];
		defaultBarrelModel[0] = new ModelRendererTurbo(this, 95, 9, textureX, textureY); // barrelMiddle
		defaultBarrelModel[1] = new ModelRendererTurbo(this, 87, 46, textureX, textureY); // ironSightFixed
		defaultBarrelModel[2] = new ModelRendererTurbo(this, 74, 9, textureX, textureY); // Box 15
		defaultBarrelModel[3] = new ModelRendererTurbo(this, 74, 9, textureX, textureY); // Box 16

		defaultBarrelModel[0].addBox(0F, 0F, 0F, 8, 6, 2, 0F); // barrelMiddle
		defaultBarrelModel[0].setRotationPoint(37F, -23F, -1F);

		defaultBarrelModel[1].addShapeBox(0F, 0F, 0F, 4, 2, 1, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // ironSightFixed
		defaultBarrelModel[1].setRotationPoint(40F, -25F, -0.5F);

		defaultBarrelModel[2].addShapeBox(0F, 0F, 0F, 8, 6, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F); // Box 15
		defaultBarrelModel[2].setRotationPoint(37F, -23F, 1F);

		defaultBarrelModel[3].addShapeBox(0F, 0F, 0F, 8, 6, 2, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 16
		defaultBarrelModel[3].setRotationPoint(37F, -23F, -3F);


		ammoModel = new ModelRendererTurbo[9];
		ammoModel[0] = new ModelRendererTurbo(this, 74, 62, textureX, textureY); // ammoPart1
		ammoModel[1] = new ModelRendererTurbo(this, 107, 63, textureX, textureY); // Box 25
		ammoModel[2] = new ModelRendererTurbo(this, 74, 88, textureX, textureY); // Box 26
		ammoModel[3] = new ModelRendererTurbo(this, 120, 65, textureX, textureY); // Box 27
		ammoModel[4] = new ModelRendererTurbo(this, 120, 65, textureX, textureY); // Box 28
		ammoModel[5] = new ModelRendererTurbo(this, 120, 65, textureX, textureY); // Box 29
		ammoModel[6] = new ModelRendererTurbo(this, 120, 65, textureX, textureY); // Box 30
		ammoModel[7] = new ModelRendererTurbo(this, 120, 82, textureX, textureY); // Box 31
		ammoModel[8] = new ModelRendererTurbo(this, 120, 76, textureX, textureY); // Box 32

		ammoModel[0].addBox(0F, 0F, 0F, 10, 19, 6, 0F); // ammoPart1
		ammoModel[0].setRotationPoint(9.5F, -11F, -3F);

		ammoModel[1].addShapeBox(0F, 0F, 0F, 1, 19, 5, 0F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F); // Box 25
		ammoModel[1].setRotationPoint(8.5F, -11F, -2.5F);

		ammoModel[2].addBox(0F, 0F, 0F, 12, 1, 7, 0F); // Box 26
		ammoModel[2].setRotationPoint(8F, 8F, -3.5F);

		ammoModel[3].addShapeBox(0F, 0F, 0F, 1, 3, 7, 0F, 0F, -0.5F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -0.5F, 0F); // Box 27
		ammoModel[3].setRotationPoint(12F, -8.5F, -3.5F);

		ammoModel[4].addShapeBox(0F, 0F, 0F, 1, 3, 7, 0F, 0F, -1F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1F, 0F); // Box 28
		ammoModel[4].setRotationPoint(11F, -8.5F, -3.5F);

		ammoModel[5].addShapeBox(0F, 0F, 0F, 1, 3, 7, 0F, 0F, -1F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1F, 0F); // Box 29
		ammoModel[5].setRotationPoint(11F, 2.5F, -3.5F);

		ammoModel[6].addShapeBox(0F, 0F, 0F, 1, 3, 7, 0F, 0F, -0.5F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -0.5F, 0F); // Box 30
		ammoModel[6].setRotationPoint(12F, 2.5F, -3.5F);

		ammoModel[7].addShapeBox(0F, 0F, 0F, 7, 1, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 31
		ammoModel[7].setRotationPoint(9.5F, -12F, -2F);

		ammoModel[8].addShapeBox(0F, 0F, 0F, 2, 1, 4, 0F, 0F, 0F, -1F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 32
		ammoModel[8].setRotationPoint(16.5F, -12F, -2F);


		slideModel = new ModelRendererTurbo[18];
		slideModel[0] = new ModelRendererTurbo(this, 1, 42, textureX, textureY); // body1
		slideModel[1] = new ModelRendererTurbo(this, 105, 29, textureX, textureY); // bodyTop1
		slideModel[2] = new ModelRendererTurbo(this, 74, 18, textureX, textureY); // bodyTop2
		slideModel[3] = new ModelRendererTurbo(this, 74, 31, textureX, textureY); // bodyTop3
		slideModel[4] = new ModelRendererTurbo(this, 74, 42, textureX, textureY); // bodyTop4
		slideModel[5] = new ModelRendererTurbo(this, 74, 42, textureX, textureY); // bodyTop5
		slideModel[6] = new ModelRendererTurbo(this, 97, 22, textureX, textureY); // bodyTop6
		slideModel[7] = new ModelRendererTurbo(this, 74, 97, textureX, textureY); // bodyTopBolt1
		slideModel[8] = new ModelRendererTurbo(this, 74, 97, textureX, textureY); // bodyTopBolt2
		slideModel[9] = new ModelRendererTurbo(this, 74, 45, textureX, textureY); // ironSightPart1
		slideModel[10] = new ModelRendererTurbo(this, 74, 45, textureX, textureY); // ironSightPart2
		slideModel[11] = new ModelRendererTurbo(this, 74, 55, textureX, textureY); // pumpBolt1
		slideModel[12] = new ModelRendererTurbo(this, 74, 55, textureX, textureY); // pumpBolt2
		slideModel[13] = new ModelRendererTurbo(this, 83, 57, textureX, textureY); // pumpBolt3
		slideModel[14] = new ModelRendererTurbo(this, 74, 50, textureX, textureY); // sliderPart1
		slideModel[15] = new ModelRendererTurbo(this, 1, 57, textureX, textureY); // Box 1
		slideModel[16] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 2
		slideModel[17] = new ModelRendererTurbo(this, 1, 79, textureX, textureY); // Box 8

		slideModel[0].addBox(0F, 0F, 0F, 8, 4, 10, 0F); // body1
		slideModel[0].setRotationPoint(-8F, -21F, -5F);

		slideModel[1].addShapeBox(0F, 0F, 0F, 3, 2, 10, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // bodyTop1
		slideModel[1].setRotationPoint(-3F, -23F, -5F);

		slideModel[2].addBox(0F, 0F, 0F, 6, 2, 10, 0F); // bodyTop2
		slideModel[2].setRotationPoint(-9F, -23F, -5F);

		slideModel[3].addBox(0F, 0F, 0F, 6, 1, 9, 0F); // bodyTop3
		slideModel[3].setRotationPoint(-9F, -24F, -4.5F);

		slideModel[4].addBox(0F, 0F, 0F, 18, 1, 1, 0F); // bodyTop4
		slideModel[4].setRotationPoint(-2F, -24F, -2F);

		slideModel[5].addBox(0F, 0F, 0F, 18, 1, 1, 0F); // bodyTop5
		slideModel[5].setRotationPoint(-2F, -24F, 1F);

		slideModel[6].addBox(0F, 0F, 0F, 1, 1, 4, 0F); // bodyTop6
		slideModel[6].setRotationPoint(16F, -24F, -2F);

		slideModel[7].addShapeBox(0F, 0F, 0F, 1, 3, 11, 0F, 0F, -0.5F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -0.5F, 0F); // bodyTopBolt1
		slideModel[7].setRotationPoint(-5.5F, -22.5F, -5.5F);

		slideModel[8].addShapeBox(0F, 0F, 0F, 1, 3, 11, 0F, 0F, -1F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1F, 0F); // bodyTopBolt2
		slideModel[8].setRotationPoint(-6.5F, -22.5F, -5.5F);

		slideModel[9].addShapeBox(0F, 0F, 0F, 4, 2, 2, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // ironSightPart1
		slideModel[9].setRotationPoint(-8F, -25.5F, -3F);

		slideModel[10].addShapeBox(0F, 0F, 0F, 4, 2, 2, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // ironSightPart2
		slideModel[10].setRotationPoint(-8F, -25.5F, 1F);

		slideModel[11].addShapeBox(0F, 0F, 0F, 1, 3, 3, 0F, 0F, -0.5F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -0.5F, 0F); // pumpBolt1
		slideModel[11].setRotationPoint(14.5F, -27F, -4.5F);
		slideModel[11].rotateAngleX = -0.78539816F;

		slideModel[12].addShapeBox(0F, 0F, 0F, 1, 3, 3, 0F, 0F, -1F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1F, 0F); // pumpBolt2
		slideModel[12].setRotationPoint(13.5F, -27F, -4.5F);
		slideModel[12].rotateAngleX = -0.78539816F;

		slideModel[13].addBox(0F, 0F, 0F, 1, 1, 3, 0F); // pumpBolt3
		slideModel[13].setRotationPoint(14F, -24.5F, -3.5F);
		slideModel[13].rotateAngleX = -0.78539816F;

		slideModel[14].addShapeBox(0F, 0F, 0F, 5, 2, 2, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // sliderPart1
		slideModel[14].setRotationPoint(11F, -24.5F, -1F);

		slideModel[15].addShapeBox(0F, 0F, 0F, 20, 2, 8, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 1
		slideModel[15].setRotationPoint(0F, -23F, -4F);

		slideModel[16].addBox(0F, 0F, 0F, 20, 4, 8, 0F); // Box 2
		slideModel[16].setRotationPoint(0F, -21F, -4F);

		slideModel[17].addBox(0F, 0F, 0F, 17, 2, 6, 0F); // Box 8
		slideModel[17].setRotationPoint(20F, -19F, -3F);

		barrelAttachPoint = new Vector3f(37F /16F, 20F /16F, 0F /16F);
		stockAttachPoint = new Vector3f(-8F /16F, 15F /16F, 0F /16F);

		gunSlideDistance = 0.5F;
		animationType = EnumAnimationType.CUSTOM;

		/* ----Start of Reload Block---- */
		/* Reload Name: DropMag Pistol Clip */
		rotateGunHorizontal = 60F;
		translateGun = new Vector3f(0.15F, 0.25F, 0F);

		translateClip = new Vector3f(0F, -4F, 0F);
		/* ----End of Reload Block---- */

		translateAll(0F, 0F, 0F);


		flipAll();
	}
}