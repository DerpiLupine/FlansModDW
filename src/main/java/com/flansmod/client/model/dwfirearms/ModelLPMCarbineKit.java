package com.flansmod.client.model.dwfirearms;

import com.flansmod.client.model.ModelAttachment;
import com.flansmod.client.tmt.ModelRendererTurbo;

import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelLPMCarbineKit extends ModelAttachment
{
	int textureX = 512;
	int textureY = 256;

	public ModelLPMCarbineKit()
	{
		attachmentModel = new ModelRendererTurbo[60];
		attachmentModel[0] = new ModelRendererTurbo(this, 1, 81, textureX, textureY); // angledGrip1
		attachmentModel[1] = new ModelRendererTurbo(this, 1, 91, textureX, textureY); // angledGrip2
		attachmentModel[2] = new ModelRendererTurbo(this, 1, 102, textureX, textureY); // angledGrip3
		attachmentModel[3] = new ModelRendererTurbo(this, 38, 104, textureX, textureY); // angledGrip4
		attachmentModel[4] = new ModelRendererTurbo(this, 1, 115, textureX, textureY); // angledGrip5
		attachmentModel[5] = new ModelRendererTurbo(this, 1, 115, textureX, textureY); // angledGrip5
		attachmentModel[6] = new ModelRendererTurbo(this, 36, 115, textureX, textureY); // angledGrip6
		attachmentModel[7] = new ModelRendererTurbo(this, 36, 115, textureX, textureY); // angledGrip6
		attachmentModel[8] = new ModelRendererTurbo(this, 92, 1, textureX, textureY); // barrel
		attachmentModel[9] = new ModelRendererTurbo(this, 92, 1, textureX, textureY); // barrel
		attachmentModel[10] = new ModelRendererTurbo(this, 92, 10, textureX, textureY); // barrel2
		attachmentModel[11] = new ModelRendererTurbo(this, 111, 20, textureX, textureY); // barrelRing1
		attachmentModel[12] = new ModelRendererTurbo(this, 111, 20, textureX, textureY); // barrelRing1
		attachmentModel[13] = new ModelRendererTurbo(this, 111, 20, textureX, textureY); // barrelRing1
		attachmentModel[14] = new ModelRendererTurbo(this, 111, 20, textureX, textureY); // barrelRing1
		attachmentModel[15] = new ModelRendererTurbo(this, 92, 19, textureX, textureY); // barrelRing2
		attachmentModel[16] = new ModelRendererTurbo(this, 92, 19, textureX, textureY); // barrelRing2
		attachmentModel[17] = new ModelRendererTurbo(this, 92, 77, textureX, textureY); // body1
		attachmentModel[18] = new ModelRendererTurbo(this, 92, 101, textureX, textureY); // body2
		attachmentModel[19] = new ModelRendererTurbo(this, 92, 45, textureX, textureY); // body3
		attachmentModel[20] = new ModelRendererTurbo(this, 196, 108, textureX, textureY); // body4
		attachmentModel[21] = new ModelRendererTurbo(this, 153, 100, textureX, textureY); // body5
		attachmentModel[22] = new ModelRendererTurbo(this, 161, 83, textureX, textureY); // body6
		attachmentModel[23] = new ModelRendererTurbo(this, 92, 63, textureX, textureY); // body7
		attachmentModel[24] = new ModelRendererTurbo(this, 92, 30, textureX, textureY); // body8
		attachmentModel[25] = new ModelRendererTurbo(this, 25, 2, textureX, textureY); // Box 42
		attachmentModel[26] = new ModelRendererTurbo(this, 25, 2, textureX, textureY); // Box 43
		attachmentModel[27] = new ModelRendererTurbo(this, 34, 4, textureX, textureY); // Box 44
		attachmentModel[28] = new ModelRendererTurbo(this, 32, 5, textureX, textureY); // Box 45
		attachmentModel[29] = new ModelRendererTurbo(this, 68, 12, textureX, textureY); // connector1
		attachmentModel[30] = new ModelRendererTurbo(this, 68, 12, textureX, textureY); // connector1
		attachmentModel[31] = new ModelRendererTurbo(this, 47, 8, textureX, textureY); // connector2
		attachmentModel[32] = new ModelRendererTurbo(this, 1, 46, textureX, textureY); // foreGrip1
		attachmentModel[33] = new ModelRendererTurbo(this, 1, 46, textureX, textureY); // foreGrip1
		attachmentModel[34] = new ModelRendererTurbo(this, 1, 26, textureX, textureY); // foreGrip2
		attachmentModel[35] = new ModelRendererTurbo(this, 28, 61, textureX, textureY); // foreGrip3
		attachmentModel[36] = new ModelRendererTurbo(this, 28, 61, textureX, textureY); // foreGrip3
		attachmentModel[37] = new ModelRendererTurbo(this, 1, 61, textureX, textureY); // foreGrip4
		attachmentModel[38] = new ModelRendererTurbo(this, 92, 122, textureX, textureY); // grip1
		attachmentModel[39] = new ModelRendererTurbo(this, 129, 122, textureX, textureY); // gripBolt
		attachmentModel[40] = new ModelRendererTurbo(this, 129, 122, textureX, textureY); // gripBolt
		attachmentModel[41] = new ModelRendererTurbo(this, 1, 5, textureX, textureY); // ironBolt1
		attachmentModel[42] = new ModelRendererTurbo(this, 1, 5, textureX, textureY); // ironBolt1-2
		attachmentModel[43] = new ModelRendererTurbo(this, 32, 17, textureX, textureY); // ironSight1
		attachmentModel[44] = new ModelRendererTurbo(this, 32, 17, textureX, textureY); // ironSight1-2
		attachmentModel[45] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // ironSightBase1
		attachmentModel[46] = new ModelRendererTurbo(this, 1, 15, textureX, textureY); // ironSightBase2
		attachmentModel[47] = new ModelRendererTurbo(this, 141, 1, textureX, textureY); // rail
		attachmentModel[48] = new ModelRendererTurbo(this, 1, 143, textureX, textureY); // stock1
		attachmentModel[49] = new ModelRendererTurbo(this, 1, 173, textureX, textureY); // stock2
		attachmentModel[50] = new ModelRendererTurbo(this, 32, 143, textureX, textureY); // stock3
		attachmentModel[51] = new ModelRendererTurbo(this, 1, 129, textureX, textureY); // stockBar1
		attachmentModel[52] = new ModelRendererTurbo(this, 1, 129, textureX, textureY); // stockBar1
		attachmentModel[53] = new ModelRendererTurbo(this, 1, 138, textureX, textureY); // stockBar2
		attachmentModel[54] = new ModelRendererTurbo(this, 1, 138, textureX, textureY); // stockBar2
		attachmentModel[55] = new ModelRendererTurbo(this, 1, 138, textureX, textureY); // stockBar2
		attachmentModel[56] = new ModelRendererTurbo(this, 1, 138, textureX, textureY); // stockBar2
		attachmentModel[57] = new ModelRendererTurbo(this, 92, 149, textureX, textureY); // grip1
		attachmentModel[58] = new ModelRendererTurbo(this, 137, 149, textureX, textureY); // grip2
		attachmentModel[59] = new ModelRendererTurbo(this, 92, 163, textureX, textureY); // grip3

		attachmentModel[0].addShapeBox(39F, 0F, -4F, 25, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // angledGrip1
		attachmentModel[0].setRotationPoint(0F, 0F, 0F);

		attachmentModel[1].addBox(39F, -2F, -4F, 15, 2, 8, 0F); // angledGrip2
		attachmentModel[1].setRotationPoint(0F, 0F, 0F);

		attachmentModel[2].addBox(54F, -4F, -4F, 10, 4, 8, 0F); // angledGrip3
		attachmentModel[2].setRotationPoint(0F, 0F, 0F);

		attachmentModel[3].addShapeBox(62F, 1F, -3.5F, 2, 3, 7, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // angledGrip4
		attachmentModel[3].setRotationPoint(0F, 0F, 0F);

		attachmentModel[4].addShapeBox(44F, -3F, -3.5F, 10, 2, 7, 0F, 0F, -8F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, -8F, -2F, 0F, 8F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 8F, 0F); // angledGrip5
		attachmentModel[4].setRotationPoint(0F, 0F, 0F);

		attachmentModel[5].addShapeBox(44F, -1F, -3.5F, 10, 2, 7, 0F, 0F, -8F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -8F, 0F, 0F, 8F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 8F, -2F); // angledGrip5
		attachmentModel[5].setRotationPoint(0F, 0F, 0F);

		attachmentModel[6].addShapeBox(39F, -1F, -3.5F, 5, 2, 7, 0F, 0F, 0F, 0F, 0F, -8F, 0F, 0F, -8F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 8F, -2F, 0F, 8F, -2F, 0F, 0F, -2F); // angledGrip6
		attachmentModel[6].setRotationPoint(0F, 0F, 0F);

		attachmentModel[7].addShapeBox(39F, -3F, -3.5F, 5, 2, 7, 0F, 0F, 0F, -2F, 0F, -8F, -2F, 0F, -8F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 8F, 0F, 0F, 8F, 0F, 0F, 0F, 0F); // angledGrip6
		attachmentModel[7].setRotationPoint(0F, 0F, 0F);

		attachmentModel[8].addShapeBox(75F, -8F, -3F, 18, 2, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // barrel
		attachmentModel[8].setRotationPoint(0F, 0F, 0F);

		attachmentModel[9].addShapeBox(75F, -12F, -3F, 18, 2, 6, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // barrel
		attachmentModel[9].setRotationPoint(0F, 0F, 0F);

		attachmentModel[10].addBox(75F, -10F, -3F, 18, 2, 6, 0F); // barrel2
		attachmentModel[10].setRotationPoint(0F, 0F, 0F);

		attachmentModel[11].addShapeBox(79F, -12.5F, -3.5F, 2, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // barrelRing1
		attachmentModel[11].setRotationPoint(0F, 0F, 0F);

		attachmentModel[12].addShapeBox(75F, -12.5F, -3.5F, 2, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // barrelRing1
		attachmentModel[12].setRotationPoint(0F, 0F, 0F);

		attachmentModel[13].addShapeBox(75F, -7.5F, -3.5F, 2, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // barrelRing1
		attachmentModel[13].setRotationPoint(0F, 0F, 0F);

		attachmentModel[14].addShapeBox(79F, -7.5F, -3.5F, 2, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // barrelRing1
		attachmentModel[14].setRotationPoint(0F, 0F, 0F);

		attachmentModel[15].addBox(75F, -10.5F, -3.5F, 2, 3, 7, 0F); // barrelRing2
		attachmentModel[15].setRotationPoint(0F, 0F, 0F);

		attachmentModel[16].addBox(79F, -10.5F, -3.5F, 2, 3, 7, 0F); // barrelRing2
		attachmentModel[16].setRotationPoint(0F, 0F, 0F);

		attachmentModel[17].addShapeBox(-8F, -13F, -5F, 24, 13, 10, 0F, 0F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0F, 0.2F); // body1
		attachmentModel[17].setRotationPoint(0F, 0F, 0F);

		attachmentModel[18].addShapeBox(16F, -13F, -5F, 20, 10, 10, 0F, 0F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0F, 0.2F); // body2
		attachmentModel[18].setRotationPoint(0F, 0F, 0F);

		attachmentModel[19].addBox(-8F, -12F, -5.5F, 43, 6, 11, 0F); // body3
		attachmentModel[19].setRotationPoint(0F, 0F, 0F);

		attachmentModel[20].addShapeBox(33F, -3F, -5F, 3, 3, 10, 0F, 0F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0F, 0.2F); // body4
		attachmentModel[20].setRotationPoint(0F, 0F, 0F);

		attachmentModel[21].addShapeBox(-24F, -15F, -5.5F, 10, 10, 11, 0F, 0F, 0F, 0F, 6F, 0F, 0F, 6F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 6F, 0F, 0F, 6F, 0F, 0F, 0F, 0F, 0F); // body5
		attachmentModel[21].setRotationPoint(0F, 0F, 0F);

		attachmentModel[22].addShapeBox(-24F, -5F, -5.5F, 10, 5, 11, 0F, 0F, 0F, 0F, 6F, 0F, 0F, 6F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 6F, 0F, -1F, 6F, 0F, -1F, 0F, 0F, -1F); // body6
		attachmentModel[22].setRotationPoint(0F, 0F, 0F);

		attachmentModel[23].addBox(-8F, -15F, -5.5F, 44, 2, 11, 0F); // body7
		attachmentModel[23].setRotationPoint(0F, 0F, 0F);

		attachmentModel[24].addShapeBox(-24F, -18F, -5.5F, 54, 3, 11, 0F, 0F, 0F, -2F, 6F, 0F, -2F, 6F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 6F, 0F, 0F, 6F, 0F, 0F, 0F, 0F, 0F); // body8
		attachmentModel[24].setRotationPoint(0F, 0F, 0F);

		attachmentModel[25].addBox(56F, -24.5F, 2F, 2, 6, 1, 0F); // Box 42
		attachmentModel[25].setRotationPoint(0F, 0F, 0F);

		attachmentModel[26].addBox(56F, -24.5F, -3F, 2, 6, 1, 0F); // Box 43
		attachmentModel[26].setRotationPoint(0F, 0F, 0F);

		attachmentModel[27].addBox(55F, -18.5F, -3F, 4, 1, 6, 0F); // Box 44
		attachmentModel[27].setRotationPoint(0F, 0F, 0F);

		attachmentModel[28].addBox(56F, -21.5F, -0.5F, 2, 3, 1, 0F); // Box 45
		attachmentModel[28].setRotationPoint(0F, 0F, 0F);

		attachmentModel[29].addShapeBox(36F, -17F, -4.5F, 1, 4, 9, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // connector1
		attachmentModel[29].setRotationPoint(0F, 0F, 0F);

		attachmentModel[30].addShapeBox(36F, -5F, -4.5F, 1, 4, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // connector1
		attachmentModel[30].setRotationPoint(0F, 0F, 0F);

		attachmentModel[31].addBox(36F, -13F, -4.5F, 1, 8, 9, 0F); // connector2
		attachmentModel[31].setRotationPoint(0F, 0F, 0F);

		attachmentModel[32].addShapeBox(37F, -4.5F, -5F, 35, 4, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // foreGrip1
		attachmentModel[32].setRotationPoint(0F, 0F, 0F);

		attachmentModel[33].addShapeBox(37F, -17.5F, -5F, 35, 4, 10, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // foreGrip1
		attachmentModel[33].setRotationPoint(0F, 0F, 0F);

		attachmentModel[34].addBox(37F, -13.5F, -5F, 35, 9, 10, 0F); // foreGrip2
		attachmentModel[34].setRotationPoint(0F, 0F, 0F);

		attachmentModel[35].addShapeBox(72F, -4.5F, -5F, 3, 4, 10, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, -2F, -3F, 0F, -2F, -3F, 0F, 0F, -2F); // foreGrip3
		attachmentModel[35].setRotationPoint(0F, 0F, 0F);

		attachmentModel[36].addShapeBox(72F, -17.5F, -5F, 3, 4, 10, 0F, 0F, 0F, -2F, 0F, -2F, -3F, 0F, -2F, -3F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // foreGrip3
		attachmentModel[36].setRotationPoint(0F, 0F, 0F);

		attachmentModel[37].addShapeBox(72F, -13.5F, -5F, 3, 9, 10, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // foreGrip4
		attachmentModel[37].setRotationPoint(0F, 0F, 0F);

		attachmentModel[38].addShapeBox(-3F, 0F, -5F, 8, 16, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 5F, 0F, 0F, -5F, 0F, 0F, -5F, 0F, 0F, 5F, 0F, 0F); // grip1
		attachmentModel[38].setRotationPoint(0F, 0F, 0F);

		attachmentModel[39].addShapeBox(-4.5F, 13F, -5.5F, 2, 2, 11, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // gripBolt
		attachmentModel[39].setRotationPoint(0F, 0F, 0F);

		attachmentModel[40].addShapeBox(0F, -0.5F, -5.5F, 2, 2, 11, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // gripBolt
		attachmentModel[40].setRotationPoint(0F, 0F, 0F);

		attachmentModel[41].addBox(-9F, -22.5F, 4F, 3, 3, 1, 0F); // ironBolt1
		attachmentModel[41].setRotationPoint(0F, 0F, 0F);

		attachmentModel[42].addBox(-9F, -22.5F, -5F, 3, 3, 1, 0F); // ironBolt1-2
		attachmentModel[42].setRotationPoint(0F, 0F, 0F);

		attachmentModel[43].addBox(-10F, -26.5F, 2F, 5, 6, 2, 0F); // ironSight1
		attachmentModel[43].setRotationPoint(0F, 0F, 0F);

		attachmentModel[44].addBox(-10F, -26.5F, -4F, 5, 6, 2, 0F); // ironSight1-2
		attachmentModel[44].setRotationPoint(0F, 0F, 0F);

		attachmentModel[45].addBox(-12F, -18.5F, -4.5F, 7, 2, 9, 0F); // ironSightBase1
		attachmentModel[45].setRotationPoint(0F, 0F, 0F);

		attachmentModel[46].addBox(-12F, -20.5F, -4F, 7, 2, 8, 0F); // ironSightBase2
		attachmentModel[46].setRotationPoint(0F, 0F, 0F);

		attachmentModel[47].addBox(49F, -11.5F, -6F, 21, 5, 12, 0F); // rail
		attachmentModel[47].setRotationPoint(0F, 0F, 0F);

		attachmentModel[48].addBox(-59F, -15F, -5F, 5, 19, 10, 0F); // stock1
		attachmentModel[48].setRotationPoint(0F, 0F, 0F);

		attachmentModel[49].addShapeBox(-59F, -18F, -5F, 7, 3, 10, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // stock2
		attachmentModel[49].setRotationPoint(0F, 0F, 0F);

		attachmentModel[50].addShapeBox(-54F, -15F, -5F, 2, 19, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 0F, 0F); // stock3
		attachmentModel[50].setRotationPoint(0F, 0F, 0F);

		attachmentModel[51].addBox(-52F, -13F, -4F, 28, 5, 3, 0F); // stockBar1
		attachmentModel[51].setRotationPoint(0F, 0F, 0F);

		attachmentModel[52].addBox(-52F, -13F, 1F, 28, 5, 3, 0F); // stockBar1
		attachmentModel[52].setRotationPoint(0F, 0F, 0F);

		attachmentModel[53].addShapeBox(-52F, -14F, -4F, 28, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // stockBar2
		attachmentModel[53].setRotationPoint(0F, 0F, 0F);

		attachmentModel[54].addShapeBox(-52F, -8F, 1F, 28, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // stockBar2
		attachmentModel[54].setRotationPoint(0F, 0F, 0F);

		attachmentModel[55].addShapeBox(-52F, -14F, 1F, 28, 1, 3, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // stockBar2
		attachmentModel[55].setRotationPoint(0F, 0F, 0F);

		attachmentModel[56].addShapeBox(-52F, -8F, -4F, 28, 1, 3, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // stockBar2
		attachmentModel[56].setRotationPoint(0F, 0F, 0F);

		attachmentModel[57].addShapeBox(-7F, -0.5F, -4.5F, 13, 4, 9, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F); // grip1
		attachmentModel[57].setRotationPoint(0F, 0F, 0F);

		attachmentModel[58].addShapeBox(-6F, 3.5F, -4.5F, 12, 14, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 4F, 0F, 0F); // grip2
		attachmentModel[58].setRotationPoint(0F, 0F, 0F);

		attachmentModel[59].addBox(-10F, 17.5F, -4.5F, 12, 2, 9, 0F); // grip3
		attachmentModel[59].setRotationPoint(0F, 0F, 0F);

		renderOffset = 0F;

		//0 Positioning.

		flipAll();
	}
}