package com.flansmod.client.model.dwfirearms; //Path where the model is located

import com.flansmod.client.model.EnumAnimationType;
import com.flansmod.client.model.ModelGun;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelPAR60 extends ModelGun //Same as Filename
{
	int textureX = 512;
	int textureY = 256;

	public ModelPAR60() //Same as Filename
	{
		gunModel = new ModelRendererTurbo[99];
		gunModel[0] = new ModelRendererTurbo(this, 100, 15, textureX, textureY); // Box 1
		gunModel[1] = new ModelRendererTurbo(this, 100, 42, textureX, textureY); // Box 2
		gunModel[2] = new ModelRendererTurbo(this, 135, 61, textureX, textureY); // Box 3
		gunModel[3] = new ModelRendererTurbo(this, 100, 61, textureX, textureY); // Box 5
		gunModel[4] = new ModelRendererTurbo(this, 167, 42, textureX, textureY); // Box 7
		gunModel[5] = new ModelRendererTurbo(this, 100, 94, textureX, textureY); // Box 8
		gunModel[6] = new ModelRendererTurbo(this, 144, 42, textureX, textureY); // Box 9
		gunModel[7] = new ModelRendererTurbo(this, 1, 68, textureX, textureY); // Box 10
		gunModel[8] = new ModelRendererTurbo(this, 38, 68, textureX, textureY); // Box 11
		gunModel[9] = new ModelRendererTurbo(this, 1, 79, textureX, textureY); // Box 14
		gunModel[10] = new ModelRendererTurbo(this, 32, 79, textureX, textureY); // Box 15
		gunModel[11] = new ModelRendererTurbo(this, 49, 79, textureX, textureY); // Box 16
		gunModel[12] = new ModelRendererTurbo(this, 163, 102, textureX, textureY); // Box 19
		gunModel[13] = new ModelRendererTurbo(this, 100, 39, textureX, textureY); // Box 20
		gunModel[14] = new ModelRendererTurbo(this, 100, 84, textureX, textureY); // Box 22
		gunModel[15] = new ModelRendererTurbo(this, 100, 27, textureX, textureY); // Box 23
		gunModel[16] = new ModelRendererTurbo(this, 156, 62, textureX, textureY); // Box 33
		gunModel[17] = new ModelRendererTurbo(this, 187, 34, textureX, textureY); // Box 34
		gunModel[18] = new ModelRendererTurbo(this, 145, 102, textureX, textureY); // Box 38
		gunModel[19] = new ModelRendererTurbo(this, 150, 107, textureX, textureY); // Box 39
		gunModel[20] = new ModelRendererTurbo(this, 150, 102, textureX, textureY); // Box 40
		gunModel[21] = new ModelRendererTurbo(this, 145, 102, textureX, textureY); // Box 41
		gunModel[22] = new ModelRendererTurbo(this, 150, 102, textureX, textureY); // Box 42
		gunModel[23] = new ModelRendererTurbo(this, 150, 107, textureX, textureY); // Box 43
		gunModel[24] = new ModelRendererTurbo(this, 145, 102, textureX, textureY); // Box 44
		gunModel[25] = new ModelRendererTurbo(this, 150, 102, textureX, textureY); // Box 45
		gunModel[26] = new ModelRendererTurbo(this, 150, 107, textureX, textureY); // Box 46
		gunModel[27] = new ModelRendererTurbo(this, 150, 99, textureX, textureY); // Box 47
		gunModel[28] = new ModelRendererTurbo(this, 150, 99, textureX, textureY); // Box 48
		gunModel[29] = new ModelRendererTurbo(this, 150, 99, textureX, textureY); // Box 49
		gunModel[30] = new ModelRendererTurbo(this, 145, 94, textureX, textureY); // Box 50
		gunModel[31] = new ModelRendererTurbo(this, 150, 94, textureX, textureY); // Box 51
		gunModel[32] = new ModelRendererTurbo(this, 150, 94, textureX, textureY); // Box 52
		gunModel[33] = new ModelRendererTurbo(this, 145, 94, textureX, textureY); // Box 53
		gunModel[34] = new ModelRendererTurbo(this, 150, 94, textureX, textureY); // Box 54
		gunModel[35] = new ModelRendererTurbo(this, 145, 94, textureX, textureY); // Box 55
		gunModel[36] = new ModelRendererTurbo(this, 163, 94, textureX, textureY); // Box 56
		gunModel[37] = new ModelRendererTurbo(this, 100, 2, textureX, textureY); // Box 57
		gunModel[38] = new ModelRendererTurbo(this, 100, 8, textureX, textureY); // Box 58
		gunModel[39] = new ModelRendererTurbo(this, 100, 2, textureX, textureY); // Box 60
		gunModel[40] = new ModelRendererTurbo(this, 22, 1, textureX, textureY); // Box 68
		gunModel[41] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 69
		gunModel[42] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 70
		gunModel[43] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 71
		gunModel[44] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 72
		gunModel[45] = new ModelRendererTurbo(this, 100, 110, textureX, textureY); // Box 76
		gunModel[46] = new ModelRendererTurbo(this, 141, 111, textureX, textureY); // Box 77
		gunModel[47] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 81
		gunModel[48] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 82
		gunModel[49] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 83
		gunModel[50] = new ModelRendererTurbo(this, 1, 12, textureX, textureY); // Box 85
		gunModel[51] = new ModelRendererTurbo(this, 121, 42, textureX, textureY); // Box 89
		gunModel[52] = new ModelRendererTurbo(this, 1, 166, textureX, textureY); // Box 90
		gunModel[53] = new ModelRendererTurbo(this, 45, 25, textureX, textureY); // Box 91
		gunModel[54] = new ModelRendererTurbo(this, 45, 25, textureX, textureY); // Box 92
		gunModel[55] = new ModelRendererTurbo(this, 174, 111, textureX, textureY); // Box 93
		gunModel[56] = new ModelRendererTurbo(this, 174, 114, textureX, textureY); // Box 94
		gunModel[57] = new ModelRendererTurbo(this, 226, 26, textureX, textureY); // Box 0
		gunModel[58] = new ModelRendererTurbo(this, 209, 26, textureX, textureY); // Box 1
		gunModel[59] = new ModelRendererTurbo(this, 226, 26, textureX, textureY); // Box 2
		gunModel[60] = new ModelRendererTurbo(this, 150, 94, textureX, textureY); // Box 6
		gunModel[61] = new ModelRendererTurbo(this, 150, 99, textureX, textureY); // Box 7
		gunModel[62] = new ModelRendererTurbo(this, 145, 94, textureX, textureY); // Box 8
		gunModel[63] = new ModelRendererTurbo(this, 150, 107, textureX, textureY); // Box 9
		gunModel[64] = new ModelRendererTurbo(this, 145, 102, textureX, textureY); // Box 10
		gunModel[65] = new ModelRendererTurbo(this, 150, 102, textureX, textureY); // Box 11
		gunModel[66] = new ModelRendererTurbo(this, 1, 46, textureX, textureY); // Box 12
		gunModel[67] = new ModelRendererTurbo(this, 1, 101, textureX, textureY); // mounter
		gunModel[68] = new ModelRendererTurbo(this, 67, 140, textureX, textureY); // innerFrame1
		gunModel[69] = new ModelRendererTurbo(this, 67, 131, textureX, textureY); // innerFrame2
		gunModel[70] = new ModelRendererTurbo(this, 78, 162, textureX, textureY); // sight
		gunModel[71] = new ModelRendererTurbo(this, 67, 151, textureX, textureY); // innerFrame1
		gunModel[72] = new ModelRendererTurbo(this, 67, 131, textureX, textureY); // innerFrame2
		gunModel[73] = new ModelRendererTurbo(this, 1, 114, textureX, textureY); // mounterBase
		gunModel[74] = new ModelRendererTurbo(this, 40, 115, textureX, textureY); // control
		gunModel[75] = new ModelRendererTurbo(this, 1, 127, textureX, textureY); // outerFrame1
		gunModel[76] = new ModelRendererTurbo(this, 24, 140, textureX, textureY); // outerFrame2
		gunModel[77] = new ModelRendererTurbo(this, 1, 140, textureX, textureY); // outerFrame2
		gunModel[78] = new ModelRendererTurbo(this, 1, 147, textureX, textureY); // outerFrame3
		gunModel[79] = new ModelRendererTurbo(this, 24, 147, textureX, textureY); // outerFrame3
		gunModel[80] = new ModelRendererTurbo(this, 1, 153, textureX, textureY); // outerFrame4
		gunModel[81] = new ModelRendererTurbo(this, 28, 153, textureX, textureY); // outerFrame4
		gunModel[82] = new ModelRendererTurbo(this, 1, 158, textureX, textureY); // outerFrame5
		gunModel[83] = new ModelRendererTurbo(this, 34, 158, textureX, textureY); // outerFrame5
		gunModel[84] = new ModelRendererTurbo(this, 1, 58, textureX, textureY); // Box 29
		gunModel[85] = new ModelRendererTurbo(this, 1, 29, textureX, textureY); // Box 30
		gunModel[86] = new ModelRendererTurbo(this, 1, 37, textureX, textureY); // Box 31
		gunModel[87] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 35
		gunModel[88] = new ModelRendererTurbo(this, 209, 1, textureX, textureY); // Box 37
		gunModel[89] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 38
		gunModel[90] = new ModelRendererTurbo(this, 24, 19, textureX, textureY); // Box 0
		gunModel[91] = new ModelRendererTurbo(this, 24, 19, textureX, textureY); // Box 1
		gunModel[92] = new ModelRendererTurbo(this, 24, 19, textureX, textureY); // Box 2
		gunModel[93] = new ModelRendererTurbo(this, 24, 19, textureX, textureY); // Box 3
		gunModel[94] = new ModelRendererTurbo(this, 24, 19, textureX, textureY); // Box 4
		gunModel[95] = new ModelRendererTurbo(this, 24, 19, textureX, textureY); // Box 5
		gunModel[96] = new ModelRendererTurbo(this, 100, 77, textureX, textureY); // Box 157
		gunModel[97] = new ModelRendererTurbo(this, 80, 89, textureX, textureY); // Box 148
		gunModel[98] = new ModelRendererTurbo(this, 49, 89, textureX, textureY); // Box 149

		gunModel[0].addShapeBox(0F, 0F, 0F, 45, 3, 8, 0F, -3F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, -3F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 1
		gunModel[0].setRotationPoint(-10F, -18.5F, -4F);

		gunModel[1].addShapeBox(0F, 0F, 0F, 2, 10, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F); // Box 2
		gunModel[1].setRotationPoint(-10F, -15.5F, -4F);

		gunModel[2].addShapeBox(0F, 0F, 0F, 2, 7, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F); // Box 3
		gunModel[2].setRotationPoint(-5F, -15.5F, -4F);

		gunModel[3].addBox(0F, 0F, 0F, 9, 7, 8, 0F); // Box 5
		gunModel[3].setRotationPoint(-3F, -15.5F, -4F);

		gunModel[4].addBox(0F, 0F, 0F, 6, 10, 8, 0F); // Box 7
		gunModel[4].setRotationPoint(6F, -15.5F, -4F);

		gunModel[5].addShapeBox(0F, 0F, 0F, 14, 7, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F); // Box 8
		gunModel[5].setRotationPoint(12F, -10.5F, -4F);

		gunModel[6].addShapeBox(0F, 0F, 0F, 3, 10, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 0F, 0F); // Box 9
		gunModel[6].setRotationPoint(26F, -15.5F, -4F);

		gunModel[7].addBox(0F, 0F, 0F, 11, 3, 7, 0F); // Box 10
		gunModel[7].setRotationPoint(-5F, -8.5F, -3.5F);

		gunModel[8].addShapeBox(0F, 0F, 0F, 11, 3, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F); // Box 11
		gunModel[8].setRotationPoint(-5F, -5.5F, -3.5F);

		gunModel[9].addShapeBox(0F, 0F, 0F, 8, 11, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, -2F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 4F, -2F, 0F); // Box 14
		gunModel[9].setRotationPoint(-3F, -2.5F, -3.5F);

		gunModel[10].addShapeBox(0F, 0F, 0F, 1, 11, 7, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 4F, 0F, 0F, -4F, 0F, -1F, -4F, 0F, -1F, 4F, 0F, 0F); // Box 15
		gunModel[10].setRotationPoint(5F, -2.5F, -3.5F);

		gunModel[11].addBox(0F, 0F, 0F, 15, 2, 7, 0F); // Box 16
		gunModel[11].setRotationPoint(-5F, -18.7F, -3.5F);

		gunModel[12].addShapeBox(0F, 0F, 0F, 17, 6, 1, 0F, -3F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, -3F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 19
		gunModel[12].setRotationPoint(26F, -17F, -5F);

		gunModel[13].addBox(0F, 0F, 0F, 41, 1, 1, 0F); // Box 20
		gunModel[13].setRotationPoint(26F, -11F, -5F);

		gunModel[14].addShapeBox(0F, 0F, 0F, 38, 1, 8, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 22
		gunModel[14].setRotationPoint(29F, -18F, -4F);

		gunModel[15].addShapeBox(0F, 0F, 0F, 38, 1, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 23
		gunModel[15].setRotationPoint(29F, -10F, -5F);

		gunModel[16].addBox(0F, 0F, 0F, 6, 6, 8, 0F); // Box 33
		gunModel[16].setRotationPoint(29F, -15.5F, -4F);

		gunModel[17].addBox(0F, 0F, 0F, 41, 1, 1, 0F); // Box 34
		gunModel[17].setRotationPoint(26F, -11F, 4F);

		gunModel[18].addShapeBox(0F, 0F, 0F, 1, 6, 1, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 38
		gunModel[18].setRotationPoint(48F, -17F, -5F);

		gunModel[19].addShapeBox(0F, 0F, 0F, 5, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.15F, 0F, 0F, 0.15F, 0F, 0F, -0.15F, 0F, 0F, -0.15F); // Box 39
		gunModel[19].setRotationPoint(43F, -17F, -4F);

		gunModel[20].addShapeBox(0F, 0F, 0F, 5, 3, 1, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 40
		gunModel[20].setRotationPoint(43F, -14F, -5F);

		gunModel[21].addShapeBox(0F, 0F, 0F, 1, 6, 1, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 41
		gunModel[21].setRotationPoint(54F, -17F, -5F);

		gunModel[22].addShapeBox(0F, 0F, 0F, 5, 3, 1, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 42
		gunModel[22].setRotationPoint(49F, -14F, -5F);

		gunModel[23].addShapeBox(0F, 0F, 0F, 5, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.15F, 0F, 0F, 0.15F, 0F, 0F, -0.15F, 0F, 0F, -0.15F); // Box 43
		gunModel[23].setRotationPoint(49F, -17F, -4F);

		gunModel[24].addShapeBox(0F, 0F, 0F, 1, 6, 1, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 44
		gunModel[24].setRotationPoint(60F, -17F, -5F);

		gunModel[25].addShapeBox(0F, 0F, 0F, 5, 3, 1, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 45
		gunModel[25].setRotationPoint(55F, -14F, -5F);

		gunModel[26].addShapeBox(0F, 0F, 0F, 5, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.15F, 0F, 0F, 0.15F, 0F, 0F, -0.15F, 0F, 0F, -0.15F); // Box 46
		gunModel[26].setRotationPoint(55F, -17F, -4F);

		gunModel[27].addShapeBox(0F, 0F, 0F, 5, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.15F, 0F, 0F, -0.15F, 0F, 0F, 0.15F, 0F, 0F, 0.15F); // Box 47
		gunModel[27].setRotationPoint(55F, -17F, 3F);

		gunModel[28].addShapeBox(0F, 0F, 0F, 5, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.15F, 0F, 0F, -0.15F, 0F, 0F, 0.15F, 0F, 0F, 0.15F); // Box 48
		gunModel[28].setRotationPoint(49F, -17F, 3F);

		gunModel[29].addShapeBox(0F, 0F, 0F, 5, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.15F, 0F, 0F, -0.15F, 0F, 0F, 0.15F, 0F, 0F, 0.15F); // Box 49
		gunModel[29].setRotationPoint(43F, -17F, 3F);

		gunModel[30].addShapeBox(0F, 0F, 0F, 1, 6, 1, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 50
		gunModel[30].setRotationPoint(48F, -17F, 4F);

		gunModel[31].addShapeBox(0F, 0F, 0F, 5, 3, 1, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 51
		gunModel[31].setRotationPoint(43F, -14F, 4F);

		gunModel[32].addShapeBox(0F, 0F, 0F, 5, 3, 1, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 52
		gunModel[32].setRotationPoint(49F, -14F, 4F);

		gunModel[33].addShapeBox(0F, 0F, 0F, 1, 6, 1, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 53
		gunModel[33].setRotationPoint(54F, -17F, 4F);

		gunModel[34].addShapeBox(0F, 0F, 0F, 5, 3, 1, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 54
		gunModel[34].setRotationPoint(55F, -14F, 4F);

		gunModel[35].addShapeBox(0F, 0F, 0F, 1, 6, 1, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 55
		gunModel[35].setRotationPoint(60F, -17F, 4F);

		gunModel[36].addShapeBox(0F, 0F, 0F, 17, 6, 1, 0F, -3F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, -1F, -3F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 56
		gunModel[36].setRotationPoint(26F, -17F, 4F);

		gunModel[37].addShapeBox(0F, 0F, 0F, 50, 1, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 57
		gunModel[37].setRotationPoint(35F, -14F, -2F);

		gunModel[38].addBox(0F, 0F, 0F, 50, 2, 4, 0F); // Box 58
		gunModel[38].setRotationPoint(35F, -13F, -2F);

		gunModel[39].addShapeBox(0F, 0F, 0F, 50, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 60
		gunModel[39].setRotationPoint(35F, -11F, -2F);

		gunModel[40].addBox(0F, 0F, 0F, 21, 2, 7, 0F); // Box 68
		gunModel[40].setRotationPoint(38F, -8.5F, -3.5F);

		gunModel[41].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 69
		gunModel[41].setRotationPoint(38F, -6.5F, -3.5F);

		gunModel[42].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 70
		gunModel[42].setRotationPoint(44F, -6.5F, -3.5F);

		gunModel[43].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 71
		gunModel[43].setRotationPoint(56F, -6.5F, -3.5F);

		gunModel[44].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 72
		gunModel[44].setRotationPoint(50F, -6.5F, -3.5F);

		gunModel[45].addBox(0F, 0F, 0F, 14, 5, 6, 0F); // Box 76
		gunModel[45].setRotationPoint(12F, -15.5F, -2F);

		gunModel[46].addBox(0F, 0F, 0F, 14, 2, 2, 0F); // Box 77
		gunModel[46].setRotationPoint(12F, -15.5F, -4F);

		gunModel[47].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 81
		gunModel[47].setRotationPoint(-5F, -24F, -3.5F);

		gunModel[48].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 82
		gunModel[48].setRotationPoint(1F, -24F, -3.5F);

		gunModel[49].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 83
		gunModel[49].setRotationPoint(7F, -24F, -3.5F);

		gunModel[50].addShapeBox(0F, 0F, 0F, 1, 6, 10, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 85
		gunModel[50].setRotationPoint(59F, -10F, -5F);

		gunModel[51].addBox(0F, 0F, 0F, 3, 10, 8, 0F); // Box 89
		gunModel[51].setRotationPoint(-8F, -15.5F, -4F);

		gunModel[52].addShapeBox(0F, 0F, 0F, 30, 30, 8, 0F, 0F, 0F, 0F, -25F, 0F, 0F, -25F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, -25F, 0F, -25F, -25F, 0F, -25F, -25F, 0.4F, 0F, -25F, 0.4F); // Box 90
		gunModel[52].setRotationPoint(6.5F, -11F, -4.2F);

		gunModel[53].addShapeBox(0F, 0F, 0F, 4, 2, 1, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 91
		gunModel[53].setRotationPoint(-2.5F, -8F, -4F);

		gunModel[54].addShapeBox(0F, 0F, 0F, 4, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 92
		gunModel[54].setRotationPoint(-2.5F, -6F, -4F);

		gunModel[55].addBox(0F, 0F, 0F, 10, 1, 1, 0F); // Box 93
		gunModel[55].setRotationPoint(13F, -10.5F, 3.3F);

		gunModel[56].addBox(0F, 0F, 0F, 4, 3, 1, 0F); // Box 94
		gunModel[56].setRotationPoint(13F, -9.5F, 3.3F);

		gunModel[57].addShapeBox(0F, 0F, 0F, 3, 2, 5, 0F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 0
		gunModel[57].setRotationPoint(81F, -15F, -2.5F);

		gunModel[58].addBox(0F, 0F, 0F, 3, 2, 5, 0F); // Box 1
		gunModel[58].setRotationPoint(81F, -13F, -2.5F);

		gunModel[59].addShapeBox(0F, 0F, 0F, 3, 2, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F); // Box 2
		gunModel[59].setRotationPoint(81F, -11F, -2.5F);

		gunModel[60].addShapeBox(0F, 0F, 0F, 5, 3, 1, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 6
		gunModel[60].setRotationPoint(61F, -14F, 4F);

		gunModel[61].addShapeBox(0F, 0F, 0F, 5, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.15F, 0F, 0F, -0.15F, 0F, 0F, 0.15F, 0F, 0F, 0.15F); // Box 7
		gunModel[61].setRotationPoint(61F, -17F, 3F);

		gunModel[62].addShapeBox(0F, 0F, 0F, 1, 6, 1, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 8
		gunModel[62].setRotationPoint(66F, -17F, 4F);

		gunModel[63].addShapeBox(0F, 0F, 0F, 5, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.15F, 0F, 0F, 0.15F, 0F, 0F, -0.15F, 0F, 0F, -0.15F); // Box 9
		gunModel[63].setRotationPoint(61F, -17F, -4F);

		gunModel[64].addShapeBox(0F, 0F, 0F, 1, 6, 1, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 10
		gunModel[64].setRotationPoint(66F, -17F, -5F);

		gunModel[65].addShapeBox(0F, 0F, 0F, 5, 3, 1, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 11
		gunModel[65].setRotationPoint(61F, -14F, -5F);

		gunModel[66].addBox(0F, 0F, 0F, 32, 2, 5, 0F); // Box 12
		gunModel[66].setRotationPoint(35F, -19.5F, -2.5F);

		gunModel[67].addBox(0F, 0F, 0F, 20, 3, 9, 0F); // mounter
		gunModel[67].setRotationPoint(24F, -24.5F, -4.5F);

		gunModel[68].addShapeBox(0F, 0F, 0F, 8, 1, 9, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // innerFrame1
		gunModel[68].setRotationPoint(26F, -36.5F, -4.5F);

		gunModel[69].addShapeBox(0F, 0F, 0F, 8, 7, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F); // innerFrame2
		gunModel[69].setRotationPoint(26F, -35.5F, -4.5F);

		gunModel[70].addShapeBox(0F, 0F, 0F, 1, 21, 21, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -14F, 0F, 0F, -14F, 0F, -14F, 0F, 0F, -14F, 0F, 0F, -14F, -14F, 0F, -14F, -14F); // sight
		gunModel[70].setRotationPoint(28F, -35.5F, -3.5F);

		gunModel[71].addBox(0F, 0F, 0F, 9, 1, 9, 0F); // innerFrame1
		gunModel[71].setRotationPoint(25F, -28.5F, -4.5F);

		gunModel[72].addShapeBox(0F, 0F, 0F, 8, 7, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F); // innerFrame2
		gunModel[72].setRotationPoint(26F, -35.5F, 3.5F);

		gunModel[73].addShapeBox(0F, 0F, 0F, 10, 3, 9, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // mounterBase
		gunModel[73].setRotationPoint(25F, -27.5F, -4.5F);

		gunModel[74].addBox(0F, 0F, 0F, 1, 3, 8, 0F); // control
		gunModel[74].setRotationPoint(24.5F, -28F, -4F);

		gunModel[75].addShapeBox(0F, 0F, 0F, 10, 1, 11, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // outerFrame1
		gunModel[75].setRotationPoint(25F, -37.5F, -5.5F);

		gunModel[76].addBox(0F, 0F, 0F, 10, 5, 1, 0F); // outerFrame2
		gunModel[76].setRotationPoint(25F, -36.5F, -5.5F);

		gunModel[77].addBox(0F, 0F, 0F, 10, 5, 1, 0F); // outerFrame2
		gunModel[77].setRotationPoint(25F, -36.5F, 4.5F);

		gunModel[78].addShapeBox(0F, 0F, 0F, 10, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F); // outerFrame3
		gunModel[78].setRotationPoint(25F, -31.5F, -5.5F);

		gunModel[79].addShapeBox(0F, 0F, 0F, 10, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F); // outerFrame3
		gunModel[79].setRotationPoint(25F, -31.5F, 4.5F);

		gunModel[80].addShapeBox(0F, 0F, 0F, 12, 3, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F); // outerFrame4
		gunModel[80].setRotationPoint(24F, -27.5F, -5.5F);

		gunModel[81].addShapeBox(0F, 0F, 0F, 12, 3, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F); // outerFrame4
		gunModel[81].setRotationPoint(24F, -27.5F, 4.5F);

		gunModel[82].addBox(0F, 0F, 0F, 15, 2, 1, 0F); // outerFrame5
		gunModel[82].setRotationPoint(24F, -24.5F, -5.5F);

		gunModel[83].addBox(0F, 0F, 0F, 15, 2, 1, 0F); // outerFrame5
		gunModel[83].setRotationPoint(24F, -24.5F, 4.5F);

		gunModel[84].addBox(0F, 0F, 0F, 45, 2, 7, 0F); // Box 29
		gunModel[84].setRotationPoint(-5F, -22F, -3.5F);

		gunModel[85].addBox(0F, 0F, 0F, 45, 1, 6, 0F); // Box 30
		gunModel[85].setRotationPoint(-5F, -20.5F, -3F);

		gunModel[86].addBox(0F, 0F, 0F, 45, 1, 7, 0F); // Box 31
		gunModel[86].setRotationPoint(-5F, -19.7F, -3.5F);

		gunModel[87].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 35
		gunModel[87].setRotationPoint(13F, -24F, -3.5F);

		gunModel[88].addBox(0F, 0F, 0F, 6, 1, 7, 0F); // Box 37
		gunModel[88].setRotationPoint(34F, -18.7F, -3.5F);

		gunModel[89].addShapeBox(0F, 0F, 0F, 3, 2, 7, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 38
		gunModel[89].setRotationPoint(19F, -24F, -3.5F);

		gunModel[90].addShapeBox(0F, 0F, 0F, 2, 1, 8, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 0
		gunModel[90].setRotationPoint(-3.5F, -20.5F, -4F);

		gunModel[91].addShapeBox(0F, 0F, 0F, 2, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 1
		gunModel[91].setRotationPoint(-3.5F, -19.5F, -4F);

		gunModel[92].addShapeBox(0F, 0F, 0F, 2, 1, 8, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 2
		gunModel[92].setRotationPoint(6.5F, -20.5F, -4F);

		gunModel[93].addShapeBox(0F, 0F, 0F, 2, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 3
		gunModel[93].setRotationPoint(6.5F, -19.5F, -4F);

		gunModel[94].addShapeBox(0F, 0F, 0F, 2, 1, 8, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 4
		gunModel[94].setRotationPoint(36F, -20.5F, -4F);

		gunModel[95].addShapeBox(0F, 0F, 0F, 2, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 5
		gunModel[95].setRotationPoint(36F, -19.5F, -4F);

		gunModel[96].addShapeBox(0F, 0F, 0F, 31, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 157
		gunModel[96].setRotationPoint(-5F, -16F, 3.5F);

		gunModel[97].addShapeBox(0F, 0F, 0F, 1, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 148
		gunModel[97].setRotationPoint(1F, 8.5F, -3.5F);

		gunModel[98].addShapeBox(0F, 0F, 0F, 8, 2, 7, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F); // Box 149
		gunModel[98].setRotationPoint(-7F, 8.5F, -3.5F);


		defaultBarrelModel = new ModelRendererTurbo[3];
		defaultBarrelModel[0] = new ModelRendererTurbo(this, 209, 10, textureX, textureY); // Box 61
		defaultBarrelModel[1] = new ModelRendererTurbo(this, 209, 18, textureX, textureY); // Box 62
		defaultBarrelModel[2] = new ModelRendererTurbo(this, 209, 10, textureX, textureY); // Box 63

		defaultBarrelModel[0].addShapeBox(0F, 0F, 0F, 10, 2, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F); // Box 61
		defaultBarrelModel[0].setRotationPoint(85F, -11F, -2.5F);

		defaultBarrelModel[1].addBox(0F, 0F, 0F, 10, 2, 5, 0F); // Box 62
		defaultBarrelModel[1].setRotationPoint(85F, -13F, -2.5F);

		defaultBarrelModel[2].addShapeBox(0F, 0F, 0F, 10, 2, 5, 0F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 63
		defaultBarrelModel[2].setRotationPoint(85F, -15F, -2.5F);


		defaultStockModel = new ModelRendererTurbo[15];
		defaultStockModel[0] = new ModelRendererTurbo(this, 209, 40, textureX, textureY); // Box 130
		defaultStockModel[1] = new ModelRendererTurbo(this, 276, 87, textureX, textureY); // Box 131
		defaultStockModel[2] = new ModelRendererTurbo(this, 209, 85, textureX, textureY); // Box 132
		defaultStockModel[3] = new ModelRendererTurbo(this, 209, 101, textureX, textureY); // Box 133
		defaultStockModel[4] = new ModelRendererTurbo(this, 276, 101, textureX, textureY); // Box 134
		defaultStockModel[5] = new ModelRendererTurbo(this, 209, 112, textureX, textureY); // Box 135
		defaultStockModel[6] = new ModelRendererTurbo(this, 276, 114, textureX, textureY); // Box 136
		defaultStockModel[7] = new ModelRendererTurbo(this, 276, 123, textureX, textureY); // Box 137
		defaultStockModel[8] = new ModelRendererTurbo(this, 209, 123, textureX, textureY); // Box 138
		defaultStockModel[9] = new ModelRendererTurbo(this, 251, 45, textureX, textureY); // Box 140
		defaultStockModel[10] = new ModelRendererTurbo(this, 209, 54, textureX, textureY); // Box 141
		defaultStockModel[11] = new ModelRendererTurbo(this, 253, 69, textureX, textureY); // Box 142
		defaultStockModel[12] = new ModelRendererTurbo(this, 226, 42, textureX, textureY); // Box 143
		defaultStockModel[13] = new ModelRendererTurbo(this, 253, 57, textureX, textureY); // Box 144
		defaultStockModel[14] = new ModelRendererTurbo(this, 234, 54, textureX, textureY); // Box 145

		defaultStockModel[0].addShapeBox(0F, 0F, 0F, 2, 7, 6, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 130
		defaultStockModel[0].setRotationPoint(-12F, -16.5F, -3F);

		defaultStockModel[1].addShapeBox(0F, 0F, 0F, 2, 7, 6, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F); // Box 131
		defaultStockModel[1].setRotationPoint(-14F, -16.5F, -3F);

		defaultStockModel[2].addBox(0F, 0F, 0F, 25, 7, 8, 0F); // Box 132
		defaultStockModel[2].setRotationPoint(-39F, -16.5F, -4F);

		defaultStockModel[3].addShapeBox(0F, 0F, 0F, 25, 2, 8, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 133
		defaultStockModel[3].setRotationPoint(-39F, -18.5F, -4F);

		defaultStockModel[4].addShapeBox(0F, 0F, 0F, 2, 2, 8, 0F, 0F, 0F, -1F, -1F, 0F, -2F, -1F, 0F, -2F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 134
		defaultStockModel[4].setRotationPoint(-14F, -18.5F, -4F);

		defaultStockModel[5].addBox(0F, 0F, 0F, 25, 3, 7, 0F); // Box 135
		defaultStockModel[5].setRotationPoint(-39F, -9.5F, -3.5F);

		defaultStockModel[6].addShapeBox(0F, 0F, 0F, 2, 3, 5, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F); // Box 136
		defaultStockModel[6].setRotationPoint(-14F, -9.5F, -2.5F);

		defaultStockModel[7].addShapeBox(0F, 0F, 0F, 2, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -1F, -1F, 0F, -2F, -1F, 0F, -2F, 0F, 0F, -1F); // Box 137
		defaultStockModel[7].setRotationPoint(-14F, -6.5F, -3.5F);

		defaultStockModel[8].addShapeBox(0F, 0F, 0F, 25, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 138
		defaultStockModel[8].setRotationPoint(-39F, -6.5F, -3.5F);

		defaultStockModel[9].addBox(0F, 0F, 0F, 2, 3, 5, 0F); // Box 140
		defaultStockModel[9].setRotationPoint(-12F, -9.5F, -2.5F);

		defaultStockModel[10].addBox(0F, 0F, 0F, 4, 22, 8, 0F); // Box 141
		defaultStockModel[10].setRotationPoint(-43F, -16.5F, -4F);

		defaultStockModel[11].addShapeBox(0F, 0F, 0F, 5, 10, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F); // Box 142
		defaultStockModel[11].setRotationPoint(-39F, -4.5F, -2.5F);

		defaultStockModel[12].addShapeBox(0F, 0F, 0F, 4, 3, 8, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 143
		defaultStockModel[12].setRotationPoint(-43F, -19.5F, -4F);

		defaultStockModel[13].addShapeBox(0F, 0F, 0F, 1, 3, 8, 0F, 0F, -1.5F, -1.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -1.5F, -1.5F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 144
		defaultStockModel[13].setRotationPoint(-44F, -19.5F, -4F);

		defaultStockModel[14].addShapeBox(0F, 0F, 0F, 1, 22, 8, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 145
		defaultStockModel[14].setRotationPoint(-44F, -16.5F, -4F);


		defaultGripModel = new ModelRendererTurbo[17];
		defaultGripModel[0] = new ModelRendererTurbo(this, 130, 122, textureX, textureY); // Box 39
		defaultGripModel[1] = new ModelRendererTurbo(this, 130, 139, textureX, textureY); // Box 6
		defaultGripModel[2] = new ModelRendererTurbo(this, 100, 122, textureX, textureY); // Box 115
		defaultGripModel[3] = new ModelRendererTurbo(this, 155, 126, textureX, textureY); // Box 116
		defaultGripModel[4] = new ModelRendererTurbo(this, 130, 132, textureX, textureY); // Box 117
		defaultGripModel[5] = new ModelRendererTurbo(this, 155, 122, textureX, textureY); // Box 118
		defaultGripModel[6] = new ModelRendererTurbo(this, 164, 122, textureX, textureY); // Box 119
		defaultGripModel[7] = new ModelRendererTurbo(this, 164, 129, textureX, textureY); // Box 120
		defaultGripModel[8] = new ModelRendererTurbo(this, 109, 122, textureX, textureY); // Box 121
		defaultGripModel[9] = new ModelRendererTurbo(this, 109, 130, textureX, textureY); // Box 122
		defaultGripModel[10] = new ModelRendererTurbo(this, 155, 126, textureX, textureY); // Box 123
		defaultGripModel[11] = new ModelRendererTurbo(this, 155, 122, textureX, textureY); // Box 124
		defaultGripModel[12] = new ModelRendererTurbo(this, 100, 122, textureX, textureY); // Box 125
		defaultGripModel[13] = new ModelRendererTurbo(this, 164, 129, textureX, textureY); // Box 126
		defaultGripModel[14] = new ModelRendererTurbo(this, 164, 122, textureX, textureY); // Box 127
		defaultGripModel[15] = new ModelRendererTurbo(this, 109, 122, textureX, textureY); // Box 128
		defaultGripModel[16] = new ModelRendererTurbo(this, 109, 130, textureX, textureY); // Box 129

		defaultGripModel[0].addShapeBox(0F, 0F, 0F, 6, 3, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, -1.5F); // Box 39
		defaultGripModel[0].setRotationPoint(60F, -9F, -3F);

		defaultGripModel[1].addBox(0F, 0F, 0F, 3, 1, 3, 0F); // Box 6
		defaultGripModel[1].setRotationPoint(61.5F, -6F, -1.5F);

		defaultGripModel[2].addShapeBox(0F, 0F, 0F, 2, 15, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 5F, 0F, 0F, 5F, 0F, 0F, -5F, 0F, 0F, -5F); // Box 115
		defaultGripModel[2].setRotationPoint(62F, -3.5F, -2.5F);

		defaultGripModel[3].addShapeBox(0F, 0F, 0F, 2, 4, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 1.5F, 0F, 0F, 1.5F, 1F, 0F, -1.5F, 1F, 0F, -1.5F); // Box 116
		defaultGripModel[3].setRotationPoint(62F, -3.5F, -3.5F);

		defaultGripModel[4].addBox(0F, 0F, 0F, 5, 3, 3, 0F); // Box 117
		defaultGripModel[4].setRotationPoint(60.5F, -5F, -1.5F);

		defaultGripModel[5].addShapeBox(0F, 0F, 0F, 2, 1, 2, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F); // Box 118
		defaultGripModel[5].setRotationPoint(62F, -4.5F, -3.5F);

		defaultGripModel[6].addShapeBox(0F, 0F, 0F, 2, 4, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 1.5F, 0F, 0F, 1.5F, 1F, 0F, -1.5F, 1F, 0F, -1.5F); // Box 119
		defaultGripModel[6].setRotationPoint(62F, 7.5F, -7F);

		defaultGripModel[7].addShapeBox(0F, 0F, 0F, 2, 4, 2, 0F, 1F, 0F, -1.5F, 1F, 0F, -1.5F, 0F, 0F, 1.5F, 0F, 0F, 1.5F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 120
		defaultGripModel[7].setRotationPoint(62F, 7.5F, -6.5F);

		defaultGripModel[8].addBox(0F, 0F, 0F, 5, 2, 5, 0F); // Box 121
		defaultGripModel[8].setRotationPoint(60.5F, 11.5F, -9F);

		defaultGripModel[9].addShapeBox(0F, 0F, 0F, 5, 1, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F); // Box 122
		defaultGripModel[9].setRotationPoint(60.5F, 13.5F, -9F);

		defaultGripModel[10].addShapeBox(0F, 0F, 0F, 2, 4, 2, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, -1.5F, 1F, 0F, -1.5F, 0F, 0F, 1.5F, 0F, 0F, 1.5F); // Box 123
		defaultGripModel[10].setRotationPoint(62F, -3.5F, 1.5F);

		defaultGripModel[11].addShapeBox(0F, 0F, 0F, 2, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 124
		defaultGripModel[11].setRotationPoint(62F, -4.5F, 1.5F);

		defaultGripModel[12].addShapeBox(0F, 0F, 0F, 2, 15, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -5F, 0F, 0F, -5F, 0F, 0F, 5F, 0F, 0F, 5F); // Box 125
		defaultGripModel[12].setRotationPoint(62F, -3.5F, 0.5F);

		defaultGripModel[13].addShapeBox(0F, 0F, 0F, 2, 4, 2, 0F, 0F, 0F, 1.5F, 0F, 0F, 1.5F, 1F, 0F, -1.5F, 1F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F); // Box 126
		defaultGripModel[13].setRotationPoint(62F, 7.5F, 4.5F);

		defaultGripModel[14].addShapeBox(0F, 0F, 0F, 2, 4, 2, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, -1.5F, 1F, 0F, -1.5F, 0F, 0F, 1.5F, 0F, 0F, 1.5F); // Box 127
		defaultGripModel[14].setRotationPoint(62F, 7.5F, 5F);

		defaultGripModel[15].addBox(0F, 0F, 0F, 5, 2, 5, 0F); // Box 128
		defaultGripModel[15].setRotationPoint(60.5F, 11.5F, 4F);

		defaultGripModel[16].addShapeBox(0F, 0F, 0F, 5, 1, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F); // Box 129
		defaultGripModel[16].setRotationPoint(60.5F, 13.5F, 4F);


		ammoModel = new ModelRendererTurbo[13];
		ammoModel[0] = new ModelRendererTurbo(this, 62, 233, textureX, textureY); // Box 86
		ammoModel[1] = new ModelRendererTurbo(this, 73, 227, textureX, textureY); // Box 87
		ammoModel[2] = new ModelRendererTurbo(this, 62, 215, textureX, textureY); // Box 13
		ammoModel[3] = new ModelRendererTurbo(this, 62, 205, textureX, textureY); // Box 14
		ammoModel[4] = new ModelRendererTurbo(this, 1, 230, textureX, textureY); // Box 15
		ammoModel[5] = new ModelRendererTurbo(this, 1, 205, textureX, textureY); // Box 16
		ammoModel[6] = new ModelRendererTurbo(this, 123, 187, textureX, textureY); // Box 17
		ammoModel[7] = new ModelRendererTurbo(this, 123, 177, textureX, textureY); // Box 18
		ammoModel[8] = new ModelRendererTurbo(this, 62, 225, textureX, textureY); // Box 19
		ammoModel[9] = new ModelRendererTurbo(this, 99, 226, textureX, textureY); // Box 20
		ammoModel[10] = new ModelRendererTurbo(this, 99, 205, textureX, textureY); // Box 21
		ammoModel[11] = new ModelRendererTurbo(this, 99, 226, textureX, textureY); // Box 22
		ammoModel[12] = new ModelRendererTurbo(this, 62, 225, textureX, textureY); // Box 23

		ammoModel[0].addShapeBox(-5F, 0F, -2F, 7, 1, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 86
		ammoModel[0].setRotationPoint(18.5F, -8.5F, 0F);
		ammoModel[0].rotateAngleZ = 0.15707963F;

		ammoModel[1].addShapeBox(2F, 0F, -2F, 3, 1, 4, 0F, 0F, 0F, -1F, 0F, -0.5F, -1.5F, 0F, -0.5F, -1.5F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 87
		ammoModel[1].setRotationPoint(18.5F, -8.5F, 0F);
		ammoModel[1].rotateAngleZ = 0.15707963F;

		ammoModel[2].addBox(-6F, 1F, -3F, 12, 3, 6, 0F); // Box 13
		ammoModel[2].setRotationPoint(18.5F, -8.5F, 0F);
		ammoModel[2].rotateAngleZ = 0.15707963F;

		ammoModel[3].addShapeBox(-6F, 4F, -3F, 12, 3, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, 4F); // Box 14
		ammoModel[3].setRotationPoint(18.5F, -8.5F, 0F);
		ammoModel[3].rotateAngleZ = 0.15707963F;

		ammoModel[4].addShapeBox(-6F, 7F, -7F, 12, 3, 14, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 2F); // Box 15
		ammoModel[4].setRotationPoint(18.5F, -8.5F, 0F);
		ammoModel[4].rotateAngleZ = 0.15707963F;

		ammoModel[5].addBox(-6F, 10F, -9F, 12, 6, 18, 0F); // Box 16
		ammoModel[5].setRotationPoint(18.5F, -8.5F, 0F);
		ammoModel[5].rotateAngleZ = 0.15707963F;

		ammoModel[6].addShapeBox(-6F, 16F, -7F, 12, 3, 14, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 17
		ammoModel[6].setRotationPoint(18.5F, -8.5F, 0F);
		ammoModel[6].rotateAngleZ = 0.15707963F;

		ammoModel[7].addShapeBox(-6F, 19F, -3F, 12, 3, 6, 0F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 18
		ammoModel[7].setRotationPoint(18.5F, -8.5F, 0F);
		ammoModel[7].rotateAngleZ = 0.15707963F;

		ammoModel[8].addShapeBox(6F, 18F, -2F, 1, 3, 4, 0F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 19
		ammoModel[8].setRotationPoint(18.5F, -8.5F, 0F);
		ammoModel[8].rotateAngleZ = 0.15707963F;

		ammoModel[9].addShapeBox(6F, 15F, -6F, 1, 3, 12, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 20
		ammoModel[9].setRotationPoint(18.5F, -8.5F, 0F);
		ammoModel[9].rotateAngleZ = 0.15707963F;

		ammoModel[10].addBox(6F, 11F, -8F, 1, 4, 16, 0F); // Box 21
		ammoModel[10].setRotationPoint(18.5F, -8.5F, 0F);
		ammoModel[10].rotateAngleZ = 0.15707963F;

		ammoModel[11].addShapeBox(6F, 8F, -6F, 1, 3, 12, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 2F); // Box 22
		ammoModel[11].setRotationPoint(18.5F, -8.5F, 0F);
		ammoModel[11].rotateAngleZ = 0.15707963F;

		ammoModel[12].addShapeBox(6F, 5F, -2F, 1, 3, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, 4F); // Box 23
		ammoModel[12].setRotationPoint(18.5F, -8.5F, 0F);
		ammoModel[12].rotateAngleZ = 0.15707963F;


		slideModel = new ModelRendererTurbo[3];
		slideModel[0] = new ModelRendererTurbo(this, 141, 116, textureX, textureY); // Box 79
		slideModel[1] = new ModelRendererTurbo(this, 24, 11, textureX, textureY); // Box 155
		slideModel[2] = new ModelRendererTurbo(this, 24, 11, textureX, textureY); // Box 156

		slideModel[0].addShapeBox(0F, 0F, 0F, 14, 3, 1, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 79
		slideModel[0].setRotationPoint(12F, -13.5F, -3.5F);

		slideModel[1].addShapeBox(0F, 0F, 0F, 2, 1, 6, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 155
		slideModel[1].setRotationPoint(22.5F, -13F, -9F);

		slideModel[2].addShapeBox(0F, 0F, 0F, 2, 1, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 156
		slideModel[2].setRotationPoint(22.5F, -12F, -9F);

		barrelAttachPoint = new Vector3f(85F /16F, 12F /16F, 0F /16F);
		stockAttachPoint = new Vector3f(-10F /16F, 11F /16F, 0F /16F);
		scopeAttachPoint = new Vector3f(9F /16F, 21.5F /16F, 0F /16F);
		gripAttachPoint = new Vector3f(49 /16F, 7F /16F, 0F /16F);

		gunSlideDistance = 0.5F;
		animationType = EnumAnimationType.BOTTOM_CLIP;

		translateAll(0F, 0F, 0F);


		flipAll();
	}
}