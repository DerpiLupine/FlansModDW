package com.flansmod.client.model.dwdefence;

import com.flansmod.client.model.ModelCustomArmour;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.common.vector.Vector3f;

public class ModelWingbreakerBody extends ModelCustomArmour
{
	int textureX = 256;
	int textureY = 128;

	public ModelWingbreakerBody()
	{
		bodyModel = new ModelRendererTurbo[22];
		bodyModel[0] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // body1
		bodyModel[1] = new ModelRendererTurbo(this, 57, 1, textureX, textureY); // scarf1
		bodyModel[2] = new ModelRendererTurbo(this, 105, 1, textureX, textureY); // scarf2
		bodyModel[3] = new ModelRendererTurbo(this, 126, 1, textureX, textureY); // scarf3
		bodyModel[4] = new ModelRendererTurbo(this, 137, 1, textureX, textureY); // beltPart
		bodyModel[5] = new ModelRendererTurbo(this, 42, 1, textureX, textureY); // beltPouch1
		bodyModel[6] = new ModelRendererTurbo(this, 42, 1, textureX, textureY); // beltpouch2
		bodyModel[7] = new ModelRendererTurbo(this, 187, 1, textureX, textureY); // backpack1
		bodyModel[8] = new ModelRendererTurbo(this, 50, 15, textureX, textureY); // backpack2
		bodyModel[9] = new ModelRendererTurbo(this, 50, 23, textureX, textureY); // backpack3
		bodyModel[10] = new ModelRendererTurbo(this, 56, 1, textureX, textureY); // backpack4
		bodyModel[11] = new ModelRendererTurbo(this, 56, 1, textureX, textureY); // backpack5
		bodyModel[12] = new ModelRendererTurbo(this, 197, 37, textureX, textureY); // wingPart1
		bodyModel[13] = new ModelRendererTurbo(this, 197, 37, textureX, textureY); // wingPart2
		bodyModel[14] = new ModelRendererTurbo(this, 220, 37, textureX, textureY); // wingFeatherLeft1
		bodyModel[15] = new ModelRendererTurbo(this, 220, 37, textureX, textureY); // wingFeatherLeft2
		bodyModel[16] = new ModelRendererTurbo(this, 220, 37, textureX, textureY); // wingFeatherLeft3
		bodyModel[17] = new ModelRendererTurbo(this, 220, 37, textureX, textureY); // wingFeatherLeft4
		bodyModel[18] = new ModelRendererTurbo(this, 220, 37, textureX, textureY); // wingFeatherRight1
		bodyModel[19] = new ModelRendererTurbo(this, 220, 37, textureX, textureY); // wingFeatherRight2
		bodyModel[20] = new ModelRendererTurbo(this, 220, 37, textureX, textureY); // wingFeatherRight3
		bodyModel[21] = new ModelRendererTurbo(this, 220, 37, textureX, textureY); // wingFeatherRight4

		bodyModel[0].addShapeBox(-8.1F, -0.1F, -4.1F, 16, 24, 8, 0F,0F, 0F, 0F,0.2F, 0F, 0F,0.2F, 0F, 0.2F,0F, 0F, 0.2F,0F, 0.2F, 0F,0.2F, 0.2F, 0F,0.2F, 0.2F, 0.2F,0F, 0.2F, 0.2F); // body1

		bodyModel[1].addShapeBox(-6.5F, -0.2F, -5F, 13, 3, 10, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,-1F, 0F, 0F,-1F, 0F, 0F,-1F, 0F, 0F,-1F, 0F, 0F); // scarf1

		bodyModel[2].addShapeBox(0F, 2F, -4.9F, 4, 12, 1, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, -0.5F,0F, 0F, -0.5F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, -0.5F,0F, 0F, -0.5F); // scarf2

		bodyModel[3].addShapeBox(0.8F, 2F, -4.5F, 4, 10, 1, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, -0.5F,0F, 0F, -0.5F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, -0.5F,0F, 0F, -0.5F); // scarf3

		bodyModel[4].addShapeBox(-8.3F, 21F, -4.3F, 16, 2, 8, 0F,0F, 0F, 0F,0.3F, 0F, 0F,0.3F, 0F, 0.6F,0F, 0F, 0.6F,0F, 0F, 0F,0.3F, 0F, 0F,0.3F, 0F, 0.6F,0F, 0F, 0.6F); // beltPart

		bodyModel[5].addBox(4.5F, 21.5F, -6F, 3, 4, 2, 0F); // beltPouch1

		bodyModel[6].addBox(1.2F, 21.5F, -6F, 3, 4, 2, 0F); // beltpouch2

		bodyModel[7].addBox(-6F, 3F, 4F, 12, 15, 5, 0F); // backpack1

		bodyModel[8].addBox(-5.5F, 3.5F, 9F, 11, 4, 3, 0F); // backpack2

		bodyModel[9].addBox(-5.5F, 8F, 9F, 11, 10, 4, 0F); // backpack3

		bodyModel[10].addShapeBox(-7.5F, 11.5F, 9.5F, 2, 6, 3, 0F,0F, -1F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, -1F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F); // backpack4

		bodyModel[11].addShapeBox(5.5F, 11.5F, 9.5F, 2, 6, 3, 0F,0F, 0F, 0F,0F, -1F, 0F,0F, -1F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F); // backpack5

		bodyModel[12].addShapeBox(-14F, 5F, 5F, 9, 5, 2, 0F,0F, 8F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 8F, 0F,4F, -8F, 0F,0F, -2F, 0F,0F, -2F, 0F,4F, -8F, 0F); // wingPart1

		bodyModel[13].addShapeBox(5F, 5F, 5F, 9, 5, 2, 0F,0F, 0F, 0F,0F, 8F, 0F,0F, 8F, 0F,0F, 0F, 0F,0F, -2F, 0F,4F, -8F, 0F,4F, -8F, 0F,0F, -2F, 0F); // wingPart2

		bodyModel[14].addShapeBox(14.5F, 7.5F, 5.5F, 9, 5, 1, 0F,0F, 8F, 0F,6F, -10F, 0F,6F, -10F, 0F,0F, 8F, 0F,2F, -10F, 0F,1F, 4F, 0F,1F, 4F, 0F,2F, -10F, 0F); // wingFeatherRight1

		bodyModel[15].addShapeBox(11.5F, 9F, 5.5F, 9, 5, 1, 0F,0F, 8F, 0F,5F, -10F, 0F,5F, -10F, 0F,0F, 8F, 0F,2F, -10F, 0F,0F, 4F, 0F,0F, 4F, 0F,2F, -10F, 0F); // wingFeatherRight2

		bodyModel[16].addShapeBox(9.5F, 12F, 5.5F, 9, 5, 1, 0F,0F, 8F, 0F,2F, -8F, 0F,2F, -8F, 0F,0F, 8F, 0F,2F, -10F, 0F,-3F, 2F, 0F,-3F, 2F, 0F,2F, -10F, 0F); // wingFeatherRight3

		bodyModel[17].addShapeBox(6F, 12.5F, 5.5F, 9, 5, 1, 0F,0F, 8F, 0F,0F, -8F, 0F,0F, -8F, 0F,0F, 8F, 0F,2F, -10F, 0F,-5F, 1F, 0F,-5F, 1F, 0F,2F, -10F, 0F); // wingFeatherRight4

		bodyModel[18].addShapeBox(-23.5F, 7.5F, 5.5F, 9, 5, 1, 0F,6F, -10F, 0F,0F, 8F, 0F,0F, 8F, 0F,6F, -10F, 0F,1F, 4F, 0F,2F, -10F, 0F,2F, -10F, 0F,1F, 4F, 0F); // wingFeatherLeft1

		bodyModel[19].addShapeBox(-20.5F, 9F, 5.5F, 9, 5, 1, 0F,5F, -10F, 0F,0F, 8F, 0F,0F, 8F, 0F,5F, -10F, 0F,0F, 4F, 0F,2F, -10F, 0F,2F, -10F, 0F,0F, 4F, 0F); // wingFeatherLeft2

		bodyModel[20].addShapeBox(-18.5F, 12F, 5.5F, 9, 5, 1, 0F,2F, -8F, 0F,0F, 8F, 0F,0F, 8F, 0F,2F, -8F, 0F,-3F, 2F, 0F,2F, -10F, 0F,2F, -10F, 0F,-3F, 2F, 0F); // wingFeatherLeft3

		bodyModel[21].addShapeBox(-15F, 12.5F, 5.5F, 9, 5, 1, 0F,0F, -8F, 0F,0F, 8F, 0F,0F, 8F, 0F,0F, -8F, 0F,-5F, 1F, 0F,2F, -10F, 0F,2F, -10F, 0F,-5F, 1F, 0F); // wingFeatherLeft4


		leftArmModel = new ModelRendererTurbo[7];
		leftArmModel[0] = new ModelRendererTurbo(this, 161, 15, textureX, textureY); // leftArm
		leftArmModel[1] = new ModelRendererTurbo(this, 227, 19, textureX, textureY); // leftArmPlating1
		leftArmModel[2] = new ModelRendererTurbo(this, 109, 7, textureX, textureY); // leftArmPlating2
		leftArmModel[3] = new ModelRendererTurbo(this, 81, 28, textureX, textureY); // pipBuck1
		leftArmModel[4] = new ModelRendererTurbo(this, 81, 28, textureX, textureY); // pipBuck2
		leftArmModel[5] = new ModelRendererTurbo(this, 194, 23, textureX, textureY); // pipBuck3
		leftArmModel[6] = new ModelRendererTurbo(this, 156, 16, textureX, textureY); // pipBuck4

		leftArmModel[0].addShapeBox(-2.1F, -4.1F, -4.1F, 8, 20, 8, 0F,0F, 0F, 0F,0.2F, 0F, 0F,0.2F, 0F, 0.2F,0F, 0F, 0.2F,0F, 0.2F, 0F,0.2F, 0.2F, 0F,0.2F, 0.2F, 0.2F,0F, 0.2F, 0.2F); // leftArm
		leftArmModel[0].setRotationPoint(7.9F, -0.1F, -4.1F);
		//(10,4,0);

		leftArmModel[1].addShapeBox(1.5F, -4.5F, -4.5F, 5, 8, 9, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, -2F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, -2F, 0F); // leftArmPlating1
		leftArmModel[1].setRotationPoint(11.5F, -0.5F, -4.5F);

		leftArmModel[2].addShapeBox(-2.4F, -2F, -4.3F, 4, 3, 8, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0.5F,0F, 0F, 0.5F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0.5F,0F, 0F, 0.5F); // leftArmPlating2
		leftArmModel[2].setRotationPoint(7.6F, 2F, -4.3F);

		leftArmModel[3].addShapeBox(-2.5F, 17F, -4.5F, 9, 1, 9, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F); // pipBuck1
		leftArmModel[3].setRotationPoint(7.5F, 21F, -4.5F);

		leftArmModel[4].addShapeBox(-2.5F, 11F, -4.5F, 9, 1, 9, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F); // pipBuck2
		leftArmModel[4].setRotationPoint(7.5F, 15F, -4.5F);

		leftArmModel[5].addShapeBox(-2.2F, 12F, -4.2F, 8, 5, 8, 0F,0F, 0F, 0F,0.4F, 0F, 0F,0.4F, 0F, 0.4F,0F, 0F, 0.4F,0F, 0F, 0F,0.4F, 0F, 0F,0.4F, 0F, 0.4F,0F, 0F, 0.4F); // pipBuck3
		leftArmModel[5].setRotationPoint(7.8F, 16F, -4.2F);

		leftArmModel[6].addShapeBox(-1.5F, 12F, -4.7F, 4, 5, 1, 0F,-0.5F, -0.5F, 0F,-0.5F, -0.5F, 0F,0.4F, 0F, 0.4F,0F, 0F, 0.4F,-0.5F, -0.5F, 0F,-0.5F, -0.5F, 0F,0.4F, 0F, 0.4F,0F, 0F, 0.4F); // pipBuck4
		leftArmModel[6].setRotationPoint(8.5F, 16F, -4.7F);


		rightArmModel = new ModelRendererTurbo[4];
		rightArmModel[0] = new ModelRendererTurbo(this, 128, 15, textureX, textureY); // rightArm
		rightArmModel[1] = new ModelRendererTurbo(this, 78, 15, textureX, textureY); // rightArm2
		rightArmModel[2] = new ModelRendererTurbo(this, 222, 1, textureX, textureY); // rightArmPlating1
		rightArmModel[3] = new ModelRendererTurbo(this, 109, 7, textureX, textureY); // rightArmPlating2

		rightArmModel[0].addShapeBox(-6.1F, -4.1F, -4.1F, 8, 22, 8, 0F,0F, 0F, 0F,0.2F, 0F, 0F,0.2F, 0F, 0.2F,0F, 0F, 0.2F,0F, 0.2F, 0F,0.2F, 0.2F, 0F,0.2F, 0.2F, 0.2F,0F, 0.2F, 0.2F); // rightArm
		rightArmModel[0].setRotationPoint(-16.1F, -0.1F, -4.1F);

		rightArmModel[1].addShapeBox(-6.2F, 16.2F, -4.2F, 8, 2, 8, 0F,0F, 0F, 0F,0.4F, 0F, 0F,0.4F, 0F, 0.4F,0F, 0F, 0.4F,0F, 0F, 0F,0.4F, 0F, 0F,0.4F, 0F, 0.4F,0F, 0F, 0.4F); // rightArm2
		rightArmModel[1].setRotationPoint(-16.2F, 20.2F, -4.2F);

		rightArmModel[2].addShapeBox(-6.5F, -4.5F, -4.5F, 5, 8, 9, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, -2F, 0F,0F, -2F, 0F,0F, 0F, 0F); // rightArmPlating1
		rightArmModel[2].setRotationPoint(-16.5F, -0.5F, -4.5F);

		rightArmModel[3].addShapeBox(-1.6F, -2F, -4.3F, 4, 3, 8, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0.5F,0F, 0F, 0.5F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0.5F,0F, 0F, 0.5F); // rightArmPlating2
		rightArmModel[3].setRotationPoint(-11.6F, 2F, -4.3F);
	}
}