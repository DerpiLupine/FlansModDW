package com.flansmod.client.model.dwdefence; //Path where the model is located

import com.flansmod.client.model.ModelCustomArmour;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelFrostweatherBody extends ModelCustomArmour //Same as Filename
{
	int textureX = 512;
	int textureY = 256;

	public ModelFrostweatherBody() //Same as Filename
	{
		bodyModel = new ModelRendererTurbo[26];
		bodyModel[0] = new ModelRendererTurbo(this, 1, 46, textureX, textureY); // armor2
		bodyModel[1] = new ModelRendererTurbo(this, 58, 28, textureX, textureY); // backArmorPlate
		bodyModel[2] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // body1
		bodyModel[3] = new ModelRendererTurbo(this, 58, 42, textureX, textureY); // frontArmorPlate
		bodyModel[4] = new ModelRendererTurbo(this, 100, 41, textureX, textureY); // pocket3
		bodyModel[5] = new ModelRendererTurbo(this, 100, 59, textureX, textureY); // pocket4
		bodyModel[6] = new ModelRendererTurbo(this, 1, 34, textureX, textureY); // Box 20
		bodyModel[7] = new ModelRendererTurbo(this, 58, 14, textureX, textureY); // Box 23
		bodyModel[8] = new ModelRendererTurbo(this, 58, 1, textureX, textureY); // Box 25
		bodyModel[9] = new ModelRendererTurbo(this, 100, 29, textureX, textureY); // Box 39
		bodyModel[10] = new ModelRendererTurbo(this, 100, 1, textureX, textureY); // Box 40
		bodyModel[11] = new ModelRendererTurbo(this, 100, 37, textureX, textureY); // Box 41
		bodyModel[12] = new ModelRendererTurbo(this, 100, 22, textureX, textureY); // Box 42
		bodyModel[13] = new ModelRendererTurbo(this, 152, 19, textureX, textureY); // Box 63
		bodyModel[14] = new ModelRendererTurbo(this, 135, 8, textureX, textureY); // Box 64
		bodyModel[15] = new ModelRendererTurbo(this, 135, 19, textureX, textureY); // Box 65
		bodyModel[16] = new ModelRendererTurbo(this, 152, 8, textureX, textureY); // Box 66
		bodyModel[17] = new ModelRendererTurbo(this, 135, 1, textureX, textureY); // Box 1
		bodyModel[18] = new ModelRendererTurbo(this, 135, 1, textureX, textureY); // Box 2
		bodyModel[19] = new ModelRendererTurbo(this, 67, 91, textureX, textureY); // Box 12
		bodyModel[20] = new ModelRendererTurbo(this, 67, 131, textureX, textureY); // Box 13
		bodyModel[21] = new ModelRendererTurbo(this, 76, 131, textureX, textureY); // Box 14
		bodyModel[22] = new ModelRendererTurbo(this, 67, 118, textureX, textureY); // Box 15
		bodyModel[23] = new ModelRendererTurbo(this, 67, 105, textureX, textureY); // Box 16
		bodyModel[24] = new ModelRendererTurbo(this, 86, 105, textureX, textureY); // Box 17
		bodyModel[25] = new ModelRendererTurbo(this, 67, 114, textureX, textureY); // Box 18

		bodyModel[0].addShapeBox(-8.5F, 19F, -4.5F, 17, 4, 9, 0F, 0.1F, 0F, 0.1F, 0F, 0F, 0.1F, 0F, 0F, 0.1F, 0.1F, 0F, 0.1F, 0.1F, 0F, 0.1F, 0F, 0F, 0.1F, 0F, 0F, 0.1F, 0.1F, 0F, 0.1F); // armor2
		bodyModel[0].setRotationPoint(0F, 0F, 0F);

		bodyModel[1].addBox(-7F, 3.5F, 4F, 14, 12, 1, 0F); // backArmorPlate
		bodyModel[1].setRotationPoint(0F, 0F, 0F);

		bodyModel[2].addShapeBox(-8.1F, -0.1F, -4.1F, 16, 24, 8, 0F, 0F, 0F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0.2F, 0F, 0.2F, 0.2F, 0F, 0.2F, 0.2F, 0.2F, 0F, 0.2F, 0.2F); // body1
		bodyModel[2].setRotationPoint(0F, 0F, 0F);

		bodyModel[3].addBox(-7F, 3.5F, -5F, 14, 12, 1, 0F); // frontArmorPlate
		bodyModel[3].setRotationPoint(0F, 0F, 0F);

		bodyModel[4].addBox(8F, 24F, -4.5F, 3, 8, 9, 0F); // pocket3
		bodyModel[4].setRotationPoint(0F, 0F, 0F);

		bodyModel[5].addShapeBox(11F, 24F, -4.5F, 1, 4, 9, 0F, 0F, 0F, 0F, -0.5F, -0.2F, 0F, -0.5F, -0.2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F); // pocket4
		bodyModel[5].setRotationPoint(0F, 0F, 0F);

		bodyModel[6].addShapeBox(-8.5F, 16F, -4.5F, 17, 2, 9, 0F, 0.1F, -1F, 0.1F, 0F, 0F, 0.1F, 0F, 0F, 0.1F, 0.1F, -1F, 0.1F, 0.1F, 1F, 0.1F, 0F, 0F, 0.1F, 0F, 0F, 0.1F, 0.1F, 1F, 0.1F); // Box 20
		bodyModel[6].setRotationPoint(0F, 0F, 0F);

		bodyModel[7].addShapeBox(-8.1F, 23.9F, -4.1F, 8, 5, 8, 0F, 0F, 0F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0.2F, 0F, 0F, 0.2F, 0.5F, 0.2F, 0.4F, -0.3F, 0.2F, 0.4F, -0.3F, 0.2F, 0.6F, 0.5F, 0.2F, 0.6F); // Box 23
		bodyModel[7].setRotationPoint(0F, 0F, 0F);

		bodyModel[8].addShapeBox(0.1F, 23.9F, -4.1F, 8, 5, 8, 0F, 0.2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.2F, 0.2F, 0F, 0.2F, -0.3F, 0.2F, 0.4F, 0.5F, 0.2F, 0.4F, 0.5F, 0.2F, 0.6F, -0.3F, 0.2F, 0.6F); // Box 25
		bodyModel[8].setRotationPoint(0F, 0F, 0F);

		bodyModel[9].addShapeBox(-6F, 4F, 9F, 12, 6, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, -0.5F, 0F, -1F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 39
		bodyModel[9].setRotationPoint(0F, 0F, 0F);

		bodyModel[10].addBox(-6F, 4F, 4F, 12, 15, 5, 0F); // Box 40
		bodyModel[10].setRotationPoint(0F, 0F, 0F);

		bodyModel[11].addBox(-2F, 10F, 8.4F, 4, 2, 1, 0F); // Box 41
		bodyModel[11].setRotationPoint(0F, 0F, 0F);

		bodyModel[12].addBox(-5.5F, 19F, 3.5F, 11, 1, 5, 0F); // Box 42
		bodyModel[12].setRotationPoint(0F, 0F, 0F);

		bodyModel[13].addShapeBox(1F, 15.8F, -7F, 6, 3, 1, 0F, 0F, -0.2F, -0.5F, 0F, -0.2F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 63
		bodyModel[13].setRotationPoint(0F, 0F, 0F);

		bodyModel[14].addBox(1F, 15.8F, -6F, 6, 8, 2, 0F); // Box 64
		bodyModel[14].setRotationPoint(0F, 0F, 0F);

		bodyModel[15].addShapeBox(-7F, 15.8F, -7F, 6, 3, 1, 0F, 0F, -0.2F, -0.5F, 0F, -0.2F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 65
		bodyModel[15].setRotationPoint(0F, 0F, 0F);

		bodyModel[16].addBox(-7F, 15.8F, -6F, 6, 8, 2, 0F); // Box 66
		bodyModel[16].setRotationPoint(0F, 0F, 0F);

		bodyModel[17].addShapeBox(-7.5F, 24F, 4.5F, 15, 2, 4, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0.5F, -2F, -1F, -0.5F, 0F, -1F, -0.5F, 0F, -1F, 0.5F, -2F, -1F); // Box 1
		bodyModel[17].setRotationPoint(0F, 0F, 0F);

		bodyModel[18].addShapeBox(-7.5F, 20F, 4.5F, 15, 2, 4, 0F, -0.5F, 0F, -1F, 0.5F, -2F, -1F, 0.5F, -2F, -1F, -0.5F, 0F, -1F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F); // Box 2
		bodyModel[18].setRotationPoint(0F, 0F, 0F);

		bodyModel[19].addBox(-6F, -1.5F, -5F, 12, 3, 10, 0F); // Box 12
		bodyModel[19].setRotationPoint(0F, 0F, 0F);

		bodyModel[20].addShapeBox(3F, 1.5F, -5.1F, 3, 10, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0.5F, 1F, -0.5F, 0.5F, 1F, -0.5F, 0F, -1F, 0F, 0F); // Box 13
		bodyModel[20].setRotationPoint(0F, 0F, 0F);

		bodyModel[21].addShapeBox(1F, 1.5F, -5F, 3, 8, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 14
		bodyModel[21].setRotationPoint(0F, 0F, 0F);

		bodyModel[22].addShapeBox(-7F, -0.5F, 4F, 14, 4, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, -1F, 0F, -2F, -1F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -3F, 0F, -2F, -3F, 0F, -2F); // Box 15
		bodyModel[22].setRotationPoint(0F, 0F, 0F);

		bodyModel[23].addShapeBox(-7F, -0.5F, 4F, 2, 1, 7, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 1F, 0F, 0F, -1F, 0F, 0F); // Box 16
		bodyModel[23].setRotationPoint(0F, 0F, 0F);

		bodyModel[24].addShapeBox(5F, -0.5F, 4F, 2, 1, 7, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, -1F, 0F, 0F, 1F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -1F, 0F, 0F, 1F, 0F, 0F); // Box 17
		bodyModel[24].setRotationPoint(0F, 0F, 0F);

		bodyModel[25].addShapeBox(-6F, -0.5F, 11F, 12, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 18
		bodyModel[25].setRotationPoint(0F, 0F, 0F);


		leftArmModel = new ModelRendererTurbo[8];
		leftArmModel[0] = new ModelRendererTurbo(this, 67, 70, textureX, textureY); // emblemPatch
		leftArmModel[1] = new ModelRendererTurbo(this, 1, 60, textureX, textureY); // leftArm
		leftArmModel[2] = new ModelRendererTurbo(this, 1, 146, textureX, textureY); // Box 4
		leftArmModel[3] = new ModelRendererTurbo(this, 38, 91, textureX, textureY); // Box 67
		leftArmModel[4] = new ModelRendererTurbo(this, 38, 120, textureX, textureY); // Box 68
		leftArmModel[5] = new ModelRendererTurbo(this, 1, 133, textureX, textureY); // Box 6
		leftArmModel[6] = new ModelRendererTurbo(this, 38, 108, textureX, textureY); // Box 8
		leftArmModel[7] = new ModelRendererTurbo(this, 1, 91, textureX, textureY); // Box 10

		leftArmModel[0].addShapeBox(5.2F, 1.5F, -4F, 1, 6, 14, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -6F, 0F, 0F, -6F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, -6F, 0F, -3F, -6F); // emblemPatch
		leftArmModel[0].setRotationPoint(0F, 0F, 0F);

		leftArmModel[1].addShapeBox(-2.1F, -4.1F, -4.1F, 8, 22, 8, 0F, 0F, 0F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0.2F, 0F, 0.2F, 0.2F, 0F, 0.2F, 0.2F, 0.2F, 0F, 0.2F, 0.2F); // leftArm
		leftArmModel[1].setRotationPoint(0F, 0F, 0F);

		leftArmModel[2].addShapeBox(-2.5F, 18F, -4.5F, 9, 2, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F); // Box 4
		leftArmModel[2].setRotationPoint(0F, 0F, 0F);

		leftArmModel[3].addShapeBox(-2.5F, 11F, -4.5F, 5, 7, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 67
		leftArmModel[3].setRotationPoint(0F, 0F, 0F);

		leftArmModel[4].addShapeBox(-2F, 11.5F, -4.55F, 4, 6, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 68
		leftArmModel[4].setRotationPoint(0F, 0F, 0F);

		leftArmModel[5].addShapeBox(-2.5F, 8F, -4.5F, 9, 3, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 6
		leftArmModel[5].setRotationPoint(0F, 0F, 0F);

		leftArmModel[6].addShapeBox(2.5F, 16F, -4.5F, 4, 2, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 8
		leftArmModel[6].setRotationPoint(0F, 0F, 0F);

		leftArmModel[7].addShapeBox(-2.5F, -4.5F, -4.5F, 9, 4, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0.5F, 0F, 0.5F, 0.5F, 0F, 0.5F, 0F, 0F, 0.5F); // Box 10
		leftArmModel[7].setRotationPoint(0F, 0F, 0F);


		rightArmModel = new ModelRendererTurbo[5];
		rightArmModel[0] = new ModelRendererTurbo(this, 34, 60, textureX, textureY); // Box 14
		rightArmModel[1] = new ModelRendererTurbo(this, 1, 119, textureX, textureY); // Box 14
		rightArmModel[2] = new ModelRendererTurbo(this, 67, 70, textureX, textureY); // Box 14
		rightArmModel[3] = new ModelRendererTurbo(this, 1, 105, textureX, textureY); // Box 4
		rightArmModel[4] = new ModelRendererTurbo(this, 1, 133, textureX, textureY); // Box 5

		rightArmModel[0].addShapeBox(-6.1F, -4.1F, -4.1F, 8, 22, 8, 0F, 0F, 0F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0.2F, 0F, 0.2F, 0.2F, 0F, 0.2F, 0.2F, 0.2F, 0F, 0.2F, 0.2F); // Box 14
		rightArmModel[0].setRotationPoint(0F, 0F, 0F);

		rightArmModel[1].addShapeBox(-6.5F, 16F, -4.5F, 9, 4, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F); // Box 14
		rightArmModel[1].setRotationPoint(0F, 0F, 0F);

		rightArmModel[2].addShapeBox(-6.2F, 1.5F, -4F, 1, 6, 14, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -6F, 0F, 0F, -6F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, -6F, 0F, -3F, -6F); // Box 14
		rightArmModel[2].setRotationPoint(0F, 0F, 0F);

		rightArmModel[3].addShapeBox(-6.5F, -4.5F, -4.5F, 9, 4, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0.5F, 0F, 0.5F); // Box 4
		rightArmModel[3].setRotationPoint(0F, 0F, 0F);

		rightArmModel[4].addShapeBox(-6.5F, 8F, -4.5F, 9, 3, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 5
		rightArmModel[4].setRotationPoint(0F, 0F, 0F);


		skirtFrontModel = new ModelRendererTurbo[2];
		skirtFrontModel[0] = new ModelRendererTurbo(this, 135, 24, textureX, textureY); // Box 28
		skirtFrontModel[1] = new ModelRendererTurbo(this, 135, 30, textureX, textureY); // Box 29

		skirtFrontModel[0].addShapeBox(-6F, -1F, -4.5F, 12, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 28
		skirtFrontModel[0].setRotationPoint(0F, 0F, 0F);

		skirtFrontModel[1].addShapeBox(-6F, 3F, -5F, 12, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, -0.5F, -1F, 0F, -0.5F); // Box 29
		skirtFrontModel[1].setRotationPoint(0F, 0F, 0F);


	}
}