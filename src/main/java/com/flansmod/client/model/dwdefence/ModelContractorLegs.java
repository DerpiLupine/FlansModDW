package com.flansmod.client.model.dwdefence;

import com.flansmod.client.model.ModelCustomArmour;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelContractorLegs extends ModelCustomArmour
{
	int textureX = 512;
	int textureY = 256;

	public ModelContractorLegs()
	{
		leftLegModel = new ModelRendererTurbo[7];
		leftLegModel[0] = new ModelRendererTurbo(this, 123, 25, textureX, textureY); // leftPants
		leftLegModel[1] = new ModelRendererTurbo(this, 160, 38, textureX, textureY); // leftPocket
		leftLegModel[2] = new ModelRendererTurbo(this, 160, 25, textureX, textureY); // leftStrap
		leftLegModel[3] = new ModelRendererTurbo(this, 175, 42, textureX, textureY); // pocketHolder
		leftLegModel[4] = new ModelRendererTurbo(this, 160, 72, textureX, textureY); // kneeArmor
		leftLegModel[5] = new ModelRendererTurbo(this, 160, 81, textureX, textureY); // kneeArmor2
		leftLegModel[6] = new ModelRendererTurbo(this, 160, 81, textureX, textureY); // kneeArmor2

		leftLegModel[0].addBox(-4.5F, -0.8F, -4.5F, 9, 24, 9, 0F); // leftPants

		leftLegModel[1].addBox(4F, 0.8F, -3F, 2, 8, 5, 0F); // leftPocket

		leftLegModel[2].addShapeBox(-4.8F, 2F, -4.8F, 9, 3, 9, 0F, 0F, -1F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, -1F, 0.4F, 0F, 1F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 1F, 0.4F); // leftStrap

		leftLegModel[3].addBox(4F, 2F, -3.5F, 1, 3, 6, 0F); // pocketHolder

		leftLegModel[4].addBox(-3F, 8F, -5.5F, 6, 7, 1, 0F); // kneeArmor

		leftLegModel[5].addShapeBox(-3F, 7F, -5.5F, 6, 1, 1, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // kneeArmor2

		leftLegModel[6].addShapeBox(-3F, 15F, -5.5F, 6, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // kneeArmor2

		//-4X, -23Y 


		rightLegModel = new ModelRendererTurbo[10];
		rightLegModel[0] = new ModelRendererTurbo(this, 123, 59, textureX, textureY); // rightPants
		rightLegModel[1] = new ModelRendererTurbo(this, 160, 59, textureX, textureY); // rightStrap
		rightLegModel[2] = new ModelRendererTurbo(this, 160, 38, textureX, textureY); // Box 0
		rightLegModel[3] = new ModelRendererTurbo(this, 175, 42, textureX, textureY); // Box 1
		rightLegModel[4] = new ModelRendererTurbo(this, 123, 93, textureX, textureY); // strap
		rightLegModel[5] = new ModelRendererTurbo(this, 123, 104, textureX, textureY); // strap
		rightLegModel[6] = new ModelRendererTurbo(this, 160, 72, textureX, textureY); // kneeArmor
		rightLegModel[7] = new ModelRendererTurbo(this, 160, 81, textureX, textureY); // kneeArmor2
		rightLegModel[8] = new ModelRendererTurbo(this, 160, 81, textureX, textureY); // kneeArmor2
		rightLegModel[9] = new ModelRendererTurbo(this, 123, 104, textureX, textureY); // Box 16

		rightLegModel[0].addBox(-4.5F, -0.8F, -4.5F, 9, 24, 9, 0F); // rightPants

		rightLegModel[1].addShapeBox(-4.8F, 2F, -4.8F, 9, 3, 9, 0F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F); // rightStrap

		rightLegModel[2].addBox(-6F, 0.8F, -3F, 2, 8, 5, 0F); // Box 0

		rightLegModel[3].addBox(-5F, 2F, -3.5F, 1, 3, 6, 0F); // Box 1

		rightLegModel[4].addShapeBox(-4.8F, 5.5F, -4.8F, 9, 1, 9, 0F, 0F, -0.5F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, -0.5F, 0.4F, 0F, 0.5F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0.5F, 0.4F); // strap

		rightLegModel[5].addShapeBox(-4.8F, 8F, -4.8F, 9, 1, 9, 0F, 0F, 1F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 1F, 0.4F, 0F, -1F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, -1F, 0.4F); // strap

		rightLegModel[6].addBox(-3F, 8F, -5.5F, 6, 7, 1, 0F); // kneeArmor

		rightLegModel[7].addShapeBox(-3F, 15F, -5.5F, 6, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // kneeArmor2

		rightLegModel[8].addShapeBox(-3F, 7F, -5.5F, 6, 1, 1, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // kneeArmor2

		rightLegModel[9].addShapeBox(-4.8F, 8.5F, -4.8F, 9, 1, 9, 0F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F); // Box 16

		//+4X, -22Y 
	}
}