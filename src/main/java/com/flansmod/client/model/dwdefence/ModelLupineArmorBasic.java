package com.flansmod.client.model.dwdefence;

import com.flansmod.client.model.ModelCustomArmour;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelLupineArmorBasic extends ModelCustomArmour
{
	int textureX = 512;
	int textureY = 256;

	public ModelLupineArmorBasic()
	{
		bodyModel = new ModelRendererTurbo[8];
		bodyModel[0] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 0
		bodyModel[1] = new ModelRendererTurbo(this, 14, 19, textureX, textureY); // Box 1
		bodyModel[2] = new ModelRendererTurbo(this, 14, 19, textureX, textureY); // Box 2
		bodyModel[3] = new ModelRendererTurbo(this, 22, 1, textureX, textureY); // Box 3
		bodyModel[4] = new ModelRendererTurbo(this, 22, 10, textureX, textureY); // Box 4
		bodyModel[5] = new ModelRendererTurbo(this, 1, 10, textureX, textureY); // Box 5
		bodyModel[6] = new ModelRendererTurbo(this, 1, 19, textureX, textureY); // Box 6
		bodyModel[7] = new ModelRendererTurbo(this, 1, 19, textureX, textureY); // Box 7

		bodyModel[0].addBox(-4.5F, 4.75F, -2.5F, 9, 7, 1, 0F); // Box 0
		//bodyModel[0].setRotationPoint();

		bodyModel[1].addBox(-3.5F, -0.25F, -2.5F, 2, 2, 5, 0F); // Box 1
		//bodyModel[1].setRotationPoint();

		bodyModel[2].addBox(1.5F, -0.25F, -2.5F, 2, 2, 5, 0F); // Box 2
		//bodyModel[2].setRotationPoint();

		bodyModel[3].addBox(-4F, 1.75F, -2.5F, 8, 3, 1, 0F); // Box 3
		//bodyModel[3].setRotationPoint();

		bodyModel[4].addBox(-4F, 1.75F, 1.5F, 8, 3, 1, 0F); // Box 4
		//bodyModel[4].setRotationPoint();

		bodyModel[5].addBox(-4.5F, 4.75F, 1.5F, 9, 7, 1, 0F); // Box 5
		//bodyModel[5].setRotationPoint();

		bodyModel[6].addShapeBox(-4.5F, 7.75F, -2.5F, 1, 3, 5, 0F,0.1F, 0F, 0.1F,0F, 0F, 0.1F,0F, 0F, 0.1F,0.1F, 0F, 0.1F,0.1F, 0F, 0.1F,0F, 0F, 0.1F,0F, 0F, 0.1F,0.1F, 0F, 0.1F); // Box 6
		//bodyModel[6].setRotationPoint();

		bodyModel[7].addShapeBox(3.5F, 7.75F, -2.5F, 1, 3, 5, 0F,0F, 0F, 0.1F,0.1F, 0F, 0.1F,0.1F, 0F, 0.1F,0F, 0F, 0.1F,0F, 0F, 0.1F,0.1F, 0F, 0.1F,0.1F, 0F, 0.1F,0F, 0F, 0.1F); // Box 7
		//bodyModel[7].setRotationPoint();


	}
}