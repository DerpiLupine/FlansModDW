package com.flansmod.client.model.dwdefence;

import com.flansmod.client.model.ModelCustomArmour;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelContractorBody extends ModelCustomArmour
{
	int textureX = 512;
	int textureY = 256;

	public ModelContractorBody()
	{
		bodyModel = new ModelRendererTurbo[18];
		bodyModel[0] = new ModelRendererTurbo(this, 38, 70, textureX, textureY); // armorPlate1
		bodyModel[1] = new ModelRendererTurbo(this, 38, 63, textureX, textureY); // armorPlate2
		bodyModel[2] = new ModelRendererTurbo(this, 38, 75, textureX, textureY); // armorPlate3
		bodyModel[3] = new ModelRendererTurbo(this, 80, 36, textureX, textureY); // backpack1
		bodyModel[4] = new ModelRendererTurbo(this, 80, 59, textureX, textureY); // backpack2
		bodyModel[5] = new ModelRendererTurbo(this, 80, 66, textureX, textureY); // backpack3
		bodyModel[6] = new ModelRendererTurbo(this, 80, 79, textureX, textureY); // backpack4
		bodyModel[7] = new ModelRendererTurbo(this, 54, 1, textureX, textureY); // bodyArmor1
		bodyModel[8] = new ModelRendererTurbo(this, 111, 1, textureX, textureY); // bodyArmor2
		bodyModel[9] = new ModelRendererTurbo(this, 111, 1, textureX, textureY); // bodyArmor2
		bodyModel[10] = new ModelRendererTurbo(this, 65, 95, textureX, textureY); // pouch
		bodyModel[11] = new ModelRendererTurbo(this, 65, 95, textureX, textureY); // pouch
		bodyModel[12] = new ModelRendererTurbo(this, 65, 95, textureX, textureY); // pouch
		bodyModel[13] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // shirt1
		bodyModel[14] = new ModelRendererTurbo(this, 38, 48, textureX, textureY); // shoulderArmor
		bodyModel[15] = new ModelRendererTurbo(this, 38, 48, textureX, textureY); // shoulderArmor
		bodyModel[16] = new ModelRendererTurbo(this, 38, 36, textureX, textureY); // shoulderArmor2
		bodyModel[17] = new ModelRendererTurbo(this, 38, 36, textureX, textureY); // shoulderArmor2

		bodyModel[0].addBox(-8F, 5F, -5.5F, 16, 3, 1, 0F); // armorPlate1

		bodyModel[1].addBox(-8F, 8.5F, -5.5F, 16, 5, 1, 0F); // armorPlate2

		bodyModel[2].addShapeBox(-8F, 13.5F, -5.5F, 16, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // armorPlate3

		bodyModel[3].addBox(-8F, 6.5F, 5F, 16, 17, 5, 0F); // backpack1

		bodyModel[4].addShapeBox(-8F, 3.5F, 5F, 16, 3, 3, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // backpack2

		bodyModel[5].addBox(-6F, 13F, 10F, 12, 10, 2, 0F); // backpack3

		bodyModel[6].addBox(-5F, 7.5F, 10F, 10, 5, 2, 0F); // backpack4

		bodyModel[7].addBox(-9F, 4.5F, -5F, 18, 20, 10, 0F); // bodyArmor1

		bodyModel[8].addBox(-7F, -0.5F, -5F, 4, 5, 10, 0F); // bodyArmor2

		bodyModel[9].addBox(3F, -0.5F, -5F, 4, 5, 10, 0F); // bodyArmor2

		bodyModel[10].addBox(3.5F, 16.5F, -6.5F, 5, 7, 2, 0F); // pouch

		bodyModel[11].addBox(-2.5F, 16.5F, -6.5F, 5, 7, 2, 0F); // pouch

		bodyModel[12].addBox(-8.5F, 16.5F, -6.5F, 5, 7, 2, 0F); // pouch

		bodyModel[13].addBox(-8.5F, -0.2F, -4.5F, 17, 25, 9, 0F); // shirt1

		bodyModel[14].addBox(7F, -0.5F, -5F, 10, 4, 10, 0F); // shoulderArmor

		bodyModel[15].addBox(-17F, -0.5F, -5F, 10, 4, 10, 0F); // shoulderArmor

		bodyModel[16].addShapeBox(7F, -1.5F, -5F, 10, 1, 10, 0F, 0F, 0F, -0.6F, -0.6F, 0F, -0.6F, -0.6F, 0F, -0.6F, 0F, 0F, -0.6F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // shoulderArmor2

		bodyModel[17].addShapeBox(-17F, -1.5F, -5F, 10, 1, 10, 0F, -0.6F, 0F, -0.6F, 0F, 0F, -0.6F, 0F, 0F, -0.6F, -0.6F, 0F, -0.6F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // shoulderArmor2


		leftArmModel = new ModelRendererTurbo[5];
		leftArmModel[0] = new ModelRendererTurbo(this, 65, 80, textureX, textureY); // elbowArmor
		leftArmModel[1] = new ModelRendererTurbo(this, 38, 79, textureX, textureY); // emblemPatch
		leftArmModel[2] = new ModelRendererTurbo(this, 1, 36, textureX, textureY); // leftSleeve
		leftArmModel[3] = new ModelRendererTurbo(this, 1, 125, textureX, textureY); // pipBuck
		leftArmModel[4] = new ModelRendererTurbo(this, 1, 104, textureX, textureY); // pipBuck2

		leftArmModel[0].addBox(6F, 6.5F, -3F, 1, 8, 6, 0F); // elbowArmor

		leftArmModel[1].addBox(4F, 0.5F, -5F, 3, 5, 10, 0F); // emblemPatch

		leftArmModel[2].addBox(-2.5F, -4.2F, -4.5F, 9, 22, 9, 0F); // leftSleeve

		leftArmModel[3].addShapeBox(-2.7F, 10.8F, -4.7F, 18, 8, 18, 0F, 0F, -4F, 0F, -8.6F, -4F, 0F, -8.6F, -4F, -8.6F, 0F, -4F, -8.6F, 0F, 0F, 0F, -8.6F, 0F, 0F, -8.6F, 0F, -8.6F, 0F, 0F, -8.6F); // pipBuck

		leftArmModel[4].addShapeBox(-0.5F, 9.8F, -5.7F, 15, 10, 10, 0F, 0F, -4F, 0F, -8.6F, -4F, 0F, -8.6F, -4F, -8.6F, 0F, -4F, -8.6F, 0F, 0F, 0F, -8.6F, 0F, 0F, -8.6F, 0F, -8.6F, 0F, 0F, -8.6F); // pipBuck2
		//(-10,-4,0);


		rightArmModel = new ModelRendererTurbo[3];
		rightArmModel[0] = new ModelRendererTurbo(this, 65, 80, textureX, textureY); // elbowArmor
		rightArmModel[1] = new ModelRendererTurbo(this, 38, 79, textureX, textureY); // emblemPatch
		rightArmModel[2] = new ModelRendererTurbo(this, 1, 70, textureX, textureY); // rightSleeve

		rightArmModel[0].addBox(-7F, 6.5F, -3F, 1, 8, 6, 0F); // elbowArmor

		rightArmModel[1].addBox(-7F, 0.5F, -5F, 3, 5, 10, 0F); // emblemPatch

		rightArmModel[2].addBox(-6.5F, -4.2F, -4.5F, 9, 22, 9, 0F); // rightSleeve
		//(10, -4,0);

	}
}