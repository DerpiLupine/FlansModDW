package com.flansmod.client.model.dwdefence;

import com.flansmod.client.model.ModelCustomArmour;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelTankHead extends ModelCustomArmour
{
	int textureX = 1024;
	int textureY = 1024;

	public ModelTankHead()
	{
		headModel = new ModelRendererTurbo[31];
		headModel[0] = new ModelRendererTurbo(this, 0, 735, textureX, textureY); // Import ImportTurret1
		headModel[1] = new ModelRendererTurbo(this, 0, 820, textureX, textureY); // Import ImportTurret2
		headModel[2] = new ModelRendererTurbo(this, 0, 900, textureX, textureY); // Import ImportTurret3
		headModel[3] = new ModelRendererTurbo(this, 140, 900, textureX, textureY); // Import ImportTurret4
		headModel[4] = new ModelRendererTurbo(this, 200, 170, textureX, textureY); // Import ImportTurret5
		headModel[5] = new ModelRendererTurbo(this, 0, 170, textureX, textureY); // Import ImportTurret6
		headModel[6] = new ModelRendererTurbo(this, 200, 200, textureX, textureY); // Import ImportTurret7
		headModel[7] = new ModelRendererTurbo(this, 200, 200, textureX, textureY); // Import ImportTurret7
		headModel[8] = new ModelRendererTurbo(this, 200, 220, textureX, textureY); // Import ImportTurret8
		headModel[9] = new ModelRendererTurbo(this, 200, 270, textureX, textureY); // Import ImportTurret10
		headModel[10] = new ModelRendererTurbo(this, 200, 270, textureX, textureY); // Import ImportTurret11
		headModel[11] = new ModelRendererTurbo(this, 200, 270, textureX, textureY); // Import ImportTurret12
		headModel[12] = new ModelRendererTurbo(this, 200, 290, textureX, textureY); // Import ImportTurret13
		headModel[13] = new ModelRendererTurbo(this, 200, 340, textureX, textureY); // Import ImportTurret14
		headModel[14] = new ModelRendererTurbo(this, 200, 350, textureX, textureY); // Import ImportTurret15
		headModel[15] = new ModelRendererTurbo(this, 200, 370, textureX, textureY); // Import ImportTurret16
		headModel[16] = new ModelRendererTurbo(this, 200, 390, textureX, textureY); // Import ImportTurret17
		headModel[17] = new ModelRendererTurbo(this, 200, 390, textureX, textureY); // Import ImportTurret18
		headModel[18] = new ModelRendererTurbo(this, 200, 390, textureX, textureY); // Import ImportTurret19
		headModel[19] = new ModelRendererTurbo(this, 200, 390, textureX, textureY); // Import ImportTurret20
		headModel[20] = new ModelRendererTurbo(this, 200, 390, textureX, textureY); // Import ImportTurret21
		headModel[21] = new ModelRendererTurbo(this, 200, 450, textureX, textureY); // Import ImportBarrel1
		headModel[22] = new ModelRendererTurbo(this, 200, 490, textureX, textureY); // Import ImportBarrel2
		headModel[23] = new ModelRendererTurbo(this, 200, 520, textureX, textureY); // Import ImportBarrel3
		headModel[24] = new ModelRendererTurbo(this, 200, 550, textureX, textureY); // Import ImportBarrel4
		headModel[25] = new ModelRendererTurbo(this, 200, 570, textureX, textureY); // Import ImportBarrel5
		headModel[26] = new ModelRendererTurbo(this, 200, 585, textureX, textureY); // Import ImportBarrel6
		headModel[27] = new ModelRendererTurbo(this, 200, 600, textureX, textureY); // Import ImportBarrel7
		headModel[28] = new ModelRendererTurbo(this, 200, 620, textureX, textureY); // Import ImportBarrel8
		headModel[29] = new ModelRendererTurbo(this, 200, 630, textureX, textureY); // Import ImportBarrel9
		headModel[30] = new ModelRendererTurbo(this, 230, 630, textureX, textureY); // Import ImportBarrel10

		headModel[0].addShapeBox(-52F, -14F, -34F, 64, 9, 68, 0F, -16F, 0F, -20F, 0F, 0F, -9F, 0F, 0F, -9F, -16F, 0F, -20F, -16F, 0F, -11F, 0F, 0F, 0F, 0F, 0F, 0F, -16F, 0F, -11F); // Import ImportTurret1
		headModel[0].setRotationPoint(0F, 0F, 0F);

		headModel[1].addShapeBox(-52F, -5F, -34F, 64, 4, 68, 0F, -16F, 0F, -11F, 0F, 0F, 0F, 0F, 0F, 0F, -16F, 0F, -11F, -20F, 0F, -20F, 0F, 0F, -9F, 0F, 0F, -9F, -20F, 0F, -20F); // Import ImportTurret2
		headModel[1].setRotationPoint(0F, 0F, 0F);

		headModel[2].addShapeBox(12F, -14F, -25F, 17, 9, 50, 0F, 0F, 0F, 0F, 0F, 0F, -9F, 0F, 0F, -9F, 0F, 0F, 0F, 0F, 0F, 9F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 9F); // Import ImportTurret3
		headModel[2].setRotationPoint(0F, 0F, 0F);

		headModel[3].addShapeBox(12F, -5F, -25F, 17, 4, 50, 0F, 0F, 0F, 9F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 9F, 0F, 0F, 0F, 0F, 0F, -9F, 0F, 0F, -9F, 0F, 0F, 0F); // Import ImportTurret4
		headModel[3].setRotationPoint(0F, 0F, 0F);

		headModel[4].addShapeBox(29F, -5F, -25F, 8, 4, 13, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -9F, 0F, -3F, -9F, 0F, -3F, 0F, 0F, 0F, 0F); // Import ImportTurret5
		headModel[4].setRotationPoint(0F, 0F, 0F);

		headModel[5].addShapeBox(29F, -5F, 12F, 8, 4, 13, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, -9F, 0F, 0F, -9F); // Import ImportTurret6
		headModel[5].setRotationPoint(0F, 0F, 0F);

		headModel[6].addShapeBox(29F, -9F, 12F, 8, 4, 6, 0F, 0F, 5F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 5F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 7F); // Import ImportTurret7
		headModel[6].setRotationPoint(0F, 0F, 0F);

		headModel[7].addShapeBox(29F, -9F, -18F, 8, 4, 6, 0F, 0F, 5F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 5F, 0F, 0F, 0F, 7F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F); // Import ImportTurret7
		headModel[7].setRotationPoint(0F, 0F, 0F);

		headModel[8].addShapeBox(-45F, -14F, -17F, 9, 7, 34, 0F, 0F, -2F, -4F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, -2F, -4F, 0F, 0F, -4F, 0F, 2F, -4F, 0F, 2F, -4F, 0F, 0F, -4F); // Import ImportTurret8
		headModel[8].setRotationPoint(0F, 0F, 0F);

		headModel[9].addBox(-10F, -14.81F, 10F, 12, 1, 4, 0F); // Import ImportTurret10
		headModel[9].setRotationPoint(0F, 0F, 0F);

		headModel[10].addShapeBox(-10F, -14.81333F, 14F, 12, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F); // Import ImportTurret11
		headModel[10].setRotationPoint(0F, 0F, 0F);

		headModel[11].addShapeBox(-10F, -14.81333F, 6F, 12, 1, 4, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -4F, 0F, 0F, -4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import ImportTurret12
		headModel[11].setRotationPoint(0F, 0F, 0F);

		headModel[12].addBox(-24F, -16.81F, 0F, 8, 3, 8, 0F); // Import ImportTurret13
		headModel[12].setRotationPoint(0F, 0F, 0F);

		headModel[13].addBox(-22F, -19.81333F, 2F, 4, 3, 4, 0F); // Import ImportTurret14
		headModel[13].setRotationPoint(0F, 0F, 0F);

		headModel[14].addBox(-21F, -31.81F, 3F, 2, 12, 2, 0F); // Import ImportTurret15
		headModel[14].setRotationPoint(0F, 0F, 0F);

		headModel[15].addBox(-32F, -16.81F, -14F, 8, 3, 11, 0F); // Import ImportTurret16
		headModel[15].setRotationPoint(0F, 0F, 0F);

		headModel[16].addBox(-30F, -23.81F, -14F, 4, 7, 2, 0F); // Import ImportTurret17
		headModel[16].setRotationPoint(0F, 0F, 0F);

		headModel[17].addBox(-30F, -23.81F, -5F, 4, 7, 2, 0F); // Import ImportTurret18
		headModel[17].setRotationPoint(0F, 0F, 0F);

		headModel[18].addBox(-6.5F, -1F, -22.5F, 15, 3, 45, 0F); // Import ImportTurret19
		headModel[18].setRotationPoint(0F, 0F, 0F);

		headModel[19].addShapeBox(8.5F, -1F, -22.5F, 15, 3, 45, 0F, 0F, 0F, 0F, 0F, 0F, -15F, 0F, 0F, -15F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -15F, 0F, 0F, -15F, 0F, 0F, 0F); // Import ImportTurret20
		headModel[19].setRotationPoint(0F, 0F, 0F);

		headModel[20].addShapeBox(-21.5F, -1F, -22.5F, 15, 3, 45, 0F, 0F, 0F, -15F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -15F, 0F, 0F, -15F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -15F); // Import ImportTurret21
		headModel[20].setRotationPoint(0F, 0F, 0F);

		headModel[21].addShapeBox(29F, -7F, -11F, 8, 6, 22, 0F, 0F, 7F, 1F, 0F, 2F, 1F, 0F, 2F, 1F, 0F, 7F, 1F, 0F, 0F, 1F, 0F, -3F, 1F, 0F, -3F, 1F, 0F, 0F, 1F); // Import ImportBarrel1
		headModel[21].setRotationPoint(0F, 0F, 0F);

		headModel[22].addShapeBox(22F, -13F, -6F, 18, 6, 12, 0F, 0F, 0F, 0F, -7F, 0F, 0F, -7F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import ImportBarrel2
		headModel[22].setRotationPoint(0F, 0F, 0F);

		headModel[23].addShapeBox(22F, -7F, -6F, 18, 5, 12, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -5F, -3F, 0F, -5F, -3F, 0F, 0F, 0F, 0F); // Import ImportBarrel3
		headModel[23].setRotationPoint(0F, 0F, 0F);

		headModel[24].addShapeBox(32F, -12F, -2.5F, 94, 5, 5, 0F, 0F, 0F, 0F, -10F, 0F, 0F, -10F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -10F, 0F, 0F, -10F, 0F, 0F, 0F, 0F, 0F); // Import ImportBarrel4
		headModel[24].setRotationPoint(0F, 0F, 0F);

		headModel[25].addBox(70F, -12.5F, -3F, 16, 6, 6, 0F); // Import ImportBarrel5
		headModel[25].setRotationPoint(0F, 0F, 0F);

		headModel[26].addBox(-30F, -23.31333F, -12F, 4, 3, 7, 0F); // Import ImportBarrel6
		headModel[26].setRotationPoint(0F, 0F, 0F);

		headModel[27].addBox(-34F, -24.81F, -10F, 13, 4, 4, 0F); // Import ImportBarrel7
		headModel[27].setRotationPoint(0F, 0F, 0F);

		headModel[28].addBox(-21F, -24.31333F, -9.5F, 7, 3, 3, 0F); // Import ImportBarrel8
		headModel[28].setRotationPoint(0F, 0F, 0F);

		headModel[29].addBox(-14F, -23.81333F, -9F, 7, 2, 2, 0F); // Import ImportBarrel9
		headModel[29].setRotationPoint(0F, 0F, 0F);

		headModel[30].addBox(-34F, -24.81333F, -6F, 4, 7, 8, 0F); // Import ImportBarrel10
		headModel[30].setRotationPoint(0F, 0F, 0F);
	}
}