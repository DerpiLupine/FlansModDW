package com.flansmod.client.model.dwdefence;

import com.flansmod.client.model.ModelCustomArmour;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelRiotHelmet extends ModelCustomArmour
{
	int textureX = 512;
	int textureY = 256;

	public ModelRiotHelmet()
	{
		headModel = new ModelRendererTurbo[10];
		headModel[0] = new ModelRendererTurbo(this, 1, 179, textureX, textureY); // helmetTop1
		headModel[1] = new ModelRendererTurbo(this, 1, 224, textureX, textureY); // helmetTop3
		headModel[2] = new ModelRendererTurbo(this, 80, 202, textureX, textureY); // helmet
		headModel[3] = new ModelRendererTurbo(this, 80, 182, textureX, textureY); // helmetTop4
		headModel[4] = new ModelRendererTurbo(this, 1, 202, textureX, textureY); // helmetTop2
		headModel[5] = new ModelRendererTurbo(this, 80, 221, textureX, textureY); // Box 18
		headModel[6] = new ModelRendererTurbo(this, 80, 237, textureX, textureY); // Box 19
		headModel[7] = new ModelRendererTurbo(this, 80, 244, textureX, textureY); // Box 21
		headModel[8] = new ModelRendererTurbo(this, 125, 227, textureX, textureY); // Box 22
		headModel[9] = new ModelRendererTurbo(this, 80, 202, textureX, textureY); // Box 23

		headModel[0].addBox(-9.5F, -14F, -10.5F, 19, 2, 20, 0F); // helmetTop1
		//headModel[0].setRotationPoint();

		headModel[1].addShapeBox(-9.5F, -18F, -9.5F, 19, 2, 19, 0F, -1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // helmetTop3
		//headModel[1].setRotationPoint();

		headModel[2].addShapeBox(-9.5F, -12F, 1.5F, 19, 7, 8, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // helmet
		//headModel[2].setRotationPoint();

		headModel[3].addShapeBox(-8.5F, -20F, -8.5F, 17, 2, 17, 0F, -3F, 0F, -3F, -3F, 0F, -3F, -3F, 0F, -3F, -3F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // helmetTop4
		//headModel[3].setRotationPoint();

		headModel[4].addShapeBox(-9.5F, -16F, -9.5F, 19, 2, 19, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F); // helmetTop2
		//headModel[4].setRotationPoint();

		headModel[5].addShapeBox(-9F, -12F, -14.5F, 18, 11, 4, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 18
		//headModel[5].setRotationPoint();

		headModel[6].addShapeBox(-9.5F, -14F, -14.5F, 19, 2, 4, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 19
		//headModel[6].setRotationPoint();

		headModel[7].addBox(-6F, -20F, -10.5F, 12, 3, 4, 0F); // Box 21
		//headModel[7].setRotationPoint();

		headModel[8].addShapeBox(0.5F, -21F, -9.5F, 4, 2, 7, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 22
		//headModel[8].setRotationPoint();

		headModel[9].addShapeBox(-9.5F, -5F, 1.5F, 19, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 23
		//headModel[9].setRotationPoint();
	}
}