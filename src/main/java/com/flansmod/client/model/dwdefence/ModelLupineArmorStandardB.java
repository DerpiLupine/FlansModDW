package com.flansmod.client.model.dwdefence;

import com.flansmod.client.model.ModelCustomArmour;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelLupineArmorStandardB extends ModelCustomArmour
{
	int textureX = 512;
	int textureY = 256;

	public ModelLupineArmorStandardB()
	{
		bodyModel = new ModelRendererTurbo[22];
		bodyModel[0] = new ModelRendererTurbo(this, 58, 31, textureX, textureY); // Box 0
		bodyModel[1] = new ModelRendererTurbo(this, 80, 57, textureX, textureY); // Box 1
		bodyModel[2] = new ModelRendererTurbo(this, 80, 57, textureX, textureY); // Box 2
		bodyModel[3] = new ModelRendererTurbo(this, 58, 44, textureX, textureY); // Box 5
		bodyModel[4] = new ModelRendererTurbo(this, 58, 58, textureX, textureY); // Box 6
		bodyModel[5] = new ModelRendererTurbo(this, 58, 58, textureX, textureY); // Box 7
		bodyModel[6] = new ModelRendererTurbo(this, 86, 51, textureX, textureY); // Box 1
		bodyModel[7] = new ModelRendererTurbo(this, 86, 51, textureX, textureY); // Box 2
		bodyModel[8] = new ModelRendererTurbo(this, 79, 39, textureX, textureY); // Box 3
		bodyModel[9] = new ModelRendererTurbo(this, 79, 31, textureX, textureY); // Box 4
		bodyModel[10] = new ModelRendererTurbo(this, 79, 45, textureX, textureY); // Box 5
		bodyModel[11] = new ModelRendererTurbo(this, 71, 59, textureX, textureY); // Box 0
		bodyModel[12] = new ModelRendererTurbo(this, 71, 59, textureX, textureY); // Box 1
		bodyModel[13] = new ModelRendererTurbo(this, 58, 58, textureX, textureY); // Box 2
		bodyModel[14] = new ModelRendererTurbo(this, 58, 58, textureX, textureY); // Box 3
		bodyModel[15] = new ModelRendererTurbo(this, 86, 51, textureX, textureY); // Box 4
		bodyModel[16] = new ModelRendererTurbo(this, 79, 50, textureX, textureY); // Box 5
		bodyModel[17] = new ModelRendererTurbo(this, 79, 50, textureX, textureY); // Box 6
		bodyModel[18] = new ModelRendererTurbo(this, 79, 50, textureX, textureY); // Box 7
		bodyModel[19] = new ModelRendererTurbo(this, 79, 50, textureX, textureY); // Box 8
		bodyModel[20] = new ModelRendererTurbo(this, 90, 45, textureX, textureY); // Box 9
		bodyModel[21] = new ModelRendererTurbo(this, 79, 45, textureX, textureY); // Box 10

		bodyModel[0].addBox(-4.5F, 1.75F, -2.5F, 9, 11, 1, 0F); // Box 0

		bodyModel[1].addBox(-3.5F, -0.25F, -2.5F, 2, 2, 5, 0F); // Box 1

		bodyModel[2].addBox(1.5F, -0.25F, -2.5F, 2, 2, 5, 0F); // Box 2

		bodyModel[3].addBox(-4.5F, 1.75F, 1.5F, 9, 11, 1, 0F); // Box 5

		bodyModel[4].addShapeBox(-4.5F, 8.25F, -2.5F, 1, 1, 5, 0F,0.1F, 0F, 0.1F,0F, 0F, 0.1F,0F, 0F, 0.1F,0.1F, 0F, 0.1F,0.1F, 0F, 0.1F,0F, 0F, 0.1F,0F, 0F, 0.1F,0.1F, 0F, 0.1F); // Box 6

		bodyModel[5].addShapeBox(3.5F, 8.25F, -2.5F, 1, 1, 5, 0F,0F, 0F, 0.1F,0.1F, 0F, 0.1F,0.1F, 0F, 0.1F,0F, 0F, 0.1F,0F, 0F, 0.1F,0.1F, 0F, 0.1F,0.1F, 0F, 0.1F,0F, 0F, 0.1F); // Box 7

		bodyModel[6].addBox(-1F, 8.25F, -3.5F, 2, 4, 1, 0F); // Box 1

		bodyModel[7].addBox(-3.5F, 8.25F, -3.5F, 2, 4, 1, 0F); // Box 2

		bodyModel[8].addBox(-4F, 4F, -3F, 8, 4, 1, 0F); // Box 3

		bodyModel[9].addBox(-3.5F, 2.75F, 2F, 7, 6, 1, 0F); // Box 4

		bodyModel[10].addBox(0.1F, 9F, 2.5F, 4, 3, 1, 0F); // Box 5

		bodyModel[11].addBox(-4.5F, 9.25F, -1.5F, 1, 2, 3, 0F); // Box 0

		bodyModel[12].addBox(3.5F, 9.25F, -1.5F, 1, 2, 3, 0F); // Box 1

		bodyModel[13].addShapeBox(3.5F, 11.25F, -2.5F, 1, 1, 5, 0F,0F, 0F, 0.1F,0.1F, 0F, 0.1F,0.1F, 0F, 0.1F,0F, 0F, 0.1F,0F, 0F, 0.1F,0.1F, 0F, 0.1F,0.1F, 0F, 0.1F,0F, 0F, 0.1F); // Box 2

		bodyModel[14].addShapeBox(-4.5F, 11.25F, -2.5F, 1, 1, 5, 0F,0.1F, 0F, 0.1F,0F, 0F, 0.1F,0F, 0F, 0.1F,0.1F, 0F, 0.1F,0.1F, 0F, 0.1F,0F, 0F, 0.1F,0F, 0F, 0.1F,0.1F, 0F, 0.1F); // Box 3

		bodyModel[15].addBox(1.5F, 8.25F, -3.5F, 2, 4, 1, 0F); // Box 4

		bodyModel[16].addBox(-5.5F, 8.5F, 0F, 1, 4, 2, 0F); // Box 5

		bodyModel[17].addBox(-5.5F, 8.5F, -2.25F, 1, 4, 2, 0F); // Box 6

		bodyModel[18].addBox(4.5F, 8.5F, 0F, 1, 4, 2, 0F); // Box 7

		bodyModel[19].addBox(4.5F, 8.5F, -2.25F, 1, 4, 2, 0F); // Box 8

		bodyModel[20].addBox(1F, 2.5F, -3F, 3, 1, 1, 0F); // Box 9

		bodyModel[21].addBox(-4.1F, 9F, 2.5F, 4, 3, 1, 0F); // Box 10
	}
}