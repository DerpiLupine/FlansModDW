package com.flansmod.client.model.dwdefence;

import com.flansmod.client.model.ModelCustomArmour;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.common.vector.Vector3f;

public class ModelAssaultBody extends ModelCustomArmour
{
	int textureX = 512;
	int textureY = 256;

	public ModelAssaultBody()
	{
		bodyModel = new ModelRendererTurbo[20];
		bodyModel[0] = new ModelRendererTurbo(this, 33, 135, textureX, textureY); // armor1
		bodyModel[1] = new ModelRendererTurbo(this, 60, 137, textureX, textureY); // armor2
		bodyModel[2] = new ModelRendererTurbo(this, 60, 137, textureX, textureY); // armor2
		bodyModel[3] = new ModelRendererTurbo(this, 60, 137, textureX, textureY); // armor2
		bodyModel[4] = new ModelRendererTurbo(this, 60, 137, textureX, textureY); // armor2
		bodyModel[5] = new ModelRendererTurbo(this, 50, 102, textureX, textureY); // backArmorPlate
		bodyModel[6] = new ModelRendererTurbo(this, 1, 102, textureX, textureY); // body1
		bodyModel[7] = new ModelRendererTurbo(this, 50, 118, textureX, textureY); // frontArmorPlate
		bodyModel[8] = new ModelRendererTurbo(this, 50, 130, textureX, textureY); // namePatch
		bodyModel[9] = new ModelRendererTurbo(this, 1, 135, textureX, textureY); // pocket1
		bodyModel[10] = new ModelRendererTurbo(this, 1, 135, textureX, textureY); // pocket1
		bodyModel[11] = new ModelRendererTurbo(this, 1, 135, textureX, textureY); // pocket1
		bodyModel[12] = new ModelRendererTurbo(this, 1, 146, textureX, textureY); // pocket2
		bodyModel[13] = new ModelRendererTurbo(this, 1, 146, textureX, textureY); // pocket2
		bodyModel[14] = new ModelRendererTurbo(this, 1, 146, textureX, textureY); // pocket2
		bodyModel[15] = new ModelRendererTurbo(this, 14, 135, textureX, textureY); // pocket3
		bodyModel[16] = new ModelRendererTurbo(this, 14, 135, textureX, textureY); // pocket3
		bodyModel[17] = new ModelRendererTurbo(this, 14, 144, textureX, textureY); // pocket4
		bodyModel[18] = new ModelRendererTurbo(this, 14, 144, textureX, textureY); // pocket4
		bodyModel[19] = new ModelRendererTurbo(this, 33, 135, textureX, textureY); // armor1

		bodyModel[0].addShapeBox(-6.5F, -0.25F, -4.5F, 4, 4, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F); // armor1
		bodyModel[0].setRotationPoint(0F, 0F, 0F);

		bodyModel[1].addShapeBox(-8.5F, 21.25F, -4.5F, 2, 2, 9, 0F, 0.1F, 0F, 0.1F, 0F, 0F, 0.1F, 0F, 0F, 0.1F, 0.1F, 0F, 0.1F, 0.1F, 0F, 0.1F, 0F, 0F, 0.1F, 0F, 0F, 0.1F, 0.1F, 0F, 0.1F); // armor2
		bodyModel[1].setRotationPoint(0F, 0F, 0F);

		bodyModel[2].addShapeBox(-8.5F, 16.25F, -4.5F, 2, 2, 9, 0F, 0.1F, 0F, 0.1F, 0F, 0F, 0.1F, 0F, 0F, 0.1F, 0.1F, 0F, 0.1F, 0.1F, 0F, 0.1F, 0F, 0F, 0.1F, 0F, 0F, 0.1F, 0.1F, 0F, 0.1F); // armor2
		bodyModel[2].setRotationPoint(0F, 0F, 0F);

		bodyModel[3].addShapeBox(6.5F, 21.25F, -4.5F, 2, 2, 9, 0F, 0.1F, 0F, 0.1F, 0F, 0F, 0.1F, 0F, 0F, 0.1F, 0.1F, 0F, 0.1F, 0.1F, 0F, 0.1F, 0F, 0F, 0.1F, 0F, 0F, 0.1F, 0.1F, 0F, 0.1F); // armor2
		bodyModel[3].setRotationPoint(0F, 0F, 0F);

		bodyModel[4].addShapeBox(6.5F, 16.25F, -4.5F, 2, 2, 9, 0F, 0.1F, 0F, 0.1F, 0F, 0F, 0.1F, 0F, 0F, 0.1F, 0.1F, 0F, 0.1F, 0.1F, 0F, 0.1F, 0F, 0F, 0.1F, 0F, 0F, 0.1F, 0.1F, 0F, 0.1F); // armor2
		bodyModel[4].setRotationPoint(0F, 0F, 0F);

		bodyModel[5].addBox(-7F, 2.75F, 4F, 14, 14, 1, 0F); // backArmorPlate
		bodyModel[5].setRotationPoint(0F, 0F, 0F);

		bodyModel[6].addShapeBox(-8.1F, -0.1F, -4.1F, 16, 24, 8, 0F, 0F, 0F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0.2F, 0F, 0.2F, 0.2F, 0F, 0.2F, 0.2F, 0.2F, 0F, 0.2F, 0.2F); // body1
		bodyModel[6].setRotationPoint(0F, 0F, 0F);

		bodyModel[7].addBox(-7F, 6.5F, -5F, 14, 9, 1, 0F); // frontArmorPlate
		bodyModel[7].setRotationPoint(0F, 0F, 0F);

		bodyModel[8].addShapeBox(1F, 4F, -4.6F, 12, 3, 1, 0F, 0F, 0F, 0F, -6F, 0F, 0F, -6F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -6F, -1F, 0F, -6F, -1F, 0F, 0F, -1F, 0F); // namePatch
		bodyModel[8].setRotationPoint(0F, 0F, 0F);

		bodyModel[9].addBox(3F, 16F, -6F, 4, 8, 2, 0F); // pocket1
		bodyModel[9].setRotationPoint(0F, 0F, 0F);

		bodyModel[10].addShapeBox(-7F, 16F, -6F, 4, 8, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // pocket1
		bodyModel[10].setRotationPoint(0F, 0F, 0F);

		bodyModel[11].addBox(-2F, 16F, -6F, 4, 8, 2, 0F); // pocket1
		bodyModel[11].setRotationPoint(0F, 0F, 0F);

		bodyModel[12].addShapeBox(-2F, 16F, -7F, 4, 3, 1, 0F, 0F, -0.2F, -0.5F, 0F, -0.2F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // pocket2
		bodyModel[12].setRotationPoint(0F, 0F, 0F);

		bodyModel[13].addShapeBox(-7F, 16F, -7F, 4, 3, 1, 0F, 0F, -0.2F, -0.5F, 0F, -0.2F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // pocket2
		bodyModel[13].setRotationPoint(0F, 0F, 0F);

		bodyModel[14].addShapeBox(3F, 16F, -7F, 4, 3, 1, 0F, 0F, -0.2F, -0.5F, 0F, -0.2F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // pocket2
		bodyModel[14].setRotationPoint(0F, 0F, 0F);

		bodyModel[15].addBox(0.5F, 18F, 4F, 7, 6, 2, 0F); // pocket3
		bodyModel[15].setRotationPoint(0F, 0F, 0F);

		bodyModel[16].addBox(-7.5F, 18F, 4F, 7, 6, 2, 0F); // pocket3
		bodyModel[16].setRotationPoint(0F, 0F, 0F);

		bodyModel[17].addShapeBox(-7.5F, 18F, 6F, 7, 3, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.2F, -0.5F, 0F, -0.2F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // pocket4
		bodyModel[17].setRotationPoint(0F, 0F, 0F);

		bodyModel[18].addShapeBox(0.5F, 18F, 6F, 7, 3, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.2F, -0.5F, 0F, -0.2F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // pocket4
		bodyModel[18].setRotationPoint(0F, 0F, 0F);

		bodyModel[19].addShapeBox(2.5F, -0.25F, -4.5F, 4, 4, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F); // armor1
		bodyModel[19].setRotationPoint(0F, 0F, 0F);


		leftArmModel = new ModelRendererTurbo[3];
		leftArmModel[0] = new ModelRendererTurbo(this, 1, 149, textureX, textureY); // emblemPatch
		leftArmModel[1] = new ModelRendererTurbo(this, 114, 102, textureX, textureY); // leftArm
		leftArmModel[2] = new ModelRendererTurbo(this, 114, 131, textureX, textureY); // leftSleeve

		leftArmModel[0].addShapeBox(5.2F, -2.5F, -4F, 1, 6, 14, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -6F, 0F, 0F, -6F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, -6F, 0F, -3F, -6F); // emblemPatch
		leftArmModel[0].setRotationPoint(0F, 0F, 0F);

		leftArmModel[1].addShapeBox(-2.1F, -4.1F, -4.1F, 8, 20, 8, 0F, 0F, 0F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0.2F, 0F, 0.2F, 0.2F, 0F, 0.2F, 0.2F, 0.2F, 0F, 0.2F, 0.2F); // leftArm
		leftArmModel[1].setRotationPoint(0F, 0F, 0F);

		leftArmModel[2].addShapeBox(-2.2F, 15.2F, -4.2F, 8, 2, 8, 0F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F); // leftSleeve
		leftArmModel[2].setRotationPoint(0F, 0F, 0F);


		rightArmModel = new ModelRendererTurbo[3];
		rightArmModel[0] = new ModelRendererTurbo(this, 81, 102, textureX, textureY); // rightArm
		rightArmModel[1] = new ModelRendererTurbo(this, 81, 131, textureX, textureY); // rightSleeve
		rightArmModel[2] = new ModelRendererTurbo(this, 1, 149, textureX, textureY); // Box 1

		rightArmModel[0].addShapeBox(-6.1F, -4.1F, -4.1F, 8, 20, 8, 0F, 0F, 0F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0.2F, 0F, 0.2F, 0.2F, 0F, 0.2F, 0.2F, 0.2F, 0F, 0.2F, 0.2F); // rightArm
		rightArmModel[0].setRotationPoint(0F, 0F, 0F);

		rightArmModel[1].addShapeBox(-6.2F, 15.2F, -4.2F, 8, 2, 8, 0F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F); // rightSleeve
		rightArmModel[1].setRotationPoint(0F, 0F, 0F);

		rightArmModel[2].addShapeBox(-6.2F, -2.5F, -4F, 1, 6, 14, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -6F, 0F, 0F, -6F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, -6F, 0F, -3F, -6F); // Box 1
		rightArmModel[2].setRotationPoint(0F, 0F, 0F);
	}
}