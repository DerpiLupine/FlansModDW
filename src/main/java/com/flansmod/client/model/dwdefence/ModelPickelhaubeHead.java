package com.flansmod.client.model.dwdefence; //Path where the model is located

import com.flansmod.client.model.ModelCustomArmour;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelPickelhaubeHead extends ModelCustomArmour //Same as Filename
{
	int textureX = 512;
	int textureY = 256;

	public ModelPickelhaubeHead() //Same as Filename
	{
		headModel = new ModelRendererTurbo[6];
		headModel[0] = new ModelRendererTurbo(this, 34, 7, textureX, textureY); // Import Box2
		headModel[1] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Import Box5
		headModel[2] = new ModelRendererTurbo(this, 34, 1, textureX, textureY); // Import Box3
		headModel[3] = new ModelRendererTurbo(this, 1, 11, textureX, textureY); // Import Box4
		headModel[4] = new ModelRendererTurbo(this, 34, 10, textureX, textureY); // Import Box7
		headModel[5] = new ModelRendererTurbo(this, 34, 16, textureX, textureY); // Import Box8

		headModel[0].addShapeBox(-4F, -5F, -5F, 8, 1, 1, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, -1F, 0F, 1F, -1F, 0F, 1F, -1F, -1F, 0F, -1F, -1F, 0F); // Import Box2
		headModel[0].setRotationPoint(0F, 0F, 0F);

		headModel[1].addShapeBox(-4F, -9F, -4F, 8, 1, 8, 0F, -1F, 0F, -2F, -1F, 0F, -2F, -1F, 0F, -2F, -1F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import Box5
		headModel[1].setRotationPoint(0F, 0F, 0F);

		headModel[2].addShapeBox(-3F, -10F, -2F, 6, 1, 4, 0F, -2F, 0F, -1.9F, -2F, 0F, -1.9F, -2F, 0F, -1.9F, -2F, 0F, -1.9F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import Box3
		headModel[2].setRotationPoint(0F, 0F, 0F);

		headModel[3].addShapeBox(-4F, -7F, -4F, 8, 2, 8, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import Box4
		headModel[3].setRotationPoint(0F, 0F, 0F);

		headModel[4].addShapeBox(-1F, -12F, -1F, 2, 3, 2, 0F, -0.95F, 1F, -0.95F, -0.95F, 1F, -0.95F, -0.95F, 1F, -0.95F, -0.95F, 1F, -0.95F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import Box7
		headModel[4].setRotationPoint(0F, 0F, 0F);

		headModel[5].addShapeBox(-4F, -8F, 4F, 8, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 1F, 0F, 0F, 1F); // Import Box8
		headModel[5].setRotationPoint(0F, 0F, 0F);
	}
}