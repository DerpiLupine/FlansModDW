package com.flansmod.client.model.dwdefence; //Path where the model is located

import com.flansmod.client.model.ModelCustomArmour;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelLMSSBody extends ModelCustomArmour //Same as Filename
{
	int textureX = 512;
	int textureY = 256;

	public ModelLMSSBody() //Same as Filename
	{
		bodyModel = new ModelRendererTurbo[29];
		bodyModel[0] = new ModelRendererTurbo(this, 49, 158, textureX, textureY); // armor1
		bodyModel[1] = new ModelRendererTurbo(this, 50, 109, textureX, textureY); // backArmorPlate
		bodyModel[2] = new ModelRendererTurbo(this, 1, 109, textureX, textureY); // body1
		bodyModel[3] = new ModelRendererTurbo(this, 91, 169, textureX, textureY); // frontArmorPlate
		bodyModel[4] = new ModelRendererTurbo(this, 49, 172, textureX, textureY); // namePatch
		bodyModel[5] = new ModelRendererTurbo(this, 1, 142, textureX, textureY); // pocket1
		bodyModel[6] = new ModelRendererTurbo(this, 1, 142, textureX, textureY); // pocket1
		bodyModel[7] = new ModelRendererTurbo(this, 1, 142, textureX, textureY); // pocket1
		bodyModel[8] = new ModelRendererTurbo(this, 1, 153, textureX, textureY); // pocket2
		bodyModel[9] = new ModelRendererTurbo(this, 1, 153, textureX, textureY); // pocket2
		bodyModel[10] = new ModelRendererTurbo(this, 1, 153, textureX, textureY); // pocket2
		bodyModel[11] = new ModelRendererTurbo(this, 31, 142, textureX, textureY); // pocket3
		bodyModel[12] = new ModelRendererTurbo(this, 31, 142, textureX, textureY); // pocket3
		bodyModel[13] = new ModelRendererTurbo(this, 31, 151, textureX, textureY); // pocket4
		bodyModel[14] = new ModelRendererTurbo(this, 31, 151, textureX, textureY); // pocket4
		bodyModel[15] = new ModelRendererTurbo(this, 49, 158, textureX, textureY); // armor1
		bodyModel[16] = new ModelRendererTurbo(this, 91, 148, textureX, textureY); // Box 0
		bodyModel[17] = new ModelRendererTurbo(this, 50, 128, textureX, textureY); // Box 1
		bodyModel[18] = new ModelRendererTurbo(this, 23, 142, textureX, textureY); // Box 2
		bodyModel[19] = new ModelRendererTurbo(this, 14, 142, textureX, textureY); // Box 3
		bodyModel[20] = new ModelRendererTurbo(this, 14, 142, textureX, textureY); // Box 4
		bodyModel[21] = new ModelRendererTurbo(this, 23, 142, textureX, textureY); // Box 5
		bodyModel[22] = new ModelRendererTurbo(this, 63, 139, textureX, textureY); // Box 6
		bodyModel[23] = new ModelRendererTurbo(this, 50, 139, textureX, textureY); // Box 7
		bodyModel[24] = new ModelRendererTurbo(this, 63, 139, textureX, textureY); // Box 8
		bodyModel[25] = new ModelRendererTurbo(this, 50, 139, textureX, textureY); // Box 9
		bodyModel[26] = new ModelRendererTurbo(this, 81, 128, textureX, textureY); // Box 10
		bodyModel[27] = new ModelRendererTurbo(this, 91, 159, textureX, textureY); // Box 11
		bodyModel[28] = new ModelRendererTurbo(this, 50, 150, textureX, textureY); // Box 16

		bodyModel[0].addShapeBox(-6.5F, -0.25F, -4.5F, 4, 4, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F); // armor1
		bodyModel[0].setRotationPoint(0F, 0F, 0F);

		bodyModel[1].addBox(-7F, 6.75F, 4F, 14, 12, 6, 0F); // backArmorPlate
		bodyModel[1].setRotationPoint(0F, 0F, 0F);

		bodyModel[2].addShapeBox(-8.1F, -0.1F, -4.1F, 16, 24, 8, 0F, 0F, 0F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0.2F, 0F, 0.2F, 0.2F, 0F, 0.2F, 0.2F, 0.2F, 0F, 0.2F, 0.2F); // body1
		bodyModel[2].setRotationPoint(0F, 0F, 0F);

		bodyModel[3].addBox(-7F, 3.5F, -5F, 14, 12, 1, 0F); // frontArmorPlate
		bodyModel[3].setRotationPoint(0F, 0F, 0F);

		bodyModel[4].addShapeBox(0.5F, 4F, -5.2F, 12, 3, 1, 0F, 0F, 0F, 0F, -6F, 0F, 0F, -6F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -6F, -1F, 0F, -6F, -1F, 0F, 0F, -1F, 0F); // namePatch
		bodyModel[4].setRotationPoint(0F, 0F, 0F);

		bodyModel[5].addBox(3F, 16F, -6F, 4, 8, 2, 0F); // pocket1
		bodyModel[5].setRotationPoint(0F, 0F, 0F);

		bodyModel[6].addShapeBox(-7F, 16F, -6F, 4, 8, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // pocket1
		bodyModel[6].setRotationPoint(0F, 0F, 0F);

		bodyModel[7].addBox(-2F, 16F, -6F, 4, 8, 2, 0F); // pocket1
		bodyModel[7].setRotationPoint(0F, 0F, 0F);

		bodyModel[8].addShapeBox(-2F, 16F, -7F, 4, 3, 1, 0F, 0F, -0.2F, -0.5F, 0F, -0.2F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // pocket2
		bodyModel[8].setRotationPoint(0F, 0F, 0F);

		bodyModel[9].addShapeBox(-7F, 16F, -7F, 4, 3, 1, 0F, 0F, -0.2F, -0.5F, 0F, -0.2F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // pocket2
		bodyModel[9].setRotationPoint(0F, 0F, 0F);

		bodyModel[10].addShapeBox(3F, 16F, -7F, 4, 3, 1, 0F, 0F, -0.2F, -0.5F, 0F, -0.2F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // pocket2
		bodyModel[10].setRotationPoint(0F, 0F, 0F);

		bodyModel[11].addBox(0.5F, 20F, 4F, 7, 6, 2, 0F); // pocket3
		bodyModel[11].setRotationPoint(0F, 0F, 0F);

		bodyModel[12].addBox(-7.5F, 20F, 4F, 7, 6, 2, 0F); // pocket3
		bodyModel[12].setRotationPoint(0F, 0F, 0F);

		bodyModel[13].addShapeBox(-7.5F, 20F, 6F, 7, 3, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.2F, -0.5F, 0F, -0.2F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // pocket4
		bodyModel[13].setRotationPoint(0F, 0F, 0F);

		bodyModel[14].addShapeBox(0.5F, 20F, 6F, 7, 3, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.2F, -0.5F, 0F, -0.2F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // pocket4
		bodyModel[14].setRotationPoint(0F, 0F, 0F);

		bodyModel[15].addShapeBox(2.5F, -0.25F, -4.5F, 4, 4, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F); // armor1
		bodyModel[15].setRotationPoint(0F, 0F, 0F);

		bodyModel[16].addShapeBox(-8.5F, -1.25F, -4.5F, 17, 1, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F); // Box 0
		bodyModel[16].setRotationPoint(0F, 0F, 0F);

		bodyModel[17].addShapeBox(-6F, 2.75F, 4F, 12, 4, 6, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 1
		bodyModel[17].setRotationPoint(0F, 0F, 0F);

		bodyModel[18].addShapeBox(5.5F, 9F, -7F, 2, 2, 1, 0F, 0F, -0.2F, -0.5F, 0F, -0.2F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 2
		bodyModel[18].setRotationPoint(0F, 0F, 0F);

		bodyModel[19].addBox(5.5F, 9F, -6F, 2, 6, 2, 0F); // Box 3
		bodyModel[19].setRotationPoint(0F, 0F, 0F);

		bodyModel[20].addBox(3F, 9F, -6F, 2, 6, 2, 0F); // Box 4
		bodyModel[20].setRotationPoint(0F, 0F, 0F);

		bodyModel[21].addShapeBox(3F, 9F, -7F, 2, 2, 1, 0F, 0F, -0.2F, -0.5F, 0F, -0.2F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 5
		bodyModel[21].setRotationPoint(0F, 0F, 0F);

		bodyModel[22].addShapeBox(-6.5F, 12F, 12F, 4, 3, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.2F, -0.5F, 0F, -0.2F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 6
		bodyModel[22].setRotationPoint(0F, 0F, 0F);

		bodyModel[23].addBox(-6.5F, 12F, 10F, 4, 8, 2, 0F); // Box 7
		bodyModel[23].setRotationPoint(0F, 0F, 0F);

		bodyModel[24].addShapeBox(-2F, 12F, 12F, 4, 3, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.2F, -0.5F, 0F, -0.2F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 8
		bodyModel[24].setRotationPoint(0F, 0F, 0F);

		bodyModel[25].addBox(-2F, 12F, 10F, 4, 8, 2, 0F); // Box 9
		bodyModel[25].setRotationPoint(0F, 0F, 0F);

		bodyModel[26].addShapeBox(-1F, 8F, 10F, 2, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.2F, -0.5F, 0F, -0.2F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 10
		bodyModel[26].setRotationPoint(0F, 0F, 0F);

		bodyModel[27].addShapeBox(-8F, -0.25F, 4F, 16, 4, 5, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -3F, -1.5F, 0F, -3F, -1.5F, 0F); // Box 11
		bodyModel[27].setRotationPoint(0F, 0F, 0F);

		bodyModel[28].addShapeBox(-7F, 18.75F, 4F, 14, 1, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F); // Box 16
		bodyModel[28].setRotationPoint(0F, 0F, 0F);


		leftArmModel = new ModelRendererTurbo[7];
		leftArmModel[0] = new ModelRendererTurbo(this, 1, 179, textureX, textureY); // emblemPatch
		leftArmModel[1] = new ModelRendererTurbo(this, 124, 108, textureX, textureY); // leftArm
		leftArmModel[2] = new ModelRendererTurbo(this, 124, 132, textureX, textureY); // leftSleeve
		leftArmModel[3] = new ModelRendererTurbo(this, 1, 158, textureX, textureY); // Box 12
		leftArmModel[4] = new ModelRendererTurbo(this, 157, 108, textureX, textureY); // Box 14
		leftArmModel[5] = new ModelRendererTurbo(this, 157, 114, textureX, textureY); // Box 15
		leftArmModel[6] = new ModelRendererTurbo(this, 122, 169, textureX, textureY); // Box 40

		leftArmModel[0].addShapeBox(5.2F, -2.5F, -4F, 1, 6, 14, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -6F, 0F, 0F, -6F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, -6F, 0F, -3F, -6F); // emblemPatch
		leftArmModel[0].setRotationPoint(0F, 0F, 0F);

		leftArmModel[1].addShapeBox(-2.1F, -4.1F, -4.1F, 8, 15, 8, 0F, 0F, 0F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0.2F, 0F, 0.2F, 0.2F, 0F, 0.2F, 0.2F, 0.2F, 0F, 0.2F, 0.2F); // leftArm
		leftArmModel[1].setRotationPoint(0F, 0F, 0F);

		leftArmModel[2].addShapeBox(-2.2F, 10.2F, -4.2F, 8, 7, 8, 0F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F); // leftSleeve
		leftArmModel[2].setRotationPoint(0F, 0F, 0F);

		leftArmModel[3].addShapeBox(5.2F, 1.5F, -4F, 1, 6, 14, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -6F, 0F, 0F, -6F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, -6F, 0F, -3F, -6F); // Box 12
		leftArmModel[3].setRotationPoint(0F, 0F, 0F);

		leftArmModel[4].addBox(-1.5F, 10.5F, -4.5F, 5, 4, 1, 0F); // Box 14
		leftArmModel[4].setRotationPoint(0F, 0F, 0F);

		leftArmModel[5].addShapeBox(-1.5F, 14.8F, -4.5F, 5, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F); // Box 15
		leftArmModel[5].setRotationPoint(0F, 0F, 0F);

		leftArmModel[6].addShapeBox(-2.2F, 17.2F, -4.2F, 8, 3, 8, 0F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F); // Box 40
		leftArmModel[6].setRotationPoint(0F, 0F, 0F);


		rightArmModel = new ModelRendererTurbo[5];
		rightArmModel[0] = new ModelRendererTurbo(this, 91, 108, textureX, textureY); // rightArm
		rightArmModel[1] = new ModelRendererTurbo(this, 91, 137, textureX, textureY); // rightSleeve
		rightArmModel[2] = new ModelRendererTurbo(this, 1, 179, textureX, textureY); // Box 1
		rightArmModel[3] = new ModelRendererTurbo(this, 1, 158, textureX, textureY); // Box 13
		rightArmModel[4] = new ModelRendererTurbo(this, 122, 169, textureX, textureY); // Box 39

		rightArmModel[0].addShapeBox(-6.1F, -4.1F, -4.1F, 8, 20, 8, 0F, 0F, 0F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0.2F, 0F, 0.2F, 0.2F, 0F, 0.2F, 0.2F, 0.2F, 0F, 0.2F, 0.2F); // rightArm
		rightArmModel[0].setRotationPoint(0F, 0F, 0F);

		rightArmModel[1].addShapeBox(-6.2F, 15.2F, -4.2F, 8, 2, 8, 0F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F); // rightSleeve
		rightArmModel[1].setRotationPoint(0F, 0F, 0F);

		rightArmModel[2].addShapeBox(-6.2F, -2.5F, -4F, 1, 6, 14, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -6F, 0F, 0F, -6F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, -6F, 0F, -3F, -6F); // Box 1
		rightArmModel[2].setRotationPoint(0F, 0F, 0F);

		rightArmModel[3].addShapeBox(-6.2F, 1.5F, -4F, 1, 6, 14, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -6F, 0F, 0F, -6F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, -6F, 0F, -3F, -6F); // Box 13
		rightArmModel[3].setRotationPoint(0F, 0F, 0F);

		rightArmModel[4].addShapeBox(-6.2F, 17.2F, -4.2F, 8, 3, 8, 0F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F); // Box 39
		rightArmModel[4].setRotationPoint(0F, 0F, 0F);


	}
}