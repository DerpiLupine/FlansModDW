package com.flansmod.client.model.dwdefence;

import com.flansmod.client.model.ModelCustomArmour;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelLupineArmorTactical extends ModelCustomArmour
{
	int textureX = 512;
	int textureY = 256;

	public ModelLupineArmorTactical()
	{
		bodyModel = new ModelRendererTurbo[23];
		bodyModel[0] = new ModelRendererTurbo(this, 1, 64, textureX, textureY); // Box 0
		bodyModel[1] = new ModelRendererTurbo(this, 22, 74, textureX, textureY); // Box 2
		bodyModel[2] = new ModelRendererTurbo(this, 22, 64, textureX, textureY); // Box 3
		bodyModel[3] = new ModelRendererTurbo(this, 22, 69, textureX, textureY); // Box 4
		bodyModel[4] = new ModelRendererTurbo(this, 1, 75, textureX, textureY); // Box 5
		bodyModel[5] = new ModelRendererTurbo(this, 24, 85, textureX, textureY); // Box 1
		bodyModel[6] = new ModelRendererTurbo(this, 24, 85, textureX, textureY); // Box 2
		bodyModel[7] = new ModelRendererTurbo(this, 27, 81, textureX, textureY); // Box 3
		bodyModel[8] = new ModelRendererTurbo(this, 1, 85, textureX, textureY); // Box 4
		bodyModel[9] = new ModelRendererTurbo(this, 31, 91, textureX, textureY); // Box 5
		bodyModel[10] = new ModelRendererTurbo(this, 39, 64, textureX, textureY); // Box 0
		bodyModel[11] = new ModelRendererTurbo(this, 39, 64, textureX, textureY); // Box 1
		bodyModel[12] = new ModelRendererTurbo(this, 24, 85, textureX, textureY); // Box 2
		bodyModel[13] = new ModelRendererTurbo(this, 22, 81, textureX, textureY); // Box 4
		bodyModel[14] = new ModelRendererTurbo(this, 22, 81, textureX, textureY); // Box 5
		bodyModel[15] = new ModelRendererTurbo(this, 24, 91, textureX, textureY); // backPocket1-2
		bodyModel[16] = new ModelRendererTurbo(this, 24, 91, textureX, textureY); // backPocket1
		bodyModel[17] = new ModelRendererTurbo(this, 1, 91, textureX, textureY); // Box 8
		bodyModel[18] = new ModelRendererTurbo(this, 31, 91, textureX, textureY); // Box 9
		bodyModel[19] = new ModelRendererTurbo(this, 31, 85, textureX, textureY); // Box 10
		bodyModel[20] = new ModelRendererTurbo(this, 22, 74, textureX, textureY); // Box 11
		bodyModel[21] = new ModelRendererTurbo(this, 44, 85, textureX, textureY); // Box 13
		bodyModel[22] = new ModelRendererTurbo(this, 44, 90, textureX, textureY); // Box 14

		bodyModel[0].addBox(-4.5F, 3.75F, -2.5F, 9, 8, 1, 0F); // Box 0

		bodyModel[1].addBox(-4.5F, -0.25F, -2.5F, 3, 1, 5, 0F); // Box 2

		bodyModel[2].addBox(-3.5F, 0.75F, -2.5F, 7, 3, 1, 0F); // Box 3

		bodyModel[3].addBox(-3.5F, 0.75F, 1.5F, 7, 3, 1, 0F); // Box 4

		bodyModel[4].addBox(-4.5F, 3.75F, 1.5F, 9, 8, 1, 0F); // Box 5

		bodyModel[5].addBox(-1F, 8F, -3.5F, 2, 4, 1, 0F); // Box 1

		bodyModel[6].addBox(-3.25F, 8F, -3.5F, 2, 4, 1, 0F); // Box 2

		bodyModel[7].addBox(1F, 4.25F, -2.75F, 3, 1, 1, 0F); // Box 3

		bodyModel[8].addBox(-3F, 1.8F, 2F, 6, 2, 3, 0F); // Box 4

		bodyModel[9].addBox(0.5F, 11F, 2.5F, 4, 3, 1, 0F); // Box 5

		bodyModel[10].addBox(-4.5F, 7.75F, -1.5F, 1, 4, 3, 0F); // Box 0

		bodyModel[11].addBox(3.5F, 7.75F, -1.5F, 1, 4, 3, 0F); // Box 1

		bodyModel[12].addBox(1.25F, 8F, -3.5F, 2, 4, 1, 0F); // Box 2

		bodyModel[13].addBox(3.25F, 5.75F, -3.5F, 1, 2, 1, 0F); // Box 4

		bodyModel[14].addBox(2F, 5.75F, -3.5F, 1, 2, 1, 0F); // Box 5

		bodyModel[15].addBox(-3.5F, 7.5F, 4.5F, 2, 4, 1, 0F); // backPocket1-2

		bodyModel[16].addBox(-1.25F, 7.5F, 4.5F, 2, 4, 1, 0F); // backPocket1

		bodyModel[17].addBox(-4F, 3.8F, 2F, 8, 7, 3, 0F); // Box 8

		bodyModel[18].addBox(-4.5F, 11F, 2.5F, 4, 3, 1, 0F); // Box 9

		bodyModel[19].addBox(-4.25F, 4F, -3F, 5, 3, 1, 0F); // Box 10

		bodyModel[20].addBox(1.5F, -0.25F, -2.5F, 3, 1, 5, 0F); // Box 11

		bodyModel[21].addBox(-3F, 0F, -2.75F, 6, 3, 1, 0F); // Box 13

		bodyModel[22].addBox(-3F, 0F, 1.75F, 6, 1, 1, 0F); // Box 14
	}
}