package com.flansmod.client.model.dwdefence;

import com.flansmod.client.model.ModelCustomArmour;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.common.vector.Vector3f;

public class ModelWingbreakerPonyBoots extends ModelCustomArmour
{
	public ModelWingbreakerPonyBoots()
	{
		int textureX = 256;
		int textureY = 128;

		leftLegModel = new ModelRendererTurbo[4];
		leftLegModel[0] = new ModelRendererTurbo(this, 32, 88, textureX, textureY); // leftBootPart1
		leftLegModel[1] = new ModelRendererTurbo(this, 1, 88, textureX, textureY); // leftBootPart2
		leftLegModel[2] = new ModelRendererTurbo(this, 1, 109, textureX, textureY); // leftBootPart3
		leftLegModel[3] = new ModelRendererTurbo(this, 1, 97, textureX, textureY); // leftBootPart4

		leftLegModel[0].addBox(-4.5F, 14.5F, -4.5F, 9, 8, 9, 0F); // leftBootPart1
		// leftLegModel[0].setRotationPoint(-0.5F, 34.5F, -4.5F);

		leftLegModel[1].addBox(-4.5F, 17.5F, -7.5F, 9, 5, 3, 0F); // leftBootPart2
		// leftLegModel[1].setRotationPoint(-0.5F, 37.5F, -7.5F);

		leftLegModel[2].addBox(-5F, 22.5F, -8F, 10, 2, 13, 0F); // leftBootPart3
		// leftLegModel[2].setRotationPoint(-1F, 42.5F, -8F);

		leftLegModel[3].addBox(-5F, 13.5F, -5F, 10, 1, 10, 0F); // leftBootPart4
		// leftLegModel[3].setRotationPoint(-1F, 33.5F, -5F);


		rightLegModel = new ModelRendererTurbo[4];
		rightLegModel[0] = new ModelRendererTurbo(this, 32, 88, textureX, textureY); // rightBootPart1
		rightLegModel[1] = new ModelRendererTurbo(this, 1, 97, textureX, textureY); // rightBootPart2
		rightLegModel[2] = new ModelRendererTurbo(this, 1, 88, textureX, textureY); // rightBootPart3
		rightLegModel[3] = new ModelRendererTurbo(this, 1, 109, textureX, textureY); // rightBootPart4

		rightLegModel[0].addBox(-4.5F, 14.5F, -4.5F, 9, 8, 9, 0F); // rightBootPart1
		// rightLegModel[0].setRotationPoint(-8.5F, 34.5F, -4.5F);

		rightLegModel[1].addBox(-5F, 13.5F, -5F, 10, 1, 10, 0F); // rightBootPart2
		// rightLegModel[1].setRotationPoint(-9F, 33.5F, -5F);

		rightLegModel[2].addBox(-4.5F, 17.5F, -7.5F, 9, 5, 3, 0F); // rightBootPart3
		// rightLegModel[2].setRotationPoint(-8.5F, 37.5F, -7.5F);

		rightLegModel[3].addBox(-5F, 22.5F, -8F, 10, 2, 13, 0F); // rightBootPart4
		// rightLegModel[3].setRotationPoint(-9F, 42.5F, -8F);
	}
}