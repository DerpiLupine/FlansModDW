package com.flansmod.client.model.dwdefence;

import com.flansmod.client.model.ModelCustomArmour;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelCasualHat extends ModelCustomArmour
{
	int textureX = 512;
	int textureY = 256;

	public ModelCasualHat()
	{
		headModel = new ModelRendererTurbo[3];
		headModel[0] = new ModelRendererTurbo(this, 1, 113, textureX, textureY); // Box 1
		headModel[1] = new ModelRendererTurbo(this, 1, 138, textureX, textureY); // Box 2
		headModel[2] = new ModelRendererTurbo(this, 1, 159, textureX, textureY); // Box 3

		headModel[0].addBox(-9F, -16F, -9F, 18, 6, 18, 0F); // Box 1
		//headModel[0].setRotationPoint();

		headModel[1].addShapeBox(-9F, -18F, -9F, 18, 2, 18, 0F, -2F, 0F, -2F, -2F, 0F, -2F, -2F, 0F, -2F, -2F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 2
		//headModel[1].setRotationPoint();

		headModel[2].addShapeBox(-9F, -12F, -13F, 18, 2, 4, 0F, -2F, -2F, 0F, -2F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 1F, 0F, -2F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 3
		//headModel[2].setRotationPoint();
	}
}