package com.flansmod.client.model.dwdefence;

import com.flansmod.client.model.ModelCustomArmour;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelCasualWolfHead extends ModelCustomArmour
{
	int textureX = 512;
	int textureY = 256;

	public ModelCasualWolfHead()
	{

		headModel = new ModelRendererTurbo[7];
		headModel[0] = new ModelRendererTurbo(this, 1, 113, textureX, textureY); // Box 1
		headModel[1] = new ModelRendererTurbo(this, 1, 138, textureX, textureY); // Box 2
		headModel[2] = new ModelRendererTurbo(this, 1, 159, textureX, textureY); // Box 3
		headModel[3] = new ModelRendererTurbo(this, 138, 1, textureX, textureY); // Box 27
		headModel[4] = new ModelRendererTurbo(this, 203, 22, textureX, textureY); // Box 28
		headModel[5] = new ModelRendererTurbo(this, 222, 21, textureX, textureY); // Box 31
		headModel[6] = new ModelRendererTurbo(this, 222, 21, textureX, textureY); // Box 32

		headModel[0].addBox(-9F, -16F, -9F, 18, 6, 18, 0F); // Box 1
		//headModel[0].setRotationPoint();

		headModel[1].addShapeBox(-9F, -18F, -9F, 18, 2, 18, 0F, -2F, 0F, -2F, -2F, 0F, -2F, -2F, 0F, -2F, -2F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 2
		//headModel[1].setRotationPoint();

		headModel[2].addShapeBox(-9F, -12F, -13F, 18, 2, 4, 0F, -2F, -2F, 0F, -2F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 1F, 0F, -2F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 3
		//headModel[2].setRotationPoint();

		headModel[3].addShapeBox(-7.8F, -16F, -8.2F, 16, 16, 16, 0F, 0.4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.4F, 0.4F, 0F, 0.4F, 0.4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.4F, 0.4F, 0F, 0.4F); // Box 27
		//headModel[3].setRotationPoint();

		headModel[4].addBox(-3F, -5F, -14F, 6, 5, 6, 0F); // Box 28
		//headModel[4].setRotationPoint();

		headModel[5].addBox(-8F, -20F, -4F, 4, 4, 2, 0F); // Box 31
		//headModel[5].setRotationPoint();

		headModel[6].addBox(4F, -20F, -4F, 4, 4, 2, 0F); // Box 32
		//headModel[6].setRotationPoint();
	}
}