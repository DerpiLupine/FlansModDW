package com.flansmod.client.model.dwdefence;

import com.flansmod.client.model.ModelCustomArmour;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelCasualLegs extends ModelCustomArmour
{
	int textureX = 512;
	int textureY = 256;

	public ModelCasualLegs()
	{
		leftLegModel = new ModelRendererTurbo[4];
		leftLegModel[0] = new ModelRendererTurbo(this, 88, 32, textureX, textureY); // leftPants
		leftLegModel[1] = new ModelRendererTurbo(this, 125, 39, textureX, textureY); // leftPocket
		leftLegModel[2] = new ModelRendererTurbo(this, 125, 53, textureX, textureY); // leftStrap
		leftLegModel[3] = new ModelRendererTurbo(this, 140, 43, textureX, textureY); // pocketHolder

		leftLegModel[0].addBox(-4.5F, -0.2F, -4.5F, 9, 24, 9, 0F); // leftPants
		// leftLegModel[0].setRotationPoint(-0.5F, 24.8F, -4.5F);

		leftLegModel[1].addBox(4F, 1.8F, -4F, 2, 8, 5, 0F); // leftPocket
		//leftLegModel[1].setRotationPoint(8F, 26.8F, -4F);

		leftLegModel[2].addShapeBox(-4.8F, 3F, -4.8F, 9, 3, 9, 0F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F); // leftStrap
		//leftLegModel[2].setRotationPoint(-0.8F, 28F, -4.8F);

		leftLegModel[3].addBox(4F, 3F, -4.5F, 1, 3, 6, 0F); // pocketHolder
		//leftLegModel[3].setRotationPoint(8F, 28F, -4.5F);

		//-4X, -20Y 


		rightLegModel = new ModelRendererTurbo[5];
		rightLegModel[0] = new ModelRendererTurbo(this, 88, 66, textureX, textureY); // rightPants
		rightLegModel[1] = new ModelRendererTurbo(this, 125, 87, textureX, textureY); // rightStrap
		rightLegModel[2] = new ModelRendererTurbo(this, 125, 77, textureX, textureY); // rightPocket
		rightLegModel[3] = new ModelRendererTurbo(this, 136, 75, textureX, textureY); // pocketHolder2
		rightLegModel[4] = new ModelRendererTurbo(this, 125, 77, textureX, textureY); // rightPocket

		rightLegModel[0].addBox(-4.5F, -0.2F, -4.5F, 9, 24, 9, 0F); // rightPants
		//rightLegModel[0].setRotationPoint(-8.5F, 24.8F, -4.5F);

		rightLegModel[1].addShapeBox(-4.8F, 3F, -4.8F, 9, 3, 9, 0F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F); // rightStrap
		//rightLegModel[1].setRotationPoint(-8.8F, 28F, -4.8F);

		rightLegModel[2].addBox(-6F, 2.8F, -4F, 2, 6, 3, 0F); // rightPocket
		//rightLegModel[2].setRotationPoint(-10F, 27.8F, -4F);

		rightLegModel[3].addBox(-5.3F, 3F, -4.5F, 1, 3, 8, 0F); // pocketHolder2
		//rightLegModel[3].setRotationPoint(-9.3F, 28F, -4.5F);

		rightLegModel[4].addBox(-6F, 2.8F, 0F, 2, 6, 3, 0F); // rightPocket
		//rightLegModel[4].setRotationPoint(-10F, 27.8F, 0F);
	}
}