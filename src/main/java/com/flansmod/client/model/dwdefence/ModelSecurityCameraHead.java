package com.flansmod.client.model.dwdefence;

import com.flansmod.client.model.ModelCustomArmour;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelSecurityCameraHead extends ModelCustomArmour
{
	int textureX = 256;
	int textureY = 128;

	public ModelSecurityCameraHead()
	{
		headModel = new ModelRendererTurbo[8];
		headModel[0] = new ModelRendererTurbo(this, 1, 46, textureX, textureY); // CamHead
		headModel[1] = new ModelRendererTurbo(this, 78, 5, textureX, textureY); // HeaderCap1
		headModel[2] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // HeaderCap3
		headModel[3] = new ModelRendererTurbo(this, 8, 22, textureX, textureY); // CamPartBack
		headModel[4] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // HeaderCap2
		headModel[5] = new ModelRendererTurbo(this, 8, 15, textureX, textureY); // LensTop
		headModel[6] = new ModelRendererTurbo(this, 8, 8, textureX, textureY); // LensMiddle
		headModel[7] = new ModelRendererTurbo(this, 8, 15, textureX, textureY); // LensBottom

		headModel[0].addBox(-9F, -16F, -13F, 18, 18, 26, 0F); // CamHead
		//headModel[0].setRotationPoint(-9F, -16F, -13F);

		headModel[1].addShapeBox(-11F, -20F, -21F, 22, 4, 36, 0F,-4F, 0F, 0F,-4F, 0F, 0F,-4F, 0F, 0F,-4F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F); // HeaderCap1
		//headModel[1].setRotationPoint(-11F, -20F, -21F);

		headModel[2].addShapeBox(-11F, -16F, -21F, 2, 8, 36, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, -4F,0F, 0F, -4F,0F, 0F, 0F,0F, 0F, 0F); // HeaderCap3
		//headModel[2].setRotationPoint(-11F, -16F, -21F);

		headModel[3].addBox(-6F, -13F, 13F, 12, 12, 2, 0F); // CamPartBack
		//headModel[3].setRotationPoint(-6F, -13F, 13F);

		headModel[4].addShapeBox(9F, -16F, -21F, 2, 8, 36, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, -4F,0F, 0F, -4F,0F, 0F, 0F,0F, 0F, 0F); // HeaderCap2
		//headModel[4].setRotationPoint(9F, -16F, -21F);

		headModel[5].addShapeBox(-6F, -13F, -15F, 12, 4, 2, 0F,-4F, 0F, 0F,-4F, 0F, 0F,-4F, 0F, 0F,-4F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F); // LensTop
		//headModel[5].setRotationPoint(-6F, -13F, -15F);

		headModel[6].addBox(-6F, -9F, -15F, 12, 4, 2, 0F); // LensMiddle
		//headModel[6].setRotationPoint(-6F, -9F, -15F);

		headModel[7].addShapeBox(-6F, -5F, -15F, 12, 4, 2, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,0F, 0F, 0F,-4F, 0F, 0F,-4F, 0F, 0F,-4F, 0F, 0F,-4F, 0F, 0F); // LensBottom
		//headModel[7].setRotationPoint(-6F, -5F, -15F);


	}
}
