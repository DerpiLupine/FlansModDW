package com.flansmod.client.model.dwdefence;

import com.flansmod.client.model.ModelCustomArmour;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.common.vector.Vector3f;

public class ModelCasualBody extends ModelCustomArmour
{
	int textureX = 512;
	int textureY = 256;

	public ModelCasualBody()
	{
		bodyModel = new ModelRendererTurbo[13];
		bodyModel[0] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // shirt1
		bodyModel[1] = new ModelRendererTurbo(this, 81, 1, textureX, textureY); // bodyArmor1
		bodyModel[2] = new ModelRendererTurbo(this, 56, 61, textureX, textureY); // bodyArmor2
		bodyModel[3] = new ModelRendererTurbo(this, 56, 61, textureX, textureY); // bodyArmor2
		bodyModel[4] = new ModelRendererTurbo(this, 54, 1, textureX, textureY); // strap
		bodyModel[5] = new ModelRendererTurbo(this, 56, 77, textureX, textureY); // radio1
		bodyModel[6] = new ModelRendererTurbo(this, 71, 83, textureX, textureY); // radio2
		bodyModel[7] = new ModelRendererTurbo(this, 56, 51, textureX, textureY); // pocket
		bodyModel[8] = new ModelRendererTurbo(this, 56, 51, textureX, textureY); // pocket
		bodyModel[9] = new ModelRendererTurbo(this, 81, 32, textureX, textureY); // strapPack
		bodyModel[10] = new ModelRendererTurbo(this, 81, 32, textureX, textureY); // strapPack
		bodyModel[11] = new ModelRendererTurbo(this, 56, 40, textureX, textureY); // backPocket
		bodyModel[12] = new ModelRendererTurbo(this, 75, 46, textureX, textureY); // stick

		bodyModel[0].addBox(-8.5F, -0.2F, -4.5F, 17, 25, 9, 0F); // shirt1

		bodyModel[1].addBox(-9F, 4.5F, -5F, 18, 20, 10, 0F); // bodyArmor1

		bodyModel[2].addBox(-7F, -0.5F, -5F, 4, 5, 10, 0F); // bodyArmor2

		bodyModel[3].addBox(3F, -0.5F, -5F, 4, 5, 10, 0F); // bodyArmor2

		bodyModel[4].addShapeBox(-7.5F, -0.5F, -5.5F, 2, 24, 11, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -16F, 2F, 0F, 15F, 0F, 0F, 15F, 0F, 0F, -16F, 2F, 0F); // strap

		bodyModel[5].addBox(3F, 3.5F, -6F, 5, 8, 2, 0F); // radio1

		bodyModel[6].addBox(6.5F, 0.5F, -6F, 1, 3, 1, 0F); // radio2

		bodyModel[7].addBox(1F, 16.5F, -6.5F, 7, 7, 2, 0F); // pocket

		bodyModel[8].addBox(-8F, 16.5F, -6.5F, 7, 7, 2, 0F); // pocket

		bodyModel[9].addShapeBox(-7.5F, 3.5F, -6.5F, 5, 3, 1, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, -2F, 2F, 0F, 2F, -2F, 0F, 2F, -2F, 0F, -2F, 2F, 0F); // strapPack

		bodyModel[10].addShapeBox(-5F, 7F, -6.5F, 5, 3, 1, 0F, 0F, -2F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, -2F, 0F, -2F, 2F, 0F, 2F, -2F, 0F, 2F, -2F, 0F, -2F, 2F, 0F); // strapPack

		bodyModel[11].addBox(-8F, 19.5F, 5F, 5, 8, 2, 0F); // backPocket

		bodyModel[12].addBox(6F, 15.5F, 5F, 2, 12, 2, 0F); // stick


		leftArmModel = new ModelRendererTurbo[3];
		leftArmModel[0] = new ModelRendererTurbo(this, 1, 36, textureX, textureY); // leftSleeve
		leftArmModel[1] = new ModelRendererTurbo(this, 38, 51, textureX, textureY); // leftPocket
		leftArmModel[2] = new ModelRendererTurbo(this, 1, 86, textureX, textureY); // armBand

		leftArmModel[0].addBox(-2.5F, -4.2F, -4.5F, 9, 15, 9, 0F); // leftSleeve
		leftArmModel[0].setRotationPoint(7.5F, 0.2F, -4.5F);
		//(-10,-4,0);

		leftArmModel[1].addShapeBox(6F, -3.5F, -3.5F, 1, 2, 7, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // leftPocket
		leftArmModel[1].setRotationPoint(16F, 0.5F, -3.5F);

		leftArmModel[2].addShapeBox(-2.7F, 0.8F, -4.7F, 18, 8, 18, 0F, 0F, -4F, 0F, -8.6F, -4F, 0F, -8.6F, -4F, -8.6F, 0F, -4F, -8.6F, 0F, 0F, 0F, -8.6F, 0F, 0F, -8.6F, 0F, -8.6F, 0F, 0F, -8.6F); // armBand
		leftArmModel[2].setRotationPoint(7.3F, 4.8F, -4.7F);


		rightArmModel = new ModelRendererTurbo[2];
		rightArmModel[0] = new ModelRendererTurbo(this, 1, 61, textureX, textureY); // rightSleeve
		rightArmModel[1] = new ModelRendererTurbo(this, 38, 76, textureX, textureY); // rightPocket

		rightArmModel[0].addBox(-6.5F, -4.2F, -4.5F, 9, 15, 9, 0F); // rightSleeve
		rightArmModel[0].setRotationPoint(-16.5F, 0.2F, -4.5F);

		rightArmModel[1].addShapeBox(-7F, -3.5F, -3.5F, 1, 2, 7, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // rightPocket
		rightArmModel[1].setRotationPoint(-17F, 0.5F, -3.5F);
	}
}