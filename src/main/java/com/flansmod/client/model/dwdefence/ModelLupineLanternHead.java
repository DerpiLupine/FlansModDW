package com.flansmod.client.model.dwdefence;

import com.flansmod.client.model.ModelCustomArmour;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelLupineLanternHead extends ModelCustomArmour
{
	int textureX = 512;
	int textureY = 256;

	public ModelLupineLanternHead()
	{
		headModel = new ModelRendererTurbo[14];
		headModel[0] = new ModelRendererTurbo(this, 147, 205, textureX, textureY); // helmet7
		headModel[1] = new ModelRendererTurbo(this, 224, 104, textureX, textureY); // decal
		headModel[2] = new ModelRendererTurbo(this, 224, 162, textureX, textureY); // goggleStrap
		headModel[3] = new ModelRendererTurbo(this, 224, 148, textureX, textureY); // headset
		headModel[4] = new ModelRendererTurbo(this, 224, 148, textureX, textureY); // headset
		headModel[5] = new ModelRendererTurbo(this, 245, 152, textureX, textureY); // headset2
		headModel[6] = new ModelRendererTurbo(this, 245, 152, textureX, textureY); // headset2
		headModel[7] = new ModelRendererTurbo(this, 147, 149, textureX, textureY); // helmet1
		headModel[8] = new ModelRendererTurbo(this, 147, 102, textureX, textureY); // helmet2
		headModel[9] = new ModelRendererTurbo(this, 147, 127, textureX, textureY); // helmet3
		headModel[10] = new ModelRendererTurbo(this, 147, 169, textureX, textureY); // helmet4
		headModel[11] = new ModelRendererTurbo(this, 147, 183, textureX, textureY); // helmet5
		headModel[12] = new ModelRendererTurbo(this, 147, 195, textureX, textureY); // helmet6
		headModel[13] = new ModelRendererTurbo(this, 266, 152, textureX, textureY); // mic

		headModel[0].addShapeBox(-7.5F, -11F, 9F, 15, 6, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F); // helmet7
		headModel[0].setRotationPoint(0F, 0F, 0F);

		headModel[1].addShapeBox(-11.2F, -9.5F, -2F, 1, 8, 14, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -8F, 0F, 0F, -8F, 0F, -5F, 0F, 0F, -5F, 0F, 0F, -5F, -8F, 0F, -5F, -8F); // decal
		headModel[1].setRotationPoint(0F, 0F, 0F);

		headModel[2].addShapeBox(-8.5F, -16.5F, -8.5F, 16, 16, 16, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 1F, 0F, 0F, 1F, 0F, 1F, 0F, 1F, 1F, 0F, 1F, 1F, 1F, 0F, 1F, 1F); // goggleStrap
		headModel[2].setRotationPoint(0F, 0F, 0F);

		headModel[3].addBox(-11F, -10F, -2.5F, 2, 5, 8, 0F); // headset
		headModel[3].setRotationPoint(0F, 0F, 0F);

		headModel[4].addBox(9F, -10F, -2.5F, 2, 5, 8, 0F); // headset
		headModel[4].setRotationPoint(0F, 0F, 0F);

		headModel[5].addShapeBox(-11F, -11F, -2.5F, 2, 1, 8, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // headset2
		headModel[5].setRotationPoint(0F, 0F, 0F);

		headModel[6].addShapeBox(9F, -11F, -2.5F, 2, 1, 8, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // headset2
		headModel[6].setRotationPoint(0F, 0F, 0F);

		headModel[7].addShapeBox(-8.5F, -19F, -8.5F, 17, 2, 17, 0F, -3F, 0F, -3F, -3F, 0F, -3F, -3F, 0F, -3F, -3F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // helmet1
		headModel[7].setRotationPoint(0F, 0F, 0F);

		headModel[8].addShapeBox(-9.5F, -17F, -9.5F, 19, 5, 19, 0F, -1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // helmet2
		headModel[8].setRotationPoint(0F, 0F, 0F);

		headModel[9].addShapeBox(-9.5F, -12F, -9.5F, 19, 2, 19, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 1F, 1F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F); // helmet3
		headModel[9].setRotationPoint(0F, 0F, 0F);

		headModel[10].addShapeBox(-9.5F, -10F, -0.5F, 19, 3, 10, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // helmet4
		headModel[10].setRotationPoint(0F, 0F, 0F);

		headModel[11].addBox(-9.5F, -7F, 1.5F, 19, 3, 8, 0F); // helmet5
		headModel[11].setRotationPoint(0F, 0F, 0F);

		headModel[12].addShapeBox(-9.5F, -4F, 1.5F, 19, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // helmet6
		headModel[12].setRotationPoint(0F, 0F, 0F);

		headModel[13].addBox(-10.5F, -5F, -4.5F, 1, 1, 8, 0F); // mic
		headModel[13].setRotationPoint(0F, 0F, 0F);
	}
}
