package com.flansmod.client.model.dwdefence;

import com.flansmod.client.model.ModelCustomArmour;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelCasualBoots extends ModelCustomArmour
{
	int textureX = 512;
	int textureY = 256;

	public ModelCasualBoots()
	{
		leftLegModel = new ModelRendererTurbo[4];
		leftLegModel[0] = new ModelRendererTurbo(this, 149, 224, textureX, textureY); // Import leftBootPart2
		leftLegModel[1] = new ModelRendererTurbo(this, 149, 199, textureX, textureY); // Import leftBootPart3
		leftLegModel[2] = new ModelRendererTurbo(this, 149, 214, textureX, textureY); // Box 3
		leftLegModel[3] = new ModelRendererTurbo(this, 149, 177, textureX, textureY); // Box 4

		leftLegModel[0].addBox(-4.5F, 19.5F, -7.5F, 9, 4, 3, 0F); // Import leftBootPart2
		leftLegModel[0].setRotationPoint(0F, 0F, 0F);

		leftLegModel[1].addBox(-5F, 23.5F, -8F, 10, 1, 13, 0F); // Import leftBootPart3
		leftLegModel[1].setRotationPoint(0F, 0F, 0F);

		leftLegModel[2].addShapeBox(-4.3F, 9.5F, -2.7F, 9, 2, 7, 0F, 0.4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.4F, 0.4F, 0F, 0.4F, 0.4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.4F, 0.4F, 0F, 0.4F); // Box 3
		leftLegModel[2].setRotationPoint(0F, 0F, 0F);

		leftLegModel[3].addShapeBox(-4.3F, 11.5F, -4.7F, 9, 12, 9, 0F, 0.4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.4F, 0.4F, 0F, 0.4F, 0.4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.4F, 0.4F, 0F, 0.4F); // Box 4
		leftLegModel[3].setRotationPoint(0F, 0F, 0F);


		rightLegModel = new ModelRendererTurbo[4];
		rightLegModel[0] = new ModelRendererTurbo(this, 149, 224, textureX, textureY); // Import rightBootPart3
		rightLegModel[1] = new ModelRendererTurbo(this, 149, 199, textureX, textureY); // Import rightBootPart4
		rightLegModel[2] = new ModelRendererTurbo(this, 149, 177, textureX, textureY); // Box 2
		rightLegModel[3] = new ModelRendererTurbo(this, 149, 214, textureX, textureY); // Box 5

		rightLegModel[0].addBox(-4.5F, 19.5F, -7.5F, 9, 4, 3, 0F); // Import rightBootPart3
		rightLegModel[0].setRotationPoint(0F, 0F, 0F);

		rightLegModel[1].addBox(-5F, 23.5F, -8F, 10, 1, 13, 0F); // Import rightBootPart4
		rightLegModel[1].setRotationPoint(0F, 0F, 0F);

		rightLegModel[2].addShapeBox(-4.3F, 11.5F, -4.7F, 9, 12, 9, 0F, 0.4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.4F, 0.4F, 0F, 0.4F, 0.4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.4F, 0.4F, 0F, 0.4F); // Box 2
		rightLegModel[2].setRotationPoint(0F, 0F, 0F);

		rightLegModel[3].addShapeBox(-4.3F, 9.5F, -2.7F, 9, 2, 7, 0F, 0.4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.4F, 0.4F, 0F, 0.4F, 0.4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.4F, 0.4F, 0F, 0.4F); // Box 5
		rightLegModel[3].setRotationPoint(0F, 0F, 0F);
	}
}