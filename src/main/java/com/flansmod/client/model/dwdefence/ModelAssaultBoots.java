package com.flansmod.client.model.dwdefence;

import com.flansmod.client.model.ModelCustomArmour;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.common.vector.Vector3f;

public class ModelAssaultBoots extends ModelCustomArmour
{
	int textureX = 512;
	int textureY = 256;

	public ModelAssaultBoots()
	{
		leftLegModel = new ModelRendererTurbo[3];
		leftLegModel[0] = new ModelRendererTurbo(this, 1, 230, textureX, textureY); // leftBoot
		leftLegModel[1] = new ModelRendererTurbo(this, 38, 239, textureX, textureY); // leftBoot2
		leftLegModel[2] = new ModelRendererTurbo(this, 38, 231, textureX, textureY); // leftBoot3

		leftLegModel[0].addShapeBox(-4.3F, 17.2F, -4.7F, 9, 7, 9, 0F, 0.4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.4F, 0.4F, 0F, 0.4F, 0.4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.4F, 0.4F, 0F, 0.4F); // leftBoot
		leftLegModel[0].setRotationPoint(0F, 0F, 0F);

		leftLegModel[1].addShapeBox(-4.3F, 15.2F, -0.7F, 9, 2, 5, 0F, 0.4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.4F, 0.4F, 0F, 0.4F, 0.4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.4F, 0.4F, 0F, 0.4F); // leftBoot2
		leftLegModel[1].setRotationPoint(0F, 0F, 0F);

		leftLegModel[2].addShapeBox(-3.3F, 20.2F, -8.1F, 7, 4, 3, 0F, 0.4F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0.4F, 1.4F, 0F, 0.4F, 0.4F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0.4F, 1.4F, 0F, 0.4F); // leftBoot3
		leftLegModel[2].setRotationPoint(0F, 0F, 0F);


		rightLegModel = new ModelRendererTurbo[3];
		rightLegModel[0] = new ModelRendererTurbo(this, 1, 230, textureX, textureY); // rightBoot
		rightLegModel[1] = new ModelRendererTurbo(this, 38, 239, textureX, textureY); // rightBoot2
		rightLegModel[2] = new ModelRendererTurbo(this, 38, 231, textureX, textureY); // rightBoot3

		rightLegModel[0].addShapeBox(-4.3F, 17.2F, -4.7F, 9, 7, 9, 0F, 0.4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.4F, 0.4F, 0F, 0.4F, 0.4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.4F, 0.4F, 0F, 0.4F); // rightBoot
		rightLegModel[0].setRotationPoint(0F, 0F, 0F);

		rightLegModel[1].addShapeBox(-4.3F, 15.2F, -0.7F, 9, 2, 5, 0F, 0.4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.4F, 0.4F, 0F, 0.4F, 0.4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.4F, 0.4F, 0F, 0.4F); // rightBoot2
		rightLegModel[1].setRotationPoint(0F, 0F, 0F);

		rightLegModel[2].addShapeBox(-3.3F, 20.2F, -8.1F, 7, 4, 3, 0F, 0.4F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0.4F, 1.4F, 0F, 0.4F, 0.4F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0.4F, 1.4F, 0F, 0.4F); // rightBoot3
		rightLegModel[2].setRotationPoint(0F, 0F, 0F);
	}
}