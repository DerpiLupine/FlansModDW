package com.flansmod.client.model.dwdefence;

import com.flansmod.client.model.ModelCustomArmour;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.common.vector.Vector3f;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelLupineArmorStandard extends ModelCustomArmour
{
	int textureX = 512;
	int textureY = 256;

	public ModelLupineArmorStandard()
	{
		bodyModel = new ModelRendererTurbo[15];
		bodyModel[0] = new ModelRendererTurbo(this, 1, 31, textureX, textureY); // Box 0
		bodyModel[1] = new ModelRendererTurbo(this, 14, 53, textureX, textureY); // Box 1
		bodyModel[2] = new ModelRendererTurbo(this, 14, 53, textureX, textureY); // Box 2
		bodyModel[3] = new ModelRendererTurbo(this, 22, 31, textureX, textureY); // Box 3
		bodyModel[4] = new ModelRendererTurbo(this, 22, 42, textureX, textureY); // Box 4
		bodyModel[5] = new ModelRendererTurbo(this, 1, 42, textureX, textureY); // Box 5
		bodyModel[6] = new ModelRendererTurbo(this, 1, 53, textureX, textureY); // Box 6
		bodyModel[7] = new ModelRendererTurbo(this, 1, 53, textureX, textureY); // Box 7
		bodyModel[8] = new ModelRendererTurbo(this, 46, 49, textureX, textureY); // Box 0
		bodyModel[9] = new ModelRendererTurbo(this, 39, 49, textureX, textureY); // Box 1
		bodyModel[10] = new ModelRendererTurbo(this, 39, 49, textureX, textureY); // Box 2
		bodyModel[11] = new ModelRendererTurbo(this, 39, 39, textureX, textureY); // Box 3
		bodyModel[12] = new ModelRendererTurbo(this, 39, 31, textureX, textureY); // Box 4
		bodyModel[13] = new ModelRendererTurbo(this, 39, 44, textureX, textureY); // Box 5
		bodyModel[14] = new ModelRendererTurbo(this, 46, 49, textureX, textureY); // Box 6

		bodyModel[0].addBox(-4.5F, 3.75F, -2.5F, 9, 9, 1, 0F); // Box 0
		//bodyModel[0].setRotationPoint();

		bodyModel[1].addBox(-3.5F, -0.25F, -2.5F, 2, 2, 5, 0F); // Box 1
		//bodyModel[1].setRotationPoint();

		bodyModel[2].addBox(1.5F, -0.25F, -2.5F, 2, 2, 5, 0F); // Box 2
		//bodyModel[2].setRotationPoint();

		bodyModel[3].addBox(-3.5F, 1.75F, -2.5F, 7, 2, 1, 0F); // Box 3
		//bodyModel[3].setRotationPoint();

		bodyModel[4].addBox(-3.5F, 1.75F, 1.5F, 7, 2, 1, 0F); // Box 4
		//bodyModel[4].setRotationPoint();

		bodyModel[5].addBox(-4.5F, 3.75F, 1.5F, 9, 9, 1, 0F); // Box 5
		//bodyModel[5].setRotationPoint();

		bodyModel[6].addShapeBox(-4.5F, 9.25F, -2.5F, 1, 3, 5, 0F,0.1F, 0F, 0.1F,0F, 0F, 0.1F,0F, 0F, 0.1F,0.1F, 0F, 0.1F,0.1F, 0F, 0.1F,0F, 0F, 0.1F,0F, 0F, 0.1F,0.1F, 0F, 0.1F); // Box 6
		//bodyModel[6].setRotationPoint();

		bodyModel[7].addShapeBox(3.5F, 9.25F, -2.5F, 1, 3, 5, 0F,0F, 0F, 0.1F,0.1F, 0F, 0.1F,0.1F, 0F, 0.1F,0F, 0F, 0.1F,0F, 0F, 0.1F,0.1F, 0F, 0.1F,0.1F, 0F, 0.1F,0F, 0F, 0.1F); // Box 7
		//bodyModel[7].setRotationPoint();

		bodyModel[8].addBox(1.25F, 10.25F, -3.5F, 1, 2, 1, 0F); // Box 0
		//bodyModel[8].setRotationPoint();

		bodyModel[9].addBox(-1F, 8.25F, -3.5F, 2, 4, 1, 0F); // Box 1
		//bodyModel[9].setRotationPoint();

		bodyModel[10].addBox(-3.5F, 8.25F, -3.5F, 2, 4, 1, 0F); // Box 2
		//bodyModel[10].setRotationPoint();

		bodyModel[11].addBox(-4F, 4.25F, -3F, 8, 3, 1, 0F); // Box 3
		//bodyModel[11].setRotationPoint();

		bodyModel[12].addBox(-3.5F, 4.75F, 2F, 7, 6, 1, 0F); // Box 4
		//bodyModel[12].setRotationPoint();

		bodyModel[13].addBox(0F, 11F, 2.5F, 4, 3, 1, 0F); // Box 5
		//bodyModel[13].setRotationPoint();

		bodyModel[14].addBox(2.5F, 10.25F, -3.5F, 1, 2, 1, 0F); // Box 6
		//bodyModel[14].setRotationPoint();


	}
}