package com.flansmod.client.model.dwdefence;

import com.flansmod.client.model.ModelCustomArmour;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelContractorBoots extends ModelCustomArmour
{
	int textureX = 512;
	int textureY = 256;

	public ModelContractorBoots()
	{
		leftLegModel = new ModelRendererTurbo[5];
		leftLegModel[0] = new ModelRendererTurbo(this, 42, 214, textureX, textureY); // Import leftBootPart2
		leftLegModel[1] = new ModelRendererTurbo(this, 38, 191, textureX, textureY); // Import leftBootPart3
		leftLegModel[2] = new ModelRendererTurbo(this, 1, 191, textureX, textureY); // Box 4
		leftLegModel[3] = new ModelRendererTurbo(this, 1, 210, textureX, textureY); // Box 8
		leftLegModel[4] = new ModelRendererTurbo(this, 42, 209, textureX, textureY); // Box 11

		leftLegModel[0].addShapeBox(-4.5F, 19.5F, -7.5F, 9, 4, 3, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Import leftBootPart2

		leftLegModel[1].addBox(-5F, 23.5F, -5F, 10, 1, 10, 0F); // Import leftBootPart3

		leftLegModel[2].addShapeBox(-4.3F, 14.5F, -4.7F, 9, 9, 9, 0F, 0.4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.4F, 0.4F, 0F, 0.4F, 0.4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.4F, 0.4F, 0F, 0.4F); // Box 4
		leftLegModel[2].setRotationPoint(0F, 0F, 0F);

		leftLegModel[3].addBox(-5F, 13.5F, -5F, 10, 1, 10, 0F); // Box 8

		leftLegModel[4].addShapeBox(-5F, 23.5F, -8F, 10, 1, 3, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 11

		//-4X, -23Y 


		rightLegModel = new ModelRendererTurbo[5];
		rightLegModel[0] = new ModelRendererTurbo(this, 38, 191, textureX, textureY); // rightPartBottom
		rightLegModel[1] = new ModelRendererTurbo(this, 1, 191, textureX, textureY); // Box 2
		rightLegModel[2] = new ModelRendererTurbo(this, 42, 214, textureX, textureY); // Box 9
		rightLegModel[3] = new ModelRendererTurbo(this, 42, 209, textureX, textureY); // Box 10
		rightLegModel[4] = new ModelRendererTurbo(this, 1, 210, textureX, textureY); // Box 12

		rightLegModel[0].addBox(-5F, 23.5F, -5F, 10, 1, 10, 0F); // rightPartBottom

		rightLegModel[1].addShapeBox(-4.3F, 14.5F, -4.7F, 9, 9, 9, 0F, 0.4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.4F, 0.4F, 0F, 0.4F, 0.4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.4F, 0.4F, 0F, 0.4F); // Box 2

		rightLegModel[2].addShapeBox(-4.5F, 19.5F, -7.5F, 9, 4, 3, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 9

		rightLegModel[3].addShapeBox(-5F, 23.5F, -8F, 10, 1, 3, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 10

		rightLegModel[4].addBox(-5F, 13.5F, -5F, 10, 1, 10, 0F); // Box 12
	}
}