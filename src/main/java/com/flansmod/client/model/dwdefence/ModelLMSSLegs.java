package com.flansmod.client.model.dwdefence; //Path where the model is located

import com.flansmod.client.model.ModelCustomArmour;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelLMSSLegs extends ModelCustomArmour //Same as Filename
{
	int textureX = 512;
	int textureY = 256;

	public ModelLMSSLegs() //Same as Filename
	{
		leftLegModel = new ModelRendererTurbo[8];
		leftLegModel[0] = new ModelRendererTurbo(this, 75, 200, textureX, textureY); // kneeArmor3
		leftLegModel[1] = new ModelRendererTurbo(this, 1, 200, textureX, textureY); // leftPants
		leftLegModel[2] = new ModelRendererTurbo(this, 1, 230, textureX, textureY); // leftPocket
		leftLegModel[3] = new ModelRendererTurbo(this, 28, 230, textureX, textureY); // leftPocket2
		leftLegModel[4] = new ModelRendererTurbo(this, 75, 212, textureX, textureY); // pocketStrap
		leftLegModel[5] = new ModelRendererTurbo(this, 51, 246, textureX, textureY); // Box 1
		leftLegModel[6] = new ModelRendererTurbo(this, 51, 230, textureX, textureY); // Box 2
		leftLegModel[7] = new ModelRendererTurbo(this, 51, 243, textureX, textureY); // Box 3

		leftLegModel[0].addShapeBox(-4.6F, 12.5F, -4.8F, 9, 2, 9, 0F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F); // kneeArmor3
		leftLegModel[0].setRotationPoint(0F, 0F, 0F);

		leftLegModel[1].addBox(-4.5F, -0.199999999999999F, -4.5F, 9, 20, 9, 0F); // leftPants
		leftLegModel[1].setRotationPoint(0F, 0F, 0F);

		leftLegModel[2].addShapeBox(4.5F, -0.5F, -4.5F, 3, 8, 10, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 1F, -1F, 0F, 1F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F); // leftPocket
		leftLegModel[2].setRotationPoint(0F, 0F, 0F);

		leftLegModel[3].addShapeBox(7.5F, -0.5F, -4.5F, 1, 4, 10, 0F, 0F, 0F, 1F, -0.5F, -0.2F, 1F, -0.5F, 0.8F, -1F, 0F, 1F, -1F, 0F, 0F, 0.5F, -0.5F, 0F, 0.5F, -0.5F, -1F, -0.5F, 0F, -1F, -0.5F); // leftPocket2
		leftLegModel[3].setRotationPoint(0F, 0F, 0F);

		leftLegModel[4].addShapeBox(-4.6F, 2F, -4.8F, 9, 3, 9, 0F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F); // pocketStrap
		leftLegModel[4].setRotationPoint(0F, 0F, 0F);

		leftLegModel[5].addShapeBox(-4F, 19F, -5F, 8, 1, 1, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 1
		leftLegModel[5].setRotationPoint(0F, 0F, 0F);

		leftLegModel[6].addShapeBox(-4F, 8F, -5F, 8, 11, 1, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 2
		leftLegModel[6].setRotationPoint(0F, 0F, 0F);

		leftLegModel[7].addShapeBox(-4F, 7F, -5F, 8, 1, 1, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 3
		leftLegModel[7].setRotationPoint(0F, 0F, 0F);


		rightLegModel = new ModelRendererTurbo[11];
		rightLegModel[0] = new ModelRendererTurbo(this, 41, 230, textureX, textureY); // rightPocket
		rightLegModel[1] = new ModelRendererTurbo(this, 75, 225, textureX, textureY); // Box 1
		rightLegModel[2] = new ModelRendererTurbo(this, 51, 230, textureX, textureY); // kneeArmor
		rightLegModel[3] = new ModelRendererTurbo(this, 51, 243, textureX, textureY); // kneeArmor2
		rightLegModel[4] = new ModelRendererTurbo(this, 75, 200, textureX, textureY); // kneeArmor3
		rightLegModel[5] = new ModelRendererTurbo(this, 38, 200, textureX, textureY); // rightPants
		rightLegModel[6] = new ModelRendererTurbo(this, 75, 212, textureX, textureY); // rightStrap
		rightLegModel[7] = new ModelRendererTurbo(this, 51, 246, textureX, textureY); // Box 0
		rightLegModel[8] = new ModelRendererTurbo(this, 41, 230, textureX, textureY); // Box 4
		rightLegModel[9] = new ModelRendererTurbo(this, 41, 230, textureX, textureY); // Box 5
		rightLegModel[10] = new ModelRendererTurbo(this, 41, 230, textureX, textureY); // Box 6

		rightLegModel[0].addShapeBox(-6.5F, 0.800000000000001F, -3F, 2, 8, 1, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // rightPocket
		rightLegModel[0].setRotationPoint(0F, 0F, 0F);

		rightLegModel[1].addBox(-5.5F, 2F, -3.5F, 1, 3, 6, 0F); // Box 1
		rightLegModel[1].setRotationPoint(0F, 0F, 0F);

		rightLegModel[2].addShapeBox(-4F, 8F, -5F, 8, 11, 1, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // kneeArmor
		rightLegModel[2].setRotationPoint(0F, 0F, 0F);

		rightLegModel[3].addShapeBox(-4F, 7F, -5F, 8, 1, 1, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // kneeArmor2
		rightLegModel[3].setRotationPoint(0F, 0F, 0F);

		rightLegModel[4].addShapeBox(-4.8F, 12.5F, -4.8F, 9, 2, 9, 0F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F); // kneeArmor3
		rightLegModel[4].setRotationPoint(0F, 0F, 0F);

		rightLegModel[5].addBox(-4.5F, -0.199999999999999F, -4.5F, 9, 20, 9, 0F); // rightPants
		rightLegModel[5].setRotationPoint(0F, 0F, 0F);

		rightLegModel[6].addShapeBox(-4.8F, 2F, -4.8F, 9, 3, 9, 0F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F); // rightStrap
		rightLegModel[6].setRotationPoint(0F, 0F, 0F);

		rightLegModel[7].addShapeBox(-4F, 19F, -5F, 8, 1, 1, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 0
		rightLegModel[7].setRotationPoint(0F, 0F, 0F);

		rightLegModel[8].addShapeBox(-6.5F, 0.800000000000001F, -2F, 2, 8, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 4
		rightLegModel[8].setRotationPoint(0F, 0F, 0F);

		rightLegModel[9].addShapeBox(-6.5F, 0.800000000000001F, 1F, 2, 8, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F); // Box 5
		rightLegModel[9].setRotationPoint(0F, 0F, 0F);

		rightLegModel[10].addShapeBox(-6.5F, 0.800000000000001F, 0F, 2, 8, 1, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 6
		rightLegModel[10].setRotationPoint(0F, 0F, 0F);


	}
}