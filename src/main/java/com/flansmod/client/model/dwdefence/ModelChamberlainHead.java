package com.flansmod.client.model.dwdefence; //Path where the model is located

import com.flansmod.client.model.ModelCustomArmour;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelChamberlainHead extends ModelCustomArmour //Same as Filename
{
	int textureX = 512;
	int textureY = 256;

	public ModelChamberlainHead() //Same as Filename
	{
		headModel = new ModelRendererTurbo[18];
		headModel[0] = new ModelRendererTurbo(this, 1, 48, textureX, textureY); // helmet1
		headModel[1] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // helmet2
		headModel[2] = new ModelRendererTurbo(this, 78, 18, textureX, textureY); // helmet3
		headModel[3] = new ModelRendererTurbo(this, 1, 68, textureX, textureY); // helmet4
		headModel[4] = new ModelRendererTurbo(this, 1, 90, textureX, textureY); // helmet5
		headModel[5] = new ModelRendererTurbo(this, 78, 1, textureX, textureY); // helmet6
		headModel[6] = new ModelRendererTurbo(this, 78, 64, textureX, textureY); // Box 4
		headModel[7] = new ModelRendererTurbo(this, 78, 40, textureX, textureY); // Box 0
		headModel[8] = new ModelRendererTurbo(this, 78, 52, textureX, textureY); // Box 1
		headModel[9] = new ModelRendererTurbo(this, 99, 67, textureX, textureY); // Box 2
		headModel[10] = new ModelRendererTurbo(this, 99, 67, textureX, textureY); // Box 3
		headModel[11] = new ModelRendererTurbo(this, 78, 60, textureX, textureY); // Box 4
		headModel[12] = new ModelRendererTurbo(this, 78, 60, textureX, textureY); // Box 5
		headModel[13] = new ModelRendererTurbo(this, 78, 64, textureX, textureY); // Box 6
		headModel[14] = new ModelRendererTurbo(this, 1, 26, textureX, textureY); // Box 8
		headModel[15] = new ModelRendererTurbo(this, 78, 74, textureX, textureY); // Box 9
		headModel[16] = new ModelRendererTurbo(this, 78, 67, textureX, textureY); // Box 10
		headModel[17] = new ModelRendererTurbo(this, 78, 74, textureX, textureY); // Box 11

		headModel[0].addShapeBox(-8.5F, -19F, -8.5F, 17, 2, 17, 0F, -3F, 0F, -3F, -3F, 0F, -3F, -3F, 0F, -3F, -3F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // helmet1
		headModel[0].setRotationPoint(0F, 0F, 0F);

		headModel[1].addShapeBox(-9.5F, -17F, -9.5F, 19, 5, 19, 0F, -1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // helmet2
		headModel[1].setRotationPoint(0F, 0F, 0F);

		headModel[2].addBox(-9.5F, -12F, -9.5F, 19, 2, 19, 0F); // helmet3
		headModel[2].setRotationPoint(0F, 0F, 0F);

		headModel[3].addShapeBox(-9.5F, -10F, -6.5F, 19, 5, 16, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // helmet4
		headModel[3].setRotationPoint(0F, 0F, 0F);

		headModel[4].addShapeBox(-9.5F, -5F, -6.5F, 19, 2, 16, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, -1F, 0F, 0F, -1F, 0F); // helmet5
		headModel[4].setRotationPoint(0F, 0F, 0F);

		headModel[5].addShapeBox(-9.5F, -3F, -5.5F, 19, 1, 15, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, -1F, 0F, -2F, -1F, 0F, -2F, -1F, -1F, 0F, -1F, -1F, 0F); // helmet6
		headModel[5].setRotationPoint(0F, 0F, 0F);

		headModel[6].addShapeBox(-9.5F, -11F, -10.5F, 19, 1, 1, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 1F, -1F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 4
		headModel[6].setRotationPoint(0F, 0F, 0F);

		headModel[7].addShapeBox(-9.5F, -10F, -10.5F, 19, 10, 1, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 0
		headModel[7].setRotationPoint(0F, 0F, 0F);

		headModel[8].addShapeBox(-7.5F, -10F, -11.5F, 15, 6, 1, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 1
		headModel[8].setRotationPoint(0F, 0F, 0F);

		headModel[9].addBox(-9.5F, -10F, -9.5F, 1, 10, 2, 0F); // Box 2
		headModel[9].setRotationPoint(0F, 0F, 0F);

		headModel[10].addBox(8.5F, -10F, -9.5F, 1, 10, 2, 0F); // Box 3
		headModel[10].setRotationPoint(0F, 0F, 0F);

		headModel[11].addShapeBox(-10F, -12F, -9.5F, 20, 1, 2, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 4
		headModel[11].setRotationPoint(0F, 0F, 0F);

		headModel[12].addShapeBox(-10F, -11F, -9.5F, 20, 1, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 5
		headModel[12].setRotationPoint(0F, 0F, 0F);

		headModel[13].addShapeBox(-9.5F, -12F, -10.5F, 19, 1, 1, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 6
		headModel[13].setRotationPoint(0F, 0F, 0F);

		headModel[14].addBox(-8.5F, -4F, -8.5F, 17, 4, 17, 0F); // Box 8
		headModel[14].setRotationPoint(0F, 0F, 0F);

		headModel[15].addShapeBox(-3.5F, 0F, -11.5F, 7, 3, 2, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 9
		headModel[15].setRotationPoint(0F, 0F, 0F);

		headModel[16].addBox(-3.5F, 0F, -9.5F, 7, 3, 3, 0F); // Box 10
		headModel[16].setRotationPoint(0F, 0F, 0F);

		headModel[17].addShapeBox(-3.5F, 0F, -6.5F, 7, 3, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F); // Box 11
		headModel[17].setRotationPoint(0F, 0F, 0F);


	}
}