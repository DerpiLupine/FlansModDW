package com.flansmod.client.model.dwdefence;

import com.flansmod.client.model.ModelCustomArmour;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelContractorHead extends ModelCustomArmour
{
	int textureX = 512;
	int textureY = 256;

	public ModelContractorHead()
	{
		headModel = new ModelRendererTurbo[16];
		headModel[0] = new ModelRendererTurbo(this, 56, 161, textureX, textureY); // Box 2
		headModel[1] = new ModelRendererTurbo(this, 1, 152, textureX, textureY); // Box 5
		headModel[2] = new ModelRendererTurbo(this, 56, 152, textureX, textureY); // Box 6
		headModel[3] = new ModelRendererTurbo(this, 1, 174, textureX, textureY); // Box 7
		headModel[4] = new ModelRendererTurbo(this, 1, 174, textureX, textureY); // Box 8
		headModel[5] = new ModelRendererTurbo(this, 1, 166, textureX, textureY); // Box 9
		headModel[6] = new ModelRendererTurbo(this, 1, 166, textureX, textureY); // Box 10
		headModel[7] = new ModelRendererTurbo(this, 85, 161, textureX, textureY); // Box 11
		headModel[8] = new ModelRendererTurbo(this, 1, 183, textureX, textureY); // Box 12
		headModel[9] = new ModelRendererTurbo(this, 1, 183, textureX, textureY); // Box 13
		headModel[10] = new ModelRendererTurbo(this, 1, 183, textureX, textureY); // Box 14
		headModel[11] = new ModelRendererTurbo(this, 1, 183, textureX, textureY); // Box 15
		headModel[12] = new ModelRendererTurbo(this, 22, 174, textureX, textureY); // Box 16
		headModel[13] = new ModelRendererTurbo(this, 90, 161, textureX, textureY); // Box 17
		headModel[14] = new ModelRendererTurbo(this, 93, 167, textureX, textureY); // Box 18
		headModel[15] = new ModelRendererTurbo(this, 39, 174, textureX, textureY); // Box 19

		headModel[0].addBox(-6F, -13F, -10F, 12, 3, 2, 0F); // Box 2

		headModel[1].addBox(-10.5F, -13F, -3F, 21, 7, 6, 0F); // Box 5

		headModel[2].addBox(-9F, -17F, -2F, 18, 4, 4, 0F); // Box 6

		headModel[3].addBox(-5F, -14F, -14F, 4, 2, 6, 0F); // Box 7

		headModel[4].addBox(1F, -14F, -14F, 4, 2, 6, 0F); // Box 8

		headModel[5].addShapeBox(-10.5F, -6F, -3F, 21, 1, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 9

		headModel[6].addShapeBox(-10.5F, -14F, -3F, 21, 1, 6, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 10

		headModel[7].addBox(-9.5F, -21F, 1F, 1, 8, 1, 0F); // Box 11

		headModel[8].addShapeBox(1F, -15F, -14F, 4, 1, 6, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 12

		headModel[9].addShapeBox(-5F, -15F, -14F, 4, 1, 6, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 13

		headModel[10].addShapeBox(1F, -12F, -14F, 4, 1, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 14

		headModel[11].addShapeBox(-5F, -12F, -14F, 4, 1, 6, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 15

		headModel[12].addBox(-2F, -17F, -9F, 4, 4, 4, 0F); // Box 16

		headModel[13].addShapeBox(-9.5F, -6F, -1F, 0, 8, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 6F, 0F, -3F, 6F, 0F, -2F, -7F, 0F, -2F, -7F); // Box 17

		headModel[14].addBox(-10F, -1F, -8.5F, 1, 1, 2, 0F); // Box 18

		headModel[15].addShapeBox(-3F, -18F, -7F, 6, 2, 4, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 19
	}
}