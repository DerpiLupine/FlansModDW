package com.flansmod.client.model.dwdefence; //Path where the model is located

import com.flansmod.client.model.ModelCustomArmour;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelChamberlainBody extends ModelCustomArmour //Same as Filename
{
	int textureX = 512;
	int textureY = 256;

	public ModelChamberlainBody() //Same as Filename
	{
		bodyModel = new ModelRendererTurbo[27];
		bodyModel[0] = new ModelRendererTurbo(this, 56, 158, textureX, textureY); // armor1
		bodyModel[1] = new ModelRendererTurbo(this, 50, 118, textureX, textureY); // backArmorPlate
		bodyModel[2] = new ModelRendererTurbo(this, 1, 109, textureX, textureY); // body1
		bodyModel[3] = new ModelRendererTurbo(this, 1, 172, textureX, textureY); // frontArmorPlate
		bodyModel[4] = new ModelRendererTurbo(this, 1, 142, textureX, textureY); // pocket1
		bodyModel[5] = new ModelRendererTurbo(this, 1, 153, textureX, textureY); // pocket2
		bodyModel[6] = new ModelRendererTurbo(this, 16, 142, textureX, textureY); // pocket3
		bodyModel[7] = new ModelRendererTurbo(this, 16, 142, textureX, textureY); // pocket3
		bodyModel[8] = new ModelRendererTurbo(this, 16, 151, textureX, textureY); // pocket4
		bodyModel[9] = new ModelRendererTurbo(this, 16, 151, textureX, textureY); // pocket4
		bodyModel[10] = new ModelRendererTurbo(this, 56, 158, textureX, textureY); // armor1
		bodyModel[11] = new ModelRendererTurbo(this, 141, 108, textureX, textureY); // Box 0
		bodyModel[12] = new ModelRendererTurbo(this, 141, 108, textureX, textureY); // Box 1
		bodyModel[13] = new ModelRendererTurbo(this, 141, 108, textureX, textureY); // Box 2
		bodyModel[14] = new ModelRendererTurbo(this, 141, 128, textureX, textureY); // Box 4
		bodyModel[15] = new ModelRendererTurbo(this, 141, 128, textureX, textureY); // Box 5
		bodyModel[16] = new ModelRendererTurbo(this, 141, 128, textureX, textureY); // Box 6
		bodyModel[17] = new ModelRendererTurbo(this, 1, 172, textureX, textureY); // Box 7
		bodyModel[18] = new ModelRendererTurbo(this, 50, 109, textureX, textureY); // Box 8
		bodyModel[19] = new ModelRendererTurbo(this, 1, 158, textureX, textureY); // Box 9
		bodyModel[20] = new ModelRendererTurbo(this, 80, 172, textureX, textureY); // Box 18
		bodyModel[21] = new ModelRendererTurbo(this, 141, 137, textureX, textureY); // Box 19
		bodyModel[22] = new ModelRendererTurbo(this, 141, 137, textureX, textureY); // Box 21
		bodyModel[23] = new ModelRendererTurbo(this, 141, 137, textureX, textureY); // Box 22
		bodyModel[24] = new ModelRendererTurbo(this, 141, 137, textureX, textureY); // Box 23
		bodyModel[25] = new ModelRendererTurbo(this, 141, 137, textureX, textureY); // Box 24
		bodyModel[26] = new ModelRendererTurbo(this, 141, 137, textureX, textureY); // Box 25

		bodyModel[0].addShapeBox(-6.5F, -0.25F, -4.5F, 4, 4, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F); // armor1
		bodyModel[0].setRotationPoint(0F, 0F, 0F);

		bodyModel[1].addBox(-2F, 6.5F, 4F, 8, 10, 4, 0F); // backArmorPlate
		bodyModel[1].setRotationPoint(0F, 0F, 0F);

		bodyModel[2].addShapeBox(-8.1F, -0.1F, -4.1F, 16, 24, 8, 0F, 0F, 0F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0.2F, 0F, 0.2F, 0.2F, 0F, 0.2F, 0.2F, 0.2F, 0F, 0.2F, 0.2F); // body1
		bodyModel[2].setRotationPoint(0F, 0F, 0F);

		bodyModel[3].addBox(-7F, 3.5F, -5F, 14, 16, 1, 0F); // frontArmorPlate
		bodyModel[3].setRotationPoint(0F, 0F, 0F);

		bodyModel[4].addBox(1.25F, 17F, -6F, 5, 8, 2, 0F); // pocket1
		bodyModel[4].setRotationPoint(0F, 0F, 0F);

		bodyModel[5].addShapeBox(1.25F, 17F, -7F, 5, 3, 1, 0F, 0F, -0.2F, -0.5F, 0F, -0.2F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // pocket2
		bodyModel[5].setRotationPoint(0F, 0F, 0F);

		bodyModel[6].addBox(0.5F, 17F, 4F, 5, 6, 2, 0F); // pocket3
		bodyModel[6].setRotationPoint(0F, 0F, 0F);

		bodyModel[7].addBox(-5.5F, 17F, 4F, 5, 6, 2, 0F); // pocket3
		bodyModel[7].setRotationPoint(0F, 0F, 0F);

		bodyModel[8].addShapeBox(-5.5F, 17F, 6F, 5, 3, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.2F, -0.5F, 0F, -0.2F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // pocket4
		bodyModel[8].setRotationPoint(0F, 0F, 0F);

		bodyModel[9].addShapeBox(0.5F, 17F, 6F, 5, 3, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.2F, -0.5F, 0F, -0.2F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // pocket4
		bodyModel[9].setRotationPoint(0F, 0F, 0F);

		bodyModel[10].addShapeBox(2.5F, -0.25F, -4.5F, 4, 4, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F); // armor1
		bodyModel[10].setRotationPoint(0F, 0F, 0F);

		bodyModel[11].addShapeBox(-8F, 4.75F, 4F, 2, 13, 6, 0F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F); // Box 0
		bodyModel[11].setRotationPoint(0F, 0F, 0F);

		bodyModel[12].addShapeBox(-4F, 4.75F, 4F, 2, 13, 6, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F); // Box 1
		bodyModel[12].setRotationPoint(0F, 0F, 0F);

		bodyModel[13].addBox(-6F, 4.75F, 4F, 2, 13, 6, 0F); // Box 2
		bodyModel[13].setRotationPoint(0F, 0F, 0F);

		bodyModel[14].addShapeBox(-6F, 2.75F, 4F, 2, 2, 6, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 4
		bodyModel[14].setRotationPoint(0F, 0F, 0F);

		bodyModel[15].addShapeBox(-4F, 2.75F, 4F, 2, 2, 6, 0F, 0F, 0F, -1F, -1F, 0F, -2.5F, -1F, 0F, -2.5F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F); // Box 5
		bodyModel[15].setRotationPoint(0F, 0F, 0F);

		bodyModel[16].addShapeBox(-8F, 2.75F, 4F, 2, 2, 6, 0F, -1F, 0F, -2.5F, 0F, 0F, -1F, 0F, 0F, -1F, -1F, 0F, -2.5F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F); // Box 6
		bodyModel[16].setRotationPoint(0F, 0F, 0F);

		bodyModel[17].addBox(-7F, 3.5F, 4F, 14, 16, 1, 0F); // Box 7
		bodyModel[17].setRotationPoint(0F, 0F, 0F);

		bodyModel[18].addShapeBox(-2F, 2F, 4F, 8, 4, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 8
		bodyModel[18].setRotationPoint(0F, 0F, 0F);

		bodyModel[19].addBox(-8.5F, 19.5F, -4.5F, 17, 4, 9, 0F); // Box 9
		bodyModel[19].setRotationPoint(0F, 0F, 0F);

		bodyModel[20].addBox(-3F, -5.5F, 6.5F, 1, 10, 1, 0F); // Box 18
		bodyModel[20].setRotationPoint(0F, 0F, 0F);

		bodyModel[21].addShapeBox(-9F, 23.9F, -4.1F, 1, 4, 8, 0F, 0F, 0F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0.2F, 0F, 0F, 0.2F, 2F, 0.2F, 0F, -1.8F, 0.2F, 0F, -1.8F, 0.2F, 0.2F, 2F, 0.2F, 0.2F); // Box 19
		bodyModel[21].setRotationPoint(0F, 0F, 0F);

		bodyModel[22].addShapeBox(-9F, 25.9F, -4.1F, 1, 4, 8, 0F, 0F, 0F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0.2F, 0F, 0F, 0.2F, 2F, 0.2F, 0F, -1.8F, 0.2F, 0F, -1.8F, 0.2F, 0.2F, 2F, 0.2F, 0.2F); // Box 21
		bodyModel[22].setRotationPoint(0F, 0F, 0F);

		bodyModel[23].addShapeBox(-9F, 27.9F, -4.1F, 1, 4, 8, 0F, 0F, 0F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0.2F, 0F, 0F, 0.2F, 2F, 0.2F, 0F, -1.8F, 0.2F, 0F, -1.8F, 0.2F, 0.2F, 2F, 0.2F, 0.2F); // Box 22
		bodyModel[23].setRotationPoint(0F, 0F, 0F);

		bodyModel[24].addShapeBox(6.9F, 23.9F, -4.1F, 1, 4, 8, 0F, 0.2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.2F, 0.2F, 0F, 0.2F, -1.8F, 0.2F, 0F, 2F, 0.2F, 0F, 2F, 0.2F, 0.2F, -1.8F, 0.2F, 0.2F); // Box 23
		bodyModel[24].setRotationPoint(0F, 0F, 0F);

		bodyModel[25].addShapeBox(6.9F, 25.9F, -4.1F, 1, 4, 8, 0F, 0.2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.2F, 0.2F, 0F, 0.2F, -1.8F, 0.2F, 0F, 2F, 0.2F, 0F, 2F, 0.2F, 0.2F, -1.8F, 0.2F, 0.2F); // Box 24
		bodyModel[25].setRotationPoint(0F, 0F, 0F);

		bodyModel[26].addShapeBox(6.9F, 27.9F, -4.1F, 1, 4, 8, 0F, 0.2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.2F, 0.2F, 0F, 0.2F, -1.8F, 0.2F, 0F, 2F, 0.2F, 0F, 2F, 0.2F, 0.2F, -1.8F, 0.2F, 0.2F); // Box 25
		bodyModel[26].setRotationPoint(0F, 0F, 0F);


		leftArmModel = new ModelRendererTurbo[9];
		leftArmModel[0] = new ModelRendererTurbo(this, 108, 108, textureX, textureY); // leftArm
		leftArmModel[1] = new ModelRendererTurbo(this, 108, 133, textureX, textureY); // leftSleeve
		leftArmModel[2] = new ModelRendererTurbo(this, 32, 181, textureX, textureY); // Box 14
		leftArmModel[3] = new ModelRendererTurbo(this, 74, 151, textureX, textureY); // Box 40
		leftArmModel[4] = new ModelRendererTurbo(this, 108, 147, textureX, textureY); // Box 10
		leftArmModel[5] = new ModelRendererTurbo(this, 108, 147, textureX, textureY); // Box 11
		leftArmModel[6] = new ModelRendererTurbo(this, 31, 142, textureX, textureY); // Box 13
		leftArmModel[7] = new ModelRendererTurbo(this, 31, 142, textureX, textureY); // Box 15
		leftArmModel[8] = new ModelRendererTurbo(this, 63, 172, textureX, textureY); // Box 17

		leftArmModel[0].addShapeBox(-2.1F, -4.1F, -4.1F, 8, 16, 8, 0F, 0F, 0F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0.2F, 0F, 0.2F, 0.2F, 0F, 0.2F, 0.2F, 0.2F, 0F, 0.2F, 0.2F); // leftArm
		leftArmModel[0].setRotationPoint(0F, 0F, 0F);

		leftArmModel[1].addShapeBox(-2.2F, 12.5F, -4.2F, 8, 5, 8, 0F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F); // leftSleeve
		leftArmModel[1].setRotationPoint(0F, 0F, 0F);

		leftArmModel[2].addShapeBox(-1.5F, 12.5F, -4.5F, 4, 5, 1, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 14
		leftArmModel[2].setRotationPoint(0F, 0F, 0F);

		leftArmModel[3].addShapeBox(-2.2F, 18.2F, -4.2F, 8, 2, 8, 0F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F); // Box 40
		leftArmModel[3].setRotationPoint(0F, 0F, 0F);

		leftArmModel[4].addShapeBox(-2.5F, 11.5F, -4.5F, 9, 1, 9, 0F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F); // Box 10
		leftArmModel[4].setRotationPoint(0F, 0F, 0F);

		leftArmModel[5].addShapeBox(-2.5F, 17.5F, -4.5F, 9, 1, 9, 0F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F); // Box 11
		leftArmModel[5].setRotationPoint(0F, 0F, 0F);

		leftArmModel[6].addShapeBox(-2.2F, -4.8F, -4.2F, 8, 4, 8, 0F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0F, 1F, 1F, 0F, 1F, 1F, 0F, 1F, 0F, 0F, 1F); // Box 13
		leftArmModel[6].setRotationPoint(0F, 0F, 0F);

		leftArmModel[7].addShapeBox(-2.2F, -0.8F, -4.2F, 8, 4, 8, 0F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0F, 1F, 1F, 0F, 1F, 1F, 0F, 1F, 0F, 0F, 1F); // Box 15
		leftArmModel[7].setRotationPoint(0F, 0F, 0F);

		leftArmModel[8].addBox(5.5F, 5F, -3.5F, 1, 7, 7, 0F); // Box 17
		leftArmModel[8].setRotationPoint(0F, 0F, 0F);


		rightArmModel = new ModelRendererTurbo[6];
		rightArmModel[0] = new ModelRendererTurbo(this, 75, 109, textureX, textureY); // rightArm
		rightArmModel[1] = new ModelRendererTurbo(this, 108, 158, textureX, textureY); // Box 39
		rightArmModel[2] = new ModelRendererTurbo(this, 31, 142, textureX, textureY); // Box 12
		rightArmModel[3] = new ModelRendererTurbo(this, 63, 172, textureX, textureY); // Box 16
		rightArmModel[4] = new ModelRendererTurbo(this, 74, 138, textureX, textureY); // Box 46
		rightArmModel[5] = new ModelRendererTurbo(this, 31, 142, textureX, textureY); // Box 104

		rightArmModel[0].addShapeBox(-6.1F, -4.1F, -4.1F, 8, 20, 8, 0F, 0F, 0F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0.2F, 0F, 0.2F, 0.2F, 0F, 0.2F, 0.2F, 0.2F, 0F, 0.2F, 0.2F); // rightArm
		rightArmModel[0].setRotationPoint(0F, 0F, 0F);

		rightArmModel[1].addShapeBox(-6.2F, 17.2F, -4.2F, 8, 3, 8, 0F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F); // Box 39
		rightArmModel[1].setRotationPoint(0F, 0F, 0F);

		rightArmModel[2].addShapeBox(-6.2F, -4.8F, -4.2F, 8, 4, 8, 0F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F, 1F, 0F, 1F, 0.4F, 0F, 1F, 0.4F, 0F, 1F, 1F, 0F, 1F); // Box 12
		rightArmModel[2].setRotationPoint(0F, 0F, 0F);

		rightArmModel[3].addBox(-6.5F, 5F, -3.5F, 1, 7, 7, 0F); // Box 16
		rightArmModel[3].setRotationPoint(0F, 0F, 0F);

		rightArmModel[4].addShapeBox(-6.2F, 13.2F, -4.2F, 8, 4, 8, 0F, 0.5F, 0F, 0.5F, 0.4F, 0F, 0.5F, 0.4F, 0F, 0.5F, 0.5F, 0F, 0.5F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F); // Box 46
		rightArmModel[4].setRotationPoint(0F, 0F, 0F);

		rightArmModel[5].addShapeBox(-6.2F, -0.8F, -4.2F, 8, 4, 8, 0F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F, 1F, 0F, 1F, 0.4F, 0F, 1F, 0.4F, 0F, 1F, 1F, 0F, 1F); // Box 104
		rightArmModel[5].setRotationPoint(0F, 0F, 0F);


		skirtFrontModel = new ModelRendererTurbo[6];
		skirtFrontModel[0] = new ModelRendererTurbo(this, 107, 181, textureX, textureY); // Box 26
		skirtFrontModel[1] = new ModelRendererTurbo(this, 107, 187, textureX, textureY); // Box 33
		skirtFrontModel[2] = new ModelRendererTurbo(this, 107, 181, textureX, textureY); // Box 90
		skirtFrontModel[3] = new ModelRendererTurbo(this, 107, 187, textureX, textureY); // Box 91
		skirtFrontModel[4] = new ModelRendererTurbo(this, 107, 181, textureX, textureY); // Box 92
		skirtFrontModel[5] = new ModelRendererTurbo(this, 107, 187, textureX, textureY); // Box 93

		skirtFrontModel[0].addShapeBox(-7.5F, -0.5F, -4.5F, 15, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 26
		skirtFrontModel[0].setRotationPoint(0F, 0F, 0F);

		skirtFrontModel[1].addShapeBox(-7.5F, 3.5F, -5.5F, 15, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0.25F, -1F, 0F, 0.25F, -1F, 0F, -0.25F, -1F, 0F, -0.25F); // Box 33
		skirtFrontModel[1].setRotationPoint(0F, 0F, 0F);

		skirtFrontModel[2].addShapeBox(-7.5F, 1.5F, -4.5F, 15, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 90
		skirtFrontModel[2].setRotationPoint(0F, 0F, 0F);

		skirtFrontModel[3].addShapeBox(-7.5F, 5.5F, -5.5F, 15, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0.25F, -1F, 0F, 0.25F, -1F, 0F, -0.25F, -1F, 0F, -0.25F); // Box 91
		skirtFrontModel[3].setRotationPoint(0F, 0F, 0F);

		skirtFrontModel[4].addShapeBox(-7.5F, 3.5F, -4.5F, 15, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 92
		skirtFrontModel[4].setRotationPoint(0F, 0F, 0F);

		skirtFrontModel[5].addShapeBox(-7.5F, 7.5F, -5.5F, 15, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0.25F, -1F, 0F, 0.25F, -1F, 0F, -0.25F, -1F, 0F, -0.25F); // Box 93
		skirtFrontModel[5].setRotationPoint(0F, 0F, 0F);


		skirtRearModel = new ModelRendererTurbo[6];
		skirtRearModel[0] = new ModelRendererTurbo(this, 107, 181, textureX, textureY); // Box 29
		skirtRearModel[1] = new ModelRendererTurbo(this, 107, 187, textureX, textureY); // Box 32
		skirtRearModel[2] = new ModelRendererTurbo(this, 107, 187, textureX, textureY); // Box 94
		skirtRearModel[3] = new ModelRendererTurbo(this, 107, 181, textureX, textureY); // Box 95
		skirtRearModel[4] = new ModelRendererTurbo(this, 107, 187, textureX, textureY); // Box 96
		skirtRearModel[5] = new ModelRendererTurbo(this, 107, 181, textureX, textureY); // Box 97

		skirtRearModel[0].addShapeBox(-7.5F, -0.5F, 3.5F, 15, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F, 1F); // Box 29
		skirtRearModel[0].setRotationPoint(0F, 0F, 0F);

		skirtRearModel[1].addShapeBox(-7.5F, 3.5F, 4.5F, 15, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -0.25F, -1F, 0F, -0.25F, -1F, 0F, 0.25F, -1F, 0F, 0.25F); // Box 32
		skirtRearModel[1].setRotationPoint(0F, 0F, 0F);

		skirtRearModel[2].addShapeBox(-7.5F, 5.5F, 4.5F, 15, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -0.25F, -1F, 0F, -0.25F, -1F, 0F, 0.25F, -1F, 0F, 0.25F); // Box 94
		skirtRearModel[2].setRotationPoint(0F, 0F, 0F);

		skirtRearModel[3].addShapeBox(-7.5F, 1.5F, 3.5F, 15, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F, 1F); // Box 95
		skirtRearModel[3].setRotationPoint(0F, 0F, 0F);

		skirtRearModel[4].addShapeBox(-7.5F, 7.5F, 4.5F, 15, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -0.25F, -1F, 0F, -0.25F, -1F, 0F, 0.25F, -1F, 0F, 0.25F); // Box 96
		skirtRearModel[4].setRotationPoint(0F, 0F, 0F);

		skirtRearModel[5].addShapeBox(-7.5F, 3.5F, 3.5F, 15, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F, 1F); // Box 97
		skirtRearModel[5].setRotationPoint(0F, 0F, 0F);


	}
}