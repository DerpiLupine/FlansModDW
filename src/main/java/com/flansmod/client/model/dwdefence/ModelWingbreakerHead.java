package com.flansmod.client.model.dwdefence;

import com.flansmod.client.model.ModelCustomArmour;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.common.vector.Vector3f;

public class ModelWingbreakerHead extends ModelCustomArmour
{
	public ModelWingbreakerHead()
	{
		int textureX = 256;
		int textureY = 128;

		headModel = new ModelRendererTurbo[8];
		headModel[0] = new ModelRendererTurbo(this, 1, 67, textureX, textureY); // goggles1
		headModel[1] = new ModelRendererTurbo(this, 2, 67, textureX, textureY); // goggles2
		headModel[2] = new ModelRendererTurbo(this, 2, 67, textureX, textureY); // goggles3
		headModel[3] = new ModelRendererTurbo(this, 68, 45, textureX, textureY); // headsetPart1
		headModel[4] = new ModelRendererTurbo(this, 82, 40, textureX, textureY); // headsetPart2
		headModel[5] = new ModelRendererTurbo(this, 66, 40, textureX, textureY); // headsetPart3
		headModel[6] = new ModelRendererTurbo(this, 59, 47, textureX, textureY); // headsetPart4
		headModel[7] = new ModelRendererTurbo(this, 68, 40, textureX, textureY); // headsetMicPart

		headModel[0].addShapeBox(-9.2F, -14F, -9.2F, 18, 2, 18, 0F,0F, 0F, 0F,0.5F, 0F, 0F,0.5F, 0F, 0.5F,0F, 0F, 0.5F,0F, 0.5F, 0F,0.5F, 0.5F, 0F,0.5F, 0.5F, 0.5F,0F, 0.5F, 0.5F); // goggles1

		headModel[1].addBox(-7F, -14.8F, -10F, 6, 4, 2, 0F); // goggles2

		headModel[2].addBox(1F, -14.8F, -10F, 6, 4, 2, 0F); // goggles3

		headModel[3].addBox(8.1F, -15F, 2.5F, 2, 4, 3, 0F); // headsetPart1

		headModel[4].addBox(9F, -14F, 3.5F, 1, 11, 1, 0F); // headsetPart2

		headModel[5].addBox(9F, -4F, -8.5F, 1, 1, 13, 0F); // headsetPart3

		headModel[6].addBox(7F, -4F, -9.5F, 3, 1, 1, 0F); // headsetPart4

		headModel[7].addBox(4F, -4.5F, -10F, 3, 2, 2, 0F); // headsetMicPart
	}
}