package com.flansmod.client.model.dwdefence;

import com.flansmod.client.model.ModelCustomArmour;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.common.vector.Vector3f;

public class ModelAssaultLegs extends ModelCustomArmour
{
	int textureX = 512;
	int textureY = 256;

	public ModelAssaultLegs()
	{
		leftLegModel = new ModelRendererTurbo[11];
		leftLegModel[0] = new ModelRendererTurbo(this, 75, 182, textureX, textureY); // kneeArmor
		leftLegModel[1] = new ModelRendererTurbo(this, 90, 182, textureX, textureY); // kneeArmor2
		leftLegModel[2] = new ModelRendererTurbo(this, 90, 182, textureX, textureY); // kneeArmor2
		leftLegModel[3] = new ModelRendererTurbo(this, 75, 170, textureX, textureY); // kneeArmor3
		leftLegModel[4] = new ModelRendererTurbo(this, 75, 170, textureX, textureY); // kneeArmor3
		leftLegModel[5] = new ModelRendererTurbo(this, 1, 170, textureX, textureY); // leftPants
		leftLegModel[6] = new ModelRendererTurbo(this, 38, 183, textureX, textureY); // leftPocket
		leftLegModel[7] = new ModelRendererTurbo(this, 38, 183, textureX, textureY); // leftPocket
		leftLegModel[8] = new ModelRendererTurbo(this, 51, 183, textureX, textureY); // leftPocket2
		leftLegModel[9] = new ModelRendererTurbo(this, 51, 183, textureX, textureY); // leftPocket2
		leftLegModel[10] = new ModelRendererTurbo(this, 38, 170, textureX, textureY); // pocketStrap

		leftLegModel[0].addBox(-3F, 7F, -5.5F, 6, 7, 1, 0F); // kneeArmor
		leftLegModel[0].setRotationPoint(0F, 0F, 0F);

		leftLegModel[1].addShapeBox(-3F, 14F, -5.5F, 6, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // kneeArmor2
		leftLegModel[1].setRotationPoint(0F, 0F, 0F);

		leftLegModel[2].addShapeBox(-3F, 6F, -5.5F, 6, 1, 1, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // kneeArmor2
		leftLegModel[2].setRotationPoint(0F, 0F, 0F);

		leftLegModel[3].addShapeBox(-4.6F, 7.5F, -4.8F, 9, 2, 9, 0F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F); // kneeArmor3
		leftLegModel[3].setRotationPoint(0F, 0F, 0F);

		leftLegModel[4].addShapeBox(-4.6F, 11.5F, -4.8F, 9, 2, 9, 0F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F); // kneeArmor3
		leftLegModel[4].setRotationPoint(0F, 0F, 0F);

		leftLegModel[5].addBox(-4.5F, -1.2F, -4.5F, 9, 20, 9, 0F); // leftPants
		leftLegModel[5].setRotationPoint(0F, 0F, 0F);

		leftLegModel[6].addBox(4.5F, -0.199999999999999F, 0F, 2, 6, 4, 0F); // leftPocket
		leftLegModel[6].setRotationPoint(0F, 0F, 0F);

		leftLegModel[7].addBox(4.5F, -0.199999999999999F, -4.5F, 2, 6, 4, 0F); // leftPocket
		leftLegModel[7].setRotationPoint(0F, 0F, 0F);

		leftLegModel[8].addShapeBox(6.5F, -0.199999999999999F, -4.5F, 1, 2, 4, 0F, 0F, 0F, 0F, -0.5F, -0.2F, 0F, -0.5F, -0.2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F); // leftPocket2
		leftLegModel[8].setRotationPoint(0F, 0F, 0F);

		leftLegModel[9].addShapeBox(6.5F, -0.199999999999999F, 0F, 1, 2, 4, 0F, 0F, 0F, 0F, -0.5F, -0.2F, 0F, -0.5F, -0.2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F); // leftPocket2
		leftLegModel[9].setRotationPoint(0F, 0F, 0F);

		leftLegModel[10].addShapeBox(-4.6F, 1F, -4.8F, 9, 3, 9, 0F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F); // pocketStrap
		leftLegModel[10].setRotationPoint(0F, 0F, 0F);


		rightLegModel = new ModelRendererTurbo[10];
		rightLegModel[0] = new ModelRendererTurbo(this, 38, 213, textureX, textureY); // rightPocket
		rightLegModel[1] = new ModelRendererTurbo(this, 75, 203, textureX, textureY); // Box 1
		rightLegModel[2] = new ModelRendererTurbo(this, 53, 213, textureX, textureY); // rightPocket2
		rightLegModel[3] = new ModelRendererTurbo(this, 75, 182, textureX, textureY); // kneeArmor
		rightLegModel[4] = new ModelRendererTurbo(this, 90, 182, textureX, textureY); // kneeArmor2
		rightLegModel[5] = new ModelRendererTurbo(this, 90, 182, textureX, textureY); // kneeArmor2
		rightLegModel[6] = new ModelRendererTurbo(this, 75, 170, textureX, textureY); // kneeArmor3
		rightLegModel[7] = new ModelRendererTurbo(this, 75, 170, textureX, textureY); // kneeArmor3
		rightLegModel[8] = new ModelRendererTurbo(this, 1, 200, textureX, textureY); // rightPants
		rightLegModel[9] = new ModelRendererTurbo(this, 38, 200, textureX, textureY); // rightStrap

		rightLegModel[0].addBox(-6.5F, -0.199999999999999F, -3F, 2, 8, 5, 0F); // rightPocket
		rightLegModel[0].setRotationPoint(0F, 0F, 0F);

		rightLegModel[1].addBox(-5F, 1F, -3.5F, 1, 3, 6, 0F); // Box 1
		rightLegModel[1].setRotationPoint(0F, 0F, 0F);

		rightLegModel[2].addShapeBox(-7.5F, -0.199999999999999F, -3F, 1, 3, 5, 0F, -0.5F, -0.2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -0.2F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F); // rightPocket2
		rightLegModel[2].setRotationPoint(0F, 0F, 0F);

		rightLegModel[3].addBox(-3F, 7F, -5.5F, 6, 7, 1, 0F); // kneeArmor
		rightLegModel[3].setRotationPoint(0F, 0F, 0F);

		rightLegModel[4].addShapeBox(-3F, 6F, -5.5F, 6, 1, 1, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // kneeArmor2
		rightLegModel[4].setRotationPoint(0F, 0F, 0F);

		rightLegModel[5].addShapeBox(-3F, 14F, -5.5F, 6, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // kneeArmor2
		rightLegModel[5].setRotationPoint(0F, 0F, 0F);

		rightLegModel[6].addShapeBox(-4.8F, 11.5F, -4.8F, 9, 2, 9, 0F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F); // kneeArmor3
		rightLegModel[6].setRotationPoint(0F, 0F, 0F);

		rightLegModel[7].addShapeBox(-4.8F, 7.5F, -4.8F, 9, 2, 9, 0F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F); // kneeArmor3
		rightLegModel[7].setRotationPoint(0F, 0F, 0F);

		rightLegModel[8].addBox(-4.5F, -1.2F, -4.5F, 9, 20, 9, 0F); // rightPants
		rightLegModel[8].setRotationPoint(0F, 0F, 0F);

		rightLegModel[9].addShapeBox(-4.8F, 1F, -4.8F, 9, 3, 9, 0F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F); // rightStrap
		rightLegModel[9].setRotationPoint(0F, 0F, 0F);
	}
}