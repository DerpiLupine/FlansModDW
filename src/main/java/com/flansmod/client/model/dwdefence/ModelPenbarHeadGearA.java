package com.flansmod.client.model.dwdefence;

import com.flansmod.client.model.ModelCustomArmour;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelPenbarHeadGearA extends ModelCustomArmour
{
	int textureX = 512;
	int textureY = 256;

	public ModelPenbarHeadGearA()
	{
		headModel = new ModelRendererTurbo[7];
		headModel[0] = new ModelRendererTurbo(this, 1, 113, textureX, textureY); // Box 1
		headModel[1] = new ModelRendererTurbo(this, 1, 138, textureX, textureY); // Box 2
		headModel[2] = new ModelRendererTurbo(this, 1, 159, textureX, textureY); // Box 3
		headModel[3] = new ModelRendererTurbo(this, 1, 177, textureX, textureY); // Box 5
		headModel[4] = new ModelRendererTurbo(this, 1, 177, textureX, textureY); // Box 6
		headModel[5] = new ModelRendererTurbo(this, 1, 166, textureX, textureY); // Box 7
		headModel[6] = new ModelRendererTurbo(this, 1, 182, textureX, textureY); // Box 8

		headModel[0].addBox(-9F, -16F, -9F, 18, 6, 18, 0F); // Box 1
		//headModel[0].setRotationPoint();

		headModel[1].addShapeBox(-9F, -18F, -9F, 18, 2, 18, 0F, -2F, 0F, -2F, -2F, 0F, -2F, -2F, 0F, -2F, -2F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 2
		//headModel[1].setRotationPoint();

		headModel[2].addShapeBox(-9F, -12F, -13F, 18, 2, 4, 0F, -2F, -2F, 0F, -2F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 1F, 0F, -2F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 3
		//headModel[2].setRotationPoint();

		headModel[3].addBox(-7F, -8.5F, -9F, 6, 3, 1, 0F); // Box 5
		//headModel[0].setRotationPoint();

		headModel[4].addBox(1F, -8.5F, -9F, 6, 3, 1, 0F); // Box 6
		//headModel[1].setRotationPoint();

		headModel[5].addBox(-8.5F, -7.5F, -9F, 17, 1, 9, 0F); // Box 7
		//headModel[2].setRotationPoint();

		headModel[6].addBox(2F, -3.5F, -14F, 1, 1, 6, 0F); // Box 8
		//eadModel[3].setRotationPoint(;
	}
}