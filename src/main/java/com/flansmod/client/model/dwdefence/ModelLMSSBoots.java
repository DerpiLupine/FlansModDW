package com.flansmod.client.model.dwdefence; //Path where the model is located

import com.flansmod.client.model.ModelCustomArmour;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelLMSSBoots extends ModelCustomArmour //Same as Filename
{
	int textureX = 512;
	int textureY = 256;

	public ModelLMSSBoots() //Same as Filename
	{
		leftLegModel = new ModelRendererTurbo[2];
		leftLegModel[0] = new ModelRendererTurbo(this, 112, 200, textureX, textureY); // leftBoot
		leftLegModel[1] = new ModelRendererTurbo(this, 112, 220, textureX, textureY); // leftBoot3

		leftLegModel[0].addShapeBox(-4.3F, 14.2F, -4.7F, 9, 10, 9, 0F, 0.4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.4F, 0.4F, 0F, 0.4F, 0.4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.4F, 0.4F, 0F, 0.4F); // leftBoot
		leftLegModel[0].setRotationPoint(0F, 0F, 0F);

		leftLegModel[1].addShapeBox(-3.3F, 20.2F, -8.1F, 7, 4, 3, 0F, 0.4F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0.4F, 1.4F, 0F, 0.4F, 0.4F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0.4F, 1.4F, 0F, 0.4F); // leftBoot3
		leftLegModel[1].setRotationPoint(0F, 0F, 0F);


		rightLegModel = new ModelRendererTurbo[2];
		rightLegModel[0] = new ModelRendererTurbo(this, 112, 200, textureX, textureY); // rightBoot
		rightLegModel[1] = new ModelRendererTurbo(this, 112, 220, textureX, textureY); // rightBoot3

		rightLegModel[0].addShapeBox(-4.3F, 14.2F, -4.7F, 9, 10, 9, 0F, 0.4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.4F, 0.4F, 0F, 0.4F, 0.4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.4F, 0.4F, 0F, 0.4F); // rightBoot
		rightLegModel[0].setRotationPoint(0F, 0F, 0F);

		rightLegModel[1].addShapeBox(-3.3F, 20.2F, -8.1F, 7, 4, 3, 0F, 0.4F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0.4F, 1.4F, 0F, 0.4F, 0.4F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0.4F, 1.4F, 0F, 0.4F); // rightBoot3
		rightLegModel[1].setRotationPoint(0F, 0F, 0F);


	}
}