package com.flansmod.client.model.dwdefence; //Path where the model is located

import com.flansmod.client.model.ModelCustomArmour;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelValkyrieLegs extends ModelCustomArmour //Same as Filename
{
	int textureX = 512;
	int textureY = 256;

	public ModelValkyrieLegs() //Same as Filename
	{
		leftLegModel = new ModelRendererTurbo[4];
		leftLegModel[0] = new ModelRendererTurbo(this, 1, 183, textureX, textureY); // Box 0
		leftLegModel[1] = new ModelRendererTurbo(this, 74, 192, textureX, textureY); // Box 3
		leftLegModel[2] = new ModelRendererTurbo(this, 74, 198, textureX, textureY); // Box 10
		leftLegModel[3] = new ModelRendererTurbo(this, 74, 198, textureX, textureY); // Box 14

		leftLegModel[0].addShapeBox(-4.1F, -0.100000000000001F, -4.1F, 8, 20, 8, 0F, 0F, 0F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0.2F, 0F, 0.2F, 0.2F, 0F, 0.2F, 0.2F, 0.2F, 0F, 0.2F, 0.2F); // Box 0
		leftLegModel[0].setRotationPoint(0F, 0F, 0F);

		leftLegModel[1].addBox(-3F, 9F, -4.5F, 6, 4, 1, 0F); // Box 3
		leftLegModel[1].setRotationPoint(0F, 0F, 0F);

		leftLegModel[2].addShapeBox(-3F, 8F, -4.5F, 6, 1, 1, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 10
		leftLegModel[2].setRotationPoint(0F, 0F, 0F);

		leftLegModel[3].addShapeBox(-3F, 13F, -4.5F, 6, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 14
		leftLegModel[3].setRotationPoint(0F, 0F, 0F);


		rightLegModel = new ModelRendererTurbo[4];
		rightLegModel[0] = new ModelRendererTurbo(this, 41, 183, textureX, textureY); // Box 1
		rightLegModel[1] = new ModelRendererTurbo(this, 74, 192, textureX, textureY); // Box 2
		rightLegModel[2] = new ModelRendererTurbo(this, 74, 198, textureX, textureY); // Box 11
		rightLegModel[3] = new ModelRendererTurbo(this, 74, 198, textureX, textureY); // Box 15

		rightLegModel[0].addShapeBox(-4.1F, -0.100000000000001F, -4.1F, 8, 20, 8, 0F, 0F, 0F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0.2F, 0F, 0.2F, 0.2F, 0F, 0.2F, 0.2F, 0.2F, 0F, 0.2F, 0.2F); // Box 1
		rightLegModel[0].setRotationPoint(0F, 0F, 0F);

		rightLegModel[1].addBox(-3F, 9F, -4.5F, 6, 4, 1, 0F); // Box 2
		rightLegModel[1].setRotationPoint(0F, 0F, 0F);

		rightLegModel[2].addShapeBox(-3F, 8F, -4.5F, 6, 1, 1, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 11
		rightLegModel[2].setRotationPoint(0F, 0F, 0F);

		rightLegModel[3].addShapeBox(-3F, 13F, -4.5F, 6, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 15
		rightLegModel[3].setRotationPoint(0F, 0F, 0F);


		skirtRearModel = new ModelRendererTurbo[4];
		skirtRearModel[0] = new ModelRendererTurbo(this, 74, 183, textureX, textureY); // Box 4
		skirtRearModel[1] = new ModelRendererTurbo(this, 74, 189, textureX, textureY); // Box 5
		skirtRearModel[2] = new ModelRendererTurbo(this, 74, 189, textureX, textureY); // Box 8
		skirtRearModel[3] = new ModelRendererTurbo(this, 74, 183, textureX, textureY); // Box 9

		skirtRearModel[0].addShapeBox(-7F, 3F, 4.3F, 14, 4, 1, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 4
		skirtRearModel[0].setRotationPoint(0F, 0F, 0F);

		skirtRearModel[1].addShapeBox(-7F, 7F, 4.3F, 14, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, -0.5F, -1F, 0F, -0.5F); // Box 5
		skirtRearModel[1].setRotationPoint(0F, 0F, 0F);

		skirtRearModel[2].addShapeBox(-7F, 4F, 4.5F, 14, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, -0.5F, -1F, 0F, -0.5F); // Box 8
		skirtRearModel[2].setRotationPoint(0F, 0F, 0F);

		skirtRearModel[3].addShapeBox(-7F, 0F, 4.5F, 14, 4, 1, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 9
		skirtRearModel[3].setRotationPoint(0F, 0F, 0F);


	}
}