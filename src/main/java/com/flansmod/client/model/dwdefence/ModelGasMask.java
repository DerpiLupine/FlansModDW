package com.flansmod.client.model.dwdefence;

import com.flansmod.client.model.ModelCustomArmour;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelGasMask extends ModelCustomArmour
{
	int textureX = 512;
	int textureY = 256;

	public ModelGasMask()
	{
		headModel = new ModelRendererTurbo[12];
		headModel[0] = new ModelRendererTurbo(this, 74, 148, textureX, textureY); // Box 4
		headModel[1] = new ModelRendererTurbo(this, 74, 158, textureX, textureY); // Box 5
		headModel[2] = new ModelRendererTurbo(this, 114, 164, textureX, textureY); // Box 6
		headModel[3] = new ModelRendererTurbo(this, 114, 164, textureX, textureY); // Box 7
		headModel[4] = new ModelRendererTurbo(this, 103, 158, textureX, textureY); // Box 9
		headModel[5] = new ModelRendererTurbo(this, 113, 138, textureX, textureY); // Box 12
		headModel[6] = new ModelRendererTurbo(this, 113, 148, textureX, textureY); // Box 13
		headModel[7] = new ModelRendererTurbo(this, 113, 138, textureX, textureY); // Box 14
		headModel[8] = new ModelRendererTurbo(this, 74, 138, textureX, textureY); // Box 15
		headModel[9] = new ModelRendererTurbo(this, 103, 164, textureX, textureY); // Box 16
		headModel[10] = new ModelRendererTurbo(this, 74, 117, textureX, textureY); // Box 17
		headModel[11] = new ModelRendererTurbo(this, 74, 138, textureX, textureY); // Box 19

		headModel[0].addShapeBox(-9F, -13F, -9F, 18, 8, 1, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 4
		//headModel[0].setRotationPoint();

		headModel[1].addBox(-4F, -6F, -14F, 8, 6, 6, 0F); // Box 5
		//headModel[1].setRotationPoint();

		headModel[2].addShapeBox(4F, -5F, -9F, 4, 5, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F); // Box 6
		//headModel[2].setRotationPoint();

		headModel[3].addShapeBox(-8F, -5F, -9F, 4, 5, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F); // Box 7
		//headModel[3].setRotationPoint();

		headModel[4].addBox(-1.5F, 0F, -13F, 3, 2, 3, 0F); // Box 9
		//headModel[4].setRotationPoint();

		headModel[5].addShapeBox(4F, -5F, -16F, 4, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 12
		//headModel[5].setRotationPoint();

		headModel[6].addBox(4F, -3F, -16F, 4, 3, 7, 0F); // Box 13
		//headModel[6].setRotationPoint();

		headModel[7].addShapeBox(4F, 0F, -16F, 4, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 14
		//headModel[7].setRotationPoint();

		headModel[8].addBox(-9F, -13F, -8F, 1, 8, 1, 0F); // Box 15
		//headModel[8].setRotationPoint();

		headModel[9].addBox(-5.5F, -4F, -13F, 2, 3, 3, 0F); // Box 16
		//headModel[9].setRotationPoint();

		headModel[10].addBox(-9F, -11F, -7F, 18, 4, 16, 0F); // Box 17
		//headModel[10].setRotationPoint();

		headModel[11].addBox(8F, -13F, -8F, 1, 8, 1, 0F); // Box 19
		//headModel[11].setRotationPoint();
	}
}