package com.flansmod.client.model.dwdefence; //Path where the model is located

import com.flansmod.client.model.ModelCustomArmour;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelChamberlainLegs extends ModelCustomArmour //Same as Filename
{
	int textureX = 512;
	int textureY = 256;

	public ModelChamberlainLegs() //Same as Filename
	{
		leftLegModel = new ModelRendererTurbo[11];
		leftLegModel[0] = new ModelRendererTurbo(this, 75, 189, textureX, textureY); // kneeArmor3
		leftLegModel[1] = new ModelRendererTurbo(this, 1, 189, textureX, textureY); // leftPants
		leftLegModel[2] = new ModelRendererTurbo(this, 1, 219, textureX, textureY); // leftPocket
		leftLegModel[3] = new ModelRendererTurbo(this, 28, 223, textureX, textureY); // leftPocket2
		leftLegModel[4] = new ModelRendererTurbo(this, 75, 207, textureX, textureY); // pocketStrap
		leftLegModel[5] = new ModelRendererTurbo(this, 51, 235, textureX, textureY); // Box 1
		leftLegModel[6] = new ModelRendererTurbo(this, 51, 219, textureX, textureY); // Box 2
		leftLegModel[7] = new ModelRendererTurbo(this, 51, 232, textureX, textureY); // Box 3
		leftLegModel[8] = new ModelRendererTurbo(this, 75, 225, textureX, textureY); // Box 1
		leftLegModel[9] = new ModelRendererTurbo(this, 75, 220, textureX, textureY); // Box 2
		leftLegModel[10] = new ModelRendererTurbo(this, 108, 170, textureX, textureY); // Box 6

		leftLegModel[0].addShapeBox(-4.6F, 10.5F, -4.8F, 9, 9, 8, 0F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F); // kneeArmor3
		leftLegModel[0].setRotationPoint(0F, 0F, 0F);

		leftLegModel[1].addBox(-4.5F, -0.199999999999999F, -4.5F, 9, 20, 9, 0F); // leftPants
		leftLegModel[1].setRotationPoint(0F, 0F, 0F);

		leftLegModel[2].addShapeBox(4.5F, -0.5F, -4.5F, 3, 8, 10, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 1F, -1F, 0F, 1F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F); // leftPocket
		leftLegModel[2].setRotationPoint(0F, 0F, 0F);

		leftLegModel[3].addShapeBox(7.5F, -0.5F, -4.5F, 1, 4, 10, 0F, 0F, 0F, 1F, -0.5F, -0.2F, 1F, -0.5F, 0.8F, -1F, 0F, 1F, -1F, 0F, 0F, 0.5F, -0.5F, 0F, 0.5F, -0.5F, -1F, -0.5F, 0F, -1F, -0.5F); // leftPocket2
		leftLegModel[3].setRotationPoint(0F, 0F, 0F);

		leftLegModel[4].addShapeBox(-4.6F, 2F, -4.8F, 9, 3, 9, 0F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F); // pocketStrap
		leftLegModel[4].setRotationPoint(0F, 0F, 0F);

		leftLegModel[5].addShapeBox(-4F, 19F, -5F, 8, 1, 1, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 1
		leftLegModel[5].setRotationPoint(0F, 0F, 0F);

		leftLegModel[6].addShapeBox(-4F, 8F, -5F, 8, 11, 1, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 2
		leftLegModel[6].setRotationPoint(0F, 0F, 0F);

		leftLegModel[7].addShapeBox(-4F, 7F, -5F, 8, 1, 1, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 3
		leftLegModel[7].setRotationPoint(0F, 0F, 0F);

		leftLegModel[8].addShapeBox(-4.6F, 15.5F, 3.6F, 9, 3, 1, 0F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F); // Box 1
		leftLegModel[8].setRotationPoint(0F, 0F, 0F);

		leftLegModel[9].addShapeBox(-4.6F, 10.5F, 3.6F, 9, 3, 1, 0F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F); // Box 2
		leftLegModel[9].setRotationPoint(0F, 0F, 0F);

		leftLegModel[10].addShapeBox(-4.6F, 19.5F, -4.8F, 9, 1, 9, 0F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0F, 0.5F, 0.9F, 0F, 0.5F, 0.9F, 0F, 0.9F, 0F, 0F, 0.9F); // Box 6
		leftLegModel[10].setRotationPoint(0F, 0F, 0F);


		rightLegModel = new ModelRendererTurbo[9];
		rightLegModel[0] = new ModelRendererTurbo(this, 51, 219, textureX, textureY); // kneeArmor
		rightLegModel[1] = new ModelRendererTurbo(this, 51, 232, textureX, textureY); // kneeArmor2
		rightLegModel[2] = new ModelRendererTurbo(this, 75, 189, textureX, textureY); // kneeArmor3
		rightLegModel[3] = new ModelRendererTurbo(this, 38, 189, textureX, textureY); // rightPants
		rightLegModel[4] = new ModelRendererTurbo(this, 75, 207, textureX, textureY); // rightStrap
		rightLegModel[5] = new ModelRendererTurbo(this, 51, 235, textureX, textureY); // Box 0
		rightLegModel[6] = new ModelRendererTurbo(this, 75, 225, textureX, textureY); // Box 0
		rightLegModel[7] = new ModelRendererTurbo(this, 75, 220, textureX, textureY); // Box 3
		rightLegModel[8] = new ModelRendererTurbo(this, 108, 170, textureX, textureY); // Box 7

		rightLegModel[0].addShapeBox(-4F, 8F, -5F, 8, 11, 1, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // kneeArmor
		rightLegModel[0].setRotationPoint(0F, 0F, 0F);

		rightLegModel[1].addShapeBox(-4F, 7F, -5F, 8, 1, 1, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // kneeArmor2
		rightLegModel[1].setRotationPoint(0F, 0F, 0F);

		rightLegModel[2].addShapeBox(-4.8F, 10.5F, -4.8F, 9, 9, 8, 0F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F); // kneeArmor3
		rightLegModel[2].setRotationPoint(0F, 0F, 0F);

		rightLegModel[3].addBox(-4.5F, -0.199999999999999F, -4.5F, 9, 20, 9, 0F); // rightPants
		rightLegModel[3].setRotationPoint(0F, 0F, 0F);

		rightLegModel[4].addShapeBox(-4.8F, 2F, -4.8F, 9, 3, 9, 0F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F); // rightStrap
		rightLegModel[4].setRotationPoint(0F, 0F, 0F);

		rightLegModel[5].addShapeBox(-4F, 19F, -5F, 8, 1, 1, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 0
		rightLegModel[5].setRotationPoint(0F, 0F, 0F);

		rightLegModel[6].addShapeBox(-4.8F, 15.5F, 3.6F, 9, 3, 1, 0F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F); // Box 0
		rightLegModel[6].setRotationPoint(0F, 0F, 0F);

		rightLegModel[7].addShapeBox(-4.8F, 10.5F, 3.6F, 9, 3, 1, 0F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F); // Box 3
		rightLegModel[7].setRotationPoint(0F, 0F, 0F);

		rightLegModel[8].addShapeBox(-4.8F, 19.5F, -4.8F, 9, 1, 9, 0F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F, 0.5F, 0F, 0.5F, 0.4F, 0F, 0.5F, 0.4F, 0F, 0.9F, 0.5F, 0F, 0.9F); // Box 7
		rightLegModel[8].setRotationPoint(0F, 0F, 0F);


	}
}