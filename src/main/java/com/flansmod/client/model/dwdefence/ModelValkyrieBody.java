package com.flansmod.client.model.dwdefence;

import com.flansmod.client.model.ModelCustomArmour;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.common.vector.Vector3f;

public class ModelValkyrieBody extends ModelCustomArmour
{
	int textureX = 512;
	int textureY = 256;

	public ModelValkyrieBody()
	{
		bodyModel = new ModelRendererTurbo[27];
		bodyModel[0] = new ModelRendererTurbo(this, 1, 46, textureX, textureY); // armor2
		bodyModel[1] = new ModelRendererTurbo(this, 60, 27, textureX, textureY); // backArmorPlate
		bodyModel[2] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // body1
		bodyModel[3] = new ModelRendererTurbo(this, 60, 43, textureX, textureY); // frontArmorPlate
		bodyModel[4] = new ModelRendererTurbo(this, 100, 41, textureX, textureY); // pocket3
		bodyModel[5] = new ModelRendererTurbo(this, 100, 59, textureX, textureY); // pocket4
		bodyModel[6] = new ModelRendererTurbo(this, 1, 34, textureX, textureY); // Box 20
		bodyModel[7] = new ModelRendererTurbo(this, 58, 14, textureX, textureY); // Box 23
		bodyModel[8] = new ModelRendererTurbo(this, 58, 1, textureX, textureY); // Box 25
		bodyModel[9] = new ModelRendererTurbo(this, 135, 13, textureX, textureY); // Box 35
		bodyModel[10] = new ModelRendererTurbo(this, 135, 1, textureX, textureY); // Box 36
		bodyModel[11] = new ModelRendererTurbo(this, 135, 1, textureX, textureY); // Box 37
		bodyModel[12] = new ModelRendererTurbo(this, 135, 13, textureX, textureY); // Box 38
		bodyModel[13] = new ModelRendererTurbo(this, 100, 29, textureX, textureY); // Box 39
		bodyModel[14] = new ModelRendererTurbo(this, 100, 1, textureX, textureY); // Box 40
		bodyModel[15] = new ModelRendererTurbo(this, 100, 37, textureX, textureY); // Box 41
		bodyModel[16] = new ModelRendererTurbo(this, 100, 22, textureX, textureY); // Box 42
		bodyModel[17] = new ModelRendererTurbo(this, 100, 96, textureX, textureY); // Box 57
		bodyModel[18] = new ModelRendererTurbo(this, 115, 99, textureX, textureY); // Box 58
		bodyModel[19] = new ModelRendererTurbo(this, 130, 103, textureX, textureY); // Box 59
		bodyModel[20] = new ModelRendererTurbo(this, 115, 115, textureX, textureY); // Box 60
		bodyModel[21] = new ModelRendererTurbo(this, 130, 119, textureX, textureY); // Box 61
		bodyModel[22] = new ModelRendererTurbo(this, 100, 112, textureX, textureY); // Box 62
		bodyModel[23] = new ModelRendererTurbo(this, 135, 30, textureX, textureY); // Box 63
		bodyModel[24] = new ModelRendererTurbo(this, 135, 19, textureX, textureY); // Box 64
		bodyModel[25] = new ModelRendererTurbo(this, 135, 30, textureX, textureY); // Box 65
		bodyModel[26] = new ModelRendererTurbo(this, 135, 19, textureX, textureY); // Box 66

		bodyModel[0].addShapeBox(-8.5F, 19F, -4.5F, 17, 4, 9, 0F, 0.1F, 0F, 0.1F, 0F, 0F, 0.1F, 0F, 0F, 0.1F, 0.1F, 0F, 0.1F, 0.1F, 0F, 0.1F, 0F, 0F, 0.1F, 0F, 0F, 0.1F, 0.1F, 0F, 0.1F); // armor2
		bodyModel[0].setRotationPoint(0F, 0F, 0F);

		bodyModel[1].addBox(-7F, 0.75F, 4F, 14, 14, 1, 0F); // backArmorPlate
		bodyModel[1].setRotationPoint(0F, 0F, 0F);

		bodyModel[2].addShapeBox(-8.1F, -0.1F, -4.1F, 16, 24, 8, 0F, 0F, 0F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0.2F, 0F, 0.2F, 0.2F, 0F, 0.2F, 0.2F, 0.2F, 0F, 0.2F, 0.2F); // body1
		bodyModel[2].setRotationPoint(0F, 0F, 0F);

		bodyModel[3].addBox(-7F, 0.5F, -5F, 14, 10, 1, 0F); // frontArmorPlate
		bodyModel[3].setRotationPoint(0F, 0F, 0F);

		bodyModel[4].addBox(8F, 24F, -4.5F, 3, 8, 9, 0F); // pocket3
		bodyModel[4].setRotationPoint(0F, 0F, 0F);

		bodyModel[5].addShapeBox(11F, 24F, -4.5F, 1, 4, 9, 0F, 0F, 0F, 0F, -0.5F, -0.2F, 0F, -0.5F, -0.2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F); // pocket4
		bodyModel[5].setRotationPoint(0F, 0F, 0F);

		bodyModel[6].addShapeBox(-8.5F, 14F, -4.5F, 17, 2, 9, 0F, 0.1F, -1F, 0.1F, 0F, 0F, 0.1F, 0F, 0F, 0.1F, 0.1F, -1F, 0.1F, 0.1F, 1F, 0.1F, 0F, 0F, 0.1F, 0F, 0F, 0.1F, 0.1F, 1F, 0.1F); // Box 20
		bodyModel[6].setRotationPoint(0F, 0F, 0F);

		bodyModel[7].addShapeBox(-8.1F, 23.9F, -4.1F, 8, 4, 8, 0F, 0F, 0F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0.2F, 0F, 0F, 0.2F, 0.5F, 0.2F, 0.4F, -0.3F, 0.2F, 0.4F, -0.3F, 0.2F, 0.6F, 0.5F, 0.2F, 0.6F); // Box 23
		bodyModel[7].setRotationPoint(0F, 0F, 0F);

		bodyModel[8].addShapeBox(0.1F, 23.9F, -4.1F, 8, 4, 8, 0F, 0.2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.2F, 0.2F, 0F, 0.2F, -0.3F, 0.2F, 0.4F, 0.5F, 0.2F, 0.4F, 0.5F, 0.2F, 0.6F, -0.3F, 0.2F, 0.6F); // Box 25
		bodyModel[8].setRotationPoint(0F, 0F, 0F);

		bodyModel[9].addShapeBox(-8.5F, 19F, 7.5F, 8, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.2F, -0.5F, 0F, -0.2F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 35
		bodyModel[9].setRotationPoint(0F, 0F, 0F);

		bodyModel[10].addBox(-8.5F, 19F, 4.5F, 8, 8, 3, 0F); // Box 36
		bodyModel[10].setRotationPoint(0F, 0F, 0F);

		bodyModel[11].addBox(0.5F, 19F, 4.5F, 8, 8, 3, 0F); // Box 37
		bodyModel[11].setRotationPoint(0F, 0F, 0F);

		bodyModel[12].addShapeBox(0.5F, 19F, 7.5F, 8, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.2F, -0.5F, 0F, -0.2F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 38
		bodyModel[12].setRotationPoint(0F, 0F, 0F);

		bodyModel[13].addShapeBox(-6F, 2F, 9F, 12, 6, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, -0.5F, 0F, -1F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 39
		bodyModel[13].setRotationPoint(0F, 0F, 0F);

		bodyModel[14].addBox(-6F, 2F, 4F, 12, 15, 5, 0F); // Box 40
		bodyModel[14].setRotationPoint(0F, 0F, 0F);

		bodyModel[15].addBox(-2F, 8F, 8.4F, 4, 2, 1, 0F); // Box 41
		bodyModel[15].setRotationPoint(0F, 0F, 0F);

		bodyModel[16].addBox(-5.5F, 17F, 3.5F, 11, 1, 5, 0F); // Box 42
		bodyModel[16].setRotationPoint(0F, 0F, 0F);

		bodyModel[17].addShapeBox(-8.3F, 27F, -3F, 1, 9, 6, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0.5F, 0F, 0F); // Box 57
		bodyModel[17].setRotationPoint(0F, 0F, 0F);

		bodyModel[18].addShapeBox(-8.8F, 36F, -3F, 1, 6, 6, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 1F, 0F, 0F); // Box 58
		bodyModel[18].setRotationPoint(0F, 0F, 0F);

		bodyModel[19].addShapeBox(-9.8F, 42F, -3F, 1, 2, 6, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0.7F, 0F, -1F, -1.2F, 0F, -1F, -1.2F, 0F, -1F, 0.7F, 0F, -1F); // Box 59
		bodyModel[19].setRotationPoint(0F, 0F, 0F);

		bodyModel[20].addShapeBox(7.8F, 36F, -3F, 1, 6, 6, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -1.5F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, -1.5F, 0F, 0F); // Box 60
		bodyModel[20].setRotationPoint(0F, 0F, 0F);

		bodyModel[21].addShapeBox(8.8F, 42F, -3F, 1, 2, 6, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -1.2F, 0F, -1F, 0.7F, 0F, -1F, 0.7F, 0F, -1F, -1.2F, 0F, -1F); // Box 61
		bodyModel[21].setRotationPoint(0F, 0F, 0F);

		bodyModel[22].addShapeBox(7.3F, 27F, -3F, 1, 9, 6, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -1F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, -1F, 0F, 0F); // Box 62
		bodyModel[22].setRotationPoint(0F, 0F, 0F);

		bodyModel[23].addShapeBox(1F, 10.8F, -7F, 6, 3, 1, 0F, 0F, -0.2F, -0.5F, 0F, -0.2F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 63
		bodyModel[23].setRotationPoint(0F, 0F, 0F);

		bodyModel[24].addBox(1F, 10.8F, -6F, 6, 8, 2, 0F); // Box 64
		bodyModel[24].setRotationPoint(0F, 0F, 0F);

		bodyModel[25].addShapeBox(-7F, 10.8F, -7F, 6, 3, 1, 0F, 0F, -0.2F, -0.5F, 0F, -0.2F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 65
		bodyModel[25].setRotationPoint(0F, 0F, 0F);

		bodyModel[26].addBox(-7F, 10.8F, -6F, 6, 8, 2, 0F); // Box 66
		bodyModel[26].setRotationPoint(0F, 0F, 0F);


		leftArmModel = new ModelRendererTurbo[14];
		leftArmModel[0] = new ModelRendererTurbo(this, 67, 66, textureX, textureY); // emblemPatch
		leftArmModel[1] = new ModelRendererTurbo(this, 1, 62, textureX, textureY); // leftArm
		leftArmModel[2] = new ModelRendererTurbo(this, 1, 154, textureX, textureY); // Box 4
		leftArmModel[3] = new ModelRendererTurbo(this, 1, 138, textureX, textureY); // Box 5
		leftArmModel[4] = new ModelRendererTurbo(this, 1, 127, textureX, textureY); // Box 10
		leftArmModel[5] = new ModelRendererTurbo(this, 1, 115, textureX, textureY); // Box 11
		leftArmModel[6] = new ModelRendererTurbo(this, 41, 172, textureX, textureY); // Box 13
		leftArmModel[7] = new ModelRendererTurbo(this, 1, 104, textureX, textureY); // Box 16
		leftArmModel[8] = new ModelRendererTurbo(this, 20, 138, textureX, textureY); // Box 17
		leftArmModel[9] = new ModelRendererTurbo(this, 1, 87, textureX, textureY); // Box 67
		leftArmModel[10] = new ModelRendererTurbo(this, 50, 105, textureX, textureY); // Box 68
		leftArmModel[11] = new ModelRendererTurbo(this, 34, 174, textureX, textureY); // Box 58
		leftArmModel[12] = new ModelRendererTurbo(this, 27, 172, textureX, textureY); // Box 59
		leftArmModel[13] = new ModelRendererTurbo(this, 20, 172, textureX, textureY); // Box 60

		leftArmModel[0].addShapeBox(5.2F, 0.5F, -4F, 1, 6, 14, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -6F, 0F, 0F, -6F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, -6F, 0F, -3F, -6F); // emblemPatch
		leftArmModel[0].setRotationPoint(0F, 0F, 0F);

		leftArmModel[1].addShapeBox(-2.1F, -4.1F, -4.1F, 8, 16, 8, 0F, 0F, 0F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0.2F, 0F, 0.2F, 0.2F, 0F, 0.2F, 0.2F, 0.2F, 0F, 0.2F, 0.2F); // leftArm
		leftArmModel[1].setRotationPoint(0F, 0F, 0F);

		leftArmModel[2].addShapeBox(2.5F, 11F, -4.5F, 4, 8, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 4
		leftArmModel[2].setRotationPoint(0F, 0F, 0F);

		leftArmModel[3].addShapeBox(6.5F, 11.5F, -4F, 1, 7, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F); // Box 5
		leftArmModel[3].setRotationPoint(0F, 0F, 0F);

		leftArmModel[4].addShapeBox(-2F, -7.1F, -4F, 8, 2, 8, 0F, 0F, 0F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F); // Box 10
		leftArmModel[4].setRotationPoint(0F, 0F, 0F);

		leftArmModel[5].addShapeBox(-2.5F, -5.1F, -4.5F, 9, 2, 9, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F); // Box 11
		leftArmModel[5].setRotationPoint(0F, 0F, 0F);

		leftArmModel[6].addShapeBox(5F, -5.1F, -3.5F, 2, 1, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 13
		leftArmModel[6].setRotationPoint(0F, 0F, 0F);

		leftArmModel[7].addBox(3.5F, -1.1F, -4.5F, 3, 1, 9, 0F); // Box 16
		leftArmModel[7].setRotationPoint(0F, 0F, 0F);

		leftArmModel[8].addShapeBox(6.5F, -4.1F, -4.5F, 1, 4, 9, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F); // Box 17
		leftArmModel[8].setRotationPoint(0F, 0F, 0F);

		leftArmModel[9].addShapeBox(-2.5F, 11.5F, -4.5F, 5, 7, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 67
		leftArmModel[9].setRotationPoint(0F, 0F, 0F);

		leftArmModel[10].addShapeBox(-2F, 12F, -4.55F, 4, 6, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 68
		leftArmModel[10].setRotationPoint(0F, 0F, 0F);

		leftArmModel[11].addShapeBox(6F, 6F, 1.5F, 1, 6, 2, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, -3F, 0.75F, 0F, -3F, 0.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 58
		leftArmModel[11].setRotationPoint(0F, 0F, 0F);

		leftArmModel[12].addShapeBox(6F, 4F, -0.5F, 1, 8, 2, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, -3F, 0.5F, 0F, -3F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 59
		leftArmModel[12].setRotationPoint(0F, 0F, 0F);

		leftArmModel[13].addShapeBox(6F, 4.5F, -2.5F, 1, 8, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 60
		leftArmModel[13].setRotationPoint(0F, 0F, 0F);


		rightArmModel = new ModelRendererTurbo[13];
		rightArmModel[0] = new ModelRendererTurbo(this, 34, 62, textureX, textureY); // Box 14
		rightArmModel[1] = new ModelRendererTurbo(this, 41, 154, textureX, textureY); // Box 14
		rightArmModel[2] = new ModelRendererTurbo(this, 67, 66, textureX, textureY); // Box 14
		rightArmModel[3] = new ModelRendererTurbo(this, 34, 87, textureX, textureY); // Box 14
		rightArmModel[4] = new ModelRendererTurbo(this, 41, 138, textureX, textureY); // Box 14
		rightArmModel[5] = new ModelRendererTurbo(this, 38, 115, textureX, textureY); // Box 14
		rightArmModel[6] = new ModelRendererTurbo(this, 38, 127, textureX, textureY); // Box 14
		rightArmModel[7] = new ModelRendererTurbo(this, 1, 172, textureX, textureY); // Box 14
		rightArmModel[8] = new ModelRendererTurbo(this, 60, 138, textureX, textureY); // Box 14
		rightArmModel[9] = new ModelRendererTurbo(this, 34, 104, textureX, textureY); // Box 14
		rightArmModel[10] = new ModelRendererTurbo(this, 60, 172, textureX, textureY); // Box 14
		rightArmModel[11] = new ModelRendererTurbo(this, 67, 172, textureX, textureY); // Box 14
		rightArmModel[12] = new ModelRendererTurbo(this, 74, 174, textureX, textureY); // Box 14

		rightArmModel[0].addShapeBox(-6.1F, -4.1F, -4.1F, 8, 16, 8, 0F, 0F, 0F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0.2F, 0F, 0.2F, 0.2F, 0F, 0.2F, 0.2F, 0.2F, 0F, 0.2F, 0.2F); // Box 14
		rightArmModel[0].setRotationPoint(0F, 0F, 0F);

		rightArmModel[1].addShapeBox(-6.5F, 11F, -4.5F, 4, 8, 9, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 14
		rightArmModel[1].setRotationPoint(0F, 0F, 0F);

		rightArmModel[2].addShapeBox(-6.2F, 0.5F, -4F, 1, 6, 14, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -6F, 0F, 0F, -6F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, -3F, -6F, 0F, -3F, -6F); // Box 14
		rightArmModel[2].setRotationPoint(0F, 0F, 0F);

		rightArmModel[3].addShapeBox(-6.1F, 12F, -4.1F, 8, 6, 8, 0F, 0F, 0F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0.2F, 0F, 0.2F, 0.2F, 0F, 0.2F, 0.2F, 0.2F, 0F, 0.2F, 0.2F); // Box 14
		rightArmModel[3].setRotationPoint(0F, 0F, 0F);

		rightArmModel[4].addShapeBox(-7.5F, 11.5F, -4F, 1, 7, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F); // Box 14
		rightArmModel[4].setRotationPoint(0F, 0F, 0F);

		rightArmModel[5].addShapeBox(-6.5F, -5.1F, -4.5F, 9, 2, 9, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F); // Box 14
		rightArmModel[5].setRotationPoint(0F, 0F, 0F);

		rightArmModel[6].addShapeBox(-6F, -7.1F, -4F, 8, 2, 8, 0F, -1F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, -1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F); // Box 14
		rightArmModel[6].setRotationPoint(0F, 0F, 0F);

		rightArmModel[7].addShapeBox(-7F, -5.1F, -3.5F, 2, 1, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 14
		rightArmModel[7].setRotationPoint(0F, 0F, 0F);

		rightArmModel[8].addShapeBox(-7.5F, -4.1F, -4.5F, 1, 4, 9, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F); // Box 14
		rightArmModel[8].setRotationPoint(0F, 0F, 0F);

		rightArmModel[9].addBox(-6.5F, -1.1F, -4.5F, 3, 1, 9, 0F); // Box 14
		rightArmModel[9].setRotationPoint(0F, 0F, 0F);

		rightArmModel[10].addShapeBox(-7F, 4.5F, -2.5F, 1, 8, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 14
		rightArmModel[10].setRotationPoint(0F, 0F, 0F);

		rightArmModel[11].addShapeBox(-7F, 4F, -0.5F, 1, 8, 2, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, -3F, 0.5F, 0F, -3F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 14
		rightArmModel[11].setRotationPoint(0F, 0F, 0F);

		rightArmModel[12].addShapeBox(-7F, 6F, 1.5F, 1, 6, 2, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, -3F, 0.75F, 0F, -3F, 0.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 14
		rightArmModel[12].setRotationPoint(0F, 0F, 0F);


		skirtFrontModel = new ModelRendererTurbo[6];
		skirtFrontModel[0] = new ModelRendererTurbo(this, 123, 73, textureX, textureY); // Box 28
		skirtFrontModel[1] = new ModelRendererTurbo(this, 123, 79, textureX, textureY); // Box 29
		skirtFrontModel[2] = new ModelRendererTurbo(this, 100, 73, textureX, textureY); // Box 30
		skirtFrontModel[3] = new ModelRendererTurbo(this, 123, 82, textureX, textureY); // Box 31
		skirtFrontModel[4] = new ModelRendererTurbo(this, 100, 84, textureX, textureY); // Box 32
		skirtFrontModel[5] = new ModelRendererTurbo(this, 100, 92, textureX, textureY); // Box 34

		skirtFrontModel[0].addShapeBox(-6F, -1F, -4.5F, 12, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 28
		skirtFrontModel[0].setRotationPoint(0F, 0F, 0F);

		skirtFrontModel[1].addShapeBox(-6F, 3F, -5F, 12, 1, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, -0.5F, -1F, 0F, -0.5F); // Box 29
		skirtFrontModel[1].setRotationPoint(0F, 0F, 0F);

		skirtFrontModel[2].addShapeBox(-5F, 3F, -4.5F, 10, 9, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 30
		skirtFrontModel[2].setRotationPoint(0F, 0F, 0F);

		skirtFrontModel[3].addShapeBox(-5F, -1F, -4.5F, 10, 4, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 31
		skirtFrontModel[3].setRotationPoint(0F, 0F, 0F);

		skirtFrontModel[4].addShapeBox(-5F, 12F, -5F, 10, 6, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, -1.5F, 0F, 0F, -1.5F); // Box 32
		skirtFrontModel[4].setRotationPoint(0F, 0F, 0F);

		skirtFrontModel[5].addShapeBox(-5F, 18F, -6F, 10, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -1F, 0F, 0.7F, -1F, 0F, 0.7F, -1F, 0F, -1.2F, -1F, 0F, -1.2F); // Box 34
		skirtFrontModel[5].setRotationPoint(0F, 0F, 0F);

		
	}
}

