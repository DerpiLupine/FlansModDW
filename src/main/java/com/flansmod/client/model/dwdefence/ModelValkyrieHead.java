package com.flansmod.client.model.dwdefence; //Path where the model is located

import com.flansmod.client.model.ModelCustomArmour;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelValkyrieHead extends ModelCustomArmour //Same as Filename
{
	int textureX = 512;
	int textureY = 256;

	public ModelValkyrieHead() //Same as Filename
	{
		headModel = new ModelRendererTurbo[18];
		headModel[0] = new ModelRendererTurbo(this, 160, 149, textureX, textureY); // goggles
		headModel[1] = new ModelRendererTurbo(this, 193, 151, textureX, textureY); // goggles2
		headModel[2] = new ModelRendererTurbo(this, 193, 151, textureX, textureY); // goggles2
		headModel[3] = new ModelRendererTurbo(this, 160, 126, textureX, textureY); // goggleStrap
		headModel[4] = new ModelRendererTurbo(this, 160, 48, textureX, textureY); // helmet1
		headModel[5] = new ModelRendererTurbo(this, 160, 1, textureX, textureY); // helmet2
		headModel[6] = new ModelRendererTurbo(this, 160, 26, textureX, textureY); // helmet3
		headModel[7] = new ModelRendererTurbo(this, 160, 68, textureX, textureY); // helmet4
		headModel[8] = new ModelRendererTurbo(this, 160, 84, textureX, textureY); // helmet5
		headModel[9] = new ModelRendererTurbo(this, 160, 95, textureX, textureY); // helmet6
		headModel[10] = new ModelRendererTurbo(this, 160, 115, textureX, textureY); // Box 78
		headModel[11] = new ModelRendererTurbo(this, 181, 116, textureX, textureY); // Box 79
		headModel[12] = new ModelRendererTurbo(this, 181, 116, textureX, textureY); // Box 80
		headModel[13] = new ModelRendererTurbo(this, 202, 107, textureX, textureY); // Box 81
		headModel[14] = new ModelRendererTurbo(this, 202, 118, textureX, textureY); // Box 86
		headModel[15] = new ModelRendererTurbo(this, 181, 105, textureX, textureY); // Box 87
		headModel[16] = new ModelRendererTurbo(this, 160, 104, textureX, textureY); // Box 88
		headModel[17] = new ModelRendererTurbo(this, 181, 105, textureX, textureY); // Box 89

		headModel[0].addShapeBox(-7.5F, -17F, -10F, 15, 4, 1, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // goggles
		headModel[0].setRotationPoint(0F, 0F, 0F);

		headModel[1].addShapeBox(-7.5F, -13F, -10F, 7, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0.5F, -1F, 0F, 0.5F, -1F, 0F, 0F, -1F, 0F, 0F); // goggles2
		headModel[1].setRotationPoint(0F, 0F, 0F);

		headModel[2].addShapeBox(0.5F, -13F, -10F, 7, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0.5F, -1F, 0F, 0.5F, -1F, 0F, 0F, -1F, 0F, 0F); // goggles2
		headModel[2].setRotationPoint(0F, 0F, 0F);

		headModel[3].addShapeBox(-9.5F, -16F, -9.5F, 19, 3, 19, 0F, 0F, 0F, -0.4F, 0F, 0F, -0.4F, 0F, -2F, 0.05F, 0F, -2F, 0.05F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0.3F, 0F, 1F, 0.3F); // goggleStrap
		headModel[3].setRotationPoint(0F, 0F, 0F);

		headModel[4].addShapeBox(-8.5F, -19.5F, -8.5F, 17, 2, 17, 0F, -3F, 0F, -3F, -3F, 0F, -3F, -3F, 0F, -3F, -3F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // helmet1
		headModel[4].setRotationPoint(0F, 0F, 0F);

		headModel[5].addShapeBox(-9.5F, -17.5F, -9.5F, 19, 5, 19, 0F, -1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // helmet2
		headModel[5].setRotationPoint(0F, 0F, 0F);

		headModel[6].addShapeBox(-9.5F, -12.5F, -9.5F, 19, 2, 19, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 1F, 1F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F); // helmet3
		headModel[6].setRotationPoint(0F, 0F, 0F);

		headModel[7].addShapeBox(-9.5F, -10.5F, -0.5F, 19, 5, 10, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // helmet4
		headModel[7].setRotationPoint(0F, 0F, 0F);

		headModel[8].addShapeBox(-9.5F, -5.5F, 1.5F, 19, 2, 8, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // helmet5
		headModel[8].setRotationPoint(0F, 0F, 0F);

		headModel[9].addShapeBox(-9.5F, -3.5F, 2.5F, 19, 1, 7, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // helmet6
		headModel[9].setRotationPoint(0F, 0F, 0F);

		headModel[10].addBox(-11F, -11F, -4F, 3, 3, 7, 0F); // Box 78
		headModel[10].setRotationPoint(0F, 0F, 0F);

		headModel[11].addShapeBox(-11F, -13F, -4F, 3, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 79
		headModel[11].setRotationPoint(0F, 0F, 0F);

		headModel[12].addShapeBox(-11F, -8F, -4F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 80
		headModel[12].setRotationPoint(0F, 0F, 0F);

		headModel[13].addShapeBox(-10.5F, -20F, 1F, 1, 9, 1, 0F, 0F, 0F, -5F, 0F, 0F, -5F, 0F, -1F, 4.75F, 0F, -1F, 4.75F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 81
		headModel[13].setRotationPoint(0F, 0F, 0F);

		headModel[14].addShapeBox(-12F, -11F, -4F, 1, 3, 4, 0F, 0F, -0.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.75F, 0F); // Box 86
		headModel[14].setRotationPoint(0F, 0F, 0F);

		headModel[15].addShapeBox(8F, -8F, -4F, 3, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 87
		headModel[15].setRotationPoint(0F, 0F, 0F);

		headModel[16].addBox(8F, -11F, -4F, 3, 3, 7, 0F); // Box 88
		headModel[16].setRotationPoint(0F, 0F, 0F);

		headModel[17].addShapeBox(8F, -13F, -4F, 3, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 89
		headModel[17].setRotationPoint(0F, 0F, 0F);


	}
}