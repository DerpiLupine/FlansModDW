package com.flansmod.client.model.dwdefence; //Path where the model is located

import com.flansmod.client.model.ModelCustomArmour;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelValkyrieBoots extends ModelCustomArmour //Same as Filename
{
	int textureX = 512;
	int textureY = 256;

	public ModelValkyrieBoots() //Same as Filename
	{
		leftLegModel = new ModelRendererTurbo[3];
		leftLegModel[0] = new ModelRendererTurbo(this, 100, 144, textureX, textureY); // Box 75
		leftLegModel[1] = new ModelRendererTurbo(this, 100, 128, textureX, textureY); // Box 76
		leftLegModel[2] = new ModelRendererTurbo(this, 100, 153, textureX, textureY); // Box 77

		leftLegModel[0].addShapeBox(-4.2F, 15.1F, -2.2F, 8, 2, 6, 0F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F); // Box 75
		leftLegModel[0].setRotationPoint(0F, 0F, 0F);

		leftLegModel[1].addShapeBox(-4.2F, 17.1F, -4.2F, 8, 7, 8, 0F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F); // Box 76
		leftLegModel[1].setRotationPoint(0F, 0F, 0F);

		leftLegModel[2].addShapeBox(-4.2F, 20.1F, -5.6F, 8, 4, 1, 0F, -1F, -0.5F, 0F, -0.6F, -0.5F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F, -1F, 0F, 0F, -0.6F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F); // Box 77
		leftLegModel[2].setRotationPoint(0F, 0F, 0F);


		rightLegModel = new ModelRendererTurbo[3];
		rightLegModel[0] = new ModelRendererTurbo(this, 100, 128, textureX, textureY); // Box 72
		rightLegModel[1] = new ModelRendererTurbo(this, 100, 144, textureX, textureY); // Box 73
		rightLegModel[2] = new ModelRendererTurbo(this, 100, 153, textureX, textureY); // Box 78

		rightLegModel[0].addShapeBox(-4.2F, 17.1F, -4.2F, 8, 7, 8, 0F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F); // Box 72
		rightLegModel[0].setRotationPoint(0F, 0F, 0F);

		rightLegModel[1].addShapeBox(-4.2F, 15.1F, -2.2F, 8, 2, 6, 0F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F); // Box 73
		rightLegModel[1].setRotationPoint(0F, 0F, 0F);

		rightLegModel[2].addShapeBox(-4.2F, 20.1F, -5.6F, 8, 4, 1, 0F, -1F, -0.5F, 0F, -0.6F, -0.5F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F, -1F, 0F, 0F, -0.6F, 0F, 0F, 0.4F, 0F, 0.4F, 0F, 0F, 0.4F); // Box 78
		rightLegModel[2].setRotationPoint(0F, 0F, 0F);


	}
}