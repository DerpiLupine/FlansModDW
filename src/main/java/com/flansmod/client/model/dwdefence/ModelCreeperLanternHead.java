package com.flansmod.client.model.dwdefence;

import com.flansmod.client.model.ModelCustomArmour;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelCreeperLanternHead extends ModelCustomArmour
{
	int textureX = 512;
	int textureY = 256;

	public ModelCreeperLanternHead()
	{
		headModel = new ModelRendererTurbo[1];
		headModel[0] = new ModelRendererTurbo(this, 224, 162, textureX, textureY); // goggleStrap

		headModel[0].addShapeBox(-8.5F, -16.5F, -8.5F, 16, 16, 16, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 1F, 0F, 0F, 1F, 0F, 1F, 0F, 1F, 1F, 0F, 1F, 1F, 1F, 0F, 1F, 1F); // goggleStrap
		headModel[0].setRotationPoint(0F, 0F, 0F);
	}
}
