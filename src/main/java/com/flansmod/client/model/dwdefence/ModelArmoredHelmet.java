package com.flansmod.client.model.dwdefence;

import com.flansmod.client.model.ModelCustomArmour;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelArmoredHelmet extends ModelCustomArmour
{
	int textureX = 512;
	int textureY = 256;

	public ModelArmoredHelmet()
	{
		headModel = new ModelRendererTurbo[22];
		headModel[0] = new ModelRendererTurbo(this, 1, 179, textureX, textureY); // helmetTop1
		headModel[1] = new ModelRendererTurbo(this, 1, 224, textureX, textureY); // helmetTop3
		headModel[2] = new ModelRendererTurbo(this, 80, 202, textureX, textureY); // helmet
		headModel[3] = new ModelRendererTurbo(this, 80, 182, textureX, textureY); // helmetTop4
		headModel[4] = new ModelRendererTurbo(this, 1, 202, textureX, textureY); // helmetTop2
		headModel[5] = new ModelRendererTurbo(this, 80, 221, textureX, textureY); // Box 18
		headModel[6] = new ModelRendererTurbo(this, 80, 237, textureX, textureY); // Box 19
		headModel[7] = new ModelRendererTurbo(this, 80, 244, textureX, textureY); // Box 21
		headModel[8] = new ModelRendererTurbo(this, 125, 227, textureX, textureY); // Box 22
		headModel[9] = new ModelRendererTurbo(this, 80, 202, textureX, textureY); // Box 23

		headModel[0].addBox(-9.5F, -14F, -10.5F, 19, 2, 20, 0F); // helmetTop1
		//headModel[0].setRotationPoint();

		headModel[1].addShapeBox(-9.5F, -18F, -9.5F, 19, 2, 19, 0F, -1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // helmetTop3
		//headModel[1].setRotationPoint();

		headModel[2].addShapeBox(-9.5F, -12F, 1.5F, 19, 7, 8, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // helmet
		//headModel[2].setRotationPoint();

		headModel[3].addShapeBox(-8.5F, -20F, -8.5F, 17, 2, 17, 0F, -3F, 0F, -3F, -3F, 0F, -3F, -3F, 0F, -3F, -3F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // helmetTop4
		//headModel[3].setRotationPoint();

		headModel[4].addShapeBox(-9.5F, -16F, -9.5F, 19, 2, 19, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F); // helmetTop2
		//headModel[4].setRotationPoint();

		headModel[5].addShapeBox(-9F, -12F, -14.5F, 18, 11, 4, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 18
		//headModel[5].setRotationPoint();

		headModel[6].addShapeBox(-9.5F, -14F, -14.5F, 19, 2, 4, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 19
		//headModel[6].setRotationPoint();

		headModel[7].addBox(-6F, -20F, -10.5F, 12, 3, 4, 0F); // Box 21
		//headModel[7].setRotationPoint();

		headModel[8].addShapeBox(0.5F, -21F, -9.5F, 4, 2, 7, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 22
		//headModel[8].setRotationPoint();

		headModel[9].addShapeBox(-9.5F, -5F, 1.5F, 19, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 23
		//headModel[9].setRotationPoint();

		headModel[10] = new ModelRendererTurbo(this, 74, 148, textureX, textureY); // Box 4
		headModel[11] = new ModelRendererTurbo(this, 74, 158, textureX, textureY); // Box 5
		headModel[12] = new ModelRendererTurbo(this, 114, 164, textureX, textureY); // Box 6
		headModel[13] = new ModelRendererTurbo(this, 114, 164, textureX, textureY); // Box 7
		headModel[14] = new ModelRendererTurbo(this, 103, 158, textureX, textureY); // Box 9
		headModel[15] = new ModelRendererTurbo(this, 113, 138, textureX, textureY); // Box 12
		headModel[16] = new ModelRendererTurbo(this, 113, 148, textureX, textureY); // Box 13
		headModel[17] = new ModelRendererTurbo(this, 113, 138, textureX, textureY); // Box 14
		headModel[18] = new ModelRendererTurbo(this, 74, 138, textureX, textureY); // Box 15
		headModel[19] = new ModelRendererTurbo(this, 103, 164, textureX, textureY); // Box 16
		headModel[20] = new ModelRendererTurbo(this, 74, 117, textureX, textureY); // Box 17
		headModel[21] = new ModelRendererTurbo(this, 74, 138, textureX, textureY); // Box 19

		headModel[10].addShapeBox(-9F, -13F, -9F, 18, 8, 1, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 4
		//headModel[0].setRotationPoint();

		headModel[11].addBox(-4F, -6F, -14F, 8, 6, 6, 0F); // Box 5
		//headModel[1].setRotationPoint();

		headModel[12].addShapeBox(4F, -5F, -9F, 4, 5, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F); // Box 6
		//headModel[2].setRotationPoint();

		headModel[13].addShapeBox(-8F, -5F, -9F, 4, 5, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F); // Box 7
		//headModel[3].setRotationPoint();

		headModel[14].addBox(-1.5F, 0F, -13F, 3, 2, 3, 0F); // Box 9
		//headModel[4].setRotationPoint();

		headModel[15].addShapeBox(4F, -5F, -16F, 4, 2, 7, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 12
		//headModel[5].setRotationPoint();

		headModel[16].addBox(4F, -3F, -16F, 4, 3, 7, 0F); // Box 13
		//headModel[6].setRotationPoint();

		headModel[17].addShapeBox(4F, 0F, -16F, 4, 2, 7, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F); // Box 14
		//headModel[7].setRotationPoint();

		headModel[18].addBox(-9F, -13F, -8F, 1, 8, 1, 0F); // Box 15
		//headModel[8].setRotationPoint();

		headModel[19].addBox(-5.5F, -4F, -13F, 2, 3, 3, 0F); // Box 16
		//headModel[9].setRotationPoint();

		headModel[20].addBox(-9F, -11F, -7F, 18, 4, 16, 0F); // Box 17
		//headModel[10].setRotationPoint();

		headModel[21].addBox(8F, -13F, -8F, 1, 8, 1, 0F); // Box 19
		//headModel[11].setRotationPoint();
	}
}