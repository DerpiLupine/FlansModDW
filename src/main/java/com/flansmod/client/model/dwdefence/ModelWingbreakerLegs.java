package com.flansmod.client.model.dwdefence;

import com.flansmod.client.model.ModelCustomArmour;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.common.vector.Vector3f;

public class ModelWingbreakerLegs extends ModelCustomArmour
{
	public ModelWingbreakerLegs()
	{
		int textureX = 256;
		int textureY = 128;

		leftLegModel = new ModelRendererTurbo[2];
		leftLegModel[0] = new ModelRendererTurbo(this, 66, 55, textureX, textureY);
		leftLegModel[1] = new ModelRendererTurbo(this, 87, 39, textureX, textureY);

		leftLegModel[0].addBox(-4.5F, 6F, -4.5F, 9, 4, 9, 0F); // leftStrap
		// leftLegModel[0].setRotationPoint(-0.5F, 26F, -4.5F);

		leftLegModel[1].addShapeBox(-4F, 4F, -5.3F, 8, 8, 2, 0F,-1F, -1F, 0F,-1F, -1F, 0F,-0.1F, 0F, 0F,-0.1F, 0F, 0F,-1F, -1F, 0F,-1F, -1F, 0F,-0.1F, 0F, 0F,-0.1F, 0F, 0F); // armourPad2
		// leftLegModel[1].setRotationPoint(0F, 24F, -5.3F);


		rightLegModel = new ModelRendererTurbo[2];
		rightLegModel[0] = new ModelRendererTurbo(this, 66, 55, textureX, textureY);
		rightLegModel[1] = new ModelRendererTurbo(this, 87, 39, textureX, textureY);

		rightLegModel[0].addBox(-4.5F, 6F, -4.5F, 9, 4, 9, 0F); // rightStrap
		// rightLegModel[0].setRotationPoint(-8.5F, 26F, -4.5F);

		rightLegModel[1].addShapeBox(-4F, 4F, -5.3F, 8, 8, 2, 0F,-1F, -1F, 0F,-1F, -1F, 0F,-0.1F, 0F, 0F,-0.1F, 0F, 0F,-1F, -1F, 0F,-1F, -1F, 0F,-0.1F, 0F, 0F,-0.1F, 0F, 0F); // armourPad1
		// rightLegModel[1].setRotationPoint(-8F, 24F, -5.3F);

		//-20 Y, -4 X
	}
}