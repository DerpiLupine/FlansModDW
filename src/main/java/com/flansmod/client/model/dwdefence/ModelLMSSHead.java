package com.flansmod.client.model.dwdefence; //Path where the model is located

import com.flansmod.client.model.ModelCustomArmour;
import com.flansmod.client.tmt.ModelRendererTurbo;
import com.flansmod.client.tmt.Coord2D;
import com.flansmod.client.tmt.Shape2D;

public class ModelLMSSHead extends ModelCustomArmour //Same as Filename
{
	int textureX = 512;
	int textureY = 256;

	public ModelLMSSHead() //Same as Filename
	{
		headModel = new ModelRendererTurbo[29];
		headModel[0] = new ModelRendererTurbo(this, 78, 56, textureX, textureY); // helmet7
		headModel[1] = new ModelRendererTurbo(this, 78, 3, textureX, textureY); // decal
		headModel[2] = new ModelRendererTurbo(this, 111, 64, textureX, textureY); // goggles
		headModel[3] = new ModelRendererTurbo(this, 130, 70, textureX, textureY); // goggles2
		headModel[4] = new ModelRendererTurbo(this, 111, 70, textureX, textureY); // goggles2
		headModel[5] = new ModelRendererTurbo(this, 109, 12, textureX, textureY); // headset
		headModel[6] = new ModelRendererTurbo(this, 78, 26, textureX, textureY); // headset
		headModel[7] = new ModelRendererTurbo(this, 130, 16, textureX, textureY); // headset2
		headModel[8] = new ModelRendererTurbo(this, 129, 6, textureX, textureY); // headset2
		headModel[9] = new ModelRendererTurbo(this, 1, 48, textureX, textureY); // helmet1
		headModel[10] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // helmet2
		headModel[11] = new ModelRendererTurbo(this, 1, 26, textureX, textureY); // helmet3
		headModel[12] = new ModelRendererTurbo(this, 1, 68, textureX, textureY); // helmet4
		headModel[13] = new ModelRendererTurbo(this, 1, 90, textureX, textureY); // helmet5
		headModel[14] = new ModelRendererTurbo(this, 147, 79, textureX, textureY); // helmet6
		headModel[15] = new ModelRendererTurbo(this, 78, 74, textureX, textureY); // helmetStrap3
		headModel[16] = new ModelRendererTurbo(this, 151, 16, textureX, textureY); // mic
		headModel[17] = new ModelRendererTurbo(this, 129, 6, textureX, textureY); // Box 0
		headModel[18] = new ModelRendererTurbo(this, 133, 50, textureX, textureY); // Box 1
		headModel[19] = new ModelRendererTurbo(this, 122, 48, textureX, textureY); // Box 2
		headModel[20] = new ModelRendererTurbo(this, 78, 42, textureX, textureY); // Box 3
		headModel[21] = new ModelRendererTurbo(this, 111, 58, textureX, textureY); // Box 4
		headModel[22] = new ModelRendererTurbo(this, 78, 64, textureX, textureY); // Box 5
		headModel[23] = new ModelRendererTurbo(this, 122, 43, textureX, textureY); // Box 6
		headModel[24] = new ModelRendererTurbo(this, 122, 43, textureX, textureY); // Box 7
		headModel[25] = new ModelRendererTurbo(this, 122, 43, textureX, textureY); // Box 8
		headModel[26] = new ModelRendererTurbo(this, 122, 43, textureX, textureY); // Box 9
		headModel[27] = new ModelRendererTurbo(this, 105, 48, textureX, textureY); // Box 11
		headModel[28] = new ModelRendererTurbo(this, 105, 42, textureX, textureY); // Box 12

		headModel[0].addShapeBox(-7.5F, -11F, 9F, 15, 6, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F); // helmet7
		headModel[0].setRotationPoint(0F, 0F, 0F);

		headModel[1].addShapeBox(-11.2F, -9.5F, -2F, 1, 8, 14, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -8F, 0F, 0F, -8F, 0F, -5F, 0F, 0F, -5F, 0F, 0F, -5F, -8F, 0F, -5F, -8F); // decal
		headModel[1].setRotationPoint(0F, 0F, 0F);

		headModel[2].addBox(-8.5F, -10F, -9F, 16, 4, 1, 0F); // goggles
		headModel[2].setRotationPoint(0F, 0F, 0F);

		headModel[3].addShapeBox(-8.5F, -6F, -9F, 8, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // goggles2
		headModel[3].setRotationPoint(0F, 0F, 0F);

		headModel[4].addShapeBox(-0.5F, -6F, -9F, 8, 2, 1, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // goggles2
		headModel[4].setRotationPoint(0F, 0F, 0F);

		headModel[5].addBox(-11F, -10F, -2.5F, 2, 5, 8, 0F); // headset
		headModel[5].setRotationPoint(0F, 0F, 0F);

		headModel[6].addBox(9F, -12F, -2.5F, 3, 7, 8, 0F); // headset
		headModel[6].setRotationPoint(0F, 0F, 0F);

		headModel[7].addShapeBox(-11F, -11F, -2.5F, 2, 1, 8, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // headset2
		headModel[7].setRotationPoint(0F, 0F, 0F);

		headModel[8].addShapeBox(9F, -13F, -2.5F, 3, 1, 8, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // headset2
		headModel[8].setRotationPoint(0F, 0F, 0F);

		headModel[9].addShapeBox(-8.5F, -19F, -8.5F, 17, 2, 17, 0F, -3F, 0F, -3F, -3F, 0F, -3F, -3F, 0F, -3F, -3F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // helmet1
		headModel[9].setRotationPoint(0F, 0F, 0F);

		headModel[10].addShapeBox(-9.5F, -17F, -9.5F, 19, 5, 19, 0F, -1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // helmet2
		headModel[10].setRotationPoint(0F, 0F, 0F);

		headModel[11].addShapeBox(-9.5F, -12F, -9.5F, 19, 2, 19, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 1F, 1F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F); // helmet3
		headModel[11].setRotationPoint(0F, 0F, 0F);

		headModel[12].addShapeBox(-9.5F, -10F, -6.5F, 19, 5, 16, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // helmet4
		headModel[12].setRotationPoint(0F, 0F, 0F);

		headModel[13].addShapeBox(-9.5F, -5F, -6.5F, 19, 2, 16, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F); // helmet5
		headModel[13].setRotationPoint(0F, 0F, 0F);

		headModel[14].addShapeBox(-9.5F, -3F, -5.5F, 19, 1, 15, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -2F, -1F, 0F, -2F, -1F, 0F, 0F, -1F, 0F, 0F); // helmet6
		headModel[14].setRotationPoint(0F, 0F, 0F);

		headModel[15].addBox(-8.5F, -4F, -8.5F, 17, 4, 17, 0F); // helmetStrap3
		headModel[15].setRotationPoint(0F, 0F, 0F);

		headModel[16].addBox(-10.5F, -5F, -4.5F, 1, 1, 8, 0F); // mic
		headModel[16].setRotationPoint(0F, 0F, 0F);

		headModel[17].addShapeBox(9F, -5F, -2.5F, 3, 1, 8, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F); // Box 0
		headModel[17].setRotationPoint(0F, 0F, 0F);

		headModel[18].addShapeBox(7F, -19F, -0.5F, 4, 2, 3, 0F, 1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 1
		headModel[18].setRotationPoint(0F, 0F, 0F);

		headModel[19].addBox(9F, -17F, -0.5F, 2, 4, 3, 0F); // Box 2
		headModel[19].setRotationPoint(0F, 0F, 0F);

		headModel[20].addBox(5F, -21.5F, -3.5F, 3, 3, 10, 0F); // Box 3
		headModel[20].setRotationPoint(0F, 0F, 0F);

		headModel[21].addShapeBox(-9F, -14.5F, -10F, 18, 4, 1, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 4
		headModel[21].setRotationPoint(0F, 0F, 0F);

		headModel[22].addBox(-5.5F, -16F, -12F, 10, 4, 3, 0F); // Box 5
		headModel[22].setRotationPoint(0F, 0F, 0F);

		headModel[23].addShapeBox(-5.5F, -16F, -14F, 4, 2, 2, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 6
		headModel[23].setRotationPoint(0F, 0F, 0F);

		headModel[24].addShapeBox(-5.5F, -14F, -14F, 4, 2, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 7
		headModel[24].setRotationPoint(0F, 0F, 0F);

		headModel[25].addShapeBox(0.5F, -14F, -14F, 4, 2, 2, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F); // Box 8
		headModel[25].setRotationPoint(0F, 0F, 0F);

		headModel[26].addShapeBox(0.5F, -16F, -14F, 4, 2, 2, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 9
		headModel[26].setRotationPoint(0F, 0F, 0F);

		headModel[27].addShapeBox(-2.5F, -19F, -9F, 4, 3, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1.5F, 0F, 0F, 1.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 11
		headModel[27].setRotationPoint(0F, 0F, 0F);

		headModel[28].addShapeBox(-2.5F, -20F, -7.5F, 4, 1, 4, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1.5F, 0F, 0F, 1.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 12
		headModel[28].setRotationPoint(0F, 0F, 0F);


	}
}